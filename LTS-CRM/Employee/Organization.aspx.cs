﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Employee
{
    public partial class Organization : System.Web.UI.Page
    {
        //The Page_Load function is called every time the page is reloaded.
        //If a button is clicked Page_Load is called first, then the button event handler.
        protected void Page_Load(object sender, EventArgs e)
        {
            // 
            //A postback is when the user clicks a button after the page has loaded
            //if it's false then we do our one time load of data her if it exists
            //When True: The page is loaded for the first time
            //When False: More than likely a button was clicked or user action has happened, after the page was loaded.
            if (IsPostBack == false)
            {


                //Declare Connection Variable (Connection)
                //Create a new object SqlConnection with the connection string initialized
                //Set the Connection variable to point to the new object instance.
                //If connection goes out of scope the database connection won't be closed and must be closed with either ( Close or Disopose )
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                //Opens a connection to the database.
                //If available draws from an open conneciton from the connection pool(if available)
                Connection.Open();
                // 
                //if SessionValue("EditOrganization.ID") > 0 then this is an edit
                //if SessionValue("EditOrganization.ID") = 0 then this is an insert
                Session.Add("EditOrganization.ID", "1");
                if (SessionValue("EditOrganization.ID").ToString().Length > 0)
                {
                    String sql = "select * from [dbo].[Organization] where [ID] = @ID";
                    // 
                    //Declare SQLCommand Object named Command
                    //Create a new Command object with a select statement that will open the row referenced by Request("ID")
                    SqlCommand Command = new SqlCommand(sql, Connection);
                    // 
                    // 'Set the @ID parameter in the Command select query
                    Command.Parameters.AddWithValue("@ID", SessionValue("EditOrganization.ID"));
                    // 
                    //Declare a SqlDataReader Ojbect
                    //Load it with the Command's select statement
                    SqlDataReader Reader = Command.ExecuteReader();
                    // 
                    //If at least one record was found
                    if (Reader.Read())
                    {
                        lblOrganization.Text = Reader["OrganizationName"].ToString();
                        lblWebsite.Text = Reader["Website"].ToString();
                        lblemployee.Text = Reader["TotalEmployees"].ToString();
                        lblphoneno.Text = Reader["PhoneNumber"].ToString();
                        lblfaxno.Text = Reader["FaxNumber"].ToString();
                        lblcountry.Text = Reader["CountryCode"].ToString();
                        lblstate.Text = Reader["StateCode"].ToString();
                        lblcity.Text= Reader["CityCode"].ToString();
                       lbladdress.Text = Reader["Address"].ToString();
                       txtorganizationdes.InnerHtml = Reader["OrganizationDescription"].ToString();

                        byte[] img = (byte[])(Reader["Image"]);

                        // If you want convert to a bitmap file
                        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                        Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(img);

                        string imgString = Convert.ToBase64String(img);
                        //Set the source with data:image/bmp
                        lmglogo.Src = String.Format("data:image/Bmp;base64,{0}\"", imgString);

                    }
                    // 
                    //Close the Data Reader we are done with it.
                    Reader.Close();

                    //Closes the Connection to the back to the connection pool.
                    Connection.Close();
                    // 
                    //The length of ID equals zero.
                    //This is an insert so don't preload any data.
                }


            }
            // 
            //End of Page_Load Event Handler
        }

        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    
    }
}