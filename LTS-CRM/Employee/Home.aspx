﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Employee.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="LTS_CRM.Employee.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <!-- MAIN CONTENT -->
    
        <!-- breadcrumb -->
        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Dashboard</li>
            </ol>

        </div>
        <div class="well">

        <div class="page page-dashboard">



            <!-- cards row -->
            <div class="row">

                <div class="col-md-12">
                    <!-- tile -->
                    <section class="tile bg-greensea widget-appointments">

                        <!-- tile header -->
                        <div class="tile-header dvd dvd-btm">
                            <h1 class="custom-font">Worldwide Office Time</h1>

                        </div>
                        <!-- /tile header -->

                        <!-- tile body -->
                        <div class="tile-body">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-md-2 b-l text-center">
                                    <span id="day" class="day"></span>
                                    <br />
                                    <span id="monthyear" class="month"></span>
                                </div>
                                <div class="col-md-2 text-center">

                                    <h4 class="text-light">London</h4>
                                    <div style="width: 100%;">
                                        <canvas id="c1" class="CoolClock:minovateClock:50:3:0"></canvas>
                                    </div>
                                    <span class="text-light"></span>

                                </div>
                                <div class="col-md-2 text-center">

                                    <h4 class="text-light">Shanghai</h4>
                                    <div style="width: 100%;">
                                        <canvas id="Canvas1" class="CoolClock:minovateClock:50:3:8"></canvas>
                                    </div>
                                    <span class="text-light"></span>

                                </div>
                                <div class="col-md-2 text-center">

                                    <h4 class="text-light">Beijing</h4>
                                    <div style="width: 100%;">
                                        <canvas id="Canvas2" class="CoolClock:minovateClock:50:3:8"></canvas>
                                    </div>
                                    <span class="text-light"></span>

                                </div>

                                <div class="col-md-2 text-center">

                                    <h4 class="text-light">Ahmedabad</h4>
                                    <div style="width: 100%;">
                                        <canvas id="Canvas4" class="CoolClock:minovateClock:50:3:5.30"></canvas>
                                    </div>
                                    <span class="text-light"></span>

                                </div>
                                <div class="col-md-2 text-center">

                                    <h4 class="text-light">World Clock</h4>
                                    <div style="width: 100%;">
                                        <canvas id="Canvas3" class="CoolClock:minovateClock:50:3:0"></canvas>
                                    </div>
                                    <span class="text-light">
                                        <!-- multiple lang dropdown : find all flags in the flags page -->
                                        <ul class="header-dropdown-list hidden-xs">
                                            <li>
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <img class="flag flag-gb" src="img/blank.gif" alt="United Kingdom">
                                                    <span>United Kingdom </span><i class="fa fa-angle-down"></i></a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li class="active">
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-us" alt="United States">
                                                            English (US)</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-fr" alt="France">
                                                            Français</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-es" alt="Spanish">
                                                            Español</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-de" alt="German">
                                                            Deutsch</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-jp" alt="Japan">
                                                            日本語</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-cn" alt="China">
                                                            中文</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-it" alt="Italy">
                                                            Italiano</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-pt" alt="Portugal">
                                                            Portugal</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-ru" alt="Russia">
                                                            Русский язык</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">
                                                            <img src="img/blank.gif" class="flag flag-kr" alt="Korea">
                                                            한국어</a>
                                                    </li>

                                                </ul>
                                            </li>
                                        </ul>
                                        <!-- end multiple lang -->
                                    </span>

                                </div>

                                <!-- /col -->
                                <!-- col -->

                                <!-- /col -->
                            </div>
                            <!-- /row -->
                        </div>
                        <!-- /tile body -->

                        <!-- tile footer -->

                        <!-- /tile footer -->

                    </section>
                    <!-- /tile -->
                </div>

            </div>
        </div>



        <section id="Section1" class="">

            <!-- row -->
            <div class="row">

                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="true">
                        <!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				
								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"
				-->
                        <header>
                            <span class="widget-icon"><i class="fa fa-table"></i></span>
                            <h2>Task List </h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th data-hide="phone">No</th>
                                            <th data-class="expand"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>Customer Name</th>
                                            <th data-hide="phone"><i class="fa fa-fw fa-phone text-muted hidden-md hidden-sm hidden-xs"></i>Phone</th>
                                            <th>Company</th>
                                            <th data-hide="phone,tablet"><i class="fa fa-fw fa-calendar txt-color-blue hidden-md hidden-sm hidden-xs"></i>Date</th>
                                            <th data-hide="phone,tablet">Stage</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Jennifer</td>
                                            <td>1-342-463-8341</td>
                                            <td>Et Rutrum Non Associates</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-warning">Quotation</span></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Clark</td>
                                            <td>1-516-859-1120</td>
                                            <td>Nam Ac Inc.</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-warning">Quotation</span></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Brendan</td>
                                            <td>1-724-406-2487</td>
                                            <td>Enim Commodo Limited</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-success">Confirmation</span></td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Warren</td>
                                            <td>1-412-485-9725</td>
                                            <td>Odio Etiam Institute</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-danger">Pending Invoice</span></td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Rajah</td>
                                            <td>1-849-642-8777</td>
                                            <td>Neque Ltd</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-warning">Quotation</span></td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Demetrius</td>
                                            <td>1-470-329-9627</td>
                                            <td>Euismod In Ltd</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-success">Confirmation</span></td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Keefe</td>
                                            <td>1-188-191-2346</td>
                                            <td>Molestie Industries</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-success">Confirmation</span></td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Leila</td>
                                            <td>1-663-655-8904</td>
                                            <td>Est LLC</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-success">Confirmation</span></td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Fritz</td>
                                            <td>1-598-234-7837</td>
                                            <td>Et Ultrices Posuere Institute</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-warning">Quotation</span></td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Cassady</td>
                                            <td>1-212-965-8381</td>
                                            <td>Vitae Erat Vel Company</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-danger">Pending Invoice</span></td>
                                        </tr>

                                        <tr>
                                            <td>11</td>
                                            <td>Brielle</td>
                                            <td>1-216-787-0056</td>
                                            <td>Quis Massa Mauris Institute</td>
                                            <td>03/01/2016</td>
                                            <td><span class="center-block padding-5 label label-success">Confirmation</span></td>
                                        </tr>

                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                </article>

                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <!-- new widget -->
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-4" data-widget-editbutton="false" data-widget-colorbutton="false">

                        <!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"

								-->

                        <header>
                            <span class="widget-icon"><i class="fa fa-check txt-color-white"></i></span>
                            <h2>Announcement/News </h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- end widget edit box <div class="widget-body no-padding smart-form">-->
                            <div class="custom-scroll table-responsive" style="height: 470px; overflow-y: scroll;">


                                <div class="row" style="width: 100%">
                                    <div class="col-xs-2 col-sm-1">
                                        <time datetime="2017-01-01" class="icon">
                                            <strong>Jan</strong>
                                            <span>01</span>
                                        </time>
                                    </div>

                                    <div class="col-xs-10 col-sm-11">
                                        <h6 class="no-margin"><a href="javascript:void(0);">Welcome to Let's Travel CRM</a></h6>
                                        <p>
                                            Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi Nam eget dui.
																			Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
																			sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel.
                                        </p>
                                    </div>

                                    <div class="col-sm-12">

                                        <hr>
                                    </div>

                                    <div class="col-xs-2 col-sm-1">
                                        <time datetime="2017-01-11" class="icon">
                                            <strong>Jan</strong>
                                            <span>11</span>
                                        </time>
                                    </div>

                                    <div class="col-xs-10 col-sm-11">
                                        <h6 class="no-margin"><a href="javascript:void(0);">Welcome to Let's Travel CRM</a></h6>
                                        <p>
                                            Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi Nam eget dui.
												Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
												sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel.
                                        </p>
                                    </div>

                                    <div class="col-sm-12">

                                        <hr>
                                    </div>

                                    <div class="col-xs-2 col-sm-1">
                                        <time datetime="2017-01-11" class="icon">
                                            <strong>Jan</strong>
                                            <span>11</span>
                                        </time>
                                    </div>

                                    <div class="col-xs-10 col-sm-11">
                                        <h6 class="no-margin"><a href="javascript:void(0);">Welcome to Let's Travel CRM</a></h6>
                                        <p>
                                            Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi Nam eget dui.
												Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
												sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel.
                                        </p>
                                    </div>






                                    <div class="col-sm-12">

                                        <hr>
                                    </div>






                                </div>

                                <!-- end content -->
                            </div>

                        </div>
                        <!-- end widget div -->
                    </div>
                    <!-- end widget -->
                </article>


            </div>
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <!-- widget options:
								        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

								        data-widget-colorbutton="false"
								        data-widget-editbutton="false"
								        data-widget-togglebutton="false"
								        data-widget-deletebutton="false"
								        data-widget-fullscreenbutton="false"
								        data-widget-custombutton="false"
								        data-widget-collapsed="true"
								        data-widget-sortable="false"

								        -->
                        <header>
                            <span class="widget-icon"><i class="fa fa-group"></i></span>
                            <h2>Employee Directory </h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content <div class="custom-scroll table-responsive" style="height:480px; overflow-y: scroll;">-->
                            <div class="widget-body no-padding">

                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">

                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="ID" />
                                            </th>


                                            <th class="hasinput">
                                                <input type="text" class="form-control" placeholder="Name" />
                                            </th>
                                            <th class="hasinput">
                                                <input type="text" class="form-control" placeholder="Designation" />
                                            </th>
                                            <th class="hasinput" style="width: 12%">
                                                <input type="text" class="form-control" placeholder="Email" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Phone" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Office" />
                                            </th>

                                        </tr>
                                        <tr>
                                            <th data-class="expand">ID</th>
                                            <th data-hide="phone, tablet">Name</th>
                                            <th data-hide="phone, tablet">Designation</th>
                                            <th data-hide="phone, tablet">Email</th>
                                            <th data-hide="phone,tablet">Phone</th>
                                            <th>Office</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>100</td>
                                            <td>Chirag Golwala</td>
                                            <td>CEO</td>
                                            <td>chirag@letstravelservices.com</td>
                                            <td>+44(0)287237911</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>101</td>
                                            <td>Rui Men</td>
                                            <td>Managing Director</td>
                                            <td>rui@letstravelservices.com</td>
                                            <td>+44(0)287237912</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>102</td>
                                            <td>Lesley Marshal</td>
                                            <td>Head of Operation</td>
                                            <td>Lesley@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>103</td>
                                            <td>Mitali Vankani</td>
                                            <td>Project Cordinator</td>
                                            <td>mitali@letstravelservices.com</td>
                                            <td>+44(0)287237914</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>104</td>
                                            <td>Rishabh Sheth</td>
                                            <td>Operation Manager</td>
                                            <td>rishabh@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>105</td>
                                            <td>Angelica Ramos</td>
                                            <td>Accountant</td>
                                            <td>angelica@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">China</span></td>
                                        </tr>
                                        <tr>
                                            <td>106</td>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>aston@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">China</span></td>
                                        </tr>
                                        <tr>
                                            <td>107</td>
                                            <td>Brielle Williamson</td>
                                            <td>Integration Specialist</td>
                                            <td>brielle@letstravelservices.com</td>
                                            <td>+91(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">Ahmedabad</span></td>
                                        </tr>
                                        <tr>
                                            <td>108</td>
                                            <td>Caesar Vance</td>
                                            <td>Pre-Sales Support</td>
                                            <td>Caesar@letstravelservices.com</td>
                                            <td>+91(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">Ahmedabad</span></td>
                                        </tr>
                                        <tr>
                                            <td>100</td>
                                            <td>Chirag Golwala</td>
                                            <td>CEO</td>
                                            <td>chirag@letstravelservices.com</td>
                                            <td>+44(0)287237911</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>101</td>
                                            <td>Rui Men</td>
                                            <td>Managing Director</td>
                                            <td>rui@letstravelservices.com</td>
                                            <td>+44(0)287237912</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>102</td>
                                            <td>Lesley Marshal</td>
                                            <td>Head of Operation</td>
                                            <td>Lesley@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>103</td>
                                            <td>Mitali Vankani</td>
                                            <td>Project Cordinator</td>
                                            <td>mitali@letstravelservices.com</td>
                                            <td>+44(0)287237914</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>104</td>
                                            <td>Rishabh Sheth</td>
                                            <td>Operation Manager</td>
                                            <td>rishabh@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">London HQ</span></td>
                                        </tr>
                                        <tr>
                                            <td>105</td>
                                            <td>Angelica Ramos</td>
                                            <td>Accountant</td>
                                            <td>angelica@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">China</span></td>
                                        </tr>
                                        <tr>
                                            <td>106</td>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>aston@letstravelservices.com</td>
                                            <td>+44(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">China</span></td>
                                        </tr>
                                        <tr>
                                            <td>107</td>
                                            <td>Brielle Williamson</td>
                                            <td>Integration Specialist</td>
                                            <td>brielle@letstravelservices.com</td>
                                            <td>+91(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">Ahmedabad</span></td>
                                        </tr>
                                        <tr>
                                            <td>108</td>
                                            <td>Caesar Vance</td>
                                            <td>Pre-Sales Support</td>
                                            <td>Caesar@letstravelservices.com</td>
                                            <td>+91(0)287237913</td>
                                            <td><span class="center-block padding-5 label label-success">Ahmedabad</span></td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>

                </article>
                <!-- end widget -->

                <!-- end row -->
            </div>
            <!-- row -->

            <div class="row">

                <!-- a blank row to get started -->
                <div class="col-sm-12">
                    <!-- your contents here -->
                </div>

            </div>

            <!-- end row -->

        </section>
        <!-- end widget grid -->
        <!-- widget grid -->

        <!-- end widget grid -->
</div>
  
    <!-- END MAIN CONTENT -->

 
    <script>

        var monthNames = ["JAN", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var d = new Date();
        document.getElementById("day").innerHTML = d.getDate();
        document.getElementById("monthyear").innerHTML = monthNames[d.getMonth()] + "-" + d.getFullYear();

    </script>

</asp:Content>
