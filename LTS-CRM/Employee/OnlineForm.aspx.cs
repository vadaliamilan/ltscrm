﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Employee
{
    public partial class OnlineForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Session.Add("CompanyID", "1");
                //Populating a DataTable from database.
                DataTable dt = this.GetDataColumn();                
                //Building an HTML string.
                StringBuilder html = new StringBuilder();
                //Table start.
                html.Append("<table id='datatable_fixed_column1' class='table table-striped table-bordered'  width='100%'>");
                //Building the Header row.
                string strimg = @"..\img\pdf.png";
                //Building the Data rows.
                foreach (DataRow row in dt.Rows)
                {

                    foreach (DataColumn column in dt.Columns)
                    {
                        html.Append("<tr align='center' >");
                        html.Append("<th  colspan='4' data-class='expand' width='100%' >");
                        html.Append(row[column.ColumnName]);
                        html.Append("</th>");
                        html.Append("</tr>");
                        DataTable dt1 = this.GetDataRow(row[column.ColumnName].ToString());                      
                        int rowno = 0;
                        for (int r = 0; r < dt1.Rows.Count; r += 0)
                        {
                              DataRow row1 = dt1.Rows[r];
                              //do stuff
                              html.Append("<tr align='center' width='100%'>");
                              html.Append("<td width='25%'>");                            
                              if (rowno < dt1.Rows.Count)
                              {
                                  string ext = comman.GetSimpleExtension(row1["FileName"].ToString());
                                  strimg = @"..\img\DocIcon\" + ext + ".png";
                                  //html.Append("<a href=\"#?id='101'\" runat=\"server\" onServerClick=\"room_click\">");
                                  html.Append("<a href=\"?id=" + comman.Encrypt(row1["ID"].ToString()) + "\" runat=\"server\" onServerClick=\"btnEdit_Click\" >");
                                  html.Append("<img src=\"" + strimg + "\" alt=\"\" border=\"3\" class=\"img-responsive\"></img>");
                                  html.Append(row1["FormName"]);
                                  html.Append("</a>");
                                  //html.Append("<asp:LinkButton class=\"btn bg-color-blueDark txt-color-white\" ID='" + row1["ID"] + "' runat=\"server\" Text=\"Click\" CommandArgument='" + row1["ID"] + "' Visible=\"true\" OnClick=\"btnEdit_Click\"><i class=\"fa fa-edit\"></i><span class=\"hidden-mobile\"> </span></asp:LinkButton>");
                                  rowno = rowno + 1;
                                  r = r + 1;
                              }
                              html.Append("</td >");
                              html.Append("<td width='25%'>");
                              if (rowno < dt1.Rows.Count)
                              {
                                  row1 = dt1.Rows[r];
                                  string ext = comman.GetSimpleExtension(row1["FileName"].ToString());
                                  strimg = @"..\img\DocIcon\" + ext + ".png";                                  
                                  html.Append("<a href=\"?id=" + comman.Encrypt(row1["ID"].ToString()) + "\" runat=\"server\" onServerClick=\"btnEdit_Click\" >");
                                  html.Append("<img src=\"" + strimg + "\" alt=\"\" border=\"3\" class=\"img-responsive\"></img>");
                                  html.Append(row1["FormName"]);
                                  html.Append("</a>");
                                  rowno = rowno + 1;
                                  r = r + 1;
                              }
                              html.Append("</td >");
                              html.Append("<td width='25%'>");
                              if (rowno < dt1.Rows.Count)
                              {
                                  row1 = dt1.Rows[r];
                                  string ext = comman.GetSimpleExtension(row1["FileName"].ToString());
                                  strimg = @"..\img\DocIcon\" + ext + ".png";
                                  
                                  html.Append("<a href=\"?id=" + comman.Encrypt(row1["ID"].ToString()) + "\" runat=\"server\" onServerClick=\"btnEdit_Click\" >");
                                  html.Append("<img src=\"" + strimg + "\" alt=\"\" border=\"3\" class=\"img-responsive\"></img>");
                                  html.Append(row1["FormName"]);
                                  html.Append("</a>");
                                  rowno = rowno + 1;
                                  r = r + 1;
                              }
                              html.Append("</td >");
                              html.Append("<td width='25%'>");
                              if (rowno < dt1.Rows.Count)
                              {
                                  row1 = dt1.Rows[r];
                                  string ext = comman.GetSimpleExtension(row1["FileName"].ToString());
                                  strimg = @"..\img\DocIcon\" + ext + ".png";

                                  html.Append("<a href=\"?id=" + comman.Encrypt(row1["ID"].ToString()) + "\" runat=\"server\" onServerClick=\"btnEdit_Click\" >");
                                  html.Append("<img src=\"" + strimg + "\" alt=\"\" border=\"3\" class=\"img-responsive\"></img>");
                                  html.Append(row1["FormName"]);
                                  html.Append("</a>");
                                  rowno = rowno + 1;
                                  r = r + 1;
                              }
                              html.Append("</td >");
                                html.Append("</tr>");                            
                        }
                    }
                }
                //Table end.
                html.Append("</table>");
                //Append the HTML string to Placeholder.
                PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
            }
            string id = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id))
            {
                DocDownload(comman.Decrypt(id));
            }
        }


        protected void DocDownload(string ID)
        {
            DataTable file = GetAFile(Convert.ToInt16(ID));
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["FileType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }
        // Get a file from the database by ID
        public static DataTable GetAFile(int id)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            DataTable file = new DataTable();

            Connection.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Connection;
            cmd.CommandTimeout = 0;

            cmd.CommandText = "SELECT ID, FileName, FileType, FileData FROM DocumentUpload "
                + "WHERE ID=@ID";
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();

            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters["@ID"].Value = id;

            adapter.SelectCommand = cmd;
            adapter.Fill(file);

            Connection.Close();

            return file;
        }
        private DataTable GetDataColumn()
        {
            //DataTable dt = new DataTable();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            string sql = "SELECT  Distinct DepartmentDepartmentID.DepartmentName as DepartmentName FROM ([dbo].[DocumentUpload] left join [Company] CompanyCompanyID on [dbo].[DocumentUpload].CompanyID = CompanyCompanyID.ID )  left join [Department] DepartmentDepartmentID on [dbo].[DocumentUpload].DepartmentID = DepartmentDepartmentID.ID left join [DocumentCategory]  on [dbo].[DocumentUpload].DocumentCategory = DocumentCategory.ID where [dbo].[DocumentUpload].DocumentCategory=3 And  [dbo].[DocumentUpload].CompanyID=@CompanyID";
            SqlCommand Command = new SqlCommand(sql);
            Command.Connection = Connection;
            Command.CommandType = CommandType.Text;
            Command.Parameters.AddWithValue("@CompanyID", SessionValue("CompanyID"));
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = Command;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            Connection.Close();
            return dt;
        }
        private DataTable GetDataRow(string department)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            string sql = " SELECT  [dbo].[DocumentUpload].FormName ,DocumentUpload.ID,DocumentUpload.FileName FROM ([dbo].[DocumentUpload] left join [Company] CompanyCompanyID on [dbo].[DocumentUpload].CompanyID = CompanyCompanyID.ID )  left join [Department] DepartmentDepartmentID on [dbo].[DocumentUpload].DepartmentID = DepartmentDepartmentID.ID left join [DocumentCategory]  on [dbo].[DocumentUpload].DocumentCategory = DocumentCategory.ID  where [dbo].[DocumentUpload].DocumentCategory=3 And  DepartmentDepartmentID.DepartmentName=@Department And [dbo].[DocumentUpload].CompanyID=@CompanyID";
            SqlCommand Command = new SqlCommand(sql);
            Command.Connection = Connection;
            Command.CommandType = CommandType.Text;
            Command.Parameters.AddWithValue("@CompanyID", SessionValue("CompanyID"));
            Command.Parameters.AddWithValue("@Department", department);
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = Command;
            DataTable dt = new DataTable();
            sda.Fill(dt);
            Connection.Close();
            return dt;
        }

        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    }
}