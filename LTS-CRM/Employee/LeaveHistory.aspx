﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Employee.Master" AutoEventWireup="true" CodeBehind="LeaveHistory.aspx.cs" Inherits="LTS_CRM.Employee.LeaveHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Leave</li>
                <li>Leave Status</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Leave Status </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">                           
                            </div>                        
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">
                                    <div class="row">
                                        <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                            <div class="input-group">
                                            </div>
                                        </div>
                                        </div>
                                           <!-- Load jQuery, SimpleModal and Basic JS files -->
  
     <link href="../smoothness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
    <script src="../smoothness/jquery-1.3.2.min.js" type="text/javascript"></script>
	<script src="../smoothness/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
<script type = "text/javascript">

    function setCurrentIndexOfGrid(index) {
        var grid = document.getElementById('<%=GridView1.ClientID %>');
        //        var cell = grid.rows[(+index) + 1].cells[0];
        //        //alert(cell.innerHTML);
        var arefno = document.getElementById('<%=lblRefNo.ClientID %>')
        arefno.innerHTML = grid.rows[(+index) + 1].cells[1].innerHTML;

        var adate = document.getElementById('<%=lblApplydate.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[0].innerHTML;

        var adate = document.getElementById('<%=lblLeaveType.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[2].innerHTML;

        var adate = document.getElementById('<%=lbldatefrom.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[3].innerHTML;

        var adate = document.getElementById('<%=lblDateTo.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[4].innerHTML;

        var adate = document.getElementById('<%=lblDayType.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[5].innerHTML;

        var adate = document.getElementById('<%=lblstatus.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[6].innerHTML;

        var adate = document.getElementById('<%=lblEmpName.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[9].innerHTML;

        var adate = document.getElementById('<%=lblReason.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[8].innerHTML;

        var adate = document.getElementById('<%=lblProgress.ClientID %>')
        adate.innerHTML = grid.rows[(+index) + 1].cells[7].innerHTML;
        return rowAction(index)
    }


 </script>
 <script type="text/javascript">
     $().ready(function () {
         $('#dialogContent').dialog({
             autoOpen: false,
             modal: true,
             bgiframe: true,
             title: "Leave Application Details",
             width: 550,
             height: 350
         });
     });

     function rowAction(uniqueID) {
         //$('#dialogContent').dialog('option', 'buttons',
         $('#dialogContent').dialog('option',
				{
				    "OK": function () { __doPostBack(uniqueID, ''); $(this).dialog("close"); },
				    "Cancel": function () { $(this).dialog("close"); }
				});

         $('#dialogContent').dialog('open');

         return false;
     }


	</script>
    

                <table id="datatable_fixed_column1" class="table table-striped table-bordered" width="100%" >
                    <tr>
                        <td style="height: 433px" align="left" valign="top">
                      
                                    <table style="width: 100%; font-size: 10pt; color: black; font-family: Verdena;">
                                    <tr>
                                    
                                        <tr>
                                            <td colspan="6" style="height: 18px; width: 545px;">
                                                <strong>
                                                    <br />
                                                    You can view the status of all your leave application here.<br />
                                                    <br />
                                                    To View Application Status :<br />
                                                    <br />
                                                    </strong></td>
                                    </tr>
                                        
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="6" style="height: 18px;  valign="middle">
                                                <strong>Step 1. Select the approval status that you want to view.<br />
                                                </strong></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="height: 45px;  valign="middle">
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" style="width: 100px; height: 18px">
                                                            </td>
                                                        <td align="left" style="width: 100px; height: 18px">
                                                            <asp:CheckBox ID="Approved" runat="server" Checked="True" Text="Approved" /></td>
                                                        <td align="left" style="width: 100px; height: 18px">
                                                            <asp:CheckBox ID="Rejected" runat="server" Checked="True" Text="Rejected" /></td>
                                                        <td align="left" style="width: 216px; height: 18px">
                                                            <asp:CheckBox ID="Canceled" runat="server" Checked="True" Text="Cancelled" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="width: 100px; height: 18px">
                                                        </td>
                                                        <td align="left" style="width: 100px; height: 18px">
                                                        </td>
                                                        <td align="left" style="width: 100px; height: 18px">
                                                        </td>
                                                        <td align="left" style="width: 216px; height: 18px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="4" style="height: 18px">
                                                            <strong>Step 2. Please click the view button<br />
                                                            </strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="4">
                                                            <asp:Button ID="Button1" runat="server" Text="View" Width="80px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="4">
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width: 100%;">
                             
                                <tr>
                                    <td style="border: 1px solid #000000" align="left" valign="top">
                                        <asp:GridView  runat="server" id="GridView1" class="table table-striped table-bordered" width="100%"
                                            EmptyDataText="No Data" AllowPaging="True" PageSize="10" OnRowDataBound="GridView1_RowDataBound" >
                                            <FooterStyle BackColor="#CCCCCC" />
                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White"  HorizontalAlign ="Left" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White"  />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#808080" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#383838" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                                            </td>
                                        </tr>
                                    </table>
                             
            
                        </td>
                    </tr>
                    <tr>
                        <td>
            </td>
                    </tr>
                </table>
        
        <div id="dialogContent" style="display:none">
		<table align="center" style="font-size: 11px; width: 501px; font-family: Verdana;
                                ">
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                                                            Leave Ref. No.</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="height: 20px; width: 279px;">
                                        <asp:Label ID="lblRefNo" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                        Name(Emp. ID)</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="width: 279px; height: 20px">
                                        <asp:Label ID="lblEmpName" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                                                            Leave Apply Date</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="height: 20px; width: 279px;">
                                        <asp:Label ID="lblApplydate" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                        Leave Type</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="height: 20px; width: 279px;">
                                        <asp:Label ID="lblLeaveType" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                        Leave Date From</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" style="width: 279px; height: 20px" colspan="4">
                                        <asp:Label ID="lbldatefrom" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                        Leave Date To</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="width: 279px; height: 20px">
                                        <asp:Label ID="lblDateTo" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                                                            Leave Day Type</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="height: 20px; width: 279px;">
                                        <asp:Label ID="lblDayType" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 22px">
                                                                            Reason</td>
                                    <td align="left" style="width: 10px; height: 22px">
                                        :</td>
                                    <td align="left" colspan="4" style="height: 22px; width: 279px;">
                                        <asp:Label ID="lblReason" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                        Leave Status</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="height: 20px; width: 279px;">
                                        <asp:Label ID="lblstatus" runat="server" Font-Bold="True" ForeColor="#C00000"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-weight: bold; width: 136px; height: 20px">
                                        Current Status</td>
                                    <td align="left" style="width: 10px; height: 20px">
                                        :</td>
                                    <td align="left" colspan="4" style="height: 20px; width: 279px;">
                                        &nbsp;<asp:Label ID="lblProgress" runat="server"></asp:Label></td>
                                </tr>
                                
                            </table>
                                           

                                  
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</asp:Content>

