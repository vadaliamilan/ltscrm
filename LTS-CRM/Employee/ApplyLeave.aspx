﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Employee.Master" AutoEventWireup="true" CodeBehind="ApplyLeave.aspx.cs" Inherits="LTS_CRM.Employee.ApplyLeave" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <div class="well">
        <asp:Label ID="msglbl" runat="server" Text=""></asp:Label>
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Leave</li>
                <li>New Leave Request</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Leave Request </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">

                                   
                                    <div class="well well-lg">
                                        <table class="table table-striped table-bordered" width="100%">

                                            <tbody>
                                                <tr>
                                                    <td style="width: 10%;">Office</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblCompany" runat="server" Text="Let's Travel Services"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Department</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblDepartment" runat="server" Text="Operation"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 10%;">Reporting to</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblhod" runat="server" Text="Ms Rui"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Employee Name</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblEmployeeName" runat="server" Text="Chin Chan"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Apply Date</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblApplyDate" runat="server" Text="23/01/2017"></asp:Label>
                                                            <asp:Label ID="lblRefNo" runat="server" Visible="False"></asp:Label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td colspan="3">
                                                        <div class="smart-form">


                                                            <fieldset>
                                                                <div class="row">
                                                                    <section class="col col-4">
                                                                        <label class="label">Leave Type</label>
                                                                        <label class="input">

                                                                             <asp:DropDownList class="form-control" ID="cmbLeaveType"  runat="server"></asp:DropDownList>
                                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ControlToValidate="cmbLeaveType" runat="server" ErrorMessage="Leave Type Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>

                                                                </div>
                                                                 <div class="row">
                                                                     <section class="col col-2">
                                                                        <label class="label">Start Date</label>
                                                                        <label class="input">
                                                                            <i class="icon-append fa fa-calendar"></i>
                                                                           <asp:TextBox ID="txtStartDate"  class="datepicker" data-dateformat='dd/mm/yy' runat="server" AutoPostBack="True" OnTextChanged="CalendarChange"></asp:TextBox>
                                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtStartDate" runat="server" ErrorMessage="Start date required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>

                                                                    </section>
                                                                      <section class="col col-2">
                                                                        <label class="label">End Date</label>
                                                                        <label class="input">
                                                                            <i class="icon-append fa fa-calendar"></i>
                                                                           
                                                                             <asp:TextBox ID="txtEndDate"  class="datepicker" data-dateformat='dd/mm/yy' runat="server" AutoPostBack="True" OnTextChanged="CalendarChange"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtEndDate" runat="server" ErrorMessage="End date required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>

                                                                    </section>
                                                                </div>
                                                               
                                                                <div class="row">
                                                                     
                                                                    <section class="col col-2">
                                                                        <label class="label">Day Type</label>
                                                                        <label class="input">

                                                                             <asp:DropDownList class="form-control" ID="cbodaytype" name="cbodaytype" OnSelectedIndexChanged = "cboDaytype_Changed" AutoPostBack = "true" runat="server">
                                                                                 <asp:ListItem Text="Full Day" Value ="FullDay" Selected ="True"></asp:ListItem>
                                                                                 <asp:ListItem Text="First Half" Value ="FirstHalf" ></asp:ListItem>
                                                                                 <asp:ListItem Text="Second Half" Value ="SecondHalf" ></asp:ListItem>
                                                                             </asp:DropDownList>
                                                                         
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-2">
                                                                        <label class="label">Day Count</label>
                                                                        <label class="input">
                                                                             <asp:Label ID="lbldaycount" runat="server" ></asp:Label>
                                                                        </label>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-4">
                                                                        <label class="label">Reason </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtreason" runat="server" class="form-control" placeholder="Reason" Text="" TextMode="MultiLine" Height="60px" MaxLength="250"></asp:TextBox>

                                                                        </label>
                                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtreason" runat="server" ErrorMessage="Please enter reason" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </section>                                                                   
                                                                   

                                                                </div>
                                                            </fieldset>

                                                            <footer>
                                                                <asp:Button ID="btnSave" runat="server" class="btn bg-color-blueDark txt-color-white" Text="Submit" style="width: 100px" OnClick="btnSave_Click" />                                                                
                                                             
                                                               
                                                                <button id="Button2" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px">
                                                                    <span style="width: 80px">Cancel </span>
                                                                </button>
                                                            </footer>


                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

      <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
