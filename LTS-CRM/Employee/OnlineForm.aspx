﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Employee.Master" AutoEventWireup="true" CodeBehind="OnlineForm.aspx.cs" Inherits="LTS_CRM.Employee.OnlineForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">                
                <li>Document List</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Document </h2>
                        </header>
                        <div>
                            
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">
                                   
                                   
                                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>




</asp:Content>
