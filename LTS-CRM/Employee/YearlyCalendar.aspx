﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Employee.Master" AutoEventWireup="true" CodeBehind="YearlyCalendar.aspx.cs" Inherits="LTS_CRM.Employee.YearlyCalendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="well">
        <asp:Label ID="msglbl" runat="server" Text=""></asp:Label>
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Leave</li>
                <li>Leave Calendar</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Leave Calendar</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">

                                   
                                    <div class="well well-lg">
                                      
              <table id="Table1" border="1" bordercolor="#000033" width="100%">
                        <tr>
                            <td style="width:25%">
                                <asp:Calendar ID="C1" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" " Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="Silver" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%">
                                <asp:Calendar ID="C2" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" " Height="100%" Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%">
                                <asp:Calendar ID="C3" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" " Height="100%" Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%">
                                <asp:Calendar ID="C4" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C5" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C6" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C7" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C8" runat="server" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C9" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C10" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C11" runat="server"  BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="#E0E0E0" />
                                    <TodayDayStyle BackColor="Gainsboro" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                            <td style="width:25%" height="182">
                                <asp:Calendar ID="C12" runat="server" BorderColor="Silver"
                                    BorderStyle="Solid" BorderWidth="1px" FirstDayOfWeek="Monday" Font-Names="Verdana"
                                    Font-Size="9pt" ForeColor="Black" NextMonthText=" " PrevMonthText=" "
                                    Width="100%" OnDayRender="C1_DayRender" BackColor="White">
                                    <SelectedDayStyle BackColor="White" ForeColor="White" />
                                    <WeekendDayStyle BorderColor="Silver" />
                                    <TodayDayStyle BackColor="#999999" ForeColor="White" />
                                    <OtherMonthDayStyle ForeColor="Tomato" BackColor="White" />
                                    <DayStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        HorizontalAlign="Center" VerticalAlign="Top" />
                                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                    <DayHeaderStyle BackColor="Transparent" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Size="8pt" ForeColor="Black" Height="8pt" HorizontalAlign="Center" />
                                    <TitleStyle BackColor="#001F3E" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True"
                                        Font-Size="12px" ForeColor="White" Height="10px" />
                                </asp:Calendar>
                            </td>
                        </tr>
                    </table>
                    <br />
                <table width="100%" style="padding: 8px; margin: 8px; font-size: 12pt; color: black; font-family: Verdena" class="table">
                    <tr>
                        <td bordercolor="#666666" colspan="8" style="height: 24px">
                            <strong>Legend</strong></td>
                    </tr>
                    <tr>
                        <td style="width: 13px; height: 24px">
                            <asp:Label ID="Label1" runat="server" BackColor="Red" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="Red" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Full Day Annual Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label5" runat="server" BackColor="Orange" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="Orange" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Medical Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label9" runat="server" BackColor="MidnightBlue" BorderColor="Black"
                                BorderStyle="Solid" BorderWidth="1px" ForeColor="MidnightBlue" Text="1"
                                Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Compassionate Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label13" runat="server" BackColor="Green" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="Green" Text="1" Width="20px"></asp:Label></td>
                        <td style=" padding: 5px; margin: 5px; height: 24px">
                            Congratulatory Leave</td>
                    </tr>
                    <tr>
                        <td style="width: 13px; height: 24px">
                            <asp:Label ID="Label2" runat="server" BackColor="SaddleBrown" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="SaddleBrown" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            First Half Annual Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label6" runat="server" BackColor="SteelBlue" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="SteelBlue" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Hospitalization</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label10" runat="server" BackColor="LightGreen" BorderColor="Black"
                                BorderStyle="Solid" BorderWidth="1px" ForeColor="LightGreen" Text="1"
                                Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Maternity Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label14" runat="server" BackColor="DarkGoldenrod" BorderColor="Black"
                                BorderStyle="Solid" BorderWidth="1px" ForeColor="DarkGoldenrod"
                                Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Study Leave&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 13px; height: 24px">
                            <asp:Label ID="Label3" runat="server" BackColor="DarkCyan" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="DarkCyan" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Second Half Annual Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label7" runat="server" BackColor="Black" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="Black" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Unpaid Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label11" runat="server" BackColor="SkyBlue" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="SkyBlue" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Paternity Leave</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label15" runat="server" BackColor="DarkKhaki" BorderColor="Black"
                                BorderStyle="Solid" BorderWidth="1px" ForeColor="DarkKhaki" Text="1"
                                Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Examination Leave</td>
                    </tr>
                    <tr>
                        <td style="width: 13px; height: 24px">
                            <asp:Label ID="Label4" runat="server" BackColor="Maroon" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="Maroon" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Pending for Approval</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label8" runat="server" BackColor="SlateGray" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="SlateGray" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Pending for Cancellation</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label12" runat="server" BackColor="DodgerBlue" BorderColor="Black"
                                BorderStyle="Solid" BorderWidth="1px" ForeColor="DodgerBlue" Text="1"
                                Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Public Holiday</td>
                        <td style="width: 3px; height: 24px">
                        </td>
                        <td style="height: 24px">
                        </td>
                    </tr>
                </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</asp:Content>
