﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Employee
{
    public partial class LeaveCalendar : System.Web.UI.Page
    {
        int val = 0;
        System.DateTime Edate = default(System.DateTime);
        string tooltip = null;
        string cCode = null;
        string CColor = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Add("UserName", "EMP0001");
            Session.Add("CompanyID", "1");
            Session.Add("EmpGender", "Male");
            Session.Add("EmpHOD", "1");
            if (IsPostBack == false)
            {
                Holidays();
                LeaveTransaction();
                duration();
            }
        }
        private void LeaveTransaction()
        {
            string StrSql = null;
            int aYear = 0;
            System.Data.DataSet ds = new System.Data.DataSet();


            aYear = DateTime.Now.Date.Year;
            StrSql = "Select LeaveDateFrom as StartDate,(DateDiff(day,LeavedateFrom,LeaveDateTo)+1) as DayDiff,LeaveType,Status,isnull(ApplyCanceled ,0) as AppCancel,Employee.EmployeeID as ID,DayType,Employee.FirstName + ' ' + Employee.SurName as [Name] From LeaveTransaction Inner Join Employee on Employee.EmployeeID=LeaveTransaction.EmployeeID where  CompanyID='" + SessionValue("CompanyID").ToString() + "'  And LeaveTransaction.Status IN ('Application Approved','Pending Approval','Cancellation Approved') Order By StartDate";
            ds = CreatDsPage(StrSql);
            Session.Add("DsLT", ds);


        }

        private void Holidays()
        {
            string StrSql = null;
            StrSql = "Select Date,HolidayName as Description from EmpHoliday where  CompanyID='" + SessionValue("CompanyID").ToString() + "'";
            System.Data.DataSet ds = new System.Data.DataSet();
            ds = CreatDsPage(StrSql);
            Session.Add("DsHoliday", ds);
        }
        private void duration()
        {
            DataTable dt = new DataTable();
            dt = new DataTable();
            DataRow dr = default(DataRow);

            dt.Columns.Add(new DataColumn("StartDate", typeof(string)));
            dt.Columns.Add(new DataColumn("DayDiff", typeof(int)));
            dt.Columns.Add(new DataColumn("LeaveType", typeof(string)));
            dt.Columns.Add(new DataColumn("Status", typeof(string)));
            dt.Columns.Add(new DataColumn("AppCancel", typeof(bool)));
            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("DayType", typeof(string)));
            dt.Columns.Add(new DataColumn("Name", typeof(string)));

            DataSet dsLT = new DataSet();
            dsLT = (DataSet)Session["DSLT"];
            int i = 0;

            for (i = 0; i <= dsLT.Tables[0].Rows.Count - 1; i++)
            {
                DataRow row1 = dsLT.Tables[0].Rows[i];
                System.DateTime leaveStartDate = default(System.DateTime);
                int dayAdd = 0;
                int daydiff = 0;
                leaveStartDate = (DateTime)row1["StartDate"];
                daydiff = (int)row1["DayDiff"];

                for (dayAdd = 1; dayAdd <= daydiff; dayAdd++)
                {
                    if (dayAdd > 1)
                    {
                        leaveStartDate = leaveStartDate.AddDays(1);// DateAdd(DateInterval.Day, 1, leaveStartDate);
                    }
                    if (IsHoliday(leaveStartDate) == false)
                    {
                        if (leaveStartDate.DayOfWeek != DayOfWeek.Saturday & leaveStartDate.DayOfWeek != DayOfWeek.Sunday)
                        {
                            dr = dt.NewRow();
                            dr[0] = leaveStartDate;
                            dr[1] = (int)row1["DayDiff"];
                            dr[2] = row1["LeaveType"].ToString();
                            dr[3] = row1["Status"].ToString();
                            dr[4] = (bool)row1["AppCancel"];
                            dr[5] = row1["ID"].ToString();
                            dr[6] = row1["DayType"].ToString();
                            dr[7] = row1["Name"].ToString();
                            dt.Rows.Add(dr);
                            //ElseIf dsLT.Tables(0).Rows(i).Item("LeaveType").ToString = "Maternity Leave" Then

                        }
                    }
                }

                ViewState.Add("dt",dt);
            }

        }
        private bool IsHoliday(System.DateTime dt)
        {
            string StrSql = null;
            bool Holiday = false;
            

            StrSql = "Select Date,HolidayName as Description from EmpHoliday where date= '" + dt.ToString("yyyy/MM/dd") + "' AND  CompanyID='" + SessionValue("CompanyID").ToString() + "'";
            Holiday = RecordExistPage(StrSql);
            return Holiday;


        }
        protected void C1_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
        {

            string strMonth = null;
            string onmouseoverStyle = "this.style.backgroundColor='#D4EDFF'";
            string onmouseoutStyle = "this.style.backgroundColor='@BackColor'";
            string rowBackColor = string.Empty;
            //"this.style.backgroundColor='#fdf5e6'"  'String.Empty
            bool notTouched = false;
            Label lblHoliday = new Label();
            StringBuilder temp = new StringBuilder();
            //Add Holiday in to the calendar
            System.Data.DataTable dt = new System.Data.DataTable();
            System.Data.DataRow dr = default(System.Data.DataRow);
            System.Data.DataSet dsHoliday = new System.Data.DataSet();


            Label lbl = new Label();
            System.Data.DataTable dt1 = new System.Data.DataTable();
            System.Data.DataRow dr1 = default(System.Data.DataRow);
            dt1 = (DataTable)ViewState["dt"];
            //dsScedule.Tables(0)
            string strColor = null;

            foreach (DataRow row in dt1.Rows)
            {

                if (e.Day.Date == Convert.ToDateTime(row[0]))
                {
                    strColor = GetColor(row);


                    //If CStr(dr1.Item("LeaveType")) = "Maternity Leave" Then
                    //    temp = New StringBuilder
                    //    temp.Append("<br>")
                    //    temp.Append("<span style=""font-family:Arial; font-weight:bold;font-size:8px; color:")
                    //    temp.Append(strColor)
                    //    temp.Append(""">")
                    //    temp.Append(CStr(dr1.Item("Name")))
                    //    temp.Append("</span>")
                    //    e.Cell.Controls.Add(New LiteralControl(temp.ToString()))
                    if (e.Day.IsWeekend == false)
                    {
                        temp = new StringBuilder();
                        temp.Append("<br>");
                        temp.Append("<span style=\"font-family:Verdana; font-weight:bold;font-size:10px; color:");
                        temp.Append(strColor);
                        temp.Append("\">");
                        temp.Append(row["Name"].ToString());
                        temp.Append("</span>");
                        e.Cell.Controls.Add(new LiteralControl(temp.ToString()));
                    }
                }

            }

            //Add Holiday
            dsHoliday = (System.Data.DataSet)Session["DsHoliday"];
            dt = dsHoliday.Tables[0];
            notTouched = true;
            foreach (DataRow row in dt.Rows)
            {
                if (e.Day.Date == Convert.ToDateTime(row[0]))
                {
                    if (e.Day.IsWeekend == false)
                    {
                        strMonth = "";
                        temp = new StringBuilder();
                        //temp.Append("<br>")
                        temp.Append("<span style=\"font-family:Arial; font-weight:bold;font-size:12px; color:");
                        temp.Append("White");
                        temp.Append("\">");
                        //temp.Append("""><br>")
                        temp.Append(strMonth);
                        temp.Append("<br>");
                        temp.Append(Convert.ToString(row[1]));
                        temp.Append("</span>");
                        e.Cell.Controls.Clear();
                        Label lblDay = new Label();
                        lblDay.Text = e.Day.Date.Day.ToString();
                        lblDay.Font.Bold = true;
                        lblDay.Font.Underline = true;

                        e.Cell.Controls.Add(lblDay);

                        e.Cell.ToolTip = Convert.ToString(row[1]);
                        e.Cell.Controls.Add(new LiteralControl(temp.ToString()));
                        e.Cell.BackColor = System.Drawing.Color.DodgerBlue;
                        e.Cell.ForeColor = System.Drawing.Color.White;
                        //System.Drawing.ColorTranslator.FromHtml("#24618E")
                        notTouched = false;
                    }

                }
            }
        }
        private string GetColor(System.Data.DataRow dt)
        {

            string Status = "";
            string StrColor = "";
            string AppType = "";
            string DayType = "";
            bool AppCancel = false;

            Status = dt["Status"].ToString();
            AppType = dt["LeaveType"].ToString();
            DayType = dt["DayType"].ToString();
            AppCancel = (bool)dt["AppCancel"];
            if (AppCancel == true)
            {
                StrColor = "SlateGray";
                return StrColor;
            }
            //If application status is pending then goes here

            if (Status == "Pending Approval")
            {

                StrColor = "Maroon";
                return StrColor;

            }

            //If Status = "Application Approved" Then
            if (AppType == "Annual Leave" | AppType == "Emergency Leave" | AppType == "Others Leave" | AppType == "Special Leave")
            {
                if (DayType == "Full Day")
                {
                    StrColor = "Red";
                    return StrColor;
                }
                else if (DayType == "First Half")
                {
                    StrColor = "SaddleBrown";
                    return StrColor;
                }
                else
                {
                    StrColor = "DarkCyan";
                    return StrColor;
                }
            }
            else if (AppType == "Medical Leave")
            {
                StrColor = "Orange";
                return StrColor;
            }
            else if (AppType == "Hospitalization Leave")
            {
                StrColor = "SteelBlue";
                return StrColor;
            }
            else if (AppType == "UnPaid Leave")
            {
                StrColor = "Black";
                return StrColor;
            }
            else if (AppType == "Compassionate Leave")
            {
                StrColor = "MidnightBlue";
                return StrColor;
            }
            else if (AppType == "Maternity Leave")
            {
                StrColor = "LightGreen";
                return StrColor;
            }
            else if (AppType == "Paternity Leave")
            {
                StrColor = "SkyBlue";
                return StrColor;
            }
            else if (AppType == "Congratulatory Leave")
            {
                StrColor = "Green";
                return StrColor;
            }
            else if (AppType == "Study Leave")
            {
                StrColor = "DarkGoldenrod";
                return StrColor;
            }
            else if (AppType == "Examination Leave")
            {
                StrColor = "DarkKhaki";
                return StrColor;
            }
            else
            {
                StrColor = "Purple";
            }
            //End If

            //If Status = "Application Rejected" Then

            //End If

            return ("");

        }

        private DataSet CreatDsPage(string StrSql)
        {
            DataSet ds = default(DataSet);
            SqlDataAdapter da = default(SqlDataAdapter);
            SqlConnection ConnPage = default(SqlConnection);
            ConnPage = new SqlConnection();
            ConnPage.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            ConnPage.Open();
            da = new SqlDataAdapter(StrSql, ConnPage);
            ds = new DataSet();
            try
            {
                da.Fill(ds);
                ConnPage.Close();
                ConnPage.Dispose();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                ConnPage.Close();
                ConnPage.Dispose();
            }

            return ds;

        }
        private void ExecuteSQLPage(string StrSql)
        {
            SqlCommand myCommand = default(SqlCommand);
            SqlConnection ConnPage = default(SqlConnection);
            ConnPage = new SqlConnection();

            ConnPage.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            ConnPage.Open();

            try
            {
                myCommand = new SqlCommand(StrSql, ConnPage);
                myCommand.CommandType = System.Data.CommandType.Text;
                myCommand.CommandText = StrSql;
                myCommand.ExecuteNonQuery();
                myCommand.Dispose();
                ConnPage.Close();
                ConnPage.Dispose();
            }
            catch (Exception ex)
            {


            }
            finally
            {
                ConnPage.Close();
                ConnPage.Dispose();

            }
        }
        private bool RecordExistPage(string StrSql)
        {
            SqlConnection ConnPage = default(SqlConnection);
            SqlCommand SqlComm = default(SqlCommand);
            SqlDataReader DataReader = default(SqlDataReader);
            ConnPage = new SqlConnection();
            ConnPage.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            ConnPage.Open();

            //Open the Database
            SqlComm = new SqlCommand(StrSql, ConnPage);
            //Execute the reader, Read data from Resoursfoodmeal
            try
            {
                DataReader = SqlComm.ExecuteReader();
                if (DataReader.Read())
                {
                    DataReader.Close();
                    return true;
                }
                else
                {
                    DataReader.Close();
                    return false;
                }
            }
            catch (Exception ex)
            {
                SqlComm.Dispose();

            }
            finally
            {
                SqlComm.Dispose();
                ConnPage.Close();
                ConnPage.Dispose();
            }
            return false;

        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }





    }
}