<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="tip.aspx.cs" Inherits="LTS_CRM.tips.tip" %>
<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div>
 <div class="well">
<div id="ribbon">
	<ol class="breadcrumb">
		<li>
			Management</li>
	</ol>
</div>

<section id="Section1">
	<div class="row">
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
				<header>
					<h2>
						Task List</h2>
				</header>
				<div>
					<div class="jarviswidget-editbox">
						&nbsp;</div>
					<div class="widget-body no-padding">
						<div class="widget-body-toolbar">
							<p class="bg-color-blueDark txt-color-white active">
								&nbsp;</p>
							<div class="tab-content">
								<div class="tab-pane active" id="tab1_pan" runat="server">
									<asp:Repeater ID="rptSupplierTipsDetail" runat="server" ClientIDMode="AutoID" EnableTheming="False">
<HeaderTemplate>
<table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
 <thead>
  <tr>

<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="ID" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Companyid" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Suppliertypeid" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Tips" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Country" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Countryid" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Airportpickup" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Airportdropoff" />
</th>
<th class="hasinput" style="width: 5%">
<input type="text" class="form-control" placeholder="Wholeday" />
</th>

<th class="hasinput" style="width: 10%"></th>
</tr>
<tr>
<th >ID</th>
<th >CompanyID</th>
<th >SupplierTypeId</th>
<th >Tips</th>
<th data-hide="phone">Country</th>
<th data-hide="phone">CountryId</th>
<th data-hide="phone">AirportPickUp</th>
<th data-hide="phone">AirportDropOff</th>
<th data-hide="phone">WholeDay</th>
<th>Action</th>
</tr>
</thead>
<tbody>
</HeaderTemplate>
<ItemTemplate>
<tr>
<td><%# Container.ItemIndex + 1 %></td>
<td><%# DataBinder.Eval(Container.DataItem, "CompanyID")%></td>
<td><%# DataBinder.Eval(Container.DataItem, "SupplierTypeId")%></td>
<td><%# DataBinder.Eval(Container.DataItem, "Tips")%></td>
<td><%# DataBinder.Eval(Container.DataItem, "Country")%></td>
<td><%# DataBinder.Eval(Container.DataItem, "CountryId")%></td>
<td><%# DataBinder.Eval(Container.DataItem, "AirportPickUp")%></td>
<td><%# DataBinder.Eval(Container.DataItem, "AirportDropOff")%></td>
<td><%# DataBinder.Eval(Container.DataItem, "WholeDay")%></td>
 <td>
<asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true">
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
<asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
</td>
</tr>
</ItemTemplate>
<FooterTemplate>
</tbody>
</table>
</FooterTemplate>
</asp:Repeater>
</div>
								<div class="tab-pane" id="tab2_pan" runat="server">
									&nbsp;</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<p>
	&nbsp;</p>

 <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
<div class="modal-dialog"> 
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal" >Close</a>
                </div>
            </div>
        </div>
    </div>
 </div>
</div>
</asp:Content>
