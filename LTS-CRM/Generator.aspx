﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Generator.aspx.cs" Inherits="LTS_CRM.Generator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/bootstrap.min.css") %>" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/font-awesome.min.css") %>" />

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/smartadmin-production-plugins.min.css") %>" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/smartadmin-production.min.css") %>" />
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/smartadmin-skins.min.css") %>" />

    <!-- SmartAdmin RTL Support  -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/smartadmin-rtl.min.css") %>" />

    <!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="~/css/your_style.css"> -->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/your_style.css") %>" />
    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp-->
    <link rel="stylesheet" type="text/css" media="screen" href="<%= ResolveUrl("~/css/demo.min.css") %>" />

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<%= ResolveUrl("~/img/favicon/favicon.ico") %>" type="image/x-icon" />
    <link rel="icon" href="<%= ResolveUrl("~/img/favicon/favicon.ico") %>" type="image/x-icon" />

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700" />

    <!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="../img/splash/sptouch-icon-iphone.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="../img/splash/touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="../img/splash/touch-icon-iphone-retina.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="../img/splash/touch-icon-ipad-retina.png" />

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="<%= ResolveUrl("~/img/splash/ipad-landscape.png") %>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
    <link rel="apple-touch-startup-image" href="<%= ResolveUrl("~/img/splash/ipad-portrait.png") %>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
    <link rel="apple-touch-startup-image" href="<%= ResolveUrl("~/img/splash/iphone.png") %>" media="screen and (max-device-width: 320px)" />

</head>
<body class="fixed-navigation  fixed-header  fixed-ribbon  pace-done smart-style-1">
    <form   runat="server" id="smartformregister" novalidate="novalidate">
        <div>

            <div class="well">

                <section id="widget-grid" class="">
                    <div class="row">
                        <article class="col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 sortable-grid ui-sortable">
                            <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                                <header>
                                    <span class="widget-icon"><i class="fa fa-list"></i></span>
                                    <h2>Page Generator </h2>
                                </header>
                                <div>
                                    <div class="jarviswidget-editbox">
                                    </div>
                                    <div class="widget-body no-padding">
                                        <div class="widget-body-toolbar">
                                            <div class="smart-form">
                                                <fieldset> <section>
                                                        <label class="input">
                                                              <asp:Literal ID="lblmsg" runat="server"></asp:Literal>

                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <asp:DropDownList ID="txtTableName" runat="server"></asp:DropDownList>
                                                            <b class="tooltip tooltip-bottom-right">Writ the Database Table name</b>

                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <asp:TextBox ID="txtFolderName" placeholder="Enter Folder name to create Page under it" runat="server"></asp:TextBox>
                                                            <b class="tooltip tooltip-bottom-right">Required to Create Folder </b>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <asp:TextBox ID="txtPageName" runat="server" placeholder="Enter Page Name to create"></asp:TextBox>
                                                            <b class="tooltip tooltip-bottom-right">Needed to enter the page name</b>
                                                        </label>
                                                    </section>
                                                    <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <asp:TextBox ID="txtInheritClassName" placeholder="Enter Inherit Project Name" ReadOnly="true" Text="LTS_CRM" runat="server"></asp:TextBox>
                                                            <b class="tooltip tooltip-bottom-right">Needed to enter the page inherit Name</b>
                                                        </label>
                                                    </section>

                                                    <%-- <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <asp:TextBox ID="txtInheritClassName_TableName" Text="Support" runat="server"></asp:TextBox>
                                                            <b class="tooltip tooltip-bottom-right">Needed to enter the page inherit Table Name</b>
                                                        </label>
                                                    </section>--%>
                                                    <section>
                                                        <label class="checkbox">
                                                            <input type="checkbox" name="subscription" id="chkIsMasterPage" runat="server" /> 
                                                            <i></i>I want to set master page, Write master page path to below if this is true..</label>
                                                    </section>

                                                    <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <asp:TextBox Text="~/master/MasterAdmin.Master" runat="server" ID="txtMasterPage"></asp:TextBox>
                                                            <b class="tooltip tooltip-bottom-right">Needed to enter the page inherit Table Name</b>
                                                        </label>
                                                    </section>
                                                   <%-- <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-envelope-o"></i>  
                                                            <input type="checkbox" name="chkIsHeader" id="chkIsHeader" runat="server" />
                                                            <b class="tooltip tooltip-bottom-right">Needed to check Master page allowed to write?</b>
                                                        </label>
                                                    </section>--%>
                                                    <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-envelope-o"></i>
                                                            <CKEditor:CKEditorControl ID="ckeHeaderRibbon" ValidationGroup="addclient" BasePath="~/ckeditor" runat="server" Height="100" Width="600" ToolbarStartupExpanded="False">
                                                            </CKEditor:CKEditorControl>
                                                            <b class="tooltip tooltip-bottom-right">Needed to enter header</b>
                                                        </label>
                                                    </section>

                                                    <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-lock"></i>
                                                            <CKEditor:CKEditorControl ID="ckeSectionStart" ValidationGroup="addclient" BasePath="~/ckeditor" runat="server" Height="100" Width="600" ToolbarStartupExpanded="False">
                                                            </CKEditor:CKEditorControl>
                                                            <b class="tooltip tooltip-bottom-right">Don't forget to change Body content update</b>
                                                        </label>
                                                    </section>

                                                   <%-- <section>
                                                        <label class="input">
                                                            <i class="icon-append fa fa-lock"></i>
                                                            <CKEditor:CKEditorControl ID="ckeSectionEnd" ValidationGroup="addclient" BasePath="~/ckeditor" runat="server" Height="100" Width="600" ToolbarStartupExpanded="False">
                                                            </CKEditor:CKEditorControl>
                                                            <b class="tooltip tooltip-bottom-right">Don't forget to add footer!!</b>
                                                        </label>
                                                    </section>--%>


                                                </fieldset>

                                                <footer>
                                                    <asp:Button ID="btnCreateGrid" OnClick="btnCreate_Click" class="btn btn-primary" runat="server" Text="Create Grid Page"></asp:Button>

                                                    <asp:Button ID="btnCreateForm" OnClick="btnCreate_Click" CommandArgument="form" CssClass="btn btn-primary" runat="server" Text="Create Form Page"></asp:Button>
                                                </footer>

                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </section>
            </div>

        </div>
    </form>
</body>
</html>
