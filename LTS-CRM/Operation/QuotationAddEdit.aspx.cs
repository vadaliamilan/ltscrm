﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LTS_CRM.Operation
{
    public partial class QuotationAddEdit : CRMPage
    {
        #region page common events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //TravelSession.Current.QuotationId = 1;

                LoadcboCompanyID(cboCompanyID);

                LoadCategoryFromGeneralTableByValue(ddlQuotationStatus, "QuotationStatus", "Select Status");

                //Bind staff
                BindStaff();

                //Bind sales
                BindSales();

                //Bind Inquery Staff
                BindInqueryStaff();

                BindCompanyORIndividual();

            }


            if (!IsPostBack)
            {
                isReadOnly = TravelSession.Current.IsReadonlyQuotation;

                btnQuotationPriceSave.Visible = !isReadOnly;
                btnOperationQuotationSave.Visible = !isReadOnly;

                //Load Quotation Staff dropdown binding
                LoadEmployee(ddlQuotationPriceStaff);

                //set usercontrol property after SetTabIndex
                BindQuotationForm();

                //Bind Quotation Price after Quotation Form
                BindQuotationPriceDetails();

                BindDocumentGrid();

                //Bind the usercontorl events
                ucBasicHotel.checksubTabs += ucBasicHotel_checksubTabs;

                pnlStatus.Visible = TravelSession.Current.QuotationId.HasValue;

                //Bind iternary
                ddlIternary.DataSource = BLItineraryTemplate.GetItineraryTemplateList();
                ddlIternary.DataTextField = "SubjectTitle";
                ddlIternary.DataValueField = "ID";
                ddlIternary.DataBind();
                ddlIternary.AppendDataBoundItems = true;
                ddlIternary.Items.Insert(0, new ListItem("Select Iternary", "0"));
                ddlIternary.SelectedIndex = 0;
            }

            ucBasicEntry.SaveBasicDelegate += ucBasicEntry_SaveBasicDelegate;
            //set tabing and again bind the forms
            setTabIndex();

        }
        public bool isReadOnly
        {
            get
            {
                if (ViewState["isReadonly"] != null)
                {
                    return Convert.ToBoolean(ViewState["isReadonly"]);
                }
                else
                    return false;
            }
            set
            {
                ViewState["isReadonly"] = value;
            }
        }
        public int NoOfServiceDays
        {
            get
            {

                if (String.IsNullOrEmpty(Convert.ToString(ViewState["noOfService"])))
                    return 0;
                else return Convert.ToInt32(ViewState["noOfService"]);
            }
            set
            {
                ViewState["noOfService"] = value;
            }
        }
        bool ucBasicHotel_checksubTabs(object sender)
        {
            SetQuotationPriceTabs();
            return true;
        }

        /// <summary>
        /// Re Bind Transaction tables like hotel, vendor, meal etc.
        /// </summary>
        /// <param name="sender"></param>
        void ucBasicEntry_SaveBasicDelegate(object sender, bool isUpdated)
        {
            //if not updated means saved as new entry then enabled the tabs
            //if (txtNoOfServiceDay.Text.Trim() != "")
            //    NoOfServiceDays = Convert.ToInt32(txtNoOfServiceDay.Text.Trim());

            SetQuotationPriceTabs();
            if (isUpdated)
            {
                ReUpdateAllThe_Transcations();

            }
        }

        private void ReUpdateAllThe_Transcations()
        {
            ucBasicHotel.BindHotelPriceDetails();
            ucCoach.BindCoachDetails();
            ucDinner.BindDinner();
            ucDriverGuide.BindDriverAccomodation();
            ucEnterance.BindEnterance();
            ucLunch.BindLunch();
            ucTips.BindTips();
            ucOthers.BindOtherservices();
            ucTourGuide.BindTourGuide();
            ucVanHire.BindVanHireDetails();
        }
        #endregion

        #region Bind Controls
        private void BindQuotationPriceDetails()
        {

            if (TravelSession.Current.QuotationPriceId.HasValue)
            {
                DLQuotationPrice obj = BLOperationQuotations.GetQuotationPriceDetailsById(TravelSession.Current.QuotationPriceId.Value);
                txtQP_TourCode.Text = obj.TourCode.Value;
                //txtQuotationStaff.Text = obj.QuotationStaff.Value;

                txtQP_NoOfPeople.Text = obj.NoOfPeople.Value.ToString();
                txtQP_Confirmation.Text = obj.Confirmation.Value;
                txtQP_Currency.Text = obj.QuotationCurrency.Value;
                if (!obj.MarkupPercentage.IsNull)
                    txtQP_MarkupPercentage.Text = obj.MarkupPercentage.Value.ToString();

                if (ddlQuotationPriceStaff.Items.Count > 0)
                {
                    if (ddlQuotationPriceStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)) != null)
                    {
                        ddlQuotationPriceStaff.ClearSelection();
                        ddlQuotationPriceStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)).Selected = true;
                    }
                }
                // check basic details saved or not. if yes then get the basic id
                // the setting of basic id to seesion hasbeen done on SetTabing
                //if(TravelSession.Current.QuotationPriceBasicId.HasValue)
                //{
                //    //here to set differet user controls

                //    ////Bind Transaction
                //    //ucBasicHotel.quotationPriceId = TravelSession.Current.QuotationPriceId.Value;
                //    //ucBasicHotel.QuotationType = QuotationBasicDeatils.QuotationPrice_Types.Hotel;
                //    //ucBasicHotel.quotationPriceBasicId = TravelSession.Current.QuotationPriceBasicId.Value;

                //}


                // For Hotel price Quotation 
                // set markup and currency override and set to the usercontorls.
                //String.Format("{0:.##}", txtQP_MarkupPercentage.Text)//string
                if (txtQP_MarkupPercentage.Text.Trim() != "")
                    ucBasicHotel.MarkUpPercentage = Convert.ToDecimal(txtQP_MarkupPercentage.Text);
                if (txtQP_Currency.Text.Trim() != "")
                    ucBasicHotel.QuotationCurrency = txtQP_Currency.Text;
                //pass total people
                ucBasicHotel.NoOfPeople = txtQP_NoOfPeople.Text.Trim() != "" ? Convert.ToInt32(txtQP_NoOfPeople.Text.Trim()) : 0;
                //Same thing will saved for other user controls IDs.

                //set override the currency and markup from Main tab to Basic control
                ucBasicEntry.quotationCurrency = txtQP_Currency.Text;
                ucBasicEntry.markupPercentage = txtQP_MarkupPercentage.Text;

                //set override details to OtherService,Enterance and other Usercontrol objects
                ucCoach.QuotationCurrency
                    = ucDinner.QuotationCurrency
                    = ucDriverGuide.QuotationCurrency
                    = ucEnterance.QuotationCurrency
                    = ucLunch.QuotationCurrency
                    = ucTips.QuotationCurrency
                    = ucEnterance.QuotationCurrency
                    = ucOthers.QuotationCurrency
                     = ucTourGuide.QuotationCurrency
                     = ucVanHire.QuotationCurrency
                     = ucOverView.QuotationCurrency
                    = ucBasicHotel.QuotationCurrency;

                ucCoach.MarkUpPercentage
                = ucDinner.MarkUpPercentage
                = ucDriverGuide.MarkUpPercentage
                = ucEnterance.MarkUpPercentage
                = ucLunch.MarkUpPercentage
                = ucTips.MarkUpPercentage
                = ucEnterance.MarkUpPercentage
                = ucOthers.MarkUpPercentage
                 = ucTourGuide.MarkUpPercentage
                 = ucVanHire.MarkUpPercentage
                = ucOverView.MarkUpPercentage
                 = ucBasicHotel.MarkUpPercentage;


                ucCoach.NoOfPeople
                = ucDinner.NoOfPeople
                = ucDriverGuide.NoOfPeople
                = ucEnterance.NoOfPeople
                = ucLunch.NoOfPeople
                = ucTips.NoOfPeople
                = ucEnterance.NoOfPeople
                = ucOthers.NoOfPeople
                = ucTourGuide.NoOfPeople
                = ucVanHire.NoOfPeople
                     = ucOverView.NoOfPeople
                = ucBasicHotel.NoOfPeople;

                ucCoach.IsReadOnly
                = ucDinner.IsReadOnly
                = ucDriverGuide.IsReadOnly
                = ucEnterance.IsReadOnly
                = ucLunch.IsReadOnly
                = ucTips.IsReadOnly
                = ucEnterance.IsReadOnly
                = ucOthers.IsReadOnly
                 = ucTourGuide.IsReadOnly
                 = ucVanHire.IsReadOnly
                = ucOverView.IsReadOnly
                 = ucBasicHotel.IsReadOnly
                = isReadOnly;

                ucBasicEntry.IsReadOnly = isReadOnly;
                btnQuotationPriceSave.Visible = !isReadOnly;
            }
        }

        private void BindQuotationForm()
        {
            if (TravelSession.Current.QuotationId.HasValue)
            {
                LTSCRM_DL.DLOperationQuotation obj = BLL.BLOperationQuotations.SelectQuotationById(TravelSession.Current.QuotationId.Value);

                //Bind form controls here.
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                txtTourCode.Text = obj.TourCode.Value;

                if (ddlQuotationStaff.Items.Count > 0 && !obj.QuotationStaff.IsNull)
                {
                    if (ddlQuotationStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)) != null)
                    {
                        ddlQuotationStaff.ClearSelection();
                        ddlQuotationStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)).Selected = true;
                    }
                }
                if (ddlStaff.Items.Count > 0 && !obj.StaffId.IsNull)
                {
                    if (ddlStaff.Items.FindByValue(Convert.ToString(obj.StaffId.Value)) != null)
                    {
                        ddlStaff.ClearSelection();
                        ddlStaff.Items.FindByValue(Convert.ToString(obj.StaffId.Value)).Selected = true;
                    }
                }
                if (ddlInquiryStaffId.Items.Count > 0 && !obj.InquiryStaffId.IsNull)
                {
                    if (ddlInquiryStaffId.Items.FindByValue(Convert.ToString(obj.InquiryStaffId.Value)) != null)
                    {
                        ddlInquiryStaffId.ClearSelection();
                        ddlInquiryStaffId.Items.FindByValue(Convert.ToString(obj.InquiryStaffId.Value)).Selected = true;
                    }
                }
                if (ddlCompanyIndividual.Items.Count > 0 && !obj.CompanyIndividual.IsNull)
                {
                    if (ddlCompanyIndividual.Items.FindByValue(Convert.ToString(obj.CompanyIndividual.Value)) != null)
                    {
                        ddlCompanyIndividual.ClearSelection();
                        ddlCompanyIndividual.Items.FindByValue(Convert.ToString(obj.CompanyIndividual.Value)).Selected = true;
                    }
                }
                txtClient_City.Text = obj.Client_City.Value;
                ddlClient_Country.Value = obj.Client_Country.Value;
                txtClient_Telephone.Text = obj.Client_Telephone.Value;
                txtClient_MobileNumber.Text = obj.Client_MobileNumber.Value;
                txtClient_Fax.Text = obj.Client_Fax.Value;
                txtClient_WeChat.Text = obj.Client_WeChat.Value;
                txtClient_Email.Text = obj.Client_Email.Value;
                if (ddlSalesId.Items.Count > 0 && !obj.SalesId.IsNull)
                {
                    if (ddlSalesId.Items.FindByValue(Convert.ToString(obj.SalesId.Value)) != null)
                    {
                        ddlSalesId.ClearSelection();
                        ddlSalesId.Items.FindByValue(Convert.ToString(obj.SalesId.Value)).Selected = true;
                    }
                }
                //txtQuotationStaff.Text = obj.QuotationStaff.Value;
                //txtDepartmentID.Text = obj.DepartmentID.Value;
                if (ddlDepartmentID.Items.Count > 0 && !obj.DepartmentID.IsNull)
                {
                    if (ddlDepartmentID.Items.FindByValue(Convert.ToString(obj.DepartmentID.Value)) != null)
                    {
                        ddlDepartmentID.ClearSelection();
                        ddlDepartmentID.Items.FindByValue(Convert.ToString(obj.DepartmentID.Value)).Selected = true;
                    }
                }
                txtOffice_City.Text = obj.Office_City.Value;
                //txtOffice_Country.Text = obj.Office_Country.Value;
                txtOffice_Telephone.Text = obj.Office_Telephone.Value;
                txtOffice_MobileNumber.Text = obj.Office_MobileNumber.Value;
                txtOffice_Fax.Text = obj.Office_Fax.Value;
                txtOffice_WeChat.Text = obj.Office_WeChat.Value;
                txtOffice_Email.Text = obj.Office_Email.Value;
                if (!obj.TourStartDate.IsNull && IsDate(Convert.ToString(obj.TourStartDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.TourStartDate.Value));
                    txtTourStartDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.TourEndDate.IsNull && IsDate(Convert.ToString(obj.TourEndDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.TourEndDate.Value));
                    txtTourEndDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtNoOfServiceDay.Text = obj.NoOfServiceDay.Value.ToString();
                NoOfServiceDays = obj.NoOfServiceDay.Value;
                //txtTotalClient.Text = obj.TotalClient.Value.ToString();
                txtNoOfAdults.Text = obj.NoOfAdults.IsNull ? "" : obj.NoOfAdults.Value.ToString();
                txtNoOfChild5To12.Text = obj.NoOfChild5To12.IsNull ? "" : obj.NoOfChild5To12.Value.ToString();
                txtNoOfChild2To5.Text = obj.NoOfChild2To5.IsNull ? "" : obj.NoOfChild2To5.Value.ToString();
                txtNoOfInfant.Text = obj.NoOfInfant1To2.IsNull ? "" : obj.NoOfInfant1To2.Value.ToString();
                txtComments.Text = obj.Comments.Value;
                txtMessageToClient.Text = obj.MessageToClient.Value;
                txtQuotationReport.Text = obj.QuotationReport.Value;

                if (ddlQuotationStatus.Items.Count > 0 && !obj.QuotationStatus.IsNull)
                {
                    if (ddlQuotationStatus.Items.FindByValue(Convert.ToString(obj.QuotationStatus.Value)) != null)
                    {
                        ddlQuotationStatus.ClearSelection();
                        ddlQuotationStatus.Items.FindByValue(Convert.ToString(obj.QuotationStatus.Value)).Selected = true;
                    }
                }

                //Set the quotation value to Quoation Price 
                OverrideQuotationValueToQuotationPrice(obj);

            }
        }

        private void OverrideQuotationValueToQuotationPrice(DLOperationQuotation obj)
        {
            //LTSCRM_DL.DLOperationQuotation obj = BLL.BLOperationQuotations.SelectQuotationById(TravelSession.Current.QuotationId.Value);
            txtQP_TourCode.Text = obj.TourCode.Value;
            txtTourCode.Text = obj.TourCode.Value;

            if (ddlQuotationStaff.Items.Count > 0 && !obj.QuotationStaff.IsNull)
            {
                if (ddlQuotationStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)) != null)
                {
                    ddlQuotationStaff.ClearSelection();
                    ddlQuotationStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)).Selected = true;
                }
            }

            if (!obj.QuotationDate.IsNull && IsDate(Convert.ToString(obj.QuotationDate.Value)))
            {
                DateTime dt = new DateTime();
                dt = Convert.ToDateTime(Convert.ToString(obj.QuotationDate.Value));
                txtQuotationPriceDate.Text = dt.ToString("dd'/'MM'/'yyyy");
            }

            txtQP_NoOfPeople.Text = Convert.ToString(obj.TotalClient.Value);
            //txtQP_Confirmation.Text = "";
            //txtQP_Currency.Text = "";
            //txtQP_MarkupPercentage.Text = "";

            if (ddlQuotationPriceStaff.Items.Count > 0 && !obj.QuotationStaff.IsNull)
            {
                if (ddlQuotationPriceStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)) != null)
                {
                    ddlQuotationPriceStaff.ClearSelection();
                    ddlQuotationPriceStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)).Selected = true;
                }
            }
            else
            {
                //bind new dropdown list
                LoadEmployee(ddlQuotationPriceStaff);
                if (ddlQuotationPriceStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)) != null)
                {
                    ddlQuotationPriceStaff.ClearSelection();
                    ddlQuotationPriceStaff.Items.FindByValue(Convert.ToString(obj.QuotationStaff.Value)).Selected = true;
                }
            }
            ucBasicEntry.quotationCurrency = txtQP_Currency.Text;
            ucBasicEntry.markupPercentage = txtQP_MarkupPercentage.Text;
            ucBasicHotel.CompanyId = obj.CompanyID.IsNull ? 0 : Convert.ToInt32(obj.CompanyID.Value);

            ucCoach.CompanyId
           = ucDinner.CompanyId
           = ucDriverGuide.CompanyId
           = ucEnterance.CompanyId
           = ucLunch.CompanyId
           = ucTips.CompanyId
           = ucOthers.CompanyId
           = ucTourGuide.CompanyId
           = ucVanHire.CompanyId
           = ucBasicHotel.CompanyId;
            //txtConfirmationStatus.Text = obj.ConfirmationStatus.Value;
        }

        /// <summary>
        /// Set department id of sales
        /// </summary>
        private void BindSales()
        {
            LoadEmployee(ddlSalesId, department: 4);
        }

        //Clients
        private void BindInqueryStaff()
        {
            LoadEmployee(ddlInquiryStaffId);
        }

        //Bind company/individual
        private void BindCompanyORIndividual()
        {
            LoadCompanyIndividual(ddlCompanyIndividual);
        }

        private void BindStaff()
        {
            LoadEmployee(ddlQuotationStaff);

            LoadEmployee(ddlStaff);
        }

        private void BindDepartment()
        {
            LoadDepartments(ddlDepartmentID);
        }

        private void setTabIndex(bool isMainActive = false)
        {
            if (TravelSession.Current.QuotationId.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.QuotationId.Value)))
            {
                tab2.Visible = true;
                tab2_pan.Visible = tab2.Visible;
                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;
                tab4.Visible = tab2.Visible;
                tab4_pan.Visible = tab2.Visible;
                tab5.Visible = tab2.Visible;
                tab5_pan.Visible = tab2.Visible;
            }
            else
            {
                tab2.Visible = false;
                tab2_pan.Visible = tab2.Visible;
                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;
                tab4.Visible = tab2.Visible;
                tab4_pan.Visible = tab2.Visible;
                tab5.Visible = tab2.Visible;
                tab5_pan.Visible = tab2.Visible;
            }

            string selectedTab = "";
            if (Request.Cookies["activetab"] != null)
            {
                selectedTab = Request.Cookies["activetab"].Value;
            }


            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");
            tab3.Attributes.Remove("class");
            tab4.Attributes.Remove("class");
            tab5.Attributes.Remove("class");

            tab1_pan.Attributes.Remove("class");
            tab2_pan.Attributes.Remove("class");
            tab3_pan.Attributes.Remove("class");
            tab4_pan.Attributes.Remove("class");
            tab5_pan.Attributes.Remove("class");

            tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab5.Attributes.Add("class", "bg-color-blueDark txt-color-white ");

            tab1_pan.Attributes.Add("class", "tab-pane ");
            tab2_pan.Attributes.Add("class", "tab-pane ");
            tab3_pan.Attributes.Add("class", "tab-pane ");
            tab4_pan.Attributes.Add("class", "tab-pane ");
            tab5_pan.Attributes.Add("class", "tab-pane ");

            switch (selectedTab)
            {
                case "#tab2_pan":
                    {
                        if (!tab2.Visible)
                            goto default;

                        tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab2_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab3_pan":
                    {
                        if (!tab3.Visible)
                            goto default;
                        tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab3_pan.Attributes.Add("class", "tab-pane active");

                        //Sub quotation tabs
                        SetQuotationPriceTabs(isMainActive);
                        break;
                    }
                case "#tab4_pan":
                    {
                        if (!tab4.Visible)
                            goto default;
                        tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab4_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab5_pan":
                    {
                        if (!tab5.Visible)
                            goto default;
                        tab5.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab5_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                default:
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                    tab1_pan.Attributes.Add("class", "tab-pane active");
                    // check if first time Save quotation and In Edit mode then Enable the First sub tab of Quotaiton Price tab
                    if (TravelSession.Current.QuotationId.HasValue)
                        SetQuotationPriceTabs(true);
                    break;
            }
            //Sub quotation tabs
            if (isMainActive && tab3.Visible)
                SetQuotationPriceTabs(true);
        }

        private void SetQuotationPriceTabs(bool isMainActive = false)
        {
            int i = 1;

            if (txtNoOfServiceDay.Text.Trim() != "")
                NoOfServiceDays = Convert.ToInt32(txtNoOfServiceDay.Text.Trim());

            string selectedSubTab = "";
            if (Request.Cookies["activesubtab"] != null)
            {
                selectedSubTab = Request.Cookies["activesubtab"].Value;
            }
            if (isMainActive || selectedSubTab == "")
            {
                subtab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                subtab1_pan.Attributes.Add("class", "tab-pane active");
                i = 2;
                selectedSubTab = "#subtab1_pan";
            }
           
            bool isVisible = false;
            if (selectedSubTab != "" && TravelSession.Current.QuotationPriceId.HasValue)
            {
                #region sub tab selection

                // Get quotation Basic ID
                //int quotationBasicId = BLL.BLOperationQuotations.GetQuotationPriceBasicIDFromPriceId(TravelSession.Current.QuotationPriceId.Value);
                //if (quotationBasicId != 0)
                //{
                //    TravelSession.Current.QuotationPriceBasicId = quotationBasicId;
                //}


                //Visible Main & Basic Details
                DataTable Quotationdata = new DataTable();
                Dictionary<int, int> dic = new Dictionary<int, int>();
                if (NoOfServiceDays > 0)
                {
                    dic = BLL.BLOperationQuotations.GetQuotationBasicDetailsPair(TravelSession.Current.QuotationPriceId.Value, NoOfServiceDays, ref Quotationdata);
                }
                //if (TravelSession.Current.QuotationPriceBasicId.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.QuotationPriceBasicId.Value)))
                if (dic.Count() > 0)
                {
                    //set if other sub tab required to visible to save or update.
                    // if sub id entry will not found then it will not visible. because it required basicID (PK) for transaction tables
                    isVisible = true;
                }

                #endregion
            }

            // Here by default sub tabs will be hidden. only two tabs ( Main and Basic details will be visible default )

            foreach (Control c in tab3_pan.Controls)
            {
                HtmlGenericControl tab = null;
                HtmlGenericControl tab_pan;
                if (c.FindControl("subtab" + i.ToString()) != null)
                {
                    tab = c.FindControl("subtab" + i.ToString()) as HtmlGenericControl;
                    //tab.Attributes.Remove("class");
                    tab.Attributes["class"] = "bg-color-blueDark txt-color-white ";
                    if (i > 2)
                        tab.Visible = isVisible;
                }
                if (c.FindControl("subtab" + i.ToString() + "_pan") != null)
                {
                    tab_pan = c.FindControl("subtab" + i.ToString() + "_pan") as HtmlGenericControl;
                    //tab_pan.Attributes.Remove("class");
                    tab_pan.Attributes["class"] = "tab-pane ";
                    if (selectedSubTab == "#" + tab_pan.ClientID)
                    {
                        if (tab != null)
                            tab.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab_pan.Attributes.Add("class", "tab-pane active");
                    }
                    if (i > 2)
                        tab_pan.Visible = isVisible;
                }
                i++;
            }

        }
        #endregion

        #region Operation Quotation - First Tab
        protected void btnOperationQuotationSave_ServerClick(object sender, EventArgs e)
        {
            DLOperationQuotation obj = new DLOperationQuotation();
            obj.TourCode = txtTourCode.Text;
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);

            obj.QuotationDate = DateTime.Now;

            if (isNonZeroAndBlank(ddlStaff))
            {
                obj.StaffId = Convert.ToInt32(ddlStaff.SelectedValue);
            }
            if (isNonZeroAndBlank(ddlInquiryStaffId))
            {
                obj.InquiryStaffId = Convert.ToInt32(ddlInquiryStaffId.SelectedValue);
            }
            if (isNonZeroAndBlank(ddlCompanyIndividual))
            {
                obj.CompanyIndividual = Convert.ToInt32(ddlCompanyIndividual.SelectedValue);
            }

            obj.Client_City = txtClient_City.Text.Trim();
            obj.Client_Country = ddlClient_Country.Value;
            obj.Client_Telephone = txtClient_Telephone.Text;
            obj.Client_MobileNumber = txtClient_MobileNumber.Text;
            obj.Client_Fax = txtClient_Fax.Text;
            obj.Client_WeChat = txtClient_WeChat.Text;
            obj.Client_Email = txtClient_Email.Text;

            if (isNonZeroAndBlank(ddlSalesId))
            {
                obj.SalesId = Convert.ToInt32(ddlSalesId.SelectedValue);
            }
            if (isNonZeroAndBlank(ddlQuotationStaff))
            {
                obj.QuotationStaff = (ddlQuotationStaff.SelectedItem.Value);
            }
            if (isNonZeroAndBlank(ddlDepartmentID))
            {
                obj.DepartmentID = Convert.ToInt32(ddlDepartmentID.SelectedItem.Value);
            }
            obj.Office_City = txtOffice_City.Text;
            obj.Office_Telephone = txtOffice_Telephone.Text;
            obj.Office_MobileNumber = txtOffice_MobileNumber.Text;
            obj.Office_Fax = txtOffice_Fax.Text;
            obj.Office_WeChat = txtOffice_WeChat.Text;
            obj.Office_Email = txtOffice_Email.Text;
            string resTourStartDate = txtTourStartDate.Text.Trim();
            if (resTourStartDate != "")
            {
                obj.TourStartDate = DateTime.ParseExact(resTourStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            string resTourEndDate = txtTourEndDate.Text.Trim();
            if (resTourEndDate != "")
            {
                obj.TourEndDate = DateTime.ParseExact(resTourEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }


            if (txtNoOfServiceDay.Text.Trim() != "")
            {
                obj.NoOfServiceDay = Convert.ToInt32(txtNoOfServiceDay.Text);
            }

            if (txtNoOfAdults.Text.Trim().Length > 0)
                obj.NoOfAdults = Convert.ToInt32(txtNoOfAdults.Text);
            if (txtNoOfChild5To12.Text.Trim().Length > 0)
                obj.NoOfChild5To12 = Convert.ToInt32(txtNoOfChild5To12.Text);
            if (txtNoOfChild2To5.Text.Trim().Length > 0)
                obj.NoOfChild2To5 = Convert.ToInt32(txtNoOfChild2To5.Text);
            if (txtNoOfInfant.Text.Trim().Length > 0)
                obj.NoOfInfant1To2 = Convert.ToInt32(txtNoOfInfant.Text);

            //obj.TotalClient = txtTotalClient.Text;

            obj.TotalClient
                = (obj.NoOfAdults.IsNull ? 0 : obj.NoOfAdults.Value)
                + (obj.NoOfChild5To12.IsNull ? 0 : obj.NoOfChild5To12)
                + (obj.NoOfChild2To5.IsNull ? 0 : obj.NoOfChild2To5)
                + (obj.NoOfInfant1To2.IsNull ? 0 : obj.NoOfInfant1To2)
                ;

            obj.Comments = txtComments.Text;
            obj.MessageToClient = txtMessageToClient.Text;
            obj.QuotationReport = txtQuotationReport.Text;

            if (!TravelSession.Current.QuotationId.HasValue)
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                //obj.QuotationStatus = QuotationStatuses.New.ToString();
                BLL.BLOperationQuotations.SaveQuotation(obj);
                TravelSession.Current.QuotationId = obj.ID.Value;

                //clear Sub tab cookie selection
                ClearTabCookies(SubTabCookieName);
            }
            else
            {
                obj.ID = TravelSession.Current.QuotationId.Value;
                if (ddlQuotationStatus.SelectedValue != "" && ddlQuotationStatus.Visible)
                    obj.QuotationStatus = ddlQuotationStatus.SelectedValue.ToString();
                 

                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                BLL.BLOperationQuotations.UpdateQuotation(obj);
            }

            //To set the value from quotaiton to Quotation price basic controls
            OverrideQuotationValueToQuotationPrice(obj);

            //first time set Request.Cookies["activesubtab"] to active first tab 
            //Request.Cookies["activesubtab"].Value = "#subtab1_pan";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "rgi", "javascript:createCookie('activesubtab', '#subtab1_pan', 100);", true);
            setTabIndex(true);
            // No need to clear value.
            //ClearFormDetailsQuotation();
        }

        private void ClearFormDetailsQuotation()
        {
            txtTourCode.Text = "";
            ddlStaff.ClearSelection();
            ddlInquiryStaffId.ClearSelection();
            ddlCompanyIndividual.ClearSelection();
            txtClient_City.Text = "";
            txtClient_Telephone.Text = "";
            txtClient_MobileNumber.Text = "";
            txtClient_Fax.Text = "";
            txtClient_WeChat.Text = "";
            txtClient_Email.Text = "";
            ddlSalesId.ClearSelection();
            ddlQuotationStaff.ClearSelection();
            ddlDepartmentID.ClearSelection();
            txtOffice_City.Text = "";
            txtOffice_Telephone.Text = "";
            txtOffice_MobileNumber.Text = "";
            txtOffice_Fax.Text = "";
            txtOffice_WeChat.Text = "";
            txtOffice_Email.Text = "";
            txtTourStartDate.Text = "";
            txtTourEndDate.Text = "";
            txtNoOfServiceDay.Text = "";
            // txtTotalClient.Text = "";
            txtNoOfAdults.Text = "";
            txtNoOfChild5To12.Text = "";
            txtNoOfChild2To5.Text = "";
            txtNoOfInfant.Text = "";
            txtComments.Text = "";
            txtMessageToClient.Text = "";
            ddlQuotationStatus.ClearSelection();
            txtQuotationReport.Text = "";
        }

        #endregion

        #region Operation Price Quotation Tab
        protected void btnQuotationPriceSave_ServerClick(object sender, EventArgs e)
        {
            if (TravelSession.Current.QuotationId.HasValue)
            {
                DLQuotationPrice obj = new DLQuotationPrice();
                obj.QuotationId = TravelSession.Current.QuotationId.Value;
                obj.TourCode = txtTourCode.Text;
                obj.QuotationStaff = Convert.ToInt32(ddlQuotationPriceStaff.SelectedItem.Value);

                obj.QuotationDate = DateTime.Now;

                obj.NoOfPeople = Convert.ToInt32(txtQP_NoOfPeople.Text);
                obj.Confirmation = txtQP_Confirmation.Text;
                obj.QuotationCurrency = txtQP_Currency.Text;
                if (txtQP_MarkupPercentage.Text.Trim().Length > 0)
                    obj.MarkupPercentage = Convert.ToDecimal(txtQP_MarkupPercentage.Text);
                //obj.ConfirmationStatus = QuotationStatuses.New.ToString();
                if (TravelSession.Current.QuotationPriceId.HasValue)
                {
                    obj.ID = TravelSession.Current.QuotationPriceId.Value;
                    obj.UpdatedBy = TravelSession.Current.UserName;
                    obj.UpdatedDate = DateTime.Now;
                    BLL.BLOperationQuotations.UpdateQuotationPrice(obj);
                    MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());

                }
                else if (!TravelSession.Current.QuotationPriceId.HasValue)
                {
                    obj.CreatedBy = TravelSession.Current.UserName;
                    obj.CreatedDate = DateTime.Now;
                    BLL.BLOperationQuotations.SaveQuotationPrice(obj);
                    TravelSession.Current.QuotationPriceId = obj.ID.Value;
                    MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                }

                //set quotation currency and markup on save
                ucBasicEntry.quotationCurrency = txtQP_Currency.Text;
                ucBasicEntry.markupPercentage = txtQP_MarkupPercentage.Text;
                ucBasicEntry.CallFromParent = true;
                //set controls with no. of people updates
                ucBasicEntry.BindQuotationPriceMain();
                ucBasicEntry.clearControls();
                ucBasicEntry.CreateBasicDetails();
                ucBasicEntry.CallFromParent = false;

                if (!obj.NoOfPeople.IsNull)
                {
                    ucBasicHotel.NoOfPeople = obj.NoOfPeople.Value;
                }
                ucBasicHotel.QuotationCurrency = ucBasicEntry.quotationCurrency;
                if (ucBasicEntry.markupPercentage != "")
                {
                    ucBasicHotel.MarkUpPercentage = ucBasicEntry.markupPercentage != "" ? Convert.ToDecimal(ucBasicEntry.markupPercentage) : 0;
                }
                ucBasicEntry.BindQuotationPriceMain();

                //ucBasicHotel.BindHotelPriceDetails();

                //first time set the override currency, no.of people and markup
                ucCoach.QuotationCurrency
                = ucDinner.QuotationCurrency
                = ucDriverGuide.QuotationCurrency
                = ucEnterance.QuotationCurrency
                = ucLunch.QuotationCurrency
                = ucTips.QuotationCurrency
                = ucOthers.QuotationCurrency
                = ucTourGuide.QuotationCurrency
                = ucVanHire.QuotationCurrency
                = ucBasicHotel.QuotationCurrency;

                ucCoach.MarkUpPercentage
                = ucDinner.MarkUpPercentage
                = ucDriverGuide.MarkUpPercentage
                = ucEnterance.MarkUpPercentage
                = ucLunch.MarkUpPercentage
                = ucTips.MarkUpPercentage
                = ucOthers.MarkUpPercentage
                = ucTourGuide.MarkUpPercentage
                = ucVanHire.MarkUpPercentage
                = ucBasicHotel.MarkUpPercentage;

                ucCoach.NoOfPeople
                = ucDinner.NoOfPeople
                = ucDriverGuide.NoOfPeople
                = ucEnterance.NoOfPeople
                = ucLunch.NoOfPeople
                = ucTips.NoOfPeople
                = ucOthers.NoOfPeople
                = ucTourGuide.NoOfPeople
                = ucVanHire.NoOfPeople
                = ucBasicHotel.NoOfPeople;

                ucCoach.CompanyId
              = ucDinner.CompanyId
              = ucDriverGuide.CompanyId
              = ucEnterance.CompanyId
              = ucLunch.CompanyId
              = ucTips.CompanyId
              = ucOthers.CompanyId
              = ucTourGuide.CompanyId
              = ucVanHire.CompanyId
              = ucBasicHotel.CompanyId;
            }
            else
            {
                MessageBox(Messages.SomethingWrong.GetDiscription(), MessageType.warning, MessageType.warning.ToString());
            }

            SetQuotationPriceTabs();
        }
        protected void lnkParent_Click(object sender, EventArgs e)
        {
            ucOverView.BindOverView();
        }

        #endregion

        #region Document Quotation
        protected void btnDocDelete_Click(object sender, EventArgs e)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            string id = (sender as LinkButton).CommandArgument;
            String sql = "delete from [dbo].[OperationQuotationDocuments] where [ID] = @ID";

            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@ID", id);
            Command.ExecuteNonQuery();
            BindDocumentGrid();
        }

        private void BindDocumentGrid()
        {
            lnkQuotationUpload.Visible = !isReadOnly;
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            String sql = " SELECT ROW_NUMBER() OVER (ORDER BY  ID) AS No,  DocumentName,ID FROM [dbo].[OperationQuotationDocuments]  ";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = sql;

            MyRepeater.DataSource = Command.ExecuteReader();
            MyRepeater.DataBind();

            Connection.Close();
        }
        protected void btnDocEdit_Click(object sender, EventArgs e)
        {
            string ID = (sender as LinkButton).CommandArgument.ToString();

            DataTable file = GetAFile(Convert.ToInt16(ID));
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["FileType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }

        public static DataTable GetAFile(int id)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            DataTable file = new DataTable();
            Connection.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Connection;
            cmd.CommandTimeout = 0;

            cmd.CommandText = "SELECT ID, FileName, FileType, FileData FROM OperationQuotationDocuments "
                + "WHERE ID=@ID";
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();

            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters["@ID"].Value = id;

            adapter.SelectCommand = cmd;
            adapter.Fill(file);

            Connection.Close();

            return file;
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }

        protected void lnkQuotationUpload_ServerClick(object sender, EventArgs e)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            SqlCommand Command;

            //Declare string InsertSQL 
            //String InsertSQL = @"Insert into [dbo].[EmpDocument] ([EmployeeID], [DocumentName],  [FileType], [FileName], [FileData], [CreatedBy], [CreatedDate] ) VALUES ( @EmployeeID, @DocumentName, @FileType, @FileName, @FileData, @CreatedBy, @CreatedDate) ";
            String InsertSQL = @"INSERT INTO [dbo].[OperationQuotationDocuments]
                                   ([OperationQuotationId]
                                   ,[DocumentName]
                                   ,[FileType]
                                   ,[FileName]
                                   ,[FileData]
                                   ,[CreatedBy]
                                   ,[CreatedDate]
                                   ,[UpdatedBy]
                                   ,[UpdatedDate])
                             VALUES (@OperationQuotationId, @DocumentName, @FileType, @FileName, @FileData, @CreatedBy, @CreatedDate, @UpdatedBy, @UpdatedDate)";

            if (TravelSession.Current.QuotationId.HasValue)
            {
                foreach (HttpPostedFile postedFile in flDocumentUpload.PostedFiles)
                {
                    //To create a PostedFile

                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                    Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

                    HttpPostedFile File = postedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    Command.Parameters.AddWithValue("@OperationQuotationId", TravelSession.Current.QuotationId.Value);
                    Command.Parameters.AddWithValue("@DocumentName", name);

                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteNonQuery();

                }
            }
            Connection.Close();
            BindDocumentGrid();
        }

        #endregion

        #region Quotation Iternary

        protected void ddlIternary_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((DropDownList)sender).SelectedIndex > 0 && ((DropDownList)sender).SelectedValue != "0")
            {
                DropDownList ddl = ((DropDownList)sender);
                int id = Convert.ToInt32(ddl.SelectedValue);
                DLItineraryTemplate obj = BLItineraryTemplate.GetItineraryTemplateById(id);
                quotationiternary.Text = obj.ItineraryTemplate.Value;
            }
        } 
        #endregion

    }
}