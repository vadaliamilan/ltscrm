﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="Quotation.aspx.cs" Inherits="LTS_CRM.Operation.Quotation" %>

<%@ Register Src="~/Operation/QuotationBasicDeatils.ascx" TagName="Basic" TagPrefix="uc" %>
<%@ Register Src="~/Operation/QuotationBasicEntry.ascx" TagName="BasicTable" TagPrefix="uc" %>
<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Quotation Management</li>
                </ol>
            </div>
             <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Quotation List</h2>
                            </header>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding1">
                                    <div class="widget-body-toolbar">
                                        <div class="row">
                                            <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                <div class="input-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                                <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="lnkAddNew" OnClick="lnkAddNew_Click" runat="server"><i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span></asp:LinkButton>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="QuotationRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                            <HeaderTemplate>
                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="No" />
                                                            </th>
                                                            <th class="hasinput" style="width: 20%">
                                                                <input type="text" class="form-control" placeholder="Tour Code" />
                                                            </th>
                                                            <th class="hasinput" style="width: 10%">
                                                                <input type="text" class="form-control" placeholder="Tour Start Date" />
                                                            </th>
                                                            <th class="hasinput" style="width: 10%">
                                                                <input type="text" class="form-control" placeholder="Tour End Date" />
                                                            </th>
                                                            <th class="hasinput" style="width: 10%">
                                                                <input type="text" class="form-control" placeholder="No. of Days" />
                                                            </th>
                                                            <th class="hasinput" style="width: 10%">
                                                                <input type="text" class="form-control" placeholder="Total Clients" />
                                                            </th>
                                                            <th class="hasinput" style="width:17%"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No</th>
                                                            <th data-hide="phone">Tour Code</th>
                                                            <th data-hide="phone">Start Date</th>
                                                            <th data-hide="phone">End Date</th>
                                                            <th data-hide="phone">No. Of Days</th>
                                                            <th data-hide="phone">Total Clients</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCode")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "TourStartDate", "{0:d'/'M'/'yyyy hh:mm:ss tt}") %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "TourEndDate", "{0:d'/'M'/'yyyy hh:mm:ss tt}") %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "NoOfServiceDay")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "TotalClient")%></td>
                                                    <td>
                                                         <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="btnView" runat="server" Text="Click" OnClick="btnView_Click" CommandArgument='<%#Eval("ID")%>' Visible='<%# Convert.ToString(Eval("QuotationStatus")).ToLower() == "confirmation" ? true:false %>'>
<i class="fa fa-eye"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                        
                                                        <asp:LinkButton  class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" Text="Click" OnClick="buttonEdit_Click" CommandArgument='<%#Eval("ID")%>' Visible='<%# Convert.ToString(Eval("QuotationStatus")).ToLower() != "confirmation" ? true:false %>' >
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" OnClick="buttonDelete_Click" CommandArgument='<%#Eval("ID")%>' Visible='<%# Convert.ToString(Eval("QuotationStatus")).ToLower() != "confirmation" ? true:false %>' OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonConfirm" runat="server" Text="Click" OnClick="buttonConfirm_Click" ToolTip="Click to Confirm"  CommandArgument='<%#Eval("ID")%>' Visible='<%# Convert.ToString(Eval("QuotationStatus")).ToLower() != "confirmation" ? true:false %>' OnClientClick="if ( !confirm('Are you sure you want to move to Quotation Confirmation?')) return false;">
<i class="fa fa-check-circle" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>      
            </table>                      
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
</asp:Content>
