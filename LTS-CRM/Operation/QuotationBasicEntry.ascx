﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="true" ClientIDMode="AutoID" CodeBehind="QuotationBasicEntry.ascx.cs" Inherits="LTS_CRM.Operation.QuotationBasicEntry" %>
<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="smart-form ">
        <br />
        <asp:PlaceHolder ID="pnlBasicGridEntry" runat="server"  ></asp:PlaceHolder>
        <footer>
            <fieldset>
                <div class="row">
                    <section class="col col-3">
                        <label class="label">
                            Quotation Currency
                        </label>
                        <label class="input">
                            <asp:TextBox ID="txtucQuotationCurrency" runat="server" placeholder="Currency" Text="" class="form-control"></asp:TextBox>
                        </label>
                    </section>
                    <section class="col col-3">
                        <label class="label">
                            Markup Percentage
                        </label>
                        <label class="input">
                            <asp:TextBox ID="txtucQP_MarkupPercentage" runat="server" placeholder="Markup Percentage" Text="" class="form-control"></asp:TextBox>
                            <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtMarkupPercentage"  ControlToValidate="txtucQP_MarkupPercentage" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                        </label>
                    </section>
                </div>
            </fieldset> 
            <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonSaveBasicQuote" runat="server" Text="Save" OnClick="buttonSaveBasicQuote_Click" Visible="true">
            </asp:LinkButton>
           <%-- <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonClearBasicQuote" runat="server" Text="Cancel" OnClick="buttonClearBasicQuote_Click" Visible="true"></asp:LinkButton>--%>
        </footer>
    </div>
</article>
