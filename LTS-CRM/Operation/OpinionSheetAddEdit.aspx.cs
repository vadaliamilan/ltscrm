﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LTSCRM_DL.BLL;
using LTS_CRM.Helper;
using LTS_CRM.BLL;

namespace LTS_CRM.Operation
{
    public partial class OpinionSheetAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadOpinionCategory(ddlCategory);
                BindFormDetails();
            }
        }
        private void LoadOpinionCategory(DropDownList ddl)
        {
            ddlCategory.DataTextField = "Category";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataSource = BLOpinionSheetCategory.GetOpinionSheetCategory();
            ddlCategory.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem() { Selected = true, Text = "Select Category", Value = string.Empty });
        }
        private void BindFormDetails()
        {
            if (TravelSession.Operation.OpinionSheetId.HasValue)
            {
                DLOpinionSheet obj = new DLOpinionSheet();
                obj = BLOpinionSheet.GetOpinionSheetById(TravelSession.Operation.OpinionSheetId.Value);
                if (!obj.Date.IsNull && IsDate(Convert.ToString(obj.Date.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.Date.Value));
                    txtDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }

                if (ddlCategory.Items.FindByValue(Convert.ToString(obj.OpinionSheetCategoryId.Value)) != null)
                {
                    ddlCategory.ClearSelection();
                    ddlCategory.Items.FindByValue(Convert.ToString(obj.OpinionSheetCategoryId.Value)).Selected = true;
                }
                txtStaff.Text = obj.Staff.Value;
            }
        }
        private void SaveDetails()
        {
            DLOpinionSheet obj = new DLOpinionSheet();
            string resDate = txtDate.Text.Trim();
            if (resDate != "")
            {
                obj.Date = DateTime.ParseExact(resDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.Staff = txtStaff.Text;
            obj.OpinionSheetCategoryId = Convert.ToInt32(ddlCategory.SelectedValue);
            if (flUpload.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = flUpload.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.FileData = fileData;
            }
            if (TravelSession.Operation.OpinionSheetId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Operation.OpinionSheetId.Value;
                BLOpinionSheet.UpdateOpinionSheet(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLOpinionSheet.SaveOpinionSheet(obj);
            }

            ClearFormDetails();
            Response.Redirect("OpinionSheet.aspx", true);

        }

        private void ClearFormDetails()
        {
            txtDate.Text = "";
            txtStaff.Text = "";
            ddlCategory.ClearSelection();
        }

        protected void btnOpinionSheetSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void Cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("OpinionSheet.aspx", true);
        }
    }
}