﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Operation
{
    public partial class LeadManagement : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            rptLeadManagement.DataSource = BLOperationLead.GetLeadList();
            rptLeadManagement.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Sales.LeadId = null;
            Response.Redirect("~/Operation/LeadManagementAddEdit.aspx", true);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Sales.LeadId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Operation/LeadManagementAddEdit.aspx", true);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLOperationLead.DeleteLead(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();
        }
    }
}