﻿<%@ Control Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="QuotationBasicDeatils.ascx.cs" Inherits="LTS_CRM.Operation.QuotationBasicDeatils" %>
<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="quotbasic">
        <div>
            <div class="jarviswidget-editbox">
                &nbsp;
            </div>
            <div class="widget-body no-padding">
                <div class="widget-body-toolbar">
                    <div class="row">
                        <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                            <div class="input-group">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <%--Bhalani Abhishek: Started--%>

                    <asp:Repeater ID="QuotationBasicGrid" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                        <HeaderTemplate>
                            <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th class="hasinput" style="width: 20%">
                                            <input type="text" class="form-control" placeholder="No. Of Days" />
                                        </th>
                                        <th class="hasinput" style="width: 10%">
                                            <input type="text" class="form-control" placeholder="Date" />
                                        </th>
                                        <th class="hasinput" style="width: 10%">
                                            <input type="text" class="form-control" placeholder="City" />
                                        </th>
                                    </tr>
                                    <tr>
                                        <th data-class="expand">No. Of Days</th>
                                        <th data-hide="phone">Date</th>
                                        <th data-hide="phone">City</th>
                                    </tr>
                                </thead>

                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                <td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%></td>
                                <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>      
                            </table>                      
                        </FooterTemplate>
                    </asp:Repeater>

                    <%--HOTEL Repater--%>
                    <asp:Panel ID="pnlHotelPrice" runat="server" Visible="false">
                        <asp:Repeater ID="QuotationPrice_Hotel" OnItemDataBound="QuotationPrice_Hotel_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 20%">
                                                <input type="text" class="form-control" placeholder="No. Of Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <%--Hotel--%>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th data-hide="phone">Date</th>
                                            <th data-hide="phone">City</th>
                                            <%--Hotel--%>
                                            <th data-hide="phone">Star Hotel</th>
                                            <th data-hide="phone">Recommend Hotel</th>
                                            <th data-hide="phone">Twin/Person</th>
                                            <th data-hide="phone">No Of People</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %></td>
                                    <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                    <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />

                                    <%--<td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%></td>--%>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:DropDownList ID="ddlHotel_starHotel" runat="server"></asp:DropDownList>
                                        <asp:HiddenField ID="hdnstarvalue" runat="server" Value='<%# Eval("StartHotel") %>' />
                                        <asp:HiddenField ID="hdnRecommendedHotel" runat="server" Value='<%# Eval("RecommendedHotel") %>' />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlHotel_Reco" runat="server"></asp:DropDownList></td>
                                    <td>
                                        <asp:TextBox ID="txtHotel_TwinPerPerson" runat="server" Value='<%# Eval("TwinPerPerson") %>'></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtHotel_NoOfPeople" runat="server" Value='<%# Eval("NoOfPeople") %>'></asp:TextBox></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                      
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="qbasicDetailForm" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Single Room Supplement Per Person
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtHotelPrice_singleroomPerPerson" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtHotelPrice_NetRateInTota" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtHotelPrice_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate Per Person
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtHotelPrice_GrossRatePerPerson" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtHotelPrice_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtHotelPrice_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-12">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtHotelPrice_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-12">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtHotelPrice_Remark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnHotelPrices" runat="server" Text="Save" class="btn bg-color-blueDark txt-color-white" OnClick="btnHotelPrices_Click" />
                        </footer>
                    </asp:Panel>
                    <%--END--%>

                    <%--Driver and Guide Accom Repater--%>
                    <asp:Panel ID="pnlDriverAndAccomPrice" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_QuotationPrice_DriverAndAccom" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 20%">
                                                <input type="text" class="form-control" placeholder="No. Of Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <%--Driver And Accom--%>
                                            <th></th>
                                            <th></th>

                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th data-hide="phone">Date</th>
                                            <th data-hide="phone">City</th>
                                            <%--Driver And Accom--%>
                                            <th data-hide="phone">Rate/Room</th>
                                            <th data-hide="phone">No of Rooms</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %></td>
                                    <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                    <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />

                                    <%--<td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%></td>--%>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:TextBox ID="txtDriverAndAccom_RateRoom" runat="server" Value='<%# Eval("RatePerRoom") %>'></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txtDriverAndAccom_NoOfRoom" runat="server" Value='<%# Eval("NoOfRoom") %>'></asp:TextBox></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                      
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="DriverAndAccomDetailForm" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtDriverAndAccomPrice_NetRateInTota" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDriverAndAccomPrice_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtDriverAndAccomPrice_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtDriverAndAccomPrice_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                 <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDriverAndAccomPrice_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDriverAndAccomPrice_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-12">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDriverAndAccomPrice_Remark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnDriverAndAccomPrices" runat="server" Text="Save" class="btn bg-color-blueDark txt-color-white" OnClick="btnDriverAndAccomPrices_Click" />
                        </footer>
                    </asp:Panel>

                    <%--Coach Repater--%>
                    <asp:Panel ID="pnlCoachPrice" runat="server" Visible="false">
                        <asp:Repeater ID="QuotationPrice_Coach" OnItemDataBound="QuotationPrice_Coach_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 20%">
                                                <input type="text" class="form-control" placeholder="No. Of Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <%--Coach--%>
                                            <th></th>
                                            <th></th>

                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th data-hide="phone">Date</th>
                                            <th data-hide="phone">City</th>
                                            <%--Coach--%>
                                            <th data-hide="phone">Type of Coach</th>
                                            <th data-hide="phone">Coach Rate/Day</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %></td>
                                    <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                    <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />

                                    <%--<td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%></td>--%>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:DropDownList ID="ddlCoach_CoachType" runat="server"></asp:DropDownList>
                                        <asp:HiddenField ID="hdnCoachType" runat="server" Value='<%# Eval("TypeOfCoach") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCoach_RatePerDay" runat="server" Value='<%# Eval("CoachRatePerDay") %>'></asp:TextBox></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                      
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="CoachDetailform" class="row smart-form" runat="server" >
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtCoach_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtCoach_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtCoach_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtCoach_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                 <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtCoach_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtCoach_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-12">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtCoach_Remark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnCoachPrices" runat="server" Text="Save" class="btn bg-color-blueDark txt-color-white" OnClick="btnCoachPrices_Click" />
                        </footer>
                    </asp:Panel>

                     
                    <!-- Bhalani Abhishek: Started-->

                    <%--VAN HIRE--%>
                    <asp:Panel ID="pnl_VanHire" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_VanHire" OnItemDataBound="rpt_VanHire_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 150px">
                                                <input type="text" class="form-control" placeholder="Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th>Date</th>
                                            <th>City</th>
                                            <th>Type Of Vehicle</th>
                                            <th>Vehicale Rate/Day</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %>

                                   <%-- <td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%>--%>
                                        <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />
                                    </td>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:DropDownList ID="ddlVanHire_TypeOfVehicle" runat="server" ></asp:DropDownList>
                                        <asp:HiddenField ID="hdnTypeOfVehicle" runat="server" Value='<%# Eval("TypeOfVehicle") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtVanHire_VehicleRatePerDay" runat="server" Value='<%# Eval("VehicleRatePerDay") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                  
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="dvVanHire" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtVanHire_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtVanHire_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtVanHire_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtVanHire_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtVanHire_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtVanHire_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtVanHire_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnVanHireSave" runat="server" OnClick="btnVanHireSave_Click" Text="Save" class="btn bg-color-blueDark txt-color-white" />
                        </footer>
                    </asp:Panel>

                    <%--Tour Guide--%>
                    <asp:Panel ID="pnl_TourGuide" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_TourGuide" OnItemDataBound="rpt_TourGuide_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 150px">
                                                <input type="text" class="form-control" placeholder="Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th>Date</th>
                                            <th>City</th>
                                            <th>Type Of Guide</th>
                                            <th>Rate/Day</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %>                                    
                                     <%--<td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%>--%>
                                        <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />
                                    </td>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:DropDownList ID="ddlTourGuide_TypeOfGuide" runat="server" ></asp:DropDownList>
                                        <asp:HiddenField ID="hdnTypeofGuide" runat="server" Value='<%# Eval("TypeOfGuide") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTypeOfGuide_RatePerDay" runat="server" Value='<%# Eval("RatePerDay") %>'></asp:TextBox>
                                    </td>
                                
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                  
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="dvTour" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtTourGuide_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTourGuide_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtTourGuide_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtTourGuide_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTourGuide_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTourGuide_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTourGuide_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnTourGuideSave" runat="server" OnClick="btnTourGuideSave_Click" Text="Save" class="btn bg-color-blueDark txt-color-white" />
                        </footer>
                    </asp:Panel>

                    <%--Lunch--%>
                    <asp:Panel ID="pnl_Lunch" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_Lunch" OnItemDataBound="rpt_Lunch_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 150px">
                                                <input type="text" class="form-control" placeholder="Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th>Date</th>
                                            <th>City</th>
                                            <th>Type Of Meal</th>
                                            <th>Meal/Person</th>
                                            <th>No.Of People</th>
                                            <th>Meal Supplement For Guid</th>
                                            <th>No. Of Tour Guide People</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %> 
                                   <%-- <td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%>--%>
                                        <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />
                                    </td>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:DropDownList ID="ddlLunch_TypeOfMeal" runat="server" ></asp:DropDownList>
                                        <asp:HiddenField ID="hdnTypeofMeal" runat="server" Value='<%# Eval("TypeOfMeal") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLunch_MealPerPerson" runat="server" Value='<%# Eval("MealPerPerson") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLunch_NoOfPeople" runat="server" Value='<%# Eval("NoOfPeople") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLunch_MealSupplementForGuid" runat="server" Value='<%# Eval("MealSupplementForGuid") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtLunch_NoOfTourGuidePeople" runat="server" Value='<%# Eval("NoOfTourGuidePeople") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                  
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="dvLunch" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtLunch_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtLunch_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtLunch_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtLunch_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtLunch_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtLunch_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtLunch_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnLunchSave" runat="server" OnClick="btnLunchSave_Click" Text="Save" class="btn bg-color-blueDark txt-color-white" />
                        </footer>
                    </asp:Panel>

                    <%--DINNER--%>
                    <asp:Panel ID="pnl_Dinner" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_Dinner" OnItemDataBound="rpt_Dinner_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 150px">
                                                <input type="text" class="form-control" placeholder="Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th>Date</th>
                                            <th>City</th>
                                            <th>Type Of Meal</th>
                                            <th>Meal/Person</th>
                                            <th>No.Of People</th>
                                            <th>Meal Supplement For Guid</th>
                                            <th>No. Of Tour Guide People</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %> 
                                   <%-- <td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%>--%>
                                        <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />
                                    </td>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:TextBox ID="txtDinner_TypeOfMeal" runat="server" Value='<%# Eval("TypeOfMeal") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDinner_MealPerPerson" runat="server" Value='<%# Eval("MealPerPerson") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDinner_NoOfPeople" runat="server" Value='<%# Eval("NoOfPeople") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDinner_MealSupplementForGuid" runat="server" Value='<%# Eval("MealSupplementForGuid") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDinner_NoOfTourGuidePeople" runat="server" Value='<%# Eval("NoOfTourGuidePeople") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                  
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="dvDinner" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtDinner_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDinner_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtDinner_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtDinner_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDinner_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDinner_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtDinner_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnDinnerSave" runat="server" OnClick="btnDinnerSave_Click" Text="Save" class="btn bg-color-blueDark txt-color-white" />
                        </footer>
                    </asp:Panel>

                    <%--Tips--%>
                    <asp:Panel ID="pnl_Tips" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_Tips" OnItemDataBound="rpt_Tips_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 150px">
                                                <input type="text" class="form-control" placeholder="Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th>Date</th>
                                            <th>City</th>
                                            <th>Tips Of the day</th>
                                            <th>No. Of People</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex + 1 %> 
                                   <%-- <td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%>--%>
                                        <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />
                                    </td>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:TextBox ID="txtTips_TipsOftheDay" runat="server" Value='<%# Eval("TipsPerDay") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTips_NoOfPeople" runat="server" Value='<%# Eval("NoOfPeople") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                  
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="dvTips" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtTips_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTips_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtTips_GrossrateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtTips_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTips_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTips_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtTips_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnTipsSave" runat="server" OnClick="btnTipsSave_Click" Text="Save" class="btn bg-color-blueDark txt-color-white" />
                        </footer>
                    </asp:Panel>


                    <%--Enterance--%>
                    <asp:Panel ID="pnl_Enterance" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_Enterance" OnItemDataBound="rpt_Enterance_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 150px">
                                                <input type="text" class="form-control" placeholder="Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th data-hide="phone"></th>
                                            <th data-hide="phone"></th>
                                            <th data-hide="phone"></th>
                                            <th data-hide="phone"></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th>Date</th>
                                            <th>City</th>
                                            <th>Entrance Name</th>
                                            <th>Adult Price</th>
                                            <th>No. Of People</th>
                                            <th data-hide="phone">Child Price</th>
                                            <th data-hide="phone">No. Of People</th>
                                            <th data-hide="phone">Concession (student/elderly)</th>
                                            <th data-hide="phone">No. Of People</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                    <td style="width: 150px !important"><%# Container.ItemIndex + 1 %> 
                                    <%--<td style="width: 150px !important"><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%>--%>
                                        <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />
                                    </td>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:TextBox ID="txtEnterance_EnteranceName" runat="server" Value='<%# Eval("EnteranceName") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEnterance_AdultPrice" runat="server" Value='<%# Eval("AdultPrice") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEnterance_NoOfPeopleAdult" runat="server" Value='<%# Eval("NoOfPeople") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEnterance_ChildPrice" runat="server" Value='<%# Eval("ChildPrice") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEnterance_NoOfChild" runat="server" Value='<%# Eval("NoOfChild") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEnterance_ConcessionStudent" runat="server" Value='<%# Eval("Concession_student_elderly") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEnterance_NoOfStudent" runat="server" Value='<%# Eval("NoOfStudentForConcession") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                  
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="Div2" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtEnterance_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtEnterance_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtEnterance_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtEnterance_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtEnterance_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtEnterance_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtEnterance_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnEnteranceSave" runat="server" OnClick="btnEnteranceSave_Click" Text="Save" class="btn bg-color-blueDark txt-color-white" />
                        </footer>
                    </asp:Panel>

                    <%--Other Service--%>
                    <asp:Panel ID="pnlOtherservices" runat="server" Visible="false">
                        <asp:Repeater ID="rpt_OtherServices" OnItemDataBound="rpt_OtherServices_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                            <HeaderTemplate>
                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th class="hasinput" style="width: 20%">
                                                <input type="text" class="form-control" placeholder="No. Of Days" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="Date" />
                                            </th>
                                            <th class="hasinput" style="width: 10%">
                                                <input type="text" class="form-control" placeholder="City" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <th data-class="expand">No. Of Days</th>
                                            <th>Date</th>
                                            <th>City</th>
                                            <th>Other Services</th>
                                            <th data-hide="phone">Price per/person</th>
                                            <th data-hide="phone">No. Of People</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <%--<td><%# Container.ItemIndex + 1 %></td>--%>
                                    <td><%# Container.ItemIndex + 1 %>
                                    <asp:HiddenField ID="hdnPK" runat="server" Value='<%# Eval("Id") %>' />
                                    <asp:HiddenField ID="hdnQuotationBasicId" runat="server" Value='<%# Eval("QuotationBasicId") %>' />
                                   <%-- <td><%# DataBinder.Eval(Container.DataItem, "NoOfDay")%>--%>

                                    </td>
                                    <td><%# String.Format("{0:dd'/'MM'/'yyyy}", DataBinder.Eval(Container.DataItem, "TourDate"))%></td>
                                    <td><%# DataBinder.Eval(Container.DataItem, "TourCity")%></td>
                                    <td>
                                        <asp:TextBox ID="txtOther_OtherServices" runat="server" Value='<%# Eval("OtherServices") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOther_RatePerPerson" runat="server" Value='<%# Eval("PricePerPerson") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOther_NoOfPeople" runat="server" Value='<%# Eval("NoOfPeople") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>      
                            </table>                  
                            </FooterTemplate>
                        </asp:Repeater>

                        <div id="Div1" class="row smart-form" runat="server">
                            <fieldset>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOther_NetRateTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Mark Up Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtOther_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Gross Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOther_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOther_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtOther_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>

                                    <section class="col col-6">
                                        <label class="label">
                                            Notes
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtOther_Notes" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-3">
                                        <label class="label">
                                            Quotation Currency
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtOther_QuotationCurrency" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-3">
                                        <label class="label">
                                            Confirmation Status
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtOther_ConfirmationStatus" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <footer>
                            <asp:Button ID="btnSaveOtherServices" runat="server" OnClick="btnSaveOtherServices_Click" Text="Save" class="btn bg-color-blueDark txt-color-white" />
                        </footer>
                    </asp:Panel>

                    <%--OverView--%>

                    <div id="pnl_OverView" visible="false" class="row smart-form" runat="server">
                        <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">
                                            Net Rate Per person
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOverView_NetRatePerPerson" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">
                                           Net Rate In Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtOverView_NetRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">
                                            Mark up percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOverView_MarkUpPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">
                                            Gross rate per person
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOverView_GrossRatePerPerson" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                      <section class="col col-4">
                                        <label class="label">
                                            Gross rate in Total
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOverView_GrossRateInTotal" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">
                                            Profit
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOverView_Profit" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">
                                            Profit Percentage
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOverView_ProfitPercentage" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">
                                            Single Room Supplement Per Person 
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ReadOnly="true" ID="txtOverView_SingleRoomSupplementPerPerson" runat="server" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-12">
                                        <label class="label">
                                            Remark
                                        </label>
                                        <label class="input">
                                            <asp:TextBox ID="txtOverview_Remark" runat="server" TextMode="MultiLine" Width="300" CssClass="form-control"></asp:TextBox>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                    <%--END--%>

                    <!-- Bhalani Abhishek: End-->

                </div>
            </div>
        </div>
    </div>
</article>
