﻿using LTS_CRM.Helper;
using LTSCRM_DL;
using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TemplateEngine.Docx;
using WordToPDF;
namespace LTS_CRM.Operation
{
    public partial class QuotationSendToClient : LTSUserControl
    {

        public int CompanyId
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_companyId"])))
                {
                    return -1;
                }
                else
                {
                    return Convert.ToInt32(ViewState["_companyId"]);
                }
            }
            set
            {
                ViewState["_companyId"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                new CRMPage().LoadCompanyIndividual(ddlClient, DefaultText: "Select Client");
            }
        }

        private void BindGrid()
        {
            if (TravelSession.Current.QuotationId.HasValue)
            {
                rpt_Quotationsent.DataSource = BLL.BLOperationQuotations.GetQuotationConfirmationListByQID(TravelSession.Current.QuotationId.Value);
                rpt_Quotationsent.DataBind();
            }
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            string ID = (sender as LinkButton).CommandArgument.ToString();
            DLQuotationConfirmationToClient obj = new DLQuotationConfirmationToClient();
            obj.Id = Convert.ToInt32(ID);
            BLL.BLOperationQuotations.DeleteClientConfirmationData(obj);
        }

        protected void ddlClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            var obj = new LTSCRM_DL.DLOperationQuotation();
            obj.ID = Convert.ToInt32(((DropDownList)sender).SelectedValue);
            obj = BLL.BLOperationQuotations.SelectById(obj.ID.Value);
            //txtFirstName.Text = obj.

        }

        public object GenerateTemplate()
        {
            TableContent tbltourcode = new TableContent("tourtitle");
            FieldContent tourcode = new FieldContent("tourcode", "");

            FieldContent tourguideName = new FieldContent("tourguide", "");
            FieldContent coachName = new FieldContent("coach", "");
            FieldContent driverName = new FieldContent("drivername", "");
            FieldContent tourguidePhone = new FieldContent("tourguidePhone", "");
            FieldContent coachTelephone = new FieldContent("coachTelephone", "");
            FieldContent mobile = new FieldContent("mobile", "");
            FieldContent prices = new FieldContent("prices", "");
            FieldContent priceinclude = new FieldContent("priceinclude", "");
            FieldContent pricesexclude = new FieldContent("pricesexclude", "");
            FieldContent remark = new FieldContent("remark", "");
            FieldContent message = new FieldContent("message", "");


            //check quotaitonId has value
            object ToLocation = null;

            if (TravelSession.Current.QuotationId.HasValue)
            {
                DataTable dt = BLL.BLOperationQuotations.SelectQuotationBookDetails(TravelSession.Current.QuotationId.Value);
                // here make for loop to get dates of tours quotation details
                foreach (DataRow dr in dt.Rows)
                {
                    tourcode.Value = dr["tourcode"].ToString();
                    tbltourcode.AddRow(new FieldContent("date1", Convert.ToDateTime(dr["tourdate"]).ToString("dd'/'MM'/'yyyy"))
                                            , new FieldContent("iternary", dr["TourCity"].ToString())
                                            , new FieldContent("hotel", dr["RecommendedHotel"].ToString())
                                            , new FieldContent("meal", dr["TypeOfMeal"].ToString())
                                            , new FieldContent("trans", dr["trans"].ToString())

                                        );
                }
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    tourguideName.Value = dr["tourguideName"].ToString();
                    coachName.Value = dr["coachName"].ToString();
                    driverName.Value = dr["driverName"].ToString();
                    tourguidePhone.Value = dr["tourguidePhone"].ToString();
                    coachTelephone.Value = dr["coachTelephone"].ToString();
                    mobile.Value = dr["mobile"].ToString();
                    prices.Value = dr["prices"].ToString();
                    priceinclude.Value = dr["priceinclude"].ToString();
                    pricesexclude.Value = dr["pricesexclude"].ToString();
                    remark.Value = dr["remark"].ToString();

                }
                message.Value = txtMessageToClient.Text.Trim().ToString();
                if (dt.Rows.Count > 0)
                {
                    var valuesToFill = new TemplateEngine.Docx.Content(tourcode, tbltourcode
                        , tourguideName
                        , coachName
                        , driverName
                        , tourguidePhone
                        , coachTelephone
                        , mobile
                        , prices
                        , priceinclude
                        , pricesexclude
                        , remark
                        , message
                        );
                    string sourcepath = Server.MapPath("~/Operation/") + "confirmation.docx";

                    string exepath = Server.MapPath("~/temp");
                    DirectoryInfo info = new DirectoryInfo(exepath);
                    if (!info.Exists)
                    {
                        info.Create();
                    }

                    string destpath = Server.MapPath("~/temp");
                    if (!Directory.Exists(destpath))
                        Directory.CreateDirectory(destpath);
                    destpath = destpath + "\\confirmation_" + tourcode.Value + ".docx";
                    File.Copy(sourcepath, destpath, true);

                    using (var outputDocument = new TemplateProcessor(destpath).SetRemoveContentControls(true).SetNoticeAboutErrors(true))
                    {
                        outputDocument.FillContent(valuesToFill);
                        outputDocument.SaveChanges();
                    }

                    Word2Pdf objWorPdf = new Word2Pdf();

                    object FromLocation = destpath;
                    string FileExtension = Path.GetExtension(Path.GetFileName(destpath));
                    string ChangeExtension = Path.GetFileName(destpath).Replace(FileExtension, ".pdf");
                    if (FileExtension == ".doc" || FileExtension == ".docx")
                    {
                        ToLocation = Path.GetDirectoryName(destpath) + "\\" + ChangeExtension;
                        objWorPdf.InputLocation = FromLocation;
                        objWorPdf.OutputLocation = ToLocation;
                        objWorPdf.Word2PdfCOnversion();
                    }
                }
            }
            return ToLocation;
        }

        protected void btnQuotationConfirmationToClientSave_ServerClick(object sender, EventArgs e)
        {
            var objTemplate = GenerateTemplate();
            if (TravelSession.Current.QuotationPriceId.HasValue)
            {
                int id = TravelSession.Current.QuotationId.Value;
                DLQuotationConfirmationToClient obj = new DLQuotationConfirmationToClient();
                obj.FirstName = txtFirstName.Text.Trim();
                obj.Email = txtEmail.Text.Trim();
                obj.QuotationId = id;
                obj.SurName = txtSurName.Text.Trim();
                obj.SendDate = DateTime.Now;
                obj.Client = ddlClient.SelectedItem.Text.Trim();

                obj.ClientId = Convert.ToInt32(ddlClient.SelectedValue);

                //Write Send email code snippet to Add status during send and save then after

                // code will be place here.
                MailMessage msg = new MailMessage();

                msg.From = new MailAddress(Convert.ToString(ConfigurationManager.AppSettings["FROM_MAIL"]));
                msg.To.Add(obj.Email.Value);
                msg.Bcc.Add(Convert.ToString(ConfigurationManager.AppSettings["BCC_MAIL"]));
                msg.Subject = Convert.ToString(ConfigurationManager.AppSettings["SUBJECT"]);
                msg.Body = "Hello, Please find attached Tour Quotation prices file";
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Host = Convert.ToString(ConfigurationManager.AppSettings["HOST"]);
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["PORT"]);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["USERNAME"].ToString(), ConfigurationManager.AppSettings["PASSWORD"].ToString());
                client.Timeout = 20000;
                try
                {
                    byte[] binaryFile = System.IO.File.ReadAllBytes(objTemplate.ToString());
                    MemoryStream memoryStream = new MemoryStream(binaryFile);
                    msg.Attachments.Add(new Attachment(memoryStream, "Quotation Template", MediaTypeNames.Application.Pdf));

                    msg.Priority = MailPriority.High;
                    client.Send(msg);
                    MessageBox(Messages.MailSent.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    obj.EmailStatus = "Success";

                }
                catch (Exception ex)
                {
                    obj.EmailStatus = "Fail : Error ::" + ex.Message;
                    //obj.EmailStatus = "Error in sending email to "+ obj.Email
                }
                finally
                {
                    msg.Dispose();
                }
                BLL.BLOperationQuotations.SaveClientConfirmationData(obj);
                DLQuotationConfirmationDocument objDoc = new DLQuotationConfirmationDocument();

                // END EMail sent
                objDoc.ConfirmationID = obj.Id;
                byte[] fileData = System.IO.File.ReadAllBytes(objTemplate.ToString());
                objDoc.DocumentName = Path.GetFileNameWithoutExtension(objTemplate.ToString());
                objDoc.DocumentType = Path.GetExtension(objTemplate.ToString());
                objDoc.IsActive = true;
                objDoc.IsDeleted = false;
                objDoc.DocumentFile = fileData;
                BLL.BLOperationQuotations.SaveClientConfirmationDataFiles(objDoc);
                MessageBox(Messages.MailSent.GetDiscription(), MessageType.success, MessageType.success.ToString());
            }
            BindGrid();
            ClearForm();
        }

        private void ClearForm()
        {
            txtMessageToClient.Text = "";
            txtSurName.Text = "";
            txtFirstName.Text = "";
            txtEmail.Text = "";
        }
    }
}