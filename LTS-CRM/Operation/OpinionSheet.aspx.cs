﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Operation
{
    public partial class OpinionSheet : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            rptOpinionSheet.DataSource = BLOpinionSheet.GetOpinionSheet();
            rptOpinionSheet.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Operation.OpinionSheetId = null;
            Response.Redirect("~/Operation/OpinionSheetAddEdit.aspx", true);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {

            TravelSession.Operation.OpinionSheetId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Operation/OpinionSheetAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLOpinionSheet.DeleteOpinionSheet(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();
        }
    }
}