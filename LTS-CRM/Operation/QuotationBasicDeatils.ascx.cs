﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace LTS_CRM.Operation
{
    public partial class QuotationBasicDeatils : LTSUserControl
    {
        #region Properties
        private QuotationPrice_Types _qtype = QuotationPrice_Types.Basic;

        [System.ComponentModel.Bindable(true)]
        public QuotationPrice_Types QuotationType
        {
            get
            {
                return _qtype;
            }
            set
            {
                _qtype = value;
            }
        }
        public enum QuotationPrice_Types
        {
            Basic = 0,
            Hotel = 1,
            DriverAndGuide = 2,
            Coach,
            VanHire,
            TourGuide,
            Lunch,
            Dinner,
            Tips,
            Entrance,
            OtherService,
            Overview
        }
        public int? quotationPriceId
        {
            get
            {
                return TravelSession.Current.QuotationPriceId.HasValue ? TravelSession.Current.QuotationPriceId.Value : (int?)null;
            }
        }

        //override from quotation price Form and set to viewstate
        public decimal MarkUpPercentage
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_MarkUpPercentage"])))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(ViewState["_MarkUpPercentage"]);
                }
            }
            set
            {
                ViewState["_MarkUpPercentage"] = value;
            }
        }
        public string QuotationCurrency
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_QuotationCurrency"])))
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(ViewState["_QuotationCurrency"]);
                }
            }
            set
            {
                ViewState["_QuotationCurrency"] = value;
            }
        }
        public int CompanyId
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_companyId"])))
                {
                    return -1;
                }
                else
                {
                    return Convert.ToInt32(ViewState["_companyId"]);
                }
            }
            set
            {
                ViewState["_companyId"] = value;
            }
        }
        public int NoOfPeople
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_NoOfPeople"])))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(ViewState["_NoOfPeople"]);
                }
            }
            set
            {
                ViewState["_NoOfPeople"] = value;
            }
        }
        #endregion

        #region Page Events

        //Delegate to call set Tabindexing 
        public delegate bool Reset_Tabing(object sender);
        public delegate void ReBind_OnBasicDetailChanged(object sender);
        public event Reset_Tabing checksubTabs;//To enabled selection of tabs
        public event ReBind_OnBasicDetailChanged ReBindAll;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //for testing
                //quotationId = 1;

                // You must to register Check Sub tab event with usercontorl ID otherwise it will retunr null. Deletgate registered then it will works.
                if (checksubTabs != null)
                    checksubTabs(null);
                if (QuotationType == QuotationPrice_Types.Hotel)
                {
                    btnHotelPrices.Visible = !IsReadOnly;
                    pnlHotelPrice.Visible = true;
                    BindHotelPriceDetails();
                }
                else if (QuotationType == QuotationPrice_Types.VanHire)
                {
                    pnl_VanHire.Visible = true;
                    btnVanHireSave.Visible = !IsReadOnly;
                    BindVanHireDetails();
                }
                else if (QuotationType == QuotationPrice_Types.DriverAndGuide)
                {
                    btnDriverAndAccomPrices.Visible = !IsReadOnly;
                    pnlDriverAndAccomPrice.Visible = true;
                    BindDriverAccomodation();
                }

                else if (QuotationType == QuotationPrice_Types.OtherService)
                {
                    pnlOtherservices.Visible = true;
                    btnSaveOtherServices.Visible = !IsReadOnly;
                    BindOtherservices();
                }
                else if (QuotationType == QuotationPrice_Types.Entrance)
                {
                    btnEnteranceSave.Visible = !IsReadOnly;
                    pnl_Enterance.Visible = true;
                    BindEnterance();
                }
                else if (QuotationType == QuotationPrice_Types.Tips)
                {
                    pnl_Tips.Visible = true;
                    btnTipsSave.Visible = !IsReadOnly;
                    BindTips();
                }
                else if (QuotationType == QuotationPrice_Types.Dinner)
                {
                    pnl_Dinner.Visible = true;
                    btnDinnerSave.Visible = !IsReadOnly;
                    BindDinner();
                }
                else if (QuotationType == QuotationPrice_Types.Lunch)
                {
                    pnl_Lunch.Visible = true;
                    btnLunchSave.Visible = !IsReadOnly;
                    BindLunch();
                }
                else if (QuotationType == QuotationPrice_Types.TourGuide)
                {
                    pnl_TourGuide.Visible = true;
                    btnTourGuideSave.Visible = !IsReadOnly;
                    BindTourGuide();
                } 
                else if (QuotationType == QuotationPrice_Types.Coach)
                {
                    btnCoachPrices.Visible = !IsReadOnly;
                    pnlCoachPrice.Visible = true;
                    BindCoachDetails();
                }
                else if (QuotationType == QuotationPrice_Types.Overview)
                {
                    pnl_OverView.Visible = true;
                    BindOverView();
                } 
                else
                {
                    BindBasicDetails();
                }
            }
        }

      

        public void BindBasicDetails()
        {
            //Declare Connection Variable (Connection)
            //Create a new object SqlConnection with the connection string initialized
            //Set the Connection variable to point to the new object instance.
            //If connection goes out of scope the database connection won't be closed and must be closed with either ( Close or Disopose )
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            //Opens a connection to the database.
            //If available draws from an open conneciton from the connection pool(if available)
            Connection.Open();
            // 

            String sql = "select * from [dbo].[QuotationPrice_Basic] where [QuotationPriceId] = @ID";
            // 
            //Declare SQLCommand Object named Command
            //Create a new Command object with a select statement that will open the row referenced by Request("ID")
            SqlCommand Command = new SqlCommand(sql, Connection);
            // 
            // 'Set the @ID parameter in the Command select query
            Command.Parameters.AddWithValue("@ID", quotationPriceId);
            // 
            //Declare a SqlDataReader Ojbect
            //Load it with the Command's select statement
            SqlDataAdapter sd = new SqlDataAdapter(Command);
            // 
            //If at least one record was found
            DataTable dt = new DataTable();
            sd.Fill(dt);
            // 
            //Close the Data Reader we are done with it.
            sd.Dispose();

            //Closes the Connection to the back to the connection pool.
            Connection.Close();
            Connection.Dispose();
            // 
            //The length of ID equals zero.
            //This is an insert so don't preload any data.

            QuotationBasicGrid.DataSource = dt;
            QuotationBasicGrid.DataBind();
        }

        #endregion

        #region Hotel prices

        public void BindHotelPriceDetails()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetQuotationPriceHotel(quotationPriceId.Value);
                QuotationPrice_Hotel.DataSource = dt;
                QuotationPrice_Hotel.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        txtHotelPrice_singleroomPerPerson.Text = dr["SingleRoomSupplementPerPerson"].ToString();
                        //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                        txtHotelPrice_NetRateInTota.Text = Convert.ToString(dr["SingleRoomSupplementPerPerson"]);
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtHotelPrice_MarkUpPercentage.Text = Convert.ToString(dr["MarkUpPercentage"]);
                        }
                        else
                        {//Set first time when there is no data available in Database for Hotel prices then Override the propery and set to controls
                            //Mark Up Percentage
                            if (this.MarkUpPercentage != 0)
                                txtHotelPrice_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtHotelPrice_GrossRatePerPerson.Text = Convert.ToString(dr["GrossRatePerPerson"]);
                        txtHotelPrice_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtHotelPrice_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtHotelPrice_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtHotelPrice_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtHotelPrice_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }

            }
        }

        protected void QuotationPrice_Hotel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.FindControl("ddlHotel_starHotel") != null)
                {

                    DropDownList ddlHotel_starHotel = e.Item.FindControl("ddlHotel_starHotel") as DropDownList;
                    new CRMPage().LoadCategoryFromGeneralTable(ddlHotel_starHotel, "LeadHotelStar");
                    if (e.Item.FindControl("hdnstarvalue") != null)
                    {
                        string strStartvalue = ((HiddenField)e.Item.FindControl("hdnstarvalue")).Value;
                        ddlHotel_starHotel.SelectedValue = strStartvalue;
                    }

                    DropDownList ddlHotel_Reco = e.Item.FindControl("ddlHotel_Reco") as DropDownList;
                    // Bind Star Hotels from Supplier Hotel details with Hotel types
                    new CRMPage().BindRecommendHotels(this.CompanyId,ddlHotel_Reco);
                    if (e.Item.FindControl("hdnRecommendedHotel") != null)
                    {
                        string strRecHotel = ((HiddenField)e.Item.FindControl("hdnRecommendedHotel")).Value;
                        ddlHotel_Reco.SelectedValue = strRecHotel;
                    }
                    if (this.NoOfPeople != 0)
                    {
                        if (e.Item.FindControl("txtHotel_NoOfPeople") != null && ((TextBox)e.Item.FindControl("txtHotel_NoOfPeople")).Text == "")
                        {
                            ((TextBox)e.Item.FindControl("txtHotel_NoOfPeople")).Text = NoOfPeople.ToString();
                        }
                    }
                }
            }
        }

        protected void btnHotelPrices_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_Hotel> objList = new List<DLQuotationPrice_Hotel>();

            foreach (RepeaterItem item in QuotationPrice_Hotel.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_Hotel();
                    obj = new LTSCRM_DL.DLQuotationPrice_Hotel();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);
                    if (hdnPkID != null && hdnPkID.Value != "")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);

                    var ddlHotel_Reco = (DropDownList)item.FindControl("ddlHotel_Reco");
                    if (ddlHotel_Reco != null && isNonZeroAndBlank(ddlHotel_Reco))
                    {
                        obj.RecommendedHotel = ddlHotel_Reco.SelectedValue;
                    }

                    var ddlHotel_starHotel = (DropDownList)item.FindControl("ddlHotel_starHotel");
                    if (ddlHotel_starHotel != null && isNonZeroAndBlank(ddlHotel_starHotel))
                    {
                        obj.StartHotel = Convert.ToInt32(ddlHotel_starHotel.SelectedValue);
                    }

                    var txtHotel_TwinPerPerson = (TextBox)item.FindControl("txtHotel_TwinPerPerson");
                    if (txtHotel_TwinPerPerson != null && txtHotel_TwinPerPerson.Text.Trim() != "")
                    {
                        obj.TwinPerPerson = Convert.ToDecimal(txtHotel_TwinPerPerson.Text);
                    }
                    var txtHotel_NoOfPeople = (TextBox)item.FindControl("txtHotel_NoOfPeople");
                    if (txtHotel_NoOfPeople != null && txtHotel_NoOfPeople.Text.Trim() != "")
                    {
                        obj.NoOfPeople = Convert.ToInt32(txtHotel_NoOfPeople.Text);
                    }
                    //calculate prices add to object
                    objList.Add(obj);



                }
            }

            try
            {
                //SingleRoomSupplementPerPerson
                //Total rate from Day 1 to day 6
                decimal dec_totalRateSinglePerPerson = 0;//objList.Sum(x => x.TwinPerPerson.Value);
                foreach (DLQuotationPrice_Hotel h in objList)
                {
                    if (!h.TwinPerPerson.IsNull)
                        dec_totalRateSinglePerPerson += h.TwinPerPerson.Value;
                }
                txtHotelPrice_singleroomPerPerson.Text = dec_totalRateSinglePerPerson.ToString();

                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_Hotel h in objList)
                {
                    if (!h.TwinPerPerson.IsNull)
                        dec_netRateTotal += h.TwinPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value);
                }
                txtHotelPrice_NetRateInTota.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtHotelPrice_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtHotelPrice_MarkUpPercentage.Text);

                    //Gross rate per person: Total rate from Day 1 to day 6 x mar up percentage
                    txtHotelPrice_GrossRatePerPerson.Text = (dec_totalRateSinglePerPerson + (dec_totalRateSinglePerPerson * dec_markupvalue / 100)).ToString();
                }
                //Gross Rate in total ::	Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_Hotel h in objList)
                {
                    if (!h.TwinPerPerson.IsNull)
                        dec_dtotal += (h.TwinPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value)) + (h.TwinPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value)) * dec_markupvalue / 100;
                }
                txtHotelPrice_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtHotelPrice_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_Hotel objInsert in objList)
                {
                    objInsert.SingleRoomSupplementPerPerson = dec_totalRateSinglePerPerson;
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRatePerPerson = Convert.ToDecimal(txtHotelPrice_GrossRatePerPerson.Text);
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtHotelPrice_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtHotelPrice_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());

                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());

                    }
                }
                BindHotelPriceDetails();
            }
            finally
            {

            }

        }
        #endregion

        #region VanHire prices
        
        public void BindVanHireDetails()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetVanHireDetails(quotationPriceId.Value);
                rpt_VanHire.DataSource = dt;
                rpt_VanHire.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtVanHire_NetRateInTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtVanHire_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtVanHire_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtVanHire_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtVanHire_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtVanHire_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtVanHire_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtVanHire_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }
            }
        }
        protected void btnVanHireSave_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_VanHire> objList = new List<DLQuotationPrice_VanHire>();

            foreach (RepeaterItem item in rpt_VanHire.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_VanHire();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);

                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);

                    var ddlVanHire_TypeOfVehicle = (DropDownList)item.FindControl("ddlVanHire_TypeOfVehicle");
                    obj.TypeOfVehicle = ddlVanHire_TypeOfVehicle.SelectedItem.Text;

                    var txtVanHire_VehicleRatePerDay = (TextBox)item.FindControl("txtVanHire_VehicleRatePerDay");
                    if (txtVanHire_VehicleRatePerDay != null && txtVanHire_VehicleRatePerDay.Text.Trim() != "")
                    {
                        obj.VehicleRatePerDay = Convert.ToDecimal(txtVanHire_VehicleRatePerDay.Text);
                    }

                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {
                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_VanHire h in objList)
                {
                    if (!h.VehicleRatePerDay.IsNull)
                        dec_netRateTotal += h.VehicleRatePerDay.Value;
                }

                txtVanHire_NetRateInTotal.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtVanHire_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtVanHire_MarkUpPercentage.Text);
                }

                //Gross Rate in total :: Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_VanHire h in objList)
                {
                    if (!h.VehicleRatePerDay.IsNull)
                    {
                        dec_dtotal +=
                           (h.VehicleRatePerDay.Value)
                           + (h.VehicleRatePerDay.Value) * dec_markupvalue / 100;
                    }
                }
                txtVanHire_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtVanHire_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_VanHire objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtVanHire_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtVanHire_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindVanHireDetails();
            }
            finally
            {

            }
        }

        protected void rpt_VanHire_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.FindControl("ddlVanHire_TypeOfVehicle") != null)
                {
                    DropDownList ddlVanHire_TypeOfVehicle = e.Item.FindControl("ddlVanHire_TypeOfVehicle") as DropDownList;
                    new CRMPage().LoadCategoryFromGeneralTable(ddlVanHire_TypeOfVehicle, "VehicaleType");
                    //BindVanHireDropdown(ddlVanHire_TypeOfVehicle);
                    if (e.Item.FindControl("hdnTypeOfVehicle") != null)
                    {
                        string vals = ((HiddenField)e.Item.FindControl("hdnTypeOfVehicle")).Value;
                        if (ddlVanHire_TypeOfVehicle.Items.FindByText(vals) != null)
                        {
                            ddlVanHire_TypeOfVehicle.ClearSelection();
                            ddlVanHire_TypeOfVehicle.Items.FindByText(vals).Selected = true;
                        }
                    }
                }
            }
        }
        #endregion

        #region Driver 
       public void BindDriverAccomodation()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetDriverGuideDetails(quotationPriceId.Value);
                rpt_QuotationPrice_DriverAndAccom.DataSource = dt;
                rpt_QuotationPrice_DriverAndAccom.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtDriverAndAccomPrice_NetRateInTota.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtDriverAndAccomPrice_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtDriverAndAccomPrice_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtDriverAndAccomPrice_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtDriverAndAccomPrice_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtDriverAndAccomPrice_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtDriverAndAccomPrice_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtDriverAndAccomPrice_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                        if (Convert.ToString(dr["ConfirmationStatus"]) != "")
                        {
                            txtDriverAndAccomPrice_ConfirmationStatus.Text = Convert.ToString(dr["ConfirmationStatus"]);
                        }
                    }
                }
             
        }
        }
        protected void btnDriverAndAccomPrices_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_DriveGuide> objList = new List<DLQuotationPrice_DriveGuide>();

            foreach (RepeaterItem item in rpt_QuotationPrice_DriverAndAccom.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_DriveGuide();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);

                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);


                    var txtDriverAndAccom_RateRoom = (TextBox)item.FindControl("txtDriverAndAccom_RateRoom");
                    if (txtDriverAndAccom_RateRoom != null && txtDriverAndAccom_RateRoom.Text.Trim() != "")
                    {
                        obj.RatePerRoom = Convert.ToDecimal(txtDriverAndAccom_RateRoom.Text);
                    }

                    var txtDriverAndAccom_NoOfRoom = (TextBox)item.FindControl("txtDriverAndAccom_NoOfRoom");
                    if (txtDriverAndAccom_NoOfRoom != null && txtDriverAndAccom_NoOfRoom.Text.Trim() != "")
                    {
                        obj.NoOfRoom = Convert.ToInt32(txtDriverAndAccom_NoOfRoom.Text);
                    }

                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {
                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_DriveGuide h in objList)
                {
                    if (!h.RatePerRoom.IsNull && !h.NoOfRoom.IsNull)
                        dec_netRateTotal += h.RatePerRoom.Value * Convert.ToDecimal(h.NoOfRoom.Value);
                }

                txtDriverAndAccomPrice_NetRateInTota.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtDriverAndAccomPrice_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtDriverAndAccomPrice_MarkUpPercentage.Text);
                }
                //Gross Rate in total ::	Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_DriveGuide h in objList)
                {
                    if (!h.RatePerRoom.IsNull)
                    {
                        dec_dtotal +=
                           (h.RatePerRoom.Value * Convert.ToDecimal(h.NoOfRoom.Value))
                           + (h.RatePerRoom.Value * Convert.ToDecimal(h.NoOfRoom.Value)) * dec_markupvalue / 100;
                    }
                    
                }
                txtDriverAndAccomPrice_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtDriverAndAccomPrice_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_DriveGuide objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtDriverAndAccomPrice_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtDriverAndAccomPrice_QuotationCurrency.Text.Trim();
                    objInsert.ConfirmationStatus = txtDriverAndAccomPrice_ConfirmationStatus.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindDriverAccomodation();
            }
            finally
            {

            }
        }

 
        #endregion

        #region Coach

        public void BindCoachDetails()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetCoachDetails(quotationPriceId.Value);
                QuotationPrice_Coach.DataSource = dt;
                QuotationPrice_Coach.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtCoach_GrossRateInTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtCoach_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtCoach_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtCoach_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtCoach_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtCoach_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtCoach_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtCoach_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                        if (Convert.ToString(dr["ConfirmationStatus"]) != "")
                        {
                            txtCoach_ConfirmationStatus.Text = Convert.ToString(dr["ConfirmationStatus"]);
                        }
                    }
                }

            }
        }
        protected void QuotationPrice_Coach_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.FindControl("ddlCoach_CoachType") != null)
                {
                    DropDownList ddlCoach_CoachType = e.Item.FindControl("ddlCoach_CoachType") as DropDownList;
                    
                    //new CRMPage().LoadCategoryFromGeneralTable(ddlCoach_CoachType, "CoachType");
                    BindCoachDropdown(ddlCoach_CoachType);
                    if (e.Item.FindControl("hdnTypeOfVehicle") != null)
                    {
                        string vals = ((HiddenField)e.Item.FindControl("hdnCoachType")).Value;
                        if (ddlCoach_CoachType.Items.FindByText(vals) != null)
                        {
                            ddlCoach_CoachType.ClearSelection();
                            ddlCoach_CoachType.Items.FindByText(vals).Selected = true;
                        }
                    }
                }
            }
        }

        private void BindCoachDropdown(DropDownList ddlCoach_CoachType)
        {
            DataTable dt= BLSupplierCoach.GetSupplierCoachMaster(this.CompanyId);
            ddlCoach_CoachType.DataSource = dt;
            ddlCoach_CoachType.DataTextField = "Name";
            ddlCoach_CoachType.DataValueField= "Id";
            ddlCoach_CoachType.DataBind();
        }
        private void BindVanHireDropdown(DropDownList ddlVanHire)
        {
            DataTable dt = BLSupplierVanHire.GetSupplierVanMaster(this.CompanyId);
            ddlVanHire.DataSource = dt;
            ddlVanHire.DataTextField = "Name";
            ddlVanHire.DataValueField = "Id";
            ddlVanHire.DataBind();
        }
        protected void btnCoachPrices_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_Coach> objList = new List<DLQuotationPrice_Coach>();

            foreach (RepeaterItem item in QuotationPrice_Coach.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_Coach();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);

                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);

                    var ddlCoach_CoachType = (DropDownList)item.FindControl("ddlCoach_CoachType");
                    obj.TypeOfCoach = ddlCoach_CoachType.SelectedItem.Text;

                    var txtCoach_RatePerDay = (TextBox)item.FindControl("txtCoach_RatePerDay");
                    if (txtCoach_RatePerDay != null && txtCoach_RatePerDay.Text.Trim() != "")
                    {
                        obj.CoachRatePerDay = Convert.ToDecimal(txtCoach_RatePerDay.Text);
                    }
                    //calculate prices add to object
                    objList.Add(obj);
                }
            }
            try
            {
                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_Coach h in objList)
                {
                    if (!h.CoachRatePerDay.IsNull)
                        dec_netRateTotal += h.CoachRatePerDay.Value;
                }

                txtCoach_NetRateInTotal.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtCoach_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtCoach_MarkUpPercentage.Text);
                }

                //Gross Rate in total :: Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_Coach h in objList)
                {
                    if (!h.CoachRatePerDay.IsNull)
                    {
                        dec_dtotal +=
                           (h.CoachRatePerDay.Value)
                           + (h.CoachRatePerDay.Value) * dec_markupvalue / 100;
                    }
                }
                txtCoach_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtCoach_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_Coach objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtCoach_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtCoach_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindCoachDetails();
            }
            finally
            {

            }
        }
        #endregion

        #region Other services
        public void BindOtherservices()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetOtherservices(quotationPriceId.Value);
                rpt_OtherServices.DataSource = dt;
                rpt_OtherServices.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtOther_NetRateTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtOther_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtOther_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtOther_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtOther_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtOther_Remark.Text = Convert.ToString(dr["Remark"]);
                        txtOther_Notes.Text = Convert.ToString(dr["Notes"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtOther_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtOther_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }

            }
        }

        protected void rpt_OtherServices_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (this.NoOfPeople != 0)
                {
                    if (e.Item.FindControl("txtOther_NoOfPeople") != null && ((TextBox)e.Item.FindControl("txtOther_NoOfPeople")).Text == "")
                    {
                        ((TextBox)e.Item.FindControl("txtOther_NoOfPeople")).Text = NoOfPeople.ToString();
                    }
                }
            }
        }

        protected void btnSaveOtherServices_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_OtherServices> objList = new List<DLQuotationPrice_OtherServices>();

            foreach (RepeaterItem item in rpt_OtherServices.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_OtherServices();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);
                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);

                    var txtOther_OtherServices = (TextBox)item.FindControl("txtOther_OtherServices");
                    if (txtOther_OtherServices != null && txtOther_OtherServices.Text.Trim() != "")
                    {
                        obj.OtherServices = txtOther_OtherServices.Text;
                    }
                    var txtOther_RatePerPerson = (TextBox)item.FindControl("txtOther_RatePerPerson");
                    if (txtOther_RatePerPerson != null && txtOther_RatePerPerson.Text.Trim() != "")
                    {
                        obj.PricePerPerson = Convert.ToDecimal(txtOther_RatePerPerson.Text);
                    }
                    var txtOther_NoOfPeople = (TextBox)item.FindControl("txtOther_NoOfPeople");
                    if (txtOther_NoOfPeople != null && txtOther_NoOfPeople.Text.Trim() != "")
                    {
                        obj.NoOfPeople = Convert.ToInt32(txtOther_NoOfPeople.Text);
                    }
                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {

                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_OtherServices h in objList)
                {
                    if (!h.PricePerPerson.IsNull)
                        dec_netRateTotal += h.PricePerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value);
                }
                txtHotelPrice_NetRateInTota.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtOther_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtOther_MarkUpPercentage.Text);
                }
                //Gross Rate in total ::	Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_OtherServices h in objList)
                {
                    if (!h.PricePerPerson.IsNull)
                        dec_dtotal += (h.PricePerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value)) + (h.PricePerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value)) * dec_markupvalue / 100;
                }
                txtOther_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtOther_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_OtherServices objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtOther_Remark.Text.Trim();
                    objInsert.Notes = txtOther_Notes.Text.Trim();
                    objInsert.QuotationCurrency = txtOther_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindOtherservices();

            }
            finally
            {

            }
        }
        #endregion

        #region Enterance Details

        public void BindEnterance()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetEnteranceDetails(quotationPriceId.Value);
                rpt_Enterance.DataSource = dt;
                rpt_Enterance.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtEnterance_NetRateInTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtEnterance_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtEnterance_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtEnterance_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtEnterance_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtEnterance_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtEnterance_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtEnterance_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }
            }
        }

        protected
        void rpt_Enterance_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (this.NoOfPeople != 0)
                {
                    if (e.Item.FindControl("txtEnterance_NoOfChild") != null && ((TextBox)e.Item.FindControl("txtEnterance_NoOfChild")).Text == "")
                    {
                        ((TextBox)e.Item.FindControl("txtEnterance_NoOfChild")).Text = NoOfPeople.ToString();
                    }
                    if (e.Item.FindControl("txtEnterance_NoOfStudent") != null && ((TextBox)e.Item.FindControl("txtEnterance_NoOfStudent")).Text == "")
                    {
                        ((TextBox)e.Item.FindControl("txtEnterance_NoOfStudent")).Text = NoOfPeople.ToString();
                    }
                    if (e.Item.FindControl("txtEnterance_NoOfPeopleAdult") != null && ((TextBox)e.Item.FindControl("txtEnterance_NoOfPeopleAdult")).Text == "")
                    {
                        ((TextBox)e.Item.FindControl("txtEnterance_NoOfPeopleAdult")).Text = NoOfPeople.ToString();
                    }
                }
            }
        }

        protected void btnEnteranceSave_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_Entrance> objList = new List<DLQuotationPrice_Entrance>();

            foreach (RepeaterItem item in rpt_Enterance.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_Entrance();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);
                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);


                    var txtEnterance_AdultPrice = (TextBox)item.FindControl("txtEnterance_AdultPrice");
                    if (txtEnterance_AdultPrice != null && txtEnterance_AdultPrice.Text.Trim() != "")
                    {
                        obj.AdultPrice = Convert.ToDecimal(txtEnterance_AdultPrice.Text);
                    }
                    var txtEnterance_NoOfPeopleAdult = (TextBox)item.FindControl("txtEnterance_NoOfPeopleAdult");
                    if (txtEnterance_NoOfPeopleAdult != null && txtEnterance_NoOfPeopleAdult.Text.Trim() != "")
                    {
                        obj.NoOfPeople = Convert.ToInt32(txtEnterance_NoOfPeopleAdult.Text);
                    }


                    var txtEnterance_ChildPrice = (TextBox)item.FindControl("txtEnterance_ChildPrice");
                    if (txtEnterance_ChildPrice != null && txtEnterance_ChildPrice.Text.Trim() != "")
                    {
                        obj.ChildPrice = Convert.ToDecimal(txtEnterance_ChildPrice.Text);
                    }
                    var txtEnterance_NoOfChild = (TextBox)item.FindControl("txtEnterance_NoOfChild");
                    if (txtEnterance_NoOfChild != null && txtEnterance_NoOfChild.Text.Trim() != "")
                    {
                        obj.NoOfChild = Convert.ToInt32(txtEnterance_NoOfChild.Text);
                    }

                    var txtEnterance_ConcessionStudent = (TextBox)item.FindControl("txtEnterance_ConcessionStudent");
                    if (txtEnterance_ConcessionStudent != null && txtEnterance_ConcessionStudent.Text.Trim() != "")
                    {
                        obj.Concession_student_elderly = Convert.ToDecimal(txtEnterance_ConcessionStudent.Text);
                    }
                    var txtEnterance_NoOfStudent = (TextBox)item.FindControl("txtEnterance_NoOfStudent");
                    if (txtEnterance_NoOfStudent != null && txtEnterance_NoOfStudent.Text.Trim() != "")
                    {
                        obj.NoOfStudentForConcession = Convert.ToInt32(txtEnterance_NoOfStudent.Text);
                    }

                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {

                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_Entrance h in objList)
                {
                    if (!h.AdultPrice.IsNull)
                        dec_netRateTotal += h.AdultPrice.Value * Convert.ToDecimal(h.NoOfPeople.Value);
                    if (!h.ChildPrice.IsNull)
                        dec_netRateTotal += h.ChildPrice.Value * Convert.ToDecimal(h.NoOfChild.Value);
                    if (!h.Concession_student_elderly.IsNull)
                        dec_netRateTotal += h.Concession_student_elderly.Value * Convert.ToDecimal(h.NoOfStudentForConcession.Value);
                }
                txtEnterance_NetRateInTotal.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtEnterance_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtEnterance_MarkUpPercentage.Text);
                }
                //Gross Rate in total ::	Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_Entrance h in objList)
                {
                    if (!h.AdultPrice.IsNull)
                    {
                        dec_dtotal +=
                           (h.AdultPrice.Value * Convert.ToDecimal(h.NoOfPeople.Value))
                           + (h.AdultPrice.Value * Convert.ToDecimal(h.NoOfPeople.Value)) * dec_markupvalue / 100;
                    }
                    if (!h.ChildPrice.IsNull)
                    {
                        dec_dtotal +=
                           (h.ChildPrice.Value * Convert.ToDecimal(h.NoOfChild.Value))
                           + (h.ChildPrice.Value * Convert.ToDecimal(h.NoOfChild.Value)) * dec_markupvalue / 100;
                    }

                    //Here is cocession calculation - Check here if required deduct or add
                    if (!h.Concession_student_elderly.IsNull)
                    {
                        dec_dtotal +=
                           (h.Concession_student_elderly.Value * Convert.ToDecimal(h.NoOfStudentForConcession.Value))
                           + (h.Concession_student_elderly.Value * Convert.ToDecimal(h.NoOfStudentForConcession.Value)) * dec_markupvalue / 100;
                    }
                }
                txtEnterance_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtOther_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_Entrance objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtEnterance_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtEnterance_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindEnterance();

            }
            finally
            {

            }
        }

        #endregion

        #region Tips

        public void BindTips()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetTipsDetails(quotationPriceId.Value);
                rpt_Tips.DataSource = dt;
                rpt_Tips.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtEnterance_NetRateInTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtEnterance_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtEnterance_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtEnterance_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtEnterance_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtEnterance_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtEnterance_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtEnterance_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }
            }
        }
        protected void btnTipsSave_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_Tips> objList = new List<DLQuotationPrice_Tips>();
            foreach (RepeaterItem item in rpt_Tips.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_Tips();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);
                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);


                    var txtTips_NoOfPeople = (TextBox)item.FindControl("txtTips_NoOfPeople");
                    if (txtTips_NoOfPeople != null && txtTips_NoOfPeople.Text.Trim() != "")
                    {
                        obj.NoOfPeople = Convert.ToInt32(txtTips_NoOfPeople.Text);
                    }
                    var txtTips_TipsOftheDay = (TextBox)item.FindControl("txtTips_TipsOftheDay");
                    if (txtTips_TipsOftheDay != null && txtTips_TipsOftheDay.Text.Trim() != "")
                    {
                        obj.TipsPerDay = Convert.ToInt32(txtTips_TipsOftheDay.Text);
                    }

                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {

                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_Tips h in objList)
                {
                    if (!h.TipsPerDay.IsNull)
                        dec_netRateTotal += h.TipsPerDay.Value * Convert.ToDecimal(h.NoOfPeople.Value);
                }
                txtTips_NetRateInTotal.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtTips_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtTips_MarkUpPercentage.Text);
                }
                //Gross Rate in total ::	Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_Tips h in objList)
                {
                    if (!h.TipsPerDay.IsNull)
                    {
                        dec_dtotal +=
                           (h.TipsPerDay.Value * Convert.ToDecimal(h.NoOfPeople.Value))
                           + (h.TipsPerDay.Value * Convert.ToDecimal(h.NoOfPeople.Value)) * dec_markupvalue / 100;
                    }
                }
                txtTips_GrossrateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtOther_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_Tips objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtTips_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtTips_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindTips();
            }
            finally
            {

            }
        }

        protected void rpt_Tips_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (this.NoOfPeople != 0)
                {
                    if (e.Item.FindControl("txtTips_NoOfPeople") != null && ((TextBox)e.Item.FindControl("txtTips_NoOfPeople")).Text == "")
                    {
                        ((TextBox)e.Item.FindControl("txtTips_NoOfPeople")).Text = NoOfPeople.ToString();
                    }
                }
            }
        }
        #endregion

        #region Dinner Details
        public void BindDinner()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetDinnerDetails(quotationPriceId.Value);
                rpt_Dinner.DataSource = dt;
                rpt_Dinner.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtDinner_NetRateInTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtDinner_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtDinner_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtDinner_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtDinner_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtDinner_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtDinner_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtDinner_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }
            }
        }
        protected void rpt_Dinner_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.FindControl("ddlDinner_TypeOfMeal") != null)
                {
                    DropDownList ddlDinner_TypeOfMeal = e.Item.FindControl("ddlDinner_TypeOfMeal") as DropDownList;
                    new CRMPage().LoadCategoryFromGeneralTable(ddlDinner_TypeOfMeal, "MealType");
                    if (e.Item.FindControl("hdnTypeofMeal") != null)
                    {
                        string vals = ((HiddenField)e.Item.FindControl("hdnTypeofMeal")).Value;
                        if (ddlDinner_TypeOfMeal.Items.FindByText(vals) != null)
                        {
                            ddlDinner_TypeOfMeal.ClearSelection();
                            ddlDinner_TypeOfMeal.Items.FindByText(vals).Selected = true;
                        }
                    }
                }

                if (this.NoOfPeople != 0)
                {
                    if (e.Item.FindControl("txtDinner_NoOfPeople") != null && ((TextBox)e.Item.FindControl("txtDinner_NoOfPeople")).Text == "")
                    {
                        ((TextBox)e.Item.FindControl("txtDinner_NoOfPeople")).Text = NoOfPeople.ToString();
                    }
                }
            }
        }

        protected void btnDinnerSave_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_Dinner> objList = new List<DLQuotationPrice_Dinner>();

            foreach (RepeaterItem item in rpt_Dinner.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_Dinner();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);

                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);

                    var ddlDinner_TypeOfMeal = (DropDownList)item.FindControl("ddlDinner_TypeOfMeal");
                    obj.TypeOfMeal = ddlDinner_TypeOfMeal.SelectedItem.Text;

                    var txtDinner_MealPerPerson = (TextBox)item.FindControl("txtDinner_MealPerPerson");
                    if (txtDinner_MealPerPerson != null && txtDinner_MealPerPerson.Text.Trim() != "")
                    {
                        obj.MealPerPerson = Convert.ToDecimal(txtDinner_MealPerPerson.Text);
                    }

                    var txtDinner_NoOfPeople = (TextBox)item.FindControl("txtDinner_NoOfPeople");
                    if (txtDinner_NoOfPeople != null && txtDinner_NoOfPeople.Text.Trim() != "")
                    {
                        obj.NoOfPeople = Convert.ToInt32(txtDinner_NoOfPeople.Text);
                    }

                    var txtDinner_MealSupplementForGuid = (TextBox)item.FindControl("txtDinner_MealSupplementForGuid");
                    if (txtDinner_MealSupplementForGuid != null && txtDinner_MealSupplementForGuid.Text.Trim() != "")
                    {
                        obj.MealSupplementForGuid = Convert.ToDecimal(txtDinner_MealSupplementForGuid.Text);
                    }

                    var txtDinner_NoOfTourGuidePeople = (TextBox)item.FindControl("txtDinner_NoOfTourGuidePeople");
                    if (txtDinner_NoOfTourGuidePeople != null && txtDinner_NoOfTourGuidePeople.Text.Trim() != "")
                    {
                        obj.NoOfTourGuidePeople = Convert.ToInt32(txtDinner_NoOfTourGuidePeople.Text);
                    }

                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {
                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_Dinner h in objList)
                {
                    if (!h.MealPerPerson.IsNull && !h.NoOfPeople.IsNull)
                        dec_netRateTotal += h.MealPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value);
                    if (!h.MealSupplementForGuid.IsNull && !h.NoOfTourGuidePeople.IsNull)
                        dec_netRateTotal += h.MealSupplementForGuid.Value * Convert.ToDecimal(h.NoOfTourGuidePeople.Value);
                }

                txtDinner_NetRateInTotal.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtDinner_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtEnterance_MarkUpPercentage.Text);
                }
                //Gross Rate in total ::	Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_Dinner h in objList)
                {
                    if (!h.MealPerPerson.IsNull)
                    {
                        dec_dtotal +=
                           (h.MealPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value))
                           + (h.MealPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value)) * dec_markupvalue / 100;
                    }
                    if (!h.MealSupplementForGuid.IsNull)
                    {
                        dec_dtotal +=
                           (h.MealSupplementForGuid.Value * Convert.ToDecimal(h.NoOfTourGuidePeople.Value))
                           + (h.MealSupplementForGuid.Value * Convert.ToDecimal(h.NoOfTourGuidePeople.Value)) * dec_markupvalue / 100;
                    }
                }
                txtDinner_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtDinner_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_Dinner objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtDinner_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtDinner_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindDinner();
            }
            finally
            {

            }
        }
       
        #endregion

        #region Lunch Transactions

       public void BindLunch()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetLunchDetails(quotationPriceId.Value);
                rpt_Lunch.DataSource = dt;
                rpt_Lunch.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtLunch_NetRateInTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtLunch_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtLunch_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtLunch_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtLunch_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtLunch_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtLunch_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtLunch_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }
            }
        }
        protected void rpt_Lunch_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.FindControl("ddlLunch_TypeOfMeal") != null)
                {
                    DropDownList ddlLunch_TypeOfMeal = e.Item.FindControl("ddlLunch_TypeOfMeal") as DropDownList;
                    new CRMPage().LoadCategoryFromGeneralTable(ddlLunch_TypeOfMeal, "MealType");
                    if (e.Item.FindControl("hdnTypeofMeal") != null)
                    {
                        string vals = ((HiddenField)e.Item.FindControl("hdnTypeofMeal")).Value;
                        if (ddlLunch_TypeOfMeal.Items.FindByText(vals) != null)
                        {
                            ddlLunch_TypeOfMeal.ClearSelection();
                            ddlLunch_TypeOfMeal.Items.FindByText(vals).Selected = true;
                        }
                    }
                }

                if (this.NoOfPeople != 0)
                {
                    if (e.Item.FindControl("txtLunch_NoOfPeople") != null && ((TextBox)e.Item.FindControl("txtLunch_NoOfPeople")).Text == "")
                    {
                        ((TextBox)e.Item.FindControl("txtLunch_NoOfPeople")).Text = NoOfPeople.ToString();
                    }
                }

                //if required to set no. of people for guide here.
                //...
            }
        }
        protected void btnLunchSave_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_Lunch> objList = new List<DLQuotationPrice_Lunch>();

            foreach (RepeaterItem item in rpt_Lunch.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_Lunch();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);

                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);

                    var ddlLunch_TypeOfMeal = (DropDownList)item.FindControl("ddlLunch_TypeOfMeal");
                    obj.TypeOfMeal = ddlLunch_TypeOfMeal.SelectedItem.Text;

                    var txtLunch_MealPerPerson = (TextBox)item.FindControl("txtLunch_MealPerPerson");
                    if (txtLunch_MealPerPerson != null && txtLunch_MealPerPerson.Text.Trim() != "")
                    {
                        obj.MealPerPerson = Convert.ToDecimal(txtLunch_MealPerPerson.Text);
                    }

                    var txtLunch_NoOfPeople = (TextBox)item.FindControl("txtLunch_NoOfPeople");
                    if (txtLunch_NoOfPeople != null && txtLunch_NoOfPeople.Text.Trim() != "")
                    {
                        obj.NoOfPeople = Convert.ToInt32(txtLunch_NoOfPeople.Text);
                    }

                    var txtLunch_MealSupplementForGuid = (TextBox)item.FindControl("txtLunch_MealSupplementForGuid");
                    if (txtLunch_MealSupplementForGuid != null && txtLunch_MealSupplementForGuid.Text.Trim() != "")
                    {
                        obj.MealSupplementForGuid = Convert.ToDecimal(txtLunch_MealSupplementForGuid.Text);
                    }

                    var txtLunch_NoOfTourGuidePeople = (TextBox)item.FindControl("txtLunch_NoOfTourGuidePeople");
                    if (txtLunch_NoOfTourGuidePeople != null && txtLunch_NoOfTourGuidePeople.Text.Trim() != "")
                    {
                        obj.NoOfTourGuidePeople = Convert.ToInt32(txtLunch_NoOfTourGuidePeople.Text);
                    }

                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {
                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_Lunch h in objList)
                {
                    if (!h.MealPerPerson.IsNull && !h.NoOfPeople.IsNull)
                        dec_netRateTotal += h.MealPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value);
                    if (!h.MealSupplementForGuid.IsNull && !h.NoOfTourGuidePeople.IsNull)
                        dec_netRateTotal += h.MealSupplementForGuid.Value * Convert.ToDecimal(h.NoOfTourGuidePeople.Value);
                }

                txtLunch_NetRateInTotal.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtLunch_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtLunch_MarkUpPercentage.Text);
                }
                //Gross Rate in total ::	Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_Lunch h in objList)
                {
                    if (!h.MealPerPerson.IsNull)
                    {
                        dec_dtotal +=
                           (h.MealPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value))
                           + (h.MealPerPerson.Value * Convert.ToDecimal(h.NoOfPeople.Value)) * dec_markupvalue / 100;
                    }
                    if (!h.MealSupplementForGuid.IsNull)
                    {
                        dec_dtotal +=
                           (h.MealSupplementForGuid.Value * Convert.ToDecimal(h.NoOfTourGuidePeople.Value))
                           + (h.MealSupplementForGuid.Value * Convert.ToDecimal(h.NoOfTourGuidePeople.Value)) * dec_markupvalue / 100;
                    }
                }
                txtLunch_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtLunch_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_Lunch objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtLunch_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtLunch_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindLunch();
            }
            finally
            {

            }
        }
        #endregion

        #region Tour Guide
        public void BindTourGuide()
        {
            if (quotationPriceId.HasValue)
            {
                // visible control
                DataTable dt = BLOperationQuotations.GetTourGuideDetails(quotationPriceId.Value);
                rpt_TourGuide.DataSource = dt;
                rpt_TourGuide.DataBind();

                //set Textboxs
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        // other service repeater bind here
                        txtTourGuide_NetRateInTotal.Text = dr["NetRateInTotal"].ToString();

                        //Mark Up Percentage
                        if (Convert.ToString(dr["MarkUpPercentage"]) != "")
                        {
                            txtTourGuide_MarkUpPercentage.Text = dr["MarkUpPercentage"].ToString();
                        }
                        else
                        {
                            if (this.MarkUpPercentage != 0)
                                txtTourGuide_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
                        }
                        txtTourGuide_GrossRateInTotal.Text = Convert.ToString(dr["GrossRateInTotal"]);
                        txtTourGuide_Profit.Text = Convert.ToString(dr["Profit"]);
                        txtTourGuide_Remark.Text = Convert.ToString(dr["Remark"]);
                        if (Convert.ToString(dr["QuotationCurrency"]) != "")
                        {
                            txtTourGuide_QuotationCurrency.Text = Convert.ToString(dr["QuotationCurrency"]);
                        }
                        else
                        {
                            if (this.QuotationCurrency != "")
                                txtTourGuide_QuotationCurrency.Text = this.QuotationCurrency.ToString();
                        }
                    }
                }
            }
        }
        protected void rpt_TourGuide_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.FindControl("ddlTourGuide_TypeOfGuide") != null)
                {
                    DropDownList ddlTourGuide_TypeOfGuide = e.Item.FindControl("ddlTourGuide_TypeOfGuide") as DropDownList;
                    new CRMPage().LoadCategoryFromGeneralTable(ddlTourGuide_TypeOfGuide, "GuideType");
                    if (e.Item.FindControl("hdnTypeofGuide") != null)
                    {
                        string vals = ((HiddenField)e.Item.FindControl("hdnTypeofGuide")).Value;
                        if (ddlTourGuide_TypeOfGuide.Items.FindByText(vals) != null)
                        {
                            ddlTourGuide_TypeOfGuide.ClearSelection();
                            ddlTourGuide_TypeOfGuide.Items.FindByText(vals).Selected = true;
                        }
                    }
                }
            }
        }

        protected void btnTourGuideSave_Click(object sender, EventArgs e)
        {
            List<DLQuotationPrice_TourGuide> objList = new List<DLQuotationPrice_TourGuide>();

            foreach (RepeaterItem item in rpt_TourGuide.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var obj = new DLQuotationPrice_TourGuide();
                    HiddenField hdnBasiID = (HiddenField)item.FindControl("hdnQuotationBasicId");
                    HiddenField hdnPkID = (HiddenField)item.FindControl("hdnPK");

                    if (hdnBasiID != null)
                        obj.QuotationPriceBasicId = Convert.ToInt32(hdnBasiID.Value);

                    if (hdnPkID != null && hdnPkID.Value != "" && hdnPkID.Value != "0")
                        obj.ID = Convert.ToInt32(hdnPkID.Value);

                    var ddlTourGuide_TypeOfGuide = (DropDownList)item.FindControl("ddlTourGuide_TypeOfGuide");
                    obj.TypeOfGuide = ddlTourGuide_TypeOfGuide.SelectedItem.Text;

                    var txtTypeOfGuide_RatePerDay = (TextBox)item.FindControl("txtTypeOfGuide_RatePerDay");
                    if (txtTypeOfGuide_RatePerDay != null && txtTypeOfGuide_RatePerDay.Text.Trim() != "")
                    {
                        obj.RatePerDay = Convert.ToDecimal(txtTypeOfGuide_RatePerDay.Text);
                    }

                    //calculate prices add to object
                    objList.Add(obj);
                }
            }

            try
            {
                //NetRateInTotal : Total rate from Day 1 to day 6 x no of people
                decimal dec_netRateTotal = 0;//dec_totalRateSinglePerPerson * objList.Sum(x => x.NoOfPeople.Value);
                foreach (DLQuotationPrice_TourGuide h in objList)
                {
                    if (!h.RatePerDay.IsNull)
                        dec_netRateTotal += h.RatePerDay.Value;
                }

                txtTourGuide_NetRateInTotal.Text = dec_netRateTotal.ToString();

                //Mark Up Percentage
                decimal dec_markupvalue = 0;

                //if (objList.Where(x => x.MarkUpPercentage.IsNull == false).FirstOrDefault() != null)
                // if markup not set then set 1 by default
                if (txtTourGuide_MarkUpPercentage.Text.Trim().Length > 0)
                {
                    //dec_markupvalue = objList.Select(x => x.MarkUpPercentage.Value).FirstOrDefault();
                    dec_markupvalue = Convert.ToDecimal(txtTourGuide_MarkUpPercentage.Text);
                }

                //Gross Rate in total :: Total Rate from Day 1 to day 6  x no of people x mark up percentage
                decimal dec_dtotal = 0;
                foreach (DLQuotationPrice_TourGuide h in objList)
                {
                    if (!h.RatePerDay.IsNull)
                    {
                        dec_dtotal +=
                           (h.RatePerDay.Value)
                           + (h.RatePerDay.Value) * dec_markupvalue / 100;
                    }
                }
                txtTourGuide_GrossRateInTotal.Text = dec_dtotal.ToString();

                //Profit Gross Rate in Total - Net Rate in total 
                txtTourGuide_Profit.Text = (dec_dtotal - dec_netRateTotal).ToString();

                //Save each day data to Database
                foreach (DLQuotationPrice_TourGuide objInsert in objList)
                {
                    objInsert.NetRateInTotal = dec_netRateTotal;
                    objInsert.MarkUpPercentage = dec_markupvalue;
                    objInsert.GrossRateInTotal = dec_dtotal;
                    objInsert.Profit = dec_dtotal - dec_netRateTotal;
                    objInsert.Remark = txtTourGuide_Remark.Text.Trim();
                    objInsert.QuotationCurrency = txtTourGuide_QuotationCurrency.Text.Trim();
                    //objInsert.ConfirmationStatus  =  
                    if (objInsert.ID.IsNull)
                    {
                        objInsert.Insert();
                        MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                    else
                    {
                        objInsert.Update();
                        MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
                    }
                }
                BindTourGuide();
            }
            finally
            {

            }
        }
        #endregion

        #region OverView
     
        public void BindOverView()
        {
            // Get the Procedure call and get the details for Overview Form here
            txtOverView_NetRatePerPerson.Text = "";
            txtOverView_NetRateInTotal.Text = "";
            txtOverView_MarkUpPercentage.Text = this.MarkUpPercentage.ToString();
            txtOverView_GrossRatePerPerson.Text = "";
            txtOverView_GrossRateInTotal.Text = "";
            txtOverView_Profit.Text = "";
            txtOverView_ProfitPercentage.Text = "";
            txtOverView_SingleRoomSupplementPerPerson.Text = "";


            txtOverview_Remark.Text = "";
            if (TravelSession.Current.QuotationPriceId.HasValue)
            {
                DLQuotationPrice_Overview obj = new DLQuotationPrice_Overview();
                obj.QuotationPriceId = TravelSession.Current.QuotationPriceId.Value;
               DataSet ds = obj.SelectAllTotal();
                 
                decimal netRatePerPerson,netRateInTotal =0;
                foreach(DataTable dt in ds.Tables)
                {
                    // Here is calculate total from DataTables columns
                    //netRatePerPerson += dt.Rows[0]["NetRateInTotal"];
                    //netRateInTotal += dt.Rows[0]["NetRateInTotal"];
                }
                 
            }
        } 
        #endregion
         
        public bool IsReadOnly
        {
            get
            {
                if (ViewState["isViewOnly"] != null)
                {
                    return Convert.ToBoolean(ViewState["isViewOnly"]);
                }
                else
                    return false;
            }
            set
            {
                ViewState["isViewOnly"] = value;
            }
        }
    }
}