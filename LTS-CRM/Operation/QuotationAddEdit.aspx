﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuotationAddEdit.aspx.cs" Inherits="LTS_CRM.Operation.QuotationAddEdit" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" %>

<%@ Register Src="~/Operation/QuotationBasicDeatils.ascx" TagName="Basic" TagPrefix="uc" %>
<%@ Register Src="~/Operation/QuotationBasicEntry.ascx" TagName="BasicTable" TagPrefix="uc" %>
<%@ Register Src="~/Operation/QuotationSendToClient.ascx" TagName="SendConfirmation" TagPrefix="uc" %>
<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .subnav > li > a {
            padding: 7px 5px 6px;
            font-size: 12px;
            margin: 0px 5px;
            width: 100%;
            display: flex;
        }
    </style>
    <script>
        function doPostBack_Overview() {
            __doPostBack('ctl00$ContentPlaceHolder1$lnkParent', '');
            return false;
        }
    </script>
    <asp:LinkButton ID="lnkParent" ClientIDMode="Static" OnClick="lnkParent_Click" Style="display: none" runat="server"></asp:LinkButton>
    <div class="well">
        <div id="ribbon">
            <ol class="breadcrumb">
                <li>Quotation Management</li>
            </ol>
        </div>
        <section id="Section1">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                        <header>
                            <h2>Quotation List</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                                &nbsp;
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar1">
                                    <ul class="nav nav-tabs tabs-left" id="client-nav">

                                        <li class="bg-color-blueDark txt-color-white active" id="tab1" runat="server">
                                            <a data-toggle="tab" href="#tab1_pan">Basic Information</a></li>
                                        <li class="bg-color-blueDark txt-color-white" id="tab2" runat="server">
                                            <a data-toggle="tab" href="#tab2_pan">Quotation Iternary</a></li>
                                        <li class="bg-color-blueDark txt-color-white" id="tab3" runat="server">
                                            <a data-toggle="tab" href="#tab3_pan">Quotation Price</a></li>
                                        <li class="bg-color-blueDark txt-color-white" id="tab4" runat="server">
                                            <a data-toggle="tab" href="#tab4_pan">Send Quotation to Client</a></li>
                                        <li class="bg-color-blueDark txt-color-white" id="tab5" runat="server">
                                            <a data-toggle="tab" href="#tab5_pan">Upload related documents</a></li>
                                        <%--  <li class="bg-color-blueDark txt-color-white" id="tab6" runat="server">
                                                <a data-toggle="tab" href="#tab6_pan">History</a></li>--%>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab1_pan" runat="server">
                                            <div class="row">
                                                <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                    <div class="input-group">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="OperationQuotationform" class="row smart-form">
                                                    <fieldset>
                                                        <%--<h3 class="caption">Basic Information</h3>--%>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Company
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addOperationQuotation"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvcboCompanyID" InitialValue="0" ControlToValidate="cboCompanyID" ValidationGroup="addOperationQuotation" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Tour Code
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtTourCode" runat="server" placeholder="Tour Code" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvtxtTourCode" ControlToValidate="txtTourCode" ValidationGroup="addOperationQuotation" runat="server" ErrorMessage="Tour Code Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Staff
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList ID="ddlStaff" runat="server" ValidationGroup="addOperationQuotation" class="form-control"></asp:DropDownList>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <h3 class="caption">Clients</h3>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Inquiry Staff Name
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList ID="ddlInquiryStaffId" runat="server" ValidationGroup="addOperationQuotation" class="form-control"></asp:DropDownList>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Company/Individual Name
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList ID="ddlCompanyIndividual" runat="server" ValidationGroup="addOperationQuotation" class="form-control"></asp:DropDownList>

                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    City
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtClient_City" runat="server" placeholder="City" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Country
                                                                </label>
                                                                <label class="input">
                                                                    <select id="ddlClient_Country" class="form-control" runat="server">
                                                                        <option value="Afghanistan">Afghanistan</option>
                                                                        <option value="Albania">Albania</option>
                                                                        <option value="Algeria">Algeria</option>
                                                                        <option value="American Samoa">American Samoa</option>
                                                                        <option value="Andorra">Andorra</option>
                                                                        <option value="Angola">Angola</option>
                                                                        <option value="Anguilla">Anguilla</option>
                                                                        <option value="Antartica">Antarctica</option>
                                                                        <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                        <option value="Argentina">Argentina</option>
                                                                        <option value="Armenia">Armenia</option>
                                                                        <option value="Aruba">Aruba</option>
                                                                        <option value="Australia">Australia</option>
                                                                        <option value="Austria">Austria</option>
                                                                        <option value="Azerbaijan">Azerbaijan</option>
                                                                        <option value="Bahamas">Bahamas</option>
                                                                        <option value="Bahrain">Bahrain</option>
                                                                        <option value="Bangladesh">Bangladesh</option>
                                                                        <option value="Barbados">Barbados</option>
                                                                        <option value="Belarus">Belarus</option>
                                                                        <option value="Belgium">Belgium</option>
                                                                        <option value="Belize">Belize</option>
                                                                        <option value="Benin">Benin</option>
                                                                        <option value="Bermuda">Bermuda</option>
                                                                        <option value="Bhutan">Bhutan</option>
                                                                        <option value="Bolivia">Bolivia</option>
                                                                        <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                        <option value="Botswana">Botswana</option>
                                                                        <option value="Bouvet Island">Bouvet Island</option>
                                                                        <option value="Brazil">Brazil</option>
                                                                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                        <option value="Bulgaria">Bulgaria</option>
                                                                        <option value="Burkina Faso">Burkina Faso</option>
                                                                        <option value="Burundi">Burundi</option>
                                                                        <option value="Cambodia">Cambodia</option>
                                                                        <option value="Cameroon">Cameroon</option>
                                                                        <option value="Canada">Canada</option>
                                                                        <option value="Cape Verde">Cape Verde</option>
                                                                        <option value="Cayman Islands">Cayman Islands</option>
                                                                        <option value="Central African Republic">Central African Republic</option>
                                                                        <option value="Chad">Chad</option>
                                                                        <option value="Chile">Chile</option>
                                                                        <option value="China">China</option>
                                                                        <option value="Christmas Island">Christmas Island</option>
                                                                        <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                        <option value="Colombia">Colombia</option>
                                                                        <option value="Comoros">Comoros</option>
                                                                        <option value="Congo">Congo</option>
                                                                        <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                        <option value="Cook Islands">Cook Islands</option>
                                                                        <option value="Costa Rica">Costa Rica</option>
                                                                        <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                        <option value="Croatia">Croatia (Hrvatska)</option>
                                                                        <option value="Cuba">Cuba</option>
                                                                        <option value="Cyprus">Cyprus</option>
                                                                        <option value="Czech Republic">Czech Republic</option>
                                                                        <option value="Denmark">Denmark</option>
                                                                        <option value="Djibouti">Djibouti</option>
                                                                        <option value="Dominica">Dominica</option>
                                                                        <option value="Dominican Republic">Dominican Republic</option>
                                                                        <option value="East Timor">East Timor</option>
                                                                        <option value="Ecuador">Ecuador</option>
                                                                        <option value="Egypt">Egypt</option>
                                                                        <option value="El Salvador">El Salvador</option>
                                                                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                        <option value="Eritrea">Eritrea</option>
                                                                        <option value="Estonia">Estonia</option>
                                                                        <option value="Ethiopia">Ethiopia</option>
                                                                        <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                        <option value="Faroe Islands">Faroe Islands</option>
                                                                        <option value="Fiji">Fiji</option>
                                                                        <option value="Finland">Finland</option>
                                                                        <option value="France">France</option>
                                                                        <option value="France Metropolitan">France, Metropolitan</option>
                                                                        <option value="French Guiana">French Guiana</option>
                                                                        <option value="French Polynesia">French Polynesia</option>
                                                                        <option value="French Southern Territories">French Southern Territories</option>
                                                                        <option value="Gabon">Gabon</option>
                                                                        <option value="Gambia">Gambia</option>
                                                                        <option value="Georgia">Georgia</option>
                                                                        <option value="Germany">Germany</option>
                                                                        <option value="Ghana">Ghana</option>
                                                                        <option value="Gibraltar">Gibraltar</option>
                                                                        <option value="Greece">Greece</option>
                                                                        <option value="Greenland">Greenland</option>
                                                                        <option value="Grenada">Grenada</option>
                                                                        <option value="Guadeloupe">Guadeloupe</option>
                                                                        <option value="Guam">Guam</option>
                                                                        <option value="Guatemala">Guatemala</option>
                                                                        <option value="Guinea">Guinea</option>
                                                                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                        <option value="Guyana">Guyana</option>
                                                                        <option value="Haiti">Haiti</option>
                                                                        <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                        <option value="Holy See">Holy See (Vatican City State)</option>
                                                                        <option value="Honduras">Honduras</option>
                                                                        <option value="Hong Kong">Hong Kong</option>
                                                                        <option value="Hungary">Hungary</option>
                                                                        <option value="Iceland">Iceland</option>
                                                                        <option value="India">India</option>
                                                                        <option value="Indonesia">Indonesia</option>
                                                                        <option value="Iran">Iran (Islamic Republic of)</option>
                                                                        <option value="Iraq">Iraq</option>
                                                                        <option value="Ireland">Ireland</option>
                                                                        <option value="Israel">Israel</option>
                                                                        <option value="Italy">Italy</option>
                                                                        <option value="Jamaica">Jamaica</option>
                                                                        <option value="Japan">Japan</option>
                                                                        <option value="Jordan">Jordan</option>
                                                                        <option value="Kazakhstan">Kazakhstan</option>
                                                                        <option value="Kenya">Kenya</option>
                                                                        <option value="Kiribati">Kiribati</option>
                                                                        <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                        <option value="Korea">Korea, Republic of</option>
                                                                        <option value="Kuwait">Kuwait</option>
                                                                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                        <option value="Lao">Lao People's Democratic Republic</option>
                                                                        <option value="Latvia">Latvia</option>
                                                                        <option value="Lebanon">Lebanon</option>
                                                                        <option value="Lesotho">Lesotho</option>
                                                                        <option value="Liberia">Liberia</option>
                                                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                        <option value="Liechtenstein">Liechtenstein</option>
                                                                        <option value="Lithuania">Lithuania</option>
                                                                        <option value="Luxembourg">Luxembourg</option>
                                                                        <option value="Macau">Macau</option>
                                                                        <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                        <option value="Madagascar">Madagascar</option>
                                                                        <option value="Malawi">Malawi</option>
                                                                        <option value="Malaysia">Malaysia</option>
                                                                        <option value="Maldives">Maldives</option>
                                                                        <option value="Mali">Mali</option>
                                                                        <option value="Malta">Malta</option>
                                                                        <option value="Marshall Islands">Marshall Islands</option>
                                                                        <option value="Martinique">Martinique</option>
                                                                        <option value="Mauritania">Mauritania</option>
                                                                        <option value="Mauritius">Mauritius</option>
                                                                        <option value="Mayotte">Mayotte</option>
                                                                        <option value="Mexico">Mexico</option>
                                                                        <option value="Micronesia">Micronesia, Federated States of</option>
                                                                        <option value="Moldova">Moldova, Republic of</option>
                                                                        <option value="Monaco">Monaco</option>
                                                                        <option value="Mongolia">Mongolia</option>
                                                                        <option value="Montserrat">Montserrat</option>
                                                                        <option value="Morocco">Morocco</option>
                                                                        <option value="Mozambique">Mozambique</option>
                                                                        <option value="Myanmar">Myanmar</option>
                                                                        <option value="Namibia">Namibia</option>
                                                                        <option value="Nauru">Nauru</option>
                                                                        <option value="Nepal">Nepal</option>
                                                                        <option value="Netherlands">Netherlands</option>
                                                                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                        <option value="New Caledonia">New Caledonia</option>
                                                                        <option value="New Zealand">New Zealand</option>
                                                                        <option value="Nicaragua">Nicaragua</option>
                                                                        <option value="Niger">Niger</option>
                                                                        <option value="Nigeria">Nigeria</option>
                                                                        <option value="Niue">Niue</option>
                                                                        <option value="Norfolk Island">Norfolk Island</option>
                                                                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                        <option value="Norway">Norway</option>
                                                                        <option value="Oman">Oman</option>
                                                                        <option value="Pakistan">Pakistan</option>
                                                                        <option value="Palau">Palau</option>
                                                                        <option value="Panama">Panama</option>
                                                                        <option value="Papua New Guinea">Papua New Guinea</option>
                                                                        <option value="Paraguay">Paraguay</option>
                                                                        <option value="Peru">Peru</option>
                                                                        <option value="Philippines">Philippines</option>
                                                                        <option value="Pitcairn">Pitcairn</option>
                                                                        <option value="Poland">Poland</option>
                                                                        <option value="Portugal">Portugal</option>
                                                                        <option value="Puerto Rico">Puerto Rico</option>
                                                                        <option value="Qatar">Qatar</option>
                                                                        <option value="Reunion">Reunion</option>
                                                                        <option value="Romania">Romania</option>
                                                                        <option value="Russia">Russian Federation</option>
                                                                        <option value="Rwanda">Rwanda</option>
                                                                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                        <option value="Saint LUCIA">Saint LUCIA</option>
                                                                        <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                        <option value="Samoa">Samoa</option>
                                                                        <option value="San Marino">San Marino</option>
                                                                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                        <option value="Saudi Arabia">Saudi Arabia</option>
                                                                        <option value="Senegal">Senegal</option>
                                                                        <option value="Seychelles">Seychelles</option>
                                                                        <option value="Sierra">Sierra Leone</option>
                                                                        <option value="Singapore">Singapore</option>
                                                                        <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                        <option value="Slovenia">Slovenia</option>
                                                                        <option value="Solomon Islands">Solomon Islands</option>
                                                                        <option value="Somalia">Somalia</option>
                                                                        <option value="South Africa">South Africa</option>
                                                                        <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                        <option value="Span">Spain</option>
                                                                        <option value="SriLanka">Sri Lanka</option>
                                                                        <option value="St. Helena">St. Helena</option>
                                                                        <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                        <option value="Sudan">Sudan</option>
                                                                        <option value="Suriname">Suriname</option>
                                                                        <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                        <option value="Swaziland">Swaziland</option>
                                                                        <option value="Sweden">Sweden</option>
                                                                        <option value="Switzerland">Switzerland</option>
                                                                        <option value="Syria">Syrian Arab Republic</option>
                                                                        <option value="Taiwan">Taiwan, Province of China</option>
                                                                        <option value="Tajikistan">Tajikistan</option>
                                                                        <option value="Tanzania">Tanzania, United Republic of</option>
                                                                        <option value="Thailand">Thailand</option>
                                                                        <option value="Togo">Togo</option>
                                                                        <option value="Tokelau">Tokelau</option>
                                                                        <option value="Tonga">Tonga</option>
                                                                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                        <option value="Tunisia">Tunisia</option>
                                                                        <option value="Turkey">Turkey</option>
                                                                        <option value="Turkmenistan">Turkmenistan</option>
                                                                        <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                        <option value="Tuvalu">Tuvalu</option>
                                                                        <option value="Uganda">Uganda</option>
                                                                        <option value="Ukraine">Ukraine</option>
                                                                        <option value="United Arab Emirates">United Arab Emirates</option>
                                                                        <option value="United Kingdom" selected>United Kingdom</option>
                                                                        <option value="United States">United States</option>
                                                                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                        <option value="Uruguay">Uruguay</option>
                                                                        <option value="Uzbekistan">Uzbekistan</option>
                                                                        <option value="Vanuatu">Vanuatu</option>
                                                                        <option value="Venezuela">Venezuela</option>
                                                                        <option value="Vietnam">Viet Nam</option>
                                                                        <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                        <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                        <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                        <option value="Western Sahara">Western Sahara</option>
                                                                        <option value="Yemen">Yemen</option>
                                                                        <option value="Yugoslavia">Yugoslavia</option>
                                                                        <option value="Zambia">Zambia</option>
                                                                        <option value="Zimbabwe">Zimbabwe</option>
                                                                    </select>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Telephone
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtClient_Telephone" data-mask="(999) 999-99999" runat="server" placeholder="Telephone" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Mobile Number
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtClient_MobileNumber" data-mask="(999) 999-99999" runat="server" placeholder="Mobile Number" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Fax
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtClient_Fax" runat="server" placeholder="Fax" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    We Chat
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtClient_WeChat" runat="server" placeholder="WeChat" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Email
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtClient_Email" runat="server" TextMode="Email" placeholder="Email" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <h3 class="caption">Sales</h3>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Sales
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList ID="ddlSalesId" runat="server" ValidationGroup="addOperationQuotation" class="form-control"></asp:DropDownList>
                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtSalesId" ValidationGroup="addOperationQuotation" ControlToValidate="ddlSalesId" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Quotation Staff
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList ID="ddlQuotationStaff" runat="server" ValidationGroup="addOperationQuotation" class="form-control"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvddlQuotationStaff" InitialValue="0" ControlToValidate="ddlQuotationStaff" ValidationGroup="addOperationQuotation" runat="server" ErrorMessage="Quotation staff Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Department
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList ID="ddlDepartmentID" runat="server" ValidationGroup="addOperationQuotation" class="form-control"></asp:DropDownList>
                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtDepartmentID" ValidationGroup="addOperationQuotation" ControlToValidate="ddlDepartmentID" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    City
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtOffice_City" runat="server" placeholder="City" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Telephone
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtOffice_Telephone" data-mask="(999) 999-99999" runat="server" placeholder="Telephone" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Mobile
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtOffice_MobileNumber" data-mask="(999) 999-99999" runat="server" placeholder="Mobile Number" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Fax
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtOffice_Fax" runat="server" placeholder="Fax" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    We Chat
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtOffice_WeChat" runat="server" placeholder="WeChat" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Email
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtOffice_Email" TextMode="Email" runat="server" placeholder="Email" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="row">
                                                        </div>
                                                        <h3 class="caption">Tour Information</h3>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Tour Start Date
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtTourStartDate" runat="server" placeholder="Tour Start Date" ValidationGroup="addOperationQuotation" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Tour End Date
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtTourEndDate" runat="server" placeholder="Tour End Date" ValidationGroup="addOperationQuotation" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    No.Of Service Day
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtNoOfServiceDay" TextMode="Number" runat="server" placeholder="No.Of Service Day" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="refvtxtNoOfServiceDay" ValidationGroup="addOperationQuotation" ControlToValidate="txtNoOfServiceDay" runat="server" Text="No. Of service day required." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtNoOfServiceDay" ValidationGroup="addOperationQuotation" ControlToValidate="txtNoOfServiceDay" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                </label>
                                                            </section>
                                                            <%--  <section class="col col-3">
                                                                    <label class="label">
                                                                        Total No. Of Clients
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtTotalClient" TextMode="Number" runat="server" placeholder="Total Client" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rfvtxtTotalClient" ValidationGroup="addOperationQuotation" ControlToValidate="txtNoOfServiceDay" runat="server" Text="Total client is required." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtTotalClient" ValidationGroup="addOperationQuotation" ControlToValidate="txtTotalClient" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>--%>
                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    No. Of Adults
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtNoOfAdults" TextMode="Number" runat="server" placeholder="No. Of Adults" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtNoOfAdults" ValidationGroup="addOperationQuotation" ControlToValidate="txtNoOfAdults" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    No. Of Child (5-12 years old)
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtNoOfChild5To12" runat="server" TextMode="Number" placeholder="No. Of Child ( 5-12 years old)" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="RegularExpressionValidator1" ValidationGroup="addOperationQuotation" ControlToValidate="txtNoOfChild5To12" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    No. Of Child (2-5 years old)
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtNoOfChild2To5" runat="server" TextMode="Number" placeholder="No. Of Child ( 2-5 years old)" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtNoOfChild" ValidationGroup="addOperationQuotation" ControlToValidate="txtNoOfChild2To5" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    No. Of Infant ( under 2 Year old )
                                                                </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtNoOfInfant" runat="server" TextMode="Number" placeholder="No. Of Infants" ValidationGroup="addOperationQuotation" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtNoOfInfant" ValidationGroup="addOperationQuotation" ControlToValidate="txtNoOfInfant" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-6">
                                                                <label class="label">
                                                                    Comments
                                                                </label>
                                                                <label class="input">
                                                                    <CKEditor:CKEditorControl ID="txtComments" ValidationGroup="addOperationQuotation" BasePath="~/ckeditor" runat="server" Height="100" Width="500" ToolbarStartupExpanded="False">
                                                                    </CKEditor:CKEditorControl>
                                                                </label>
                                                            </section>
                                                            <section class="col col-6">
                                                                <label class="label">
                                                                    Message To Client
                                                                </label>
                                                                <label class="input">
                                                                    <CKEditor:CKEditorControl ID="txtMessageToClient" ValidationGroup="addOperationQuotation" BasePath="~/ckeditor" runat="server" Height="100" Width="500" ToolbarStartupExpanded="False">
                                                                    </CKEditor:CKEditorControl>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="row" id="pnlStatus" runat="server">
                                                            <section class="col col-3">
                                                                <label class="label">
                                                                    Quotation Status
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList ID="ddlQuotationStatus" runat="server" ValidationGroup="addOperationQuotation" class="form-control"></asp:DropDownList>
                                                                </label>
                                                            </section>
                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-12">
                                                                <label class="label">
                                                                    Quotation Report
                                                                </label>
                                                                <label class="input">
                                                                    <CKEditor:CKEditorControl ID="txtQuotationReport" ValidationGroup="addOperationQuotation" BasePath="~/ckeditor" runat="server" Height="100" Width="700" ToolbarStartupExpanded="False">
                                                                    </CKEditor:CKEditorControl>
                                                                </label>
                                                            </section>
                                                        </div>
                                                    </fieldset>
                                                    <footer>
                                                        <button id="btnOperationQuotationSave" runat="server" onserverclick="btnOperationQuotationSave_ServerClick" validationgroup="addOperationQuotation" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                            <span style="width: 80px">Save </span>
                                                        </button>
                                                        <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="javascript: window.location.href = 'Quotation.aspx'" />
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab2_pan" runat="server">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="OperationQuotationIternary" class="row smart-form" runat="server"  >
                                                    <fieldset>
                                                        <div class="row">
                                                            <section class="col col-12">
                                                                <label class="label">
                                                                    Choose Iternary
                                                                </label>
                                                                <label class="input">
                                                                    <asp:DropDownList class="form-control" ID="ddlIternary" AutoPostBack="true" OnSelectedIndexChanged="ddlIternary_SelectedIndexChanged" runat="server" ValidationGroup="additernary"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvddlIternary" ControlToValidate="ddlIternary" ValidationGroup="additernary" runat="server" ErrorMessage="Choose iternary" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                             
                                                                </label>
                                                            </section>
                                                            <section class="col col-12">
                                                                <label class="label">
                                                                    Quotation Iternary
                                                                </label>
                                                                <label class="input">
                                                                    <CKEditor:CKEditorControl ID="quotationiternary" ValidationGroup="additernary" BasePath="~/ckeditor" runat="server" Height="100" Width="450" ToolbarStartupExpanded="False">
                                                                    </CKEditor:CKEditorControl>
                                                                </label>
                                                            </section>
                                                        </div>
                                                    </fieldset>
                                                    <footer>
                                                        <button id="Button1" runat="server" validationgroup="addOperationQuotation" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                            <span style="width: 80px">Save </span>
                                                        </button>
                                                        <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab3_pan" runat="server" visible="false">
                                            <div class="clearfix">&nbsp;</div>
                                            <div class="">
                                                <div class="tabs-left1 dista">
                                                    <ul class="nav nav-tabs tabs-left dista_cell subnav" id="nav">
                                                        <li class="bg-color-blueDark txt-color-white active" id="subtab1" runat="server">
                                                            <a data-toggle="tab" href="#subtab1_pan">Main</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab2" runat="server">
                                                            <a data-toggle="tab" href="#subtab2_pan">Basic Detail </a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab3" runat="server">
                                                            <a data-toggle="tab" href="#subtab3_pan">Hotel Details</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab4" runat="server">
                                                            <a data-toggle="tab" href="#subtab4_pan">Driver & Guide</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab5" runat="server">
                                                            <a data-toggle="tab" href="#subtab5_pan">Coach Details</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab6" runat="server">
                                                            <a data-toggle="tab" href="#subtab6_pan">Van Hire</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab7" runat="server">
                                                            <a data-toggle="tab" href="#subtab7_pan">Tour Guide</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab8" runat="server">
                                                            <a data-toggle="tab" href="#subtab8_pan">Lunch</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab9" runat="server">
                                                            <a data-toggle="tab" href="#subtab9_pan">Dinner</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab10" runat="server">
                                                            <a data-toggle="tab" href="#subtab10_pan">Tips</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab11" runat="server">
                                                            <a data-toggle="tab" href="#subtab11_pan">Enterenace</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab12" runat="server">
                                                            <a data-toggle="tab" href="#subtab12_pan">Other Services</a></li>
                                                        <li class="bg-color-blueDark txt-color-white" id="subtab13" runat="server">
                                                            <a data-toggle="tab" onclick="return doPostBack_Overview();" href="#subtab13_pan">Overview</a></li>
                                                    </ul>
                                                    <div class="tab-content dista_cell">
                                                        <div class="tab-pane active" id="subtab1_pan" runat="server">
                                                            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                                <div id="QuotationPriceform" class="row smart-form">
                                                                    <fieldset>
                                                                        <div class="row">
                                                                            <section class="col col-3">
                                                                                <label class="label">
                                                                                    Tour Code
                                                                                </label>
                                                                                <label class="input">
                                                                                    <asp:TextBox ID="txtQP_TourCode" runat="server" placeholder="Tour Code" ValidationGroup="addQuotationPrice" Text="" class="form-control"></asp:TextBox>
                                                                                </label>
                                                                            </section>
                                                                            <section class="col col-3">
                                                                                <label class="label">
                                                                                    Quotation Staff
                                                                                </label>
                                                                                <label class="input">
                                                                                    <asp:DropDownList ID="ddlQuotationPriceStaff" runat="server" ValidationGroup="addQuotationPrice" class="form-control"></asp:DropDownList>
                                                                                </label>
                                                                            </section>
                                                                        </div>
                                                                        <div class="row">
                                                                            <section class="col col-3">
                                                                                <label class="label">
                                                                                    Quotation Date
                                                                                </label>
                                                                                <label class="input">
                                                                                    <asp:TextBox ID="txtQuotationPriceDate" runat="server" placeholder="Quotation Date" ValidationGroup="addQuotationPrice" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                                </label>
                                                                            </section>
                                                                            <section class="col col-3">
                                                                                <label class="label">
                                                                                    No.Of People
                                                                                </label>
                                                                                <label class="input">
                                                                                    <asp:TextBox ID="txtQP_NoOfPeople" runat="server" placeholder="No. Of People" ValidationGroup="addQuotationPrice" Text="" class="form-control"></asp:TextBox>
                                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtNoOfPeople" ValidationGroup="addQuotationPrice" ControlToValidate="txtQP_NoOfPeople" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                                </label>
                                                                            </section>
                                                                            <section class="col col-3">
                                                                                <label class="label">
                                                                                    Confirmation
                                                                                </label>
                                                                                <label class="input">
                                                                                    <asp:TextBox ID="txtQP_Confirmation" runat="server" placeholder="Confirmation" ValidationGroup="addQuotationPrice" Text="" class="form-control"></asp:TextBox>
                                                                                </label>
                                                                            </section>
                                                                            <section class="col col-3">
                                                                                <label class="label">
                                                                                    Quotation Currency
                                                                                </label>
                                                                                <label class="input">
                                                                                    <asp:TextBox ID="txtQP_Currency" runat="server" placeholder="Quotation Currency" ValidationGroup="addQuotationPrice" Text="" class="form-control"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rev" ValidationGroup="addQuotationPrice" ControlToValidate="txtQP_Currency" runat="server" Text="required field" ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RequiredFieldValidator>

                                                                                </label>
                                                                            </section>
                                                                        </div>
                                                                        <div class="row">
                                                                            <section class="col col-3">
                                                                                <label class="label">
                                                                                    Markup Percentage
                                                                                </label>
                                                                                <label class="input">
                                                                                    <asp:TextBox ID="txtQP_MarkupPercentage" runat="server" placeholder="Markup Percentage" ValidationGroup="addQuotationPrice" Text="" class="form-control"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rfvtxtQP_MarkupPercentage" ValidationGroup="addQuotationPrice" ControlToValidate="txtQP_MarkupPercentage" runat="server" Text="required field" ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtMarkupPercentage" ValidationGroup="addQuotationPrice" ControlToValidate="txtQP_MarkupPercentage" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                                </label>
                                                                            </section>
                                                                        </div>
                                                                    </fieldset>
                                                                    <footer>
                                                                        <button id="btnQuotationPriceSave" onserverclick="btnQuotationPriceSave_ServerClick" runat="server" validationgroup="addQuotationPrice" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                            <span style="width: 80px">Save</span>
                                                                        </button>
                                                                        <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                                                    </footer>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <%--Basic Details--%>
                                                        <div class="tab-pane" id="subtab2_pan" runat="server">
                                                            <div class="col-xs-12 col-sm-10 col-md-11 col-lg-11">
                                                                <div id="QuotationPrice_Basicform" class="row smart-form">
                                                                    <uc:BasicTable runat="server" ID="ucBasicEntry"></uc:BasicTable>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab3_pan" runat="server" visible="false">
                                                            <%-- <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10"> --%>
                                                            <uc:Basic runat="server" QuotationType="Hotel" ID="ucBasicHotel"></uc:Basic>
                                                            <%--</div>--%>
                                                        </div>
                                                        <div class="tab-pane" id="subtab4_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                                <uc:Basic runat="server" QuotationType="DriverAndGuide" ID="ucDriverGuide"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab5_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="Coach" ID="ucCoach"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab6_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="VanHire" ID="ucVanHire"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab7_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="TourGuide" ID="ucTourGuide"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab8_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="Lunch" ID="ucLunch"></uc:Basic>
                                                            </div>

                                                        </div>
                                                        <div class="tab-pane" id="subtab9_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="Dinner" ID="ucDinner"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab10_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="Tips" ID="ucTips"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab11_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="Entrance" ID="ucEnterance"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab12_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="OtherService" ID="ucOthers"></uc:Basic>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="subtab13_pan" runat="server" visible="false">
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xauto">
                                                                <uc:Basic runat="server" QuotationType="Overview" ID="ucOverView"></uc:Basic>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--<iframe id="frm" src="QuotationPrice.aspx" height="990" width="100%" style="overflow-x:hidden;overflow-y:auto"></iframe>--%>
                                            </div>
                                        </div>

                                        
                                        <div class="tab-pane" id="tab4_pan" runat="server">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="row">
                                                    <uc:SendConfirmation runat="server" id="UcConfirmation"></uc:SendConfirmation>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="tab-pane" id="tab5_pan" runat="server">


                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <br />
                                                    <br />

                                                    <div class="row">
                                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                            <div class="input-group">
                                                                <asp:FileUpload class="btn btn-default" ID="flDocumentUpload" runat="server" />

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-9 col-md-9 col-lg-9 text-left">

                                                            <a id="lnkQuotationUpload" class="btn bg-color-blueDark txt-color-white" href="javascript:void" onserverclick="lnkQuotationUpload_ServerClick" runat="server">
                                                                <i class="fa fa-plus"></i><span class="hidden-mobile">Upload </span>
                                                            </a>

                                                        </div>

                                                    </div>
                                                    <br />
                                                    <br />
                                                    <asp:Repeater ID="MyRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                        <HeaderTemplate>
                                                            <table id="datatable_fixed_column" class="table " width="100%">

                                                                <thead>

                                                                    <tr>
                                                                        <th data-hide="phone">No</th>
                                                                        <th data-hide="phone">Document Name</th>


                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "No")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "DocumentName")%></td>


                                                                <td>
                                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="btnDocEdit" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnDocEdit_Click"><i class="fa fa-eye"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="btnDocDelete" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnDocDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                </td>

                                                            </tr>

                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <br />
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </article>
            </div>
        </section>

        <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">
                            <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
