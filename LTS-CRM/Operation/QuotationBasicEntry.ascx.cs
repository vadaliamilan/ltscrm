﻿using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace LTS_CRM.Operation
{
    public partial class QuotationBasicEntry : LTSUserControl
    {
        #region Properties
        public bool IsReadOnly
        {
            get
            {
                if (ViewState["isViewOnly"] != null)
                {
                    return Convert.ToBoolean(ViewState["isViewOnly"]);
                }
                else
                    return false;
            }
            set
            {
                ViewState["isViewOnly"] = value;
            }
        }
        /// <summary>
        /// NoOfDay
        /// </summary>
        public int NoOfDays
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["noOfDays"])))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(ViewState["noOfDays"]);
                }
            }
            set
            {
                ViewState["noOfDays"] = value;
            }
        }
        public Dictionary<int, int> quotationBasics
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_quotationBasics"])))
                {
                    return null;
                }
                else
                {
                    return ViewState["_quotationBasics"] as Dictionary<int, int>;
                }
            }
            set
            {
                ViewState["_quotationBasics"] = value;
            }
        }

        public int quotationId
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_quotationId"])))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(ViewState["_quotationId"]);
                }
            }
            set
            {
                ViewState["_quotationId"] = value;
            }
        }
        public int quotationPriceId
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_quotationPriceId"])))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(ViewState["_quotationPriceId"]);
                }
            }
            set
            {
                ViewState["_quotationPriceId"] = value;
            }
        }
        public DataTable quotationData
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_quotationData"])))
                {
                    return new DataTable();
                }
                else
                {
                    return ((DataTable)(ViewState["_quotationData"]));
                }
            }
            set
            {
                ViewState["_quotationData"] = value;
            }
        }
        public string quotationCurrency
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_quotationCurrency"])))
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(ViewState["_quotationCurrency"]);
                }
            }
            set
            {
                ViewState["_quotationCurrency"] = value;
            }
        }
        public string markupPercentage
        {
            get
            {
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["_markupPercentage"])))
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(ViewState["_markupPercentage"]);
                }
            }
            set
            {
                ViewState["_markupPercentage"] = value;
            }
        }


        #endregion
        public delegate void Save_BasicDetails(object sender,bool isUpdated);
        public event Save_BasicDetails SaveBasicDelegate;
        public bool CallFromParent { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (this.Visible)
                {
                    BindQuotationPriceMain();
                }

                buttonSaveBasicQuote.Visible = !IsReadOnly;
            }
            if (this.Visible && CallFromParent == false)
            {
                BindQuotationPriceMain();
                CreateBasicDetails();
            }

        }
        

        public void BindQuotationPriceMain()
        {
            //Bind Transaction
            this.quotationId = TravelSession.Current.QuotationId.Value;
            this.Visible = true;     

            LTSCRM_DL.DLOperationQuotation obj = BLL.BLOperationQuotations.SelectQuotationById(this.quotationId);
            this.NoOfDays = obj.NoOfServiceDay.Value;
            if (this.markupPercentage  !="")
                 txtucQP_MarkupPercentage.Text=this.markupPercentage ;
            if (this.quotationCurrency !="")
                txtucQuotationCurrency.Text = this.quotationCurrency;
            this.quotationPriceId = BLL.BLOperationQuotations.GetQuotationPriceID(this.quotationId);

            //Check if Basic element exists or not
            DataTable dtnew = new DataTable();
            if (this.quotationPriceId != 0)
            {
                this.quotationBasics = BLL.BLOperationQuotations.GetQuotationBasicDetailsPair(this.quotationPriceId, NoOfDays, ref dtnew);
                this.quotationData = dtnew;
            }
            else
            {
                if (this.quotationBasics!=null)
                //add 0 value if the basic details not saved to DB : first time..
                for (int i = 0; i < NoOfDays; i++)
                {
                    this.quotationBasics.Add(i, 0);
                }
            }
            if (this.quotationBasics!=null && this.quotationBasics.Count < NoOfDays)
            {
                for (int i = this.quotationBasics.Count; i < NoOfDays; i++)
                {
                    this.quotationBasics.Add(i, 0);
                }
            }
        }
        public void CreateBasicDetails()
        {
            #region Bind Basic
            if (NoOfDays != 0 && this.quotationBasics!=null)
            {
                #region Declaration
                StringBuilder str = new StringBuilder();
                HtmlTable tableContent = new HtmlTable();
                HtmlTableRow row = null;
                HtmlTableCell cell = null;
                DataTable dt = new DataTable();
                TextBox txt_date, txt_city;
                #endregion

                //load parameters
                tableContent.ID = "basic_data_grid";
                //setting table properties
                tableContent.Width = "100%";
                tableContent.Attributes.Add("class", "table table-striped table-bordered");
                tableContent.Style.Add("margin", "0");
                if (NoOfDays > 0)
                {
                    //foreach (Control ctrl in Page.Controls)
                    //{
                    //    if (Convert.ToString(ctrl.ID) == "basic_data_grid")
                    //    {
                    //        Page.Controls.Remove(ctrl);
                    //    }
                    //}

                    #region Header
                    row = new HtmlTableRow();

                    cell = new HtmlTableCell();
                    cell.InnerText = "No. Of Days";
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerText = "Date";
                    row.Cells.Add(cell);


                    cell = new HtmlTableCell();
                    cell.InnerText = "City";
                    row.Cells.Add(cell);

                    tableContent.Rows.Add(row);
                    #endregion
                    #region add dynamc controls
                    for (int i = 0; i < NoOfDays; i++)
                    {
                        row = new HtmlTableRow();

                        cell = new HtmlTableCell();
                        cell.InnerText = (i + 1).ToString();
                        row.Cells.Add(cell);

                        txt_date = new TextBox();
                        txt_date.ID = "txtQBasic" + quotationId + "_" + quotationPriceId + "date" + "_" + quotationBasics[i] + "_" + i.ToString();
                        txt_date.CssClass = "form-control datepicker";
                        txt_date.Attributes.Add("data-dateformat", "dd/mm/yy");
                         if (Request.Form[txt_date.ClientID] != null)
                        {
                            txt_date.Text = Request.Form[txt_date.ClientID].ToString();
                        }
                        cell = new HtmlTableCell();
                        cell.Controls.Add(txt_date);
                        row.Cells.Add(cell);

                        txt_city = new TextBox();
                        txt_city.ID = "txtQBasic" + quotationId + "_" + quotationPriceId + "city" + "_" + quotationBasics[i] + "_" + i.ToString();
                        txt_city.CssClass = "form-control";
                        if (Request.Form[txt_city.ClientID] != null)
                        {
                            txt_city.Text = Request.Form[txt_city.ClientID].ToString();
                        }
                        
                        cell = new HtmlTableCell();
                        cell.Controls.Add(txt_city);
                        row.Cells.Add(cell);

                        DataView dv = this.quotationData.DefaultView;
                        if (dv != null && quotationBasics[i] != 0)
                        {
                            dv.RowFilter = "ID= " + quotationBasics[i];
                            if (this.quotationData.Rows.Count >= i + 1 && dv.ToTable().Rows.Count > 0)
                            {
                                try
                                {
                                    DateTime dt1 = Convert.ToDateTime(dv[0].Row["TourDate"].ToString());
                                    txt_date.Text = dt1.ToString("dd'/'MM'/'yyyy");
                                }
                                finally { }
                                txt_city.Text = dv[0].Row["TourCity"].ToString();

                                if (!string.IsNullOrEmpty(Convert.ToString(dv[0].Row["QuotationCurrency"])))
                                {
                                    if (txtucQuotationCurrency.Text.Trim().Length==0)
                                    txtucQuotationCurrency.Text = Convert.ToString(dv[0].Row["QuotationCurrency"]);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(dv[0].Row["MarkupPercentage"])))
                                {
                                    if (txtucQP_MarkupPercentage.Text.Trim().Length == 0)
                                        txtucQP_MarkupPercentage.Text = Convert.ToString(dv[0].Row["MarkupPercentage"]);
                                }
                            }
                        }

                        tableContent.Rows.Add(row);
                    }
                    if (this.quotationData.Rows.Count == 0)
                    {
                        txtucQP_MarkupPercentage.Text = this.markupPercentage;
                        txtucQuotationCurrency.Text = this.quotationCurrency;
                    }
                    #endregion
                    tableContent.EnableViewState = true;
                    pnlBasicGridEntry.Controls.Add(tableContent);
                    ViewState["tbl"] = true;

                }
            }
            #endregion
        }
        #region comment
        //protected override object SaveViewState()
        //{
        //    object[] newViewState = new object[2];

        //    List<string> txtValues = new List<string>();
        //    var tbl = pnlBasicGridEntry.FindControl("basic_data_grid");
        //    if (tbl != null)

        //        foreach (HtmlTableRow row in tbl.Controls)
        //        {
        //            foreach (HtmlTableCell cell in row.Controls)
        //            {
        //                foreach (Control c in cell.Controls)
        //                {
        //                    if (c is TextBox)
        //                    {
        //                        txtValues.Add(((TextBox)c).Text);
        //                    }
        //                }
        //            }
        //        }

        //    newViewState[0] = txtValues.ToArray();
        //    newViewState[1] = base.SaveViewState();
        //    return newViewState;
        //}
        //protected override void LoadViewState(object savedState)
        //{
        //    //if we can identify the custom view state as defined in the override for SaveViewState
        //    if (savedState is object[] && ((object[])savedState).Length == 2 && ((object[])savedState)[0] is string[])
        //    {
        //        object[] newViewState = (object[])savedState;
        //        string[] txtValues = (string[])(newViewState[0]);
        //        if (txtValues.Length > 0)
        //        {
        //            //re-load tables
        //            CreateBasicDetails();
        //            int i = 0;
        //            var tbl = pnlBasicGridEntry.FindControl("basic_data_grid");
        //            if (tbl != null)
        //            {
        //                foreach (HtmlTableRow row in tbl.Controls)
        //                {
        //                    foreach (HtmlTableCell cell in row.Controls)
        //                    {
        //                        foreach (Control c in cell.Controls)
        //                        {
        //                            if (c is TextBox)
        //                            {
        //                                TextBox txt = (TextBox)c;
        //                                if (i < txtValues.Length)
        //                                {
        //                                    txt.Text = txtValues[i++].ToString();
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //        }
        //        //load the ViewState normally
        //        base.LoadViewState(newViewState[1]);
        //    }
        //    else
        //    {
        //        base.LoadViewState(savedState);
        //    }
        //} 
        #endregion
       
        /// <summary>
        ///  
        /// We have to update all transaction repeater control on this save so 
        /// we used delegate on page and use QuotationBasicDeatils.ascx delegate to rebind all to other controls..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void buttonSaveBasicQuote_Click(object sender, EventArgs e)
        {
           bool saved = false;
            //LTSCRM_DL.DLQuotationPrice_Basic obj = new LTSCRM_DL.DLQuotationPrice_Basic();
            //obj.QuotationPriceId = quotationPriceId;
           
            //obj.NoOfDay = NoOfDays;
            ////if (this.quotationCurrency.Trim().Length>0)

            //obj.QuotationCurrency = this.quotationCurrency;
            //if (txtucQuotationCurrency.Text.Trim().Length > 0)
            //{
            //    obj.QuotationCurrency = txtucQuotationCurrency.Text.Trim();
            //}
            //if (txtucQP_MarkupPercentage.Text.Trim().Length > 0)
            //{
            //    obj.MarkupPercentage = Convert.ToDecimal(txtucQP_MarkupPercentage.Text.Trim());
            //}

            var tbl = pnlBasicGridEntry.FindControl("basic_data_grid");
            if (tbl != null)
            {
                //foreach (TextBox textBox in pnlBasicGridEntry.Controls.OfType<TextBox>())
                //{
                //    MessageBox(textBox.ID + ": " + textBox.Text + "\\n", MessageType.success, MessageType.success.ToString());
                //}
                foreach (HtmlTableRow row in tbl.Controls)
                {
                    string _tourcity  ="";
                    DateTime? tourDate = null;
                    bool isAlreadyFound = false;
                    int basicId = 0;
                    foreach (HtmlTableCell cell in row.Controls)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is TextBox)
                            {
                                TextBox txt = (TextBox)c;
                                if (c.ID.Contains("txtQBasic" + quotationId + "_" + quotationPriceId + "date"))
                                {
                                    string resSupplierDate = txt.Text.Trim();
                                    if (resSupplierDate != "")
                                    {
                                        tourDate = DateTime.ParseExact(resSupplierDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                     
                                    //Here to get the pk of the basic details
                                    isAlreadyFound = IsAvailableInDb(c.ID);
                                    basicId = getPKByID(c.ID);
                                }
                                if (c.ID.Contains("txtQBasic" + quotationId + "_" + quotationPriceId + "city"))
                                {
                                    if (txt.Text!="")
                                    _tourcity = txt.Text;
                                }

                            }
                        }
                        if (_tourcity  !=""&&  tourDate.HasValue)
                        {
                            LTSCRM_DL.DLQuotationPrice_Basic obj = new LTSCRM_DL.DLQuotationPrice_Basic();
                            obj.QuotationPriceId = quotationPriceId;
                            
                            obj.NoOfDay = NoOfDays;
                            //if (this.quotationCurrency.Trim().Length>0)

                            obj.QuotationCurrency = this.quotationCurrency;
                            if (txtucQuotationCurrency.Text.Trim().Length > 0)
                            {
                                obj.QuotationCurrency = txtucQuotationCurrency.Text.Trim();
                            }
                            if (txtucQP_MarkupPercentage.Text.Trim().Length > 0)
                            {
                                obj.MarkupPercentage = Convert.ToDecimal(txtucQP_MarkupPercentage.Text.Trim());
                            }
                            if (isAlreadyFound && basicId != 0)
                            {
                                DLQuotationPrice_Basic objBasic = BLL.BLOperationQuotations.GetQuotationPriceBasicDetailsById(basicId);
                                //set the values
                                objBasic.NoOfDay = obj.NoOfDay;
                                objBasic.QuotationCurrency = obj.QuotationCurrency;
                                objBasic.MarkupPercentage = obj.MarkupPercentage;
                                objBasic.ConfirmationStatus = obj.ConfirmationStatus;
                                objBasic.TourCity = _tourcity;
                                objBasic.TourDate = tourDate.Value;
                                objBasic.UpdatedBy = TravelSession.Current.UserName;
                                objBasic.UpdatedDate = DateTime.Now;
                                BLL.BLOperationQuotations.UpdateQuotationPriceBasicDetails(objBasic);
                            }
                            else
                            {
                                obj.TourDate = tourDate.Value;
                                obj.TourCity = _tourcity;
                                obj.CreatedBy = TravelSession.Current.UserName;
                                obj.CreatedDate = DateTime.Now;
                                BLL.BLOperationQuotations.SaveQuotationPriceBasicDetails(obj);
                               
                                saved = true;
                            }
                        }
                    }
                }
            }

            //Call basic delegate to page and rebind all basiccontrol repeater binding to referen no. of day and date update to all transactions
            if (SaveBasicDelegate != null)
                SaveBasicDelegate(sender, true);
            if (saved)
                MessageBox(Messages.Updated.GetDiscription(), MessageType.success, MessageType.success.ToString());
            else
                MessageBox(Messages.Inserted.GetDiscription(), MessageType.success, MessageType.success.ToString());
         
            //re bind control
            //BindQuotationPriceMain();

            //pnlBasicGridEntry.Controls.Clear();
            //CreateBasicDetails();
        }

        private bool IsAvailableInDb(string referrer)
        {
            string basicId = referrer.Substring(referrer.Substring(0, referrer.LastIndexOf("_")).LastIndexOf("_") + 1);
            basicId = basicId.Split('_')[0];
            return basicId != "0";
        }
        private int getPKByID(string referrer)
        {
            string basicId = referrer.Substring(referrer.Substring(0, referrer.LastIndexOf("_")).LastIndexOf("_") + 1);
            int pk = Convert.ToInt32(basicId.Split('_')[0]);
            return pk;
        }
        protected void buttonClearBasicQuote_Click(object sender, EventArgs e)
        {

        }


        internal void clearControls()
        {
            pnlBasicGridEntry.Controls.Clear();
        }
    }
}