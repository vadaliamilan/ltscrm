﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Operation
{
    public partial class Quotation : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.IsReadonlyQuotation = false;
                BindGrid();
            }
        }

        private void BindGrid()
        {
            DataTable data = BLL.BLOperationQuotations.SelectAll();
            QuotationRepeater.DataSource = data;
            QuotationRepeater.DataBind();
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int Qid = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLOperationQuotations.Delete(Qid);
            if (isSuccess)
                BindGrid();
        }
        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            ClearTabCookies();
            ClearTabCookies(SubTabCookieName);

            TravelSession.Current.QuotationId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            TravelSession.Current.IsReadonlyQuotation = false;
            int i = BLL.BLOperationQuotations.GetQuotationPriceID(TravelSession.Current.QuotationId.Value);
            if (i != 0)
            {
                TravelSession.Current.QuotationPriceId = i;
            }
            Response.Redirect("QuotationAddEdit.aspx", false);
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.QuotationId = null;
            TravelSession.Current.QuotationPriceId = null;
            TravelSession.Current.QuotationPriceBasicId = null;
            Response.Redirect("QuotationAddEdit.aspx", false);
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            TravelSession.Current.IsReadonlyQuotation = true;
            
            TravelSession.Current.QuotationId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            int i = BLL.BLOperationQuotations.GetQuotationPriceID(TravelSession.Current.QuotationId.Value);
            if (i != 0)
            {
                TravelSession.Current.QuotationPriceId = i;
            }
            Response.Redirect("QuotationAddEdit.aspx", false);
        }

        protected void buttonConfirm_Click(object sender, EventArgs e)
        {
            int QuotationId = Convert.ToInt32((sender as LinkButton).CommandArgument);
         DLOperationQuotation obj=  BLL.BLOperationQuotations.SelectQuotationById(QuotationId);
         obj.QuotationStatus = "Confirmation";
         BLL.BLOperationQuotations.UpdateQuotation(obj);
        }
    }
}