﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuotationSendToClient.ascx.cs" Inherits="LTS_CRM.Operation.QuotationSendToClient" %>

<section id="SentList">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="QuotationConfirmationToClientform" class="row smart-form">
                <fieldset>
                    <div class="row">
                      
                        <section class="col col-3">
                            <label class="label">
                                Client
                            </label>
                            <label class="input">
                                <asp:DropDownList ID="ddlClient" OnSelectedIndexChanged="ddlClient_SelectedIndexChanged" AutoPostBack="true" runat="server"  ValidationGroup="addQuotationConfirmationToClient"   class="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvClient" runat="server" ControlToValidate="ddlClient" ValidationGroup="addQuotationConfirmationToClient"  SetFocusOnError="true" Display="Dynamic" ErrorMessage="*" InitialValue="0" Text="please choose client"></asp:RequiredFieldValidator>
                            </label>
                        </section>
                    </div>
                    <div class="row">
                        <section class="col col-3">
                            <label class="label">
                                First Name
                            </label>
                            <label class="input">
                                <asp:TextBox ID="txtFirstName" runat="server" placeholder="FirstName" ValidationGroup="addQuotationConfirmationToClient" Text="" class="form-control"></asp:TextBox>
                            </label>
                        </section>
                        <section class="col col-3">
                            <label class="label">
                                SurName
                            </label>
                            <label class="input">
                                <asp:TextBox ID="txtSurName" runat="server" placeholder="SurName" ValidationGroup="addQuotationConfirmationToClient" Text="" class="form-control"></asp:TextBox>
                            </label>
                        </section>
                        <section class="col col-3">
                            <label class="label">
                                Email
                            </label>
                            <label class="input">
                                <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" ValidationGroup="addQuotationConfirmationToClient" Text="" class="form-control"></asp:TextBox>
                            </label>
                        </section> 
                    </div> <div class="row">
                        <section class="col col-3">
                            <label class="label">
                                Message To Client
                            </label>                       
                            <label class="label">
                                  <CKEditor:CKEditorControl ID="txtMessageToClient" ValidationGroup="addOperationQuotation" BasePath="~/ckeditor" runat="server" Height="100" Width="500" ToolbarStartupExpanded="False">
                                </CKEditor:CKEditorControl>
                            </label>
                        </section>
                        
                    </div>
                </fieldset>
                <footer>
                    <button id="btnQuotationConfirmationToClientSave" runat="server" onserverclick="btnQuotationConfirmationToClientSave_ServerClick" validationgroup="addQuotationConfirmationToClient" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                        <span style="width: 80px">Send </span>
                    </button>
                    <a href="~/Operation/Quotation.aspx" class="btn bg-color-blueDark txt-color-white" style="width: 100px" runat="server">Cancel</a>
                </footer>
            </div>
        </div>
    </div>
    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                <header>
                    <h2>Quotation Send Client</h2>
                </header>
                <div>
                    <div class="jarviswidget-editbox">
                        &nbsp;
                    </div>
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                            <div class="row">
                                <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                    <div class="input-group">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                            <asp:Repeater ID="rpt_Quotationsent" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                <HeaderTemplate>
                                    <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="hasinput" style="width: 5%">
                                                    <input type="text" class="form-control" placeholder="No" />
                                                </th>
                                                <th class="hasinput" style="width: 20%">
                                                    <input type="text" class="form-control" placeholder="Date" />
                                                </th>
                                                <th class="hasinput" style="width: 10%">
                                                    <input type="text" class="form-control" placeholder="Client" />
                                                </th>
                                                <th class="hasinput" style="width: 10%">
                                                    <input type="text" class="form-control" placeholder="First Name" />
                                                </th>
                                                <th class="hasinput" style="width: 10%">
                                                    <input type="text" class="form-control" placeholder="Surname" />
                                                </th>
                                                <th class="hasinput" style="width: 10%">
                                                    <input type="text" class="form-control" placeholder="E-mail" />
                                                </th>
                                                <th class="hasinput" style="width: 10%">
                                                    <input type="text" class="form-control" placeholder="E-mail sent" />
                                                </th>
                                                <th class="hasinput" style="width: 10%"></th>
                                            </tr>
                                            <tr>
                                                <th>No</th>
                                                <th data-hide="phone">Date</th>
                                                <th data-hide="phone">Client</th>
                                                <th data-hide="phone">First Name</th>
                                                <th data-hide="phone">SurName</th>
                                                <th data-hide="phone">Email</th>
                                                <th data-hide="phone">Email Sent</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "SendDate","{0:d'/'M'/'yyyy hh:mm:ss tt}")%>

                                        </td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "Client") %></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "FirstName")%></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "SurName")%></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "Email")%></td>
                                        <td><%# DataBinder.Eval(Container.DataItem, "EmailStatus")%></td>
                                        <td>

                                            <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" OnClick="buttonDelete_Click" CommandArgument='<%#Eval("ID")%>'   OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>      
            </table>                      
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
