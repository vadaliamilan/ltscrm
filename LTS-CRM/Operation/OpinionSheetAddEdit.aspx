﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="OpinionSheetAddEdit.aspx.cs" Inherits="LTS_CRM.Operation.OpinionSheetAddEdit" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Operation</li>
                    <li>Opinion Sheet</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Opinion Sheet Add-Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">

                                    <div class="tab-content">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="OpinionSheetform" class="row smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Company 
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addBasicgroup"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addBasicgroup" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Date
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtDate" runat="server" placeholder="Date" ValidationGroup="addOpinionSheet" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Staff
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtStaff" runat="server" placeholder="Staff" ValidationGroup="addOpinionSheet" Text="" class="form-control"></asp:TextBox>
                                                            </label>
                                                        </section>
                                                       
                                                    </div>
                                                    <div class="row">
                                                         <section class="col col-3">
                                                            <label class="label">
                                                                Category
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList ID="ddlCategory" runat="server" class="form-control">
                                                                </asp:DropDownList>
                                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidatorCategory" ControlToValidate="ddlCategory" ValidationGroup="addOpinionSheet" runat="server" ErrorMessage="Category Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Upload
                                                            </label>
                                                            <label class="input">
                                                                <asp:FileUpload ID="flUpload" runat="server" />
                                                            </label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                                <footer>
                                                    <button id="btnOpinionSheetSave" runat="server" validationgroup="addOpinionSheet" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnOpinionSheetSave_ServerClick">
                                                        <span style="width: 80px">Save </span>
                                                    </button>
                                                    <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" runat="server" id="Cancel" onserverclick="Cancel_ServerClick" />
                                                </footer>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
    </div>
</asp:Content>

