﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM
{
    public partial class Generator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();

                String sql = "SELECT TABLE_NAME  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA='dbo' order by  TABLE_NAME";
                SqlCommand Command = new SqlCommand();
                Command.Connection = Connection;
                Command.CommandText = sql;
                SqlDataAdapter da = new SqlDataAdapter(Command);
                DataTable dt = new DataTable();
                da.Fill(dt);
                Connection.Close();
                Connection.Dispose();

                if (dt.Rows.Count > 0)
                {
                    txtTableName.DataSource = dt;
                    txtTableName.DataTextField = "TABLE_NAME";
                    txtTableName.DataValueField = "TABLE_NAME";
                    txtTableName.DataBind();
                }

                ckeHeaderRibbon.Text = "<div id=\"ribbon\">" +
                    "<span class=\"ribbon-button-alignment\">" +
                        "<span id=\"refresh\" class=\"btn btn-ribbon\" data-action=\"resetWidgets\" data-title=\"refresh\" rel=\"tooltip\" data-placement=\"bottom\" data-original-title=\"<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings.\" data-html=\"true\">" +
                            "<i class=\"fa fa-refresh\"></i>" +
                        "</span>" +
                    "</span>" +
                    "<ol class=\"breadcrumb\">" +
                        "<li>Management</li>" +
                    //"<li>Management</li>" +
                    "</ol>" +
                "</div>";

                ckeSectionStart.Text = "  <section id=\"Section1\" class=\"\">" +
                   "<div class=\"row\">" +
                   "<article class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">" +
                   "    <div class=\"jarviswidget jarviswidget-color-blueDark\" id=\"empdir\" data-widget-editbutton=\"false\">" +
                   "        <header>" +
                   "            <span class=\"widget-icon\"><i class=\"fa fa-list\"></i></span>" +
                   "            <h2>Client List </h2>" +
                   "        </header>" +
                   "        <div>" +
                   "            <div class=\"jarviswidget-editbox\">" +
                   "            </div>" +
                   "            <div class=\"widget-body no-padding\">" +
                   "                <div class=\"widget-body-toolbar\">" +

                  " <ul class=\"nav nav-tabs tabs-left\" id=\"client-nav\">"+
                  "                         <li id=\"tab1\" runat=\"server\" class=\"bg-color-blueDark txt-color-white active\">"+
                  "                             <a href=\"#tab1_pan\" data-toggle=\"tab\"> Basic Detail  </a>"+
                  "                         </li>"+
                  "                         <li id=\"tab2\" runat=\"server\" class=\"bg-color-blueDark txt-color-white\">"+
                  "                             <a href=\"#tab2_pan\" data-toggle=\"tab\"> Tab2</a>"+
                  "                         </li>"+
                  "                     </ul>"+
                  "                     <div class=\"tab-content\">"+
                  "                         <div runat=\"server\" id=\"tab1_pan\" class=\"tab-pane active\">" +
                   "                    <div class=\"row\">" +
                   "                        <div class=\"col-xs-9 col-sm-5 col-md-5 col-lg-5\">" +
                   "                            <div class=\"input-group\">" +
                   "                            </div>" +
                   "                        </div>" +


                   "                    </div>" +
                   "###@@Data@@###" +
                    "</div>" +
                    "                   <div id=\"tab2_pan\" runat=\"server\" class=\"tab-pane\">"+
                    "                   </div>"+
                    "               </div>"+
                    " </div> </div> </div> </div> </article> </div> </section></div>";
            }
        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            String sql = "";
            SqlCommand Command = new SqlCommand();
            SqlDataAdapter da = null;
            DataTable dt = null;
            StringBuilder strDesignerBuilder = new StringBuilder();
            Button btnType = ((Button)sender);
            string strColumnName, strTxtID = "";

            if (txtFolderName.Text.Trim() != "")
            {
                if (!Directory.Exists((Server.MapPath("~/") + txtFolderName.Text)))
                {
                    Directory.CreateDirectory(Server.MapPath("~/") + txtFolderName.Text);
                }
            }

            Connection.Open();
            //  sql = "Select  * From dbo." + txtTableName.Text.Trim();
            sql = "Select  * From dbo." + txtTableName.SelectedValue;

            string strTableName = txtTableName.SelectedValue;

            Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = sql;
            da = new SqlDataAdapter(Command);
            dt = new DataTable();
            da.Fill(dt);
            Connection.Close();
            Connection.Dispose();
            StringBuilder strForm = new StringBuilder();
            StringBuilder strFormControlSet = new StringBuilder();
            StringBuilder strObjectToFromControl = new StringBuilder()
                , strClearForm = new StringBuilder(), strClearFormDetails = new StringBuilder(), strBindFormDetails = new StringBuilder()
                ;

            strFormControlSet.AppendLine("private void BindFormDetails(){ ");
            strBindFormDetails.AppendLine("BindFormDetails();");
            strClearForm.AppendLine("ClearFormDetails();");
            strClearFormDetails.AppendLine("private void ClearFormDetails(){");
            strObjectToFromControl.AppendLine("private void SaveDetails(){ ");
            if (btnType.CommandArgument == "form")
            {

                //strForm.AppendLine("<table id=\"user\" class=\"table table-bordered table-striped\" style=\"clear: both;\">");

                //strForm.AppendLine("<tbody>");

                strForm.AppendLine("<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\" >");
                strForm.AppendLine("<div id=\"" + strTableName + "form\" class=\"row smart-form\" >");
                strForm.AppendLine("<fieldset>");
                int i = 0, j = 0;
                foreach (DataColumn dc in dt.Columns)
                {
                    j++;
                    if (dc.ColumnName.ToLower() != "createdby" && dc.ColumnName.ToLower() != "createddate" && dc.ColumnName.ToLower() != "updatedby" && dc.ColumnName.ToLower() != "updateddate")
                    {
                        strColumnName = dc.ColumnName;// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dc.ColumnName);
                        strTxtID = "txt" + strColumnName;


                        //if (Dataset1.Tables[0].Columns[1].ColumnName.ToLower().Contains("date") || Dataset1.Tables[0].Columns[1].DataType.ToString() == "System.DateTime")
                        //{
                        //    //Do work;
                        //}
                        /*  Boolean
                                Byte
                                Char
                                DateTime
                                Decimal
                                Double
                                Int16
                                Int32
                                Int64
                                SByte
                                Single
                                String
                                TimeSpan
                                UInt16
                                UInt32
                                UInt64*/

                        //if (dc.DataType.Name.ToString().ToLower() != "int32" && dc.DataType.Name.ToString().ToLower() != "boolean" && dc.DataType.Name.ToString().ToLower() != "varbinary")
                        {
                            if (i % 4 == 0 && dt.Columns.Count > 3)
                            {
                                if (i > 0 && i % 4 == 0)
                                    strForm.AppendLine("</div>");
                                strForm.AppendLine("<div class=\"row\" >");
                                //strForm.AppendLine("<div class=\"row\" alt='" + dt.Columns.Count.ToString() + "-" + i.ToString() + "-" + j.ToString() + "'>");
                            }
                            strForm.AppendLine(" <section class=\"col col-3\">");
                            
                            strForm.AppendLine("<label class=\"label\">");
                            strForm.AppendLine(strColumnName.Replace("_"," "));
                            strForm.AppendLine("</label>");
                            strForm.AppendLine("     <label class=\"input\">");
                            //strForm.AppendLine("         <i class=\"icon-prepend fa fa-globe\"></i>");

                            //strForm.AppendLine("<tr>");
                            //strForm.AppendLine("<td style=\"width: 10%;\"> ");

                            //strForm.AppendLine(strColumnName);

                            //strForm.AppendLine("</td>");
                            //strForm.AppendLine("    <td style=\"width: 10%\">");
                            //strForm.AppendLine("        <div class=\"col-lg-4\">");
                            if(dc.DataType.Name.ToString().ToLower() == "int32" 
                                ||
                                dc.DataType.Name.ToString().ToLower().Contains("decimal")
                                )
                            {
                                strForm.AppendLine("<asp:TextBox ID=\"" + strTxtID + "\" " + (dc.MaxLength > 0 ? "MaxLength=\"" + dc.MaxLength + "\"" : "") +   "runat=\"server\" placeholder=\"" + strColumnName + "\" ValidationGroup=\"add" + strTableName + "\" Text=\"\" class=\"form-control\"></asp:TextBox>");
                                strObjectToFromControl.AppendLine("obj." + strColumnName +"="+strTxtID + ".Text;");
                                strFormControlSet.AppendLine(strTxtID + ".Text=obj." + strColumnName + ".Value;");
                                strClearFormDetails.AppendLine(strTxtID + ".Text=\"\";");
                                if (!dc.AllowDBNull)
                                    strForm.AppendLine("<asp:RequiredFieldValidator SetFocusOnError=\"true\" Font-Bold=\"true\" ForeColor=\"Red\" ID=\"rfv" + strTxtID + "\" ValidationGroup=\"add" + strTableName + "\" ControlToValidate=\"" + strTxtID + "\" runat=\"server\"  ErrorMessage=\"" + strColumnName + " is Required\" Display=\"Dynamic\" Font-Bold=\"true\" ForeColor=\"Red\"></asp:RequiredFieldValidator>");
                                strForm.AppendLine("<asp:RegularExpressionValidator SetFocusOnError=\"true\" Font-Bold=\"true\" ForeColor=\"Red\" ID=\"rex" + strTxtID + "\" ValidationGroup=\"add" + strTableName + "\" ControlToValidate=\"" + strTxtID + "\" runat=\"server\"  Text=\"Only Digit & decimal point allowed. ErrorMessage=\"*\" Display=\"Dynamic\" ValidationExpression=\"[0-9]*\\.?[0-9]*\"></asp:RegularExpressionValidator>");
                            }
                            else if(dc.DataType.Name.ToString().ToLower() == "varbinary")
                            {
                                strForm.AppendLine(" <asp:FileUpload class=\"btn btn-default\" ID=\""+strTxtID+"\" runat=\"server\" ValidationGroup=\"add" + strTableName + "\"></asp:FileUpload>");
                                //strObjectToFromControl.AppendLine("obj." + strColumnName +"="+strTxtID + ".Text;");
                                //strFormControlSet.AppendLine(strTxtID + ".Text=obj." + strColumnName + ".Value;");
                                //strClearFormDetails.AppendLine(strTxtID + ".Text=\"\";");
                            }
                            else if (dc.DataType.Name.ToString().ToLower() == "string")
                            {
                                string strMask = "";
                                if(dc.ColumnName.ToLower().Contains("telephone") || dc.ColumnName.ToLower().Contains("mobile") )
                                {
                                    strMask = " data-mask=\"(999) 999-99999\" ";
                                }
                                strForm.AppendLine("<asp:TextBox ID=\"" + strTxtID + "\" " + (dc.MaxLength > 0 ? "MaxLength=\"" + dc.MaxLength + "\"" : "") + strMask + "runat=\"server\" placeholder=\"" + strColumnName + "\" ValidationGroup=\"add" + strTableName + "\" Text=\"\" class=\"form-control\"></asp:TextBox>");
                                strObjectToFromControl.AppendLine("obj." + strColumnName +"="+strTxtID + ".Text;");
                                strFormControlSet.AppendLine(strTxtID + ".Text=obj." + strColumnName + ".Value;");
                                strClearFormDetails.AppendLine(strTxtID + ".Text=\"\";");
                                if (!dc.AllowDBNull)
                                    strForm.AppendLine("<asp:RequiredFieldValidator ID=\"rfv" + strTxtID + "\" ValidationGroup=\"add" + strTableName + "\" ControlToValidate=\"" + strTxtID + "\" runat=\"server\" SetFocusOnError=\"true\" ErrorMessage=\"" + strColumnName + " is Required\" Display=\"Dynamic\" Font-Bold=\"true\" ForeColor=\"Red\"></asp:RequiredFieldValidator>");
                            }
                            else if (dc.DataType.Name.ToString().ToLower() == "datetime")
                            {
                                strForm.AppendLine("<asp:TextBox ID=\"" + strTxtID + "\"  runat=\"server\" placeholder=\"" + strColumnName + "\" ValidationGroup=\"add" + strTableName + "\" Text=\"\"  class=\"form-control datepicker\" data-dateformat='dd/mm/yy'></asp:TextBox>");

                                strFormControlSet.AppendLine("if (!obj." + strColumnName + ".IsNull  &&  IsDate(Convert.ToString(obj." + strColumnName + ".Value)))");
                                strFormControlSet.AppendLine("{");
                                strFormControlSet.AppendLine("   DateTime dt = new DateTime();");
                                strFormControlSet.AppendLine("dt = Convert.ToDateTime(Convert.ToString(obj." + strColumnName + ".Value));");
                                strFormControlSet.AppendLine("   " + strTxtID + ".Text = dt.ToString(\"dd'/'MM'/'yyyy\");");
                                strFormControlSet.AppendLine("}");

                                strObjectToFromControl.AppendLine("string res" + strColumnName + " = " + strTxtID + ".Text.Trim();");
                                strObjectToFromControl.AppendLine("if (res" + strColumnName + " != \"\")");
                                strObjectToFromControl.AppendLine("{");
                                strObjectToFromControl.AppendLine("    obj." + strColumnName + " = DateTime.ParseExact(res" + strColumnName + ", \"dd/MM/yyyy\", CultureInfo.InvariantCulture);");
                                strObjectToFromControl.AppendLine("}");
                                strClearFormDetails.AppendLine(strTxtID + ".Text=\"\";");
                                
                                //strFormControlSet.AppendLine(strTxtID + ".Text=obj." + strColumnName);
                                if (!dc.AllowDBNull)
                                    strForm.AppendLine("<asp:RequiredFieldValidator ID=\"rfv" + strTxtID + "\" ValidationGroup=\"add" + strTableName + "\" ControlToValidate=\"" + strTxtID + "\" runat=\"server\" SetFocusOnError=\"true\" ErrorMessage=\"" + strColumnName + " is Required\" Display=\"Dynamic\" Font-Bold=\"true\" ForeColor=\"Red\"></asp:RequiredFieldValidator>");


                            }

                            strDesignerBuilder.AppendLine("protected global::System.Web.UI.WebControls.TextBox " + strTxtID + ";");
                            strDesignerBuilder.AppendLine("protected global::System.Web.UI.WebControls.RequiredFieldValidator " + "rfv" + strTxtID + ";");

                            //strForm.AppendLine("        </div>");
                            //strForm.AppendLine("    </td>");
                            //strForm.AppendLine("</tr>");

                            strForm.AppendLine("             </label>");
                            strForm.AppendLine(" </section>");
                            i++;

                        }
                    }
                    if (dt.Columns.Count == j)
                        strForm.AppendLine("</div>");

                }

                strClearFormDetails.AppendLine("} ");
                strFormControlSet.AppendLine("} ");

                strObjectToFromControl.AppendLine(strClearForm.ToString());
                strObjectToFromControl.AppendLine("}");
                strForm.AppendLine("</fieldset>");

                strForm.AppendLine("<footer>");
                strForm.AppendLine("    <button id=\"btn" + strTableName + "Save\" runat=\"server\" ValidationGroup=\"add" + strTableName + "\" class=\"btn bg-color-blueDark txt-color-white\" style=\"width: 100px\" type=\"submit\" >");
                strForm.AppendLine("     <span style=\"width: 80px\">Save </span>");
                strForm.AppendLine("    </button>");
                strForm.AppendLine("    <input action=\"action\" type=\"button\" class=\"btn bg-color-blueDark txt-color-white\" value=\"Cancel\" style=\"width: 100px\" onclick=\"history.go(-1);\" />");
                strForm.AppendLine("</footer>");
                strForm.AppendLine("</div>");
                strForm.AppendLine("</div>");


                //strForm.AppendLine(" <tr>");
                //strForm.AppendLine("             <td style=\"width: 10%;\"></td>");
                //strForm.AppendLine("             <td style=\"width: 40%\"></td>");
                //strForm.AppendLine("         </tr> ");
                //strForm.AppendLine("     </tbody>");
                //strForm.AppendLine(" </table>");
            }
            else
            {
                strDesignerBuilder.AppendLine("protected global::System.Web.UI.WebControls.Repeater " + strTableName.Trim() + ";");

                strForm.AppendLine("<asp:Repeater ID=\"rpt" + strTableName.Trim() + "\" runat=\"server\" ClientIDMode=\"AutoID\" EnableTheming=\"False\">");
                strForm.AppendLine("<HeaderTemplate>");
                strForm.AppendLine("<table id=\"datatable_fixed_column\" class=\"table table-striped table-bordered\" width=\"100%\">");
                strForm.AppendLine(" <thead>");
                strForm.AppendLine("  <tr>");
                strForm.AppendLine("");


                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToLower() != "createdby" && dc.ColumnName.ToLower() != "createddate" && dc.ColumnName.ToLower() != "updatedby" && dc.ColumnName.ToLower() != "updateddate")
                    {
                        strColumnName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dc.ColumnName);
                        strTxtID = "txt" + strColumnName;
                        strForm.AppendLine("<th class=\"hasinput\" style=\"width: 5%\">");
                        strForm.AppendLine("<input type=\"text\" class=\"form-control\" placeholder=\"" + strColumnName + "\" />");
                        strForm.AppendLine("</th>");
                    }
                }
                strForm.AppendLine("");
                strForm.AppendLine("<th class=\"hasinput\" style=\"width: 10%\"></th>");
                strForm.AppendLine("</tr>");
                strForm.AppendLine("<tr>");
                int i = 0;
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToLower() != "createdby" && dc.ColumnName.ToLower() != "createddate" && dc.ColumnName.ToLower() != "updatedby" && dc.ColumnName.ToLower() != "updateddate")
                    {
                        strColumnName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dc.ColumnName);
                        strTxtID = "txt" + strColumnName;
                        i++;
                        strForm.AppendLine("<th " + (i > 4 ? "data-hide=\"phone\"" : "") + ">" + dc.ColumnName + "</th>");
                    }
                }
                strForm.AppendLine("<th>Action</th>");
                strForm.AppendLine("</tr>");
                strForm.AppendLine("</thead>");
                strForm.AppendLine("<tbody>");
                strForm.AppendLine("</HeaderTemplate>");
                strForm.AppendLine("<ItemTemplate>");
                strForm.AppendLine("<tr>");

                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToLower() != "createdby" && dc.ColumnName.ToLower() != "createddate" && dc.ColumnName.ToLower() != "updatedby" && dc.ColumnName.ToLower() != "updateddate")
                    {
                        //if(dc.DataType.Name.ToLower())
                        //strColumnName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dc.ColumnName);
                        //strTxtID = "txt" + strColumnName;
                        if(dc.ColumnName.ToLower().ToString()=="id")
                        {
                            strForm.AppendLine("<td><%# Container.ItemIndex + 1 %></td>");
                        }
                        else 
                            strForm.AppendLine("<td><%# DataBinder.Eval(Container.DataItem, \"" + dc.ColumnName + "\")%></td>");
                    }
                }


                strForm.AppendLine(" <td>");
                strForm.AppendLine("<asp:LinkButton class=\"btn bg-color-blueDark txt-color-white\" ID=\"buttonEdit\" runat=\"server\" Text=\"Click\" CommandArgument='<%#Eval(\"ID\")%>' Visible=\"true\">");
                strForm.AppendLine("<i class=\"fa fa-edit\"></i><span class=\"hidden-mobile\"></span></asp:LinkButton>");
                strForm.AppendLine("<asp:LinkButton class=\"btn bg-color-blueDark txt-color-white\" ID=\"buttonDelete\" runat=\"server\" Text=\"Click\" CommandArgument='<%#Eval(\"ID\")%>' Visible=\"true\" OnClientClick=\"if ( !confirm('Are you sure you want to delete this record?')) return false;\">");
                strForm.AppendLine("<i class=\"fa fa-times\" aria-hidden=\"true\"></i><span class=\"hidden-mobile\"> </span></asp:LinkButton>");
                strForm.AppendLine("</td>");
                strForm.AppendLine("</tr>");
                strForm.AppendLine("</ItemTemplate>");
                strForm.AppendLine("<FooterTemplate>");
                strForm.AppendLine("</tbody>");
                strForm.AppendLine("</table>");
                strForm.AppendLine("</FooterTemplate>");
                strForm.AppendLine("</asp:Repeater>");


                //close brackets
                strClearFormDetails.AppendLine("} ");
                strFormControlSet.AppendLine("} ");

                strObjectToFromControl.AppendLine(strClearForm.ToString());
                strObjectToFromControl.AppendLine("}");
            }
            string inheritNamespace = txtInheritClassName.Text.Trim() + "." + (txtFolderName.Text.Trim().Length > 0 ? (txtFolderName.Text) : "");
            string inheritName = inheritNamespace + "." + txtPageName.Text.Trim();

            string[] aspxLines = {"<%@ Page Language=\"C#\" AutoEventWireup=\"true\" ClientIDMode=\"Static\" CodeBehind=\""+txtPageName.Text.Trim()+".aspx.cs\" Inherits=\""+ inheritName +"\" "+
                                      (chkIsMasterPage.Checked? "MasterPageFile=\""+ txtMasterPage.Text.Trim() + "\"":"")
                                    + "%>",
  
                //"<!DOCTYPE html>",
                //"<html xmlns=\"http://www.w3.org/1999/xhtml\">",
                //"<head>",
                //"<title>The New Page</title>",
                //"</head>",
                //"<body>",
                //"   <form id=\"form1\" runat=\"server\">",
                "<asp:Content ID=\"cntGen\" ContentPlaceHolderID=\"ContentPlaceHolder1\" runat=\"server\">",
                "       <div>",
                //"           <asp:Literal ID=\"output\" runat=\"server\"/>",
                " <div class=\"well\">",
                ckeHeaderRibbon.Text, 
                ckeSectionStart.Text.Replace("###@@Data@@###",strForm.ToString()),

                " <div class=\"modal fade\" id=\"myModal\" role=\"dialog\" data-keyboard=\"false\" data-backdrop=\"static\">",
"<div class=\"modal-dialog\"> ",
"            <!-- Modal content-->",
"            <div class=\"modal-content\">",
"                <div class=\"modal-header\">",
"                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>",
"                    <h4 class=\"modal-title\">",
"                        <asp:Label ID=\"lblheader\" runat=\"server\" Text=\"Message\"></asp:Label></h4>",
"                </div>",
"                <div class=\"modal-body\">",
"                    <p>",
"                        <asp:Label ID=\"lblmsg\" runat=\"server\" Text=\"Message\"></asp:Label>",
"                    </p>",
"                </div>",
"                <div class=\"modal-footer\">",
"                    <a class=\"btn btn-default\" data-dismiss=\"modal\" >Close</a>",
"                </div>",
"            </div>",
"        </div>",
"    </div>",
                " </div>",
                "</div>",
            "</asp:Content>"
    
            //"   </form>",
                //"</body>",
                //"</html>"
        };
            string[] csLines = { 
                "using LTSCRM_DL;"
                ,"using LTS_CRM.BLL;"
                ,"using System;"
                ,"using System.Globalization;"
                ,"using System.Collections.Generic;"
                ,"using System.Globalization;"
                ,"using System.Linq;"
                ,"using System.Web;"
                ,"using LTS_CRM.Helper;"
                ,"using System.Web.UI;",
                "using System.Web.UI.WebControls;",
                "namespace "+inheritNamespace+" {",
                "    public partial class "+txtPageName.Text.Trim()+" : CRMPage {",
                "        protected void Page_Load(object sender, EventArgs e) {",
                //"            output.Text = \"Our new page\";",
            "if (!IsPostBack)",
            "{",
            strBindFormDetails.ToString(),
            "}",

                "        }",
                strFormControlSet.ToString(),
                strObjectToFromControl.ToString(),
                strClearFormDetails.ToString(),
                "    }",
                "}"};
            string[] csDesignerLines = { 
               
                "//------------------------------------------------------------------------------",
                "// <auto-generated>",
                "//     This code was generated by a tool.",
                "//",
                "//     Changes to this file may cause incorrect behavior and will be lost if",
                "//     the code is regenerated.", 
                "// </auto-generated>",
                "//------------------------------------------------------------------------------",

                "namespace "+inheritNamespace+" {",
                "    public partial class "+txtPageName.Text.Trim()+"  {",
                strDesignerBuilder.ToString(),
                "protected global::System.Web.UI.WebControls.Label lblheader;",
                "protected global::System.Web.UI.WebControls.Label lblmsg;", 
                "protected global::System.Web.UI.HtmlControls.HtmlGenericControl tab1;",
                "protected global::System.Web.UI.HtmlControls.HtmlGenericControl tab2;",
                "protected global::System.Web.UI.HtmlControls.HtmlGenericControl tab1_pan;",
                "protected global::System.Web.UI.HtmlControls.HtmlGenericControl tab2_pan;", 
        
                "    }",
                "}"};


            File.WriteAllLines(Server.MapPath("" + (txtFolderName.Text.Trim().Length > 0 ? (txtFolderName.Text + "\\") : "") + txtPageName.Text.Trim() + ".aspx"), aspxLines);
            File.WriteAllLines(Server.MapPath("" + (txtFolderName.Text.Trim().Length > 0 ? (txtFolderName.Text + "\\") : "") + txtPageName.Text.Trim() + ".aspx.cs"), csLines);
            File.WriteAllLines(Server.MapPath("" + (txtFolderName.Text.Trim().Length > 0 ? (txtFolderName.Text + "\\") : "") + txtPageName.Text.Trim() + ".aspx.designer.cs"), csDesignerLines);

            lblmsg.Text = "Aspx Page Created Successfully";
        }
        protected void btnRedirect_Click(object sender, EventArgs e)
        {
            Response.Redirect("" + txtPageName.Text.Trim() + ".aspx");
        }
    }

}
