﻿using LTS_CRM.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LTS_CRM.Helper;
namespace LTS_CRM.Supplier
{
    public partial class SupplierHotel : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                TravelSession.Current.HotelID = null;
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            DataTable dataTableHotel = BLSupplierHotel.GetSupplierHoteltList();
            HotelRepeater.DataSource = dataTableHotel;
            HotelRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.HotelID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/HotelAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int hotelID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierHotel.DeleteSupplierHotel(hotelID);
            if (isSuccess)
                BindGrid();
        }

        protected void addHotelWholeselar_Click(object sender, EventArgs e)
        {
            TravelSession.Current.HotelID = null;
            ClearTabCookies(TabCookieName);
            Response.Redirect("HotelAddEdit.aspx", true);
        }
    }
}