﻿using LTSCRM_DL;
using LTS_CRM.BLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LTS_CRM.Helper;


namespace LTS_CRM.Supplier
{
    public partial class EntranceTicketAddEdit : CRMPage
    {
        #region Common Page function
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                //call to clear tabing index on Not postback


                LoadcboCompanyID(cboCompanyID);
                LoadSupplierMaster(ddlName, App_Code.Helper.EnumSupplierType.Entrance);
                LoadSupplierType(ddlsuppliertype, App_Code.Helper.EnumSupplierType.Entrance);
                LoadSupplierMaster(ddlInfo_supplierName, App_Code.Helper.EnumSupplierType.Entrance);

                // main form
                BindFormDetails();

                // Rate & Information
                BindRateGrid();
                BindInformationForm_Init();
                //BindHotelInformationGrid();
                BindSupplierContactGrid();
            }

            setTabIndex();
        }


        private void setTabIndex()
        {
            if (TravelSession.Current.EntranceTicketID != null && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.EntranceTicketID.Value)))
            {
                tab2.Visible = true;
                tab2_pan.Visible = tab2.Visible;
                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;

                tab4.Visible = tab2.Visible;
                tab5.Visible = tab2.Visible;

                tab4_pan.Visible = tab2.Visible;
                tab5_pan.Visible = tab2.Visible;
            }
            else
            {
                tab2.Visible = false;
                tab2_pan.Visible = tab2.Visible;
                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;
                tab4.Visible = tab2.Visible;
                tab5.Visible = tab2.Visible;
                tab4_pan.Visible = tab2.Visible;
                tab5_pan.Visible = tab2.Visible;
            }

            string selectedTab = "";
            if (Request.Cookies["activetab"] != null)
            {
                selectedTab = Request.Cookies["activetab"].Value;
            }

            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");
            tab3.Attributes.Remove("class");
            tab4.Attributes.Remove("class");
            tab5.Attributes.Remove("class");
            tab1_pan.Attributes.Remove("class");
            tab2_pan.Attributes.Remove("class");
            tab3_pan.Attributes.Remove("class");
            tab4_pan.Attributes.Remove("class");
            tab5_pan.Attributes.Remove("class");

            tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab5.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab1_pan.Attributes.Add("class", "tab-pane ");
            tab2_pan.Attributes.Add("class", "tab-pane ");
            tab3_pan.Attributes.Add("class", "tab-pane ");
            tab4_pan.Attributes.Add("class", "tab-pane ");
            tab5_pan.Attributes.Add("class", "tab-pane ");

            switch (selectedTab)
            {
                case "#tab2_pan":
                    {
                        if (!tab2.Visible)
                            goto default;

                        tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab2_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab3_pan":
                    {
                        if (!tab3.Visible)
                            goto default;
                        tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab3_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab4_pan":
                    {
                        if (!tab4.Visible)
                            goto default;
                        tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab4_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab5_pan":
                    {
                        if (!tab5.Visible)
                            goto default;
                        tab5.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab5_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                default:
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                    tab1_pan.Attributes.Add("class", "tab-pane active");
                    break;
            }
        }
        #endregion

        #region Basic Info
        private void BindFormDetails()
        {
            if (TravelSession.Current.EntranceTicketID != null && TravelSession.Current.EntranceTicketID.HasValue)
            {
                DateTime dt = new DateTime();
                DLSuppliersDetails obj = BLSupplierEntraceTicket.GetSupplierByID(TravelSession.Current.EntranceTicketID.Value);
                if (IsDate(Convert.ToString(obj.SupplierDate.Value)))
                {
                    dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.SupplierDate.Value));
                    txtSupplierDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }

                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                txtContractedBy.Text = obj.ContractedBy.Value;

                if (ddlName.Items.Count > 0 && ddlName.Items.FindByText(Convert.ToString(obj.Name.Value)) != null)
                {
                    ddlName.ClearSelection();
                    ddlName.Items.FindByText(Convert.ToString(obj.Name.Value)).Selected = true;
                }

                txtWebsite.Text = obj.Website.Value;
                //txtAgency.Text = obj.Agency.Value;
                txtClientCode.Text = obj.ClientCode.Value;
                txtUserName.Text = obj.UserName.Value;
                txtPassword.Text = obj.Password.Value;
                txtTelephone.Text = obj.Telephone.Value;
                txtFaxNumber.Text = obj.FaxNumber.Value;
                txtEmergencyNo.Text = obj.EmergencyNo.Value;
                txtEmail.Text = obj.Email.Value;
                txtAlternateEmail.Text = obj.AlternateEmail.Value;
                txtAddress1.Text = obj.Address1.Value;
                txtAddress2.Text = obj.Address2.Value;
                txtCity.Text = obj.City.Value;
                txtState.Text = obj.State.Value;
                cmbcountry.Value = obj.Country.Value;
                txtPostalCode.Text = obj.PostalCode.Value;

                cke_txtPaymentTerms.Text = obj.PaymentTerms.Value;
                txtCreditToLetsTravel.Text = obj.CreditToLetsTravel.Value;
                txtBlackList.Text = obj.BlackList.Value;
                if (!obj.ContractStartDate.IsNull && IsDate(Convert.ToString(obj.ContractStartDate.Value)))
                {
                    dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ContractStartDate.Value));
                    txtContractStartDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.ContractEndingDate.IsNull && IsDate(Convert.ToString(obj.ContractEndingDate.Value)))
                {
                    dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ContractEndingDate.Value));
                    txtContractEndingDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.ContractRenewalDate.IsNull && IsDate(Convert.ToString(obj.ContractRenewalDate.Value)))
                {
                    dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ContractRenewalDate.Value));
                    txtContractRenewalDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }

                if (!obj.Rating.IsNull && ddlRating.Items.Count > 0 && ddlRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)) != null)
                {
                    ddlRating.ClearSelection();
                    ddlRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)).Selected = true;
                }
                //if (!obj.StarRating.IsNull && ddlStarRating.Items.Count > 0 && ddlStarRating.Items.FindByText(Convert.ToString(obj.StarRating.Value)) != null)
                //{
                //    ddlStarRating.ClearSelection();
                //    ddlStarRating.Items.FindByText(Convert.ToString(obj.StarRating.Value)).Selected = true;
                //}
                //cke_txtMainBusiness.Text = obj.MainBusiness.Value;
                cke_txtComments.Text = obj.Comments.Value;

                //Bind userControl Maincontact form
                ucMainContact.BindForm(obj);
                if (!obj.FileData.IsNull)
                ucView.BindDocLink(obj.FileData.Value);
                if (!obj.HowToBook.IsNull)
                    ckHowToBook.Text = obj.HowToBook.Value;

            }
        }
        private void ClearFormDetails()
        {
            TravelSession.Current.EntranceTicketID = null;

            txtSupplierDate.Text = "";
            txtContractedBy.Text = "";
            ddlName.ClearSelection();
            ddlRating.ClearSelection();
            //ddlStarRating.ClearSelection();
            txtWebsite.Text = "";
            //txtAgency.Text = "";
            txtClientCode.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtTelephone.Text = "";
            txtFaxNumber.Text = "";
            txtEmergencyNo.Text = "";
            txtEmail.Text = "";
            txtAlternateEmail.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            cmbcountry.SelectedIndex = -1;
            txtPostalCode.Text = "";

            cke_txtPaymentTerms.Text = "";
            txtCreditToLetsTravel.Text = "";
            txtBlackList.Text = "";
            txtContractStartDate.Text = "";
            txtContractEndingDate.Text = "";
            txtContractRenewalDate.Text = "";

            //cke_txtMainBusiness.Text = "";
            cke_txtComments.Text = "";

        }
        private void SaveDetails()
        {

            DLSuppliersDetails obj = new DLSuppliersDetails();
            string resSupplierDate = txtSupplierDate.Text.Trim();
            if (resSupplierDate != "")
            {
                obj.SupplierDate = DateTime.ParseExact(resSupplierDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.SupplierMasterID = Convert.ToInt32(ddlName.SelectedValue);
            obj.ContractedBy = txtContractedBy.Text;
            obj.Name = ddlName.SelectedItem.Text;
            //obj.StarRating = ddlStarRating.SelectedItem.Text;
            obj.Website = txtWebsite.Text;
            //obj.Agency = txtAgency.Text;
            obj.ClientCode = txtClientCode.Text;
            obj.UserName = txtUserName.Text;
            obj.Password = txtPassword.Text;
            obj.Telephone = txtTelephone.Text;
            obj.FaxNumber = txtFaxNumber.Text;
            obj.EmergencyNo = txtEmergencyNo.Text;
            obj.Email = txtEmail.Text;
            obj.AlternateEmail = txtAlternateEmail.Text;
            obj.Address1 = txtAddress1.Text;
            obj.Address2 = txtAddress2.Text;
            obj.City = txtCity.Text;
            obj.State = txtState.Text;
            obj.Country = cmbcountry.Value;
            obj.PostalCode = txtPostalCode.Text;

            obj.PaymentTerms = cke_txtPaymentTerms.Text;
            obj.CreditToLetsTravel = txtCreditToLetsTravel.Text;
            obj.BlackList = txtBlackList.Text;


            #region Supplier Main contact settings
            //Save Main contact to supplier Details
            obj.Title = ucMainContact.Title;
            obj.SurName = ucMainContact.SurName;
            obj.FirstName = ucMainContact.FirstName;
            obj.ContactEmail = ucMainContact.ContactEmail;
            obj.FaxNo = ucMainContact.FaxNo;
            obj.MobileNo = ucMainContact.MobileNo;
            obj.Telephone = ucMainContact.Telephone;
            obj.Department = ucMainContact.Department;
            obj.Facebook = ucMainContact.Facebook;
            obj.LinkedIn = ucMainContact.LinkedIn;
            obj.Position = ucMainContact.Position;
            obj.Skype = ucMainContact.Skype;
            obj.Twitter = ucMainContact.Twitter;
            obj.WeChat = ucMainContact.WeChat;
            obj.Whatsup = ucMainContact.Whatsup;
            #endregion
            string resContractStartDate = txtContractStartDate.Text.Trim();
            if (resContractStartDate != "")
            {
                obj.ContractStartDate = DateTime.ParseExact(resContractStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            string resContractEndingDate = txtContractEndingDate.Text.Trim();
            if (resContractEndingDate != "")
            {
                obj.ContractEndingDate = DateTime.ParseExact(resContractEndingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            string resContractRenewalDate = txtContractRenewalDate.Text.Trim();
            if (resContractRenewalDate != "")
            {
                obj.ContractRenewalDate = DateTime.ParseExact(resContractRenewalDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.Rating = ddlRating.SelectedValue;
            //obj.MainBusiness = cke_txtMainBusiness.Text;
            obj.Comments = cke_txtComments.Text;
            if (flContractDoc.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = flContractDoc.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.FileData = fileData;
            }
            if (TravelSession.Current.EntranceTicketID != null && TravelSession.Current.EntranceTicketID.HasValue)
            {

                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Current.EntranceTicketID.Value;
                BLSupplierEntraceTicket.UpdateSupplierDetail(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLSupplierEntraceTicket.SaveSupplierDetails(obj);
                //lblheader.Text = "Record Saved Successfully";
                //lblmsg.Text = "Your Account has been created. <br />Your Hotel Supplier ID is " + (Int32)obj.ID;
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            ClearFormDetails();
            Response.Redirect("EntranceTicket.aspx", true);
        }
        protected void btnSuppliersDetailsSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }
        protected void btnCancel_ServerClick(object sender, EventArgs e)
        {
            ClearFormDetails();
            Response.Redirect("~/Supplier/EntranceTicket.aspx", false);
        }


        #endregion

        #region  Rate

        protected void btnSupplierRateSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveRateDetails();
            }
        }
        private void SaveRateDetails()
        {
            if (TravelSession.Current.EntranceTicketID.HasValue)
            {
                DLSupplierEntranceTicketRate obj = new  DLSupplierEntranceTicketRate();
                obj.SupplierDetailID = TravelSession.Current.EntranceTicketID.Value;
                 
                string resMonthFrom = txtMonthFrom.Text.Trim();
                if (resMonthFrom != "")
                {
                    obj.MonthFrom = DateTime.ParseExact(resMonthFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                string resMonthTo = txtMonthTo.Text.Trim();
                if (resMonthTo != "")
                {
                    obj.MonthTo = DateTime.ParseExact(resMonthTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                string resDailyOpeningTime = txtDailyOpeningTime.Text.Trim();
                if (resDailyOpeningTime != "")
                {
                    //obj.DailyOpeningTime = DateTime.ParseExact(resDailyOpeningTime, "HH:mm", CultureInfo.InvariantCulture);
                    obj.DailyOpeningTime = DateTime.ParseExact(resDailyOpeningTime, "h:mm tt", CultureInfo.InvariantCulture);
                }
                string resDailyCloseTime = txtDailyCloseTime.Text.Trim();
                if (resDailyCloseTime != "")
                {
                    obj.DailyCloseTime = DateTime.ParseExact(resDailyCloseTime, "h:mm tt", CultureInfo.InvariantCulture);
                }
                obj.LastAdmission = txtLastAdmission.Text;
                obj.Status = txtStatus.Text;
                obj.Remark = txtRemark.Text;

                if (TravelSession.Current.EntranceTicketRateID.HasValue)
                {
                    obj.UpdatedBy = TravelSession.Current.UserName;
                    obj.UpdatedDate = DateTime.Now;
                    obj.ID = TravelSession.Current.EntranceTicketRateID.Value;
                    BLSupplierEntraceTicket.UpdateSupplierRate(obj);
                }
                else
                {
                    obj.CreatedBy = TravelSession.Current.UserName;
                    obj.CreatedDate = DateTime.Now;
                    BLSupplierEntraceTicket.InsertSupplierRate(obj);
                }
                BindRateGrid();
                ClearRateFormDetails();
            }
        }

        private void ClearRateFormDetails()
        {
            TravelSession.Current.EntranceTicketRateID = null;
            txtMonthFrom.Text = "";
            txtMonthTo.Text = "";
            txtDailyOpeningTime.Text = "";
            txtDailyCloseTime.Text = "";
            txtLastAdmission.Text = "";
            txtStatus.Text = "";
            txtRemark.Text = "";
        }
        private void BindRateGrid()
        {
            if (TravelSession.Current.EntranceTicketID.HasValue)
            {
                SupplierRateRepeater.DataSource = BLSupplierEntraceTicket.GetSupplierRateListBySupplierID(TravelSession.Current.EntranceTicketID.Value);
                SupplierRateRepeater.DataBind();
            }
        }
        private void BindRateForm()
        {
            if (TravelSession.Current.EntranceTicketRateID.HasValue && TravelSession.Current.EntranceTicketID.HasValue)
            {
                DLSupplierEntranceTicketRate obj = new DLSupplierEntranceTicketRate();
                obj.ID = TravelSession.Current.EntranceTicketRateID.Value;
                BLSupplierEntraceTicket.GetSupplierRateListByPk(obj);
                  
                if (!obj.MonthFrom.IsNull && IsDate(Convert.ToString(obj.MonthFrom.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.MonthFrom.Value));
                    txtMonthFrom.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.MonthTo.IsNull && IsDate(Convert.ToString(obj.MonthTo.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.MonthTo.Value));
                    txtMonthTo.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.DailyOpeningTime.IsNull && IsDate(Convert.ToString(obj.DailyOpeningTime.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.DailyOpeningTime.Value));
                    //txtDailyOpeningTime.Text = dt.ToString("dd'/'MM'/'yyyy");
                    //txtDailyOpeningTime.Text = dt.TimeOfDay;
                    txtDailyOpeningTime.Text = dt.ToString("HH:mm");
                }
                if (!obj.DailyCloseTime.IsNull && IsDate(Convert.ToString(obj.DailyCloseTime.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.DailyCloseTime.Value));
                    //txtDailyCloseTime.Text = dt.ToString("dd'/'MM'/'yyyy");
                    txtDailyCloseTime.Text = dt.ToString("HH:mm");

                }
                txtLastAdmission.Text = obj.LastAdmission.Value;
                txtStatus.Text = obj.Status.Value;
                txtRemark.Text = obj.Remark.Value;

            }
        }

        protected void buttonEditRate_Click(object sender, EventArgs e)
        {
            TravelSession.Current.EntranceTicketRateID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            BindRateForm();
        }

        protected void buttonDeleteRate_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierEntraceTicket.DeleteSupplierRateByPk(Id);
            if (isSuccess)
                BindRateGrid();
        }

        #endregion

        #region  Supplier Information


        private void BindInformationForm_Init()
        {
            if (TravelSession.Current.EntranceTicketID.HasValue)
            {
                DLSupplierInformation obj = BLSupplierInsurance.GetInsuranceSupplierInformation(TravelSession.Current.EntranceTicketID.Value);
                if (!obj.ID.IsNull)
                {
                    TravelSession.Current.EntranceTicketInfoID = obj.ID.Value;
                    txtInfo_currency.Text = obj.Currency.Value;
                    cke_txtHotelInfoRemark.Text = isNullCheck(obj.Remarks);

                    txtInfo_postal.Text = obj.PostalCode.Value;
                    txtInfo_telehphone.Text = obj.Telehpone.Value;
                    txtInfo_city.Text = obj.City.Value;
                    txtInfo_address1.Text = obj.Address1.Value;
                    txtInfo_address2.Text = obj.Address2.Value;
                    ddlInfo_supplierName.ClearSelection();

                    if (ddlInfo_supplierName.Items.Count > 0 && ddlInfo_supplierName.Items.FindByText(Convert.ToString(obj.SupplierName.Value)) != null)
                    {
                        ddlInfo_supplierName.ClearSelection();
                        ddlInfo_supplierName.Items.FindByText(Convert.ToString(obj.SupplierName.Value)).Selected = true;
                    }
                }
                else
                    TravelSession.Current.EntranceTicketInfoID = null;
            }
        }

        private void SaveSupplierInformation()
        {
            if (TravelSession.Current.EntranceTicketID.HasValue)
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = TravelSession.Current.EntranceTicketID.Value;
                obj.Currency = txtInfo_currency.Text;
                obj.Remarks = cke_txtHotelInfoRemark.Text;
                
                if (TravelSession.Current.EntranceTicketInfoID.HasValue)
                {
                    obj.UpdatedBy = TravelSession.Current.UserName;
                    obj.UpdatedDate = DateTime.Now;
                    obj.ID = TravelSession.Current.EntranceTicketInfoID.Value;
                    BLSupplierEntraceTicket.UpdateHotelSupplierInfo(obj);
                }
                else
                {
                    obj.CreatedBy = TravelSession.Current.UserName;
                    obj.CreatedDate = DateTime.Now;
                    BLSupplierEntraceTicket.InsertHotelSupplierInfo(obj);
                }

            }
        }


        protected void btnSupplierInformationSave_ServerClick(object sender, EventArgs e)
        {
            SaveSupplierInformation();
        }


        #endregion

        #region How TO Book
        protected void btnHowTobookSave_ServerClick(object sender, EventArgs e)
        {
            if (TravelSession.Current.EntranceTicketID.HasValue)
            {
                DLSuppliersDetails obj = new DLSuppliersDetails();
                obj.ID = TravelSession.Current.EntranceTicketID.Value;
                obj.SelectOne();
                obj.HowToBook = ckHowToBook.Text;
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                BLSupplierEntraceTicket.UpdateSupplierDetail(obj);
            }
        }


        #endregion

        #region Contact

        protected void buttonEditContact_Click(object sender, EventArgs e)
        {
            TravelSession.Current.EntranceTicketContactID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            BindSupplierContactFrom();
        }

        protected void buttonDeleteContact_Click(object sender, EventArgs e)
        {
            int contactId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierContacts.DeleteSupplierContactByPk(contactId);
            if (isSuccess)
                BindSupplierContactGrid();
        }
        protected void btnotherContactSave_ServerClick(object sender, EventArgs e)
        {
            SaveSupplierContactDetails();
        }
        protected void btnotherContactCancel_ServerClick(object sender, EventArgs e)
        {
            ClearContactFormDetails();
        }
        private void SaveSupplierContactDetails()
        {
            if (TravelSession.Current.EntranceTicketID.HasValue)
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = TravelSession.Current.EntranceTicketID.Value;
                obj.Title = ddlTitle1.Value;
                obj.SurName = txtSurName1.Text;
                obj.MainContact = false;
                obj.FirstName = txtFirstName1.Text;
                obj.Position = txtPosition1.Text;
                obj.Department = txtDepartment1.Text;
                obj.Email = txtcontactEmail1.Text;
                obj.DirectPhone = txtcontactTelephone1.Text;
                obj.MobileNo = txtcontactMobile1.Text;
                obj.FaxNo = txtcontactFax1.Text;
                obj.WeChat = txtWeChat1.Text;
                obj.Whatsup = txtWhatsapp1.Text;
                obj.Skype = txtSkype1.Text;
                obj.Facebook = txtFacebook1.Text;
                obj.Twitter = txtTwitter1.Text;
                obj.LinkedIn = txtLinkedin1.Text;

                if (TravelSession.Current.EntranceTicketContactID.HasValue)
                {
                    obj.UpdatedBy = TravelSession.Current.UserName.ToString();
                    obj.UpdatedDate = DateTime.Now;
                    obj.ID = TravelSession.Current.EntranceTicketContactID.Value;
                    BLSupplierContacts.UpdateSupplierContact(obj);
                }
                else
                {
                    obj.CreatedBy = TravelSession.Current.UserName.ToString();
                    obj.CreatedDate = DateTime.Now;
                    BLSupplierContacts.SaveSupplierContact(obj);
                }
                BindSupplierContactGrid();
                ClearContactFormDetails();
            }
        }

        private void ClearContactFormDetails()
        {
            TravelSession.Current.EntranceTicketContactID = null;
            ddlTitle1.SelectedIndex = 0;
            txtSurName1.Text = "";
            txtFirstName1.Text = "";
            txtPosition1.Text = "";
            txtDepartment1.Text = "";
            txtcontactEmail1.Text = "";
            txtcontactTelephone1.Text = "";
            txtcontactMobile1.Text = "";
            txtcontactFax1.Text = "";
            txtWeChat1.Text = "";
            txtWhatsapp1.Text = "";
            txtSkype1.Text = "";
            txtFacebook1.Text = "";
            txtTwitter1.Text = "";
            txtLinkedin1.Text = "";
        }

        private void BindSupplierContactGrid()
        {
            if (TravelSession.Current.EntranceTicketID.HasValue)
            {
                rptSupplierContactRepeater.DataSource = BLSupplierContacts.GetSupplierContactsbySupplierID(TravelSession.Current.EntranceTicketID.Value);
                rptSupplierContactRepeater.DataBind();
            }
        }
        private void BindSupplierContactFrom()
        {
            if (TravelSession.Current.EntranceTicketContactID.HasValue)
            {
                DLSupplierContacts obj = BLSupplierContacts.GetSupplierContactsbyPk(TravelSession.Current.EntranceTicketContactID.Value);
                ddlTitle1.Value = obj.Title.Value;
                txtSurName1.Text = obj.SurName.Value;
                txtFirstName1.Text = obj.FirstName.Value;
                txtPosition1.Text = obj.Position.Value;
                txtDepartment1.Text = obj.Department.Value;
                txtcontactEmail1.Text = obj.Email.Value;
                txtcontactTelephone1.Text = obj.DirectPhone.Value;
                txtcontactMobile1.Text = obj.MobileNo.Value;
                txtcontactFax1.Text = obj.FaxNo.Value;
                txtWeChat1.Text = obj.WeChat.Value;
                txtWhatsapp1.Text = obj.Whatsup.Value;
                txtSkype1.Text = obj.Skype.Value;
                txtFacebook1.Text = obj.Facebook.Value;
                txtTwitter1.Text = obj.Twitter.Value;
                txtLinkedin1.Text = obj.LinkedIn.Value;
            }
        }
        #endregion
    }
}