﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="TourGuideAddEdit.aspx.cs" Inherits="LTS_CRM.Supplier.TourGuideAddEdit" %>

<%@ Register Src="~/Supplier/viewDocument.ascx" TagName="ViewDocument" TagPrefix="uc" %>
<%@ Register Src="~/Supplier/viewDocument.ascx" TagName="File" TagPrefix="uc" %>


<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Supplier</li>
                    <li>Tour Guide Management</li>
                </ol>
            </div>


            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Tour Guide Add Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">

                                        <ul class="nav nav-tabs tabs-left" id="client-nav">
                                            <li id="tab1" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab1_pan" data-toggle="tab">Basic Detail</a>
                                            </li>
                                            <li id="tab2" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab2_pan" data-toggle="tab">How To Book</a>
                                            </li>
                                            <li id="tab3" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab3_pan" data-toggle="tab">Information</a>
                                            </li>
                                            <li id="tab4" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab4_pan" data-toggle="tab">Rate</a>
                                            </li>
                                            <li id="tab5" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab5_pan" data-toggle="tab">Upload TourGuide Photos</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div runat="server" id="tab1_pan" class="tab-pane">
                                                <div class="row">
                                                    <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                        <div class="input-group">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="SuppliersDetailsform" class="row smart-form">
                                                    <div class="col-sm-12">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Company
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addBasicgroup"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addBasicgroup" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Supplier Type
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList Enabled="false" class="form-control" ID="ddlsuppliertype" Height="30" runat="server"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Recruited By
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtRecruitedBy" runat="server" placeholder="Recruited By" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        SurName
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtSurName" runat="server" placeholder="SurName" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        SurName In Chinese
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtSurNameInChinese" runat="server" placeholder="SurName In Chinese" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        First Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        First Name In Chinese
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFirstNameInChinese" runat="server" placeholder="First Name In Chinese" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Sex
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlSex" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control datepicker">
                                                                        </asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Date Of Birth
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtDateOfBirth" runat="server" placeholder="Date Of Birth" ValidationGroup="addSupplierTourGuide" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Origin
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtOrigin" runat="server" placeholder="Origin" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Nationality
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtNationality" runat="server" placeholder="Nationality" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Married
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlMarried" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Children
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlChildren" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control">
                                                                            <asp:ListItem Selected="True" Value="0" Text="None"></asp:ListItem>
                                                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Visa Status
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtVisaStatus" runat="server" placeholder="Visa Status" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Home Phone
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtHomePhone" runat="server" placeholder="Home Phone" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="regex" ValidationGroup="addSupplierTourGuide" ControlToValidate="txtHomePhone" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Mobile Phone
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtMobilePhone" data-mask="(999) 999-99999" runat="server" placeholder="Mobile Phone" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        We Chat
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtWeChat" runat="server" placeholder="We Chat" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Email
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Address 1 
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtAddress1" runat="server" placeholder="Address1" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Address 2
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtAddress2" runat="server" placeholder="Address2" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        City
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtCity" runat="server" placeholder="City" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Country
                                                                    </label>
                                                                    <label class="input">
                                                                        <select id="ddlCountry" class="form-control" runat="server">
                                                                            <option value="Afghanistan">Afghanistan</option>
                                                                            <option value="Albania">Albania</option>
                                                                            <option value="Algeria">Algeria</option>
                                                                            <option value="American Samoa">American Samoa</option>
                                                                            <option value="Andorra">Andorra</option>
                                                                            <option value="Angola">Angola</option>
                                                                            <option value="Anguilla">Anguilla</option>
                                                                            <option value="Antartica">Antarctica</option>
                                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                            <option value="Argentina">Argentina</option>
                                                                            <option value="Armenia">Armenia</option>
                                                                            <option value="Aruba">Aruba</option>
                                                                            <option value="Australia">Australia</option>
                                                                            <option value="Austria">Austria</option>
                                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                                            <option value="Bahamas">Bahamas</option>
                                                                            <option value="Bahrain">Bahrain</option>
                                                                            <option value="Bangladesh">Bangladesh</option>
                                                                            <option value="Barbados">Barbados</option>
                                                                            <option value="Belarus">Belarus</option>
                                                                            <option value="Belgium">Belgium</option>
                                                                            <option value="Belize">Belize</option>
                                                                            <option value="Benin">Benin</option>
                                                                            <option value="Bermuda">Bermuda</option>
                                                                            <option value="Bhutan">Bhutan</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                            <option value="Botswana">Botswana</option>
                                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                                            <option value="Brazil">Brazil</option>
                                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                            <option value="Bulgaria">Bulgaria</option>
                                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                                            <option value="Burundi">Burundi</option>
                                                                            <option value="Cambodia">Cambodia</option>
                                                                            <option value="Cameroon">Cameroon</option>
                                                                            <option value="Canada">Canada</option>
                                                                            <option value="Cape Verde">Cape Verde</option>
                                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                                            <option value="Central African Republic">Central African Republic</option>
                                                                            <option value="Chad">Chad</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="China">China</option>
                                                                            <option value="Christmas Island">Christmas Island</option>
                                                                            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Comoros">Comoros</option>
                                                                            <option value="Congo">Congo</option>
                                                                            <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                            <option value="Cook Islands">Cook Islands</option>
                                                                            <option value="Costa Rica">Costa Rica</option>
                                                                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                            <option value="Croatia">Croatia (Hrvatska)</option>
                                                                            <option value="Cuba">Cuba</option>
                                                                            <option value="Cyprus">Cyprus</option>
                                                                            <option value="Czech Republic">Czech Republic</option>
                                                                            <option value="Denmark">Denmark</option>
                                                                            <option value="Djibouti">Djibouti</option>
                                                                            <option value="Dominica">Dominica</option>
                                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                                            <option value="East Timor">East Timor</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Egypt">Egypt</option>
                                                                            <option value="El Salvador">El Salvador</option>
                                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                            <option value="Eritrea">Eritrea</option>
                                                                            <option value="Estonia">Estonia</option>
                                                                            <option value="Ethiopia">Ethiopia</option>
                                                                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                                            <option value="Fiji">Fiji</option>
                                                                            <option value="Finland">Finland</option>
                                                                            <option value="France">France</option>
                                                                            <option value="France Metropolitan">France, Metropolitan</option>
                                                                            <option value="French Guiana">French Guiana</option>
                                                                            <option value="French Polynesia">French Polynesia</option>
                                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                                            <option value="Gabon">Gabon</option>
                                                                            <option value="Gambia">Gambia</option>
                                                                            <option value="Georgia">Georgia</option>
                                                                            <option value="Germany">Germany</option>
                                                                            <option value="Ghana">Ghana</option>
                                                                            <option value="Gibraltar">Gibraltar</option>
                                                                            <option value="Greece">Greece</option>
                                                                            <option value="Greenland">Greenland</option>
                                                                            <option value="Grenada">Grenada</option>
                                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                                            <option value="Guam">Guam</option>
                                                                            <option value="Guatemala">Guatemala</option>
                                                                            <option value="Guinea">Guinea</option>
                                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                            <option value="Guyana">Guyana</option>
                                                                            <option value="Haiti">Haiti</option>
                                                                            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                            <option value="Holy See">Holy See (Vatican City State)</option>
                                                                            <option value="Honduras">Honduras</option>
                                                                            <option value="Hong Kong">Hong Kong</option>
                                                                            <option value="Hungary">Hungary</option>
                                                                            <option value="Iceland">Iceland</option>
                                                                            <option value="India">India</option>
                                                                            <option value="Indonesia">Indonesia</option>
                                                                            <option value="Iran">Iran (Islamic Republic of)</option>
                                                                            <option value="Iraq">Iraq</option>
                                                                            <option value="Ireland">Ireland</option>
                                                                            <option value="Israel">Israel</option>
                                                                            <option value="Italy">Italy</option>
                                                                            <option value="Jamaica">Jamaica</option>
                                                                            <option value="Japan">Japan</option>
                                                                            <option value="Jordan">Jordan</option>
                                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                                            <option value="Kenya">Kenya</option>
                                                                            <option value="Kiribati">Kiribati</option>
                                                                            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                            <option value="Korea">Korea, Republic of</option>
                                                                            <option value="Kuwait">Kuwait</option>
                                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                            <option value="Lao">Lao People's Democratic Republic</option>
                                                                            <option value="Latvia">Latvia</option>
                                                                            <option value="Lebanon">Lebanon</option>
                                                                            <option value="Lesotho">Lesotho</option>
                                                                            <option value="Liberia">Liberia</option>
                                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                                            <option value="Lithuania">Lithuania</option>
                                                                            <option value="Luxembourg">Luxembourg</option>
                                                                            <option value="Macau">Macau</option>
                                                                            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                            <option value="Madagascar">Madagascar</option>
                                                                            <option value="Malawi">Malawi</option>
                                                                            <option value="Malaysia">Malaysia</option>
                                                                            <option value="Maldives">Maldives</option>
                                                                            <option value="Mali">Mali</option>
                                                                            <option value="Malta">Malta</option>
                                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                                            <option value="Martinique">Martinique</option>
                                                                            <option value="Mauritania">Mauritania</option>
                                                                            <option value="Mauritius">Mauritius</option>
                                                                            <option value="Mayotte">Mayotte</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Micronesia">Micronesia, Federated States of</option>
                                                                            <option value="Moldova">Moldova, Republic of</option>
                                                                            <option value="Monaco">Monaco</option>
                                                                            <option value="Mongolia">Mongolia</option>
                                                                            <option value="Montserrat">Montserrat</option>
                                                                            <option value="Morocco">Morocco</option>
                                                                            <option value="Mozambique">Mozambique</option>
                                                                            <option value="Myanmar">Myanmar</option>
                                                                            <option value="Namibia">Namibia</option>
                                                                            <option value="Nauru">Nauru</option>
                                                                            <option value="Nepal">Nepal</option>
                                                                            <option value="Netherlands">Netherlands</option>
                                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                            <option value="New Caledonia">New Caledonia</option>
                                                                            <option value="New Zealand">New Zealand</option>
                                                                            <option value="Nicaragua">Nicaragua</option>
                                                                            <option value="Niger">Niger</option>
                                                                            <option value="Nigeria">Nigeria</option>
                                                                            <option value="Niue">Niue</option>
                                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                            <option value="Norway">Norway</option>
                                                                            <option value="Oman">Oman</option>
                                                                            <option value="Pakistan">Pakistan</option>
                                                                            <option value="Palau">Palau</option>
                                                                            <option value="Panama">Panama</option>
                                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Philippines">Philippines</option>
                                                                            <option value="Pitcairn">Pitcairn</option>
                                                                            <option value="Poland">Poland</option>
                                                                            <option value="Portugal">Portugal</option>
                                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                                            <option value="Qatar">Qatar</option>
                                                                            <option value="Reunion">Reunion</option>
                                                                            <option value="Romania">Romania</option>
                                                                            <option value="Russia">Russian Federation</option>
                                                                            <option value="Rwanda">Rwanda</option>
                                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                            <option value="Saint LUCIA">Saint LUCIA</option>
                                                                            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                            <option value="Samoa">Samoa</option>
                                                                            <option value="San Marino">San Marino</option>
                                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                                            <option value="Senegal">Senegal</option>
                                                                            <option value="Seychelles">Seychelles</option>
                                                                            <option value="Sierra">Sierra Leone</option>
                                                                            <option value="Singapore">Singapore</option>
                                                                            <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                            <option value="Slovenia">Slovenia</option>
                                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                                            <option value="Somalia">Somalia</option>
                                                                            <option value="South Africa">South Africa</option>
                                                                            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                            <option value="Span">Spain</option>
                                                                            <option value="SriLanka">Sri Lanka</option>
                                                                            <option value="St. Helena">St. Helena</option>
                                                                            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                            <option value="Sudan">Sudan</option>
                                                                            <option value="Suriname">Suriname</option>
                                                                            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="Swaziland">Swaziland</option>
                                                                            <option value="Sweden">Sweden</option>
                                                                            <option value="Switzerland">Switzerland</option>
                                                                            <option value="Syria">Syrian Arab Republic</option>
                                                                            <option value="Taiwan">Taiwan, Province of China</option>
                                                                            <option value="Tajikistan">Tajikistan</option>
                                                                            <option value="Tanzania">Tanzania, United Republic of</option>
                                                                            <option value="Thailand">Thailand</option>
                                                                            <option value="Togo">Togo</option>
                                                                            <option value="Tokelau">Tokelau</option>
                                                                            <option value="Tonga">Tonga</option>
                                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                            <option value="Tunisia">Tunisia</option>
                                                                            <option value="Turkey">Turkey</option>
                                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                                            <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                            <option value="Tuvalu">Tuvalu</option>
                                                                            <option value="Uganda">Uganda</option>
                                                                            <option value="Ukraine">Ukraine</option>
                                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                                            <option value="United Kingdom" selected>United Kingdom</option>
                                                                            <option value="United States">United States</option>
                                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                                            <option value="Vanuatu">Vanuatu</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Vietnam">Viet Nam</option>
                                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                            <option value="Western Sahara">Western Sahara</option>
                                                                            <option value="Yemen">Yemen</option>
                                                                            <option value="Yugoslavia">Yugoslavia</option>
                                                                            <option value="Zambia">Zambia</option>
                                                                            <option value="Zimbabwe">Zimbabwe</option>
                                                                        </select>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Postal Code
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtPostCode" runat="server" placeholder="PostCode" ValidationGroup="addSupplierTourGuide" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rfv" ValidationGroup="addSupplierTourGuide" ControlToValidate="txtPostCode" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Speaking Language
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlSpeaking" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        No. Of Year Tour Guide
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtNoOfYearTourGuide" runat="server" MaxLength="4" ValidationGroup="addSupplierTourGuide"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ValidationGroup="addSupplierTourGuide" ID="rextxtNoOfYearTourGuide" ControlToValidate="txtNoOfYearTourGuide" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Area Of Working
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlAreaOfWorking" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Type Of Work
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlTypeOfWorking" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Vehicle Owned
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlVehicleOwned" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Kind Of Group Work
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtKindOfGroupWork" TextMode="MultiLine" runat="server" placeholder="Kind Of Group Work" ValidationGroup="addSupplierTourGuide" Text="" class="form-control">
                                                                        </asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        KindOfGroupNotWork
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlKindOfGroupNotWork" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Rating
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlRating" runat="server" ValidationGroup="addSupplierTourGuide" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">

                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Special Requirement
                                                                    </label>
                                                                    <label class="input">
                                                                        <CKEditor:CKEditorControl ID="txtSpecialRequirement" ValidationGroup="addSupplierTourGuide" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>


                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Black List
                                                                    </label>
                                                                    <label class="input">
                                                                        <CKEditor:CKEditorControl ID="txtBlackList" ValidationGroup="addSupplierTourGuide" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Comments
                                                                    </label>
                                                                    <label class="input">

                                                                        <CKEditor:CKEditorControl ID="txtComments" ValidationGroup="addSupplierTourGuide" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Driving Licence
                                                                    </label>

                                                                    <section class="col col-9">
                                                                        <label class="input">
                                                                            <asp:FileUpload class="btn btn-default" ID="fl_DrivingLic" runat="server"></asp:FileUpload>

                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <uc:File runat="server" ID="ucDrivingLicView" Visible="False"></uc:File>

                                                                    </section>

                                                                    <label class="input">
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Vehicle Photo
                                                                    </label>
                                                                    <section class="col col-9">
                                                                        <label class="input">
                                                                            <asp:FileUpload class="btn btn-default" ID="fl_VehicalePhoto" runat="server"></asp:FileUpload>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <uc:File runat="server" ID="ucVehicalePhotoView" Visible="False"></uc:File>
                                                                    </section>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Passport Copy
                                                                    </label>
                                                                    <section class="col col-9">
                                                                        <label class="input">
                                                                            <asp:FileUpload class="btn btn-default" ID="fl_PassportCopy" runat="server"></asp:FileUpload>

                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <uc:File runat="server" ID="ucPassportCopyView" Visible="False"></uc:File>

                                                                    </section>

                                                                    <label class="input">
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Address Proof
                                                                    </label>
                                                                    <section class="col col-9">
                                                                        <label class="input">
                                                                            <asp:FileUpload class="btn btn-default" ID="fl_AddressProof" runat="server"></asp:FileUpload>

                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <uc:File runat="server" ID="ucAddressProofView" Visible="False"></uc:File>

                                                                    </section>

                                                                    <label class="input">
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Guide Licence
                                                                    </label>
                                                                    <section class="col col-9">
                                                                        <label class="input">
                                                                            <asp:FileUpload class="btn btn-default" ID="fl_GuideLicence" runat="server"></asp:FileUpload>

                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <uc:File runat="server" ID="ucGuideLicenceView" Visible="False"></uc:File>

                                                                    </section>

                                                                    <label class="input">
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        CV Of Guide
                                                                    </label>
                                                                    <section class="col col-9">
                                                                        <label class="input">
                                                                            <asp:FileUpload class="btn btn-default" ID="fl_CVOfGuide" runat="server"></asp:FileUpload>

                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <uc:File runat="server" ID="ucCVOfGuideView" Visible="False"></uc:File>

                                                                    </section>

                                                                    <label class="input">
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnSaveBasic" runat="server" validationgroup="addSupplierTourGuide" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnSaveBasic_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <button id="btnCancel" runat="server" causesvalidation="false" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnCancel_ServerClick">
                                                                <span style="width: 80px">Cancel </span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                </div>

                                            </div>
                                            <div id="tab2_pan" runat="server" class="tab-pane">
                                                <div id="SupplierHotelBook" class="smart-form">
                                                    <fieldset>
                                                        <div class="row">
                                                            <section class="col col-12">
                                                                <label class="input">
                                                                    How To Book
                                                                        <CKEditor:CKEditorControl ID="ckHowToBook" BasePath="~/ckeditor" runat="server" Height="300" Width="800" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                </label>
                                                            </section>
                                                        </div>
                                                    </fieldset>
                                                    <footer>
                                                        <button id="btnHowTobookSave" runat="server" onserverclick="btnHowTobookSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                            <span style="width: 80px">Save</span>
                                                        </button>
                                                    </footer>
                                                </div>
                                            </div>

                                            <div id="tab3_pan" runat="server" class="tab-pane">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div id="SupplierInformationform" class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Supplier Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlInfo_supplierName" runat="server" ValidationGroup="addSupplierInformation" class="form-control" onchange="return fn_supplierChange(this,'ddlsuppliertype','cboCompanyID');"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvsupplierName" ControlToValidate="ddlInfo_supplierName" ValidationGroup="addSupplierInformation" InitialValue="0" runat="server" ErrorMessage="Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Telehphone
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_telehphone" data-mask="(999) 999-99999" runat="server" placeholder="Telehphone" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Address Line 1
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_address1" runat="server" placeholder="Address Line 1" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Address Line 2
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_address2" runat="server" placeholder="Address Line 2" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        City
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_city" runat="server" placeholder="City" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Country
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_country" runat="server" placeholder="Country" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Postal Code
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_postal" runat="server" placeholder="Postal Code" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Currency
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_currency" runat="server" placeholder="Currency" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnSupplierInformationSave" runat="server" validationgroup="addSupplierInformation" onserverclick="btnSupplierInformationSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <%-- <button id="btnSupplierInfoCancel" runat="server" causesvalidation="false" validationgroup="addSupplierInformation" onserverclick="btnSupplierInfoCancel_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Cancel</span>
                                                            </button>--%>
                                                        </footer>
                                                    </div>

                                                </div>

                                            </div>
                                            <div id="tab4_pan" runat="server" class="tab-pane">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="SupplierRateform" class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Season
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlSeason" runat="server" ValidationGroup="addSupplierTourGuideRate" class="form-control"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        From Date
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFromDate" runat="server" placeholder="From Date" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        To Date
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtToDate" runat="server" placeholder="To Date" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Half day 4 9 Seater Driver Guide Without Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtHalf_day_4_9_SeaterDriverGuideWithoutVehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtHalf_day_4_9_SeaterDriverGuideWithoutVehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtHalf_day_4_9_SeaterDriverGuideWithoutVehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Half day 12 17 Seater Driver Guide Without Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtHalf_day_12_17_Seater_Driver_GuideWithoutVehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtHalf_day_12_17_Seater_Driver_GuideWithoutVehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtHalf_day_12_17_Seater_Driver_GuideWithoutVehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Half day coach Guide
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtHalf_day_coach_Guide" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtHalf_day_coach_Guide" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtHalf_day_coach_Guide" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>

                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Half day 4 9 Seater Driver Guide With Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtHalf_day_4_9_Seater_Driver_Guide_With_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtHalf_day_4_9_Seater_Driver_Guide_With_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtHalf_day_4_9_Seater_Driver_Guide_With_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Half day 12 17 Seater Driver Guide With Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtHalf_day_12_17_Seater_Driver_Guide_With_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtHalf_day_12_17_Seater_Driver_Guide_With_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtHalf_day_12_17_Seater_Driver_Guide_With_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Half day tips
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtHalf_day_tips" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtHalf_day_tips" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtHalf_day_tips" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Full day 4 9 Seater Driver Guide Without Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFull_day_4_9_Seater_Driver_Guide_Without_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtFull_day_4_9_Seater_Driver_Guide_Without_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtFull_day_4_9_Seater_Driver_Guide_Without_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>

                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Full day 12 17 Seater Driver Guide Without Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFull_day_12_17_Seater_Driver_Guide_Without_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtFull_day_12_17_Seater_Driver_Guide_Without_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtFull_day_12_17_Seater_Driver_Guide_Without_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Full Day coach Guide
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFull_Day_coach_Guide" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtFull_Day_coach_Guide" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtFull_Day_coach_Guide" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Full day 4 9 Seater Driver Guide With Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFull_day_4_9_Seater_Driver_Guide_With_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtFull_day_4_9_Seater_Driver_Guide_With_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtFull_day_4_9_Seater_Driver_Guide_With_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Full day 12 17 Seater Driver Guide WithVehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFull_day_12_17_Seater_Driver_Guide_WithVehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtFull_day_12_17_Seater_Driver_Guide_WithVehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtFull_day_12_17_Seater_Driver_Guide_WithVehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>

                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Full day tips
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFull_day_tips" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtFull_day_tips" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtFull_day_tips" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Extra hour With Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtExtra_hour_With_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtExtra_hour_With_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtExtra_hour_With_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Extra hour Without vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtExtra_hour_Without_vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtExtra_hour_Without_vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtExtra_hour_Without_vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        London Edinburgh With Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtLondon_Edinburgh_With_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtLondon_Edinburgh_With_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtLondon_Edinburgh_With_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        London Edinburgh Without Vehicle
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtLondon_Edinburgh_Without_Vehicle" runat="server" placeholder="" ValidationGroup="addSupplierTourGuideRate" Text="" class="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator SetFocusOnError="true" Font-Bold="true" ForeColor="Red" ID="rextxtLondon_Edinburgh_Without_Vehicle" ValidationGroup="addSupplierTourGuideRate" ControlToValidate="txtLondon_Edinburgh_Without_Vehicle" runat="server" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnSupplierTourGuideRateSave" onserverclick="btnSupplierTourGuideRateSave_ServerClick" runat="server" validationgroup="addSupplierTourGuideRate" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save</span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="SupplierRateform1" class="row smart-form1">
                                                        <asp:Repeater ID="rptSupplierTourGuideRate" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                            <HeaderTemplate>
                                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                                    <thead>
                                                                        <tr>

                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="ID" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Suppliertourguideid" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Season" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Fromdate" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Todate" />
                                                                            </th>

                                                                            <th class="hasinput" style="width: 10%"></th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>ID</th>
                                                                            <th>SupplierTourGuideID</th>
                                                                            <th>Season</th>
                                                                            <th>FromDate</th>
                                                                            <th data-hide="phone">ToDate</th>

                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "SupplierTourGuideID")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "Season")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "FromDate","{0:dd'/'MM'/'yyyy}")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "ToDate","{0:dd'/'MM'/'yyyy}")%></td>
                                                                    <td>
                                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" OnClick="buttonEdit_Click" Text="Edit" CommandArgument='<%#Eval("ID")%>' Visible="true">
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Delete" OnClick="buttonDelete_Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
</table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="tab5_pan" runat="server" class="tab-pane">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="tourphotos" class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <h3 class="text-primary">Tour Guide Photos</h3>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <asp:FileUpload ID="flTourPhoto" runat="server" AllowMultiple="true" />
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnTourPhotoSave" runat="server" onserverclick="btnTourPhotoSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <button id="btnTourPhotoCancel" runat="server" causesvalidation="false" onserverclick="btnTourPhotoCancel_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Cancel</span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="clearfix"></div>
                                                    <div id="tourphotosGrid" class="row smart-form1">

                                                        <asp:Repeater ID="rptPhotos" OnItemDataBound="rptPhotos_ItemDataBound" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                            <HeaderTemplate>
                                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="hasinput" style="width: 8%">
                                                                                <input type="text" class="form-control" placeholder="No" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 40%">
                                                                                <input type="text" class="form-control" placeholder="File Name" />
                                                                            </th>
                                                                            <th style="width: 15%"></th>
                                                                            <th class="hasinput" style="width: 20%"></th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th data-hide="phone">No</th>
                                                                            <th data-class="expand">File Name</th>
                                                                            <th data-class="expand">Download</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "RealFileName")%></td>
                                                                    <td>
                                                                        <asp:HiddenField ID="hdnview" Value='<%# Eval("Id") %>' runat="server" />
                                                                        <uc:ViewDocument runat="server" ID="ucPhotoView"></uc:ViewDocument>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDeletePhoto" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonDeletePhoto_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record, it will delete client contact?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    </td>
                                                                </tr>

                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                            </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>


        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal">Close</a>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

