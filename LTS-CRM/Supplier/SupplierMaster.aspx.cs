﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class SupplierMaster : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.SupplierMasterID = null;
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(ddlCompany);
                GetAllSupplierType(ddlSupplierType);
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            DataTable dataTableHotel = BLSupplierMaster.GetSupplierMasterList(Convert.ToInt32(ddlCompany.SelectedValue), Convert.ToInt32(ddlSupplierType.SelectedValue));
            SupplierMasterRepeater.DataSource = dataTableHotel;
            SupplierMasterRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.SupplierMasterID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/SupplierMasterAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLSupplierMaster.DeleteSupplierMaster(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();

           

        }

        protected void addSupplierMaster_Click(object sender, EventArgs e)
        {
            TravelSession.Current.SupplierMasterID = null;
            ClearTabCookies(TabCookieName);
            Response.Redirect("~/Supplier/SupplierMasterAddEdit.aspx", false);
        }

        protected void ddlSupplierType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}