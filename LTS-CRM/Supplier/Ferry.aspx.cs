﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class Ferry : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            DataTable dataTableFerry = BLSupplierFerry.GetSupplierFerryList();
            FerryRepeater.DataSource = dataTableFerry;
            FerryRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.FerryID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/FerryAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int ferryID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierFerry.DeleteSupplierFerry(ferryID);
            if (isSuccess)
                BindGrid();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.FerryID = null;
            Response.Redirect("~/Supplier/FerryAddEdit.aspx", false);
        }
    }
}