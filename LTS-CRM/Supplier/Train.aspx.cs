﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class Train : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            DataTable dataTableTrain = BLSupplierTrain.GetSupplierTrainList();
            TrainRepeater.DataSource = dataTableTrain;
            TrainRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.TrainID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/TrainAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int trainID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierTrain.DeleteSupplierTrain(trainID);
            if (isSuccess)
                BindGrid();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.TrainID = null;
            Response.Redirect("~/Supplier/TrainAddedit.aspx", false);
        }
    }
}