﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="RestaurantAddEdit.aspx.cs" Inherits="LTS_CRM.Supplier.RestaurantAddEdit" %>

<%@ Register TagName="MainContact" Src="~/Supplier/UserContacts.ascx" TagPrefix="uc" %>
<%@ Register Src="~/Supplier/viewDocument.ascx" TagName="File" TagPrefix="uc" %>

<%@ Register Src="~/Supplier/viewDocument.ascx" TagName="SampleMenuFile" TagPrefix="uc" %>
<%@ Register Src="~/Supplier/viewDocument.ascx" TagName="FoodPhoto" TagPrefix="uc" %>
<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Supplier</li>
                    <li>Restaurant Management</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Restaurant Add Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">

                                        <ul class="nav nav-tabs tabs-left" id="client-nav">
                                            <li id="tab1" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab1_pan" data-toggle="tab">Basic Detail  </a>
                                            </li>
                                            <li id="tab2" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab2_pan" data-toggle="tab">How To Book</a>
                                            </li>
                                            <li id="tab3" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab3_pan" data-toggle="tab">Information </a>
                                            </li>
                                            <li id="tab4" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab4_pan" data-toggle="tab">RATE</a>
                                            </li>
                                            <li id="tab5" runat="server" class="bg-color-blueDark txt-color-white">
                                                <a href="#tab5_pan" data-toggle="tab">Contact</a>
                                            </li>

                                        </ul>
                                        <div class="tab-content">
                                            <div runat="server" id="tab1_pan" class="tab-pane">
                                                <div class="row">
                                                    <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                        <div class="input-group">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="SuppliersDetailsform" class="row smart-form">
                                                    <div class="col-sm-12">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Company
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addBasicgroup"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator SetFocusOnError="true"  ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addBasicgroup" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        SupplierType
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList Enabled="false" class="form-control" ID="ddlsuppliertype" Height="30" runat="server"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Supplier Date
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtSupplierDate" runat="server" placeholder="Supplier Date" ValidationGroup="addBasicgroup" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                        <asp:RequiredFieldValidator  SetFocusOnError="true" ID="rfvtxtSupplierDate" ControlToValidate="txtSupplierDate" ValidationGroup="addBasicgroup" runat="server" ErrorMessage="SupplierDate Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Contracted By
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtContractedBy" runat="server" placeholder="Contracted By" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlName" runat="server" ValidationGroup="addBasicgroup" class="form-control" onchange="return fn_supplierChange(this,'ddlsuppliertype','cboCompanyID');"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator  SetFocusOnError="true" ID="rfvddlName" ControlToValidate="ddlName" ValidationGroup="addBasicgroup" InitialValue="0" runat="server" ErrorMessage="Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <%--<section class="col col-3">
                                                                    <label class="label">
                                                                        Star Rating
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlStarRating" runat="server" class="form-control">
                                                                            <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="Bed and Breakfast" Value="Bed and Breakfast"></asp:ListItem>
                                                                            <asp:ListItem Text="Apartment" Value="Apartment"></asp:ListItem>
                                                                            <asp:ListItem Text="1*" Value="1*"></asp:ListItem>
                                                                            <asp:ListItem Text="2*" Value="2*"></asp:ListItem>
                                                                            <asp:ListItem Text="3*" Value="3*"></asp:ListItem>
                                                                            <asp:ListItem Text="4*" Value="4*"></asp:ListItem>
                                                                            <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>
                                                                </section>--%>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Website
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtWebsite" runat="server" placeholder="Website" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Type OF Food
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtTypeOfFood" runat="server" placeholder="Type of Food" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Agency
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtAgency" runat="server" placeholder="Agency" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Client Code
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtClientCode" runat="server" placeholder="Client Code" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>

                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        UserName
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtUserName" runat="server" placeholder="UserName" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Password
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Password" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Telephone
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtTelephone" runat="server" placeholder="Telephone" data-mask="(999) 999-99999" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Fax Number
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtFaxNumber" runat="server" placeholder="Fax Number" data-mask="(999) 999-99999" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Emergency No
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtEmergencyNo" runat="server" placeholder="Emergency No" data-mask="(999) 999-99999" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Reservation Email
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Flit Email" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Reservation Email
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtAlternateEmail" runat="server" placeholder="Group Email" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Address Line 1
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtAddress1" runat="server" placeholder="Address Line 1" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Address Line 2
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtAddress2" runat="server" placeholder="Address Line 2" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        City
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtCity" runat="server" placeholder="City" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Country
                                                                    </label>
                                                                    <label class="input">
                                                                        <%--<asp:TextBox ID="txtCountry" runat="server" placeholder="Country" validationgroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>--%>
                                                                        <select id="cmbcountry" class="form-control" runat="server">

                                                                            <option value="Afghanistan">Afghanistan</option>
                                                                            <option value="Albania">Albania</option>
                                                                            <option value="Algeria">Algeria</option>
                                                                            <option value="American Samoa">American Samoa</option>
                                                                            <option value="Andorra">Andorra</option>
                                                                            <option value="Angola">Angola</option>
                                                                            <option value="Anguilla">Anguilla</option>
                                                                            <option value="Antartica">Antarctica</option>
                                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                            <option value="Argentina">Argentina</option>
                                                                            <option value="Armenia">Armenia</option>
                                                                            <option value="Aruba">Aruba</option>
                                                                            <option value="Australia">Australia</option>
                                                                            <option value="Austria">Austria</option>
                                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                                            <option value="Bahamas">Bahamas</option>
                                                                            <option value="Bahrain">Bahrain</option>
                                                                            <option value="Bangladesh">Bangladesh</option>
                                                                            <option value="Barbados">Barbados</option>
                                                                            <option value="Belarus">Belarus</option>
                                                                            <option value="Belgium">Belgium</option>
                                                                            <option value="Belize">Belize</option>
                                                                            <option value="Benin">Benin</option>
                                                                            <option value="Bermuda">Bermuda</option>
                                                                            <option value="Bhutan">Bhutan</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                            <option value="Botswana">Botswana</option>
                                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                                            <option value="Brazil">Brazil</option>
                                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                            <option value="Bulgaria">Bulgaria</option>
                                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                                            <option value="Burundi">Burundi</option>
                                                                            <option value="Cambodia">Cambodia</option>
                                                                            <option value="Cameroon">Cameroon</option>
                                                                            <option value="Canada">Canada</option>
                                                                            <option value="Cape Verde">Cape Verde</option>
                                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                                            <option value="Central African Republic">Central African Republic</option>
                                                                            <option value="Chad">Chad</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="China">China</option>
                                                                            <option value="Christmas Island">Christmas Island</option>
                                                                            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Comoros">Comoros</option>
                                                                            <option value="Congo">Congo</option>
                                                                            <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                            <option value="Cook Islands">Cook Islands</option>
                                                                            <option value="Costa Rica">Costa Rica</option>
                                                                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                            <option value="Croatia">Croatia (Hrvatska)</option>
                                                                            <option value="Cuba">Cuba</option>
                                                                            <option value="Cyprus">Cyprus</option>
                                                                            <option value="Czech Republic">Czech Republic</option>
                                                                            <option value="Denmark">Denmark</option>
                                                                            <option value="Djibouti">Djibouti</option>
                                                                            <option value="Dominica">Dominica</option>
                                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                                            <option value="East Timor">East Timor</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Egypt">Egypt</option>
                                                                            <option value="El Salvador">El Salvador</option>
                                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                            <option value="Eritrea">Eritrea</option>
                                                                            <option value="Estonia">Estonia</option>
                                                                            <option value="Ethiopia">Ethiopia</option>
                                                                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                                            <option value="Fiji">Fiji</option>
                                                                            <option value="Finland">Finland</option>
                                                                            <option value="France">France</option>
                                                                            <option value="France Metropolitan">France, Metropolitan</option>
                                                                            <option value="French Guiana">French Guiana</option>
                                                                            <option value="French Polynesia">French Polynesia</option>
                                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                                            <option value="Gabon">Gabon</option>
                                                                            <option value="Gambia">Gambia</option>
                                                                            <option value="Georgia">Georgia</option>
                                                                            <option value="Germany">Germany</option>
                                                                            <option value="Ghana">Ghana</option>
                                                                            <option value="Gibraltar">Gibraltar</option>
                                                                            <option value="Greece">Greece</option>
                                                                            <option value="Greenland">Greenland</option>
                                                                            <option value="Grenada">Grenada</option>
                                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                                            <option value="Guam">Guam</option>
                                                                            <option value="Guatemala">Guatemala</option>
                                                                            <option value="Guinea">Guinea</option>
                                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                            <option value="Guyana">Guyana</option>
                                                                            <option value="Haiti">Haiti</option>
                                                                            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                            <option value="Holy See">Holy See (Vatican City State)</option>
                                                                            <option value="Honduras">Honduras</option>
                                                                            <option value="Hong Kong">Hong Kong</option>
                                                                            <option value="Hungary">Hungary</option>
                                                                            <option value="Iceland">Iceland</option>
                                                                            <option value="India">India</option>
                                                                            <option value="Indonesia">Indonesia</option>
                                                                            <option value="Iran">Iran (Islamic Republic of)</option>
                                                                            <option value="Iraq">Iraq</option>
                                                                            <option value="Ireland">Ireland</option>
                                                                            <option value="Israel">Israel</option>
                                                                            <option value="Italy">Italy</option>
                                                                            <option value="Jamaica">Jamaica</option>
                                                                            <option value="Japan">Japan</option>
                                                                            <option value="Jordan">Jordan</option>
                                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                                            <option value="Kenya">Kenya</option>
                                                                            <option value="Kiribati">Kiribati</option>
                                                                            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                            <option value="Korea">Korea, Republic of</option>
                                                                            <option value="Kuwait">Kuwait</option>
                                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                            <option value="Lao">Lao People's Democratic Republic</option>
                                                                            <option value="Latvia">Latvia</option>
                                                                            <option value="Lebanon">Lebanon</option>
                                                                            <option value="Lesotho">Lesotho</option>
                                                                            <option value="Liberia">Liberia</option>
                                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                                            <option value="Lithuania">Lithuania</option>
                                                                            <option value="Luxembourg">Luxembourg</option>
                                                                            <option value="Macau">Macau</option>
                                                                            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                            <option value="Madagascar">Madagascar</option>
                                                                            <option value="Malawi">Malawi</option>
                                                                            <option value="Malaysia">Malaysia</option>
                                                                            <option value="Maldives">Maldives</option>
                                                                            <option value="Mali">Mali</option>
                                                                            <option value="Malta">Malta</option>
                                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                                            <option value="Martinique">Martinique</option>
                                                                            <option value="Mauritania">Mauritania</option>
                                                                            <option value="Mauritius">Mauritius</option>
                                                                            <option value="Mayotte">Mayotte</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Micronesia">Micronesia, Federated States of</option>
                                                                            <option value="Moldova">Moldova, Republic of</option>
                                                                            <option value="Monaco">Monaco</option>
                                                                            <option value="Mongolia">Mongolia</option>
                                                                            <option value="Montserrat">Montserrat</option>
                                                                            <option value="Morocco">Morocco</option>
                                                                            <option value="Mozambique">Mozambique</option>
                                                                            <option value="Myanmar">Myanmar</option>
                                                                            <option value="Namibia">Namibia</option>
                                                                            <option value="Nauru">Nauru</option>
                                                                            <option value="Nepal">Nepal</option>
                                                                            <option value="Netherlands">Netherlands</option>
                                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                            <option value="New Caledonia">New Caledonia</option>
                                                                            <option value="New Zealand">New Zealand</option>
                                                                            <option value="Nicaragua">Nicaragua</option>
                                                                            <option value="Niger">Niger</option>
                                                                            <option value="Nigeria">Nigeria</option>
                                                                            <option value="Niue">Niue</option>
                                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                            <option value="Norway">Norway</option>
                                                                            <option value="Oman">Oman</option>
                                                                            <option value="Pakistan">Pakistan</option>
                                                                            <option value="Palau">Palau</option>
                                                                            <option value="Panama">Panama</option>
                                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Philippines">Philippines</option>
                                                                            <option value="Pitcairn">Pitcairn</option>
                                                                            <option value="Poland">Poland</option>
                                                                            <option value="Portugal">Portugal</option>
                                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                                            <option value="Qatar">Qatar</option>
                                                                            <option value="Reunion">Reunion</option>
                                                                            <option value="Romania">Romania</option>
                                                                            <option value="Russia">Russian Federation</option>
                                                                            <option value="Rwanda">Rwanda</option>
                                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                            <option value="Saint LUCIA">Saint LUCIA</option>
                                                                            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                            <option value="Samoa">Samoa</option>
                                                                            <option value="San Marino">San Marino</option>
                                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                                            <option value="Senegal">Senegal</option>
                                                                            <option value="Seychelles">Seychelles</option>
                                                                            <option value="Sierra">Sierra Leone</option>
                                                                            <option value="Singapore">Singapore</option>
                                                                            <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                            <option value="Slovenia">Slovenia</option>
                                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                                            <option value="Somalia">Somalia</option>
                                                                            <option value="South Africa">South Africa</option>
                                                                            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                            <option value="Span">Spain</option>
                                                                            <option value="SriLanka">Sri Lanka</option>
                                                                            <option value="St. Helena">St. Helena</option>
                                                                            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                            <option value="Sudan">Sudan</option>
                                                                            <option value="Suriname">Suriname</option>
                                                                            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="Swaziland">Swaziland</option>
                                                                            <option value="Sweden">Sweden</option>
                                                                            <option value="Switzerland">Switzerland</option>
                                                                            <option value="Syria">Syrian Arab Republic</option>
                                                                            <option value="Taiwan">Taiwan, Province of China</option>
                                                                            <option value="Tajikistan">Tajikistan</option>
                                                                            <option value="Tanzania">Tanzania, United Republic of</option>
                                                                            <option value="Thailand">Thailand</option>
                                                                            <option value="Togo">Togo</option>
                                                                            <option value="Tokelau">Tokelau</option>
                                                                            <option value="Tonga">Tonga</option>
                                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                            <option value="Tunisia">Tunisia</option>
                                                                            <option value="Turkey">Turkey</option>
                                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                                            <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                            <option value="Tuvalu">Tuvalu</option>
                                                                            <option value="Uganda">Uganda</option>
                                                                            <option value="Ukraine">Ukraine</option>
                                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                                            <option value="United Kingdom" selected>United Kingdom</option>
                                                                            <option value="United States">United States</option>
                                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                                            <option value="Vanuatu">Vanuatu</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Vietnam">Viet Nam</option>
                                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                            <option value="Western Sahara">Western Sahara</option>
                                                                            <option value="Yemen">Yemen</option>
                                                                            <option value="Yugoslavia">Yugoslavia</option>
                                                                            <option value="Zambia">Zambia</option>
                                                                            <option value="Zimbabwe">Zimbabwe</option>



                                                                        </select>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Postal Code
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtPostalCode" runat="server" placeholder="Postal Code" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Black List
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtBlackList" runat="server" placeholder="Black List" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Credit To Let's Travel
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtCreditToLetsTravel" runat="server" placeholder="Credit To Let's Travel" ValidationGroup="addBasicgroup" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Rating
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlRating" runat="server" class="form-control">
                                                                            <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">

                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Contract Start Date
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtContractStartDate" runat="server" placeholder="Contract Start Date" ValidationGroup="addBasicgroup" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Contract Ending Date
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtContractEndingDate" runat="server" placeholder="Contract Ending Date" ValidationGroup="addBasicgroup" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Contract Renewal Date
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtContractRenewalDate" runat="server" placeholder="Contract Renewal Date" ValidationGroup="addBasicgroup" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <%--   <section class="col col-4">
                                                                    <label class="input">
                                                                        Main Business
                                                                <CKEditor:CKEditorControl ID="cke_txtMainBusiness" validationgroup="addBasicgroup" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>--%>
                                                                <section class="col col-4">
                                                                    <label class="input">
                                                                        Comments
                                                                <CKEditor:CKEditorControl ID="cke_txtComments" ValidationGroup="addBasicgroup" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="input">
                                                                        PaymentTerms
                                                                  <CKEditor:CKEditorControl ID="cke_txtPaymentTerms" ValidationGroup="addBasicgroup" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                  </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="input">
                                                                        Upload Contract and related documents
                                                                        <asp:FileUpload class="btn btn-default" ID="flContractDoc" runat="server"></asp:FileUpload>
                                                                        <uc:File runat="server" ID="ucView" Visible="False"></uc:File>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <uc:MainContact runat="server" ID="ucMainContact"></uc:MainContact>

                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnDMCBasicSave" runat="server" validationgroup="addBasicgroup" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnSuppliersDetailsSave_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <button id="btnCancel" runat="server" causesvalidation="false" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnCancel_ServerClick">
                                                                <span style="width: 80px">Cancel </span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                </div>

                                            </div>
                                            <div id="tab2_pan" runat="server" class="tab-pane">
                                                <div id="SupplierHotelBook" class="smart-form">
                                                    <fieldset>
                                                        <div class="row">
                                                            <section class="col col-12">
                                                                <label class="input">
                                                                    How To Book
                                                                        <CKEditor:CKEditorControl ID="ckHowToBook" BasePath="~/ckeditor" runat="server" Height="300" Width="800" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                </label>
                                                            </section>
                                                        </div>
                                                    </fieldset>
                                                    <footer>
                                                        <button id="btnHowTobookSave" runat="server" onserverclick="btnHowTobookSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                            <span style="width: 80px">Save</span>
                                                        </button>
                                                    </footer>
                                                </div>
                                            </div>

                                            <div id="tab3_pan" runat="server" class="tab-pane">

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <div id="SupplierInformationform" class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Supplier Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlInfo_supplierName" runat="server" ValidationGroup="addSupplierInformation" class="form-control" onchange="return fn_supplierChange(this,'ddlsuppliertype','cboCompanyID');"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator SetFocusOnError="true"  ID="rfvsupplierName" ControlToValidate="ddlInfo_supplierName" ValidationGroup="addSupplierInformation" InitialValue="0" runat="server" ErrorMessage="Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Telehphone
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_telehphone" data-mask="(999) 999-99999" runat="server" placeholder="Telehphone" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Address Line 1
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_address1" runat="server" placeholder="Address Line 1" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Address Line 2
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_address2" runat="server" placeholder="Address Line 2" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        City
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_city" runat="server" placeholder="City" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Country
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_country" runat="server" placeholder="Country" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Postal Code
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_postal" runat="server" placeholder="Postal Code" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">

                                                                <section class="col col-4">
                                                                    <label class="label">
                                                                        Currency
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtInfo_currency" runat="server" placeholder="Currency" ValidationGroup="addSupplierInformation" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="input">
                                                                        Remarks
                                                                        <CKEditor:CKEditorControl ID="cke_txtHotelInfoRemark" ValidationGroup="addSuppliersDetails" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnSupplierInformationSave" runat="server" validationgroup="addSupplierInformation" onserverclick="btnSupplierInformationSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                        </footer>
                                                    </div>

                                                </div>

                                            </div>
                                            <div id="tab4_pan" runat="server" class="tab-pane">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="DMCRate" class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Type Of Food
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtRate_TypeOfFood" runat="server" placeholder="Type Of Food" ValidationGroup="addSupplierRestaurantRate" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>

                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Rate
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtRate" runat="server" placeholder="Rate" ValidationGroup="addSupplierRestaurantRate" Text="" class="form-control"></asp:TextBox>
                                                                          <asp:RegularExpressionValidator ID="rgx" Font-Bold="true" ForeColor="Red" ControlToValidate="txtRate" runat="server" ValidationGroup="addSupplierRestaurantRate"  SetFocusOnError="true" Text="Only Digit & decimal point allowed."
                                                                            ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*"></asp:RegularExpressionValidator>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Sample Menu File
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:FileUpload class="btn btn-default" ID="flSampleMenu" runat="server"></asp:FileUpload>
                                                                        <uc:SampleMenuFile runat="server" ID="ucsample" Visible="False"></uc:SampleMenuFile >

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Food Picture
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:FileUpload class="btn btn-default" ID="flFoodPicture" runat="server"></asp:FileUpload>
                                                                        <uc:FoodPhoto ID="ucFoodPhoto" runat="server" Visible="false" />
                                                                    </label>
                                                                </section>
                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Description
                                                                    </label>
                                                                    <label class="input">
                                                                        <CKEditor:CKEditorControl ID="txtRate_Description" ValidationGroup="addSupplierRestaurantRate" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-5">
                                                                    <label class="label">
                                                                        Comments
                                                                    </label>
                                                                    <label class="input">
                                                                        <CKEditor:CKEditorControl ID="txtRate_Comments" ValidationGroup="addSupplierRestaurantRate" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnSupplierRateSave" runat="server" onserverclick="btnSupplierRateSave_ServerClick" validationgroup="addSupplierRestaurantRate" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                        <asp:Repeater ID="SupplierRateRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                            <HeaderTemplate>
                                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="No" />
                                                                            </th> 
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Type Of Food" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Description" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Rate/Person" />
                                                                            </th> 
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Samplemenufile" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Comments" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="Foodpicturefile" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 10%"></th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>ID</th> 
                                                                            <th>Type Of Food</th>
                                                                            <th>Description</th>
                                                                            <th>Rate/person</th>
                                                                            <th>Sample Menu</th>
                                                                            <th data-hide="phone">Comments</th>
                                                                            <th data-hide="phone">Food Picture</th> 
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td> 
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "TypeOfFood")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "Description")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "Rate")%></td> 
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "SampleMenuFile")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "Comments")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "FoodPictureFile")%></td> 
                                                                    <td>
                                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true">
                                                                            <i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                                                                                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
                                                                            <i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                            </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                    </div>

                                                </div>
                                            </div>
                                            <div id="tab5_pan" runat="server" class="tab-pane">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="wholesalertContact" class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <h3 class="text-primary">Contact Person</h3>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-2">
                                                                    <label class="label">
                                                                        Title
                                                                    </label>
                                                                    <label class="input">
                                                                        <select id="ddlTitle1" class="form-control" runat="server">
                                                                            <option>Mr</option>
                                                                            <option>Ms</option>
                                                                            <option>Mrs</option>
                                                                        </select>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                </section>
                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Surname
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>
                                                                        <asp:TextBox ID="txtSurName1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Sur Name" Text=""></asp:TextBox>
                                                                        <asp:RequiredFieldValidator  SetFocusOnError="true" ID="rfvtxtSurName1" ValidationGroup="addSubClientContact" ControlToValidate="txtSurName1" runat="server" ErrorMessage="Last Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>


                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        First Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>
                                                                        <asp:TextBox ID="txtFirstName1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="First Name" Text=""></asp:TextBox>
                                                                        <asp:RequiredFieldValidator  SetFocusOnError="true" ID="RequiredFieldValidator2" ValidationGroup="addSubClientContact" ControlToValidate="txtFirstName1" runat="server" ErrorMessage="First Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Position
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-map-marker"></i>
                                                                        <asp:TextBox ID="txtPosition1" class="form-control" placeholder="Position" ValidationGroup="addSubClientContact" runat="server">
                                                                        </asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Department
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa-building-o"></i>
                                                                        <asp:TextBox ID="txtDepartment1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Department" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Email
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-envelope-o"></i>

                                                                        <asp:TextBox ID="txtcontactEmail1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Email" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Phone Number
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-phone"></i>
                                                                        <asp:TextBox ID="txtcontactTelephone1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addSubClientContact" placeholder="Phone Number" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Mobile Number
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-mobile"></i>
                                                                        <asp:TextBox ID="txtcontactMobile1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addSubClientContact" placeholder="Mobile Number" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Fax Number
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-fax"></i>
                                                                        <asp:TextBox ID="txtcontactFax1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Fax Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        WeChat
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-wechat"></i>
                                                                        <asp:TextBox ID="txtWeChat1" runat="server" class="form-control" placeholder="Enter WeChat" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        What's up
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-whatsapp"></i>
                                                                        <asp:TextBox ID="txtWhatsapp1" runat="server" class="form-control" placeholder="Enter What's up" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Skype
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-skype"></i>
                                                                        <asp:TextBox ID="txtSkype1" runat="server" class="form-control" placeholder="Enter Skype" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Facebook
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-facebook"></i>
                                                                        <asp:TextBox ID="txtFacebook1" runat="server" class="form-control" placeholder="Enter Facebook" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Twitter
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-twitter"></i>

                                                                        <asp:TextBox ID="txtTwitter1" runat="server" class="form-control" placeholder="Enter Twitter" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Linked in
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-linkedin"></i>
                                                                        <asp:TextBox ID="txtLinkedin1" runat="server" class="form-control" placeholder="Enter Linked in" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnotherContactSave" runat="server" validationgroup="addSubClientContact" onserverclick="btnotherContactSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <button id="btnotherContactCancel" runat="server" causesvalidation="false" validationgroup="addSubClientContact" onserverclick="btnotherContactCancel_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Cancel</span>
                                                            </button>
                                                        </footer>
                                                    </div>

                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <asp:Repeater ID="rptSupplierContactRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                        <HeaderTemplate>
                                                            <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="hasinput" style="width: 8%">
                                                                            <input type="text" class="form-control" placeholder="No" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 20%">
                                                                            <input type="text" class="form-control" placeholder="Sur Name" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 15%">
                                                                            <input type="text" class="form-control" placeholder="First Name" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 15%">
                                                                            <input type="text" class="form-control" placeholder="Email" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 15%">
                                                                            <input type="text" class="form-control" placeholder="Department" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 10%">
                                                                            <input type="text" class="form-control" placeholder="Contact No" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 12%"></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th data-hide="phone">No</th>
                                                                        <th data-class="expand">Sur Name</th>
                                                                        <th data-hide="phone">First Name</th>
                                                                        <th data-hide="phone">Email</th>
                                                                        <th data-hide="phone">Department</th>
                                                                        <th data-hide="phone">Contact No</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "SurName")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "FirstName")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "Email")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "Department")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "MobileNo")%></td>
                                                                <td>
                                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEditContact" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonEditContact_Click"><i class="fa fa-edit"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDeleteContact" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonDeleteContact_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record, it will delete client contact?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                </td>
                                                            </tr>

                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>


        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" data-dismiss="modal">Close</a>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

