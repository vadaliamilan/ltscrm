﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="SupplierMaster.aspx.cs" Inherits="LTS_CRM.Supplier.SupplierMaster" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Literal ID="output" runat="server" />
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Supplier Management</li>
                    <li>Master</li>
                </ol>
            </div>
            <p>
                &nbsp;
            </p>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Supplier Master List</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <div class="row">
                                            <div class="col-xs-9 col-sm-7 col-md-8 col-lg-8 text-left">
                                                <label>
                                                    Company:
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged"></asp:DropDownList>
                                                </label>
                                                <label>
                                                    &nbsp;&nbsp;Supplier Type:
                                                <asp:DropDownList ID="ddlSupplierType" AutoPostBack="true" OnSelectedIndexChanged="ddlSupplierType_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                                </label>
                                            </div>
                                            <div class="col-xs-3 col-sm-12 col-md-4 col-lg-4 text-right">
                                                <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="addSupplierMaster" OnClick="addSupplierMaster_Click" runat="server"><i class="fa fa-plus"></i><span class="hidden-mobile">Add New</span></asp:LinkButton>
                                            </div>
                                        </div>

                                        <asp:Repeater ID="SupplierMasterRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                            <HeaderTemplate>
                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                    <thead>
                                                        <tr>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="No" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Name" />
                                                            </th>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Type" />
                                                            </th>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="City" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Telephone" />
                                                            </th>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Email" />
                                                            </th>

                                                            <th class="hasinput" style="width: 10%"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No</th>
                                                            <th data-class="expand">Name</th>
                                                            <th>SupplierType</th>
                                                            <th data-hide="phone">Country</th>
                                                            <th data-hide="phone">Telephone</th>
                                                            <th data-hide="phone">Email</th>

                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>

                                                    <td><%# DataBinder.Eval(Container.DataItem, "Name")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "SupplierType")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "City")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Telephone")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Email")%></td>
                                                    <td>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" OnClick="buttonEdit_Click" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true">
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" OnClick="buttonDelete_Click" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>      
            </table>                      
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
            <p>
                &nbsp;
            </p>

        </div>
    </div>
</asp:Content>
