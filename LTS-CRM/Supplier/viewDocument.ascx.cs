﻿using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class viewDocument : LTSUserControl
    {
       
        public string fileName{get;set;}
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(!String.IsNullOrEmpty(fileName))
                {
                    lnkView.Text = fileName;
                }
            }
        }
       public byte[] FileState {
           get
           {
               if (ViewState["filebyte"] != null)
                   return (byte[])ViewState["filebyte"];
               else return null;

           }
           set
           {
               ViewState["filebyte"] = value;
           }
       }
        public void BindDocLink(Byte[] FileData)
        {
            this.Visible = true;
            lnkView.Visible = true;
            FileState = FileData;
        }

        protected void lnkView_Click(object sender, EventArgs e)
        {
            if (FileState != null)
            {
                Byte[] data = (Byte[])FileState;

                //Stream stream = new MemoryStream(data);

                string mimetype=Mime.GetMimeFromBytes(data);
                if (mimetype == "image/pjpeg")
                    mimetype = "image/jpeg";
                string ext="";
                ext=Mime.GetExtensionFromMime(mimetype,out ext);
                // Send the file to the browser
                Response.AddHeader("Content-type", mimetype);
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + "Document" + ext);
                Response.BinaryWrite(data);
                Response.Flush();
                Response.End();
            }
        }
    }
}