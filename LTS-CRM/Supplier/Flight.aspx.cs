﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class Flight : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                ClearTabCookies();
                BindGrid();
        }
        }
        protected void BindGrid()
        {
            DataTable dataTableFlight = BLSupplierFlight.GetSupplierFlightList();
            FlightRepeater.DataSource = dataTableFlight;
            FlightRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.FlightID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/FlightAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int flightID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierFlight.DeleteSupplierFlight(flightID);
            if (isSuccess)
                BindGrid();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.FlightID = null;
            Response.Redirect("~/Supplier/FlightAddEdit.aspx", false);
        }
    }
}