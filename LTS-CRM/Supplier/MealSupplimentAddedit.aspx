﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" CodeBehind="MealSupplimentAddedit.aspx.cs" MasterPageFile="~/master/MasterAdmin.Master" Inherits="LTS_CRM.Supplier.MealSuppliementAddedit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Supplier Management</li>
                    <li>Meal Suppliment</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Meal Suppliment Add-Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <ul class="nav nav-tabs tabs-left" id="client-nav">
                                            <li class="bg-color-blueDark txt-color-white active" id="tab1" runat="server">
                                                <a data-toggle="tab" href="#tab1_pan">Basic Detail </a></li>
                                              <li class="bg-color-blueDark txt-color-white" id="tab2" runat="server">
                                                <a data-toggle="tab" href="#tab2_pan">How To Book</a></li>
                                            <li class="bg-color-blueDark txt-color-white" id="tab3" runat="server">
                                                <a data-toggle="tab" href="#tab3_pan">Contact</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1_pan" runat="server">
                                                <div class="row">
                                                    <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                        <div class="input-group">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="SupplierMealSupplementform" class="row smart-form">
                                                        <fieldset>
                                                             <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Company
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" validationgroup="addBasicgroup"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator SetFocusOnError="true"  ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" validationgroup="addBasicgroup" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Supplier Type
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList Enabled="false" class="form-control" ID="ddlsuppliertype" Height="30" runat="server"></asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Added By
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtAddedBy" runat="server" placeholder="Added By" ValidationGroup="addSupplierMealSupplement" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                               
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Meal Supplement Type
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlMealSupplementType" runat="server"  ValidationGroup="addSupplierMealSupplement" class="form-control">
                                                                            <asp:ListItem Value="Breakfast MS" >Breakfast MS</asp:ListItem>
                                                                            <asp:ListItem Value="Lunch MS" >Lunch MS</asp:ListItem>
                                                                            <asp:ListItem Value="Dinner MS" >Dinner MS</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList ID="ddlName" runat="server" validationgroup="addBasicgroup" class="form-control" onchange="return fn_supplierChange(this,'ddlsuppliertype','cboCompanyID');"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rfvddlName"  SetFocusOnError="true" ControlToValidate="ddlName" validationgroup="addBasicgroup" InitialValue="0" runat="server" ErrorMessage="Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                 <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Postal Code
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtPostalCode" runat="server" placeholder="Postal Code" ValidationGroup="addSupplierMealSupplement" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         City
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtCity" runat="server" placeholder="City" ValidationGroup="addSupplierMealSupplement" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         State
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtState" runat="server" placeholder="State" ValidationGroup="addSupplierMealSupplement" Text="" class="form-control"></asp:TextBox>
                                                                    </label>
                                                                </section> <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Country
                                                                    </label>
                                                                    <label class="input">
                                                                         
                                                                    <select id="cmbcountry" class="form-control" runat="server" >
                                                                    <option value="Afghanistan">Afghanistan</option>
                                                                    <option value="Albania">Albania</option>
                                                                    <option value="Algeria">Algeria</option>
                                                                    <option value="American Samoa">American Samoa</option>
                                                                    <option value="Andorra">Andorra</option>
                                                                    <option value="Angola">Angola</option>
                                                                    <option value="Anguilla">Anguilla</option>
                                                                    <option value="Antartica">Antarctica</option>
                                                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                    <option value="Argentina">Argentina</option>
                                                                    <option value="Armenia">Armenia</option>
                                                                    <option value="Aruba">Aruba</option>
                                                                    <option value="Australia">Australia</option>
                                                                    <option value="Austria">Austria</option>
                                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                                    <option value="Bahamas">Bahamas</option>
                                                                    <option value="Bahrain">Bahrain</option>
                                                                    <option value="Bangladesh">Bangladesh</option>
                                                                    <option value="Barbados">Barbados</option>
                                                                    <option value="Belarus">Belarus</option>
                                                                    <option value="Belgium">Belgium</option>
                                                                    <option value="Belize">Belize</option>
                                                                    <option value="Benin">Benin</option>
                                                                    <option value="Bermuda">Bermuda</option>
                                                                    <option value="Bhutan">Bhutan</option>
                                                                    <option value="Bolivia">Bolivia</option>
                                                                    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                    <option value="Botswana">Botswana</option>
                                                                    <option value="Bouvet Island">Bouvet Island</option>
                                                                    <option value="Brazil">Brazil</option>
                                                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                    <option value="Bulgaria">Bulgaria</option>
                                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                                    <option value="Burundi">Burundi</option>
                                                                    <option value="Cambodia">Cambodia</option>
                                                                    <option value="Cameroon">Cameroon</option>
                                                                    <option value="Canada">Canada</option>
                                                                    <option value="Cape Verde">Cape Verde</option>
                                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                                    <option value="Central African Republic">Central African Republic</option>
                                                                    <option value="Chad">Chad</option>
                                                                    <option value="Chile">Chile</option>
                                                                    <option value="China">China</option>
                                                                    <option value="Christmas Island">Christmas Island</option>
                                                                    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                    <option value="Colombia">Colombia</option>
                                                                    <option value="Comoros">Comoros</option>
                                                                    <option value="Congo">Congo</option>
                                                                    <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                    <option value="Cook Islands">Cook Islands</option>
                                                                    <option value="Costa Rica">Costa Rica</option>
                                                                    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                    <option value="Croatia">Croatia (Hrvatska)</option>
                                                                    <option value="Cuba">Cuba</option>
                                                                    <option value="Cyprus">Cyprus</option>
                                                                    <option value="Czech Republic">Czech Republic</option>
                                                                    <option value="Denmark">Denmark</option>
                                                                    <option value="Djibouti">Djibouti</option>
                                                                    <option value="Dominica">Dominica</option>
                                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                                    <option value="East Timor">East Timor</option>
                                                                    <option value="Ecuador">Ecuador</option>
                                                                    <option value="Egypt">Egypt</option>
                                                                    <option value="El Salvador">El Salvador</option>
                                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                    <option value="Eritrea">Eritrea</option>
                                                                    <option value="Estonia">Estonia</option>
                                                                    <option value="Ethiopia">Ethiopia</option>
                                                                    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                                    <option value="Fiji">Fiji</option>
                                                                    <option value="Finland">Finland</option>
                                                                    <option value="France">France</option>
                                                                    <option value="France Metropolitan">France, Metropolitan</option>
                                                                    <option value="French Guiana">French Guiana</option>
                                                                    <option value="French Polynesia">French Polynesia</option>
                                                                    <option value="French Southern Territories">French Southern Territories</option>
                                                                    <option value="Gabon">Gabon</option>
                                                                    <option value="Gambia">Gambia</option>
                                                                    <option value="Georgia">Georgia</option>
                                                                    <option value="Germany">Germany</option>
                                                                    <option value="Ghana">Ghana</option>
                                                                    <option value="Gibraltar">Gibraltar</option>
                                                                    <option value="Greece">Greece</option>
                                                                    <option value="Greenland">Greenland</option>
                                                                    <option value="Grenada">Grenada</option>
                                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                                    <option value="Guam">Guam</option>
                                                                    <option value="Guatemala">Guatemala</option>
                                                                    <option value="Guinea">Guinea</option>
                                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                    <option value="Guyana">Guyana</option>
                                                                    <option value="Haiti">Haiti</option>
                                                                    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                    <option value="Holy See">Holy See (Vatican City State)</option>
                                                                    <option value="Honduras">Honduras</option>
                                                                    <option value="Hong Kong">Hong Kong</option>
                                                                    <option value="Hungary">Hungary</option>
                                                                    <option value="Iceland">Iceland</option>
                                                                    <option value="India">India</option>
                                                                    <option value="Indonesia">Indonesia</option>
                                                                    <option value="Iran">Iran (Islamic Republic of)</option>
                                                                    <option value="Iraq">Iraq</option>
                                                                    <option value="Ireland">Ireland</option>
                                                                    <option value="Israel">Israel</option>
                                                                    <option value="Italy">Italy</option>
                                                                    <option value="Jamaica">Jamaica</option>
                                                                    <option value="Japan">Japan</option>
                                                                    <option value="Jordan">Jordan</option>
                                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                                    <option value="Kenya">Kenya</option>
                                                                    <option value="Kiribati">Kiribati</option>
                                                                    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                    <option value="Korea">Korea, Republic of</option>
                                                                    <option value="Kuwait">Kuwait</option>
                                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                    <option value="Lao">Lao People's Democratic Republic</option>
                                                                    <option value="Latvia">Latvia</option>
                                                                    <option value="Lebanon">Lebanon</option>
                                                                    <option value="Lesotho">Lesotho</option>
                                                                    <option value="Liberia">Liberia</option>
                                                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                                    <option value="Lithuania">Lithuania</option>
                                                                    <option value="Luxembourg">Luxembourg</option>
                                                                    <option value="Macau">Macau</option>
                                                                    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                    <option value="Madagascar">Madagascar</option>
                                                                    <option value="Malawi">Malawi</option>
                                                                    <option value="Malaysia">Malaysia</option>
                                                                    <option value="Maldives">Maldives</option>
                                                                    <option value="Mali">Mali</option>
                                                                    <option value="Malta">Malta</option>
                                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                                    <option value="Martinique">Martinique</option>
                                                                    <option value="Mauritania">Mauritania</option>
                                                                    <option value="Mauritius">Mauritius</option>
                                                                    <option value="Mayotte">Mayotte</option>
                                                                    <option value="Mexico">Mexico</option>
                                                                    <option value="Micronesia">Micronesia, Federated States of</option>
                                                                    <option value="Moldova">Moldova, Republic of</option>
                                                                    <option value="Monaco">Monaco</option>
                                                                    <option value="Mongolia">Mongolia</option>
                                                                    <option value="Montserrat">Montserrat</option>
                                                                    <option value="Morocco">Morocco</option>
                                                                    <option value="Mozambique">Mozambique</option>
                                                                    <option value="Myanmar">Myanmar</option>
                                                                    <option value="Namibia">Namibia</option>
                                                                    <option value="Nauru">Nauru</option>
                                                                    <option value="Nepal">Nepal</option>
                                                                    <option value="Netherlands">Netherlands</option>
                                                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                    <option value="New Caledonia">New Caledonia</option>
                                                                    <option value="New Zealand">New Zealand</option>
                                                                    <option value="Nicaragua">Nicaragua</option>
                                                                    <option value="Niger">Niger</option>
                                                                    <option value="Nigeria">Nigeria</option>
                                                                    <option value="Niue">Niue</option>
                                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                    <option value="Norway">Norway</option>
                                                                    <option value="Oman">Oman</option>
                                                                    <option value="Pakistan">Pakistan</option>
                                                                    <option value="Palau">Palau</option>
                                                                    <option value="Panama">Panama</option>
                                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                                    <option value="Paraguay">Paraguay</option>
                                                                    <option value="Peru">Peru</option>
                                                                    <option value="Philippines">Philippines</option>
                                                                    <option value="Pitcairn">Pitcairn</option>
                                                                    <option value="Poland">Poland</option>
                                                                    <option value="Portugal">Portugal</option>
                                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                                    <option value="Qatar">Qatar</option>
                                                                    <option value="Reunion">Reunion</option>
                                                                    <option value="Romania">Romania</option>
                                                                    <option value="Russia">Russian Federation</option>
                                                                    <option value="Rwanda">Rwanda</option>
                                                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                    <option value="Saint LUCIA">Saint LUCIA</option>
                                                                    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                    <option value="Samoa">Samoa</option>
                                                                    <option value="San Marino">San Marino</option>
                                                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                                    <option value="Senegal">Senegal</option>
                                                                    <option value="Seychelles">Seychelles</option>
                                                                    <option value="Sierra">Sierra Leone</option>
                                                                    <option value="Singapore">Singapore</option>
                                                                    <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                    <option value="Slovenia">Slovenia</option>
                                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                                    <option value="Somalia">Somalia</option>
                                                                    <option value="South Africa">South Africa</option>
                                                                    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                    <option value="Span">Spain</option>
                                                                    <option value="SriLanka">Sri Lanka</option>
                                                                    <option value="St. Helena">St. Helena</option>
                                                                    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                    <option value="Sudan">Sudan</option>
                                                                    <option value="Suriname">Suriname</option>
                                                                    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                    <option value="Swaziland">Swaziland</option>
                                                                    <option value="Sweden">Sweden</option>
                                                                    <option value="Switzerland">Switzerland</option>
                                                                    <option value="Syria">Syrian Arab Republic</option>
                                                                    <option value="Taiwan">Taiwan, Province of China</option>
                                                                    <option value="Tajikistan">Tajikistan</option>
                                                                    <option value="Tanzania">Tanzania, United Republic of</option>
                                                                    <option value="Thailand">Thailand</option>
                                                                    <option value="Togo">Togo</option>
                                                                    <option value="Tokelau">Tokelau</option>
                                                                    <option value="Tonga">Tonga</option>
                                                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                    <option value="Tunisia">Tunisia</option>
                                                                    <option value="Turkey">Turkey</option>
                                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                                    <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                    <option value="Tuvalu">Tuvalu</option>
                                                                    <option value="Uganda">Uganda</option>
                                                                    <option value="Ukraine">Ukraine</option>
                                                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                                                    <option value="United Kingdom" selected>United Kingdom</option>
                                                                    <option value="United States">United States</option>
                                                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                    <option value="Uruguay">Uruguay</option>
                                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                                    <option value="Vanuatu">Vanuatu</option>
                                                                    <option value="Venezuela">Venezuela</option>
                                                                    <option value="Vietnam">Viet Nam</option>
                                                                    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                    <option value="Western Sahara">Western Sahara</option>
                                                                    <option value="Yemen">Yemen</option>
                                                                    <option value="Yugoslavia">Yugoslavia</option>
                                                                    <option value="Zambia">Zambia</option>
                                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                                </select>
                                                                    </label>
                                                                </section>
                                                               
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    
                                                                    <label class="label"> 
                                                                         Purpose
                                                                    </label>
                                                                    <label class="input">
                                                                        
                                                                    <CKEditor:CKEditorControl ID="txtPurpose"  BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                  </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnSupplierMealSupplementSave" runat="server" validationgroup="addSupplierMealSupplement" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnMealDetails_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                                        </footer>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab2_pan" runat="server" class="tab-pane">
                                                 <div id="SupplierHotelBook" class="smart-form">
                                                        <fieldset>
                                                             
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <label class="input"> 
                                                                        How To Book
                                                                        <CKEditor:CKEditorControl ID="ckHowToBook" BasePath="~/ckeditor" runat="server" Height="300" Width="800" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnHowTobookSave" runat="server"   onserverclick="btnHowTobookSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save</span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                            </div>
                                            <div class="tab-pane" id="tab3_pan" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="wholesalertContact" class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-12">
                                                                    <h3 class="text-primary">Contact Person</h3>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-2">
                                                                    <label class="label"> 
                                                                         Title
                                                                    </label>
                                                                    <label class="input">
                                                                        <select id="ddlTitle1" class="form-control" runat="server">
                                                                            <option>Mr</option>
                                                                            <option>Ms</option>
                                                                            <option>Mrs</option>
                                                                        </select>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                </section>
                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Sur Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>
                                                                        <asp:TextBox ID="txtSurName1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Sur Name" Text=""></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvtxtSurName1" SetFocusOnError="true"  ValidationGroup="addSubClientContact" ControlToValidate="txtSurName1" runat="server" ErrorMessage="Last Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>


                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         First Name
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>
                                                                        <asp:TextBox ID="txtFirstName1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="First Name" Text=""></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  SetFocusOnError="true" ValidationGroup="addSubClientContact" ControlToValidate="txtFirstName1" runat="server" ErrorMessage="First Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Position
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-map-marker"></i>
                                                                        <asp:TextBox ID="txtPosition1" class="form-control" placeholder="Position" ValidationGroup="addSubClientContact" runat="server">
                                                                        </asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Department
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa-building-o"></i>
                                                                        <asp:TextBox ID="txtDepartment1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Department" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Email
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-envelope-o"></i>

                                                                        <asp:TextBox ID="txtcontactEmail1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Email" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Phone Number
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-phone"></i>
                                                                        <asp:TextBox ID="txtcontactTelephone1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addSubClientContact" placeholder="Phone Number" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Mobile Number
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-mobile"></i>
                                                                        <asp:TextBox ID="txtcontactMobile1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addSubClientContact" placeholder="Mobile Number" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Fax Number
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-fax"></i>
                                                                        <asp:TextBox ID="txtcontactFax1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Fax Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         WeChat
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-wechat"></i>

                                                                        <asp:TextBox ID="txtWeChat1" runat="server" class="form-control" placeholder="Enter WeChat" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         What's up
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-whatsapp"></i>
                                                                        <asp:TextBox ID="txtWhatsapp1" runat="server" class="form-control" placeholder="Enter What's up" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Skype
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-skype"></i>

                                                                        <asp:TextBox ID="txtSkype1" runat="server" class="form-control" placeholder="Enter Skype" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Facebook
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-facebook"></i>
                                                                        <asp:TextBox ID="txtFacebook1" runat="server" class="form-control" placeholder="Enter Facebook" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Twitter
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-twitter"></i>

                                                                        <asp:TextBox ID="txtTwitter1" runat="server" class="form-control" placeholder="Enter Twitter" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Linked in
                                                                    </label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-linkedin"></i>
                                                                        <asp:TextBox ID="txtLinkedin1" runat="server" class="form-control" placeholder="Enter Linked in" Text=""></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnotherContactSave" runat="server" validationgroup="addSubClientContact" onserverclick="btnotherContactSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <button id="btnotherContactCancel" runat="server" causesvalidation="false" validationgroup="addSubClientContact" onserverclick="btnotherContactCancel_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                <span style="width: 80px">Cancel</span>
                                                            </button>
                                                        </footer>
                                                    </div>

                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <asp:Repeater ID="rptClientContactRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                        <HeaderTemplate>
                                                            <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="hasinput" style="width: 8%">
                                                                            <input type="text" class="form-control" placeholder="No" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 20%">
                                                                            <input type="text" class="form-control" placeholder="Sur Name" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 15%">
                                                                            <input type="text" class="form-control" placeholder="First Name" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 15%">
                                                                            <input type="text" class="form-control" placeholder="Email" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 15%">
                                                                            <input type="text" class="form-control" placeholder="Department" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 10%">
                                                                            <input type="text" class="form-control" placeholder="Contact No" />
                                                                        </th>
                                                                        <th class="hasinput" style="width: 12%"></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th data-hide="phone">No</th>
                                                                        <th data-class="expand">Sur Name</th>
                                                                        <th data-hide="phone">First Name</th>
                                                                        <th data-hide="phone">Email</th>
                                                                        <th data-hide="phone">Department</th>
                                                                        <th data-hide="phone">Contact No</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%# Container.ItemIndex + 1 %></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "SurName")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "FirstName")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "Email")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "Department")%></td>
                                                                <td><%# DataBinder.Eval(Container.DataItem, "MobileNo")%></td>
                                                                <td>
                                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEditContact" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonEditContact_Click"><i class="fa fa-edit"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDeleteContact" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonDeleteContact_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record, it will delete client contact?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                </td>
                                                            </tr>

                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </article>
                </div>
            </section>

            <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
