﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="SupplierMasterAddEdit.aspx.cs" Inherits="LTS_CRM.Supplier.SupplierMasterAddEdit" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Literal ID="output" runat="server" />
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Supplier Management</li>
                    <li>Master</li>
                </ol>
            </div>
            <p>
                &nbsp;
            </p>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Supplier Master Add Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-5 col-md-4 col-lg-4">
                                            </div>
                                            <div class="col-xs-3 col-sm-5 col-md-4 col-lg-4">
                                            </div>
                                            <div class="col-xs-3 col-sm-12 col-md-4 col-lg-4 text-right">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div id="SupplierMasterform" class="row smart-form">
                                                    <fieldset>
                                                         <div class="row">
                                                             <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Company
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addSupplierMaster"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator  SetFocusOnError="true" ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addSupplierMaster" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                              
                                                                <section class="col col-3">
                                                                    <label class="label"> 
                                                                         Supplier Type
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="ddlsuppliertype" ValidationGroup="addSupplierMaster" Height="30" runat="server"></asp:DropDownList>
                                                                    <asp:RequiredFieldValidator  SetFocusOnError="true" ID="rfvType" InitialValue="0" ControlToValidate="ddlsuppliertype" ValidationGroup="addSupplierMaster" runat="server" ErrorMessage="Supplier Type Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                             </div>
                                                        <div class="row">
                                                              
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Name
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtName" runat="server" placeholder="Name" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator  SetFocusOnError="true" ID="rfvName" runat="server" ControlToValidate="txtName" ValidationGroup="addSupplierMaster" ErrorMessage="Name is Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Address Line 1
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtAddress1" runat="server" placeholder="Address Line 1" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator  SetFocusOnError="true" ID="rfvAddress1" runat="server" ControlToValidate="txtAddress1" ValidationGroup="addSupplierMaster" ErrorMessage="Address Line 1 is Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                </label>
                                                            </section>
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Address Line 2
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtAddress2" runat="server" placeholder="Address Line 2" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>

                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         City
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtCity" runat="server" placeholder="City" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         State
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtState" runat="server" placeholder="State" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Country
                                                                    </label>
                                                                <label class="input">
                                                                    
                                                                    <select id="cmbcountry" class="form-control" runat="server" >

                                                                    <option value="Afghanistan">Afghanistan</option>
                                                                    <option value="Albania">Albania</option>
                                                                    <option value="Algeria">Algeria</option>
                                                                    <option value="American Samoa">American Samoa</option>
                                                                    <option value="Andorra">Andorra</option>
                                                                    <option value="Angola">Angola</option>
                                                                    <option value="Anguilla">Anguilla</option>
                                                                    <option value="Antartica">Antarctica</option>
                                                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                    <option value="Argentina">Argentina</option>
                                                                    <option value="Armenia">Armenia</option>
                                                                    <option value="Aruba">Aruba</option>
                                                                    <option value="Australia">Australia</option>
                                                                    <option value="Austria">Austria</option>
                                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                                    <option value="Bahamas">Bahamas</option>
                                                                    <option value="Bahrain">Bahrain</option>
                                                                    <option value="Bangladesh">Bangladesh</option>
                                                                    <option value="Barbados">Barbados</option>
                                                                    <option value="Belarus">Belarus</option>
                                                                    <option value="Belgium">Belgium</option>
                                                                    <option value="Belize">Belize</option>
                                                                    <option value="Benin">Benin</option>
                                                                    <option value="Bermuda">Bermuda</option>
                                                                    <option value="Bhutan">Bhutan</option>
                                                                    <option value="Bolivia">Bolivia</option>
                                                                    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                    <option value="Botswana">Botswana</option>
                                                                    <option value="Bouvet Island">Bouvet Island</option>
                                                                    <option value="Brazil">Brazil</option>
                                                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                    <option value="Bulgaria">Bulgaria</option>
                                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                                    <option value="Burundi">Burundi</option>
                                                                    <option value="Cambodia">Cambodia</option>
                                                                    <option value="Cameroon">Cameroon</option>
                                                                    <option value="Canada">Canada</option>
                                                                    <option value="Cape Verde">Cape Verde</option>
                                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                                    <option value="Central African Republic">Central African Republic</option>
                                                                    <option value="Chad">Chad</option>
                                                                    <option value="Chile">Chile</option>
                                                                    <option value="China">China</option>
                                                                    <option value="Christmas Island">Christmas Island</option>
                                                                    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                    <option value="Colombia">Colombia</option>
                                                                    <option value="Comoros">Comoros</option>
                                                                    <option value="Congo">Congo</option>
                                                                    <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                    <option value="Cook Islands">Cook Islands</option>
                                                                    <option value="Costa Rica">Costa Rica</option>
                                                                    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                    <option value="Croatia">Croatia (Hrvatska)</option>
                                                                    <option value="Cuba">Cuba</option>
                                                                    <option value="Cyprus">Cyprus</option>
                                                                    <option value="Czech Republic">Czech Republic</option>
                                                                    <option value="Denmark">Denmark</option>
                                                                    <option value="Djibouti">Djibouti</option>
                                                                    <option value="Dominica">Dominica</option>
                                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                                    <option value="East Timor">East Timor</option>
                                                                    <option value="Ecuador">Ecuador</option>
                                                                    <option value="Egypt">Egypt</option>
                                                                    <option value="El Salvador">El Salvador</option>
                                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                    <option value="Eritrea">Eritrea</option>
                                                                    <option value="Estonia">Estonia</option>
                                                                    <option value="Ethiopia">Ethiopia</option>
                                                                    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                                    <option value="Fiji">Fiji</option>
                                                                    <option value="Finland">Finland</option>
                                                                    <option value="France">France</option>
                                                                    <option value="France Metropolitan">France, Metropolitan</option>
                                                                    <option value="French Guiana">French Guiana</option>
                                                                    <option value="French Polynesia">French Polynesia</option>
                                                                    <option value="French Southern Territories">French Southern Territories</option>
                                                                    <option value="Gabon">Gabon</option>
                                                                    <option value="Gambia">Gambia</option>
                                                                    <option value="Georgia">Georgia</option>
                                                                    <option value="Germany">Germany</option>
                                                                    <option value="Ghana">Ghana</option>
                                                                    <option value="Gibraltar">Gibraltar</option>
                                                                    <option value="Greece">Greece</option>
                                                                    <option value="Greenland">Greenland</option>
                                                                    <option value="Grenada">Grenada</option>
                                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                                    <option value="Guam">Guam</option>
                                                                    <option value="Guatemala">Guatemala</option>
                                                                    <option value="Guinea">Guinea</option>
                                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                    <option value="Guyana">Guyana</option>
                                                                    <option value="Haiti">Haiti</option>
                                                                    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                    <option value="Holy See">Holy See (Vatican City State)</option>
                                                                    <option value="Honduras">Honduras</option>
                                                                    <option value="Hong Kong">Hong Kong</option>
                                                                    <option value="Hungary">Hungary</option>
                                                                    <option value="Iceland">Iceland</option>
                                                                    <option value="India">India</option>
                                                                    <option value="Indonesia">Indonesia</option>
                                                                    <option value="Iran">Iran (Islamic Republic of)</option>
                                                                    <option value="Iraq">Iraq</option>
                                                                    <option value="Ireland">Ireland</option>
                                                                    <option value="Israel">Israel</option>
                                                                    <option value="Italy">Italy</option>
                                                                    <option value="Jamaica">Jamaica</option>
                                                                    <option value="Japan">Japan</option>
                                                                    <option value="Jordan">Jordan</option>
                                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                                    <option value="Kenya">Kenya</option>
                                                                    <option value="Kiribati">Kiribati</option>
                                                                    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                    <option value="Korea">Korea, Republic of</option>
                                                                    <option value="Kuwait">Kuwait</option>
                                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                    <option value="Lao">Lao People's Democratic Republic</option>
                                                                    <option value="Latvia">Latvia</option>
                                                                    <option value="Lebanon">Lebanon</option>
                                                                    <option value="Lesotho">Lesotho</option>
                                                                    <option value="Liberia">Liberia</option>
                                                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                                    <option value="Lithuania">Lithuania</option>
                                                                    <option value="Luxembourg">Luxembourg</option>
                                                                    <option value="Macau">Macau</option>
                                                                    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                    <option value="Madagascar">Madagascar</option>
                                                                    <option value="Malawi">Malawi</option>
                                                                    <option value="Malaysia">Malaysia</option>
                                                                    <option value="Maldives">Maldives</option>
                                                                    <option value="Mali">Mali</option>
                                                                    <option value="Malta">Malta</option>
                                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                                    <option value="Martinique">Martinique</option>
                                                                    <option value="Mauritania">Mauritania</option>
                                                                    <option value="Mauritius">Mauritius</option>
                                                                    <option value="Mayotte">Mayotte</option>
                                                                    <option value="Mexico">Mexico</option>
                                                                    <option value="Micronesia">Micronesia, Federated States of</option>
                                                                    <option value="Moldova">Moldova, Republic of</option>
                                                                    <option value="Monaco">Monaco</option>
                                                                    <option value="Mongolia">Mongolia</option>
                                                                    <option value="Montserrat">Montserrat</option>
                                                                    <option value="Morocco">Morocco</option>
                                                                    <option value="Mozambique">Mozambique</option>
                                                                    <option value="Myanmar">Myanmar</option>
                                                                    <option value="Namibia">Namibia</option>
                                                                    <option value="Nauru">Nauru</option>
                                                                    <option value="Nepal">Nepal</option>
                                                                    <option value="Netherlands">Netherlands</option>
                                                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                    <option value="New Caledonia">New Caledonia</option>
                                                                    <option value="New Zealand">New Zealand</option>
                                                                    <option value="Nicaragua">Nicaragua</option>
                                                                    <option value="Niger">Niger</option>
                                                                    <option value="Nigeria">Nigeria</option>
                                                                    <option value="Niue">Niue</option>
                                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                    <option value="Norway">Norway</option>
                                                                    <option value="Oman">Oman</option>
                                                                    <option value="Pakistan">Pakistan</option>
                                                                    <option value="Palau">Palau</option>
                                                                    <option value="Panama">Panama</option>
                                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                                    <option value="Paraguay">Paraguay</option>
                                                                    <option value="Peru">Peru</option>
                                                                    <option value="Philippines">Philippines</option>
                                                                    <option value="Pitcairn">Pitcairn</option>
                                                                    <option value="Poland">Poland</option>
                                                                    <option value="Portugal">Portugal</option>
                                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                                    <option value="Qatar">Qatar</option>
                                                                    <option value="Reunion">Reunion</option>
                                                                    <option value="Romania">Romania</option>
                                                                    <option value="Russia">Russian Federation</option>
                                                                    <option value="Rwanda">Rwanda</option>
                                                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                    <option value="Saint LUCIA">Saint LUCIA</option>
                                                                    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                    <option value="Samoa">Samoa</option>
                                                                    <option value="San Marino">San Marino</option>
                                                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                                    <option value="Senegal">Senegal</option>
                                                                    <option value="Seychelles">Seychelles</option>
                                                                    <option value="Sierra">Sierra Leone</option>
                                                                    <option value="Singapore">Singapore</option>
                                                                    <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                    <option value="Slovenia">Slovenia</option>
                                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                                    <option value="Somalia">Somalia</option>
                                                                    <option value="South Africa">South Africa</option>
                                                                    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                    <option value="Span">Spain</option>
                                                                    <option value="SriLanka">Sri Lanka</option>
                                                                    <option value="St. Helena">St. Helena</option>
                                                                    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                    <option value="Sudan">Sudan</option>
                                                                    <option value="Suriname">Suriname</option>
                                                                    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                    <option value="Swaziland">Swaziland</option>
                                                                    <option value="Sweden">Sweden</option>
                                                                    <option value="Switzerland">Switzerland</option>
                                                                    <option value="Syria">Syrian Arab Republic</option>
                                                                    <option value="Taiwan">Taiwan, Province of China</option>
                                                                    <option value="Tajikistan">Tajikistan</option>
                                                                    <option value="Tanzania">Tanzania, United Republic of</option>
                                                                    <option value="Thailand">Thailand</option>
                                                                    <option value="Togo">Togo</option>
                                                                    <option value="Tokelau">Tokelau</option>
                                                                    <option value="Tonga">Tonga</option>
                                                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                    <option value="Tunisia">Tunisia</option>
                                                                    <option value="Turkey">Turkey</option>
                                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                                    <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                    <option value="Tuvalu">Tuvalu</option>
                                                                    <option value="Uganda">Uganda</option>
                                                                    <option value="Ukraine">Ukraine</option>
                                                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                                                    <option value="United Kingdom" selected>United Kingdom</option>
                                                                    <option value="United States">United States</option>
                                                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                    <option value="Uruguay">Uruguay</option>
                                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                                    <option value="Vanuatu">Vanuatu</option>
                                                                    <option value="Venezuela">Venezuela</option>
                                                                    <option value="Vietnam">Viet Nam</option>
                                                                    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                    <option value="Western Sahara">Western Sahara</option>
                                                                    <option value="Yemen">Yemen</option>
                                                                    <option value="Yugoslavia">Yugoslavia</option>
                                                                    <option value="Zambia">Zambia</option>
                                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                                </select>
                                                                </label>
                                                            </section>

                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Telephone
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtTelephone" data-mask="(999) 999-99999" runat="server" placeholder="Telephone" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Postal Code
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtPostalCode" runat="server" placeholder="Postal Code" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>

                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Email
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>

                                                        </div>
                                                        <div class="row">
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Credit To Let's Travel
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtCreditToLetsTravel" runat="server" placeholder="Credit To Let's Travel" ValidationGroup="addSupplierMaster" Text="" class="form-control"></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Contract Ending Date
                                                                    </label>
                                                                <label class="input">
                                                                    <asp:TextBox ID="txtContractEndingDate" runat="server" placeholder="Contract Ending Date" ValidationGroup="addSupplierMaster" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                </label>
                                                            </section>
                                                            <section class="col col-4">
                                                                    <label class="label"> 
                                                                         Payment Terms
                                                                    </label>
                                                                <label class="input">
                                                                    
                                                                     <CKEditor:CKEditorControl ID="txtPaymentTerms" ValidationGroup="addSupplierMaster" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                     </CKEditor:CKEditorControl>
                                                                </label>
                                                            </section>
                                                        </div>
                                                    </fieldset>
                                                    <footer>
                                                        <button id="btnSupplierMasterSave" runat="server" onserverclick="btnSupplierMasterSave_ServerClick" validationgroup="addSupplierMaster" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                            <span style="width: 80px">Save </span>
                                                        </button>
                                                        <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                                    </footer>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
            <p>
                &nbsp;
            </p>

        </div>
    </div>
</asp:Content>
