﻿using LTS_CRM.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class MealSuppliement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindGrid();
        }
        protected void BindGrid()
        {
            DataTable dataTableMealSuppliment = BLSupplierMealSuppliment.GetSupplierMealSupplimentList();
            MealSupplimentRepeater.DataSource = dataTableMealSuppliment;
            MealSupplimentRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.MealSuppliementID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/MealSupplimentAddedit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int mealSupplimentId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierMealSuppliment.DeleteSupplierMealSuppliment(mealSupplimentId);
            if (isSuccess)
                BindGrid();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.MealSuppliementID = null;
            Response.Redirect("~/Supplier/MealSupplimentAddedit.aspx", false);
        }
    }
}