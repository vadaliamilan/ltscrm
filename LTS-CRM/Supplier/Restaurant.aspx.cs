﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class Restaurant : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            DataTable dataTableResturant = BLSupplierResturant.GetRestaurant();
            RestaurantRepeater.DataSource = dataTableResturant;
            RestaurantRepeater.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.RestaurantID = null;
            Response.Redirect("~/Supplier/RestaurantAddEdit.aspx", false);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.RestaurantID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/RestaurantAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int resturantID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierResturant.DeleteResturant(resturantID);
            if (isSuccess)
                BindGrid();
        }
    }
}