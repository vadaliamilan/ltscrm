﻿using LTS_CRM.Helper;
using LTS_CRM.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace LTS_CRM.Supplier
{
    /// <summary>
    /// Summary description for supplier
    /// </summary>
    public class supplier : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            int id = 0, typeID = 0, companyId = 0;

            id = Convert.ToInt32(context.Request["id"]);
            typeID = Convert.ToInt32(context.Request["typeID"]);
            companyId = Convert.ToInt32(context.Request["companyId"]);
            SupplierModel supplier = new SupplierModel();

            if (id != 0 && typeID != 0 && companyId != 0)
            {
                DataTable dt = BLL.BLSupplierMaster.GetSupplierMasterByID(id, typeID, companyId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        supplier.id = Convert.ToInt32(dr["ID"]);
                        supplier.companyid = Convert.ToInt32(dr["CompanyID"]);
                        supplier.suppliertypeid = Convert.ToInt32(dr["SupplierTypeID"]);
                        supplier.name = Convert.ToString(dr["Name"]);
                        supplier.address1 = Convert.ToString(dr["Address1"]);
                        supplier.address2 = Convert.ToString(dr["Address2"]);
                        supplier.city = Convert.ToString(dr["City"]);
                        supplier.state = Convert.ToString(dr["State"]);
                        supplier.country = Convert.ToString(dr["Country"]);
                        supplier.telephone = Convert.ToString(dr["Telephone"]);
                        supplier.postalcode = Convert.ToString(dr["PostalCode"]);
                        supplier.email = Convert.ToString(dr["Email"]);
                        supplier.credittoletstravel = Convert.ToString(dr["CreditToLetsTravel"]);
                        supplier.paymentterms = Convert.ToString(dr["PaymentTerms"]);

                        if (new CRMPage().IsDate(Convert.ToString(dr["ContractEndingDate"])))
                        {
                            DateTime dt1 = new DateTime();
                            dt1 = Convert.ToDateTime(Convert.ToString(dr["ContractEndingDate"]));
                            supplier.contractendingdate = dt1;
                        }
                    }
                }
            }

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            string sersupplier = javaScriptSerializer.Serialize(supplier);
            context.Response.ContentType = "text/html";
            context.Response.Write(sersupplier);
            //context.Response.Write(supplier);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}