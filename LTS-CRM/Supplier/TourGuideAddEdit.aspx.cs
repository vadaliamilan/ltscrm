﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class TourGuideAddEdit : CRMPage
    {
        #region Common Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSupplierType(ddlsuppliertype, App_Code.Helper.EnumSupplierType.TourGuide);
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTableByValue(ddlSex, getDescriptionByEnum(Category.Sex));
                LoadCategoryFromGeneralTableByValue(ddlMarried, getDescriptionByEnum(Category.Married));
                LoadCategoryFromGeneralTableByValue(ddlSpeaking, getDescriptionByEnum(Category.SpeakingLang));
                LoadCategoryFromGeneralTableByValue(ddlAreaOfWorking, getDescriptionByEnum(Category.AreaOfWorking));
                LoadCategoryFromGeneralTableByValue(ddlTypeOfWorking, getDescriptionByEnum(Category.TypeOfWork));
                LoadCategoryFromGeneralTableByValue(ddlVehicleOwned, getDescriptionByEnum(Category.VehicleType));
                LoadCategoryFromGeneralTableByValue(ddlKindOfGroupNotWork, getDescriptionByEnum(Category.NotWorkWithGroup));
                LoadCategoryFromGeneralTableByValue(ddlRating, getDescriptionByEnum(Category.ChannelStarRating));
                LoadCategoryFromGeneralTableByValue(ddlSeason, getDescriptionByEnum(Category.Seasons));


                LoadSupplierMaster(ddlInfo_supplierName, App_Code.Helper.EnumSupplierType.TourGuide);
                // main form
                BindFormDetails();

                // BindInformationGrid();
                BindInformationForm_Init();

                BindRateGrid();
                BindRateFormDetails();

            }
            setTabIndex();

        }

        private void setTabIndex()
        {
            if (TravelSession.Current.TourGuideID.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.TourGuideID.Value)))
            {
                tab2.Visible = true;
                tab2_pan.Visible = tab2.Visible;

                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;

                tab4.Visible = tab2.Visible;
                tab4_pan.Visible = tab2.Visible;

                tab5.Visible = tab2.Visible;
                tab5_pan.Visible = tab2.Visible;
            }
            else
            {
                tab2.Visible = false;
                tab2_pan.Visible = tab2.Visible;

                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;

                tab4.Visible = tab2.Visible;
                tab4_pan.Visible = tab2.Visible;

                tab5.Visible = tab2.Visible;
                tab5_pan.Visible = tab2.Visible;
            }

            string selectedTab = "";
            if (Request.Cookies["activetab"] != null)
            {
                selectedTab = Request.Cookies["activetab"].Value;
            }

            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");
            tab3.Attributes.Remove("class");
            tab4.Attributes.Remove("class");
            tab5.Attributes.Remove("class");

            tab1_pan.Attributes.Remove("class");
            tab2_pan.Attributes.Remove("class");
            tab3_pan.Attributes.Remove("class");
            tab4_pan.Attributes.Remove("class");
            tab5_pan.Attributes.Remove("class");

            tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab5.Attributes.Add("class", "bg-color-blueDark txt-color-white ");


            tab1_pan.Attributes.Add("class", "tab-pane ");
            tab2_pan.Attributes.Add("class", "tab-pane ");
            tab3_pan.Attributes.Add("class", "tab-pane ");
            tab4_pan.Attributes.Add("class", "tab-pane ");
            tab5_pan.Attributes.Add("class", "tab-pane ");


            switch (selectedTab)
            {
                case "#tab2_pan":
                    {
                        if (!tab2.Visible)
                            goto default;

                        tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab2_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab3_pan":
                    {
                        if (!tab3.Visible)
                            goto default;
                        tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab3_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }

                case "#tab4_pan":
                    {
                        if (!tab4.Visible)
                            goto default;
                        tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab4_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab5_pan":
                    {
                        if (!tab5.Visible)
                            goto default;
                        tab5.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab5_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                default:
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                    tab1_pan.Attributes.Add("class", "tab-pane active");
                    break;
            }
        }
        #endregion

        #region Basic Details
        private void BindFormDetails()
        {
            if (TravelSession.Current.TourGuideID.HasValue)
            {
                DLSupplierTourGuide obj = new DLSupplierTourGuide();
                obj.ID = TravelSession.Current.TourGuideID.Value;
                obj = BLSupplierTourGuide.GetSupplierByID(obj);
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                if (ddlsuppliertype.Items.FindByValue(Convert.ToString(obj.SupplierTypeId.Value)) != null)
                {
                    ddlsuppliertype.ClearSelection();
                    ddlsuppliertype.Items.FindByValue(Convert.ToString(obj.SupplierTypeId.Value)).Selected = true;
                }
                if (ddlsuppliertype.Items.FindByValue(Convert.ToString(obj.SupplierTypeId.Value)) != null)
                {
                    ddlsuppliertype.ClearSelection();
                    ddlsuppliertype.Items.FindByValue(Convert.ToString(obj.SupplierTypeId.Value)).Selected = true;
                }
                txtRecruitedBy.Text = obj.CreatedBy.IsNull ? "" : obj.CreatedBy.Value;
                txtSurName.Text = obj.SurName.Value;
                txtSurNameInChinese.Text = obj.SurNameInChinese.Value;
                txtFirstName.Text = obj.FirstName.Value;
                txtFirstNameInChinese.Text = obj.FirstNameInChinese.Value;

                if (ddlSex.Items.FindByValue(Convert.ToString(obj.Sex.Value)) != null)
                {
                    ddlSex.ClearSelection();
                    ddlSex.Items.FindByValue(Convert.ToString(obj.Sex.Value)).Selected = true;
                }

                if (!obj.DateOfBirth.IsNull && IsDate(Convert.ToString(obj.DateOfBirth.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.DateOfBirth.Value));
                    txtDateOfBirth.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtOrigin.Text = obj.Origin.Value;
                txtNationality.Text = obj.Nationality.Value;

                if (ddlMarried.Items.FindByValue(Convert.ToString(obj.Married.Value)) != null)
                {
                    ddlMarried.ClearSelection();
                    ddlMarried.Items.FindByValue(Convert.ToString(obj.Married.Value)).Selected = true;
                }
                if (ddlChildren.Items.FindByValue(Convert.ToString(obj.Children.Value)) != null)
                {
                    ddlChildren.ClearSelection();
                    ddlChildren.Items.FindByValue(Convert.ToString(obj.Children.Value)).Selected = true;
                }
                txtVisaStatus.Text = obj.VisaStatus.Value;
                txtHomePhone.Text = obj.HomePhone.Value;
                txtMobilePhone.Text = obj.MobilePhone.Value;
                txtWeChat.Text = obj.WeChat.Value;
                txtEmail.Text = obj.Email.Value;
                txtAddress1.Text = obj.Address1.Value;
                txtAddress2.Text = obj.Address2.Value;
                txtCity.Text = obj.City.Value;
                ddlCountry.Value = obj.Country.Value;
                txtPostCode.Text = obj.PostCode.Value;

                //if (!obj.SpeakingLanguage.IsNull && ddlSpeaking.Items.FindByValue(Convert.ToString(obj.SpeakingLanguage.Value)) != null)
                //{
                //    ddlSpeaking.ClearSelection();
                //    ddlSpeaking.Items.FindByValue(Convert.ToString(obj.SpeakingLanguage.Value)).Selected = true;
                //}
                if (!obj.SpeakingLanguage.IsNull && ddlSpeaking.Items.FindByValue(Convert.ToString(obj.SpeakingLanguage.Value)) != null)
                {
                    ddlSpeaking.ClearSelection();
                    ddlSpeaking.Items.FindByValue(Convert.ToString(obj.SpeakingLanguage.Value)).Selected = true;
                }
                txtNoOfYearTourGuide.Text = obj.NoOfYearTourGuide.Value.ToString();

                if (!obj.AreaOfWorking.IsNull && ddlAreaOfWorking.Items.FindByValue(Convert.ToString(obj.AreaOfWorking.Value)) != null)
                {
                    ddlAreaOfWorking.ClearSelection();
                    ddlAreaOfWorking.Items.FindByValue(Convert.ToString(obj.AreaOfWorking.Value)).Selected = true;
                }
                if (!obj.TypeOfWork.IsNull && ddlTypeOfWorking.Items.FindByValue(Convert.ToString(obj.TypeOfWork.Value)) != null)
                {
                    ddlTypeOfWorking.ClearSelection();
                    ddlTypeOfWorking.Items.FindByValue(Convert.ToString(obj.TypeOfWork.Value)).Selected = true;
                }

                if (!obj.VehicleOwned.IsNull && ddlVehicleOwned.Items.FindByValue(Convert.ToString(obj.VehicleOwned.Value)) != null)
                {
                    ddlVehicleOwned.ClearSelection();
                    ddlVehicleOwned.Items.FindByValue(Convert.ToString(obj.VehicleOwned.Value)).Selected = true;
                }
                if (!obj.VehicleOwned.IsNull && ddlVehicleOwned.Items.FindByValue(Convert.ToString(obj.VehicleOwned.Value)) != null)
                {
                    ddlVehicleOwned.ClearSelection();
                    ddlVehicleOwned.Items.FindByValue(Convert.ToString(obj.VehicleOwned.Value)).Selected = true;
                }

                if (!obj.KindOfGroupNotWork.IsNull && ddlKindOfGroupNotWork.Items.FindByValue(Convert.ToString(obj.KindOfGroupNotWork.Value)) != null)
                {
                    ddlKindOfGroupNotWork.ClearSelection();
                    ddlKindOfGroupNotWork.Items.FindByValue(Convert.ToString(obj.KindOfGroupNotWork.Value)).Selected = true;
                }

                if (!obj.Rating.IsNull && ddlRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)) != null)
                {
                    ddlRating.ClearSelection();
                    ddlRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)).Selected = true;
                }

                txtKindOfGroupWork.Text = obj.KindOfGroupWork.Value;
                txtSpecialRequirement.Text = obj.SpecialRequirement.Value;

                txtBlackList.Text = obj.BlackList.Value;
                txtComments.Text = obj.Comments.Value;

                if (!obj.HowToBook.IsNull)
                    ckHowToBook.Text = obj.HowToBook.Value;
                
                if (!obj.DrivingLicence.IsNull)
                    ucDrivingLicView.BindDocLink(obj.DrivingLicence.Value);

                if (!obj.VehiclePhoto.IsNull)
                    ucVehicalePhotoView.BindDocLink(obj.VehiclePhoto.Value);

                if (!obj.PassportCopy.IsNull)
                    ucPassportCopyView.BindDocLink(obj.PassportCopy.Value);

                if (!obj.AddressProof.IsNull)
                    ucAddressProofView.BindDocLink(obj.AddressProof.Value);

                if (!obj.GuideLicence.IsNull)
                    ucGuideLicenceView.BindDocLink(obj.GuideLicence.Value);

                if (!obj.CVOfGuide.IsNull)
                    ucCVOfGuideView.BindDocLink(obj.CVOfGuide.Value);
            }
        }

        private void SaveDetails()
        {
            DLSupplierTourGuide obj = new DLSupplierTourGuide();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.SupplierTypeId = Convert.ToInt32(ddlsuppliertype.SelectedValue);

            obj.RecruitedBy = TravelSession.Current.UserName;
            obj.SurName = txtSurName.Text;
            obj.SurNameInChinese = txtSurNameInChinese.Text;
            obj.FirstName = txtFirstName.Text;
            obj.FirstNameInChinese = txtFirstNameInChinese.Text;
            obj.Sex = ddlSex.SelectedValue;
            string resDateOfBirth = txtDateOfBirth.Text.Trim();
            if (resDateOfBirth != "")
            {
                obj.DateOfBirth = DateTime.ParseExact(resDateOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.Origin = txtOrigin.Text;
            obj.Nationality = txtNationality.Text;
            obj.Married = ddlMarried.SelectedValue;
            if (ddlChildren.SelectedIndex>0)
            obj.Children = Convert.ToInt32(ddlChildren.SelectedValue);
            obj.VisaStatus = txtVisaStatus.Text;
            obj.HomePhone = txtHomePhone.Text;
            obj.MobilePhone = txtMobilePhone.Text;
            obj.WeChat = txtWeChat.Text;
            obj.Email = txtEmail.Text;
            obj.Address1 = txtAddress1.Text;
            obj.Address2 = txtAddress2.Text;
            obj.City = txtCity.Text;
            obj.Country = ddlCountry.Value;
            obj.PostCode = txtPostCode.Text;
            obj.SpeakingLanguage = ddlSpeaking.SelectedValue;
            obj.NoOfYearTourGuide = 0;
            if (txtNoOfYearTourGuide.Text.Trim() != "")
                obj.NoOfYearTourGuide = Convert.ToInt32(txtNoOfYearTourGuide.Text);
            obj.AreaOfWorking = ddlAreaOfWorking.SelectedValue;
            obj.TypeOfWork = ddlTypeOfWorking.SelectedValue;
            obj.VehicleOwned = ddlVehicleOwned.SelectedValue;
            obj.KindOfGroupWork = txtKindOfGroupWork.Text;
            obj.KindOfGroupNotWork = ddlKindOfGroupNotWork.SelectedValue;
            obj.SpecialRequirement = txtSpecialRequirement.Text;
            if (ddlRating.SelectedIndex > 0)
                obj.Rating = Convert.ToInt32(ddlRating.SelectedValue);
            obj.BlackList = txtBlackList.Text;
            obj.Comments = txtComments.Text;
            obj.HowToBook = ckHowToBook.Text;
            obj.RecruitedBy = TravelSession.Current.UserName;

            if (fl_AddressProof.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = fl_AddressProof.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.AddressProof = fileData;
            }

            if (fl_CVOfGuide.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = fl_CVOfGuide.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.CVOfGuide = fileData;
            }
            if (fl_DrivingLic.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = fl_DrivingLic.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.DrivingLicence = fileData;
            }
            if (fl_GuideLicence.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = fl_GuideLicence.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.GuideLicence = fileData;
            }
            if (fl_PassportCopy.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = fl_PassportCopy.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.PassportCopy = fileData;
            }
            if (fl_VehicalePhoto.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = fl_VehicalePhoto.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.VehiclePhoto = fileData;
            }

            if (TravelSession.Current.TourGuideID.HasValue)
            {
             
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Current.TourGuideID.Value;
                BLL.BLSupplierTourGuide.UpdateSupplierDetail(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLL.BLSupplierTourGuide.SaveSupplierDetails(obj);
                //lblheader.Text = "Record Saved Successfully";
                //lblmsg.Text = "Your Account has been created. <br />Your Hotel Supplier ID is " + (Int32)obj.ID;
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }

            ClearFormDetails();
            Response.Redirect("TourGuide.aspx", true);
        }

        private void ClearFormDetails()
        {
            ddlsuppliertype.ClearSelection();
            txtRecruitedBy.Text = "";
            txtSurName.Text = "";
            txtSurNameInChinese.Text = "";
            txtFirstName.Text = "";
            txtFirstNameInChinese.Text = "";
            ddlSex.ClearSelection();
            txtDateOfBirth.Text = "";
            txtOrigin.Text = "";
            txtNationality.Text = "";
            ddlMarried.ClearSelection();
            ddlChildren.ClearSelection();
            txtVisaStatus.Text = "";
            txtHomePhone.Text = "";
            txtMobilePhone.Text = "";
            txtWeChat.Text = "";
            txtEmail.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            txtPostCode.Text = "";
            ddlSpeaking.Text = "";
            txtNoOfYearTourGuide.Text = "";
            ddlAreaOfWorking.ClearSelection();
            ddlTypeOfWorking.ClearSelection();
            ddlVehicleOwned.ClearSelection();
            txtKindOfGroupWork.Text = "";
            ddlKindOfGroupNotWork.ClearSelection();
            txtSpecialRequirement.Text = "";
            ddlRating.ClearSelection();
            txtBlackList.Text = "";
            txtComments.Text = "";
            ckHowToBook.Text = "";
            TravelSession.Current.TourGuideID = null;
        }

        protected void btnSaveBasic_ServerClick(object sender, EventArgs e)
        {
            SaveDetails();
        }

        protected void btnCancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("TourGuide.aspx", true);

        }

        #endregion

        #region Information functions
        private void SaveInformation()
        {
            if (TravelSession.Current.TourGuideID.HasValue)
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = TravelSession.Current.TourGuideID.Value;
                obj.Currency = txtInfo_currency.Text;
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                if (TravelSession.Current.FlightInformationID.HasValue)
                {
                    obj.UpdatedBy = TravelSession.Current.UserName;
                    obj.UpdatedDate = DateTime.Now;
                    obj.ID = TravelSession.Current.TourGuideInformationId.Value;
                    BLSupplierTourGuide.UpdateSupplierInfo(obj);
                }
                else
                {
                    BLSupplierTourGuide.InsertSupplierInfo(obj);
                }
            }
        }
        private void BindInformationForm_Init()
        {
            if (TravelSession.Current.TourGuideID.HasValue)
            {
                DLSupplierInformation obj = BLSupplierTourGuide.GetSupplierInformation(TravelSession.Current.TourGuideID.Value);
                if (!obj.ID.IsNull)
                {
                    TravelSession.Current.TourGuideInformationId = obj.ID.Value;
                    txtInfo_currency.Text = obj.Currency.Value;
                    txtInfo_postal.Text = obj.PostalCode.Value;
                    txtInfo_telehphone.Text = obj.Telehpone.Value;
                    txtInfo_city.Text = obj.City.Value;
                    txtInfo_address1.Text = obj.Address1.Value;
                    txtInfo_address2.Text = obj.Address2.Value;
                    ddlInfo_supplierName.ClearSelection();

                    if (ddlInfo_supplierName.Items.Count > 0 && ddlInfo_supplierName.Items.FindByText(Convert.ToString(obj.SupplierName.Value)) != null)
                    {
                        ddlInfo_supplierName.ClearSelection();
                        ddlInfo_supplierName.Items.FindByText(Convert.ToString(obj.SupplierName.Value)).Selected = true;
                    }
                }
                else
                    TravelSession.Current.TourGuideInformationId = null;
            }
        }

        protected void btnSupplierInformationSave_ServerClick(object sender, EventArgs e)
        {
            SaveInformation();
        }
        #endregion

        #region How TO Book
        protected void btnHowTobookSave_ServerClick(object sender, EventArgs e)
        {
            if (TravelSession.Current.TourGuideID.HasValue)
            {
                DLSupplierTourGuide obj = new DLSupplierTourGuide();
                obj.ID = TravelSession.Current.TourGuideID.Value;
                obj.SelectOne();
                obj.HowToBook = ckHowToBook.Text;
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                BLSupplierTourGuide.UpdateSupplierDetail(obj);
            }
        }


        #endregion

        #region Rate Tab


        private void BindRateFormDetails()
        {
            if (TravelSession.Current.TourGuideID.HasValue && TravelSession.Current.TourGuideRateID.HasValue)
            {
                DLSupplierTourGuideRate obj = new DLSupplierTourGuideRate();
                //obj.ID = TravelSession.Current.TourGuideRateID.Value;
                obj = BLSupplierTourGuide.GetSupplierRateListByPk(TravelSession.Current.TourGuideRateID.Value);
                
                if (!obj.ID.IsNull)
                {
                    TravelSession.Current.TourGuideRateID = obj.ID.Value;
                    ddlSeason.Text = obj.Season.Value;
                    if (!obj.FromDate.IsNull && IsDate(Convert.ToString(obj.FromDate.Value)))
                    {
                        DateTime dt = new DateTime();
                        dt = Convert.ToDateTime(Convert.ToString(obj.FromDate.Value));
                        txtFromDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                    }
                    if (!obj.ToDate.IsNull && IsDate(Convert.ToString(obj.ToDate.Value)))
                    {
                        DateTime dt = new DateTime();
                        dt = Convert.ToDateTime(Convert.ToString(obj.ToDate.Value));
                        txtToDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                    }
                    txtHalf_day_4_9_SeaterDriverGuideWithoutVehicle.Text = obj.Half_day_4_9_SeaterDriverGuideWithoutVehicle.Value.ToString();
                    txtHalf_day_12_17_Seater_Driver_GuideWithoutVehicle.Text = obj.Half_day_12_17_Seater_Driver_GuideWithoutVehicle.Value.ToString();
                    txtHalf_day_coach_Guide.Text = obj.Half_day_coach_Guide.Value.ToString();
                    txtHalf_day_4_9_Seater_Driver_Guide_With_Vehicle.Text = obj.Half_day_4_9_Seater_Driver_Guide_With_Vehicle.Value.ToString();
                    txtHalf_day_12_17_Seater_Driver_Guide_With_Vehicle.Text = obj.Half_day_12_17_Seater_Driver_Guide_With_Vehicle.Value.ToString();
                    txtHalf_day_tips.Text = obj.Half_day_tips.Value.ToString();
                    txtFull_day_4_9_Seater_Driver_Guide_Without_Vehicle.Text = obj.Full_day_4_9_Seater_Driver_Guide_Without_Vehicle.Value.ToString();
                    txtFull_day_12_17_Seater_Driver_Guide_Without_Vehicle.Text = obj.Full_day_12_17_Seater_Driver_Guide_Without_Vehicle.Value.ToString();
                    txtFull_Day_coach_Guide.Text = obj.Full_Day_coach_Guide.Value.ToString();
                    txtFull_day_4_9_Seater_Driver_Guide_With_Vehicle.Text = obj.Full_day_4_9_Seater_Driver_Guide_With_Vehicle.Value.ToString();
                    txtFull_day_12_17_Seater_Driver_Guide_WithVehicle.Text = obj.Full_day_12_17_Seater_Driver_Guide_WithVehicle.Value.ToString();
                    txtFull_day_tips.Text = obj.Full_day_tips.Value.ToString();
                    txtExtra_hour_With_Vehicle.Text = obj.Extra_hour_With_Vehicle.Value.ToString();
                    txtExtra_hour_Without_vehicle.Text = obj.Extra_hour_Without_vehicle.Value.ToString();
                    txtLondon_Edinburgh_With_Vehicle.Text = obj.London_Edinburgh_With_Vehicle.Value.ToString();
                    txtLondon_Edinburgh_Without_Vehicle.Text = obj.London_Edinburgh_Without_Vehicle.Value.ToString();
                }
            }
        }
        private void BindRateGrid() {
            if (TravelSession.Current.TourGuideID.HasValue )
            {
                DataTable dt = BLSupplierTourGuide.GetSupplierRateListBySupplierID(TravelSession.Current.TourGuideID.Value);
                rptSupplierTourGuideRate.DataSource = dt;
                rptSupplierTourGuideRate.DataBind();
            }
        }
        protected void btnSupplierTourGuideRateSave_ServerClick(object sender, EventArgs e)
        {
            SaveRateDetails();
        }

        private void SaveRateDetails()
        {
            if (TravelSession.Current.TourGuideID.HasValue)
            {
                DLSupplierTourGuideRate obj = new DLSupplierTourGuideRate();
                obj.SupplierTourGuideID = TravelSession.Current.TourGuideID.Value;
                obj.Season = ddlSeason.SelectedValue;
                string resFromDate = txtFromDate.Text.Trim();
                if (resFromDate != "")
                {
                    obj.FromDate = DateTime.ParseExact(resFromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                string resToDate = txtToDate.Text.Trim();
                if (resToDate != "")
                {
                    obj.ToDate = DateTime.ParseExact(resToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                if (txtHalf_day_4_9_SeaterDriverGuideWithoutVehicle.Text.Trim().Length > 0)
                    obj.Half_day_4_9_SeaterDriverGuideWithoutVehicle = Convert.ToDecimal(txtHalf_day_4_9_SeaterDriverGuideWithoutVehicle.Text.Trim());
                
                if (txtHalf_day_12_17_Seater_Driver_GuideWithoutVehicle.Text.Trim().Length > 0)
                    obj.Half_day_12_17_Seater_Driver_GuideWithoutVehicle = Convert.ToDecimal(txtHalf_day_12_17_Seater_Driver_GuideWithoutVehicle.Text.Trim());

                if (txtHalf_day_coach_Guide.Text.Trim().Length > 0)
                     obj.Half_day_coach_Guide = Convert.ToDecimal(txtHalf_day_coach_Guide.Text.Trim());

                if (txtHalf_day_4_9_Seater_Driver_Guide_With_Vehicle.Text.Trim().Length > 0)
                     
                     obj.Half_day_4_9_Seater_Driver_Guide_With_Vehicle = Convert.ToDecimal(txtHalf_day_4_9_Seater_Driver_Guide_With_Vehicle.Text.Trim());

                if (txtHalf_day_12_17_Seater_Driver_Guide_With_Vehicle.Text.Trim().Length > 0)
                    obj.Half_day_12_17_Seater_Driver_Guide_With_Vehicle = Convert.ToDecimal(txtHalf_day_12_17_Seater_Driver_Guide_With_Vehicle.Text.Trim());

                if (txtHalf_day_tips.Text.Trim().Length > 0)
                    obj.Half_day_tips = Convert.ToDecimal(txtHalf_day_tips.Text.Trim());

                if (txtFull_day_4_9_Seater_Driver_Guide_Without_Vehicle.Text.Trim().Length > 0)
                    obj.Full_day_4_9_Seater_Driver_Guide_Without_Vehicle = Convert.ToDecimal(txtFull_day_4_9_Seater_Driver_Guide_Without_Vehicle.Text.Trim());
                
                if (txtFull_day_12_17_Seater_Driver_Guide_Without_Vehicle.Text.Trim().Length>0)
                        obj.Full_day_12_17_Seater_Driver_Guide_Without_Vehicle = Convert.ToDecimal(txtFull_day_12_17_Seater_Driver_Guide_Without_Vehicle.Text.Trim());

                if (txtFull_Day_coach_Guide.Text.Trim().Length > 0)
                    obj.Full_Day_coach_Guide = Convert.ToDecimal(txtFull_Day_coach_Guide.Text.Trim());

                if (txtFull_day_4_9_Seater_Driver_Guide_With_Vehicle.Text.Trim().Length > 0)
                    obj.Full_day_4_9_Seater_Driver_Guide_With_Vehicle = Convert.ToDecimal(txtFull_day_4_9_Seater_Driver_Guide_With_Vehicle.Text.Trim());

                if (txtFull_day_12_17_Seater_Driver_Guide_WithVehicle.Text.Trim().Length > 0)
                    obj.Full_day_12_17_Seater_Driver_Guide_WithVehicle = Convert.ToDecimal(txtFull_day_12_17_Seater_Driver_Guide_WithVehicle.Text.Trim());

                if (txtFull_day_tips.Text.Trim().Length > 0)
                    obj.Full_day_tips = Convert.ToDecimal(txtFull_day_tips.Text.Trim());

                if (txtExtra_hour_With_Vehicle.Text.Trim().Length > 0)
                    obj.Extra_hour_With_Vehicle = Convert.ToDecimal(txtExtra_hour_With_Vehicle.Text.Trim());

                if (txtExtra_hour_Without_vehicle.Text.Trim().Length > 0)
                    obj.Extra_hour_Without_vehicle = Convert.ToDecimal(txtExtra_hour_Without_vehicle.Text.Trim());

                if (txtLondon_Edinburgh_With_Vehicle.Text.Trim().Length > 0)
                    obj.London_Edinburgh_With_Vehicle = Convert.ToDecimal(txtLondon_Edinburgh_With_Vehicle.Text.Trim());

                if (txtLondon_Edinburgh_Without_Vehicle.Text.Trim().Length > 0)
                    obj.London_Edinburgh_Without_Vehicle = Convert.ToDecimal(txtLondon_Edinburgh_Without_Vehicle.Text.Trim());

                if (TravelSession.Current.TourGuideRateID.HasValue)
                {
                    obj.UpdatedBy = TravelSession.Current.UserName;
                    obj.UpdatedDate = DateTime.Now;
                    obj.ID = TravelSession.Current.TourGuideID.Value;
                    BLSupplierTourGuide.UpdateSupplierRate(obj);
                }
                else
                {
                    obj.CreatedBy = TravelSession.Current.UserName;
                    obj.CreatedDate = DateTime.Now;
                    BLSupplierTourGuide.InsertSupplierRate(obj);
                }
                 ClearRateFormDetails();
            }
        }

        protected void btnTourPhotoSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (TravelSession.Current.TourGuideID.HasValue)
                {
                    DLTourGuidePhotos obj = new DLTourGuidePhotos();
                    obj.TourGuideId = TravelSession.Current.TourGuideID.Value;
                    if (flTourPhoto.PostedFiles.Count > 0)
                    {
                        foreach (HttpPostedFile file in flTourPhoto.PostedFiles)
                        {
                            if (file.ContentLength <= comman.TourPhotoSizeLimit)
                            {
                                HttpPostedFile File = flTourPhoto.PostedFile;
                                int size = File.ContentLength;
                                string name = File.FileName;
                                int position = name.LastIndexOf("\\");
                                name = name.Substring(position + 1);
                                string contentType = File.ContentType;
                                byte[] fileData = new byte[size];
                                File.InputStream.Read(fileData, 0, size);
                                obj.Photo = fileData;

                                obj.CreatedBy = TravelSession.Current.UserName;
                                obj.CreatedDate = DateTime.Now;
                                obj.UpdatedBy = obj.CreatedBy;
                                obj.UpdatedDate = DateTime.Now;
                                obj.RealFileName = name;
                                obj.Filename = name;
                                obj.FileExt = Path.GetExtension(name);
                                BLSupplierTourGuide.InsertPhotos(obj);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alert", "javascript:alert('You can not exceed limit size 3MB on photos, please try again!');", true);
                            }
                        }
                        BindPhotoGrid();
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        
        protected void btnTourPhotoCancel_ServerClick(object sender, EventArgs e)
        {
            flTourPhoto.PostedFile.InputStream.Dispose();
            flTourPhoto.Dispose();
        }

        protected void rptPhotos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if(e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                if (e.Item.FindControl("ucPhotoView") != null)
                {
                   viewDocument doc = e.Item.FindControl("ucPhotoView") as viewDocument;
                   if(e.Item.FindControl("hdnview") !=null)
                   {
                       HiddenField hdn = e.Item.FindControl("hdnview") as HiddenField;
                       doc.BindDocLink(BLL.BLSupplierTourGuide.GetBytesOfTourPhotoById(Convert.ToInt32(hdn.Value)));
                   }
                }
            }
        }

        protected void buttonDeletePhoto_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLSupplierTourGuide.DeleteTourGuidePhoto(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindPhotoGrid();
        }
        private void BindPhotoGrid()
        {
            if (TravelSession.Current.TourGuideID.HasValue)
            {
                rptPhotos.DataSource = BLSupplierTourGuide.GetAllPhotos(TravelSession.Current.TourGuideID.Value);
                rptPhotos.DataBind();
            }
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.TourGuideRateID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            BindRateFormDetails();
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLSupplierTourGuide.DeleteSupplierRateByPk(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
            BindRateGrid();
        }


        private void ClearRateFormDetails()
        {
            TravelSession.Current.TourGuideRateID = null;
            ddlSeason.ClearSelection();
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtHalf_day_4_9_SeaterDriverGuideWithoutVehicle.Text = "";
            txtHalf_day_12_17_Seater_Driver_GuideWithoutVehicle.Text = "";
            txtHalf_day_coach_Guide.Text = "";
            txtHalf_day_4_9_Seater_Driver_Guide_With_Vehicle.Text = "";
            txtHalf_day_12_17_Seater_Driver_Guide_With_Vehicle.Text = "";
            txtHalf_day_tips.Text = "";
            txtFull_day_4_9_Seater_Driver_Guide_Without_Vehicle.Text = "";
            txtFull_day_12_17_Seater_Driver_Guide_Without_Vehicle.Text = "";
            txtFull_Day_coach_Guide.Text = "";
            txtFull_day_4_9_Seater_Driver_Guide_With_Vehicle.Text = "";
            txtFull_day_12_17_Seater_Driver_Guide_WithVehicle.Text = "";
            txtFull_day_tips.Text = "";
            txtExtra_hour_With_Vehicle.Text = "";
            txtExtra_hour_Without_vehicle.Text = "";
            txtLondon_Edinburgh_With_Vehicle.Text = "";
            txtLondon_Edinburgh_Without_Vehicle.Text = "";
        }


        #endregion
    }
}