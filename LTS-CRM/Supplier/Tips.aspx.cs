﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class Tips : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            rptSupplierTipsDetail.DataSource = BLSupplierTips.GetTips();
            rptSupplierTipsDetail.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.TipsID = null;
            Response.Redirect("~/Supplier/TipsAddEdit.aspx", false);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {

            TravelSession.Current.TipsID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/TipsAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLSupplierTips.DeleteTips(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();
        }
    }
}