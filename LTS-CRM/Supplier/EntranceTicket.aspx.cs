﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class EntranceTicket : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            DataTable dataTableResturant = BLSupplierEntraceTicket.GetEntranceTicket();
            EntranceTicketRepeater.DataSource = dataTableResturant;
            EntranceTicketRepeater.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.EntranceTicketID = null;
            Response.Redirect("~/Supplier/EntranceTicketAddEdit.aspx", false);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.EntranceTicketID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/EntranceTicketAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int entranceTicketID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierEntraceTicket.DeleteEntranceTicket(entranceTicketID);
            if (isSuccess)
                BindGrid();
        }
    }
}