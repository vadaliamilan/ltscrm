﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class SupplierMasterAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                GetAllSupplierType(ddlsuppliertype);
                BindFormDetails();
            }
        }
        private void BindFormDetails()
        {
            if (TravelSession.Current.SupplierMasterID.HasValue)
            {
                DateTime dt = new DateTime();
                DLSupplierMaster obj = new DLSupplierMaster();
                obj.ID = TravelSession.Current.SupplierMasterID.Value;
                obj = BLSupplierMaster.GetSupplierMasterDetailByPk(obj);

               
                if (!obj.CompanyID.IsNull && cboCompanyID.Items.Count > 0 && cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }

                if (!obj.SupplierTypeID.IsNull && ddlsuppliertype.Items.Count > 0 && ddlsuppliertype.Items.FindByValue(Convert.ToString(obj.SupplierTypeID.Value)) != null)
                {
                    ddlsuppliertype.ClearSelection();
                    ddlsuppliertype.Items.FindByValue(Convert.ToString(obj.SupplierTypeID.Value)).Selected = true;
                }

                txtName.Text = obj.Name.Value;
                txtAddress1.Text = obj.Address1.Value;
                txtAddress2.Text = obj.Address2.Value;
                txtCity.Text = obj.City.Value;
                txtState.Text = obj.State.Value;
                cmbcountry.Value = obj.Country.Value;
                txtTelephone.Text = obj.Telephone.Value;
                txtPostalCode.Text = obj.PostalCode.Value;
                txtEmail.Text = obj.Email.Value;
                txtCreditToLetsTravel.Text = obj.CreditToLetsTravel.Value;
                txtPaymentTerms.Text = obj.PaymentTerms.Value;
                if (!obj.ContractEndingDate.IsNull && IsDate(Convert.ToString(obj.ContractEndingDate.Value)))
                {
                    dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ContractEndingDate.Value));
                    txtContractEndingDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
            }
        }
        private void SaveDetails()
        {
            DLSupplierMaster obj = new DLSupplierMaster();

            obj.SupplierTypeID =  Convert.ToInt32(ddlsuppliertype.SelectedValue);
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.Name = txtName.Text;
            obj.Address1 = txtAddress1.Text;
            obj.Address2 = txtAddress2.Text;
            obj.City = txtCity.Text;
            obj.State = txtState.Text;
            obj.Country = cmbcountry.Value;
            obj.Telephone = txtTelephone.Text;
            obj.PostalCode = txtPostalCode.Text;
            obj.Email = txtEmail.Text;
            obj.CreditToLetsTravel = txtCreditToLetsTravel.Text;
            obj.PaymentTerms = txtPaymentTerms.Text;
            string resContractEndingDate = txtContractEndingDate.Text.Trim();
              
            if (resContractEndingDate != "")
            {
                obj.ContractEndingDate = DateTime.ParseExact(resContractEndingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (TravelSession.Current.SupplierMasterID.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Current.SupplierMasterID.Value;
                BLSupplierMaster.UpdateMasterDetails(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLSupplierMaster.InsertMasterDetails(obj);
            }
            ClearFormDetails();
            Response.Redirect("SupplierMaster.aspx", true);
        }

        private void ClearFormDetails()
        {
            TravelSession.Current.SupplierMasterID = null;
            ddlsuppliertype.ClearSelection();
            txtName.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            cmbcountry.SelectedIndex = -1;
            txtTelephone.Text = "";
            txtPostalCode.Text = "";
            txtEmail.Text = "";
            txtCreditToLetsTravel.Text = "";
            txtPaymentTerms.Text = "";
            txtContractEndingDate.Text = "";
        }
        protected void btnSupplierMasterSave_ServerClick(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                SaveDetails();
            }
        }
    }
}