﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class UserContacts : LTSUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //if(SupplierDetailId!=null)
                //{
                //    BindForm();
                //}
            }
        }

        public int? SupplierDetailId { get; set; }
        public void BindForm(DLSuppliersDetails obj)
        {
            try
            {
                //if (SupplierDetailId.HasValue)
                {
                    ddlTitle1.Value                  =                     obj.Title.Value;
                    txtcontactEmail1.Text                  =                     obj.ContactEmail.Value;
                    txtcontactFax1.Text                  =                     obj.FaxNo.Value;
                    txtcontactMobile1.Text                  =                     obj.MobileNo.Value;
                    txtcontactTelephone1.Text                  =                     obj.Telephone.Value;
                    txtDepartment1.Text                  =                     obj.Department.Value;
                    txtFacebook1.Text                  =                     obj.Facebook.Value;
                    txtFirstName1.Text                  =                     obj.FirstName.Value;
                    txtLinkedin1.Text                  =                     obj.LinkedIn.Value;
                    txtPosition1.Text                  =                     obj.Position.Value;
                    txtSkype1.Text                  =                     obj.Skype.Value;
                    txtSurName1.Text                  =                     obj.SurName.Value;
                    txtTwitter1.Text                  =                     obj.Twitter.Value;
                    txtWeChat1.Text                  =                     obj.WeChat.Value;
                    txtWhatsapp1.Text                  =                     obj.Whatsup.Value;
                }
            }
            catch {
            
            }
        }

        public string Title
        {
            get
            {
                return ddlTitle1.Value;
            }
            set
            {
                ddlTitle1.Value                  = value;
            }
        }
        public string ContactEmail
        {
            get
            {
                return txtcontactEmail1.Text;
            }
            set
            {
                txtcontactEmail1.Text                  = value;
            }
        }
        public string FaxNo
        {
            get
            {
                return txtcontactFax1.Text;
            }
            set
            {
                txtcontactFax1.Text                  = value;
            }
        }

        public string MobileNo
        {
            get
            {
                return txtcontactMobile1.Text;
            }
            set
            {
                txtcontactMobile1.Text                  = value;
            }
        }
        public string Telephone
        {
            get
            {
                return txtcontactTelephone1.Text;
            }
            set
            {
                txtcontactTelephone1.Text                  = value;
            }
        }
        public string Department
        {
            get
            {
                return txtDepartment1.Text;
            }
            set
            {
                txtDepartment1.Text                  = value;
            }
        }
        public string Facebook
        {
            get
            {
                return txtFacebook1.Text;
            }
            set
            {
                txtFacebook1.Text                  = value;
            }
        }
        public string FirstName
        {
            get
            {
                return txtFirstName1.Text;
            }
            set
            {
                txtFirstName1.Text                  = value;
            }
        }
        public string LinkedIn
        {
            get
            {
                return txtLinkedin1.Text;
            }
            set
            {
                txtLinkedin1.Text                  = value;
            }
        }
        public string Position
        {
            get
            {
                return txtPosition1.Text;
            }
            set
            {
                txtPosition1.Text                  = value;
            }
        }
        public string Skype
        {
            get
            {
                return txtSkype1.Text;
            }
            set
            {
                txtSkype1.Text                  = value;
            }
        }
        public string SurName
        {
            get
            {
                return txtSurName1.Text;
            }
            set
            {
                txtSurName1.Text                  = value;
            }
        }
        public string Twitter
        {
            get
            {
                return txtTwitter1.Text;
            }
            set
            {
                txtTwitter1.Text                  = value;
            }
        }
        public string WeChat
        {
            get
            {
                return txtWeChat1.Text;
            }
            set
            {
                txtWeChat1.Text                  = value;
            }
        }
        public string Whatsup
        {
            get
            {
                return txtWhatsapp1.Text;
            }
            set
            {
                txtWhatsapp1.Text                  = value;
            }
        }
    }
}