﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserContacts.ascx.cs" ClientIDMode="AutoID" Inherits="LTS_CRM.Supplier.UserContacts" %>
<div id="userContact" class="row smart-form" style="margin:0px">
    <fieldset>
        <div class="row">
            <section class="col col-12">
                <h3 class="text-primary">Contact Person</h3>
            </section>
        </div>
        <div class="row">
            <section class="col col-2">
                <label class="label">
                    Title
                </label>
                <label class="input">
                    <select id="ddlTitle1" class="form-control" runat="server">
                        <option>Mr</option>
                        <option>Ms</option>
                        <option>Mrs</option>
                    </select>
                </label>
            </section>
            <section class="col col-3">
            </section>
        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">
                    SurName
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-user"></i>
                    <asp:TextBox ID="txtSurName1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Sur Name" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator  SetFocusOnError="true"  ID="rfvtxtSurName1" ValidationGroup="addBasicgroup" ControlToValidate="txtSurName1" runat="server" ErrorMessage="Last Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    First Name
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-user"></i>
                    <asp:TextBox ID="txtFirstName1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="First Name" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator  SetFocusOnError="true"  ID="RequiredFieldValidator2" ValidationGroup="addBasicgroup" ControlToValidate="txtFirstName1" runat="server" ErrorMessage="First Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Position
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-map-marker"></i>
                    <asp:TextBox ID="txtPosition1" class="form-control" placeholder="Position" ValidationGroup="addBasicgroup" runat="server">
                    </asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Department
                </label>
                <label class="input">
                    <i class="icon-prepend fa-building-o"></i>
                    <asp:TextBox ID="txtDepartment1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Department" Text=""></asp:TextBox>

                </label>
            </section>
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">
                    Email
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-envelope-o"></i>

                    <asp:TextBox ID="txtcontactEmail1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Email" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Phone Number
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-phone"></i>
                    <asp:TextBox ID="txtcontactTelephone1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addBasicgroup" placeholder="Phone Number" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Mobile Number
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-mobile"></i>
                    <asp:TextBox ID="txtcontactMobile1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addBasicgroup" placeholder="Mobile Number" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Fax Number
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-fax"></i>
                    <asp:TextBox ID="txtcontactFax1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Fax Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                </label>
            </section>
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">
                    WeChat
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-wechat"></i>
                    <asp:TextBox ID="txtWeChat1" runat="server" class="form-control" placeholder="Enter WeChat" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    What's up
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-whatsapp"></i>
                    <asp:TextBox ID="txtWhatsapp1" runat="server" class="form-control" placeholder="Enter What's up" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Skype
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-skype"></i>
                    <asp:TextBox ID="txtSkype1" runat="server" class="form-control" placeholder="Enter Skype" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Facebook
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-facebook"></i>
                    <asp:TextBox ID="txtFacebook1" runat="server" class="form-control" placeholder="Enter Facebook" Text=""></asp:TextBox>

                </label>
            </section>
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">
                    Twitter
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-twitter"></i>

                    <asp:TextBox ID="txtTwitter1" runat="server" class="form-control" placeholder="Enter Twitter" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Linked in
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-linkedin"></i>
                    <asp:TextBox ID="txtLinkedin1" runat="server" class="form-control" placeholder="Enter Linked in" Text=""></asp:TextBox>

                </label>
            </section>
        </div>
    </fieldset>
    <%--<footer>
        <button id="btnotherContactSave" runat="server" ValidationGroup="addBasicgroup" onserverclick="btnotherContactSave_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
            <span style="width: 80px">Save </span>
        </button>
        <button id="btnotherContactCancel" runat="server" causesvalidation="false" ValidationGroup="addBasicgroup" onserverclick="btnotherContactCancel_ServerClick" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
            <span style="width: 80px">Cancel</span>
        </button>
    </footer>--%>
</div>
