﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class TourGuide : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindControls(); 
            }
        }

        private void BindControls()
        {
            TourGuideRepeater.DataSource = BLL.BLSupplierTourGuide.GetTourGuide();
            TourGuideRepeater.DataBind();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.TourGuideID = null;
            TravelSession.Current.TourGuideRateID = null;
            Response.Redirect("TourGuideAddEdit.aspx", true);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.TourGuideID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/TourGuideAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierTourGuide.DeleteTourGuide(id);
            if (isSuccess)
                BindControls();
        }
    }
}