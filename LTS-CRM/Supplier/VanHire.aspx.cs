﻿using LTS_CRM.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class VanHire : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindGrid();
        }
        protected void BindGrid()
        {
            DataTable dataTableVanHire = BLSupplierVanHire.GetSupplierVanList();
            VanHireRepeater.DataSource = dataTableVanHire;
            VanHireRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.VanHireID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/VanHireAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int vanHireID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierVanHire.DeleteSupplierVan(vanHireID);
            if (isSuccess)
                BindGrid();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.VanHireID = null;
            Response.Redirect("~/Supplier/VanHireAddedit.aspx", false);
        }
    }
}