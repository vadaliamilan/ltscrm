﻿
using LTS_CRM.App_Code.Helper;
using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class MealSuppliementAddedit : CRMPage
    {
        #region Common Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadSupplierMaster(ddlName, App_Code.Helper.EnumSupplierType.MealSupplementsfordriverandguide);
                LoadSupplierType(ddlsuppliertype, App_Code.Helper.EnumSupplierType.MealSupplementsfordriverandguide);

                // main form
                BindFormDetails();

                // Rate & Information
                BindOtherContactGrid();
                // BindInformationGrid();
               
               
            }

            setTabIndex();
        }
        private void setTabIndex()
        {
            if (TravelSession.Current.MealSuppliementID.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.MealSuppliementID.Value)))
            {
                tab2.Visible = true;
                tab2_pan.Visible = tab2.Visible;

                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;
            }
            else
            {
                tab2.Visible = false;
                tab2_pan.Visible = tab2.Visible;

                tab3.Visible = tab2.Visible;
                tab3_pan.Visible = tab2.Visible;
            }

            string selectedTab = "";
            if (Request.Cookies["activetab"] != null)
            {
                selectedTab = Request.Cookies["activetab"].Value;
            }

            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");
            tab3.Attributes.Remove("class");
           
            tab1_pan.Attributes.Remove("class");
            tab2_pan.Attributes.Remove("class");
            tab3_pan.Attributes.Remove("class");
           
            tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
           
            tab1_pan.Attributes.Add("class", "tab-pane ");
            tab2_pan.Attributes.Add("class", "tab-pane ");
            tab3_pan.Attributes.Add("class", "tab-pane ");
           
            switch (selectedTab)
            {
                case "#tab2_pan":
                    {
                        if (!tab2.Visible)
                            goto default;

                        tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab2_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab3_pan":
                    {
                        if (!tab3.Visible)
                            goto default;
                        tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab3_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }

                default:
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                    tab1_pan.Attributes.Add("class", "tab-pane active");
                    break;
            }
        }

        #endregion

        #region Basic Info
        private void BindFormDetails()
        {
            if (TravelSession.Current.MealSuppliementID.HasValue)
            {
               
                DLSupplierMealSupplement obj = new DLSupplierMealSupplement();
                obj.ID = TravelSession.Current.MealSuppliementID.Value;
                obj = BLSupplierMealSuppliment.GetSupplierMealSupplimentById(obj);
                txtAddedBy.Text = obj.AddedBy.Value;
                txtPurpose.Text = obj.Purpose.Value;
                
                if(ddlMealSupplementType.Items.Count>0 && ddlMealSupplementType.Items.FindByValue(obj.MealSupplementType.Value) !=null )
                {
                    ddlMealSupplementType.ClearSelection();
                    ddlMealSupplementType.Items.FindByValue(obj.MealSupplementType.Value).Selected=true;
                }
                txtCity.Text = obj.City.Value;
                txtState.Text = obj.State.Value;
                cmbcountry.Value = obj.Country.Value;
                txtPostalCode.Text = obj.PostalCode.Value;
                if (!obj.ExtraField1.IsNull)
                    ckHowToBook.Text = obj.ExtraField1.Value;
            }
        }
        private void ClearFormDetails()
        {
            TravelSession.Current.MealSuppliementID = null;
            txtAddedBy.Text = "";
            txtPurpose.Text = "";
            ddlMealSupplementType.ClearSelection();
            txtCity.Text = "";
            txtState.Text = "";
            txtPostalCode.Text = ""; 
        }
        private void SaveDetails()
        {

            DLSupplierMealSupplement obj = new DLSupplierMealSupplement();
         
            if (TravelSession.Current.MealSuppliementID.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Current.MealSuppliementID.Value;
                BLL.BLSupplierMealSuppliment.UpdateMealSupplimentSupplier(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                obj.AddedBy = txtAddedBy.Text;
                obj.Purpose = txtPurpose.Text;
                obj.MealSupplementType = ddlMealSupplementType.SelectedValue;
                obj.City = txtCity.Text;
                obj.State = txtState.Text;
                obj.Country = cmbcountry.Value;
                obj.PostalCode = txtPostalCode.Text;
                BLL.BLSupplierMealSuppliment.SaveMealSupplimentSupplier(obj);
                //lblheader.Text = "Record Saved Successfully";
                //lblmsg.Text = "Your Account has been created. <br />Your Hotel Supplier ID is " + (Int32)obj.ID;
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }

            ClearFormDetails();
            Response.Redirect("MealSuppliment.aspx", true);

        }
        protected void btnMealDetails_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }
        protected void btnCancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("MealSuppliment.aspx", true);
        }
        #endregion


        #region Supplier Contact Details

        private void ClearContactFormDetails()
        {
            TravelSession.Current.MealSuppliementContactID = null;
            ddlTitle1.SelectedIndex = 0;
            txtSurName1.Text = "";
            txtFirstName1.Text = "";
            txtPosition1.Text = "";
            txtDepartment1.Text = "";
            txtcontactEmail1.Text = "";
            txtcontactTelephone1.Text = "";
            txtcontactMobile1.Text = "";
            txtcontactFax1.Text = "";
            txtWeChat1.Text = "";
            txtWhatsapp1.Text = "";
            txtSkype1.Text = "";
            txtFacebook1.Text = "";
            txtTwitter1.Text = "";
            txtLinkedin1.Text = "";
        }
        private void BindOtherContactFrom()
        {
            if (TravelSession.Current.MealSuppliementContactID.HasValue)
            {
                DLSupplierContacts obj = BLSupplierMealSuppliment.GetSupplierContactsbyPk(TravelSession.Current.MealSuppliementContactID.Value);
                ddlTitle1.Value = obj.Title.Value;
                txtSurName1.Text = obj.SurName.Value;
                txtFirstName1.Text = obj.FirstName.Value;
                txtPosition1.Text = obj.Position.Value;
                txtDepartment1.Text = obj.Department.Value;
                txtcontactEmail1.Text = obj.Email.Value;
                txtcontactTelephone1.Text = obj.DirectPhone.Value;
                txtcontactMobile1.Text = obj.MobileNo.Value;
                txtcontactFax1.Text = obj.FaxNo.Value;
                txtWeChat1.Text = obj.WeChat.Value;
                txtWhatsapp1.Text = obj.Whatsup.Value;
                txtSkype1.Text = obj.Skype.Value;
                txtFacebook1.Text = obj.Facebook.Value;
                txtTwitter1.Text = obj.Twitter.Value;
                txtLinkedin1.Text = obj.LinkedIn.Value;
            }
        }
        private void BindOtherContactGrid()
        {
            if (TravelSession.Current.MealSuppliementID.HasValue)
            {
                rptClientContactRepeater.DataSource = BLSupplierMealSuppliment.GetSupplierContactsbySupplierID(TravelSession.Current.MealSuppliementID.Value);
                rptClientContactRepeater.DataBind();
            }
        }
        private void SaveSupplierContactDetails()
        {
            if (TravelSession.Current.MealSuppliementID.HasValue)
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = TravelSession.Current.MealSuppliementID.Value;
                obj.Title = ddlTitle1.Value;
                obj.SurName = txtSurName1.Text;
                obj.MainContact = false;
                obj.FirstName = txtFirstName1.Text;
                obj.Position = txtPosition1.Text;
                obj.Department = txtDepartment1.Text;
                obj.Email = txtcontactEmail1.Text;
                obj.DirectPhone = txtcontactTelephone1.Text;
                obj.MobileNo = txtcontactMobile1.Text;
                obj.FaxNo = txtcontactFax1.Text;
                obj.WeChat = txtWeChat1.Text;
                obj.Whatsup = txtWhatsapp1.Text;
                obj.Skype = txtSkype1.Text;
                obj.Facebook = txtFacebook1.Text;
                obj.Twitter = txtTwitter1.Text;
                obj.LinkedIn = txtLinkedin1.Text;

                if (TravelSession.Current.MealSuppliementContactID.HasValue)
                {
                    obj.UpdatedBy = TravelSession.Current.UserName.ToString();
                    obj.UpdatedDate = DateTime.Now;
                    obj.ID = TravelSession.Current.MealSuppliementContactID.Value;
                    BLSupplierMealSuppliment.UpdateSupplierContact(obj);
                }
                else
                {
                    obj.CreatedBy = TravelSession.Current.UserName.ToString();
                    obj.CreatedDate = DateTime.Now;
                    BLSupplierMealSuppliment.SaveSupplierContact(obj);
                }
                BindOtherContactGrid();
                ClearContactFormDetails();
            }

        }
        protected void buttonEditContact_Click(object sender, EventArgs e)
        {
            TravelSession.Current.MealSuppliementContactID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            BindOtherContactFrom();
        }

        protected void buttonDeleteContact_Click(object sender, EventArgs e)
        {
            int contactId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierMealSuppliment.DeleteSupplierContactByPk(contactId);
            if (isSuccess)
                BindOtherContactGrid();
        }

        protected void btnotherContactSave_ServerClick(object sender, EventArgs e)
        {
            SaveSupplierContactDetails();
        }

        protected void btnotherContactCancel_ServerClick(object sender, EventArgs e)
        {
            ClearContactFormDetails();
        }
        #endregion

        #region How TO Book
        protected void btnHowTobookSave_ServerClick(object sender, EventArgs e)
        {
            if (TravelSession.Current.MealSuppliementID.HasValue)
            {
                DLSupplierMealSupplement obj = new DLSupplierMealSupplement();
                obj.ID = TravelSession.Current.MealSuppliementID.Value;
                obj.SelectOne();
                obj.ExtraField1 = ckHowToBook.Text;
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                BLSupplierMealSuppliment.UpdateMealSupplimentSupplier(obj);
            }
        }


        #endregion

    }
}