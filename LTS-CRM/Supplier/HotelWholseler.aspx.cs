﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class HotelWholseler : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
                ClearTabCookies();
            }
        }

        protected void BindGrid()
        {
            DataTable dataTableClient = BLSupplierHotelWholseler.GetSupplierHotelWholeSalerList();
            HotelwholeselarRepeater.DataSource = dataTableClient;
            HotelwholeselarRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.HotelWholeselarID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/HotelWholselerAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int hotelWholeselarID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierHotelWholseler.DeleteSupplierHotelWholseler(hotelWholeselarID);
            if (isSuccess)
                BindGrid();
        }

        protected void addHotelWholeselar_Click(object sender, EventArgs e)
        {
            TravelSession.Current.HotelWholeselarID = null;
            Response.Redirect("HotelWholselerAddEdit.aspx", true);
        }
    }
}