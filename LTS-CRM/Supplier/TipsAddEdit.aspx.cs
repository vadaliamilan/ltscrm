﻿using LTSCRM_DL;
using LTS_CRM.BLL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LTS_CRM.Helper;
namespace LTS_CRM.Supplier
{
    public partial class TipsAddEdit : CRMPage
    {
        #region Common Page function
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                //call to clear tabing index on Not postback

                LoadcboCompanyID(cboCompanyID);
                LoadSupplierType(ddlsuppliertype, App_Code.Helper.EnumSupplierType.Tip);
                // main form
                BindFormDetails();
            }
        }



        #endregion

        #region Basic Info

        protected void btnCancel_ServerClick(object sender, EventArgs e)
        {
            ClearFormDetails();
            Response.Redirect("~/Supplier/Tips.aspx", false);

        }
        private void BindFormDetails()
        {
            if (TravelSession.Current.TipsID != null && TravelSession.Current.TipsID.HasValue)
            {
                DLSupplierTipsDetail obj = BLSupplierTips.GetSupplierByID(TravelSession.Current.TipsID.Value);
                cmbcountry.Value = obj.Country.Value;
                txtRate.Text = obj.Tips.Value.ToString();
                if (!obj.WholeDay.IsNull)
                txtWholeDay.Text = obj.WholeDay.Value.ToString();
                if (!obj.AirportPickUp.IsNull)
                    txtAirportPickUp.Text = obj.AirportPickUp.Value.ToString();
                if (!obj.AirportDropOff.IsNull)
                    txtAirportDropOff.Text = obj.AirportDropOff.Value.ToString();
            }
        }
        private void ClearFormDetails()
        {
            TravelSession.Current.TipsID = null;
            cmbcountry.SelectedIndex = -1;
            txtRate.Text = "";
            txtWholeDay.Text = "";
            txtAirportPickUp.Text = "";
            txtAirportDropOff.Text = "";
        }
        private void SaveDetails()
        {

            DLSupplierTipsDetail obj = new DLSupplierTipsDetail();

            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.SupplierTypeId = Convert.ToInt32(ddlsuppliertype.SelectedValue);
            obj.Country = cmbcountry.Value;

            obj.Tips = Convert.ToDecimal(txtRate.Text.Trim());
            if (txtWholeDay.Text.Trim().Length > 0)
                obj.WholeDay = Convert.ToDecimal(txtWholeDay.Text.Trim());
            if (txtAirportDropOff.Text.Trim().Length > 0)
                obj.AirportDropOff = Convert.ToDecimal(txtAirportDropOff.Text.Trim());
            if (txtAirportPickUp.Text.Trim().Length > 0)
                obj.AirportPickUp = Convert.ToDecimal(txtAirportPickUp.Text.Trim());

            if (TravelSession.Current.TipsID != null && TravelSession.Current.TipsID.HasValue)
            {

                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Current.TipsID.Value;
                BLSupplierTips.UpdateSupplierDetail(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLSupplierTips.SaveSupplierDetails(obj);
            }
            ClearFormDetails();
            Response.Redirect("Tips.aspx", true);
        }
        protected void btnSuppliersDetailsSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        #endregion

    }
}