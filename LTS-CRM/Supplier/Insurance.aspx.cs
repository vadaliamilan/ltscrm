﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class Insurance : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            DataTable dataTableInsurance = BLSupplierInsurance.GetSupplierInsuranceList();
            InsuranceRepeater.DataSource = dataTableInsurance;
            InsuranceRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.InsuranceID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/InsuranceAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int insuranceID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierInsurance.DeleteSupplierInsurance(insuranceID);
            if (isSuccess)
                BindGrid();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.InsuranceID = null;
            Response.Redirect("~/Supplier/InsuranceAddEdit.aspx", false);
        }
    }
}