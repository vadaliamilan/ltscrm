﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Supplier
{
    public partial class DMC : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            DataTable dataTableResturant = BLSupplierDMC.GetDMC();
            DMCRepeater.DataSource = dataTableResturant;
            DMCRepeater.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Current.DMCID = null;
            Response.Redirect("~/Supplier/DMCAddEdit.aspx", false);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.DMCID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Supplier/DMCAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int dmcID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLSupplierDMC.DeleteDmc(dmcID);
            if (isSuccess)
                BindGrid();
        }
    }
}