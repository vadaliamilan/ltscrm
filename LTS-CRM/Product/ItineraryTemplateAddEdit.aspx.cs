﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Product
{
    public partial class ItineraryTemplateAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTable(ddlTourType, "ItineraryType", "Select Type");
                BindFormDetails();
            }
        }
        private void BindFormDetails()
        {
            if (TravelSession.Product.ItineraryTemplateId.HasValue)
            {
                DLItineraryTemplate obj = new DLItineraryTemplate();
                obj = BLItineraryTemplate.GetItineraryTemplateById(TravelSession.Product.ItineraryTemplateId.Value);

                txtStaff.Text = obj.Staff.Value;
                cmbcountry.Value = obj.Country.Value;
                txtRegion.Text = obj.Region.Value;
                txtNoOfDays.Text = Convert.ToString(obj.NoOfDays.Value);
                txtNoOfNights.Text = Convert.ToString(obj.NoOfNights.Value);
                txtSubjectTitle.Text = obj.SubjectTitle.Value;
                txtItineraryTemplate.Text = obj.ItineraryTemplate.Value;
                hiddenFieldAttachment.Value = !obj.ItineraryPhotoId.IsNull ? Convert.ToString(obj.ItineraryPhotoId) : string.Empty;
                if (ddlTourType.Items.FindByText(Convert.ToString(obj.TourType.Value)) != null)
                {
                    ddlTourType.ClearSelection();
                    ddlTourType.Items.FindByText(Convert.ToString(obj.TourType.Value)).Selected = true;
                }
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
            }
        }
        private void SaveDetails()
        {
            DLItineraryTemplate obj = new DLItineraryTemplate();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.Staff = txtStaff.Text;
            obj.Country = cmbcountry.Value;
            obj.Region = txtRegion.Text;
            obj.TourType = ddlTourType.SelectedItem.Text;
            obj.NoOfDays = Convert.ToInt32(txtNoOfDays.Text);
            obj.NoOfNights = Convert.ToInt32(txtNoOfNights.Text);
            obj.SubjectTitle = txtSubjectTitle.Text;
            obj.ItineraryTemplate = txtItineraryTemplate.Text;

            #region Save Document in the Product Document
            DLProductDocument doc = new DLProductDocument();
            if (flContractDoc.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = flContractDoc.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                doc.FileData = fileData;
                doc.FileName = name;
            }
            int? productDocumentId = null;
            if (!doc.FileData.IsNull)
            {
                if (!string.IsNullOrEmpty(hiddenFieldAttachment.Value))
                {
                    doc.ID = Convert.ToInt32(hiddenFieldAttachment.Value);
                    doc.UpdatedBy = TravelSession.Current.UserName;
                    doc.UpdatedDate = DateTime.Now;
                    productDocumentId = BLProductDocument.UpdateProductDocument(doc);
                }
                else
                {
                    doc.CreatedBy = TravelSession.Current.UserName;
                    doc.CreatedDate = DateTime.Now;
                    productDocumentId = BLProductDocument.SaveProductDocument(doc);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(hiddenFieldAttachment.Value))  
                productDocumentId =Convert.ToInt32(hiddenFieldAttachment.Value);
            }

            #endregion
            if (productDocumentId!=null)
            obj.ItineraryPhotoId = (SqlInt32)productDocumentId;
            if (TravelSession.Product.ItineraryTemplateId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Product.ItineraryTemplateId.Value;
                BLItineraryTemplate.UpdateItineraryTemplate(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLItineraryTemplate.SaveItineraryTempate(obj);
            }

            ClearFormDetails();
            Response.Redirect("ItineraryTemplate.aspx", true);

        }
        private void ClearFormDetails()
        {
            txtStaff.Text = "";
            txtRegion.Text = "";
            ddlTourType.ClearSelection();
            txtNoOfDays.Text = "";
            txtNoOfNights.Text = "";
            txtSubjectTitle.Text = "";
            txtItineraryTemplate.Text = "";
        }
        protected void btnItineraryTemplateSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("ItineraryTemplate.aspx", true);
        }
    }
}