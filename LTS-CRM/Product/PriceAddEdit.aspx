﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="PriceAddEdit.aspx.cs" Inherits="LTS_CRM.Product.PriceAddEdit" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Product</li>
                    <li>Price Including & Excluding & Remark List</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Price Including & Excluding & Remark Add-Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">

                                    <div class="tab-content">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="PriceTemplateform" class="row smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Company 
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addPriceTemplate"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addPriceTemplate" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Staff
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtStaff" runat="server" placeholder="Staff" ValidationGroup="addPriceTemplate" Text="" class="form-control"></asp:TextBox>
                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidatorStaff" ControlToValidate="txtStaff" ValidationGroup="addPriceTemplate" runat="server" ErrorMessage="Staff Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Language
                                                            </label>
                                                            <label class="input">
                                                                  <asp:DropDownList class="form-control" ID="DropDownListLanguage" runat="server" ValidationGroup="addPriceTemplate"></asp:DropDownList>
                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidatorLanguage" ControlToValidate="DropDownListLanguage" ValidationGroup="addPriceTemplate" runat="server" ErrorMessage="Language Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Title
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtTitle" runat="server" placeholder="Title" ValidationGroup="addPriceTemplate" Text="" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTitle" ControlToValidate="txtTitle" ValidationGroup="addPriceTemplate" runat="server" ErrorMessage="Title Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Description
                                                            </label>
                                                            <label class="input">
                                                                <CKEditor:CKEditorControl ID="txtDescription"  BasePath="~/ckeditor" runat="server" Height="100" Width="700" ToolbarStartupExpanded="False">
                                                                </CKEditor:CKEditorControl>
                                                            </label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                                <footer>
                                                    <button id="btnPriceTemplateSave" runat="server" validationgroup="addPriceTemplate" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnPriceTemplateSave_ServerClick">
                                                        <span style="width: 80px">Save </span>
                                                    </button>
                                                    <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" runat="server" id="cancel" onserverclick="cancel_ServerClick" />
                                                </footer>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </article>
                </div>
            </section>
        </div>
</asp:Content>


