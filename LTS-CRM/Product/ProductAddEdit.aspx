﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductAddEdit.aspx.cs" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" Inherits="LTS_CRM.Product.ProductAddEdit" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scmProduct" runat="server"></asp:ScriptManager>
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Product Management</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Product Add Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <ul class="nav nav-tabs tabs-left" id="client-nav">
                                            <li class="bg-color-blueDark txt-color-white active" id="tab1" runat="server">
                                                <a data-toggle="tab" href="#tab1_pan">Basic Detail </a></li>
                                            <%-- <li  class="bg-color-blueDark txt-color-white"  id="tab2" runat="server" visible="false">
                                                <a data-toggle="tab" href="#tab2_pan">Contacts</a>
                                            </li>

                                            <li class="bg-color-blueDark txt-color-white" id="tab3" runat="server" visible="false">
                                                <a data-toggle="tab" href="#tab3_pan">Product & Price</a>
                                            </li>--%>
                                            <li class="bg-color-blueDark txt-color-white" id="tab4" runat="server" visible="false">
                                                <a data-toggle="tab" href="#tab4_pan">How to book</a>
                                            </li>
                                            <li class="bg-color-blueDark txt-color-white" id="tab7" runat="server" visible="false">
                                                <a data-toggle="tab" href="#tab7_pan">How to sell</a>
                                            </li>
                                            <li class="bg-color-blueDark txt-color-white" id="tab8" runat="server" visible="false">
                                                <a data-toggle="tab" href="#tab8_pan">Upload itinerary, photos and related documents</a>
                                            </li>


                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1_pan" runat="server">
                                                <div class="row">
                                                    <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                        <div class="input-group">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="Productform" class="row smart-form">
                                                        <fieldset>
                                                            <asp:Panel ID="pnlStep" runat="server">
                                                                <div class="row">
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Company 
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addProduct"></asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addProduct" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Creation Date 
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtCreationDate" runat="server" placeholder="CreationDate" ValidationGroup="addProduct" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtCreationDate" ControlToValidate="txtCreationDate" ValidationGroup="addProduct" runat="server" ErrorMessage="Creation Date Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Contracted By 
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtContractedBy" runat="server" placeholder="Contracted By" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtContractedBy" ControlToValidate="txtContractedBy" ValidationGroup="addProduct" runat="server" ErrorMessage="Contracted By Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Data Entry By 
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtDataEntryBy" runat="server" placeholder="Data Entry By" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvtxtDataEntryBy" ControlToValidate="txtDataEntryBy" ValidationGroup="addProduct" runat="server" ErrorMessage="DataEntry By Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Product Type
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:DropDownList ID="ddlProductType" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlProductType_SelectedIndexChanged">
                                                                                <asp:ListItem Text="Self Develop Product" Value="0" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Text="Resell product from other company" Value="1"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </label>
                                                                    </section>
                                                                </div>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlStepProduct" runat="server">

                                                                <asp:Panel ID="pnlResellProduct" Visible="false" runat="server">

                                                                    <div class="row">
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Supplier Name
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:DropDownList ID="ddlSupplierName" runat="server" ValidationGroup="addProduct" class="form-control"></asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSupplierName" ControlToValidate="ddlSupplierName" ValidationGroup="addProduct" runat="server" ErrorMessage="Supplier Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Contract
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:DropDownList ID="ddlContract" runat="server" class="form-control"></asp:DropDownList>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Contract With
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:DropDownList ID="ddlContractWith" runat="server" class="form-control"></asp:DropDownList>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Emergency No
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtEmergencyNo" runat="server" data-mask="(999) 999-99999" placeholder="Emergency No" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                    </div>
                                                                    <div class="row">
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Reservation Email
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtReservationEmail" runat="server" placeholder="ReservationEmail" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Alternation Email
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtAlternationEmail" runat="server" placeholder="AlternationEmail" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Currency
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:DropDownList ID="ddlCurrency" runat="server" ValidationGroup="addProduct" class="form-control"></asp:DropDownList>
                                                                            </label>
                                                                        </section>

                                                                    </div>
                                                                    <div class="row">
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Contract Type
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:DropDownList ID="ddlContractType" runat="server" ValidationGroup="addProduct" class="form-control"></asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorContractType" ControlToValidate="ddlContractType" ValidationGroup="addProduct" runat="server" ErrorMessage="Contract Type Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Net Rate
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtNetRate" runat="server" placeholder="Net Rate" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorNetRate" ControlToValidate="txtNetRate" ValidationGroup="addProduct" runat="server" SetFocusOnError="true" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Commission
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtCommission" runat="server" placeholder="Commission" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorCommission" ControlToValidate="txtCommission" ValidationGroup="addProduct" runat="server" SetFocusOnError="true" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                            </label>
                                                                        </section>
                                                                    </div>
                                                                    <hr />
                                                                    <div class="row">
                                                                        <section class="col col-12">
                                                                            <h3 class="text-primary">Main Contact</h3>
                                                                        </section>
                                                                    </div>
                                                                    <div class="row">
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Title
                                                                            </label>
                                                                            <label class="input">
                                                                                <select id="ddlTitle" class="form-control" runat="server">
                                                                                    <option>Mr</option>
                                                                                    <option>Ms</option>
                                                                                    <option>Mrs</option>
                                                                                </select>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Sur Name
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtSurnameMain" runat="server" placeholder="SurName" ValidationGroup="addProductContact" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                First Name
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtFirstNameMain" runat="server" placeholder="FirstName" ValidationGroup="addProductContact" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Position
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtPositionMain" runat="server" placeholder="Position" ValidationGroup="addProductContact" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                    </div>
                                                                    <div class="row">
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Department
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtDepartmentMain" runat="server" placeholder="Department" ValidationGroup="addProductContact" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Email
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtEmailMain" runat="server" placeholder="Email" ValidationGroup="addProductContact" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                        <section class="col col-3">
                                                                            <label class="label">
                                                                                Direct Phone
                                                                            </label>
                                                                            <label class="input">
                                                                                <asp:TextBox ID="txtTelephoneMain" data-mask="(999) 999-99999" runat="server" placeholder="Direct Phone" ValidationGroup="addProductContact" Text="" class="form-control"></asp:TextBox>
                                                                            </label>
                                                                        </section>
                                                                    </div>
                                                                </asp:Panel>
                                                                <hr />
                                                                <div class="row">
                                                                    <section class="col col-12">
                                                                        <h3 class="text-primary">Product</h3>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Country
                                                                        </label>
                                                                        <select id="cmbcountry" class="form-control" runat="server">
                                                                            <option value="Afghanistan">Afghanistan</option>
                                                                            <option value="Albania">Albania</option>
                                                                            <option value="Algeria">Algeria</option>
                                                                            <option value="American Samoa">American Samoa</option>
                                                                            <option value="Andorra">Andorra</option>
                                                                            <option value="Angola">Angola</option>
                                                                            <option value="Anguilla">Anguilla</option>
                                                                            <option value="Antartica">Antarctica</option>
                                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                            <option value="Argentina">Argentina</option>
                                                                            <option value="Armenia">Armenia</option>
                                                                            <option value="Aruba">Aruba</option>
                                                                            <option value="Australia">Australia</option>
                                                                            <option value="Austria">Austria</option>
                                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                                            <option value="Bahamas">Bahamas</option>
                                                                            <option value="Bahrain">Bahrain</option>
                                                                            <option value="Bangladesh">Bangladesh</option>
                                                                            <option value="Barbados">Barbados</option>
                                                                            <option value="Belarus">Belarus</option>
                                                                            <option value="Belgium">Belgium</option>
                                                                            <option value="Belize">Belize</option>
                                                                            <option value="Benin">Benin</option>
                                                                            <option value="Bermuda">Bermuda</option>
                                                                            <option value="Bhutan">Bhutan</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                            <option value="Botswana">Botswana</option>
                                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                                            <option value="Brazil">Brazil</option>
                                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                            <option value="Bulgaria">Bulgaria</option>
                                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                                            <option value="Burundi">Burundi</option>
                                                                            <option value="Cambodia">Cambodia</option>
                                                                            <option value="Cameroon">Cameroon</option>
                                                                            <option value="Canada">Canada</option>
                                                                            <option value="Cape Verde">Cape Verde</option>
                                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                                            <option value="Central African Republic">Central African Republic</option>
                                                                            <option value="Chad">Chad</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="China">China</option>
                                                                            <option value="Christmas Island">Christmas Island</option>
                                                                            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Comoros">Comoros</option>
                                                                            <option value="Congo">Congo</option>
                                                                            <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                            <option value="Cook Islands">Cook Islands</option>
                                                                            <option value="Costa Rica">Costa Rica</option>
                                                                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                            <option value="Croatia">Croatia (Hrvatska)</option>
                                                                            <option value="Cuba">Cuba</option>
                                                                            <option value="Cyprus">Cyprus</option>
                                                                            <option value="Czech Republic">Czech Republic</option>
                                                                            <option value="Denmark">Denmark</option>
                                                                            <option value="Djibouti">Djibouti</option>
                                                                            <option value="Dominica">Dominica</option>
                                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                                            <option value="East Timor">East Timor</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Egypt">Egypt</option>
                                                                            <option value="El Salvador">El Salvador</option>
                                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                            <option value="Eritrea">Eritrea</option>
                                                                            <option value="Estonia">Estonia</option>
                                                                            <option value="Ethiopia">Ethiopia</option>
                                                                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                                            <option value="Fiji">Fiji</option>
                                                                            <option value="Finland">Finland</option>
                                                                            <option value="France">France</option>
                                                                            <option value="France Metropolitan">France, Metropolitan</option>
                                                                            <option value="French Guiana">French Guiana</option>
                                                                            <option value="French Polynesia">French Polynesia</option>
                                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                                            <option value="Gabon">Gabon</option>
                                                                            <option value="Gambia">Gambia</option>
                                                                            <option value="Georgia">Georgia</option>
                                                                            <option value="Germany">Germany</option>
                                                                            <option value="Ghana">Ghana</option>
                                                                            <option value="Gibraltar">Gibraltar</option>
                                                                            <option value="Greece">Greece</option>
                                                                            <option value="Greenland">Greenland</option>
                                                                            <option value="Grenada">Grenada</option>
                                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                                            <option value="Guam">Guam</option>
                                                                            <option value="Guatemala">Guatemala</option>
                                                                            <option value="Guinea">Guinea</option>
                                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                            <option value="Guyana">Guyana</option>
                                                                            <option value="Haiti">Haiti</option>
                                                                            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                            <option value="Holy See">Holy See (Vatican City State)</option>
                                                                            <option value="Honduras">Honduras</option>
                                                                            <option value="Hong Kong">Hong Kong</option>
                                                                            <option value="Hungary">Hungary</option>
                                                                            <option value="Iceland">Iceland</option>
                                                                            <option value="India">India</option>
                                                                            <option value="Indonesia">Indonesia</option>
                                                                            <option value="Iran">Iran (Islamic Republic of)</option>
                                                                            <option value="Iraq">Iraq</option>
                                                                            <option value="Ireland">Ireland</option>
                                                                            <option value="Israel">Israel</option>
                                                                            <option value="Italy">Italy</option>
                                                                            <option value="Jamaica">Jamaica</option>
                                                                            <option value="Japan">Japan</option>
                                                                            <option value="Jordan">Jordan</option>
                                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                                            <option value="Kenya">Kenya</option>
                                                                            <option value="Kiribati">Kiribati</option>
                                                                            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                            <option value="Korea">Korea, Republic of</option>
                                                                            <option value="Kuwait">Kuwait</option>
                                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                            <option value="Lao">Lao People's Democratic Republic</option>
                                                                            <option value="Latvia">Latvia</option>
                                                                            <option value="Lebanon">Lebanon</option>
                                                                            <option value="Lesotho">Lesotho</option>
                                                                            <option value="Liberia">Liberia</option>
                                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                                            <option value="Lithuania">Lithuania</option>
                                                                            <option value="Luxembourg">Luxembourg</option>
                                                                            <option value="Macau">Macau</option>
                                                                            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                            <option value="Madagascar">Madagascar</option>
                                                                            <option value="Malawi">Malawi</option>
                                                                            <option value="Malaysia">Malaysia</option>
                                                                            <option value="Maldives">Maldives</option>
                                                                            <option value="Mali">Mali</option>
                                                                            <option value="Malta">Malta</option>
                                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                                            <option value="Martinique">Martinique</option>
                                                                            <option value="Mauritania">Mauritania</option>
                                                                            <option value="Mauritius">Mauritius</option>
                                                                            <option value="Mayotte">Mayotte</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Micronesia">Micronesia, Federated States of</option>
                                                                            <option value="Moldova">Moldova, Republic of</option>
                                                                            <option value="Monaco">Monaco</option>
                                                                            <option value="Mongolia">Mongolia</option>
                                                                            <option value="Montserrat">Montserrat</option>
                                                                            <option value="Morocco">Morocco</option>
                                                                            <option value="Mozambique">Mozambique</option>
                                                                            <option value="Myanmar">Myanmar</option>
                                                                            <option value="Namibia">Namibia</option>
                                                                            <option value="Nauru">Nauru</option>
                                                                            <option value="Nepal">Nepal</option>
                                                                            <option value="Netherlands">Netherlands</option>
                                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                            <option value="New Caledonia">New Caledonia</option>
                                                                            <option value="New Zealand">New Zealand</option>
                                                                            <option value="Nicaragua">Nicaragua</option>
                                                                            <option value="Niger">Niger</option>
                                                                            <option value="Nigeria">Nigeria</option>
                                                                            <option value="Niue">Niue</option>
                                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                            <option value="Norway">Norway</option>
                                                                            <option value="Oman">Oman</option>
                                                                            <option value="Pakistan">Pakistan</option>
                                                                            <option value="Palau">Palau</option>
                                                                            <option value="Panama">Panama</option>
                                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Philippines">Philippines</option>
                                                                            <option value="Pitcairn">Pitcairn</option>
                                                                            <option value="Poland">Poland</option>
                                                                            <option value="Portugal">Portugal</option>
                                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                                            <option value="Qatar">Qatar</option>
                                                                            <option value="Reunion">Reunion</option>
                                                                            <option value="Romania">Romania</option>
                                                                            <option value="Russia">Russian Federation</option>
                                                                            <option value="Rwanda">Rwanda</option>
                                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                            <option value="Saint LUCIA">Saint LUCIA</option>
                                                                            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                            <option value="Samoa">Samoa</option>
                                                                            <option value="San Marino">San Marino</option>
                                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                                            <option value="Senegal">Senegal</option>
                                                                            <option value="Seychelles">Seychelles</option>
                                                                            <option value="Sierra">Sierra Leone</option>
                                                                            <option value="Singapore">Singapore</option>
                                                                            <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                            <option value="Slovenia">Slovenia</option>
                                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                                            <option value="Somalia">Somalia</option>
                                                                            <option value="South Africa">South Africa</option>
                                                                            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                            <option value="Span">Spain</option>
                                                                            <option value="SriLanka">Sri Lanka</option>
                                                                            <option value="St. Helena">St. Helena</option>
                                                                            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                            <option value="Sudan">Sudan</option>
                                                                            <option value="Suriname">Suriname</option>
                                                                            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="Swaziland">Swaziland</option>
                                                                            <option value="Sweden">Sweden</option>
                                                                            <option value="Switzerland">Switzerland</option>
                                                                            <option value="Syria">Syrian Arab Republic</option>
                                                                            <option value="Taiwan">Taiwan, Province of China</option>
                                                                            <option value="Tajikistan">Tajikistan</option>
                                                                            <option value="Tanzania">Tanzania, United Republic of</option>
                                                                            <option value="Thailand">Thailand</option>
                                                                            <option value="Togo">Togo</option>
                                                                            <option value="Tokelau">Tokelau</option>
                                                                            <option value="Tonga">Tonga</option>
                                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                            <option value="Tunisia">Tunisia</option>
                                                                            <option value="Turkey">Turkey</option>
                                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                                            <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                            <option value="Tuvalu">Tuvalu</option>
                                                                            <option value="Uganda">Uganda</option>
                                                                            <option value="Ukraine">Ukraine</option>
                                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                                            <option value="United Kingdom" selected>United Kingdom</option>
                                                                            <option value="United States">United States</option>
                                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                                            <option value="Vanuatu">Vanuatu</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Vietnam">Viet Nam</option>
                                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                            <option value="Western Sahara">Western Sahara</option>
                                                                            <option value="Yemen">Yemen</option>
                                                                            <option value="Yugoslavia">Yugoslavia</option>
                                                                            <option value="Zambia">Zambia</option>
                                                                            <option value="Zimbabwe">Zimbabwe</option>
                                                                        </select>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Region
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtRegion" runat="server" placeholder="Region" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Type of Product 
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:DropDownList ID="ddlTypeOfProduct" runat="server" class="form-control"></asp:DropDownList>
                                                                        </label>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            No Of days 
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtNoOfDays" runat="server" placeholder="No Of days" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorNoOfDays" ControlToValidate="txtNoOfDays" ValidationGroup="addProduct" runat="server" SetFocusOnError="true" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            No Of Nights 
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtNoOfNights" runat="server" placeholder="No Of Nights" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorNoOfNights" ControlToValidate="txtNoOfNights" ValidationGroup="addProduct" runat="server" SetFocusOnError="true" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Title
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtTitle" runat="server" placeholder="Title" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                        </label>
                                                                    </section>

                                                                    <section class="col col-3" id="pnlReselPriceSection" runat="server" visible="false">
                                                                        <label class="label">
                                                                            Price
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:TextBox ID="txtPrice" runat="server" placeholder="Price" ValidationGroup="addProduct" Text="" class="form-control"></asp:TextBox>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">
                                                                            Iternary
                                                                        </label>
                                                                        <label class="input">
                                                                            <asp:FileUpload ID="flIternaryUpload" runat="server" />
                                                                        </label>
                                                                    </section>

                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-4">
                                                                        <label class="label">
                                                                            Comments
                                                                        </label>
                                                                        <label class="input">
                                                                            <CKEditor:CKEditorControl ID="CKEtxtComment" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                            </CKEditor:CKEditorControl>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-4">
                                                                        <label class="label">
                                                                            Price Including
                                                                        </label>
                                                                        <label class="input">
                                                                            <CKEditor:CKEditorControl ID="CKEditorControlIncluding" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                            </CKEditor:CKEditorControl>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-4">
                                                                        <label class="label">
                                                                            Price Excluding
                                                                        </label>
                                                                        <label class="input">
                                                                            <CKEditor:CKEditorControl ID="CKEditorControlExcluding" BasePath="~/ckeditor" runat="server" Height="100" Width="300" ToolbarStartupExpanded="False">
                                                                            </CKEditor:CKEditorControl>
                                                                        </label>
                                                                    </section>
                                                                </div>
                                                            </asp:Panel>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnProductSave" runat="server" validationgroup="addProduct" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnProductSave_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                            <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" runat="server" onserverclick="ProductCancel_ServerClick" id="ProductCancel" />
                                                        </footer>

                                                    </div>
                                                </div>
                                            </div>
                                            <%--<div class="tab-pane" id="tab2_pan" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div id="ProductContactform" class="row smart-form">
                                                        <fieldset>
                                                              <div class="row">
            <section class="col col-2">
                <label class="label">
                    Title
                </label>
                <label class="input">
                    <select id="ddlTitle1" class="form-control" runat="server">
                        <option>Mr</option>
                        <option>Ms</option>
                        <option>Mrs</option>
                    </select>
                </label>
            </section>
            <section class="col col-3">
            </section>
        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">
                    SurName
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-user"></i>
                    <asp:TextBox ID="txtSurName1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Sur Name" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator  SetFocusOnError="true"  ID="rfvtxtSurName1" ValidationGroup="addBasicgroup" ControlToValidate="txtSurName1" runat="server" ErrorMessage="Last Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    First Name
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-user"></i>
                    <asp:TextBox ID="txtFirstName1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="First Name" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator  SetFocusOnError="true"  ID="RequiredFieldValidator2" ValidationGroup="addBasicgroup" ControlToValidate="txtFirstName1" runat="server" ErrorMessage="First Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Position
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-map-marker"></i>
                    <asp:TextBox ID="txtPosition1" class="form-control" placeholder="Position" ValidationGroup="addBasicgroup" runat="server">
                    </asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Department
                </label>
                <label class="input">
                    <i class="icon-prepend fa-building-o"></i>
                    <asp:TextBox ID="txtDepartment1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Department" Text=""></asp:TextBox>

                </label>
            </section>
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">
                    Email
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-envelope-o"></i>

                    <asp:TextBox ID="txtcontactEmail1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Email" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Phone Number
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-phone"></i>
                    <asp:TextBox ID="txtcontactTelephone1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addBasicgroup" placeholder="Phone Number" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Mobile Number
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-mobile"></i>
                    <asp:TextBox ID="txtcontactMobile1" runat="server" class="form-control" data-mask="(999) 999-99999" ValidationGroup="addBasicgroup" placeholder="Mobile Number" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Fax Number
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-fax"></i>
                    <asp:TextBox ID="txtcontactFax1" runat="server" class="form-control" ValidationGroup="addBasicgroup" placeholder="Fax Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                </label>
            </section>
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">
                    WeChat
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-wechat"></i>
                    <asp:TextBox ID="txtWeChat1" runat="server" class="form-control" placeholder="Enter WeChat" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    What's up
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-whatsapp"></i>
                    <asp:TextBox ID="txtWhatsapp1" runat="server" class="form-control" placeholder="Enter What's up" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Skype
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-skype"></i>
                    <asp:TextBox ID="txtSkype1" runat="server" class="form-control" placeholder="Enter Skype" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Facebook
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-facebook"></i>
                    <asp:TextBox ID="txtFacebook1" runat="server" class="form-control" placeholder="Enter Facebook" Text=""></asp:TextBox>

                </label>
            </section>
        </div>
        <div class="row">
            <section class="col col-3">
                <label class="label">
                    Twitter
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-twitter"></i>

                    <asp:TextBox ID="txtTwitter1" runat="server" class="form-control" placeholder="Enter Twitter" Text=""></asp:TextBox>

                </label>
            </section>
            <section class="col col-3">
                <label class="label">
                    Linked in
                </label>
                <label class="input">
                    <i class="icon-prepend fa fa-linkedin"></i>
                    <asp:TextBox ID="txtLinkedin1" runat="server" class="form-control" placeholder="Enter Linked in" Text=""></asp:TextBox>

                </label>
            </section>
        </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="btnProductContactSave" runat="server" validationgroup="addProductContact" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnProductContactSave_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                </div>
                                            </div>--%>

                                            <%--<div class="tab-pane" id="tab3_pan" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="row smart-form">
                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-sm-3">
                                                                    <button id="btnviewprice" validationgroup="addProduct" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                        <span style="width: 80px">Add Price </span>
                                                                    </button>
                                                                </section>
                                                                <section class="col col-sm-3">

                                                                    <button id="btnviewprice" validationgroup="addProduct" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                                        <span style="width: 80px">View Price </span>
                                                                    </button>

                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>--%>
                                            <div class="tab-pane" id="tab4_pan" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="row smart-form">

                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        How to book
                                                                    </label>
                                                                    <label class="input">
                                                                        <CKEditor:CKEditorControl ID="ckhowtobook" BasePath="~/ckeditor" runat="server" Height="100" Width="600" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>

                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="buttonHowToBook" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="buttonHowToBook_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab7_pan" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="row smart-form">

                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        How to Sell
                                                                    </label>
                                                                    <label class="input">
                                                                        <CKEditor:CKEditorControl ID="CKEditorControlHowToSell" BasePath="~/ckeditor" runat="server" Height="100" Width="600" ToolbarStartupExpanded="False">
                                                                        </CKEditor:CKEditorControl>
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="buttonHowToSell" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="buttonHowToSell_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                        </footer>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab8_pan" runat="server">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="row smart-form">

                                                        <fieldset>
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">
                                                                        Upload itinerary                                                                   
                                                                    </label>
                                                                    <label class="input">
                                                                        <asp:FileUpload runat="server" ID="UploadSelfDocument" />
                                                                        Choose file
                                                                    </label>
                                                                </section>
                                                            </div>
                                                        </fieldset>
                                                        <footer>
                                                            <button id="buttonUploadSelfDocument" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="buttonUploadSelfDocument_ServerClick">
                                                                <span style="width: 80px">Save </span>
                                                            </button>
                                                        </footer>

                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                    <fieldset>
                                                        <asp:Repeater ID="rptSelfProductDocument" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                            <HeaderTemplate>
                                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="hasinput" style="width: 5%">
                                                                                <input type="text" class="form-control" placeholder="ID" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 55%">
                                                                                <input type="text" class="form-control" placeholder="File Name" />
                                                                            </th>
                                                                            <th class="hasinput" style="width: 15%"></th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>ID</th>
                                                                            <th>File   </th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "FileName")%></td>
                                                                    <td>
                                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonSelfProductDocumentDelete" runat="server" Text="Click" OnClick="buttonSelfProductDocumentDelete_Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
</table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                    </fieldset>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>

            <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
