﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="Price.aspx.cs" Inherits="LTS_CRM.Product.Price" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Literal ID="output" runat="server" />
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Product</li>
                    <li>Price Including & Excluding & Remark</li>
                </ol>
            </div>
            <p>
                &nbsp;
            </p>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Price Including & Excluding & Remark List</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <div class="row">
                                            <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                <div class="input-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                                <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="lnkAddNew" OnClick="lnkAddNew_Click" runat="server"><i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span></asp:LinkButton>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptPriceTemplate" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                            <HeaderTemplate>
                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                    <thead>
                                                        <tr>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="ID" />
                                                            </th>
                                                            <th class="hasinput" style="width: 15%">
                                                                <input type="text" class="form-control" placeholder="Staff" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Language" />
                                                            </th>
                                                            <th class="hasinput" style="width: 25%">
                                                                <input type="text" class="form-control" placeholder="Title" />
                                                            </th>
                                                            <th class="hasinput" style="width: 25%">
                                                                <input type="text" class="form-control" placeholder="Description" />
                                                            </th>
                                                             <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Creation Date" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th data-class="expand">Staff</th>
                                                            <th data-hide="phone">Language</th>
                                                            <th data-hide="phone">Title</th>
                                                            <th data-hide="phone">Description</th>
                                                            <th data-hide="phone">Creation Date</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Staff")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Language")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Title")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Description")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "CreatedDate","{0:dd/MM/yyyy}")%></td>
                                                    <td>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" Text="Click" OnClick="buttonEdit_Click" CommandArgument='<%#Eval("ID")%>' Visible="true">
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" OnClick="buttonDelete_Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
</table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
            <p>
                &nbsp;
            </p>

        </div>
    </div>
</asp:Content>




