﻿using LTS_CRM.App_Code.BLL.Product;
using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Product
{
    public partial class ProductAddEdit : CRMPage
    {
        #region Product Save & Edit
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTable(ddlTypeOfProduct, "ProductType", "Select Product Type");
                LoadCategoryFromGeneralTable(ddlContractType, "ProductContractType", "Select Contract Type");
                LoadCategoryFromGeneralTable(ddlCurrency, "Currency", "Select Currency");
                LoadCategoryFromGeneralTable(ddlContractWith, "ContractWith", "Select Contract with");
                LoadCategoryFromGeneralTable(ddlContract, "Contract", "Select Contract");
                pnlReselPriceSection.Visible = pnlResellProduct.Visible = false;

                BindFormDetails();

                //Document Grid
                BindSelfDocumentGrid();
            }
            if (TravelSession.Product.ProductId.HasValue)
                setTabs();
        }

        private void BindFormDetails()
        {
            if (TravelSession.Product.ProductId.HasValue)
            {
                DLProduct obj = BLProduct.GetProductById(TravelSession.Product.ProductId.Value);
                if (!obj.CreationDate.IsNull && IsDate(Convert.ToString(obj.CreationDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.CreationDate.Value));
                    txtCreationDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtContractedBy.Text = obj.ContractedBy.Value;
                txtDataEntryBy.Text = obj.DataEntryBy.Value;
                if (ddlProductType.Items.FindByText(Convert.ToString(obj.ProductType.Value)) != null)
                {
                    ddlProductType.ClearSelection();
                    ddlProductType.Items.FindByText(Convert.ToString(obj.ProductType.Value)).Selected = true;
                }
                if (ddlProductType.SelectedValue == "1")
                {
                    pnlResellProduct.Visible = pnlReselPriceSection.Visible = true;
                    LoadResellers();
                }
                ddlProductType.Enabled = false;
                if (!obj.SupplierName.IsNull)
                {
                    if (ddlSupplierName.Items.FindByValue(Convert.ToString(obj.SupplierName.Value)) != null)
                    {
                        ddlSupplierName.ClearSelection();
                        ddlSupplierName.Items.FindByValue(Convert.ToString(obj.SupplierName.Value)).Selected = true;
                    }
                }
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                if (ddlContract.Items.FindByText(Convert.ToString(obj.Contract.Value)) != null)
                {
                    ddlContract.ClearSelection();
                    ddlContract.Items.FindByText(Convert.ToString(obj.Contract.Value)).Selected = true;
                }
                if (ddlContractWith.Items.FindByText(Convert.ToString(obj.ContractWith.Value)) != null)
                {
                    ddlContractWith.ClearSelection();
                    ddlContractWith.Items.FindByText(Convert.ToString(obj.ContractWith.Value)).Selected = true;
                }
                if (ddlCurrency.Items.FindByText(Convert.ToString(obj.Currency.Value)) != null)
                {
                    ddlCurrency.ClearSelection();
                    ddlCurrency.Items.FindByText(Convert.ToString(obj.Currency.Value)).Selected = true;
                }
                txtEmergencyNo.Text = obj.EmergencyNo.Value;
                txtReservationEmail.Text = obj.ReservationEmail.Value;
                txtAlternationEmail.Text = obj.AlternationEmail.Value;
                if (!obj.ContractType.IsNull)
                {
                    if (ddlContractType.Items.FindByText(Convert.ToString(obj.ContractType.Value)) != null)
                    {
                        ddlContractType.ClearSelection();
                        ddlContractType.Items.FindByText(Convert.ToString(obj.ContractType.Value)).Selected = true;
                    }
                }

                if (!obj.SupplierId.IsNull)
                {
                    if (ddlSupplierName.Items.FindByValue(Convert.ToString(obj.SupplierId.Value)) != null)
                    {
                        ddlSupplierName.ClearSelection();
                        ddlSupplierName.Items.FindByValue(Convert.ToString(obj.SupplierId.Value)).Selected = true;
                    }
                }
                if (!obj.TypeOfProduct.IsNull)
                {
                    if (ddlTypeOfProduct.Items.FindByText(Convert.ToString(obj.TypeOfProduct.Value)) != null)
                    {
                        ddlTypeOfProduct.ClearSelection();
                        ddlTypeOfProduct.Items.FindByText(Convert.ToString(obj.TypeOfProduct.Value)).Selected = true;
                    }
                }
                cmbcountry.Value = obj.Country.Value;
                txtRegion.Text = obj.Region.Value;
                txtTitle.Text = obj.ProductTitle.Value;
                if (!obj.Comment.IsNull)
                    CKEtxtComment.Text = obj.Comment.Value;
                if (!obj.HowToBook.IsNull)
                    ckhowtobook.Text = obj.HowToBook.Value;
                if (!obj.HowToSell.IsNull)
                    CKEditorControlHowToSell.Text = obj.HowToSell.Value;
                if (!obj.PriceIncluding.IsNull)
                    CKEditorControlIncluding.Text = obj.PriceIncluding.Value;
                if (!obj.PriceExcluding.IsNull)
                    CKEditorControlExcluding.Text = obj.PriceExcluding.Value;
                txtNoOfDays.Text = Convert.ToString(obj.NoOfDays.Value);
                txtNoOfNights.Text = Convert.ToString(obj.NoOfNights.Value);
                if (!obj.NetRate.IsNull)
                    txtNetRate.Text = Convert.ToString(obj.NetRate.Value);
                if (!obj.Commission.IsNull)
                    txtCommission.Text = Convert.ToString(obj.Commission.Value);
                if (!obj.Price.IsNull)
                    txtPrice.Text = Convert.ToString(obj.Price.Value);
                if (!obj.ContactTitle.IsNull)
                    ddlTitle.Value = obj.ContactTitle.Value;
                if (!obj.ContactSurName.IsNull)
                    txtSurnameMain.Text = obj.ContactSurName.Value;
                if (!obj.ContactFirstName.IsNull)
                    txtFirstNameMain.Text = obj.ContactFirstName.Value;
                if (!obj.ContactPosition.IsNull)
                    txtPositionMain.Text = obj.ContactPosition.Value;
                if (!obj.ContactDepartment.IsNull)
                    txtDepartmentMain.Text = obj.ContactDepartment.Value;
                if (!obj.ContactEmail.IsNull)
                    txtEmailMain.Text = obj.ContactEmail.Value;
                if (!obj.ContactDirectPhone.IsNull)
                    txtTelephoneMain.Text = obj.ContactDirectPhone.Value;
            }
        }

        private void ClearFormDetails()
        {
            TravelSession.Product.ProductId = null;
            txtCreationDate.Text = "";
            txtContractedBy.Text = "";
            txtDataEntryBy.Text = "";
            ddlProductType.ClearSelection();
            ddlSupplierName.ClearSelection();
            ddlCurrency.ClearSelection();
            ddlContractWith.ClearSelection();
            txtEmergencyNo.Text = "";
            txtReservationEmail.Text = "";
            txtAlternationEmail.Text = "";
            ddlContract.ClearSelection();
            ddlContractType.ClearSelection();
            txtRegion.Text = "";
            ddlTypeOfProduct.ClearSelection();
            txtTitle.Text = "";
            CKEtxtComment.Text = "";
            ckhowtobook.Text = "";

        }

        private void SaveDetails()
        {
            DLProduct obj = new DLProduct();
            string resCreationDate = txtCreationDate.Text.Trim();
            if (resCreationDate != "")
            {
                obj.CreationDate = DateTime.ParseExact(resCreationDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.ContractedBy = txtContractedBy.Text;
            obj.DataEntryBy = txtDataEntryBy.Text;
            if (ddlProductType.SelectedItem != null)
                obj.ProductType = ddlProductType.SelectedItem.Text;
            if (ddlSupplierName.SelectedItem != null)
                obj.SupplierName = ddlSupplierName.SelectedItem.Text;
            obj.Contract = ddlContract.SelectedItem.Text;
            obj.ContractWith = ddlContractWith.SelectedItem.Text;
            obj.EmergencyNo = txtEmergencyNo.Text;
            obj.ReservationEmail = txtReservationEmail.Text;
            obj.AlternationEmail = txtAlternationEmail.Text;
            obj.Currency = ddlCurrency.SelectedItem.Text;
            if (ddlContractType.SelectedItem != null)
                obj.ContractType = ddlContractType.SelectedItem.Text;
            obj.Country = cmbcountry.Value;
            obj.Region = txtRegion.Text;
            if (ddlTypeOfProduct.SelectedItem != null)
                obj.TypeOfProduct = ddlTypeOfProduct.SelectedItem.Text;
            obj.ProductTitle = txtTitle.Text;
            obj.Comment = CKEtxtComment.Text;
            obj.HowToBook = ckhowtobook.Text;
            obj.PriceIncluding = CKEditorControlIncluding.Text;
            obj.PriceExcluding = CKEditorControlExcluding.Text;
            obj.NoOfDays = !string.IsNullOrEmpty(txtNoOfDays.Text) ? Convert.ToInt32(txtNoOfDays.Text) : 0;
            obj.NoOfNights = !string.IsNullOrEmpty(txtNoOfNights.Text) ? Convert.ToInt32(txtNoOfNights.Text) : 0;
            obj.NetRate = !string.IsNullOrEmpty(txtNetRate.Text) ? Convert.ToDecimal(txtNetRate.Text) : 0;
            obj.Commission = !string.IsNullOrEmpty(txtCommission.Text) ? Convert.ToDecimal(txtCommission.Text) : 0;
            obj.ContractType = ddlContractType.SelectedItem.Text;
            obj.Price = !string.IsNullOrEmpty(txtPrice.Text) ? Convert.ToDecimal(txtPrice.Text) : 0;
            obj.SupplierId = Convert.ToInt32(ddlSupplierName.SelectedValue);
            obj.ContactTitle = ddlTitle.Value;
            obj.ContactSurName = txtSurnameMain.Text;
            obj.ContactFirstName = txtFirstNameMain.Text;
            obj.ContactPosition = txtPositionMain.Text;
            obj.ContactDepartment = txtDepartmentMain.Text;
            obj.ContactEmail = txtEmailMain.Text;
            obj.ContactDirectPhone = txtTelephoneMain.Text;
            if (TravelSession.Product.ProductId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Product.ProductId.Value;
                BLProduct.UpdateProduct(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLProduct.SaveProduct(obj);
            }
            ClearFormDetails();
            Response.Redirect("Product.aspx", true);
        }

        private void setTabs()
        {
            //tab2_pan.Visible = false;
            //tab3_pan.Visible = false;
            tab4_pan.Visible = false;
            tab7_pan.Visible = false;
            tab8_pan.Visible = false;

            //tab2.Visible = false;
            //tab3.Visible = false;
            tab4.Visible = false;
            tab7.Visible = false;
            tab8.Visible = false;
            if (ddlProductType.SelectedValue == "0")
            {
                //tab5_pan.Visible = true;
                //tab6_pan.Visible = tab5_pan.Visible;
                tab7_pan.Visible = true;//tab5_pan.Visible;
                tab8_pan.Visible = true;//tab5_pan.Visible;
                //tab5.Visible = tab5_pan.Visible;
                //tab6.Visible = tab5_pan.Visible;
                tab7.Visible = true;//tab5_pan.Visible;
                tab8.Visible = true;//tab5_pan.Visible;
            }
            else
            {

                //tab5_pan.Visible = true;
                tab4_pan.Visible = true;//tab5_pan.Visible;
                //tab3_pan.Visible = true;//tab5_pan.Visible;
                //tab2_pan.Visible = true;//tab5_pan.Visible;
                //tab2.Visible = true;//tab5_pan.Visible;
                //tab3.Visible = true;//tab5_pan.Visible;
                tab4.Visible = true;//tab5_pan.Visible;
                                    //tab5.Visible = tab5_pan.Visible;
                tab8.Visible = true;
                tab8_pan.Visible = true;
            }
            string selectedTab = "";
            if (Request.Cookies["activetab"] != null)
            {
                selectedTab = Request.Cookies["activetab"].Value;
            }

            tab1.Attributes.Remove("class");
            // tab2.Attributes.Remove("class");
            // tab3.Attributes.Remove("class");
            tab4.Attributes.Remove("class");
            // tab5.Attributes.Remove("class");
            tab1_pan.Attributes.Remove("class");
            //tab2_pan.Attributes.Remove("class");
            //tab3_pan.Attributes.Remove("class");
            tab4_pan.Attributes.Remove("class");

            tab7.Attributes.Remove("class");
            tab8.Attributes.Remove("class");
            tab7_pan.Attributes.Remove("class");
            tab8_pan.Attributes.Remove("class");
            //tab5_pan.Attributes.Remove("class");

            tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            //tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            //tab3.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab7.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab8.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            // tab5.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab1_pan.Attributes.Add("class", "tab-pane ");
            //tab2_pan.Attributes.Add("class", "tab-pane ");
            //tab3_pan.Attributes.Add("class", "tab-pane ");
            tab4_pan.Attributes.Add("class", "tab-pane ");

            tab7_pan.Attributes.Add("class", "tab-pane ");
            tab8_pan.Attributes.Add("class", "tab-pane ");
            //tab5_pan.Attributes.Add("class", "tab-pane ");

            switch (selectedTab)
            {


                case "#tab4_pan":
                    {
                        if (!tab4.Visible)
                            goto default;
                        tab4.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab4_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab7_pan":
                    {
                        if (!tab7.Visible)
                            goto default;
                        tab7.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab7_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                case "#tab8_pan":
                    {
                        if (!tab8.Visible)
                            goto default;
                        tab8.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab8_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }
                default:
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                    tab1_pan.Attributes.Add("class", "tab-pane active");
                    break;
            }
        }

        protected void ddlProductType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = ((DropDownList)sender);
            setTabs();
            pnlResellProduct.Visible = pnlReselPriceSection.Visible = false;
            if (ddl.SelectedValue == "1")
            {
                pnlResellProduct.Visible = pnlReselPriceSection.Visible = true;
                LoadResellers();
            }
        }

        private void LoadResellers()
        {
            DataTable dt = new DataTable();
            LoadAllSuppliers(ddlSupplierName, out dt);
            ViewState["SupplierDetails"] = dt;
        }

        protected void btnProductSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void ProductCancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("Product.aspx", true);
        }
        #endregion

        protected void buttonHowToBook_ServerClick(object sender, EventArgs e)
        {
            if (TravelSession.Product.ProductId.HasValue)
            {
                int ProductId = TravelSession.Product.ProductId.Value;
                DLProduct product = BLProduct.GetProductById(ProductId);
                product.HowToBook = ckhowtobook.Text;
                BLProduct.UpdateProduct(product);
            }
        }


        #region Self Product
        protected void buttonHowToSell_ServerClick(object sender, EventArgs e)
        {
            if (TravelSession.Product.ProductId.HasValue)
            {
                int ProductId = TravelSession.Product.ProductId.Value;
                DLProduct product = BLProduct.GetProductById(ProductId);
                product.HowToSell = CKEditorControlHowToSell.Text;
                BLProduct.UpdateProduct(product);
            }
        }

        protected void buttonUploadSelfDocument_ServerClick(object sender, EventArgs e)
        {
            DLProductDocument doc = new DLProductDocument();
            if (UploadSelfDocument.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = UploadSelfDocument.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                doc.FileData = fileData;
                doc.FileName = name;
            }
            doc.ProductID = TravelSession.Product.ProductId.Value;
            if (TravelSession.Product.ProductSelfDocumentId.HasValue)
            {
                doc.UpdatedBy = TravelSession.Current.UserName;
                doc.UpdatedDate = DateTime.Now;
                BLProductDocument.UpdateProductDocument(doc);
            }
            else
            {
                doc.CreatedBy = TravelSession.Current.UserName;
                doc.CreatedDate = DateTime.Now;
                BLProductDocument.SaveProductDocument(doc);
            }
            BindSelfDocumentGrid();
        }

        protected void BindSelfDocumentGrid()
        {
            if (TravelSession.Product.ProductId.HasValue)
            {
                int productId = TravelSession.Product.ProductId.Value;
                rptSelfProductDocument.DataSource = BLProductDocument.GetProductDocumentListByProductId(productId);
                rptSelfProductDocument.DataBind();
            }
        }

        protected void buttonSelfProductDocumentDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLProductDocument.DeleteProductDocument(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindSelfDocumentGrid();
        }

        #endregion

        #region Contact Save
        //protected void btnProductContactSave_ServerClick(object sender, EventArgs e)
        //{
        //    int ProductId = TravelSession.Product.ProductId.Value;
        //    DLProduct obj = BLProduct.GetProductById(ProductId);
        //    obj.ContactTitle = ddlTitle1.Value;
        //    obj.ContactSurName = txtSurName1.Text;
        //    obj.ContactFirstName = txtFirstName1.Text;
        //    obj.ContactEmail = txtcontactEmail1.Text;
        //    obj.ContactFaxNo = txtcontactFax1.Text;
        //    obj.ContactMobileNo = txtcontactMobile1.Text;
        //    obj.ContactDirectPhone = txtcontactTelephone1.Text;
        //    obj.ContactDepartment = txtDepartment1.Text;
        //    obj.Facebook = txtFacebook1.Text;
        //    obj.LinkedIn = txtLinkedin1.Text;
        //    obj.ContactPosition = txtPosition1.Text;
        //    obj.Skype = txtSkype1.Text;
        //    obj.Twitter = txtTwitter1.Text;
        //    obj.WeChat = txtWeChat1.Text;
        //    obj.Whatsup = txtWhatsapp1.Text;
        //    BLProduct.UpdateProduct(obj);
        //} 

        #endregion
    }
}