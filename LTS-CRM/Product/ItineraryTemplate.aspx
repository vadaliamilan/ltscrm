﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="ItineraryTemplate.aspx.cs" Inherits="LTS_CRM.Product.ItineraryTemplate" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Literal ID="output" runat="server" />
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Product</li>
                    <li>Itinerary</li>
                </ol>
            </div>
            <p>
                &nbsp;
            </p>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Itinerary List</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <div class="row">
                                            <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                <div class="input-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                                <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="lnkAddNew" OnClick="lnkAddNew_Click" runat="server"><i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span></asp:LinkButton>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="rptItineraryTemplate" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                            <HeaderTemplate>
                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                    <thead>
                                                        <tr>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="ID" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Creation Date" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Staff" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Country" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Region" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Tour Type" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="No Of Days" />
                                                            </th>
                                                            <th class="hasinput" style="width: 15%">
                                                                <input type="text" class="form-control" placeholder="Subject Title" />
                                                            </th>
                                                            <th class="hasinput" style="width: 5%"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th data-class="expand">Creation Date</th>
                                                            <th data-hide="phone">Staff</th>
                                                            <th data-hide="phone">Country</th>
                                                            <th data-hide="phone">Region</th>
                                                            <th data-hide="phone">Tour Type</th>
                                                            <th data-hide="phone">No Of Days</th>
                                                            <th data-hide="phone">Title</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "CreatedDate","{0:dd/MM/yyyy}")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Staff")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Country")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Region")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "TourType")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "NoOfDays")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "SubjectTitle")%></td>
                                                    <td>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" Text="Click" OnClick="buttonEdit_Click" CommandArgument='<%#Eval("ID")%>' Visible="true">
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" OnClick="buttonDelete_Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
</table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
            <p>
                &nbsp;
            </p>

        </div>
    </div>
</asp:Content>




