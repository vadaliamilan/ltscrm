﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Product
{
    public partial class ItineraryTemplate : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            rptItineraryTemplate.DataSource = BLItineraryTemplate.GetItineraryTemplateList();
            rptItineraryTemplate.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Product.ItineraryTemplateId = null;
            Response.Redirect("~/Product/ItineraryTemplateAddEdit.aspx", false);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Product.ItineraryTemplateId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Product/ItineraryTemplateAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLItineraryTemplate.DeleteItineraryTemplate(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();
        }
    }
}