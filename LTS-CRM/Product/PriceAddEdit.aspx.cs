﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Product
{
    public partial class PriceAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTable(DropDownListLanguage, "Language", "Select Language");
                BindFormDetails();
            }
        }
        private void BindFormDetails()
        {
            if (TravelSession.Product.PriceTemplateId.HasValue)
            {
                DLPriceTemplate obj = new DLPriceTemplate();
                obj = BLPriceTemplate.GetPriceTemplateById(TravelSession.Product.PriceTemplateId.Value);
                if (DropDownListLanguage.Items.FindByText(Convert.ToString(obj.Language.Value)) != null)
                {
                    DropDownListLanguage.ClearSelection();
                    DropDownListLanguage.Items.FindByText(Convert.ToString(obj.Language.Value)).Selected = true;
                }
                txtStaff.Text = obj.Staff.Value;
                txtTitle.Text = obj.Title.Value;
                txtDescription.Text = obj.Description.Value;
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
            }
        }
        private void SaveDetails()
        {
            DLPriceTemplate obj = new DLPriceTemplate();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.Staff = txtStaff.Text;
            obj.Language =DropDownListLanguage.SelectedItem.Text;
            obj.Title = txtTitle.Text;
            obj.Description = txtDescription.Text;
            if (TravelSession.Product.PriceTemplateId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Product.PriceTemplateId.Value;
                BLPriceTemplate.UpdatePriceTemplate(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLPriceTemplate.SavePriceTempate(obj);
            }

            ClearFormDetails();
            Response.Redirect("Price.aspx", true);

        }
        private void ClearFormDetails()
        {
            txtStaff.Text = "";
            DropDownListLanguage.ClearSelection();
            txtTitle.Text = "";
            txtDescription.Text = "";
        }

        protected void btnPriceTemplateSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("Price.aspx", true);
        }
    }
}