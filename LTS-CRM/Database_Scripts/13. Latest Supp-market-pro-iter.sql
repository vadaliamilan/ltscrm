﻿/********** New/Updated Tables  Scripts ************/
 
GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_Without_vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_Without_vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_tips'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_tips'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_Day_coach_Guide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_Day_coach_Guide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_tips'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_tips'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_coach_Guide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_coach_Guide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'ToDate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'ToDate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'FromDate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'FromDate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Season'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Season'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'CVOfGuide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'CVOfGuide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'GuideLicence'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'GuideLicence'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AddressProof'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AddressProof'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'PassportCopy'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'PassportCopy'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'VehiclePhoto'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'VehiclePhoto'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'DrivingLicence'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'DrivingLicence'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'Comments'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'Comments'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpecialRequirement'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpecialRequirement'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupNotWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupNotWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'TypeOfWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'TypeOfWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AreaOfWorking'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AreaOfWorking'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'NoOfYearTourGuide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'NoOfYearTourGuide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpeakingLanguage'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpeakingLanguage'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CreatedBy'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'FoodPictureFile'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'FoodPictureFile'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'Rate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'Rate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'TypeOfFood'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'TypeOfFood'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierEntranceTicketRate', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierEntranceTicketRate', @level2type=N'COLUMN',@level2name=N'CreatedBy'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierDMCRate', N'COLUMN',N'FileData'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierDMCRate', @level2type=N'COLUMN',@level2name=N'FileData'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ItineraryTemplate', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItineraryTemplate', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate] DROP CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide] DROP CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTipsDetail_SupplierTipsDetail]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]'))
ALTER TABLE [dbo].[SupplierTipsDetail] DROP CONSTRAINT [FK_SupplierTipsDetail_SupplierTipsDetail]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] DROP CONSTRAINT [FK_SuppliersDetails_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] DROP CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierRestaurantRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]'))
ALTER TABLE [dbo].[SupplierRestaurantRate] DROP CONSTRAINT [FK_SupplierRestaurantRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierEntranceTicketRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]'))
ALTER TABLE [dbo].[SupplierEntranceTicketRate] DROP CONSTRAINT [FK_SupplierEntranceTicketRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierDMCRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]'))
ALTER TABLE [dbo].[SupplierDMCRate] DROP CONSTRAINT [FK_SupplierDMCRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet] DROP CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [FK_MarketingContacts_MarketingChannel]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Campaign_MarketingProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]'))
ALTER TABLE [dbo].[MarketingCampaign] DROP CONSTRAINT [FK_Campaign_MarketingProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItineraryPhotos_ItineraryPhotos]') AND parent_object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]'))
ALTER TABLE [dbo].[ItineraryPhotos] DROP CONSTRAINT [FK_ItineraryPhotos_ItineraryPhotos]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [DF_MarketingContacts_UpdatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [DF_MarketingContacts_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] DROP CONSTRAINT [DF_MarketingChannel_UpdatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] DROP CONSTRAINT [DF_MarketingChannel_CreatedDate]
END

GO
/****** Object:  Table [dbo].[TaskList]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TaskList]') AND type in (N'U'))
DROP TABLE [dbo].[TaskList]
GO
/****** Object:  Table [dbo].[SupplierTourGuideRate]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTourGuideRate]
GO
/****** Object:  Table [dbo].[SupplierTourGuide]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTourGuide]
GO
/****** Object:  Table [dbo].[SupplierTipsDetail]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTipsDetail]
GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
DROP TABLE [dbo].[SuppliersDetails]
GO
/****** Object:  Table [dbo].[SupplierRestaurantRate]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierRestaurantRate]
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketRate]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierEntranceTicketRate]
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketFit]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketFit]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierEntranceTicketFit]
GO
/****** Object:  Table [dbo].[SupplierDMCRate]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierDMCRate]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
DROP TABLE [dbo].[Product]
GO
/****** Object:  Table [dbo].[PriceTemplate]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PriceTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[PriceTemplate]
GO
/****** Object:  Table [dbo].[OpinionSheetCategory]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheetCategory]') AND type in (N'U'))
DROP TABLE [dbo].[OpinionSheetCategory]
GO
/****** Object:  Table [dbo].[OpinionSheet]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheet]') AND type in (N'U'))
DROP TABLE [dbo].[OpinionSheet]
GO
/****** Object:  Table [dbo].[MarketingProject]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingProject]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingProject]
GO
/****** Object:  Table [dbo].[MarketingContacts]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingContacts]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingContacts]
GO
/****** Object:  Table [dbo].[MarketingChannel]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingChannel]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingChannel]
GO
/****** Object:  Table [dbo].[MarketingCampaign]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingCampaign]
GO
/****** Object:  Table [dbo].[LeadManagement]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LeadManagement]') AND type in (N'U'))
DROP TABLE [dbo].[LeadManagement]
GO
/****** Object:  Table [dbo].[ItineraryTemplate]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[ItineraryTemplate]
GO
/****** Object:  Table [dbo].[ItineraryPhotos]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]') AND type in (N'U'))
DROP TABLE [dbo].[ItineraryPhotos]
GO
/****** Object:  Table [dbo].[GeneralCategoryItems]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralCategoryItems]') AND type in (N'U'))
DROP TABLE [dbo].[GeneralCategoryItems]
GO
/****** Object:  Table [dbo].[ContactManagement]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactManagement]') AND type in (N'U'))
DROP TABLE [dbo].[ContactManagement]
GO
/****** Object:  Table [dbo].[AppointmentList]    Script Date: 09-05-2017 18:30:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppointmentList]') AND type in (N'U'))
DROP TABLE [dbo].[AppointmentList]
GO
/****** Object:  Table [dbo].[AppointmentList]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppointmentList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AppointmentList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Subject] [nvarchar](250) NULL,
	[Status] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[AppointmentDate] [datetime] NULL,
	[AppointmentTime] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_AppointmentList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactManagement]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactManagement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactManagement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[Status] [nvarchar](50) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ContactManagement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneralCategoryItems]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralCategoryItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeneralCategoryItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](150) NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_GeneralCategoryItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ItineraryPhotos]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ItineraryPhotos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItineraryID] [int] NULL,
	[Photos] [varbinary](max) NULL,
	[DocType] [nvarchar](20) NULL,
	[Extension] [nvarchar](10) NULL,
	[FileName] [nvarchar](50) NULL,
	[RealFileName] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ItineraryPhotos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItineraryTemplate]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryTemplate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ItineraryTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[Staff] [nvarchar](250) NULL,
	[Country] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[TourType] [nvarchar](50) NULL,
	[NoOfDays] [int] NULL,
	[NoOfNights] [int] NULL,
	[SubjectTitle] [nvarchar](250) NULL,
	[ItineraryTemplate] [nvarchar](max) NULL,
	[ItineraryPhotoId] [int] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ItineraryTemplate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[LeadManagement]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LeadManagement]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[LeadManagement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[CampaignID] [int] NULL,
	[TourType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Gender] [nvarchar](10) NOT NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[PreferredContactTime] [nvarchar](50) NULL,
	[PreferredContactMethod] [nvarchar](50) NULL,
	[PlannedDepartureDate] [datetime] NULL,
	[HowManyDays] [int] NULL,
	[HowManyPeople] [int] NULL,
	[HowManyChild] [int] NULL,
	[PlaceOfDeparture] [nvarchar](50) NULL,
	[PlaceOfReturn] [nvarchar](50) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[Currency] [nvarchar](15) NULL,
	[TourGuide] [nvarchar](50) NULL,
	[FlightsNeeded] [bit] NULL,
	[Vehicle] [nvarchar](50) NULL,
	[HotelType] [nvarchar](50) NULL,
	[Meals] [nvarchar](50) NULL,
	[AdmissionTickets] [nvarchar](50) NULL,
	[Insurance] [nvarchar](50) NULL,
	[LeadSource] [nvarchar](50) NULL,
	[LeadOwner] [nvarchar](50) NULL,
	[LeadStatus] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_LeadManagement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
GO
/****** Object:  Table [dbo].[MarketingCampaign]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingCampaign](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ProjectID] [int] NULL,
	[CampaignName] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CampaignDescription] [nvarchar](max) NULL,
	[EstimateBudget] [decimal](18, 2) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[Email] [nvarchar](50) NULL,
	[AlternateEmail] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MarketingChannel]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingChannel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingChannel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContractedBy] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](100) NULL,
	[CompanyWebsite] [nvarchar](250) NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](250) NULL,
	[CompanyTelephone] [nvarchar](50) NULL,
	[CompanyEmail] [nvarchar](50) NULL,
	[CompanyAddress1] [nvarchar](100) NULL,
	[CompanyAddress2] [nvarchar](100) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostCode] [nvarchar](50) NULL,
	[AdvertisingCost] [nvarchar](50) NULL,
	[AmountOfFee] [decimal](18, 2) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[BlackList] [nvarchar](50) NULL,
	[Rating] [nvarchar](50) NULL,
	[MainBusiness] [nvarchar](max) NULL,
	[HowToUseMarketingChannel] [nvarchar](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[FirstDayOfCollabration] [nvarchar](250) NULL,
	[ContractExpireDate] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
	[SignedAgreedContractUpload] [varbinary](max) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[ExtraField1] [nvarchar](50) NULL,
	[ExtraField2] [nvarchar](50) NULL,
	[ExtraField3] [nvarchar](50) NULL,
	[ExtraField4] [nvarchar](50) NULL,
	[ExtraField5] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingChannel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MarketingContacts]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingContacts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingContacts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MarketingChannelID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingContacts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MarketingProject]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingProject]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingProject](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[ProjectName] [nvarchar](250) NULL,
	[ProjectDescribution] [nvarchar](max) NULL,
	[InitiateBy] [nvarchar](50) NULL,
	[StaffInvolved] [nvarchar](50) NULL,
	[ProjectStartingDate] [datetime] NULL,
	[ProjectPlannedEndingDate] [datetime] NULL,
	[WorkingProcessComment] [nvarchar](max) NULL,
	[ProjectActualEndDate] [datetime] NULL,
	[ProjectIncome] [decimal](18, 2) NULL,
	[ProjectExpense] [decimal](18, 2) NULL,
	[ProjectGrossProfit] [decimal](18, 2) NULL,
	[Rating] [nvarchar](50) NULL,
	[ProjectReport] [nvarchar](max) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[TotalEstimateBudget] [decimal](18, 2) NULL,
	[ExtraField1] [nvarchar](50) NULL,
	[ExtraField2] [nvarchar](50) NULL,
	[ExtraField3] [nvarchar](50) NULL,
	[ExtraField4] [nvarchar](50) NULL,
	[ExtraField5] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingProject] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[OpinionSheet]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpinionSheet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Date] [datetime] NULL,
	[Staff] [nvarchar](250) NULL,
	[OpinionSheetCategoryId] [int] NULL,
	[FileData] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OpinionSheet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpinionSheetCategory]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheetCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpinionSheetCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OpinionSheetCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PriceTemplate]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PriceTemplate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PriceTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Staff] [nvarchar](250) NULL,
	[Language] [nvarchar](50) NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_PriceTemplate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Product]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
	[ContractedBy] [nvarchar](250) NULL,
	[DataEntryBy] [nvarchar](250) NULL,
	[ProductType] [nvarchar](250) NULL,
	[SupplierId] [int] NULL,
	[SupplierName] [nvarchar](250) NULL,
	[Contract] [nvarchar](250) NULL,
	[ContractWith] [nvarchar](250) NULL,
	[EmergencyNo] [nvarchar](250) NULL,
	[ReservationEmail] [nvarchar](250) NULL,
	[AlternationEmail] [nvarchar](250) NULL,
	[Currency] [nvarchar](250) NULL,
	[ContractType] [nvarchar](250) NULL,
	[NetRate] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Country] [nvarchar](250) NULL,
	[Region] [nvarchar](250) NULL,
	[TypeOfProduct] [nvarchar](50) NULL,
	[NoOfDays] [int] NULL,
	[NoOfNights] [int] NULL,
	[ProductTitle] [nvarchar](250) NULL,
	[ItineraryTemplate] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NULL,
	[Comment] [nvarchar](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[PriceIncluding] [nvarchar](max) NULL,
	[PriceExcluding] [nvarchar](max) NULL,
	[ContactTitle] [nvarchar](50) NULL,
	[ContactSurName] [nvarchar](150) NOT NULL,
	[ContactFirstName] [nvarchar](510) NOT NULL,
	[ContactPosition] [nvarchar](50) NULL,
	[ContactDepartment] [nvarchar](150) NULL,
	[ContactEmail] [nvarchar](150) NULL,
	[ContactDirectPhone] [nvarchar](50) NULL,
	[ContactMobileNo] [nvarchar](50) NULL,
	[ContactFaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[ExtraField1] [nvarchar](250) NULL,
	[ExtraField2] [nvarchar](250) NULL,
	[ExtraField3] [nvarchar](250) NULL,
	[ExtraField4] [nvarchar](250) NULL,
	[ExtraField5] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierDMCRate]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierDMCRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[FileData] [varbinary](max) NULL,
	[NetRate] [decimal](18, 2) NULL,
	[GrossRate] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierDMCRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketFit]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketFit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierEntranceTicketFit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NoOfPeople] [int] NULL,
	[Adult] [decimal](18, 2) NULL,
 CONSTRAINT [PK_SupplierEntranceTicketFit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketRate]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierEntranceTicketRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[MonthFrom] [datetime] NULL,
	[MonthTo] [datetime] NULL,
	[DailyOpeningTime] [datetime] NULL,
	[DailyCloseTime] [datetime] NULL,
	[LastAdmission] [nvarchar](250) NULL,
	[Status] [nvarchar](50) NULL,
	[Remark] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierEntranceTicketRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierRestaurantRate]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierRestaurantRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[TypeOfFood] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[Rate] [decimal](18, 2) NULL,
	[SampleOfMenu] [nvarchar](max) NULL,
	[SampleMenuFile] [varbinary](max) NULL,
	[FoodPictureFile] [varbinary](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierRestaurantRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SuppliersDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierMasterID] [int] NULL,
	[SupplierTypeID] [int] NULL,
	[SupplierDate] [datetime] NULL,
	[ContractedBy] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[StarRating] [nvarchar](10) NULL,
	[Website] [nvarchar](150) NULL,
	[Agency] [nvarchar](100) NULL,
	[ClientCode] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[FaxNumber] [nvarchar](15) NULL,
	[EmergencyNo] [nvarchar](15) NULL,
	[Email] [nvarchar](50) NULL,
	[AlternateEmail] [nvarchar](50) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[HotelChain] [nvarchar](50) NULL,
	[Franchised] [nvarchar](50) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[CreditToLetsTravel] [nvarchar](50) NULL,
	[BlackList] [nvarchar](200) NULL,
	[ContractStartDate] [datetime] NULL,
	[ContractEndingDate] [datetime] NULL,
	[ContractRenewalDate] [datetime] NULL,
	[Rating] [nvarchar](10) NULL,
	[MainBusiness] [nvarchar](200) NULL,
	[Comments] [nvarchar](max) NULL,
	[FileData] [varbinary](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NULL,
	[FirstName] [nvarchar](510) NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[ContactEmail] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SuppliersDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTipsDetail]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTipsDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierTypeId] [int] NULL,
	[Tips] [decimal](18, 2) NULL,
	[Country] [nvarchar](50) NULL,
	[CountryId] [int] NULL,
	[AirportPickUp] [decimal](18, 2) NULL,
	[AirportDropOff] [decimal](18, 2) NULL,
	[WholeDay] [decimal](18, 2) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTipsDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierTourGuide]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTourGuide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[SupplierTypeId] [int] NULL,
	[TypeOfSupplier] [int] NULL,
	[RecruitedBy] [nvarchar](250) NULL,
	[SurName] [nvarchar](250) NULL,
	[SurNameInChinese] [nvarchar](250) NULL,
	[FirstName] [nvarchar](250) NULL,
	[FirstNameInChinese] [nvarchar](250) NULL,
	[DateOfBirth] [datetime] NULL,
	[Origin] [nvarchar](50) NULL,
	[Nationality] [nvarchar](50) NULL,
	[Married] [nvarchar](50) NULL,
	[Children] [int] NULL,
	[VisaStatus] [nvarchar](250) NULL,
	[HomePhone] [nvarchar](15) NULL,
	[MobilePhone] [nvarchar](15) NULL,
	[WeChat] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Address1] [nvarchar](500) NULL,
	[Address2] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostCode] [nvarchar](10) NULL,
	[SpeakingLanguage] [nvarchar](1000) NULL,
	[NoOfYearTourGuide] [int] NULL,
	[AreaOfWorking] [nvarchar](250) NULL,
	[TypeOfWork] [nvarchar](250) NULL,
	[VehicleOwned] [nvarchar](250) NULL,
	[KindOfGroupWork] [nvarchar](max) NULL,
	[KindOfGroupNotWork] [nvarchar](150) NULL,
	[SpecialRequirement] [nvarchar](max) NULL,
	[Rating] [int] NULL,
	[BlackList] [nvarchar](50) NULL,
	[Comments] [nvarchar](max) NULL,
	[DrivingLicence] [varbinary](max) NULL,
	[VehiclePhoto] [varbinary](max) NULL,
	[PassportCopy] [varbinary](max) NULL,
	[AddressProof] [varbinary](max) NULL,
	[GuideLicence] [varbinary](max) NULL,
	[CVOfGuide] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTourGuid] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTourGuideRate]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTourGuideRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierTourGuideID] [int] NULL,
	[Season] [nvarchar](150) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle] [decimal](10, 2) NULL,
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle] [decimal](10, 2) NULL,
	[Half_day_coach_Guide] [decimal](10, 2) NULL,
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Half_day_tips] [decimal](10, 2) NULL,
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle] [decimal](10, 2) NULL,
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle] [decimal](10, 2) NULL,
	[Full_Day_coach_Guide] [decimal](10, 2) NULL,
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle] [decimal](10, 2) NULL,
	[Full_day_tips] [decimal](10, 2) NULL,
	[Extra_hour_With_Vehicle] [decimal](10, 2) NULL,
	[Extra_hour_Without_vehicle] [decimal](10, 2) NULL,
	[London_Edinburgh_With_Vehicle] [decimal](10, 2) NULL,
	[London_Edinburgh_Without_Vehicle] [decimal](10, 2) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTourGuideRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TaskList]    Script Date: 09-05-2017 18:30:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TaskList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TaskList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Subject] [nvarchar](250) NULL,
	[AssignedTo] [nvarchar](150) NULL,
	[TaskStatus] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[DueDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_TaskList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] ADD  CONSTRAINT [DF_MarketingChannel_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] ADD  CONSTRAINT [DF_MarketingChannel_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] ADD  CONSTRAINT [DF_MarketingContacts_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] ADD  CONSTRAINT [DF_MarketingContacts_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItineraryPhotos_ItineraryPhotos]') AND parent_object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]'))
ALTER TABLE [dbo].[ItineraryPhotos]  WITH CHECK ADD  CONSTRAINT [FK_ItineraryPhotos_ItineraryPhotos] FOREIGN KEY([ItineraryID])
REFERENCES [dbo].[ItineraryTemplate] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItineraryPhotos_ItineraryPhotos]') AND parent_object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]'))
ALTER TABLE [dbo].[ItineraryPhotos] CHECK CONSTRAINT [FK_ItineraryPhotos_ItineraryPhotos]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Campaign_MarketingProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]'))
ALTER TABLE [dbo].[MarketingCampaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_MarketingProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[MarketingProject] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Campaign_MarketingProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]'))
ALTER TABLE [dbo].[MarketingCampaign] CHECK CONSTRAINT [FK_Campaign_MarketingProject]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts]  WITH CHECK ADD  CONSTRAINT [FK_MarketingContacts_MarketingChannel] FOREIGN KEY([MarketingChannelID])
REFERENCES [dbo].[MarketingChannel] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts] CHECK CONSTRAINT [FK_MarketingContacts_MarketingChannel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet]  WITH CHECK ADD  CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory] FOREIGN KEY([OpinionSheetCategoryId])
REFERENCES [dbo].[OpinionSheetCategory] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet] CHECK CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierDMCRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]'))
ALTER TABLE [dbo].[SupplierDMCRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierDMCRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierDMCRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]'))
ALTER TABLE [dbo].[SupplierDMCRate] CHECK CONSTRAINT [FK_SupplierDMCRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierEntranceTicketRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]'))
ALTER TABLE [dbo].[SupplierEntranceTicketRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierEntranceTicketRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierEntranceTicketRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]'))
ALTER TABLE [dbo].[SupplierEntranceTicketRate] CHECK CONSTRAINT [FK_SupplierEntranceTicketRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierRestaurantRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]'))
ALTER TABLE [dbo].[SupplierRestaurantRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierRestaurantRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierRestaurantRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]'))
ALTER TABLE [dbo].[SupplierRestaurantRate] CHECK CONSTRAINT [FK_SupplierRestaurantRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails]  WITH CHECK ADD  CONSTRAINT [FK_SuppliersDetails_SupplierMaster] FOREIGN KEY([SupplierMasterID])
REFERENCES [dbo].[SupplierMaster] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] CHECK CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails]  WITH CHECK ADD  CONSTRAINT [FK_SuppliersDetails_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] CHECK CONSTRAINT [FK_SuppliersDetails_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTipsDetail_SupplierTipsDetail]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]'))
ALTER TABLE [dbo].[SupplierTipsDetail]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTipsDetail_SupplierTipsDetail] FOREIGN KEY([SupplierTypeId])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTipsDetail_SupplierTipsDetail]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]'))
ALTER TABLE [dbo].[SupplierTipsDetail] CHECK CONSTRAINT [FK_SupplierTipsDetail_SupplierTipsDetail]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid] FOREIGN KEY([TypeOfSupplier])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide] CHECK CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide] FOREIGN KEY([SupplierTourGuideID])
REFERENCES [dbo].[SupplierTourGuide] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate] CHECK CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ItineraryTemplate', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItineraryTemplate', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierDMCRate', N'COLUMN',N'FileData'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Itinerary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierDMCRate', @level2type=N'COLUMN',@level2name=N'FileData'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierEntranceTicketRate', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Added By' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierEntranceTicketRate', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'TypeOfFood'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Food' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'TypeOfFood'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'Rate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rate /person' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'Rate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'FoodPictureFile'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Upload picuture of Food' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'FoodPictureFile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Added By' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpeakingLanguage'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Speaking Languages' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpeakingLanguage'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'NoOfYearTourGuide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No of years as tour guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'NoOfYearTourGuide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AreaOfWorking'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Area of Working' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AreaOfWorking'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'TypeOfWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Work' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'TypeOfWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What kind of groups that you have been working for?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupNotWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What kind of groups that you do not wish to work for?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupNotWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpecialRequirement'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special requirement from tour guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpecialRequirement'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'Comments'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comments from company staff' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'Comments'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'DrivingLicence'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Driving Licence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'DrivingLicence'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'VehiclePhoto'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle Photo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'VehiclePhoto'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'PassportCopy'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Passport Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'PassportCopy'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AddressProof'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Proof' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AddressProof'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'GuideLicence'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Guide Licence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'GuideLicence'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'CVOfGuide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CV of Guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'CVOfGuide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Season'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Winter season | Summer Season | Summer High season
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Season'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'FromDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'From Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'FromDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'ToDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'To Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'ToDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 4-9 seater driver guide without vehicle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 12 - 17 seater driver guide without vehicle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_coach_Guide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day coach guide
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_coach_Guide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 4-9 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 12 - 17 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_tips'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day tips
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_tips'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 4-9 seater driver guide without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 12- 17 seater driver guide without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_Day_coach_Guide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full Day coach guide
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_Day_coach_Guide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 4-9 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 12- 17 seater driver guide withvehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_tips'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day tips
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_tips'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extra hour with Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_Without_vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extra hour without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_Without_vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'London - Edinburgh with Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'London - Edinburgh Without Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_Without_Vehicle'
GO

/*****************END***************/
-- ========================================================================================================
-- [Stored Procedures generated for table:  AppointmentList]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_AppointmentList_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_AppointmentList_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'AppointmentList'
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daAppointmentDate datetime
-- Gets: @daAppointmentTime datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_Insert]
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daAppointmentDate datetime,
	@daAppointmentTime datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[AppointmentList]
(
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sSubject,
	@sStatus,
	@sDescription,
	@daAppointmentDate,
	@daAppointmentTime,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_AppointmentList_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_AppointmentList_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'AppointmentList'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daAppointmentDate datetime
-- Gets: @daAppointmentTime datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_Update]
	@iID int,
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daAppointmentDate datetime,
	@daAppointmentTime datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[AppointmentList]
SET 
	[CompanyID] = @iCompanyID,
	[Subject] = @sSubject,
	[Status] = @sStatus,
	[Description] = @sDescription,
	[AppointmentDate] = @daAppointmentDate,
	[AppointmentTime] = @daAppointmentTime,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_AppointmentList_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_AppointmentList_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'AppointmentList'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[AppointmentList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_AppointmentList_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_AppointmentList_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'AppointmentList'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[AppointmentList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_AppointmentList_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_AppointmentList_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'AppointmentList'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[AppointmentList]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     AppointmentList]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  ContactManagement]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ContactManagement_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ContactManagement_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'ContactManagement'
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ContactManagement]
(
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@sCompany,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sStatus,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ContactManagement_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ContactManagement_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'ContactManagement'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ContactManagement]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[Status] = @sStatus,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ContactManagement_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ContactManagement_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'ContactManagement'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ContactManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ContactManagement_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ContactManagement_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'ContactManagement'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ContactManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ContactManagement_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ContactManagement_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'ContactManagement'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ContactManagement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     ContactManagement]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  GeneralCategoryItems]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_GeneralCategoryItems_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_GeneralCategoryItems_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'GeneralCategoryItems'
-- Gets: @sCategory nvarchar(150)
-- Gets: @sTitle nvarchar(150)
-- Gets: @sDescription nvarchar(500)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_Insert]
	@sCategory nvarchar(150),
	@sTitle nvarchar(150),
	@sDescription nvarchar(500),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[GeneralCategoryItems]
(
	[Category],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@sCategory,
	@sTitle,
	@sDescription,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_GeneralCategoryItems_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_GeneralCategoryItems_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'GeneralCategoryItems'
-- Gets: @iID int
-- Gets: @sCategory nvarchar(150)
-- Gets: @sTitle nvarchar(150)
-- Gets: @sDescription nvarchar(500)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_Update]
	@iID int,
	@sCategory nvarchar(150),
	@sTitle nvarchar(150),
	@sDescription nvarchar(500),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[GeneralCategoryItems]
SET 
	[Category] = @sCategory,
	[Title] = @sTitle,
	[Description] = @sDescription,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_GeneralCategoryItems_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_GeneralCategoryItems_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'GeneralCategoryItems'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[GeneralCategoryItems]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_GeneralCategoryItems_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_GeneralCategoryItems_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'GeneralCategoryItems'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[Category],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[GeneralCategoryItems]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_GeneralCategoryItems_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_GeneralCategoryItems_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'GeneralCategoryItems'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[Category],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[GeneralCategoryItems]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     GeneralCategoryItems]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  ItineraryPhotos]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'ItineraryPhotos'
-- Gets: @iItineraryID int
-- Gets: @biPhotos varbinary(Max)
-- Gets: @sDocType nvarchar(20)
-- Gets: @sExtension nvarchar(10)
-- Gets: @sFileName nvarchar(50)
-- Gets: @sRealFileName nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_Insert]
	@iItineraryID int,
	@biPhotos varbinary(Max),
	@sDocType nvarchar(20),
	@sExtension nvarchar(10),
	@sFileName nvarchar(50),
	@sRealFileName nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ItineraryPhotos]
(
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iItineraryID,
	@biPhotos,
	@sDocType,
	@sExtension,
	@sFileName,
	@sRealFileName,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'ItineraryPhotos'
-- Gets: @iID int
-- Gets: @iItineraryID int
-- Gets: @biPhotos varbinary(Max)
-- Gets: @sDocType nvarchar(20)
-- Gets: @sExtension nvarchar(10)
-- Gets: @sFileName nvarchar(50)
-- Gets: @sRealFileName nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_Update]
	@iID int,
	@iItineraryID int,
	@biPhotos varbinary(Max),
	@sDocType nvarchar(20),
	@sExtension nvarchar(10),
	@sFileName nvarchar(50),
	@sRealFileName nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ItineraryPhotos]
SET 
	[ItineraryID] = @iItineraryID,
	[Photos] = @biPhotos,
	[DocType] = @sDocType,
	[Extension] = @sExtension,
	[FileName] = @sFileName,
	[RealFileName] = @sRealFileName,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [ItineraryID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'ItineraryPhotos'.
-- Will reset field [ItineraryID] with value @iItineraryIDOld  to value @iItineraryID
-- Gets: @iItineraryID int
-- Gets: @iItineraryIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]
	@iItineraryID int,
	@iItineraryIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ItineraryPhotos]
SET
	[ItineraryID] = @iItineraryID
WHERE
	[ItineraryID] = @iItineraryIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'ItineraryPhotos'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ItineraryPhotos]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [ItineraryID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'ItineraryPhotos'
-- based on a foreign key field.
-- Gets: @iItineraryID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]
	@iItineraryID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ItineraryPhotos]
WHERE
	[ItineraryID] = @iItineraryID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'ItineraryPhotos'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryPhotos]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'ItineraryPhotos'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryPhotos]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [ItineraryID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'ItineraryPhotos'
-- based on a foreign key field.
-- Gets: @iItineraryID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]
	@iItineraryID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryPhotos]
WHERE
	[ItineraryID] = @iItineraryID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     ItineraryPhotos]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  ItineraryTemplate]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryTemplate_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryTemplate_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'ItineraryTemplate'
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sTourType nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sSubjectTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @iItineraryPhotoId int
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_Insert]
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sCountry nvarchar(50),
	@sRegion nvarchar(50),
	@sTourType nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sSubjectTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@iItineraryPhotoId int,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ItineraryTemplate]
(
	[CompanyID],
	[Staff],
	[Country],
	[Region],
	[TourType],
	[NoOfDays],
	[NoOfNights],
	[SubjectTitle],
	[ItineraryTemplate],
	[ItineraryPhotoId],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sStaff,
	@sCountry,
	@sRegion,
	@sTourType,
	@iNoOfDays,
	@iNoOfNights,
	@sSubjectTitle,
	@sItineraryTemplate,
	@iItineraryPhotoId,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryTemplate_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryTemplate_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'ItineraryTemplate'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sTourType nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sSubjectTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @iItineraryPhotoId int
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_Update]
	@iID int,
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sCountry nvarchar(50),
	@sRegion nvarchar(50),
	@sTourType nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sSubjectTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@iItineraryPhotoId int,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ItineraryTemplate]
SET 
	[CompanyID] = @iCompanyID,
	[Staff] = @sStaff,
	[Country] = @sCountry,
	[Region] = @sRegion,
	[TourType] = @sTourType,
	[NoOfDays] = @iNoOfDays,
	[NoOfNights] = @iNoOfNights,
	[SubjectTitle] = @sSubjectTitle,
	[ItineraryTemplate] = @sItineraryTemplate,
	[ItineraryPhotoId] = @iItineraryPhotoId,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryTemplate_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryTemplate_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'ItineraryTemplate'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ItineraryTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryTemplate_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryTemplate_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'ItineraryTemplate'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Country],
	[Region],
	[TourType],
	[NoOfDays],
	[NoOfNights],
	[SubjectTitle],
	[ItineraryTemplate],
	[ItineraryPhotoId],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_ItineraryTemplate_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_ItineraryTemplate_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'ItineraryTemplate'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Country],
	[Region],
	[TourType],
	[NoOfDays],
	[NoOfNights],
	[SubjectTitle],
	[ItineraryTemplate],
	[ItineraryPhotoId],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryTemplate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     ItineraryTemplate]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  LeadManagement]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_LeadManagement_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_LeadManagement_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'LeadManagement'
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[LeadManagement]
(
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@iCampaignID,
	@sTourType,
	@sCompany,
	@sTitle,
	@sGender,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sPreferredContactTime,
	@sPreferredContactMethod,
	@daPlannedDepartureDate,
	@iHowManyDays,
	@iHowManyPeople,
	@iHowManyChild,
	@sPlaceOfDeparture,
	@sPlaceOfReturn,
	@dcTotalBudget,
	@sCurrency,
	@sTourGuide,
	@bFlightsNeeded,
	@sVehicle,
	@sHotelType,
	@sMeals,
	@sAdmissionTickets,
	@sInsurance,
	@sLeadSource,
	@sLeadOwner,
	@sLeadStatus,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_LeadManagement_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_LeadManagement_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'LeadManagement'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[LeadManagement]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[CampaignID] = @iCampaignID,
	[TourType] = @sTourType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[Gender] = @sGender,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[PreferredContactTime] = @sPreferredContactTime,
	[PreferredContactMethod] = @sPreferredContactMethod,
	[PlannedDepartureDate] = @daPlannedDepartureDate,
	[HowManyDays] = @iHowManyDays,
	[HowManyPeople] = @iHowManyPeople,
	[HowManyChild] = @iHowManyChild,
	[PlaceOfDeparture] = @sPlaceOfDeparture,
	[PlaceOfReturn] = @sPlaceOfReturn,
	[TotalBudget] = @dcTotalBudget,
	[Currency] = @sCurrency,
	[TourGuide] = @sTourGuide,
	[FlightsNeeded] = @bFlightsNeeded,
	[Vehicle] = @sVehicle,
	[HotelType] = @sHotelType,
	[Meals] = @sMeals,
	[AdmissionTickets] = @sAdmissionTickets,
	[Insurance] = @sInsurance,
	[LeadSource] = @sLeadSource,
	[LeadOwner] = @sLeadOwner,
	[LeadStatus] = @sLeadStatus,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_LeadManagement_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_LeadManagement_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'LeadManagement'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[LeadManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_LeadManagement_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_LeadManagement_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'LeadManagement'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[LeadManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_LeadManagement_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_LeadManagement_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'LeadManagement'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[LeadManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_LeadManagement_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_LeadManagement_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'LeadManagement'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[LeadManagement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     LeadManagement]
-- ========================================================================================================
GO

-- [End of Stored Procedures for table:     LeadManagement]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  MarketingCampaign]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'MarketingCampaign'
-- Gets: @iCompanyID int
-- Gets: @iProjectID int
-- Gets: @sCampaignName nvarchar(50)
-- Gets: @daStartDate datetime
-- Gets: @daEndDate datetime
-- Gets: @sCampaignDescription nvarchar(Max)
-- Gets: @dcEstimateBudget decimal(18, 2)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sFax nvarchar(50)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_Insert]
	@iCompanyID int,
	@iProjectID int,
	@sCampaignName nvarchar(50),
	@daStartDate datetime,
	@daEndDate datetime,
	@sCampaignDescription nvarchar(Max),
	@dcEstimateBudget decimal(18, 2),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sFax nvarchar(50),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingCampaign]
(
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iProjectID,
	@sCampaignName,
	@daStartDate,
	@daEndDate,
	@sCampaignDescription,
	@dcEstimateBudget,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sTelephone,
	@sEmail,
	@sAlternateEmail,
	@sFax,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'MarketingCampaign'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iProjectID int
-- Gets: @sCampaignName nvarchar(50)
-- Gets: @daStartDate datetime
-- Gets: @daEndDate datetime
-- Gets: @sCampaignDescription nvarchar(Max)
-- Gets: @dcEstimateBudget decimal(18, 2)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sFax nvarchar(50)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_Update]
	@iID int,
	@iCompanyID int,
	@iProjectID int,
	@sCampaignName nvarchar(50),
	@daStartDate datetime,
	@daEndDate datetime,
	@sCampaignDescription nvarchar(Max),
	@dcEstimateBudget decimal(18, 2),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sFax nvarchar(50),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingCampaign]
SET 
	[CompanyID] = @iCompanyID,
	[ProjectID] = @iProjectID,
	[CampaignName] = @sCampaignName,
	[StartDate] = @daStartDate,
	[EndDate] = @daEndDate,
	[CampaignDescription] = @sCampaignDescription,
	[EstimateBudget] = @dcEstimateBudget,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[Telephone] = @sTelephone,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[Fax] = @sFax,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [ProjectID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'MarketingCampaign'.
-- Will reset field [ProjectID] with value @iProjectIDOld  to value @iProjectID
-- Gets: @iProjectID int
-- Gets: @iProjectIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]
	@iProjectID int,
	@iProjectIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingCampaign]
SET
	[ProjectID] = @iProjectID
WHERE
	[ProjectID] = @iProjectIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'MarketingCampaign'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingCampaign]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [ProjectID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'MarketingCampaign'
-- based on a foreign key field.
-- Gets: @iProjectID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]
	@iProjectID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[MarketingCampaign]
WHERE
	[ProjectID] = @iProjectID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'MarketingCampaign'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingCampaign]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'MarketingCampaign'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingCampaign]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [ProjectID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'MarketingCampaign'
-- based on a foreign key field.
-- Gets: @iProjectID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]
	@iProjectID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingCampaign]
WHERE
	[ProjectID] = @iProjectID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     MarketingCampaign]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  MarketingChannel]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'MarketingChannel'
-- Gets: @iCompanyID int
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sCompanyName nvarchar(100)
-- Gets: @sCompanyWebsite nvarchar(250)
-- Gets: @sUsername nvarchar(50)
-- Gets: @sPassword nvarchar(250)
-- Gets: @sCompanyTelephone nvarchar(50)
-- Gets: @sCompanyEmail nvarchar(50)
-- Gets: @sCompanyAddress1 nvarchar(100)
-- Gets: @sCompanyAddress2 nvarchar(100)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(50)
-- Gets: @sAdvertisingCost nvarchar(50)
-- Gets: @dcAmountOfFee decimal(18, 2)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sRating nvarchar(50)
-- Gets: @sMainBusiness nvarchar(Max)
-- Gets: @sHowToUseMarketingChannel nvarchar(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sFirstDayOfCollabration nvarchar(250)
-- Gets: @daContractExpireDate datetime
-- Gets: @sStatus nvarchar(50)
-- Gets: @biSignedAgreedContractUpload varbinary(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Insert]
	@iCompanyID int,
	@sContractedBy nvarchar(50),
	@sCompanyName nvarchar(100),
	@sCompanyWebsite nvarchar(250),
	@sUsername nvarchar(50),
	@sPassword nvarchar(250),
	@sCompanyTelephone nvarchar(50),
	@sCompanyEmail nvarchar(50),
	@sCompanyAddress1 nvarchar(100),
	@sCompanyAddress2 nvarchar(100),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(50),
	@sAdvertisingCost nvarchar(50),
	@dcAmountOfFee decimal(18, 2),
	@sPaymentTerms nvarchar(Max),
	@sBlackList nvarchar(50),
	@sRating nvarchar(50),
	@sMainBusiness nvarchar(Max),
	@sHowToUseMarketingChannel nvarchar(Max),
	@sComments nvarchar(Max),
	@sFirstDayOfCollabration nvarchar(250),
	@daContractExpireDate datetime,
	@sStatus nvarchar(50),
	@biSignedAgreedContractUpload varbinary(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingChannel]
(
	[CompanyID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContractedBy,
	@sCompanyName,
	@sCompanyWebsite,
	@sUsername,
	@sPassword,
	@sCompanyTelephone,
	@sCompanyEmail,
	@sCompanyAddress1,
	@sCompanyAddress2,
	@sCity,
	@sCountry,
	@sPostCode,
	@sAdvertisingCost,
	@dcAmountOfFee,
	@sPaymentTerms,
	@sBlackList,
	@sRating,
	@sMainBusiness,
	@sHowToUseMarketingChannel,
	@sComments,
	@sFirstDayOfCollabration,
	@daContractExpireDate,
	@sStatus,
	@biSignedAgreedContractUpload,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'MarketingChannel'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sCompanyName nvarchar(100)
-- Gets: @sCompanyWebsite nvarchar(250)
-- Gets: @sUsername nvarchar(50)
-- Gets: @sPassword nvarchar(250)
-- Gets: @sCompanyTelephone nvarchar(50)
-- Gets: @sCompanyEmail nvarchar(50)
-- Gets: @sCompanyAddress1 nvarchar(100)
-- Gets: @sCompanyAddress2 nvarchar(100)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(50)
-- Gets: @sAdvertisingCost nvarchar(50)
-- Gets: @dcAmountOfFee decimal(18, 2)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sRating nvarchar(50)
-- Gets: @sMainBusiness nvarchar(Max)
-- Gets: @sHowToUseMarketingChannel nvarchar(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sFirstDayOfCollabration nvarchar(250)
-- Gets: @daContractExpireDate datetime
-- Gets: @sStatus nvarchar(50)
-- Gets: @biSignedAgreedContractUpload varbinary(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Update]
	@iID int,
	@iCompanyID int,
	@sContractedBy nvarchar(50),
	@sCompanyName nvarchar(100),
	@sCompanyWebsite nvarchar(250),
	@sUsername nvarchar(50),
	@sPassword nvarchar(250),
	@sCompanyTelephone nvarchar(50),
	@sCompanyEmail nvarchar(50),
	@sCompanyAddress1 nvarchar(100),
	@sCompanyAddress2 nvarchar(100),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(50),
	@sAdvertisingCost nvarchar(50),
	@dcAmountOfFee decimal(18, 2),
	@sPaymentTerms nvarchar(Max),
	@sBlackList nvarchar(50),
	@sRating nvarchar(50),
	@sMainBusiness nvarchar(Max),
	@sHowToUseMarketingChannel nvarchar(Max),
	@sComments nvarchar(Max),
	@sFirstDayOfCollabration nvarchar(250),
	@daContractExpireDate datetime,
	@sStatus nvarchar(50),
	@biSignedAgreedContractUpload varbinary(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingChannel]
SET 
	[CompanyID] = @iCompanyID,
	[ContractedBy] = @sContractedBy,
	[CompanyName] = @sCompanyName,
	[CompanyWebsite] = @sCompanyWebsite,
	[Username] = @sUsername,
	[Password] = @sPassword,
	[CompanyTelephone] = @sCompanyTelephone,
	[CompanyEmail] = @sCompanyEmail,
	[CompanyAddress1] = @sCompanyAddress1,
	[CompanyAddress2] = @sCompanyAddress2,
	[City] = @sCity,
	[Country] = @sCountry,
	[PostCode] = @sPostCode,
	[AdvertisingCost] = @sAdvertisingCost,
	[AmountOfFee] = @dcAmountOfFee,
	[PaymentTerms] = @sPaymentTerms,
	[BlackList] = @sBlackList,
	[Rating] = @sRating,
	[MainBusiness] = @sMainBusiness,
	[HowToUseMarketingChannel] = @sHowToUseMarketingChannel,
	[Comments] = @sComments,
	[FirstDayOfCollabration] = @sFirstDayOfCollabration,
	[ContractExpireDate] = @daContractExpireDate,
	[Status] = @sStatus,
	[SignedAgreedContractUpload] = @biSignedAgreedContractUpload,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'MarketingChannel'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingChannel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'MarketingChannel'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingChannel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'MarketingChannel'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingChannel]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     MarketingChannel]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  MarketingProject]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'MarketingProject'
-- Gets: @iCompanyID int
-- Gets: @sReferenceNo nvarchar(100)
-- Gets: @sProjectName nvarchar(250)
-- Gets: @sProjectDescribution nvarchar(Max)
-- Gets: @sInitiateBy nvarchar(50)
-- Gets: @sStaffInvolved nvarchar(50)
-- Gets: @daProjectStartingDate datetime
-- Gets: @daProjectPlannedEndingDate datetime
-- Gets: @sWorkingProcessComment nvarchar(Max)
-- Gets: @daProjectActualEndDate datetime
-- Gets: @dcProjectIncome decimal(18, 2)
-- Gets: @dcProjectExpense decimal(18, 2)
-- Gets: @dcProjectGrossProfit decimal(18, 2)
-- Gets: @sRating nvarchar(50)
-- Gets: @sProjectReport nvarchar(Max)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @dcTotalEstimateBudget decimal(18, 2)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Insert]
	@iCompanyID int,
	@sReferenceNo nvarchar(100),
	@sProjectName nvarchar(250),
	@sProjectDescribution nvarchar(Max),
	@sInitiateBy nvarchar(50),
	@sStaffInvolved nvarchar(50),
	@daProjectStartingDate datetime,
	@daProjectPlannedEndingDate datetime,
	@sWorkingProcessComment nvarchar(Max),
	@daProjectActualEndDate datetime,
	@dcProjectIncome decimal(18, 2),
	@dcProjectExpense decimal(18, 2),
	@dcProjectGrossProfit decimal(18, 2),
	@sRating nvarchar(50),
	@sProjectReport nvarchar(Max),
	@dcTotalBudget decimal(18, 2),
	@dcTotalEstimateBudget decimal(18, 2),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingProject]
(
	[CompanyID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[TotalBudget],
	[TotalEstimateBudget],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sReferenceNo,
	@sProjectName,
	@sProjectDescribution,
	@sInitiateBy,
	@sStaffInvolved,
	@daProjectStartingDate,
	@daProjectPlannedEndingDate,
	@sWorkingProcessComment,
	@daProjectActualEndDate,
	@dcProjectIncome,
	@dcProjectExpense,
	@dcProjectGrossProfit,
	@sRating,
	@sProjectReport,
	@dcTotalBudget,
	@dcTotalEstimateBudget,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'MarketingProject'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sReferenceNo nvarchar(100)
-- Gets: @sProjectName nvarchar(250)
-- Gets: @sProjectDescribution nvarchar(Max)
-- Gets: @sInitiateBy nvarchar(50)
-- Gets: @sStaffInvolved nvarchar(50)
-- Gets: @daProjectStartingDate datetime
-- Gets: @daProjectPlannedEndingDate datetime
-- Gets: @sWorkingProcessComment nvarchar(Max)
-- Gets: @daProjectActualEndDate datetime
-- Gets: @dcProjectIncome decimal(18, 2)
-- Gets: @dcProjectExpense decimal(18, 2)
-- Gets: @dcProjectGrossProfit decimal(18, 2)
-- Gets: @sRating nvarchar(50)
-- Gets: @sProjectReport nvarchar(Max)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @dcTotalEstimateBudget decimal(18, 2)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Update]
	@iID int,
	@iCompanyID int,
	@sReferenceNo nvarchar(100),
	@sProjectName nvarchar(250),
	@sProjectDescribution nvarchar(Max),
	@sInitiateBy nvarchar(50),
	@sStaffInvolved nvarchar(50),
	@daProjectStartingDate datetime,
	@daProjectPlannedEndingDate datetime,
	@sWorkingProcessComment nvarchar(Max),
	@daProjectActualEndDate datetime,
	@dcProjectIncome decimal(18, 2),
	@dcProjectExpense decimal(18, 2),
	@dcProjectGrossProfit decimal(18, 2),
	@sRating nvarchar(50),
	@sProjectReport nvarchar(Max),
	@dcTotalBudget decimal(18, 2),
	@dcTotalEstimateBudget decimal(18, 2),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingProject]
SET 
	[CompanyID] = @iCompanyID,
	[ReferenceNo] = @sReferenceNo,
	[ProjectName] = @sProjectName,
	[ProjectDescribution] = @sProjectDescribution,
	[InitiateBy] = @sInitiateBy,
	[StaffInvolved] = @sStaffInvolved,
	[ProjectStartingDate] = @daProjectStartingDate,
	[ProjectPlannedEndingDate] = @daProjectPlannedEndingDate,
	[WorkingProcessComment] = @sWorkingProcessComment,
	[ProjectActualEndDate] = @daProjectActualEndDate,
	[ProjectIncome] = @dcProjectIncome,
	[ProjectExpense] = @dcProjectExpense,
	[ProjectGrossProfit] = @dcProjectGrossProfit,
	[Rating] = @sRating,
	[ProjectReport] = @sProjectReport,
	[TotalBudget] = @dcTotalBudget,
	[TotalEstimateBudget] = @dcTotalEstimateBudget,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'MarketingProject'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingProject]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'MarketingProject'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[TotalBudget],
	[TotalEstimateBudget],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingProject]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'MarketingProject'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[TotalBudget],
	[TotalEstimateBudget],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingProject]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     MarketingProject]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  OpinionSheet]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'OpinionSheet'
-- Gets: @iCompanyID int
-- Gets: @daDate datetime
-- Gets: @sStaff nvarchar(250)
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Insert]
	@iCompanyID int,
	@daDate datetime,
	@sStaff nvarchar(250),
	@iOpinionSheetCategoryId int,
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OpinionSheet]
(
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@daDate,
	@sStaff,
	@iOpinionSheetCategoryId,
	@biFileData,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'OpinionSheet'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @daDate datetime
-- Gets: @sStaff nvarchar(250)
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Update]
	@iID int,
	@iCompanyID int,
	@daDate datetime,
	@sStaff nvarchar(250),
	@iOpinionSheetCategoryId int,
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheet]
SET 
	[CompanyID] = @iCompanyID,
	[Date] = @daDate,
	[Staff] = @sStaff,
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId,
	[FileData] = @biFileData,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [OpinionSheetCategoryId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'OpinionSheet'.
-- Will reset field [OpinionSheetCategoryId] with value @iOpinionSheetCategoryIdOld  to value @iOpinionSheetCategoryId
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @iOpinionSheetCategoryIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iOpinionSheetCategoryIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheet]
SET
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'OpinionSheet'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OpinionSheet]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [OpinionSheetCategoryId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'OpinionSheet'
-- based on a foreign key field.
-- Gets: @iOpinionSheetCategoryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[OpinionSheet]
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'OpinionSheet'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheet]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'OpinionSheet'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheet]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [OpinionSheetCategoryId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'OpinionSheet'
-- based on a foreign key field.
-- Gets: @iOpinionSheetCategoryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheet]
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     OpinionSheet]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  PriceTemplate]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_PriceTemplate_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_PriceTemplate_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'PriceTemplate'
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sLanguage nvarchar(50)
-- Gets: @sTitle nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_Insert]
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sLanguage nvarchar(50),
	@sTitle nvarchar(250),
	@sDescription nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[PriceTemplate]
(
	[CompanyID],
	[Staff],
	[Language],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sStaff,
	@sLanguage,
	@sTitle,
	@sDescription,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_PriceTemplate_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_PriceTemplate_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'PriceTemplate'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sLanguage nvarchar(50)
-- Gets: @sTitle nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_Update]
	@iID int,
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sLanguage nvarchar(50),
	@sTitle nvarchar(250),
	@sDescription nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[PriceTemplate]
SET 
	[CompanyID] = @iCompanyID,
	[Staff] = @sStaff,
	[Language] = @sLanguage,
	[Title] = @sTitle,
	[Description] = @sDescription,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_PriceTemplate_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_PriceTemplate_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'PriceTemplate'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[PriceTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_PriceTemplate_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_PriceTemplate_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'PriceTemplate'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Language],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[PriceTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_PriceTemplate_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_PriceTemplate_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'PriceTemplate'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Language],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[PriceTemplate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     PriceTemplate]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  Product]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'Product'
-- Gets: @iCompanyID int
-- Gets: @daCreationDate datetime
-- Gets: @sContractedBy nvarchar(250)
-- Gets: @sDataEntryBy nvarchar(250)
-- Gets: @sProductType nvarchar(250)
-- Gets: @iSupplierId int
-- Gets: @sSupplierName nvarchar(250)
-- Gets: @sContract nvarchar(250)
-- Gets: @sContractWith nvarchar(250)
-- Gets: @sEmergencyNo nvarchar(250)
-- Gets: @sReservationEmail nvarchar(250)
-- Gets: @sAlternationEmail nvarchar(250)
-- Gets: @sCurrency nvarchar(250)
-- Gets: @sContractType nvarchar(250)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sCountry nvarchar(250)
-- Gets: @sRegion nvarchar(250)
-- Gets: @sTypeOfProduct nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sProductTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @dcPrice decimal(18, 2)
-- Gets: @sComment nvarchar(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sPriceIncluding nvarchar(Max)
-- Gets: @sPriceExcluding nvarchar(Max)
-- Gets: @sContactTitle nvarchar(50)
-- Gets: @sContactSurName nvarchar(150)
-- Gets: @sContactFirstName nvarchar(510)
-- Gets: @sContactPosition nvarchar(50)
-- Gets: @sContactDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sContactDirectPhone nvarchar(50)
-- Gets: @sContactMobileNo nvarchar(50)
-- Gets: @sContactFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Insert]
	@iCompanyID int,
	@daCreationDate datetime,
	@sContractedBy nvarchar(250),
	@sDataEntryBy nvarchar(250),
	@sProductType nvarchar(250),
	@iSupplierId int,
	@sSupplierName nvarchar(250),
	@sContract nvarchar(250),
	@sContractWith nvarchar(250),
	@sEmergencyNo nvarchar(250),
	@sReservationEmail nvarchar(250),
	@sAlternationEmail nvarchar(250),
	@sCurrency nvarchar(250),
	@sContractType nvarchar(250),
	@dcNetRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sCountry nvarchar(250),
	@sRegion nvarchar(250),
	@sTypeOfProduct nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sProductTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@dcPrice decimal(18, 2),
	@sComment nvarchar(Max),
	@sHowToBook nvarchar(Max),
	@sPriceIncluding nvarchar(Max),
	@sPriceExcluding nvarchar(Max),
	@sContactTitle nvarchar(50),
	@sContactSurName nvarchar(150),
	@sContactFirstName nvarchar(510),
	@sContactPosition nvarchar(50),
	@sContactDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sContactDirectPhone nvarchar(50),
	@sContactMobileNo nvarchar(50),
	@sContactFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[Product]
(
	[CompanyID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[ProductTitle],
	[ItineraryTemplate],
	[Price],
	[Comment],
	[HowToBook],
	[PriceIncluding],
	[PriceExcluding],
	[ContactTitle],
	[ContactSurName],
	[ContactFirstName],
	[ContactPosition],
	[ContactDepartment],
	[ContactEmail],
	[ContactDirectPhone],
	[ContactMobileNo],
	[ContactFaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@daCreationDate,
	@sContractedBy,
	@sDataEntryBy,
	@sProductType,
	@iSupplierId,
	@sSupplierName,
	@sContract,
	@sContractWith,
	@sEmergencyNo,
	@sReservationEmail,
	@sAlternationEmail,
	@sCurrency,
	@sContractType,
	@dcNetRate,
	@dcCommission,
	@sCountry,
	@sRegion,
	@sTypeOfProduct,
	@iNoOfDays,
	@iNoOfNights,
	@sProductTitle,
	@sItineraryTemplate,
	@dcPrice,
	@sComment,
	@sHowToBook,
	@sPriceIncluding,
	@sPriceExcluding,
	@sContactTitle,
	@sContactSurName,
	@sContactFirstName,
	@sContactPosition,
	@sContactDepartment,
	@sContactEmail,
	@sContactDirectPhone,
	@sContactMobileNo,
	@sContactFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'Product'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @daCreationDate datetime
-- Gets: @sContractedBy nvarchar(250)
-- Gets: @sDataEntryBy nvarchar(250)
-- Gets: @sProductType nvarchar(250)
-- Gets: @iSupplierId int
-- Gets: @sSupplierName nvarchar(250)
-- Gets: @sContract nvarchar(250)
-- Gets: @sContractWith nvarchar(250)
-- Gets: @sEmergencyNo nvarchar(250)
-- Gets: @sReservationEmail nvarchar(250)
-- Gets: @sAlternationEmail nvarchar(250)
-- Gets: @sCurrency nvarchar(250)
-- Gets: @sContractType nvarchar(250)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sCountry nvarchar(250)
-- Gets: @sRegion nvarchar(250)
-- Gets: @sTypeOfProduct nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sProductTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @dcPrice decimal(18, 2)
-- Gets: @sComment nvarchar(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sPriceIncluding nvarchar(Max)
-- Gets: @sPriceExcluding nvarchar(Max)
-- Gets: @sContactTitle nvarchar(50)
-- Gets: @sContactSurName nvarchar(150)
-- Gets: @sContactFirstName nvarchar(510)
-- Gets: @sContactPosition nvarchar(50)
-- Gets: @sContactDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sContactDirectPhone nvarchar(50)
-- Gets: @sContactMobileNo nvarchar(50)
-- Gets: @sContactFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Update]
	@iID int,
	@iCompanyID int,
	@daCreationDate datetime,
	@sContractedBy nvarchar(250),
	@sDataEntryBy nvarchar(250),
	@sProductType nvarchar(250),
	@iSupplierId int,
	@sSupplierName nvarchar(250),
	@sContract nvarchar(250),
	@sContractWith nvarchar(250),
	@sEmergencyNo nvarchar(250),
	@sReservationEmail nvarchar(250),
	@sAlternationEmail nvarchar(250),
	@sCurrency nvarchar(250),
	@sContractType nvarchar(250),
	@dcNetRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sCountry nvarchar(250),
	@sRegion nvarchar(250),
	@sTypeOfProduct nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sProductTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@dcPrice decimal(18, 2),
	@sComment nvarchar(Max),
	@sHowToBook nvarchar(Max),
	@sPriceIncluding nvarchar(Max),
	@sPriceExcluding nvarchar(Max),
	@sContactTitle nvarchar(50),
	@sContactSurName nvarchar(150),
	@sContactFirstName nvarchar(510),
	@sContactPosition nvarchar(50),
	@sContactDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sContactDirectPhone nvarchar(50),
	@sContactMobileNo nvarchar(50),
	@sContactFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Product]
SET 
	[CompanyID] = @iCompanyID,
	[CreationDate] = @daCreationDate,
	[ContractedBy] = @sContractedBy,
	[DataEntryBy] = @sDataEntryBy,
	[ProductType] = @sProductType,
	[SupplierId] = @iSupplierId,
	[SupplierName] = @sSupplierName,
	[Contract] = @sContract,
	[ContractWith] = @sContractWith,
	[EmergencyNo] = @sEmergencyNo,
	[ReservationEmail] = @sReservationEmail,
	[AlternationEmail] = @sAlternationEmail,
	[Currency] = @sCurrency,
	[ContractType] = @sContractType,
	[NetRate] = @dcNetRate,
	[Commission] = @dcCommission,
	[Country] = @sCountry,
	[Region] = @sRegion,
	[TypeOfProduct] = @sTypeOfProduct,
	[NoOfDays] = @iNoOfDays,
	[NoOfNights] = @iNoOfNights,
	[ProductTitle] = @sProductTitle,
	[ItineraryTemplate] = @sItineraryTemplate,
	[Price] = @dcPrice,
	[Comment] = @sComment,
	[HowToBook] = @sHowToBook,
	[PriceIncluding] = @sPriceIncluding,
	[PriceExcluding] = @sPriceExcluding,
	[ContactTitle] = @sContactTitle,
	[ContactSurName] = @sContactSurName,
	[ContactFirstName] = @sContactFirstName,
	[ContactPosition] = @sContactPosition,
	[ContactDepartment] = @sContactDepartment,
	[ContactEmail] = @sContactEmail,
	[ContactDirectPhone] = @sContactDirectPhone,
	[ContactMobileNo] = @sContactMobileNo,
	[ContactFaxNo] = @sContactFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'Product'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[Product]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'Product'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[ProductTitle],
	[ItineraryTemplate],
	[Price],
	[Comment],
	[HowToBook],
	[PriceIncluding],
	[PriceExcluding],
	[ContactTitle],
	[ContactSurName],
	[ContactFirstName],
	[ContactPosition],
	[ContactDepartment],
	[ContactEmail],
	[ContactDirectPhone],
	[ContactMobileNo],
	[ContactFaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'Product'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[ProductTitle],
	[ItineraryTemplate],
	[Price],
	[Comment],
	[HowToBook],
	[PriceIncluding],
	[PriceExcluding],
	[ContactTitle],
	[ContactSurName],
	[ContactFirstName],
	[ContactPosition],
	[ContactDepartment],
	[ContactEmail],
	[ContactDirectPhone],
	[ContactMobileNo],
	[ContactFaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     Product]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SupplierDMCRate]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SupplierDMCRate'
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(250)
-- Gets: @biFileData varbinary(Max)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcGrossRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sRemarks nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_Insert]
	@iSupplierDetailID int,
	@sTitle nvarchar(250),
	@biFileData varbinary(Max),
	@dcNetRate decimal(18, 2),
	@dcGrossRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sRemarks nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierDMCRate]
(
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sTitle,
	@biFileData,
	@dcNetRate,
	@dcGrossRate,
	@dcCommission,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SupplierDMCRate'
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(250)
-- Gets: @biFileData varbinary(Max)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcGrossRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sRemarks nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@sTitle nvarchar(250),
	@biFileData varbinary(Max),
	@dcNetRate decimal(18, 2),
	@dcGrossRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sRemarks nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierDMCRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[Title] = @sTitle,
	[FileData] = @biFileData,
	[NetRate] = @dcNetRate,
	[GrossRate] = @dcGrossRate,
	[Commission] = @dcCommission,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SupplierDMCRate'.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierDMCRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SupplierDMCRate'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierDMCRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SupplierDMCRate'
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierDMCRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SupplierDMCRate'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierDMCRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SupplierDMCRate'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierDMCRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SupplierDMCRate'
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierDMCRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SupplierDMCRate]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SupplierEntranceTicketFit]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketFit_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketFit_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SupplierEntranceTicketFit'
-- Gets: @iNoOfPeople int
-- Gets: @dcAdult decimal(18, 2)
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Insert]
	@iNoOfPeople int,
	@dcAdult decimal(18, 2),
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierEntranceTicketFit]
(
	[NoOfPeople],
	[Adult]
)
VALUES
(
	@iNoOfPeople,
	@dcAdult
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketFit_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketFit_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SupplierEntranceTicketFit'
-- Gets: @iID int
-- Gets: @iNoOfPeople int
-- Gets: @dcAdult decimal(18, 2)
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Update]
	@iID int,
	@iNoOfPeople int,
	@dcAdult decimal(18, 2),
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierEntranceTicketFit]
SET 
	[NoOfPeople] = @iNoOfPeople,
	[Adult] = @dcAdult
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketFit_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketFit_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SupplierEntranceTicketFit'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierEntranceTicketFit]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketFit_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketFit_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SupplierEntranceTicketFit'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[NoOfPeople],
	[Adult]
FROM [dbo].[SupplierEntranceTicketFit]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketFit_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketFit_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SupplierEntranceTicketFit'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[NoOfPeople],
	[Adult]
FROM [dbo].[SupplierEntranceTicketFit]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SupplierEntranceTicketFit]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SupplierEntranceTicketRate]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SupplierEntranceTicketRate'
-- Gets: @iSupplierDetailID int
-- Gets: @daMonthFrom datetime
-- Gets: @daMonthTo datetime
-- Gets: @daDailyOpeningTime datetime
-- Gets: @daDailyCloseTime datetime
-- Gets: @sLastAdmission nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Insert]
	@iSupplierDetailID int,
	@daMonthFrom datetime,
	@daMonthTo datetime,
	@daDailyOpeningTime datetime,
	@daDailyCloseTime datetime,
	@sLastAdmission nvarchar(250),
	@sStatus nvarchar(50),
	@sRemark nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierEntranceTicketRate]
(
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daMonthFrom,
	@daMonthTo,
	@daDailyOpeningTime,
	@daDailyCloseTime,
	@sLastAdmission,
	@sStatus,
	@sRemark,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SupplierEntranceTicketRate'
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daMonthFrom datetime
-- Gets: @daMonthTo datetime
-- Gets: @daDailyOpeningTime datetime
-- Gets: @daDailyCloseTime datetime
-- Gets: @sLastAdmission nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daMonthFrom datetime,
	@daMonthTo datetime,
	@daDailyOpeningTime datetime,
	@daDailyCloseTime datetime,
	@sLastAdmission nvarchar(250),
	@sStatus nvarchar(50),
	@sRemark nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierEntranceTicketRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[MonthFrom] = @daMonthFrom,
	[MonthTo] = @daMonthTo,
	[DailyOpeningTime] = @daDailyOpeningTime,
	[DailyCloseTime] = @daDailyCloseTime,
	[LastAdmission] = @sLastAdmission,
	[Status] = @sStatus,
	[Remark] = @sRemark,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SupplierEntranceTicketRate'.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierEntranceTicketRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SupplierEntranceTicketRate'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SupplierEntranceTicketRate'
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SupplierEntranceTicketRate'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SupplierEntranceTicketRate'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierEntranceTicketRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SupplierEntranceTicketRate'
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SupplierEntranceTicketRate]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SupplierRestaurantRate]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SupplierRestaurantRate'
-- Gets: @iSupplierDetailID int
-- Gets: @sTypeOfFood nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @dcRate decimal(18, 2)
-- Gets: @sSampleOfMenu nvarchar(Max)
-- Gets: @biSampleMenuFile varbinary(Max)
-- Gets: @biFoodPictureFile varbinary(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_Insert]
	@iSupplierDetailID int,
	@sTypeOfFood nvarchar(250),
	@sDescription nvarchar(Max),
	@dcRate decimal(18, 2),
	@sSampleOfMenu nvarchar(Max),
	@biSampleMenuFile varbinary(Max),
	@biFoodPictureFile varbinary(Max),
	@sComments nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierRestaurantRate]
(
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sTypeOfFood,
	@sDescription,
	@dcRate,
	@sSampleOfMenu,
	@biSampleMenuFile,
	@biFoodPictureFile,
	@sComments,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SupplierRestaurantRate'
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sTypeOfFood nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @dcRate decimal(18, 2)
-- Gets: @sSampleOfMenu nvarchar(Max)
-- Gets: @biSampleMenuFile varbinary(Max)
-- Gets: @biFoodPictureFile varbinary(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@sTypeOfFood nvarchar(250),
	@sDescription nvarchar(Max),
	@dcRate decimal(18, 2),
	@sSampleOfMenu nvarchar(Max),
	@biSampleMenuFile varbinary(Max),
	@biFoodPictureFile varbinary(Max),
	@sComments nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierRestaurantRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[TypeOfFood] = @sTypeOfFood,
	[Description] = @sDescription,
	[Rate] = @dcRate,
	[SampleOfMenu] = @sSampleOfMenu,
	[SampleMenuFile] = @biSampleMenuFile,
	[FoodPictureFile] = @biFoodPictureFile,
	[Comments] = @sComments,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SupplierRestaurantRate'.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierRestaurantRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SupplierRestaurantRate'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierRestaurantRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SupplierRestaurantRate'
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierRestaurantRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SupplierRestaurantRate'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierRestaurantRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SupplierRestaurantRate'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierRestaurantRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [SupplierDetailID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SupplierRestaurantRate'
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierRestaurantRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SupplierRestaurantRate]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SuppliersDetails]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SuppliersDetails'
-- Gets: @iCompanyID int
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierTypeID int
-- Gets: @daSupplierDate datetime
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sName nvarchar(50)
-- Gets: @sStarRating nvarchar(10)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sAgency nvarchar(100)
-- Gets: @sClientCode nvarchar(50)
-- Gets: @sUserName nvarchar(50)
-- Gets: @sPassword nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFaxNumber nvarchar(15)
-- Gets: @sEmergencyNo nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sHotelChain nvarchar(50)
-- Gets: @sFranchised nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @daContractStartDate datetime
-- Gets: @daContractEndingDate datetime
-- Gets: @daContractRenewalDate datetime
-- Gets: @sRating nvarchar(10)
-- Gets: @sMainBusiness nvarchar(200)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biFileData varbinary(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Insert]
	@iCompanyID int,
	@iSupplierMasterID int,
	@iSupplierTypeID int,
	@daSupplierDate datetime,
	@sContractedBy nvarchar(50),
	@sName nvarchar(50),
	@sStarRating nvarchar(10),
	@sWebsite nvarchar(150),
	@sAgency nvarchar(100),
	@sClientCode nvarchar(50),
	@sUserName nvarchar(50),
	@sPassword nvarchar(50),
	@sTelephone nvarchar(15),
	@sFaxNumber nvarchar(15),
	@sEmergencyNo nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sHotelChain nvarchar(50),
	@sFranchised nvarchar(50),
	@sPaymentTerms nvarchar(Max),
	@sCreditToLetsTravel nvarchar(50),
	@sBlackList nvarchar(200),
	@daContractStartDate datetime,
	@daContractEndingDate datetime,
	@daContractRenewalDate datetime,
	@sRating nvarchar(10),
	@sMainBusiness nvarchar(200),
	@sComments nvarchar(Max),
	@biFileData varbinary(Max),
	@sHowToBook nvarchar(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SuppliersDetails]
(
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierMasterID,
	@iSupplierTypeID,
	@daSupplierDate,
	@sContractedBy,
	@sName,
	@sStarRating,
	@sWebsite,
	@sAgency,
	@sClientCode,
	@sUserName,
	@sPassword,
	@sTelephone,
	@sFaxNumber,
	@sEmergencyNo,
	@sEmail,
	@sAlternateEmail,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sPostalCode,
	@sHotelChain,
	@sFranchised,
	@sPaymentTerms,
	@sCreditToLetsTravel,
	@sBlackList,
	@daContractStartDate,
	@daContractEndingDate,
	@daContractRenewalDate,
	@sRating,
	@sMainBusiness,
	@sComments,
	@biFileData,
	@sHowToBook,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sPosition,
	@sDepartment,
	@sContactEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SuppliersDetails'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierTypeID int
-- Gets: @daSupplierDate datetime
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sName nvarchar(50)
-- Gets: @sStarRating nvarchar(10)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sAgency nvarchar(100)
-- Gets: @sClientCode nvarchar(50)
-- Gets: @sUserName nvarchar(50)
-- Gets: @sPassword nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFaxNumber nvarchar(15)
-- Gets: @sEmergencyNo nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sHotelChain nvarchar(50)
-- Gets: @sFranchised nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @daContractStartDate datetime
-- Gets: @daContractEndingDate datetime
-- Gets: @daContractRenewalDate datetime
-- Gets: @sRating nvarchar(10)
-- Gets: @sMainBusiness nvarchar(200)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biFileData varbinary(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierMasterID int,
	@iSupplierTypeID int,
	@daSupplierDate datetime,
	@sContractedBy nvarchar(50),
	@sName nvarchar(50),
	@sStarRating nvarchar(10),
	@sWebsite nvarchar(150),
	@sAgency nvarchar(100),
	@sClientCode nvarchar(50),
	@sUserName nvarchar(50),
	@sPassword nvarchar(50),
	@sTelephone nvarchar(15),
	@sFaxNumber nvarchar(15),
	@sEmergencyNo nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sHotelChain nvarchar(50),
	@sFranchised nvarchar(50),
	@sPaymentTerms nvarchar(Max),
	@sCreditToLetsTravel nvarchar(50),
	@sBlackList nvarchar(200),
	@daContractStartDate datetime,
	@daContractEndingDate datetime,
	@daContractRenewalDate datetime,
	@sRating nvarchar(10),
	@sMainBusiness nvarchar(200),
	@sComments nvarchar(Max),
	@biFileData varbinary(Max),
	@sHowToBook nvarchar(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierMasterID] = @iSupplierMasterID,
	[SupplierTypeID] = @iSupplierTypeID,
	[SupplierDate] = @daSupplierDate,
	[ContractedBy] = @sContractedBy,
	[Name] = @sName,
	[StarRating] = @sStarRating,
	[Website] = @sWebsite,
	[Agency] = @sAgency,
	[ClientCode] = @sClientCode,
	[UserName] = @sUserName,
	[Password] = @sPassword,
	[Telephone] = @sTelephone,
	[FaxNumber] = @sFaxNumber,
	[EmergencyNo] = @sEmergencyNo,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[PostalCode] = @sPostalCode,
	[HotelChain] = @sHotelChain,
	[Franchised] = @sFranchised,
	[PaymentTerms] = @sPaymentTerms,
	[CreditToLetsTravel] = @sCreditToLetsTravel,
	[BlackList] = @sBlackList,
	[ContractStartDate] = @daContractStartDate,
	[ContractEndingDate] = @daContractEndingDate,
	[ContractRenewalDate] = @daContractRenewalDate,
	[Rating] = @sRating,
	[MainBusiness] = @sMainBusiness,
	[Comments] = @sComments,
	[FileData] = @biFileData,
	[HowToBook] = @sHowToBook,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[ContactEmail] = @sContactEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [SupplierMasterID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SuppliersDetails'.
-- Will reset field [SupplierMasterID] with value @iSupplierMasterIDOld  to value @iSupplierMasterID
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierMasterIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iSupplierMasterIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET
	[SupplierMasterID] = @iSupplierMasterID
WHERE
	[SupplierMasterID] = @iSupplierMasterIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [SupplierTypeID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SuppliersDetails'.
-- Will reset field [SupplierTypeID] with value @iSupplierTypeIDOld  to value @iSupplierTypeID
-- Gets: @iSupplierTypeID int
-- Gets: @iSupplierTypeIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iSupplierTypeIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET
	[SupplierTypeID] = @iSupplierTypeID
WHERE
	[SupplierTypeID] = @iSupplierTypeIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SuppliersDetails'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SuppliersDetails]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [SupplierMasterID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SuppliersDetails'
-- based on a foreign key field.
-- Gets: @iSupplierMasterID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierMasterID] = @iSupplierMasterID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [SupplierTypeID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SuppliersDetails'
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SuppliersDetails'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SuppliersDetails'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [SupplierMasterID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SuppliersDetails'
-- based on a foreign key field.
-- Gets: @iSupplierMasterID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierMasterID] = @iSupplierMasterID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [SupplierTypeID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SuppliersDetails'
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SuppliersDetails]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SupplierTipsDetail]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SupplierTipsDetail'
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @dcTips decimal(18, 2)
-- Gets: @sCountry nvarchar(50)
-- Gets: @iCountryId int
-- Gets: @dcAirportPickUp decimal(18, 2)
-- Gets: @dcAirportDropOff decimal(18, 2)
-- Gets: @dcWholeDay decimal(18, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_Insert]
	@iCompanyID int,
	@iSupplierTypeId int,
	@dcTips decimal(18, 2),
	@sCountry nvarchar(50),
	@iCountryId int,
	@dcAirportPickUp decimal(18, 2),
	@dcAirportDropOff decimal(18, 2),
	@dcWholeDay decimal(18, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTipsDetail]
(
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierTypeId,
	@dcTips,
	@sCountry,
	@iCountryId,
	@dcAirportPickUp,
	@dcAirportDropOff,
	@dcWholeDay,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SupplierTipsDetail'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @dcTips decimal(18, 2)
-- Gets: @sCountry nvarchar(50)
-- Gets: @iCountryId int
-- Gets: @dcAirportPickUp decimal(18, 2)
-- Gets: @dcAirportDropOff decimal(18, 2)
-- Gets: @dcWholeDay decimal(18, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierTypeId int,
	@dcTips decimal(18, 2),
	@sCountry nvarchar(50),
	@iCountryId int,
	@dcAirportPickUp decimal(18, 2),
	@dcAirportDropOff decimal(18, 2),
	@dcWholeDay decimal(18, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTipsDetail]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierTypeId] = @iSupplierTypeId,
	[Tips] = @dcTips,
	[Country] = @sCountry,
	[CountryId] = @iCountryId,
	[AirportPickUp] = @dcAirportPickUp,
	[AirportDropOff] = @dcAirportDropOff,
	[WholeDay] = @dcWholeDay,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [SupplierTypeId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SupplierTipsDetail'.
-- Will reset field [SupplierTypeId] with value @iSupplierTypeIdOld  to value @iSupplierTypeId
-- Gets: @iSupplierTypeId int
-- Gets: @iSupplierTypeIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]
	@iSupplierTypeId int,
	@iSupplierTypeIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTipsDetail]
SET
	[SupplierTypeId] = @iSupplierTypeId
WHERE
	[SupplierTypeId] = @iSupplierTypeIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SupplierTipsDetail'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTipsDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [SupplierTypeId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SupplierTipsDetail'
-- based on a foreign key field.
-- Gets: @iSupplierTypeId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]
	@iSupplierTypeId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTipsDetail]
WHERE
	[SupplierTypeId] = @iSupplierTypeId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SupplierTipsDetail'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTipsDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SupplierTipsDetail'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTipsDetail]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [SupplierTypeId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SupplierTipsDetail'
-- based on a foreign key field.
-- Gets: @iSupplierTypeId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]
	@iSupplierTypeId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTipsDetail]
WHERE
	[SupplierTypeId] = @iSupplierTypeId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SupplierTipsDetail]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SupplierTourGuide]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SupplierTourGuide'
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @iTypeOfSupplier int
-- Gets: @sRecruitedBy nvarchar(250)
-- Gets: @sSurName nvarchar(250)
-- Gets: @sSurNameInChinese nvarchar(250)
-- Gets: @sFirstName nvarchar(250)
-- Gets: @sFirstNameInChinese nvarchar(250)
-- Gets: @daDateOfBirth datetime
-- Gets: @sOrigin nvarchar(50)
-- Gets: @sNationality nvarchar(50)
-- Gets: @sMarried nvarchar(50)
-- Gets: @iChildren int
-- Gets: @sVisaStatus nvarchar(250)
-- Gets: @sHomePhone nvarchar(15)
-- Gets: @sMobilePhone nvarchar(15)
-- Gets: @sWeChat nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(500)
-- Gets: @sAddress2 nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(10)
-- Gets: @sSpeakingLanguage nvarchar(1000)
-- Gets: @iNoOfYearTourGuide int
-- Gets: @sAreaOfWorking nvarchar(250)
-- Gets: @sTypeOfWork nvarchar(250)
-- Gets: @sVehicleOwned nvarchar(250)
-- Gets: @sKindOfGroupWork nvarchar(Max)
-- Gets: @sKindOfGroupNotWork nvarchar(150)
-- Gets: @sSpecialRequirement nvarchar(Max)
-- Gets: @iRating int
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biDrivingLicence varbinary(Max)
-- Gets: @biVehiclePhoto varbinary(Max)
-- Gets: @biPassportCopy varbinary(Max)
-- Gets: @biAddressProof varbinary(Max)
-- Gets: @biGuideLicence varbinary(Max)
-- Gets: @biCVOfGuide varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Insert]
	@iCompanyID int,
	@iSupplierTypeId int,
	@iTypeOfSupplier int,
	@sRecruitedBy nvarchar(250),
	@sSurName nvarchar(250),
	@sSurNameInChinese nvarchar(250),
	@sFirstName nvarchar(250),
	@sFirstNameInChinese nvarchar(250),
	@daDateOfBirth datetime,
	@sOrigin nvarchar(50),
	@sNationality nvarchar(50),
	@sMarried nvarchar(50),
	@iChildren int,
	@sVisaStatus nvarchar(250),
	@sHomePhone nvarchar(15),
	@sMobilePhone nvarchar(15),
	@sWeChat nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(500),
	@sAddress2 nvarchar(500),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(10),
	@sSpeakingLanguage nvarchar(1000),
	@iNoOfYearTourGuide int,
	@sAreaOfWorking nvarchar(250),
	@sTypeOfWork nvarchar(250),
	@sVehicleOwned nvarchar(250),
	@sKindOfGroupWork nvarchar(Max),
	@sKindOfGroupNotWork nvarchar(150),
	@sSpecialRequirement nvarchar(Max),
	@iRating int,
	@sBlackList nvarchar(50),
	@sComments nvarchar(Max),
	@biDrivingLicence varbinary(Max),
	@biVehiclePhoto varbinary(Max),
	@biPassportCopy varbinary(Max),
	@biAddressProof varbinary(Max),
	@biGuideLicence varbinary(Max),
	@biCVOfGuide varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTourGuide]
(
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierTypeId,
	@iTypeOfSupplier,
	@sRecruitedBy,
	@sSurName,
	@sSurNameInChinese,
	@sFirstName,
	@sFirstNameInChinese,
	@daDateOfBirth,
	@sOrigin,
	@sNationality,
	@sMarried,
	@iChildren,
	@sVisaStatus,
	@sHomePhone,
	@sMobilePhone,
	@sWeChat,
	@sEmail,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sCountry,
	@sPostCode,
	@sSpeakingLanguage,
	@iNoOfYearTourGuide,
	@sAreaOfWorking,
	@sTypeOfWork,
	@sVehicleOwned,
	@sKindOfGroupWork,
	@sKindOfGroupNotWork,
	@sSpecialRequirement,
	@iRating,
	@sBlackList,
	@sComments,
	@biDrivingLicence,
	@biVehiclePhoto,
	@biPassportCopy,
	@biAddressProof,
	@biGuideLicence,
	@biCVOfGuide,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SupplierTourGuide'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @iTypeOfSupplier int
-- Gets: @sRecruitedBy nvarchar(250)
-- Gets: @sSurName nvarchar(250)
-- Gets: @sSurNameInChinese nvarchar(250)
-- Gets: @sFirstName nvarchar(250)
-- Gets: @sFirstNameInChinese nvarchar(250)
-- Gets: @daDateOfBirth datetime
-- Gets: @sOrigin nvarchar(50)
-- Gets: @sNationality nvarchar(50)
-- Gets: @sMarried nvarchar(50)
-- Gets: @iChildren int
-- Gets: @sVisaStatus nvarchar(250)
-- Gets: @sHomePhone nvarchar(15)
-- Gets: @sMobilePhone nvarchar(15)
-- Gets: @sWeChat nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(500)
-- Gets: @sAddress2 nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(10)
-- Gets: @sSpeakingLanguage nvarchar(1000)
-- Gets: @iNoOfYearTourGuide int
-- Gets: @sAreaOfWorking nvarchar(250)
-- Gets: @sTypeOfWork nvarchar(250)
-- Gets: @sVehicleOwned nvarchar(250)
-- Gets: @sKindOfGroupWork nvarchar(Max)
-- Gets: @sKindOfGroupNotWork nvarchar(150)
-- Gets: @sSpecialRequirement nvarchar(Max)
-- Gets: @iRating int
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biDrivingLicence varbinary(Max)
-- Gets: @biVehiclePhoto varbinary(Max)
-- Gets: @biPassportCopy varbinary(Max)
-- Gets: @biAddressProof varbinary(Max)
-- Gets: @biGuideLicence varbinary(Max)
-- Gets: @biCVOfGuide varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierTypeId int,
	@iTypeOfSupplier int,
	@sRecruitedBy nvarchar(250),
	@sSurName nvarchar(250),
	@sSurNameInChinese nvarchar(250),
	@sFirstName nvarchar(250),
	@sFirstNameInChinese nvarchar(250),
	@daDateOfBirth datetime,
	@sOrigin nvarchar(50),
	@sNationality nvarchar(50),
	@sMarried nvarchar(50),
	@iChildren int,
	@sVisaStatus nvarchar(250),
	@sHomePhone nvarchar(15),
	@sMobilePhone nvarchar(15),
	@sWeChat nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(500),
	@sAddress2 nvarchar(500),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(10),
	@sSpeakingLanguage nvarchar(1000),
	@iNoOfYearTourGuide int,
	@sAreaOfWorking nvarchar(250),
	@sTypeOfWork nvarchar(250),
	@sVehicleOwned nvarchar(250),
	@sKindOfGroupWork nvarchar(Max),
	@sKindOfGroupNotWork nvarchar(150),
	@sSpecialRequirement nvarchar(Max),
	@iRating int,
	@sBlackList nvarchar(50),
	@sComments nvarchar(Max),
	@biDrivingLicence varbinary(Max),
	@biVehiclePhoto varbinary(Max),
	@biPassportCopy varbinary(Max),
	@biAddressProof varbinary(Max),
	@biGuideLicence varbinary(Max),
	@biCVOfGuide varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuide]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierTypeId] = @iSupplierTypeId,
	[TypeOfSupplier] = @iTypeOfSupplier,
	[RecruitedBy] = @sRecruitedBy,
	[SurName] = @sSurName,
	[SurNameInChinese] = @sSurNameInChinese,
	[FirstName] = @sFirstName,
	[FirstNameInChinese] = @sFirstNameInChinese,
	[DateOfBirth] = @daDateOfBirth,
	[Origin] = @sOrigin,
	[Nationality] = @sNationality,
	[Married] = @sMarried,
	[Children] = @iChildren,
	[VisaStatus] = @sVisaStatus,
	[HomePhone] = @sHomePhone,
	[MobilePhone] = @sMobilePhone,
	[WeChat] = @sWeChat,
	[Email] = @sEmail,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[Country] = @sCountry,
	[PostCode] = @sPostCode,
	[SpeakingLanguage] = @sSpeakingLanguage,
	[NoOfYearTourGuide] = @iNoOfYearTourGuide,
	[AreaOfWorking] = @sAreaOfWorking,
	[TypeOfWork] = @sTypeOfWork,
	[VehicleOwned] = @sVehicleOwned,
	[KindOfGroupWork] = @sKindOfGroupWork,
	[KindOfGroupNotWork] = @sKindOfGroupNotWork,
	[SpecialRequirement] = @sSpecialRequirement,
	[Rating] = @iRating,
	[BlackList] = @sBlackList,
	[Comments] = @sComments,
	[DrivingLicence] = @biDrivingLicence,
	[VehiclePhoto] = @biVehiclePhoto,
	[PassportCopy] = @biPassportCopy,
	[AddressProof] = @biAddressProof,
	[GuideLicence] = @biGuideLicence,
	[CVOfGuide] = @biCVOfGuide,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [TypeOfSupplier].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SupplierTourGuide'.
-- Will reset field [TypeOfSupplier] with value @iTypeOfSupplierOld  to value @iTypeOfSupplier
-- Gets: @iTypeOfSupplier int
-- Gets: @iTypeOfSupplierOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iTypeOfSupplierOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuide]
SET
	[TypeOfSupplier] = @iTypeOfSupplier
WHERE
	[TypeOfSupplier] = @iTypeOfSupplierOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SupplierTourGuide'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [TypeOfSupplier].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SupplierTourGuide'
-- based on a foreign key field.
-- Gets: @iTypeOfSupplier int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTourGuide]
WHERE
	[TypeOfSupplier] = @iTypeOfSupplier
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SupplierTourGuide'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SupplierTourGuide'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [TypeOfSupplier].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SupplierTourGuide'
-- based on a foreign key field.
-- Gets: @iTypeOfSupplier int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
WHERE
	[TypeOfSupplier] = @iTypeOfSupplier
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SupplierTourGuide]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SupplierTourGuideRate]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SupplierTourGuideRate'
-- Gets: @iSupplierTourGuideID int
-- Gets: @sSeason nvarchar(150)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_coach_Guide decimal(10, 2)
-- Gets: @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_tips decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_Day_coach_Guide decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2)
-- Gets: @dcFull_day_tips decimal(10, 2)
-- Gets: @dcExtra_hour_With_Vehicle decimal(10, 2)
-- Gets: @dcExtra_hour_Without_vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_With_Vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_Without_Vehicle decimal(10, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_Insert]
	@iSupplierTourGuideID int,
	@sSeason nvarchar(150),
	@daFromDate datetime,
	@daToDate datetime,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_coach_Guide decimal(10, 2),
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_tips decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_Day_coach_Guide decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2),
	@dcFull_day_tips decimal(10, 2),
	@dcExtra_hour_With_Vehicle decimal(10, 2),
	@dcExtra_hour_Without_vehicle decimal(10, 2),
	@dcLondon_Edinburgh_With_Vehicle decimal(10, 2),
	@dcLondon_Edinburgh_Without_Vehicle decimal(10, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTourGuideRate]
(
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierTourGuideID,
	@sSeason,
	@daFromDate,
	@daToDate,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle,
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle,
	@dcHalf_day_coach_Guide,
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle,
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle,
	@dcHalf_day_tips,
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle,
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle,
	@dcFull_Day_coach_Guide,
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle,
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle,
	@dcFull_day_tips,
	@dcExtra_hour_With_Vehicle,
	@dcExtra_hour_Without_vehicle,
	@dcLondon_Edinburgh_With_Vehicle,
	@dcLondon_Edinburgh_Without_Vehicle,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SupplierTourGuideRate'
-- Gets: @iID int
-- Gets: @iSupplierTourGuideID int
-- Gets: @sSeason nvarchar(150)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_coach_Guide decimal(10, 2)
-- Gets: @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_tips decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_Day_coach_Guide decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2)
-- Gets: @dcFull_day_tips decimal(10, 2)
-- Gets: @dcExtra_hour_With_Vehicle decimal(10, 2)
-- Gets: @dcExtra_hour_Without_vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_With_Vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_Without_Vehicle decimal(10, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_Update]
	@iID int,
	@iSupplierTourGuideID int,
	@sSeason nvarchar(150),
	@daFromDate datetime,
	@daToDate datetime,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_coach_Guide decimal(10, 2),
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_tips decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_Day_coach_Guide decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2),
	@dcFull_day_tips decimal(10, 2),
	@dcExtra_hour_With_Vehicle decimal(10, 2),
	@dcExtra_hour_Without_vehicle decimal(10, 2),
	@dcLondon_Edinburgh_With_Vehicle decimal(10, 2),
	@dcLondon_Edinburgh_Without_Vehicle decimal(10, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuideRate]
SET 
	[SupplierTourGuideID] = @iSupplierTourGuideID,
	[Season] = @sSeason,
	[FromDate] = @daFromDate,
	[ToDate] = @daToDate,
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle] = @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle,
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle] = @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle,
	[Half_day_coach_Guide] = @dcHalf_day_coach_Guide,
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle] = @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle,
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle] = @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle,
	[Half_day_tips] = @dcHalf_day_tips,
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle] = @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle,
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle] = @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle,
	[Full_Day_coach_Guide] = @dcFull_Day_coach_Guide,
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle] = @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle,
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle] = @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle,
	[Full_day_tips] = @dcFull_day_tips,
	[Extra_hour_With_Vehicle] = @dcExtra_hour_With_Vehicle,
	[Extra_hour_Without_vehicle] = @dcExtra_hour_Without_vehicle,
	[London_Edinburgh_With_Vehicle] = @dcLondon_Edinburgh_With_Vehicle,
	[London_Edinburgh_Without_Vehicle] = @dcLondon_Edinburgh_Without_Vehicle,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [SupplierTourGuideID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'SupplierTourGuideRate'.
-- Will reset field [SupplierTourGuideID] with value @iSupplierTourGuideIDOld  to value @iSupplierTourGuideID
-- Gets: @iSupplierTourGuideID int
-- Gets: @iSupplierTourGuideIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iSupplierTourGuideIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuideRate]
SET
	[SupplierTourGuideID] = @iSupplierTourGuideID
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SupplierTourGuideRate'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTourGuideRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [SupplierTourGuideID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'SupplierTourGuideRate'
-- based on a foreign key field.
-- Gets: @iSupplierTourGuideID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SupplierTourGuideRate'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SupplierTourGuideRate'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [SupplierTourGuideID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'SupplierTourGuideRate'
-- based on a foreign key field.
-- Gets: @iSupplierTourGuideID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SupplierTourGuideRate]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  TaskList]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_TaskList_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_TaskList_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'TaskList'
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sAssignedTo nvarchar(150)
-- Gets: @sTaskStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daDueDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_Insert]
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sAssignedTo nvarchar(150),
	@sTaskStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daDueDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[TaskList]
(
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sSubject,
	@sAssignedTo,
	@sTaskStatus,
	@sDescription,
	@daDueDate,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_TaskList_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_TaskList_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'TaskList'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sAssignedTo nvarchar(150)
-- Gets: @sTaskStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daDueDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_Update]
	@iID int,
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sAssignedTo nvarchar(150),
	@sTaskStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daDueDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[TaskList]
SET 
	[CompanyID] = @iCompanyID,
	[Subject] = @sSubject,
	[AssignedTo] = @sAssignedTo,
	[TaskStatus] = @sTaskStatus,
	[Description] = @sDescription,
	[DueDate] = @daDueDate,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_TaskList_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_TaskList_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'TaskList'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[TaskList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_TaskList_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_TaskList_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'TaskList'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[TaskList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_TaskList_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_TaskList_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'TaskList'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[TaskList]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     TaskList]
-- ========================================================================================================
GO
