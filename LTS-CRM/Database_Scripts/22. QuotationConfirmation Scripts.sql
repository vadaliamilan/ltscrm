
/****** Object:  Table [dbo].[QuotationConfirmationToClient]    Script Date: 04-08-2017 12:28:17 ******/
DROP TABLE [dbo].[QuotationConfirmationToClient]
GO

/****** Object:  Table [dbo].[QuotationConfirmationToClient]    Script Date: 04-08-2017 12:28:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[QuotationConfirmationToClient](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NULL,
	[SendDate] [datetime] NULL,
	[Client] [nvarchar](100) NULL,
	[ClientId] [int] NULL,
	[FirstName] [nvarchar](50) NULL,
	[SurName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[EmailStatus] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_QuotationConfirmationToClient] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[QuotationConfirmationToClient]  WITH CHECK ADD  CONSTRAINT [FK_QuotationConfirmationToClient_QuotationConfirmationToClient] FOREIGN KEY([QuotationId])
REFERENCES [dbo].[OperationQuotation] ([ID])
GO

ALTER TABLE [dbo].[QuotationConfirmationToClient] CHECK CONSTRAINT [FK_QuotationConfirmationToClient_QuotationConfirmationToClient]
GO

 
ALTER TABLE [dbo].[QuotationConfirmationDocument] DROP CONSTRAINT [FK_QuotationConfirmationDocument_QuotationConfirmationToClient]
GO

ALTER TABLE [dbo].[QuotationConfirmationDocument] DROP CONSTRAINT [DF_QuotationConfirmationDocument_IsDeleted]
GO

ALTER TABLE [dbo].[QuotationConfirmationDocument] DROP CONSTRAINT [DF_QuotationConfirmationDocument_IsActive]
GO

/****** Object:  Table [dbo].[QuotationConfirmationDocument]    Script Date: 04-08-2017 13:02:45 ******/
DROP TABLE [dbo].[QuotationConfirmationDocument]
GO

/****** Object:  Table [dbo].[QuotationConfirmationDocument]    Script Date: 04-08-2017 13:02:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[QuotationConfirmationDocument](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[confirmationID] [int] NULL,
	[documentName] [nvarchar](50) NULL,
	[documentType] [nvarchar](20) NULL,
	[extension] [nvarchar](10) NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[documentFile] [varbinary](max) NULL,
 CONSTRAINT [PK_QuotationConfirmationDocument] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[QuotationConfirmationDocument] ADD  CONSTRAINT [DF_QuotationConfirmationDocument_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE [dbo].[QuotationConfirmationDocument] ADD  CONSTRAINT [DF_QuotationConfirmationDocument_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

ALTER TABLE [dbo].[QuotationConfirmationDocument]  WITH CHECK ADD  CONSTRAINT [FK_QuotationConfirmationDocument_QuotationConfirmationToClient] FOREIGN KEY([confirmationID])
REFERENCES [dbo].[QuotationConfirmationToClient] ([Id])
GO

ALTER TABLE [dbo].[QuotationConfirmationDocument] CHECK CONSTRAINT [FK_QuotationConfirmationDocument_QuotationConfirmationToClient]
GO



-- ========================================================================================================
-- [Stored Procedures generated for table:  QuotationConfirmationDocument]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'QuotationConfirmationDocument'
-- Gets: @iconfirmationID int
-- Gets: @sdocumentName nvarchar(50)
-- Gets: @sdocumentType nvarchar(20)
-- Gets: @sextension nvarchar(10)
-- Gets: @bIsActive bit
-- Gets: @bIsDeleted bit
-- Gets: @bidocumentFile varbinary(Max)
-- Returns: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_Insert]
	@iconfirmationID int,
	@sdocumentName nvarchar(50),
	@sdocumentType nvarchar(20),
	@sextension nvarchar(10),
	@bIsActive bit,
	@bIsDeleted bit,
	@bidocumentFile varbinary(Max),
	@iId int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationConfirmationDocument]
(
	[confirmationID],
	[documentName],
	[documentType],
	[extension],
	[IsActive],
	[IsDeleted],
	[documentFile]
)
VALUES
(
	@iconfirmationID,
	@sdocumentName,
	@sdocumentType,
	@sextension,
	@bIsActive,
	@bIsDeleted,
	@bidocumentFile
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iId=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'QuotationConfirmationDocument'
-- Gets: @iId int
-- Gets: @iconfirmationID int
-- Gets: @sdocumentName nvarchar(50)
-- Gets: @sdocumentType nvarchar(20)
-- Gets: @sextension nvarchar(10)
-- Gets: @bIsActive bit
-- Gets: @bIsDeleted bit
-- Gets: @bidocumentFile varbinary(Max)
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_Update]
	@iId int,
	@iconfirmationID int,
	@sdocumentName nvarchar(50),
	@sdocumentType nvarchar(20),
	@sextension nvarchar(10),
	@bIsActive bit,
	@bIsDeleted bit,
	@bidocumentFile varbinary(Max),
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationConfirmationDocument]
SET 
	[confirmationID] = @iconfirmationID,
	[documentName] = @sdocumentName,
	[documentType] = @sdocumentType,
	[extension] = @sextension,
	[IsActive] = @bIsActive,
	[IsDeleted] = @bIsDeleted,
	[documentFile] = @bidocumentFile
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [confirmationID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_UpdateAllWconfirmationIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_UpdateAllWconfirmationIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'QuotationConfirmationDocument'.
-- Will reset field [confirmationID] with value @iconfirmationIDOld  to value @iconfirmationID
-- Gets: @iconfirmationID int
-- Gets: @iconfirmationIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_UpdateAllWconfirmationIDLogic]
	@iconfirmationID int,
	@iconfirmationIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationConfirmationDocument]
SET
	[confirmationID] = @iconfirmationID
WHERE
	[confirmationID] = @iconfirmationIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'QuotationConfirmationDocument'
-- using the Primary Key. 
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_Delete]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationConfirmationDocument]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [confirmationID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_DeleteAllWconfirmationIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_DeleteAllWconfirmationIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'QuotationConfirmationDocument'
-- based on a foreign key field.
-- Gets: @iconfirmationID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_DeleteAllWconfirmationIDLogic]
	@iconfirmationID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationConfirmationDocument]
WHERE
	[confirmationID] = @iconfirmationID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'QuotationConfirmationDocument'
-- based on the Primary Key.
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_SelectOne]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[Id],
	[confirmationID],
	[documentName],
	[documentType],
	[extension],
	[IsActive],
	[IsDeleted],
	[documentFile]
FROM [dbo].[QuotationConfirmationDocument]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'QuotationConfirmationDocument'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[Id],
	[confirmationID],
	[documentName],
	[documentType],
	[extension],
	[IsActive],
	[IsDeleted],
	[documentFile]
FROM [dbo].[QuotationConfirmationDocument]
ORDER BY 
	[Id] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [confirmationID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationDocument_SelectAllWconfirmationIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationDocument_SelectAllWconfirmationIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'QuotationConfirmationDocument'
-- based on a foreign key field.
-- Gets: @iconfirmationID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationDocument_SelectAllWconfirmationIDLogic]
	@iconfirmationID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[Id],
	[confirmationID],
	[documentName],
	[documentType],
	[extension],
	[IsActive],
	[IsDeleted],
	[documentFile]
FROM [dbo].[QuotationConfirmationDocument]
WHERE
	[confirmationID] = @iconfirmationID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     QuotationConfirmationDocument]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  QuotationConfirmationToClient]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'QuotationConfirmationToClient'
-- Gets: @iQuotationId int
-- Gets: @daSendDate datetime
-- Gets: @sClient nvarchar(100)
-- Gets: @iClientId int
-- Gets: @sFirstName nvarchar(50)
-- Gets: @sSurName nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sEmailStatus nvarchar(50)
-- Gets: @sMessage nvarchar(Max)
-- Gets: @daCreatedOn datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daModifiedOn datetime
-- Gets: @sModifiedBy nvarchar(50)
-- Returns: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_Insert]
	@iQuotationId int,
	@daSendDate datetime,
	@sClient nvarchar(100),
	@iClientId int,
	@sFirstName nvarchar(50),
	@sSurName nvarchar(50),
	@sEmail nvarchar(50),
	@sEmailStatus nvarchar(50),
	@sMessage nvarchar(Max),
	@daCreatedOn datetime,
	@sCreatedBy nvarchar(50),
	@daModifiedOn datetime,
	@sModifiedBy nvarchar(50),
	@iId int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationConfirmationToClient]
(
	[QuotationId],
	[SendDate],
	[Client],
	[ClientId],
	[FirstName],
	[SurName],
	[Email],
	[EmailStatus],
	[Message],
	[CreatedOn],
	[CreatedBy],
	[ModifiedOn],
	[ModifiedBy]
)
VALUES
(
	@iQuotationId,
	@daSendDate,
	@sClient,
	@iClientId,
	@sFirstName,
	@sSurName,
	@sEmail,
	@sEmailStatus,
	@sMessage,
	@daCreatedOn,
	@sCreatedBy,
	@daModifiedOn,
	@sModifiedBy
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iId=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'QuotationConfirmationToClient'
-- Gets: @iId int
-- Gets: @iQuotationId int
-- Gets: @daSendDate datetime
-- Gets: @sClient nvarchar(100)
-- Gets: @iClientId int
-- Gets: @sFirstName nvarchar(50)
-- Gets: @sSurName nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sEmailStatus nvarchar(50)
-- Gets: @sMessage nvarchar(Max)
-- Gets: @daCreatedOn datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daModifiedOn datetime
-- Gets: @sModifiedBy nvarchar(50)
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_Update]
	@iId int,
	@iQuotationId int,
	@daSendDate datetime,
	@sClient nvarchar(100),
	@iClientId int,
	@sFirstName nvarchar(50),
	@sSurName nvarchar(50),
	@sEmail nvarchar(50),
	@sEmailStatus nvarchar(50),
	@sMessage nvarchar(Max),
	@daCreatedOn datetime,
	@sCreatedBy nvarchar(50),
	@daModifiedOn datetime,
	@sModifiedBy nvarchar(50),
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationConfirmationToClient]
SET 
	[QuotationId] = @iQuotationId,
	[SendDate] = @daSendDate,
	[Client] = @sClient,
	[ClientId] = @iClientId,
	[FirstName] = @sFirstName,
	[SurName] = @sSurName,
	[Email] = @sEmail,
	[EmailStatus] = @sEmailStatus,
	[Message] = @sMessage,
	[CreatedOn] = @daCreatedOn,
	[CreatedBy] = @sCreatedBy,
	[ModifiedOn] = @daModifiedOn,
	[ModifiedBy] = @sModifiedBy
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [QuotationId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_UpdateAllWQuotationIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_UpdateAllWQuotationIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'QuotationConfirmationToClient'.
-- Will reset field [QuotationId] with value @iQuotationIdOld  to value @iQuotationId
-- Gets: @iQuotationId int
-- Gets: @iQuotationIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_UpdateAllWQuotationIdLogic]
	@iQuotationId int,
	@iQuotationIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationConfirmationToClient]
SET
	[QuotationId] = @iQuotationId
WHERE
	[QuotationId] = @iQuotationIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'QuotationConfirmationToClient'
-- using the Primary Key. 
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_Delete]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationConfirmationToClient]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [QuotationId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_DeleteAllWQuotationIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_DeleteAllWQuotationIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'QuotationConfirmationToClient'
-- based on a foreign key field.
-- Gets: @iQuotationId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_DeleteAllWQuotationIdLogic]
	@iQuotationId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationConfirmationToClient]
WHERE
	[QuotationId] = @iQuotationId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'QuotationConfirmationToClient'
-- based on the Primary Key.
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_SelectOne]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[Id],
	[QuotationId],
	[SendDate],
	[Client],
	[ClientId],
	[FirstName],
	[SurName],
	[Email],
	[EmailStatus],
	[Message],
	[CreatedOn],
	[CreatedBy],
	[ModifiedOn],
	[ModifiedBy]
FROM [dbo].[QuotationConfirmationToClient]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'QuotationConfirmationToClient'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[Id],
	[QuotationId],
	[SendDate],
	[Client],
	[ClientId],
	[FirstName],
	[SurName],
	[Email],
	[EmailStatus],
	[Message],
	[CreatedOn],
	[CreatedBy],
	[ModifiedOn],
	[ModifiedBy]
FROM [dbo].[QuotationConfirmationToClient]
ORDER BY 
	[Id] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [QuotationId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_QuotationConfirmationToClient_SelectAllWQuotationIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_QuotationConfirmationToClient_SelectAllWQuotationIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'QuotationConfirmationToClient'
-- based on a foreign key field.
-- Gets: @iQuotationId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationConfirmationToClient_SelectAllWQuotationIdLogic]
	@iQuotationId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[Id],
	[QuotationId],
	[SendDate],
	[Client],
	[ClientId],
	[FirstName],
	[SurName],
	[Email],
	[EmailStatus],
	[Message],
	[CreatedOn],
	[CreatedBy],
	[ModifiedOn],
	[ModifiedBy]
FROM [dbo].[QuotationConfirmationToClient]
WHERE
	[QuotationId] = @iQuotationId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     QuotationConfirmationToClient]
-- ========================================================================================================
GO

-- //// Select Stored procedure, based on field [QuotationId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationQuotation_GetBookingDetailsById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].sp_OperationQuotation_GetBookingDetailsById
GO


---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'OperationQuotation'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
Create PROCEDURE [dbo].sp_OperationQuotation_GetBookingDetailsById
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
distinct
QM.tourcode,
	QB.ID as basicId
	,QB.QuotationPriceId
	,QB.NoOfDay
	,QB.TourDate
	,QB.TourCity
	,QB.QuotationCurrency
	,QH.RecommendedHotel as ID
	,SM.Name as  RecommendedHotel
	,QMeal.TypeOfMeal as  TypeOfMeal
	,'' as trans 
	,''as tourguideName
	,'' as coachName
	,''as driverName
	,''as tourguidePhone
	,''as coachTelephone
	,''as mobile
	,''as prices
	,''as priceinclude
	,''as pricesexclude
	,''as remark

FROM  [dbo].[OperationQuotation] QM   
inner join QuotationPrice QP on QM.ID= QP.QuotationId
left join QuotationPrice_Basic QB on QB.QuotationPriceId= qp.ID
left join QuotationPrice_Hotel QH on QH.QuotationPriceBasicId= QB.ID
left join QuotationPrice_Lunch QMeal on QMeal.QuotationPriceBasicId= QB.ID
left join QuotationPrice_Coach QCoach on QCoach.QuotationPriceBasicId= QB.ID
left join SupplierMaster SM on SM.id = QH.RecommendedHotel
 
WHERE
	QM.[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO