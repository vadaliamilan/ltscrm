 IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet] DROP CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory]
GO
/****** Object:  Table [dbo].[OpinionSheetCategory]    Script Date: 13-05-2017 10:59:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheetCategory]') AND type in (N'U'))
DROP TABLE [dbo].[OpinionSheetCategory]
GO
/****** Object:  Table [dbo].[OpinionSheet]    Script Date: 13-05-2017 10:59:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheet]') AND type in (N'U'))
DROP TABLE [dbo].[OpinionSheet]
GO
/****** Object:  Table [dbo].[OpinionSheet]    Script Date: 13-05-2017 10:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpinionSheet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Date] [datetime] NULL,
	[Staff] [nvarchar](250) NULL,
	[OpinionSheetCategoryId] [int] NULL,
	[FileData] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OpinionSheet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpinionSheetCategory]    Script Date: 13-05-2017 10:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheetCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpinionSheetCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OpinionSheetCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet]  WITH CHECK ADD  CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory] FOREIGN KEY([OpinionSheetCategoryId])
REFERENCES [dbo].[OpinionSheetCategory] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet] CHECK CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory]
GO
