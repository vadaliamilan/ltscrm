/****** Object:  Table [dbo].[GeneralCategoryItems]    Script Date: 24-07-2017 14:30:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralCategoryItems]') AND type in (N'U'))
DROP TABLE [dbo].[GeneralCategoryItems]
GO
/****** Object:  Table [dbo].[GeneralCategoryItems]    Script Date: 24-07-2017 14:30:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralCategoryItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeneralCategoryItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](150) NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_GeneralCategoryItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[GeneralCategoryItems] ON 

GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (1, N'Language', N'English', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (2, N'Language', N'Chinese', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (3, N'AppointmentStatus', N'Start', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (4, N'AppointmentStatus', N'End', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (5, N'AppointmentStatus', N'Pending', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (6, N'ItineraryType', N'Students
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (7, N'ItineraryType', N'Government
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (8, N'ItineraryType', N'Business
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (9, N'ItineraryType', N'Family
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (10, N'TaskStatus', N'Start', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (11, N'TaskStatus', N'Pending', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (12, N'TaskStatus', N'End', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (13, N'ContactStatus', N'Start', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (14, N'ContactStatus', N'Enquiry', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (15, N'ContactStatus', N'Approve', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (16, N'ChannelStatus', N'Start', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (17, N'ChannelStatus', N'Enquiry', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (18, N'ChannelStatus', N'Approve', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (19, N'ChannelStarRating', N'1', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (20, N'ChannelStarRating', N'2', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (21, N'ChannelStarRating', N'3', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (22, N'ProductType', N'Sell', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (23, N'ProductType', N'Resell', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (24, N'ProductType', N'New', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (25, N'ProductContractType', N'Net Rate', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (26, N'ProductContractType', N'Commision', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (27, N'LeadTourType', N'Tailor-Made Tours', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (28, N'LeadPreferredContactTime', N'Anytime', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (29, N'LeadPreferredContactTime', N'Working Hours', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (30, N'LeadPreferredContactTime', N'Off Hours', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (31, N'LeadPreferredContactMethod', N'Email', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (32, N'LeadPreferredContactMethod', N'Mobile', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (33, N'LeadTourGuide', N'English-Speaking', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (34, N'LeadTourGuide', N'Other Languages', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (35, N'LeadFlightNeeded', N'Yes', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (36, N'LeadFlightNeeded', N'No', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (37, N'LeadHotelStar', N'3-Star', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (38, N'LeadHotelStar', N'4-Star', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (39, N'LeadHotelStar', N'5-Star', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (40, N'LeadHotelStar', N'Not Needed', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (41, N'LeadHotelStar', N'Arrange Specific Hotels', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (42, N'LeadMeal', N'Included', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (43, N'LeadMeal', N'Excluded', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (44, N'LeadAdmissionTicket', N'Included', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (45, N'LeadAdmissionTicket', N'Excluded', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (46, N'LeadInsurance', N'Included', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (47, N'LeadInsurance', N'Excluded', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (48, N'Currency', N'GBP', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (49, N'Currency', N'EUR', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (50, N'Currency', N'USD', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (51, N'LeadStatus', N'Start', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (52, N'LeadStatus', N'Pending', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (53, N'LeadStatus', N'End', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (54, N'Contract', N'Yes', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (55, N'Contract', N'No', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (56, N'ContractWith', N'Parent Company
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (57, N'ContractWith', N'View parent Company File
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (58, N'ContractWith', N'Directly with supplier
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (59, N'ContractWith', N'No Contract
', NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (60, N'ChannelStarRating', N'5', N'5', N'Nilesh', CAST(0x0000A78301754823 AS DateTime), N'Nilesh', CAST(0x0000A78301754823 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (61, N'Sex', N'Male', N'Male', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (62, N'Sex', N'Female', N'Female', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (63, N'Married', N'Single', N'Single', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (64, N'Married', N'Married', N'Married', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (65, N'Married', N'Divorced', N'Divorced', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (66, N'SpeakingLang', N'Mandarin
', N'Mandarin
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (67, N'SpeakingLang', N'English
', N'English
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (68, N'SpeakingLang', N'Italian
', N'Italian
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (69, N'SpeakingLang', N'Cantonese
', N'Cantonese
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (70, N'SpeakingLang', N'French
', N'French
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (71, N'AreaOfWorking', N'London
', N'London
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (72, N'AreaOfWorking', N'London and surronding area
', N'London and surronding area
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (73, N'AreaOfWorking', N'Manchester
', N'Manchester
', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (74, N'AreaOfWorking', N'Edinburgh', N'Edinburgh', NULL, CAST(0x0000A78B01422360 AS DateTime), NULL, CAST(0x0000A78B01422360 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (75, N'AreaOfWorking', N'Edinburgh and surrdonging area', N'Edinburgh', NULL, CAST(0x0000A78B01422361 AS DateTime), NULL, CAST(0x0000A78B01422361 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (76, N'AreaOfWorking', N'Belfast', N'Belfast', NULL, CAST(0x0000A78B01422361 AS DateTime), NULL, CAST(0x0000A78B01422361 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (77, N'AreaOfWorking', N'Belfast and surronding area', N'Belfast and surronding area', NULL, CAST(0x0000A78B01422362 AS DateTime), NULL, CAST(0x0000A78B01422362 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (78, N'AreaOfWorking', N'Dublin', N'Dublin', NULL, CAST(0x0000A78B0142236B AS DateTime), NULL, CAST(0x0000A78B0142236B AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (79, N'AreaOfWorking', N'Dublin and surronding area', N'Dublin and surronding area', NULL, CAST(0x0000A78B0142236C AS DateTime), NULL, CAST(0x0000A78B0142236C AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (80, N'AreaOfWorking', N'Uk', N'Uk', NULL, CAST(0x0000A78B01422378 AS DateTime), NULL, CAST(0x0000A78B01422378 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (81, N'AreaOfWorking', N'Belfast and Ireland', N'Belfast and Ireland', NULL, CAST(0x0000A78B01422379 AS DateTime), NULL, CAST(0x0000A78B01422379 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (82, N'AreaOfWorking', N'UK and Ireland', N'UK and Ireland', NULL, CAST(0x0000A78B01422381 AS DateTime), NULL, CAST(0x0000A78B01422381 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (83, N'TypeOfWork', N'4-9 seater driver guide', N'4-9 seater driver guide', NULL, CAST(0x0000A78B01440B60 AS DateTime), NULL, CAST(0x0000A78B01440B60 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (84, N'TypeOfWork', N'12 - 17 seater driver guide', N'12 - 17 seater driver guide', NULL, CAST(0x0000A78B01440B60 AS DateTime), NULL, CAST(0x0000A78B01440B60 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (85, N'TypeOfWork', N'coach guide', N'coach guide', NULL, CAST(0x0000A78B01440B60 AS DateTime), NULL, CAST(0x0000A78B01440B60 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (86, N'VehicleType', N'5 seater', N'5 seater', NULL, CAST(0x0000A78B01445356 AS DateTime), NULL, CAST(0x0000A78B01445356 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (87, N'VehicleType', N'8 seater', N'8 seater', NULL, CAST(0x0000A78B01445356 AS DateTime), NULL, CAST(0x0000A78B01445356 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (88, N'VehicleType', N'9 seater', N'9 seater', NULL, CAST(0x0000A78B01445356 AS DateTime), NULL, CAST(0x0000A78B01445356 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (89, N'NotWorkWithGroup', N'Business', N'Business', NULL, CAST(0x0000A78B01447800 AS DateTime), NULL, CAST(0x0000A78B01447800 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (90, N'NotWorkWithGroup', N'Students', N'Students', NULL, CAST(0x0000A78B01447800 AS DateTime), NULL, CAST(0x0000A78B01447800 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (91, N'NotWorkWithGroup', N'Corporate', N'Corporate', NULL, CAST(0x0000A78B01447800 AS DateTime), NULL, CAST(0x0000A78B01447800 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (92, N'NotWorkWithGroup', N'Students', N'Students', NULL, CAST(0x0000A78B01447800 AS DateTime), NULL, CAST(0x0000A78B01447800 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (93, N'NotWorkWithGroup', N'others', N'others', NULL, CAST(0x0000A78B01447800 AS DateTime), NULL, CAST(0x0000A78B01447800 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (94, N'Seasons', N'Winter season', N'Winter season', NULL, CAST(0x0000A78B017237A8 AS DateTime), NULL, CAST(0x0000A78B017237A8 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (95, N'Seasons', N'Summer Season', N'Summer Season', NULL, CAST(0x0000A78B017237A8 AS DateTime), NULL, CAST(0x0000A78B017237A8 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (96, N'Seasons', N'Summer High season', N'Summer High season', NULL, CAST(0x0000A78B017237A8 AS DateTime), NULL, CAST(0x0000A78B017237A8 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (97, N'QuotationStatus', N'QuotationInProgress', NULL, NULL, CAST(0x0000A79800000000 AS DateTime), NULL, CAST(0x0000A79800000000 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (98, N'QuotationStatus', N'Amendament', N'Amendament', N'Nilesh', CAST(0x0000A7980104C128 AS DateTime), N'Nilesh', CAST(0x0000A7980104C128 AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (99, N'QuotationStatus', N'Confirmation', N'Confirmation', N'Nilesh', CAST(0x0000A7980104D32D AS DateTime), N'Nilesh', CAST(0x0000A7980104D32D AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (100, N'QuotationStatus', N'Cancel', N'Cancel', N'Nilesh', CAST(0x0000A7980104DC2A AS DateTime), N'Nilesh', CAST(0x0000A7980104DC2A AS DateTime))
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (101, N'MealType', N'Italian', N'Italian', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (102, N'MealType', N'Maxican', N'Maxican', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (103, N'GuideType', N'General', N'General', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (104, N'GuideType', N'City', N'City', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (105, N'GuideType', N'Heritage', N'Heritage', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (106, N'VehicaleType', N'Car', N'Car', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (107, N'VehicaleType', N'Bike', N'Bike', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (108, N'VehicaleType', N'Bus', N'BUS', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (109, N'VehicaleType', N'Van', N'Van', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (110, N'CoachType', N'CoachType1', N'CoachType1', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (111, N'CoachType', N'BUS', N'BUS', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (112, N'CoachType', N'Train', N'Train', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (113, N'CoachType', N'Flight', N'Flight', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GeneralCategoryItems] ([ID], [Category], [Title], [Description], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate]) VALUES (114, N'CoachType', N'Tour', N'Tour', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[GeneralCategoryItems] OFF
GO
