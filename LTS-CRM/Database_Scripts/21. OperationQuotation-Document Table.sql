
ALTER TABLE [dbo].[OperationQuotationDocuments] DROP CONSTRAINT [FK_OperationQuotationDocuments_OperationQuotation]
GO

/****** Object:  Table [dbo].[OperationQuotationDocuments]    Script Date: 19-07-2017 19:52:20 ******/
DROP TABLE [dbo].[OperationQuotationDocuments]
GO

/****** Object:  Table [dbo].[OperationQuotationDocuments]    Script Date: 19-07-2017 19:52:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OperationQuotationDocuments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OperationQuotationId] [int] NOT NULL,
	[DocumentName] [nvarchar](1500) NOT NULL,
	[FileType] [nvarchar](1500) NULL,
	[FileName] [nvarchar](1500) NULL,
	[FileData] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OperationQuotationDocuments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[OperationQuotationDocuments]  WITH CHECK ADD  CONSTRAINT [FK_OperationQuotationDocuments_OperationQuotation] FOREIGN KEY([OperationQuotationId])
REFERENCES [dbo].[OperationQuotation] ([ID])
GO

ALTER TABLE [dbo].[OperationQuotationDocuments] CHECK CONSTRAINT [FK_OperationQuotationDocuments_OperationQuotation]
GO


