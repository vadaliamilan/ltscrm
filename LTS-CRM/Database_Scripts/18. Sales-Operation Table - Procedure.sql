/****** Object:  Table [dbo].[SalesTaskList]    Script Date: 27-05-2017 21:22:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesTaskList]') AND type in (N'U'))
DROP TABLE [dbo].[SalesTaskList]
GO
/****** Object:  Table [dbo].[SalesLeadManagement]    Script Date: 27-05-2017 21:22:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesLeadManagement]') AND type in (N'U'))
DROP TABLE [dbo].[SalesLeadManagement]
GO
/****** Object:  Table [dbo].[SalesContactManagement]    Script Date: 27-05-2017 21:22:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesContactManagement]') AND type in (N'U'))
DROP TABLE [dbo].[SalesContactManagement]
GO
/****** Object:  Table [dbo].[SalesAppointmentList]    Script Date: 27-05-2017 21:22:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesAppointmentList]') AND type in (N'U'))
DROP TABLE [dbo].[SalesAppointmentList]
GO
/****** Object:  Table [dbo].[OperationLeadMgmt]    Script Date: 27-05-2017 21:22:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationLeadMgmt]') AND type in (N'U'))
DROP TABLE [dbo].[OperationLeadMgmt]
GO
/****** Object:  Table [dbo].[OperationContactMgmt]    Script Date: 27-05-2017 21:22:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationContactMgmt]') AND type in (N'U'))
DROP TABLE [dbo].[OperationContactMgmt]
GO
/****** Object:  Table [dbo].[OperationContactMgmt]    Script Date: 27-05-2017 21:22:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationContactMgmt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OperationContactMgmt](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[Status] [nvarchar](50) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OperationContactMgmt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OperationLeadMgmt]    Script Date: 27-05-2017 21:22:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationLeadMgmt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OperationLeadMgmt](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[CampaignID] [int] NULL,
	[TourType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Gender] [nvarchar](10) NOT NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[PreferredContactTime] [nvarchar](50) NULL,
	[PreferredContactMethod] [nvarchar](50) NULL,
	[PlannedDepartureDate] [datetime] NULL,
	[HowManyDays] [int] NULL,
	[HowManyPeople] [int] NULL,
	[HowManyChild] [int] NULL,
	[PlaceOfDeparture] [nvarchar](50) NULL,
	[PlaceOfReturn] [nvarchar](50) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[Currency] [nvarchar](15) NULL,
	[TourGuide] [nvarchar](50) NULL,
	[FlightsNeeded] [bit] NULL,
	[Vehicle] [nvarchar](50) NULL,
	[HotelType] [nvarchar](50) NULL,
	[Meals] [nvarchar](50) NULL,
	[AdmissionTickets] [nvarchar](50) NULL,
	[Insurance] [nvarchar](50) NULL,
	[LeadSource] [nvarchar](50) NULL,
	[LeadOwner] [nvarchar](50) NULL,
	[LeadStatus] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OperationLeadMgmt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesAppointmentList]    Script Date: 27-05-2017 21:22:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesAppointmentList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SalesAppointmentList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Subject] [nvarchar](250) NULL,
	[Status] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[AppointmentDate] [datetime] NULL,
	[AppointmentTime] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SalesAppointmentList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SalesContactManagement]    Script Date: 27-05-2017 21:22:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesContactManagement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SalesContactManagement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[Status] [nvarchar](50) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SALESContactManagement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesLeadManagement]    Script Date: 27-05-2017 21:22:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesLeadManagement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SalesLeadManagement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[CampaignID] [int] NULL,
	[TourType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Gender] [nvarchar](10) NOT NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[PreferredContactTime] [nvarchar](50) NULL,
	[PreferredContactMethod] [nvarchar](50) NULL,
	[PlannedDepartureDate] [datetime] NULL,
	[HowManyDays] [int] NULL,
	[HowManyPeople] [int] NULL,
	[HowManyChild] [int] NULL,
	[PlaceOfDeparture] [nvarchar](50) NULL,
	[PlaceOfReturn] [nvarchar](50) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[Currency] [nvarchar](15) NULL,
	[TourGuide] [nvarchar](50) NULL,
	[FlightsNeeded] [bit] NULL,
	[Vehicle] [nvarchar](50) NULL,
	[HotelType] [nvarchar](50) NULL,
	[Meals] [nvarchar](50) NULL,
	[AdmissionTickets] [nvarchar](50) NULL,
	[Insurance] [nvarchar](50) NULL,
	[LeadSource] [nvarchar](50) NULL,
	[LeadOwner] [nvarchar](50) NULL,
	[LeadStatus] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SalesLeadManagement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalesTaskList]    Script Date: 27-05-2017 21:22:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesTaskList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SalesTaskList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Subject] [nvarchar](250) NULL,
	[AssignedTo] [nvarchar](150) NULL,
	[TaskStatus] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[DueDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SalesTaskList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/-------- PROCEDURES -------------/
 

-- ========================================================================================================
-- [Stored Procedures generated for table:  OperationContactMgmt]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationContactMgmt_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationContactMgmt_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'OperationContactMgmt'
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OperationContactMgmt]
(
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@sCompany,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sStatus,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationContactMgmt_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationContactMgmt_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'OperationContactMgmt'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OperationContactMgmt]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[Status] = @sStatus,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationContactMgmt_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationContactMgmt_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'OperationContactMgmt'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OperationContactMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationContactMgmt_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationContactMgmt_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'OperationContactMgmt'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationContactMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationContactMgmt_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationContactMgmt_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'OperationContactMgmt'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationContactMgmt]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     OperationContactMgmt]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  OperationLeadMgmt]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationLeadMgmt_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationLeadMgmt_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'OperationLeadMgmt'
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OperationLeadMgmt]
(
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@iCampaignID,
	@sTourType,
	@sCompany,
	@sTitle,
	@sGender,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sPreferredContactTime,
	@sPreferredContactMethod,
	@daPlannedDepartureDate,
	@iHowManyDays,
	@iHowManyPeople,
	@iHowManyChild,
	@sPlaceOfDeparture,
	@sPlaceOfReturn,
	@dcTotalBudget,
	@sCurrency,
	@sTourGuide,
	@bFlightsNeeded,
	@sVehicle,
	@sHotelType,
	@sMeals,
	@sAdmissionTickets,
	@sInsurance,
	@sLeadSource,
	@sLeadOwner,
	@sLeadStatus,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationLeadMgmt_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationLeadMgmt_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'OperationLeadMgmt'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OperationLeadMgmt]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[CampaignID] = @iCampaignID,
	[TourType] = @sTourType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[Gender] = @sGender,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[PreferredContactTime] = @sPreferredContactTime,
	[PreferredContactMethod] = @sPreferredContactMethod,
	[PlannedDepartureDate] = @daPlannedDepartureDate,
	[HowManyDays] = @iHowManyDays,
	[HowManyPeople] = @iHowManyPeople,
	[HowManyChild] = @iHowManyChild,
	[PlaceOfDeparture] = @sPlaceOfDeparture,
	[PlaceOfReturn] = @sPlaceOfReturn,
	[TotalBudget] = @dcTotalBudget,
	[Currency] = @sCurrency,
	[TourGuide] = @sTourGuide,
	[FlightsNeeded] = @bFlightsNeeded,
	[Vehicle] = @sVehicle,
	[HotelType] = @sHotelType,
	[Meals] = @sMeals,
	[AdmissionTickets] = @sAdmissionTickets,
	[Insurance] = @sInsurance,
	[LeadSource] = @sLeadSource,
	[LeadOwner] = @sLeadOwner,
	[LeadStatus] = @sLeadStatus,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationLeadMgmt_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationLeadMgmt_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'OperationLeadMgmt'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OperationLeadMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationLeadMgmt_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationLeadMgmt_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'OperationLeadMgmt'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationLeadMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_OperationLeadMgmt_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_OperationLeadMgmt_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'OperationLeadMgmt'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationLeadMgmt]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     OperationLeadMgmt]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SalesAppointmentList]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesAppointmentList_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesAppointmentList_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SalesAppointmentList'
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daAppointmentDate datetime
-- Gets: @daAppointmentTime datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesAppointmentList_Insert]
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daAppointmentDate datetime,
	@daAppointmentTime datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SalesAppointmentList]
(
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sSubject,
	@sStatus,
	@sDescription,
	@daAppointmentDate,
	@daAppointmentTime,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesAppointmentList_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesAppointmentList_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SalesAppointmentList'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daAppointmentDate datetime
-- Gets: @daAppointmentTime datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesAppointmentList_Update]
	@iID int,
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daAppointmentDate datetime,
	@daAppointmentTime datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SalesAppointmentList]
SET 
	[CompanyID] = @iCompanyID,
	[Subject] = @sSubject,
	[Status] = @sStatus,
	[Description] = @sDescription,
	[AppointmentDate] = @daAppointmentDate,
	[AppointmentTime] = @daAppointmentTime,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesAppointmentList_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesAppointmentList_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SalesAppointmentList'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesAppointmentList_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SalesAppointmentList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesAppointmentList_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesAppointmentList_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SalesAppointmentList'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesAppointmentList_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesAppointmentList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesAppointmentList_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesAppointmentList_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SalesAppointmentList'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesAppointmentList_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesAppointmentList]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SalesAppointmentList]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SalesContactManagement]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesContactManagement_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesContactManagement_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SalesContactManagement'
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesContactManagement_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SalesContactManagement]
(
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@sCompany,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sStatus,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesContactManagement_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesContactManagement_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SalesContactManagement'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesContactManagement_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SalesContactManagement]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[Status] = @sStatus,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesContactManagement_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesContactManagement_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SalesContactManagement'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesContactManagement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SalesContactManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesContactManagement_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesContactManagement_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SalesContactManagement'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesContactManagement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesContactManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesContactManagement_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesContactManagement_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SalesContactManagement'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesContactManagement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesContactManagement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SalesContactManagement]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SalesLeadManagement]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesLeadManagement_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesLeadManagement_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SalesLeadManagement'
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesLeadManagement_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SalesLeadManagement]
(
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@iCampaignID,
	@sTourType,
	@sCompany,
	@sTitle,
	@sGender,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sPreferredContactTime,
	@sPreferredContactMethod,
	@daPlannedDepartureDate,
	@iHowManyDays,
	@iHowManyPeople,
	@iHowManyChild,
	@sPlaceOfDeparture,
	@sPlaceOfReturn,
	@dcTotalBudget,
	@sCurrency,
	@sTourGuide,
	@bFlightsNeeded,
	@sVehicle,
	@sHotelType,
	@sMeals,
	@sAdmissionTickets,
	@sInsurance,
	@sLeadSource,
	@sLeadOwner,
	@sLeadStatus,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesLeadManagement_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesLeadManagement_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SalesLeadManagement'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesLeadManagement_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SalesLeadManagement]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[CampaignID] = @iCampaignID,
	[TourType] = @sTourType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[Gender] = @sGender,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[PreferredContactTime] = @sPreferredContactTime,
	[PreferredContactMethod] = @sPreferredContactMethod,
	[PlannedDepartureDate] = @daPlannedDepartureDate,
	[HowManyDays] = @iHowManyDays,
	[HowManyPeople] = @iHowManyPeople,
	[HowManyChild] = @iHowManyChild,
	[PlaceOfDeparture] = @sPlaceOfDeparture,
	[PlaceOfReturn] = @sPlaceOfReturn,
	[TotalBudget] = @dcTotalBudget,
	[Currency] = @sCurrency,
	[TourGuide] = @sTourGuide,
	[FlightsNeeded] = @bFlightsNeeded,
	[Vehicle] = @sVehicle,
	[HotelType] = @sHotelType,
	[Meals] = @sMeals,
	[AdmissionTickets] = @sAdmissionTickets,
	[Insurance] = @sInsurance,
	[LeadSource] = @sLeadSource,
	[LeadOwner] = @sLeadOwner,
	[LeadStatus] = @sLeadStatus,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesLeadManagement_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesLeadManagement_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SalesLeadManagement'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesLeadManagement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SalesLeadManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesLeadManagement_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesLeadManagement_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SalesLeadManagement'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesLeadManagement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesLeadManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesLeadManagement_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesLeadManagement_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SalesLeadManagement'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesLeadManagement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesLeadManagement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SalesLeadManagement]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  SalesTaskList]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesTaskList_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesTaskList_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'SalesTaskList'
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sAssignedTo nvarchar(150)
-- Gets: @sTaskStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daDueDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesTaskList_Insert]
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sAssignedTo nvarchar(150),
	@sTaskStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daDueDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SalesTaskList]
(
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sSubject,
	@sAssignedTo,
	@sTaskStatus,
	@sDescription,
	@daDueDate,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesTaskList_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesTaskList_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'SalesTaskList'
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sAssignedTo nvarchar(150)
-- Gets: @sTaskStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daDueDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesTaskList_Update]
	@iID int,
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sAssignedTo nvarchar(150),
	@sTaskStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daDueDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SalesTaskList]
SET 
	[CompanyID] = @iCompanyID,
	[Subject] = @sSubject,
	[AssignedTo] = @sAssignedTo,
	[TaskStatus] = @sTaskStatus,
	[Description] = @sDescription,
	[DueDate] = @daDueDate,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesTaskList_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesTaskList_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'SalesTaskList'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesTaskList_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SalesTaskList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesTaskList_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesTaskList_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'SalesTaskList'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesTaskList_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesTaskList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_SalesTaskList_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_SalesTaskList_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'SalesTaskList'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SalesTaskList_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SalesTaskList]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     SalesTaskList]
-- ========================================================================================================
GO

/*********** END ***************/