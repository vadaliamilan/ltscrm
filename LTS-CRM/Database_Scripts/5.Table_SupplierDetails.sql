IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierMaster', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierMaster', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Breakfast'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Breakfast'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'FamilyRoom'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'FamilyRoom'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Triple'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Triple'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Twin_DoubleForSoleUse'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Twin_DoubleForSoleUse'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Single'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Single'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'HalfTwin'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'HalfTwin'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'Cabin'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'Cabin'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'GroupRate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'GroupRate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'FitRate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'FitRate'

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate] DROP CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate] DROP CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate] DROP CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] DROP CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement] DROP CONSTRAINT [FK_SupplierMealSupplement_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster] DROP CONSTRAINT [FK_SupplierMaster_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate] DROP CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation] DROP CONSTRAINT [FK_SupplierInformation_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate] DROP CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate] DROP CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts] DROP CONSTRAINT [FK_SupplierContacts_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SupplierType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SupplierType] DROP CONSTRAINT [DF_SupplierType_IsActive]
END

GO
/****** Object:  Table [dbo].[SupplierVanHireRate]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierVanHireRate]
GO
/****** Object:  Table [dbo].[SupplierType]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierType]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierType]
GO
/****** Object:  Table [dbo].[SupplierTrainRate]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTrainRate]
GO
/****** Object:  Table [dbo].[SupplierTheaterRate]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTheaterRate]
GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
DROP TABLE [dbo].[SuppliersDetails]
GO
/****** Object:  Table [dbo].[SupplierMealSupplement]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierMealSupplement]
GO
/****** Object:  Table [dbo].[SupplierMaster]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMaster]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierMaster]
GO
/****** Object:  Table [dbo].[SupplierInsuranceRate]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierInsuranceRate]
GO
/****** Object:  Table [dbo].[SupplierInformation]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInformation]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierInformation]
GO
/****** Object:  Table [dbo].[SupplierHotelRate]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierHotelRate]
GO
/****** Object:  Table [dbo].[SupplierFerryRate]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierFerryRate]
GO
/****** Object:  Table [dbo].[SupplierContacts]    Script Date: 05-04-2017 18:51:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierContacts]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierContacts]
GO
/****** Object:  Table [dbo].[SupplierContacts]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierContacts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierContacts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](510) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierContacts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierFerryRate]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierFerryRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[CountryFrom] [nvarchar](50) NULL,
	[PortFrom] [nvarchar](50) NULL,
	[CountryTo] [nvarchar](50) NULL,
	[PortTo] [nvarchar](50) NULL,
	[DepartTime] [datetime] NULL,
	[ArrivalTime] [datetime] NULL,
	[OneWayOrReturn] [bit] NULL,
	[Description] [nvarchar](max) NULL,
	[FitRate] [nvarchar](10) NULL,
	[GroupRate] [nvarchar](10) NULL,
	[Remark] [nvarchar](max) NULL,
	[Cabin] [int] NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierFerryRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierHotelRate]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierHotelRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[HalfTwin] [nvarchar](150) NULL,
	[Single] [nvarchar](50) NULL,
	[Twin_DoubleForSoleUse] [nvarchar](50) NULL,
	[Triple] [nvarchar](50) NULL,
	[FamilyRoom] [nvarchar](50) NULL,
	[Breakfast] [nvarchar](50) NULL,
	[DinnerAtHotel] [nvarchar](50) NULL,
	[RatePerPerson] [nvarchar](10) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierHotelRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierInformation]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInformation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierInformation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[Currency] [nvarchar](10) NULL,
	[NoofBedRooms] [int] NULL,
	[HotelFacility] [nvarchar](max) NULL,
	[Parking] [nvarchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierInformation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierInsuranceRate]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierInsuranceRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[RateDescription] [nvarchar](max) NULL,
	[Rate] [nvarchar](10) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierInsuranceRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierMaster]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierTypeID] [int] NULL,
	[Name] [nvarchar](250) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[Email] [nvarchar](50) NULL,
	[CreditToLetsTravel] [nvarchar](50) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[ContractEndingDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierMealSupplement]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]') AND type in (N'U'))
BEGIN
 

CREATE TABLE [dbo].[SupplierMealSupplement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierTypeID] [int] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[Purpose] [nvarchar](max) NULL,
	[MealSupplementType] [nvarchar](50) NULL,
	[UK] [decimal](18, 0) NULL,
	[Ireland] [decimal](18, 0) NULL,
	[Europe] [decimal](18, 0) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierMealSupplement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


END

GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SuppliersDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierTypeID] [int] NULL,
	[SupplierDate] [datetime] NULL,
	[ContractedBy] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[StarRating] [nvarchar](10) NULL,
	[Website] [nvarchar](150) NULL,
	[Agency] [nvarchar](100) NULL,
	[ClientCode] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[FaxNumber] [nvarchar](15) NULL,
	[EmergencyNo] [nvarchar](15) NULL,
	[Email] [nvarchar](50) NULL,
	[AlternateEmail] [nvarchar](50) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[HotelChain] [nvarchar](50) NULL,
	[Franchised] [nvarchar](50) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[CreditToLetsTravel] [nvarchar](50) NULL,
	[BlackList] [nvarchar](200) NULL,
	[ContractStartDate] [datetime] NULL,
	[ContractEndingDate] [datetime] NULL,
	[ContractRenewalDate] [datetime] NULL,
	[Rating] [nvarchar](10) NULL,
	[MainBusiness] [nvarchar](200) NULL,
	[Comments] [nvarchar](max) NULL,
	[FileData] [varbinary](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SuppliersDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTheaterRate]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTheaterRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[Rate] [nvarchar](10) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTheaterRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierTrainRate]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTrainRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[RouteFrom] [nvarchar](150) NULL,
	[RouteTo] [nvarchar](150) NULL,
	[Rate] [nvarchar](10) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTrainRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierType]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierType] [varchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SupplierType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierVanHireRate]    Script Date: 05-04-2017 18:51:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierVanHireRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[NoOfSeater] [nvarchar](10) NULL,
	[MakeAndModel] [nvarchar](50) NULL,
	[RatePerDay] [nvarchar](10) NULL,
	[AgeofVanInGeneral] [nvarchar](50) NULL,
	[IdealNoOfPassengers] [nvarchar](10) NULL,
	[MaxNoWithoutluggage] [nvarchar](10) NULL,
	[MaxNoWithluggage] [nvarchar](10) NULL,
	[InsurancePolicy] [nvarchar](50) NULL,
	[AccessFeeInsurance] [nvarchar](10) NULL,
	[DriverRequirement] [nvarchar](150) NULL,
	[FileData] [varbinary](max) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierVanHireRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SupplierType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SupplierType] ADD  CONSTRAINT [DF_SupplierType_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts]  WITH CHECK ADD  CONSTRAINT [FK_SupplierContacts_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts] CHECK CONSTRAINT [FK_SupplierContacts_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate] CHECK CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate] CHECK CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation]  WITH CHECK ADD  CONSTRAINT [FK_SupplierInformation_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation] CHECK CONSTRAINT [FK_SupplierInformation_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate] CHECK CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster]  WITH CHECK ADD  CONSTRAINT [FK_SupplierMaster_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster] CHECK CONSTRAINT [FK_SupplierMaster_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement]  WITH CHECK ADD  CONSTRAINT [FK_SupplierMealSupplement_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement] CHECK CONSTRAINT [FK_SupplierMealSupplement_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails]  WITH CHECK ADD  CONSTRAINT [FK_SuppliersDetails_SupplierMaster] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierMaster] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] CHECK CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate] CHECK CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate] CHECK CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate] CHECK CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'FitRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FIT Rate (less than 10 people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'FitRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'GroupRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Rate (more than 10 people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'GroupRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'Cabin'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cabin ( 4people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'Cabin'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'HalfTwin'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half Twin(2 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'HalfTwin'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Single'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Single ( 1 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Single'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Twin_DoubleForSoleUse'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Twin/Double for sole use ( 1 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Twin_DoubleForSoleUse'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Triple'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Triple ( 2 Adults + 1 Child)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Triple'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'FamilyRoom'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Triple ( 2 Adults + 1 Child)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'FamilyRoom'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Breakfast'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Family Room ( Adults + 1 Child) ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Breakfast'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierMaster', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierMaster', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
