IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_VanHire_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_VanHire]'))
ALTER TABLE [dbo].[QuotationPrice_VanHire] DROP CONSTRAINT [FK_QuotationPrice_VanHire_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_TourGuide_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_TourGuide]'))
ALTER TABLE [dbo].[QuotationPrice_TourGuide] DROP CONSTRAINT [FK_QuotationPrice_TourGuide_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Tips_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Tips]'))
ALTER TABLE [dbo].[QuotationPrice_Tips] DROP CONSTRAINT [FK_QuotationPrice_Tips_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Overview_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Overview]'))
ALTER TABLE [dbo].[QuotationPrice_Overview] DROP CONSTRAINT [FK_QuotationPrice_Overview_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_OtherServices_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_OtherServices]'))
ALTER TABLE [dbo].[QuotationPrice_OtherServices] DROP CONSTRAINT [FK_QuotationPrice_OtherServices_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Lunch_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Lunch]'))
ALTER TABLE [dbo].[QuotationPrice_Lunch] DROP CONSTRAINT [FK_QuotationPrice_Lunch_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Hotel_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Hotel]'))
ALTER TABLE [dbo].[QuotationPrice_Hotel] DROP CONSTRAINT [FK_QuotationPrice_Hotel_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Entrance_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Entrance]'))
ALTER TABLE [dbo].[QuotationPrice_Entrance] DROP CONSTRAINT [FK_QuotationPrice_Entrance_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_DriveGuide_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_DriveGuide]'))
ALTER TABLE [dbo].[QuotationPrice_DriveGuide] DROP CONSTRAINT [FK_QuotationPrice_DriveGuide_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Dinner_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Dinner]'))
ALTER TABLE [dbo].[QuotationPrice_Dinner] DROP CONSTRAINT [FK_QuotationPrice_Dinner_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Coach_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Coach]'))
ALTER TABLE [dbo].[QuotationPrice_Coach] DROP CONSTRAINT [FK_QuotationPrice_Coach_QuotationPrice_Basic]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Basic_QuotationPrice]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Basic]'))
ALTER TABLE [dbo].[QuotationPrice_Basic] DROP CONSTRAINT [FK_QuotationPrice_Basic_QuotationPrice]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice]'))
ALTER TABLE [dbo].[QuotationPrice] DROP CONSTRAINT [FK_QuotationPrice_OperationQuotation]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Quotation_Summary_Quotation_Summary]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]'))
ALTER TABLE [dbo].[Quotation_Summary] DROP CONSTRAINT [FK_Quotation_Summary_Quotation_Summary]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Quotation_Summary_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]'))
ALTER TABLE [dbo].[Quotation_Summary] DROP CONSTRAINT [FK_Quotation_Summary_OperationQuotation]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OperationQuotationDocuments_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[OperationQuotationDocuments]'))
ALTER TABLE [dbo].[OperationQuotationDocuments] DROP CONSTRAINT [FK_OperationQuotationDocuments_OperationQuotation]
GO
/****** Object:  Table [dbo].[QuotationPrice_VanHire]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_VanHire]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_VanHire]
GO
/****** Object:  Table [dbo].[QuotationPrice_TourGuide]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_TourGuide]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_TourGuide]
GO
/****** Object:  Table [dbo].[QuotationPrice_Tips]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Tips]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Tips]
GO
/****** Object:  Table [dbo].[QuotationPrice_Overview]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Overview]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Overview]
GO
/****** Object:  Table [dbo].[QuotationPrice_OtherServices]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_OtherServices]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_OtherServices]
GO
/****** Object:  Table [dbo].[QuotationPrice_Lunch]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Lunch]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Lunch]
GO
/****** Object:  Table [dbo].[QuotationPrice_Hotel]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Hotel]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Hotel]
GO
/****** Object:  Table [dbo].[QuotationPrice_Entrance]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Entrance]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Entrance]
GO
/****** Object:  Table [dbo].[QuotationPrice_DriveGuide]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_DriveGuide]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_DriveGuide]
GO
/****** Object:  Table [dbo].[QuotationPrice_Dinner]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Dinner]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Dinner]
GO
/****** Object:  Table [dbo].[QuotationPrice_Coach]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Coach]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Coach]
GO
/****** Object:  Table [dbo].[QuotationPrice_Basic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Basic]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice_Basic]
GO
/****** Object:  Table [dbo].[QuotationPrice]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice]') AND type in (N'U'))
DROP TABLE [dbo].[QuotationPrice]
GO
/****** Object:  Table [dbo].[Quotation_Summary]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]') AND type in (N'U'))
DROP TABLE [dbo].[Quotation_Summary]
GO
/****** Object:  Table [dbo].[OperationQuotationDocuments]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationQuotationDocuments]') AND type in (N'U'))
DROP TABLE [dbo].[OperationQuotationDocuments]
GO
/****** Object:  Table [dbo].[OperationQuotation]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationQuotation]') AND type in (N'U'))
DROP TABLE [dbo].[OperationQuotation]
GO
/****** Object:  Table [dbo].[OperationLeadMgmt]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationLeadMgmt]') AND type in (N'U'))
DROP TABLE [dbo].[OperationLeadMgmt]
GO
/****** Object:  Table [dbo].[OperationContactMgmt]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationContactMgmt]') AND type in (N'U'))
DROP TABLE [dbo].[OperationContactMgmt]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_VanHire_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_UpdateAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_UpdateAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_UpdateAllWQuotationIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Tips_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_SelectAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_SelectAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_SelectAllWQuotationIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Final]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Final]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_Final]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Overview_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Lunch_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Hotel_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Entrance_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Dinner_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DeleteAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DeleteAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_DeleteAllWQuotationIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_UpdateAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectByPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_DeleteAllWQuotationPriceBasicIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Coach_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_UpdateAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_UpdateAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_UpdateAllWQuotationPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_SelectAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_SelectAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_SelectAllWQuotationPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_DeleteAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_DeleteAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_DeleteAllWQuotationPriceIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_QuotationPrice_Basic_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_UpdateAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_UpdateAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_UpdateAllWQuotationIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_SelectAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_SelectAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_SelectAllWQuotationIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_DeleteAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_DeleteAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_DeleteAllWQuotationIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Quotation_Summary_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationQuotation_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationQuotation_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationQuotation_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationQuotation_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationQuotation_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationLeadMgmt_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationLeadMgmt_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationLeadMgmt_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationLeadMgmt_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationLeadMgmt_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_Update]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationContactMgmt_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationContactMgmt_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationContactMgmt_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_Insert]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationContactMgmt_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_Delete]    Script Date: 24-07-2017 13:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OperationContactMgmt_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''OperationContactMgmt''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OperationContactMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''OperationContactMgmt''
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OperationContactMgmt]
(
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@sCompany,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sStatus,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''OperationContactMgmt''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationContactMgmt]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''OperationContactMgmt''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationContactMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationContactMgmt_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationContactMgmt_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''OperationContactMgmt''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationContactMgmt_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OperationContactMgmt]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[Status] = @sStatus,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''OperationLeadMgmt''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OperationLeadMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''OperationLeadMgmt''
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OperationLeadMgmt]
(
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@iCampaignID,
	@sTourType,
	@sCompany,
	@sTitle,
	@sGender,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sPreferredContactTime,
	@sPreferredContactMethod,
	@daPlannedDepartureDate,
	@iHowManyDays,
	@iHowManyPeople,
	@iHowManyChild,
	@sPlaceOfDeparture,
	@sPlaceOfReturn,
	@dcTotalBudget,
	@sCurrency,
	@sTourGuide,
	@bFlightsNeeded,
	@sVehicle,
	@sHotelType,
	@sMeals,
	@sAdmissionTickets,
	@sInsurance,
	@sLeadSource,
	@sLeadOwner,
	@sLeadStatus,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''OperationLeadMgmt''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationLeadMgmt]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''OperationLeadMgmt''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationLeadMgmt]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationLeadMgmt_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationLeadMgmt_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''OperationLeadMgmt''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationLeadMgmt_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OperationLeadMgmt]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[CampaignID] = @iCampaignID,
	[TourType] = @sTourType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[Gender] = @sGender,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[PreferredContactTime] = @sPreferredContactTime,
	[PreferredContactMethod] = @sPreferredContactMethod,
	[PlannedDepartureDate] = @daPlannedDepartureDate,
	[HowManyDays] = @iHowManyDays,
	[HowManyPeople] = @iHowManyPeople,
	[HowManyChild] = @iHowManyChild,
	[PlaceOfDeparture] = @sPlaceOfDeparture,
	[PlaceOfReturn] = @sPlaceOfReturn,
	[TotalBudget] = @dcTotalBudget,
	[Currency] = @sCurrency,
	[TourGuide] = @sTourGuide,
	[FlightsNeeded] = @bFlightsNeeded,
	[Vehicle] = @sVehicle,
	[HotelType] = @sHotelType,
	[Meals] = @sMeals,
	[AdmissionTickets] = @sAdmissionTickets,
	[Insurance] = @sInsurance,
	[LeadSource] = @sLeadSource,
	[LeadOwner] = @sLeadOwner,
	[LeadStatus] = @sLeadStatus,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''OperationQuotation''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationQuotation_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON


declare @qpid int, @qbasicId int
select @qpid = id from [dbo].[QuotationPrice] where QuotationId= @iID
--select basic ID
select @qbasicId =id FROM [dbo].[QuotationPrice_Basic] where QuotationPriceId= @qpid

DELETE FROM [dbo].QuotationPrice_Coach where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_Dinner where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_DriveGuide where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_Entrance where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_Hotel where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_Lunch where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_OtherServices where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_Tips where QuotationPriceBasicId= @qbasicId
DELETE FROM [dbo].QuotationPrice_VanHire where QuotationPriceBasicId= @qbasicId

DELETE FROM [dbo].QuotationPrice_Overview where QuotationPriceId= @qbasicId

-- Basic master
DELETE FROM [dbo].[QuotationPrice_Basic] where QuotationPriceId= @qpid

DELETE FROM [dbo].[QuotationPrice] where QuotationId= @iID

DELETE FROM [dbo].Quotation_Summary where QuotationId= @iID

-- DELETE an existing row from the table.
DELETE FROM [dbo].[OperationQuotation]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''OperationQuotation''
-- Gets: @iCompanyID int
-- Gets: @sTourCode nvarchar(50)
-- Gets: @daQuotationDate datetime
-- Gets: @iStaffId int
-- Gets: @iInquiryStaffId int
-- Gets: @iCompanyIndividual int
-- Gets: @sClient_City nvarchar(100)
-- Gets: @sClient_Country nvarchar(100)
-- Gets: @sClient_Telephone nvarchar(15)
-- Gets: @sClient_MobileNumber nvarchar(15)
-- Gets: @sClient_Fax nvarchar(15)
-- Gets: @sClient_WeChat nvarchar(25)
-- Gets: @sClient_Email nvarchar(50)
-- Gets: @iSalesId int
-- Gets: @sQuotationStaff nvarchar(50)
-- Gets: @iDepartmentID int
-- Gets: @sOffice_City nvarchar(100)
-- Gets: @sOffice_Country nvarchar(100)
-- Gets: @sOffice_Telephone nvarchar(15)
-- Gets: @sOffice_MobileNumber nvarchar(15)
-- Gets: @sOffice_Fax nvarchar(15)
-- Gets: @sOffice_WeChat nvarchar(25)
-- Gets: @sOffice_Email nvarchar(50)
-- Gets: @daTourStartDate datetime
-- Gets: @daTourEndDate datetime
-- Gets: @iNoOfServiceDay int
-- Gets: @iTotalClient int
-- Gets: @iNoOfAdults int
-- Gets: @iNoOfChild5To12 int
-- Gets: @iNoOfChild2To5 int
-- Gets: @iNoOfInfant1To2 int
-- Gets: @sComments nvarchar(Max)
-- Gets: @sMessageToClient nvarchar(Max)
-- Gets: @sQuotationStatus nvarchar(100)
-- Gets: @sQuotationReport nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationQuotation_Insert]
	@iCompanyID int,
	@sTourCode nvarchar(50),
	@daQuotationDate datetime,
	@iStaffId int,
	@iInquiryStaffId int,
	@iCompanyIndividual int,
	@sClient_City nvarchar(100),
	@sClient_Country nvarchar(100),
	@sClient_Telephone nvarchar(15),
	@sClient_MobileNumber nvarchar(15),
	@sClient_Fax nvarchar(15),
	@sClient_WeChat nvarchar(25),
	@sClient_Email nvarchar(50),
	@iSalesId int,
	@sQuotationStaff nvarchar(50),
	@iDepartmentID int,
	@sOffice_City nvarchar(100),
	@sOffice_Country nvarchar(100),
	@sOffice_Telephone nvarchar(15),
	@sOffice_MobileNumber nvarchar(15),
	@sOffice_Fax nvarchar(15),
	@sOffice_WeChat nvarchar(25),
	@sOffice_Email nvarchar(50),
	@daTourStartDate datetime,
	@daTourEndDate datetime,
	@iNoOfServiceDay int,
	@iTotalClient int,
	@iNoOfAdults int,
	@iNoOfChild5To12 int,
	@iNoOfChild2To5 int,
	@iNoOfInfant1To2 int,
	@sComments nvarchar(Max),
	@sMessageToClient nvarchar(Max),
	@sQuotationStatus nvarchar(100),
	@sQuotationReport nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OperationQuotation]
(
	[CompanyID],
	[TourCode],
	[QuotationDate],
	[StaffId],
	[InquiryStaffId],
	[CompanyIndividual],
	[Client_City],
	[Client_Country],
	[Client_Telephone],
	[Client_MobileNumber],
	[Client_Fax],
	[Client_WeChat],
	[Client_Email],
	[SalesId],
	[QuotationStaff],
	[DepartmentID],
	[Office_City],
	[Office_Country],
	[Office_Telephone],
	[Office_MobileNumber],
	[Office_Fax],
	[Office_WeChat],
	[Office_Email],
	[TourStartDate],
	[TourEndDate],
	[NoOfServiceDay],
	[TotalClient],
	[NoOfAdults],
	[NoOfChild5To12],
	[NoOfChild2To5],
	[NoOfInfant1To2],
	[Comments],
	[MessageToClient],
	[QuotationStatus],
	[QuotationReport],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sTourCode,
	@daQuotationDate,
	@iStaffId,
	@iInquiryStaffId,
	@iCompanyIndividual,
	@sClient_City,
	@sClient_Country,
	@sClient_Telephone,
	@sClient_MobileNumber,
	@sClient_Fax,
	@sClient_WeChat,
	@sClient_Email,
	@iSalesId,
	@sQuotationStaff,
	@iDepartmentID,
	@sOffice_City,
	@sOffice_Country,
	@sOffice_Telephone,
	@sOffice_MobileNumber,
	@sOffice_Fax,
	@sOffice_WeChat,
	@sOffice_Email,
	@daTourStartDate,
	@daTourEndDate,
	@iNoOfServiceDay,
	@iTotalClient,
	@iNoOfAdults,
	@iNoOfChild5To12,
	@iNoOfChild2To5,
	@iNoOfInfant1To2,
	@sComments,
	@sMessageToClient,
	@sQuotationStatus,
	@sQuotationReport,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''OperationQuotation''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationQuotation_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[TourCode],
	[QuotationDate],
	[StaffId],
	[InquiryStaffId],
	[CompanyIndividual],
	[Client_City],
	[Client_Country],
	[Client_Telephone],
	[Client_MobileNumber],
	[Client_Fax],
	[Client_WeChat],
	[Client_Email],
	[SalesId],
	[QuotationStaff],
	[DepartmentID],
	[Office_City],
	[Office_Country],
	[Office_Telephone],
	[Office_MobileNumber],
	[Office_Fax],
	[Office_WeChat],
	[Office_Email],
	[TourStartDate],
	[TourEndDate],
	[NoOfServiceDay],
	[TotalClient],
	[NoOfAdults],
	[NoOfChild5To12],
	[NoOfChild2To5],
	[NoOfInfant1To2],
	[Comments],
	[MessageToClient],
	[QuotationStatus],
	[QuotationReport],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationQuotation]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''OperationQuotation''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationQuotation_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[TourCode],
	[QuotationDate],
	[StaffId],
	[InquiryStaffId],
	[CompanyIndividual],
	[Client_City],
	[Client_Country],
	[Client_Telephone],
	[Client_MobileNumber],
	[Client_Fax],
	[Client_WeChat],
	[Client_Email],
	[SalesId],
	[QuotationStaff],
	[DepartmentID],
	[Office_City],
	[Office_Country],
	[Office_Telephone],
	[Office_MobileNumber],
	[Office_Fax],
	[Office_WeChat],
	[Office_Email],
	[TourStartDate],
	[TourEndDate],
	[NoOfServiceDay],
	[TotalClient],
	[NoOfAdults],
	[NoOfChild5To12],
	[NoOfChild2To5],
	[NoOfInfant1To2],
	[Comments],
	[MessageToClient],
	[QuotationStatus],
	[QuotationReport],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OperationQuotation]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OperationQuotation_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OperationQuotation_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''OperationQuotation''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sTourCode nvarchar(50)
-- Gets: @daQuotationDate datetime
-- Gets: @iStaffId int
-- Gets: @iInquiryStaffId int
-- Gets: @iCompanyIndividual int
-- Gets: @sClient_City nvarchar(100)
-- Gets: @sClient_Country nvarchar(100)
-- Gets: @sClient_Telephone nvarchar(15)
-- Gets: @sClient_MobileNumber nvarchar(15)
-- Gets: @sClient_Fax nvarchar(15)
-- Gets: @sClient_WeChat nvarchar(25)
-- Gets: @sClient_Email nvarchar(50)
-- Gets: @iSalesId int
-- Gets: @sQuotationStaff nvarchar(50)
-- Gets: @iDepartmentID int
-- Gets: @sOffice_City nvarchar(100)
-- Gets: @sOffice_Country nvarchar(100)
-- Gets: @sOffice_Telephone nvarchar(15)
-- Gets: @sOffice_MobileNumber nvarchar(15)
-- Gets: @sOffice_Fax nvarchar(15)
-- Gets: @sOffice_WeChat nvarchar(25)
-- Gets: @sOffice_Email nvarchar(50)
-- Gets: @daTourStartDate datetime
-- Gets: @daTourEndDate datetime
-- Gets: @iNoOfServiceDay int
-- Gets: @iTotalClient int
-- Gets: @iNoOfAdults int
-- Gets: @iNoOfChild5To12 int
-- Gets: @iNoOfChild2To5 int
-- Gets: @iNoOfInfant1To2 int
-- Gets: @sComments nvarchar(Max)
-- Gets: @sMessageToClient nvarchar(Max)
-- Gets: @sQuotationStatus nvarchar(100)
-- Gets: @sQuotationReport nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OperationQuotation_Update]
	@iID int,
	@iCompanyID int,
	@sTourCode nvarchar(50),
	@daQuotationDate datetime,
	@iStaffId int,
	@iInquiryStaffId int,
	@iCompanyIndividual int,
	@sClient_City nvarchar(100),
	@sClient_Country nvarchar(100),
	@sClient_Telephone nvarchar(15),
	@sClient_MobileNumber nvarchar(15),
	@sClient_Fax nvarchar(15),
	@sClient_WeChat nvarchar(25),
	@sClient_Email nvarchar(50),
	@iSalesId int,
	@sQuotationStaff nvarchar(50),
	@iDepartmentID int,
	@sOffice_City nvarchar(100),
	@sOffice_Country nvarchar(100),
	@sOffice_Telephone nvarchar(15),
	@sOffice_MobileNumber nvarchar(15),
	@sOffice_Fax nvarchar(15),
	@sOffice_WeChat nvarchar(25),
	@sOffice_Email nvarchar(50),
	@daTourStartDate datetime,
	@daTourEndDate datetime,
	@iNoOfServiceDay int,
	@iTotalClient int,
	@iNoOfAdults int,
	@iNoOfChild5To12 int,
	@iNoOfChild2To5 int,
	@iNoOfInfant1To2 int,
	@sComments nvarchar(Max),
	@sMessageToClient nvarchar(Max),
	@sQuotationStatus nvarchar(100),
	@sQuotationReport nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OperationQuotation]
SET 
	[CompanyID] = @iCompanyID,
	[TourCode] = @sTourCode,
	[QuotationDate] = @daQuotationDate,
	[StaffId] = @iStaffId,
	[InquiryStaffId] = @iInquiryStaffId,
	[CompanyIndividual] = @iCompanyIndividual,
	[Client_City] = @sClient_City,
	[Client_Country] = @sClient_Country,
	[Client_Telephone] = @sClient_Telephone,
	[Client_MobileNumber] = @sClient_MobileNumber,
	[Client_Fax] = @sClient_Fax,
	[Client_WeChat] = @sClient_WeChat,
	[Client_Email] = @sClient_Email,
	[SalesId] = @iSalesId,
	[QuotationStaff] = @sQuotationStaff,
	[DepartmentID] = @iDepartmentID,
	[Office_City] = @sOffice_City,
	[Office_Country] = @sOffice_Country,
	[Office_Telephone] = @sOffice_Telephone,
	[Office_MobileNumber] = @sOffice_MobileNumber,
	[Office_Fax] = @sOffice_Fax,
	[Office_WeChat] = @sOffice_WeChat,
	[Office_Email] = @sOffice_Email,
	[TourStartDate] = @daTourStartDate,
	[TourEndDate] = @daTourEndDate,
	[NoOfServiceDay] = @iNoOfServiceDay,
	[TotalClient] = @iTotalClient,
	[NoOfAdults] = @iNoOfAdults,
	[NoOfChild5To12] = @iNoOfChild5To12,
	[NoOfChild2To5] = @iNoOfChild2To5,
	[NoOfInfant1To2] = @iNoOfInfant1To2,
	[Comments] = @sComments,
	[MessageToClient] = @sMessageToClient,
	[QuotationStatus] = @sQuotationStatus,
	[QuotationReport] = @sQuotationReport,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''Quotation_Summary''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[Quotation_Summary]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_DeleteAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_DeleteAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''Quotation_Summary''
-- based on a foreign key field.
-- Gets: @iQuotationId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_DeleteAllWQuotationIdLogic]
	@iQuotationId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[Quotation_Summary]
WHERE
	[QuotationId] = @iQuotationId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''Quotation_Summary''
-- Gets: @iQuotationId int
-- Gets: @iNoOfDays int
-- Gets: @daQuotationDate datetime
-- Gets: @sTourCity nvarchar(100)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @dcMarkupPercentage decimal(10, 2)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @dcTotalAmount decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @iNoOfAdult int
-- Gets: @iNoOfChild int
-- Gets: @iNoOfInfants int
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_Insert]
	@iQuotationId int,
	@iNoOfDays int,
	@daQuotationDate datetime,
	@sTourCity nvarchar(100),
	@sQuotationCurrency nvarchar(100),
	@dcMarkupPercentage decimal(10, 2),
	@sConfirmationStatus nvarchar(100),
	@dcTotalAmount decimal(10, 2),
	@iNoOfPeople int,
	@iNoOfAdult int,
	@iNoOfChild int,
	@iNoOfInfants int,
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[Quotation_Summary]
(
	[QuotationId],
	[NoOfDays],
	[QuotationDate],
	[TourCity],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[TotalAmount],
	[NoOfPeople],
	[NoOfAdult],
	[NoOfChild],
	[NoOfInfants],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationId,
	@iNoOfDays,
	@daQuotationDate,
	@sTourCity,
	@sQuotationCurrency,
	@dcMarkupPercentage,
	@sConfirmationStatus,
	@dcTotalAmount,
	@iNoOfPeople,
	@iNoOfAdult,
	@iNoOfChild,
	@iNoOfInfants,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''Quotation_Summary''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationId],
	[NoOfDays],
	[QuotationDate],
	[TourCity],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[TotalAmount],
	[NoOfPeople],
	[NoOfAdult],
	[NoOfChild],
	[NoOfInfants],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Quotation_Summary]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_SelectAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_SelectAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''Quotation_Summary''
-- based on a foreign key field.
-- Gets: @iQuotationId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_SelectAllWQuotationIdLogic]
	@iQuotationId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationId],
	[NoOfDays],
	[QuotationDate],
	[TourCity],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[TotalAmount],
	[NoOfPeople],
	[NoOfAdult],
	[NoOfChild],
	[NoOfInfants],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Quotation_Summary]
WHERE
	[QuotationId] = @iQuotationId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''Quotation_Summary''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationId],
	[NoOfDays],
	[QuotationDate],
	[TourCity],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[TotalAmount],
	[NoOfPeople],
	[NoOfAdult],
	[NoOfChild],
	[NoOfInfants],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Quotation_Summary]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''Quotation_Summary''
-- Gets: @iID int
-- Gets: @iQuotationId int
-- Gets: @iNoOfDays int
-- Gets: @daQuotationDate datetime
-- Gets: @sTourCity nvarchar(100)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @dcMarkupPercentage decimal(10, 2)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @dcTotalAmount decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @iNoOfAdult int
-- Gets: @iNoOfChild int
-- Gets: @iNoOfInfants int
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_Update]
	@iID int,
	@iQuotationId int,
	@iNoOfDays int,
	@daQuotationDate datetime,
	@sTourCity nvarchar(100),
	@sQuotationCurrency nvarchar(100),
	@dcMarkupPercentage decimal(10, 2),
	@sConfirmationStatus nvarchar(100),
	@dcTotalAmount decimal(10, 2),
	@iNoOfPeople int,
	@iNoOfAdult int,
	@iNoOfChild int,
	@iNoOfInfants int,
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Quotation_Summary]
SET 
	[QuotationId] = @iQuotationId,
	[NoOfDays] = @iNoOfDays,
	[QuotationDate] = @daQuotationDate,
	[TourCity] = @sTourCity,
	[QuotationCurrency] = @sQuotationCurrency,
	[MarkupPercentage] = @dcMarkupPercentage,
	[ConfirmationStatus] = @sConfirmationStatus,
	[TotalAmount] = @dcTotalAmount,
	[NoOfPeople] = @iNoOfPeople,
	[NoOfAdult] = @iNoOfAdult,
	[NoOfChild] = @iNoOfChild,
	[NoOfInfants] = @iNoOfInfants,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Quotation_Summary_UpdateAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Quotation_Summary_UpdateAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''Quotation_Summary''.
-- Will reset field [QuotationId] with value @iQuotationIdOld  to value @iQuotationId
-- Gets: @iQuotationId int
-- Gets: @iQuotationIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Quotation_Summary_UpdateAllWQuotationIdLogic]
	@iQuotationId int,
	@iQuotationIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Quotation_Summary]
SET
	[QuotationId] = @iQuotationId
WHERE
	[QuotationId] = @iQuotationIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Basic''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Basic]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_DeleteAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_DeleteAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Basic''
-- based on a foreign key field.
-- Gets: @iQuotationPriceId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_DeleteAllWQuotationPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Basic]
WHERE
	[QuotationPriceId] = @iQuotationPriceId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Basic''
-- Gets: @iQuotationPriceId int
-- Gets: @iNoOfDay int
-- Gets: @daTourDate datetime
-- Gets: @sTourCity nvarchar(100)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @dcMarkupPercentage decimal(10, 2)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_Insert]
	@iQuotationPriceId int,
	@iNoOfDay int,
	@daTourDate datetime,
	@sTourCity nvarchar(100),
	@sQuotationCurrency nvarchar(100),
	@dcMarkupPercentage decimal(10, 2),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Basic]
(
	[QuotationPriceId],
	[NoOfDay],
	[TourDate],
	[TourCity],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceId,
	@iNoOfDay,
	@daTourDate,
	@sTourCity,
	@sQuotationCurrency,
	@dcMarkupPercentage,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Basic''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceId],
	[NoOfDay],
	[TourDate],
	[TourCity],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Basic]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_SelectAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_SelectAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Basic''
-- based on a foreign key field.
-- Gets: @iQuotationPriceId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_SelectAllWQuotationPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	QB.[ID],
	--case when QB.[ID] is null then 0 else qb.ID end as ID,
	QB.[QuotationPriceId],
	QB.[NoOfDay],
	QB.[TourDate],
	QB.[TourCity], 
	QB.MarkupPercentage,
	QB.QuotationCurrency,
	--case when QB.[MarkupPercentage] is null  then QP.MarkupPercentage else Qb.MarkupPercentage end  as MarkupPercentage,
	--case when QB.QuotationCurrency is null  then QP.QuotationCurrency else Qb.QuotationCurrency end  as QuotationCurrency ,
	QB.[ConfirmationStatus],
	QB.[CreatedBy],
	QB.[CreatedDate],
	QB.[UpdatedBy],
	QB.[UpdatedDate]
FROM 
[dbo].[QuotationPrice_Basic] QB 
--dbo.[QuotationPrice] QP
--LEFT OUTER join [dbo].[QuotationPrice_Basic] QB on QP.Id=QB.QuotationPriceId
WHERE
--QP.ID=@iQuotationPriceId
	[QuotationPriceId] = @iQuotationPriceId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR

' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Basic''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceId],
	[NoOfDay],
	[TourDate],
	[TourCity],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Basic]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Basic''
-- Gets: @iID int
-- Gets: @iQuotationPriceId int
-- Gets: @iNoOfDay int
-- Gets: @daTourDate datetime
-- Gets: @sTourCity nvarchar(100)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @dcMarkupPercentage decimal(10, 2)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_Update]
	@iID int,
	@iQuotationPriceId int,
	@iNoOfDay int,
	@daTourDate datetime,
	@sTourCity nvarchar(100),
	@sQuotationCurrency nvarchar(100),
	@dcMarkupPercentage decimal(10, 2),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Basic]
SET 
	[QuotationPriceId] = @iQuotationPriceId,
	[NoOfDay] = @iNoOfDay,
	[TourDate] = @daTourDate,
	[TourCity] = @sTourCity,
	[QuotationCurrency] = @sQuotationCurrency,
	[MarkupPercentage] = @dcMarkupPercentage,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Basic_UpdateAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Basic_UpdateAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Basic''.
-- Will reset field [QuotationPriceId] with value @iQuotationPriceIdOld  to value @iQuotationPriceId
-- Gets: @iQuotationPriceId int
-- Gets: @iQuotationPriceIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Basic_UpdateAllWQuotationPriceIdLogic]
	@iQuotationPriceId int,
	@iQuotationPriceIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Basic]
SET
	[QuotationPriceId] = @iQuotationPriceId
WHERE
	[QuotationPriceId] = @iQuotationPriceIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Coach''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Coach]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Coach''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Coach]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Coach''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfCoach nvarchar(100)
-- Gets: @dcCoachRatePerDay decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_Insert]
	@iQuotationPriceBasicId int,
	@sTypeOfCoach nvarchar(100),
	@dcCoachRatePerDay decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Coach]
(
	[QuotationPriceBasicId],
	[TypeOfCoach],
	[CoachRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@sTypeOfCoach,
	@dcCoachRatePerDay,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Coach''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfCoach],
	[CoachRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Coach]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Coach''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfCoach],
	[CoachRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Coach]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Coach''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	 QT.ID
	,QT.QuotationPriceBasicId
	,QT.TypeOfCoach
	,QT.CoachRatePerDay
	,QT.NetRateInTotal
	,QT.MarkUpPercentage
	,QT.GrossRateInTotal
	,QT.Profit
	,QT.Remark
	,QT.QuotationCurrency
	,QT.ConfirmationStatus
	,QT.CreatedBy
	,QT.CreatedDate
	,QT.UpdatedBy
	,QT.UpdatedDate,


	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QT.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].QuotationPrice_Coach QT on  QB.ID = QT.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
    
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Coach''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfCoach],
	[CoachRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Coach]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Coach''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfCoach nvarchar(100)
-- Gets: @dcCoachRatePerDay decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@sTypeOfCoach nvarchar(100),
	@dcCoachRatePerDay decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Coach]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[TypeOfCoach] = @sTypeOfCoach,
	[CoachRatePerDay] = @dcCoachRatePerDay,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Coach_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Coach_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Coach''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Coach_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Coach]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DeleteAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DeleteAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice''
-- based on a foreign key field.
-- Gets: @iQuotationId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DeleteAllWQuotationIdLogic]
	@iQuotationId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice]
WHERE
	[QuotationId] = @iQuotationId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Dinner''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Dinner]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Dinner''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Dinner]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Dinner''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfMeal nvarchar(100)
-- Gets: @dcMealPerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcMealSupplementForGuid decimal(10, 2)
-- Gets: @iNoOfTourGuidePeople int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_Insert]
	@iQuotationPriceBasicId int,
	@sTypeOfMeal nvarchar(100),
	@dcMealPerPerson decimal(10, 2),
	@iNoOfPeople int,
	@dcMealSupplementForGuid decimal(10, 2),
	@iNoOfTourGuidePeople int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Dinner]
(
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@sTypeOfMeal,
	@dcMealPerPerson,
	@iNoOfPeople,
	@dcMealSupplementForGuid,
	@iNoOfTourGuidePeople,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Dinner''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Dinner]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Dinner''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Dinner]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Dinner''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CReate PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	QT.[ID],
	QT.[QuotationPriceBasicId],
	QT.[TypeOfMeal],
	QT.[MealPerPerson],
	QT.[NoOfPeople],
	QT.[MealSupplementForGuid],
	QT.[NoOfTourGuidePeople],
	QT.[NetRateInTotal],
	QT.[MarkUpPercentage],
	QT.[GrossRateInTotal],
	QT.[Profit],
	QT.[Remark],
	QT.[QuotationCurrency],
	QT.[ConfirmationStatus],
	QT.[CreatedBy],
	QT.[CreatedDate],
	QT.[UpdatedBy],
	QT.[UpdatedDate],
 

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QT.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].[QuotationPrice_Dinner] QT on  QB.ID = QT.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
  
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Dinner''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Dinner]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Dinner''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfMeal nvarchar(100)
-- Gets: @dcMealPerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcMealSupplementForGuid decimal(10, 2)
-- Gets: @iNoOfTourGuidePeople int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@sTypeOfMeal nvarchar(100),
	@dcMealPerPerson decimal(10, 2),
	@iNoOfPeople int,
	@dcMealSupplementForGuid decimal(10, 2),
	@iNoOfTourGuidePeople int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Dinner]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[TypeOfMeal] = @sTypeOfMeal,
	[MealPerPerson] = @dcMealPerPerson,
	[NoOfPeople] = @iNoOfPeople,
	[MealSupplementForGuid] = @dcMealSupplementForGuid,
	[NoOfTourGuidePeople] = @iNoOfTourGuidePeople,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Dinner_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Dinner_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Dinner''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Dinner_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Dinner]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_DriveGuide''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_DriveGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_DriveGuide''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_DriveGuide]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_DriveGuide''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @dcRatePerRoom decimal(10, 2)
-- Gets: @iNoOfRoom int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_Insert]
	@iQuotationPriceBasicId int,
	@dcRatePerRoom decimal(10, 2),
	@iNoOfRoom int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_DriveGuide]
(
	[QuotationPriceBasicId],
	[RatePerRoom],
	[NoOfRoom],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@dcRatePerRoom,
	@iNoOfRoom,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_DriveGuide''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[RatePerRoom],
	[NoOfRoom],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_DriveGuide]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_DriveGuide''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[RatePerRoom],
	[NoOfRoom],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_DriveGuide]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_DriveGuide''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.

;WITH CTE AS( 
SELECT
	QT.[ID],
	QT.[QuotationPriceBasicId],
	QT.[RatePerRoom],
	QT.[NoOfRoom],
	QT.[NetRateInTotal],
	QT.[MarkUpPercentage],
	QT.[GrossRateInTotal],
	QT.[Profit],
	QT.[Remark],
	QT.[QuotationCurrency],
	QT.[ConfirmationStatus],
	QT.[CreatedBy],
	QT.[CreatedDate],
	QT.[UpdatedBy],
	QT.[UpdatedDate],

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QT.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].[QuotationPrice_DriveGuide] QT on  QB.ID = QT.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_DriveGuide''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[RatePerRoom],
	[NoOfRoom],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_DriveGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_DriveGuide''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @dcRatePerRoom decimal(10, 2)
-- Gets: @iNoOfRoom int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@dcRatePerRoom decimal(10, 2),
	@iNoOfRoom int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_DriveGuide]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[RatePerRoom] = @dcRatePerRoom,
	[NoOfRoom] = @iNoOfRoom,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_DriveGuide_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_DriveGuide_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_DriveGuide''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_DriveGuide_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_DriveGuide]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Entrance''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Entrance]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Entrance''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Entrance]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Entrance''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sEnteranceName nvarchar(150)
-- Gets: @dcAdultPrice decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcChildPrice decimal(10, 2)
-- Gets: @iNoOfChild int
-- Gets: @dcConcession_student_elderly decimal(10, 2)
-- Gets: @iNoOfStudentForConcession int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_Insert]
	@iQuotationPriceBasicId int,
	@sEnteranceName nvarchar(150),
	@dcAdultPrice decimal(10, 2),
	@iNoOfPeople int,
	@dcChildPrice decimal(10, 2),
	@iNoOfChild int,
	@dcConcession_student_elderly decimal(10, 2),
	@iNoOfStudentForConcession int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Entrance]
(
	[QuotationPriceBasicId],
	[EnteranceName],
	[AdultPrice],
	[NoOfPeople],
	[ChildPrice],
	[NoOfChild],
	[Concession_student_elderly],
	[NoOfStudentForConcession],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@sEnteranceName,
	@dcAdultPrice,
	@iNoOfPeople,
	@dcChildPrice,
	@iNoOfChild,
	@dcConcession_student_elderly,
	@iNoOfStudentForConcession,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Entrance''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[EnteranceName],
	[AdultPrice],
	[NoOfPeople],
	[ChildPrice],
	[NoOfChild],
	[Concession_student_elderly],
	[NoOfStudentForConcession],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Entrance]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Entrance''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[EnteranceName],
	[AdultPrice],
	[NoOfPeople],
	[ChildPrice],
	[NoOfChild],
	[Concession_student_elderly],
	[NoOfStudentForConcession],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Entrance]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Entrance''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
create PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.


;WITH CTE AS( 
SELECT
	QE.[ID],
	QE.[QuotationPriceBasicId],
	QE.[EnteranceName],
	QE.[AdultPrice],
	QE.[NoOfPeople],
	QE.[ChildPrice],
	QE.[NoOfChild],
	QE.[Concession_student_elderly],
	QE.[NoOfStudentForConcession],
	QE.[NetRateInTotal],
	QE.[MarkUpPercentage],
	QE.[GrossRateInTotal],
	QE.[Profit],
	QE.[Remark],
	QE.[QuotationCurrency],
	QE.[ConfirmationStatus],
	QE.[CreatedBy],
	QE.[CreatedDate],
	QE.[UpdatedBy],
	QE.[UpdatedDate],

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QE.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].[QuotationPrice_Entrance] QE on  QB.ID = QE.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
 
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Entrance''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[EnteranceName],
	[AdultPrice],
	[NoOfPeople],
	[ChildPrice],
	[NoOfChild],
	[Concession_student_elderly],
	[NoOfStudentForConcession],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Entrance]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Entrance''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sEnteranceName nvarchar(150)
-- Gets: @dcAdultPrice decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcChildPrice decimal(10, 2)
-- Gets: @iNoOfChild int
-- Gets: @dcConcession_student_elderly decimal(10, 2)
-- Gets: @iNoOfStudentForConcession int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@sEnteranceName nvarchar(150),
	@dcAdultPrice decimal(10, 2),
	@iNoOfPeople int,
	@dcChildPrice decimal(10, 2),
	@iNoOfChild int,
	@dcConcession_student_elderly decimal(10, 2),
	@iNoOfStudentForConcession int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Entrance]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[EnteranceName] = @sEnteranceName,
	[AdultPrice] = @dcAdultPrice,
	[NoOfPeople] = @iNoOfPeople,
	[ChildPrice] = @dcChildPrice,
	[NoOfChild] = @iNoOfChild,
	[Concession_student_elderly] = @dcConcession_student_elderly,
	[NoOfStudentForConcession] = @iNoOfStudentForConcession,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Entrance_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Entrance_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Entrance''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Entrance_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Entrance]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Hotel''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Hotel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Hotel''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Hotel]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Hotel''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iStartHotel int
-- Gets: @sRecommendedHotel nvarchar(200)
-- Gets: @dcTwinPerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcSingleRoomSupplementPerPerson decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRatePerPerson decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_Insert]
	@iQuotationPriceBasicId int,
	@iStartHotel int,
	@sRecommendedHotel nvarchar(200),
	@dcTwinPerPerson decimal(10, 2),
	@iNoOfPeople int,
	@dcSingleRoomSupplementPerPerson decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRatePerPerson decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Hotel]
(
	[QuotationPriceBasicId],
	[StartHotel],
	[RecommendedHotel],
	[TwinPerPerson],
	[NoOfPeople],
	[SingleRoomSupplementPerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@iStartHotel,
	@sRecommendedHotel,
	@dcTwinPerPerson,
	@iNoOfPeople,
	@dcSingleRoomSupplementPerPerson,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRatePerPerson,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Hotel''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[StartHotel],
	[RecommendedHotel],
	[TwinPerPerson],
	[NoOfPeople],
	[SingleRoomSupplementPerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Hotel]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Hotel''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[StartHotel],
	[RecommendedHotel],
	[TwinPerPerson],
	[NoOfPeople],
	[SingleRoomSupplementPerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Hotel]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Hotel''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectAllWQuotationPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	QH.[ID],
	QH.[QuotationPriceBasicId],
	QH.[StartHotel],
	QH.[RecommendedHotel],
	QH.[TwinPerPerson],
	QH.[NoOfPeople],
	QH.[SingleRoomSupplementPerPerson],
	QH.[NetRateInTotal],
	QH.[MarkUpPercentage],
	QH.[GrossRatePerPerson],
	QH.[GrossRateInTotal],
	QH.[Profit],
	QH.[Remark],
	QH.[QuotationCurrency],
	QH.[ConfirmationStatus],
	QH.[CreatedBy],
	QH.[CreatedDate],
	QH.[UpdatedBy],
	QH.[UpdatedDate],

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QH.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].[QuotationPrice_Hotel] QH on  QB.ID = QH.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
 
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR

' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Hotel''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[StartHotel],
	[RecommendedHotel],
	[TwinPerPerson],
	[NoOfPeople],
	[SingleRoomSupplementPerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Hotel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Hotel''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iStartHotel int
-- Gets: @sRecommendedHotel nvarchar(200)
-- Gets: @dcTwinPerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcSingleRoomSupplementPerPerson decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRatePerPerson decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@iStartHotel int,
	@sRecommendedHotel nvarchar(200),
	@dcTwinPerPerson decimal(10, 2),
	@iNoOfPeople int,
	@dcSingleRoomSupplementPerPerson decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRatePerPerson decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Hotel]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[StartHotel] = @iStartHotel,
	[RecommendedHotel] = @sRecommendedHotel,
	[TwinPerPerson] = @dcTwinPerPerson,
	[NoOfPeople] = @iNoOfPeople,
	[SingleRoomSupplementPerPerson] = @dcSingleRoomSupplementPerPerson,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRatePerPerson] = @dcGrossRatePerPerson,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Hotel_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Hotel_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Hotel''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Hotel_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Hotel]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice''
-- Gets: @iQuotationId int
-- Gets: @sTourCode nvarchar(50)
-- Gets: @iQuotationStaff int
-- Gets: @daQuotationDate datetime
-- Gets: @iNoOfPeople int
-- Gets: @sConfirmation nvarchar(100)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @dcMarkupPercentage decimal(10, 2)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Insert]
	@iQuotationId int,
	@sTourCode nvarchar(50),
	@iQuotationStaff int,
	@daQuotationDate datetime,
	@iNoOfPeople int,
	@sConfirmation nvarchar(100),
	@sQuotationCurrency nvarchar(100),
	@dcMarkupPercentage decimal(10, 2),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice]
(
	[QuotationId],
	[TourCode],
	[QuotationStaff],
	[QuotationDate],
	[NoOfPeople],
	[Confirmation],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationId,
	@sTourCode,
	@iQuotationStaff,
	@daQuotationDate,
	@iNoOfPeople,
	@sConfirmation,
	@sQuotationCurrency,
	@dcMarkupPercentage,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Lunch''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Lunch]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Lunch''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Lunch]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Lunch''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfMeal nvarchar(100)
-- Gets: @dcMealPerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcMealSupplementForGuid decimal(10, 2)
-- Gets: @iNoOfTourGuidePeople int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_Insert]
	@iQuotationPriceBasicId int,
	@sTypeOfMeal nvarchar(100),
	@dcMealPerPerson decimal(10, 2),
	@iNoOfPeople int,
	@dcMealSupplementForGuid decimal(10, 2),
	@iNoOfTourGuidePeople int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Lunch]
(
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@sTypeOfMeal,
	@dcMealPerPerson,
	@iNoOfPeople,
	@dcMealSupplementForGuid,
	@iNoOfTourGuidePeople,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Lunch''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Lunch]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Lunch''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Lunch]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Lunch''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	  
	QT.[ID],
	QT.[QuotationPriceBasicId],
	QT.[TypeOfMeal],
	QT.[MealPerPerson],
	QT.[NoOfPeople],
	QT.[MealSupplementForGuid],
	QT.[NoOfTourGuidePeople],
	QT.[NetRateInTotal],
	QT.[MarkUpPercentage],
	QT.[GrossRateInTotal],
	QT.[Profit],
	QT.[Remark],
	QT.[QuotationCurrency],
	QT.[ConfirmationStatus],
	QT.[CreatedBy],
	QT.[CreatedDate],
	QT.[UpdatedBy],
	QT.[UpdatedDate],

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QT.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].[QuotationPrice_Lunch] QT on  QB.ID = QT.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
    
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Lunch''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfMeal],
	[MealPerPerson],
	[NoOfPeople],
	[MealSupplementForGuid],
	[NoOfTourGuidePeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Lunch]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Lunch''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfMeal nvarchar(100)
-- Gets: @dcMealPerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @dcMealSupplementForGuid decimal(10, 2)
-- Gets: @iNoOfTourGuidePeople int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@sTypeOfMeal nvarchar(100),
	@dcMealPerPerson decimal(10, 2),
	@iNoOfPeople int,
	@dcMealSupplementForGuid decimal(10, 2),
	@iNoOfTourGuidePeople int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Lunch]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[TypeOfMeal] = @sTypeOfMeal,
	[MealPerPerson] = @dcMealPerPerson,
	[NoOfPeople] = @iNoOfPeople,
	[MealSupplementForGuid] = @dcMealSupplementForGuid,
	[NoOfTourGuidePeople] = @iNoOfTourGuidePeople,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Lunch_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Lunch_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Lunch''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Lunch_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Lunch]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_OtherServices''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_OtherServices]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_OtherServices''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_OtherServices]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_OtherServices''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sOtherServices nvarchar(250)
-- Gets: @dcPricePerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @sNotes nvarchar(Max)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_Insert]
	@iQuotationPriceBasicId int,
	@sOtherServices nvarchar(250),
	@dcPricePerPerson decimal(10, 2),
	@iNoOfPeople int,
	@sNotes nvarchar(Max),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_OtherServices]
(
	[QuotationPriceBasicId],
	[OtherServices],
	[PricePerPerson],
	[NoOfPeople],
	[Notes],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@sOtherServices,
	@dcPricePerPerson,
	@iNoOfPeople,
	@sNotes,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_OtherServices''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[OtherServices],
	[PricePerPerson],
	[NoOfPeople],
	[Notes],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_OtherServices]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_OtherServices''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[OtherServices],
	[PricePerPerson],
	[NoOfPeople],
	[Notes],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_OtherServices]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Hotel''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	QS.ID, 
	QS.QuotationPriceBasicId, 
	QS.OtherServices, 
	QS.PricePerPerson, 
	QS.NoOfPeople, 
	QS.Notes, 
	QS.NetRateInTotal, 
	QS.MarkUpPercentage, 
	QS.GrossRateInTotal, 
	QS.Profit, 
	QS.Remark, 
	QS.QuotationCurrency, 
	QS.ConfirmationStatus, 
	QS.CreatedBy, 
	QS.CreatedDate, 
	QS.UpdatedBy, 
	QS.UpdatedDate,
 

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QS.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].QuotationPrice_OtherServices QS on  QB.ID = QS.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
 
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR

' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_OtherServices''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[OtherServices],
	[PricePerPerson],
	[NoOfPeople],
	[Notes],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_OtherServices]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_OtherServices''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sOtherServices nvarchar(250)
-- Gets: @dcPricePerPerson decimal(10, 2)
-- Gets: @iNoOfPeople int
-- Gets: @sNotes nvarchar(Max)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@sOtherServices nvarchar(250),
	@dcPricePerPerson decimal(10, 2),
	@iNoOfPeople int,
	@sNotes nvarchar(Max),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_OtherServices]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[OtherServices] = @sOtherServices,
	[PricePerPerson] = @dcPricePerPerson,
	[NoOfPeople] = @iNoOfPeople,
	[Notes] = @sNotes,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_OtherServices_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_OtherServices_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_OtherServices''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_OtherServices_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_OtherServices]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Overview''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Overview]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Overview''
-- based on a foreign key field.
-- Gets: @iQuotationPriceId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Overview]
WHERE
	[QuotationPriceId] = @iQuotationPriceId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Final]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Final]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Overview''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_Final]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON


IF OBJECT_ID(''tempdb..#tmpQuotp'') IS NOT NULL drop table #tmpQuotp

select * into #tmpQuotp  from  (select * from [dbo].[QuotationPrice_Basic]		where QuotationPriceId = @iQuotationPriceId) as Tbl
-- SELECT one or more existing rows from the table.
 select * from [dbo].[QuotationPrice_Coach]     where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_Dinner]         where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_DriveGuide]		where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_Entrance]		where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_Hotel]			where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_Lunch]			where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_OtherServices]	where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_Tips]			where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_TourGuide]		where QuotationPriceBasicId in (select id from #tmpQuotp )
select * from [dbo].[QuotationPrice_VanHire]		where QuotationPriceBasicId in (select id from #tmpQuotp )
 
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR


' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Overview''
-- Gets: @iQuotationPriceId int
-- Gets: @dcNetRatePerPerson decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRatePerPerson decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @dcProfitPercentage decimal(10, 2)
-- Gets: @dcSingleRoomSupplementPerPerson decimal(10, 2)
-- Gets: @dcMarkUpPercentage2 decimal(10, 2)
-- Gets: @dcGrossRatePerPerson2 decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_Insert]
	@iQuotationPriceId int,
	@dcNetRatePerPerson decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRatePerPerson decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@dcProfitPercentage decimal(10, 2),
	@dcSingleRoomSupplementPerPerson decimal(10, 2),
	@dcMarkUpPercentage2 decimal(10, 2),
	@dcGrossRatePerPerson2 decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Overview]
(
	[QuotationPriceId],
	[NetRatePerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[ProfitPercentage],
	[SingleRoomSupplementPerPerson],
	[MarkUpPercentage2],
	[GrossRatePerPerson2],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceId,
	@dcNetRatePerPerson,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRatePerPerson,
	@dcGrossRateInTotal,
	@dcProfit,
	@dcProfitPercentage,
	@dcSingleRoomSupplementPerPerson,
	@dcMarkUpPercentage2,
	@dcGrossRatePerPerson2,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Overview''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceId],
	[NetRatePerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[ProfitPercentage],
	[SingleRoomSupplementPerPerson],
	[MarkUpPercentage2],
	[GrossRatePerPerson2],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Overview]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Overview''
-- based on a foreign key field.
-- Gets: @iQuotationPriceId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceId],
	[NetRatePerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[ProfitPercentage],
	[SingleRoomSupplementPerPerson],
	[MarkUpPercentage2],
	[GrossRatePerPerson2],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Overview]
WHERE
	[QuotationPriceId] = @iQuotationPriceId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Overview''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceId],
	[NetRatePerPerson],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRatePerPerson],
	[GrossRateInTotal],
	[Profit],
	[ProfitPercentage],
	[SingleRoomSupplementPerPerson],
	[MarkUpPercentage2],
	[GrossRatePerPerson2],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Overview]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Overview''
-- Gets: @iID int
-- Gets: @iQuotationPriceId int
-- Gets: @dcNetRatePerPerson decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRatePerPerson decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @dcProfitPercentage decimal(10, 2)
-- Gets: @dcSingleRoomSupplementPerPerson decimal(10, 2)
-- Gets: @dcMarkUpPercentage2 decimal(10, 2)
-- Gets: @dcGrossRatePerPerson2 decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_Update]
	@iID int,
	@iQuotationPriceId int,
	@dcNetRatePerPerson decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRatePerPerson decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@dcProfitPercentage decimal(10, 2),
	@dcSingleRoomSupplementPerPerson decimal(10, 2),
	@dcMarkUpPercentage2 decimal(10, 2),
	@dcGrossRatePerPerson2 decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Overview]
SET 
	[QuotationPriceId] = @iQuotationPriceId,
	[NetRatePerPerson] = @dcNetRatePerPerson,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRatePerPerson] = @dcGrossRatePerPerson,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[ProfitPercentage] = @dcProfitPercentage,
	[SingleRoomSupplementPerPerson] = @dcSingleRoomSupplementPerPerson,
	[MarkUpPercentage2] = @dcMarkUpPercentage2,
	[GrossRatePerPerson2] = @dcGrossRatePerPerson2,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Overview''.
-- Will reset field [QuotationPriceId] with value @iQuotationPriceIdOld  to value @iQuotationPriceId
-- Gets: @iQuotationPriceId int
-- Gets: @iQuotationPriceIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic]
	@iQuotationPriceId int,
	@iQuotationPriceIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Overview]
SET
	[QuotationPriceId] = @iQuotationPriceId
WHERE
	[QuotationPriceId] = @iQuotationPriceIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationId],
	[TourCode],
	[QuotationStaff],
	[QuotationDate],
	[NoOfPeople],
	[Confirmation],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_SelectAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_SelectAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice''
-- based on a foreign key field.
-- Gets: @iQuotationId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_SelectAllWQuotationIdLogic]
	@iQuotationId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationId],
	[TourCode],
	[QuotationStaff],
	[QuotationDate],
	[NoOfPeople],
	[Confirmation],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice]
WHERE
	[QuotationId] = @iQuotationId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationId],
	[TourCode],
	[QuotationStaff],
	[QuotationDate],
	[NoOfPeople],
	[Confirmation],
	[QuotationCurrency],
	[MarkupPercentage],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_Tips''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_Tips]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_Tips''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_Tips]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_Tips''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iTipsPerDay int
-- Gets: @iNoOfPeople int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_Insert]
	@iQuotationPriceBasicId int,
	@iTipsPerDay int,
	@iNoOfPeople int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_Tips]
(
	[QuotationPriceBasicId],
	[TipsPerDay],
	[NoOfPeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@iTipsPerDay,
	@iNoOfPeople,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_Tips''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TipsPerDay],
	[NoOfPeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Tips]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Tips''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TipsPerDay],
	[NoOfPeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Tips]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_Tips''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
Create PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	QT.[ID],
	QT.[QuotationPriceBasicId],
	QT.[TipsPerDay],
	QT.[NoOfPeople],
	QT.[NetRateInTotal],
	QT.[MarkUpPercentage],
	QT.[GrossRateInTotal],
	QT.[Profit],
	QT.[Remark],
	QT.[QuotationCurrency],
	QT.[ConfirmationStatus],
	QT.[CreatedBy],
	QT.[CreatedDate],
	QT.[UpdatedBy],
	QT.[UpdatedDate],
 

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QT.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].[QuotationPrice_Tips] QT on  QB.ID = QT.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
 
 
	 
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_Tips''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TipsPerDay],
	[NoOfPeople],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_Tips]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_Tips''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iTipsPerDay int
-- Gets: @iNoOfPeople int
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@iTipsPerDay int,
	@iNoOfPeople int,
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Tips]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[TipsPerDay] = @iTipsPerDay,
	[NoOfPeople] = @iNoOfPeople,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_Tips''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_Tips]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_TourGuide''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_TourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_TourGuide''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_TourGuide]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_TourGuide''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @dcRatePerDay decimal(10, 2)
-- Gets: @iNoOfRoom int
-- Gets: @sTypeOfGuide nvarchar(100)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_Insert]
	@iQuotationPriceBasicId int,
	@dcRatePerDay decimal(10, 2),
	@iNoOfRoom int,
	@sTypeOfGuide nvarchar(100),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_TourGuide]
(
	[QuotationPriceBasicId],
	[RatePerDay],
	[NoOfRoom],
	[TypeOfGuide],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@dcRatePerDay,
	@iNoOfRoom,
	@sTypeOfGuide,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_TourGuide''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[RatePerDay],
	[NoOfRoom],
	[TypeOfGuide],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_TourGuide]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_TourGuide''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[RatePerDay],
	[NoOfRoom],
	[TypeOfGuide],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_TourGuide]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_TourGuide''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	QT.[ID],
	QT.[QuotationPriceBasicId],
	QT.[RatePerDay],
	QT.[NoOfRoom],
	QT.[TypeOfGuide],
	QT.[NetRateInTotal],
	QT.[MarkUpPercentage],
	QT.[GrossRateInTotal],
	QT.[Profit],
	QT.[Remark],
	QT.[QuotationCurrency],
	QT.[ConfirmationStatus],
	QT.[CreatedBy],
	QT.[CreatedDate],
	QT.[UpdatedBy],
	QT.[UpdatedDate],

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QT.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].QuotationPrice_TourGuide QT on  QB.ID = QT.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_TourGuide''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[RatePerDay],
	[NoOfRoom],
	[TypeOfGuide],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_TourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_TourGuide''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @dcRatePerDay decimal(10, 2)
-- Gets: @iNoOfRoom int
-- Gets: @sTypeOfGuide nvarchar(100)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@dcRatePerDay decimal(10, 2),
	@iNoOfRoom int,
	@sTypeOfGuide nvarchar(100),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_TourGuide]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[RatePerDay] = @dcRatePerDay,
	[NoOfRoom] = @iNoOfRoom,
	[TypeOfGuide] = @sTypeOfGuide,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_TourGuide_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_TourGuide_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_TourGuide''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_TourGuide_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_TourGuide]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice''
-- Gets: @iID int
-- Gets: @iQuotationId int
-- Gets: @sTourCode nvarchar(50)
-- Gets: @iQuotationStaff int
-- Gets: @daQuotationDate datetime
-- Gets: @iNoOfPeople int
-- Gets: @sConfirmation nvarchar(100)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @dcMarkupPercentage decimal(10, 2)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_Update]
	@iID int,
	@iQuotationId int,
	@sTourCode nvarchar(50),
	@iQuotationStaff int,
	@daQuotationDate datetime,
	@iNoOfPeople int,
	@sConfirmation nvarchar(100),
	@sQuotationCurrency nvarchar(100),
	@dcMarkupPercentage decimal(10, 2),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice]
SET 
	[QuotationId] = @iQuotationId,
	[TourCode] = @sTourCode,
	[QuotationStaff] = @iQuotationStaff,
	[QuotationDate] = @daQuotationDate,
	[NoOfPeople] = @iNoOfPeople,
	[Confirmation] = @sConfirmation,
	[QuotationCurrency] = @sQuotationCurrency,
	[MarkupPercentage] = @dcMarkupPercentage,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_UpdateAllWQuotationIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_UpdateAllWQuotationIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice''.
-- Will reset field [QuotationId] with value @iQuotationIdOld  to value @iQuotationId
-- Gets: @iQuotationId int
-- Gets: @iQuotationIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_UpdateAllWQuotationIdLogic]
	@iQuotationId int,
	@iQuotationIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice]
SET
	[QuotationId] = @iQuotationId
WHERE
	[QuotationId] = @iQuotationIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_Delete]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''QuotationPrice_VanHire''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[QuotationPrice_VanHire]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_DeleteAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_DeleteAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''QuotationPrice_VanHire''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_DeleteAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[QuotationPrice_VanHire]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_Insert]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''QuotationPrice_VanHire''
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfVehicle nvarchar(100)
-- Gets: @dcVehicleRatePerDay decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_Insert]
	@iQuotationPriceBasicId int,
	@sTypeOfVehicle nvarchar(100),
	@dcVehicleRatePerDay decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[QuotationPrice_VanHire]
(
	[QuotationPriceBasicId],
	[TypeOfVehicle],
	[VehicleRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iQuotationPriceBasicId,
	@sTypeOfVehicle,
	@dcVehicleRatePerDay,
	@dcNetRateInTotal,
	@dcMarkUpPercentage,
	@dcGrossRateInTotal,
	@dcProfit,
	@sRemark,
	@sQuotationCurrency,
	@sConfirmationStatus,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectAll]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_VanHire''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfVehicle],
	[VehicleRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_VanHire]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''QuotationPrice_VanHire''
-- based on a foreign key field.
-- Gets: @iQuotationPriceBasicId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfVehicle],
	[VehicleRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_VanHire]
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectByPriceIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectByPriceIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''QuotationPrice_VanHire''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectByPriceIdLogic]
	@iQuotationPriceId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.

;WITH CTE AS( 
SELECT
	QT.[ID],
	QT.[QuotationPriceBasicId],
	QT.[TypeOfVehicle],
	QT.[VehicleRatePerDay],
	QT.[NetRateInTotal],
	QT.[MarkUpPercentage],
	QT.[GrossRateInTotal],
	QT.[Profit],
	QT.[Remark],
	QT.[QuotationCurrency],
	QT.[ConfirmationStatus],
	QT.[CreatedBy],
	QT.[CreatedDate],
	QT.[UpdatedBy],
	QT.[UpdatedDate], 

	QB.ID as QuotationBasicId,-- as quotationpriceBasicID of the day
	QB.QuotationPriceId, 
	QB.NoOfDay, 
	QB.TourDate, 
	QB.TourCity
	--QB.QuotationCurrency, 
	--QB.MarkupPercentage, 
	--QB.ConfirmationStatus
	
    , RN = ROW_NUMBER()OVER(PARTITION BY  Qb.id ORDER BY QT.ID)
FROM            
QuotationPrice_Basic AS QB 
Left join [dbo].[QuotationPrice_VanHire] QT on  QB.ID = QT.QuotationPriceBasicId
WHERE
QB.QuotationPriceId=@iQuotationPriceId 
)
SELECT * FROM CTE
WHERE RN = 1
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_SelectOne]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''QuotationPrice_VanHire''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[QuotationPriceBasicId],
	[TypeOfVehicle],
	[VehicleRatePerDay],
	[NetRateInTotal],
	[MarkUpPercentage],
	[GrossRateInTotal],
	[Profit],
	[Remark],
	[QuotationCurrency],
	[ConfirmationStatus],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[QuotationPrice_VanHire]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_Update]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''QuotationPrice_VanHire''
-- Gets: @iID int
-- Gets: @iQuotationPriceBasicId int
-- Gets: @sTypeOfVehicle nvarchar(100)
-- Gets: @dcVehicleRatePerDay decimal(10, 2)
-- Gets: @dcNetRateInTotal decimal(10, 2)
-- Gets: @dcMarkUpPercentage decimal(10, 2)
-- Gets: @dcGrossRateInTotal decimal(10, 2)
-- Gets: @dcProfit decimal(10, 2)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sQuotationCurrency nvarchar(100)
-- Gets: @sConfirmationStatus nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_Update]
	@iID int,
	@iQuotationPriceBasicId int,
	@sTypeOfVehicle nvarchar(100),
	@dcVehicleRatePerDay decimal(10, 2),
	@dcNetRateInTotal decimal(10, 2),
	@dcMarkUpPercentage decimal(10, 2),
	@dcGrossRateInTotal decimal(10, 2),
	@dcProfit decimal(10, 2),
	@sRemark nvarchar(Max),
	@sQuotationCurrency nvarchar(100),
	@sConfirmationStatus nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_VanHire]
SET 
	[QuotationPriceBasicId] = @iQuotationPriceBasicId,
	[TypeOfVehicle] = @sTypeOfVehicle,
	[VehicleRatePerDay] = @dcVehicleRatePerDay,
	[NetRateInTotal] = @dcNetRateInTotal,
	[MarkUpPercentage] = @dcMarkUpPercentage,
	[GrossRateInTotal] = @dcGrossRateInTotal,
	[Profit] = @dcProfit,
	[Remark] = @sRemark,
	[QuotationCurrency] = @sQuotationCurrency,
	[ConfirmationStatus] = @sConfirmationStatus,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_QuotationPrice_VanHire_UpdateAllWQuotationPriceBasicIdLogic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_QuotationPrice_VanHire_UpdateAllWQuotationPriceBasicIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''QuotationPrice_VanHire''.
-- Will reset field [QuotationPriceBasicId] with value @iQuotationPriceBasicIdOld  to value @iQuotationPriceBasicId
-- Gets: @iQuotationPriceBasicId int
-- Gets: @iQuotationPriceBasicIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_QuotationPrice_VanHire_UpdateAllWQuotationPriceBasicIdLogic]
	@iQuotationPriceBasicId int,
	@iQuotationPriceBasicIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[QuotationPrice_VanHire]
SET
	[QuotationPriceBasicId] = @iQuotationPriceBasicId
WHERE
	[QuotationPriceBasicId] = @iQuotationPriceBasicIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  Table [dbo].[OperationContactMgmt]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationContactMgmt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OperationContactMgmt](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[Status] [nvarchar](50) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OperationContactMgmt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OperationLeadMgmt]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationLeadMgmt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OperationLeadMgmt](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[CampaignID] [int] NULL,
	[TourType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Gender] [nvarchar](10) NOT NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[PreferredContactTime] [nvarchar](50) NULL,
	[PreferredContactMethod] [nvarchar](50) NULL,
	[PlannedDepartureDate] [datetime] NULL,
	[HowManyDays] [int] NULL,
	[HowManyPeople] [int] NULL,
	[HowManyChild] [int] NULL,
	[PlaceOfDeparture] [nvarchar](50) NULL,
	[PlaceOfReturn] [nvarchar](50) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[Currency] [nvarchar](15) NULL,
	[TourGuide] [nvarchar](50) NULL,
	[FlightsNeeded] [bit] NULL,
	[Vehicle] [nvarchar](50) NULL,
	[HotelType] [nvarchar](50) NULL,
	[Meals] [nvarchar](50) NULL,
	[AdmissionTickets] [nvarchar](50) NULL,
	[Insurance] [nvarchar](50) NULL,
	[LeadSource] [nvarchar](50) NULL,
	[LeadOwner] [nvarchar](50) NULL,
	[LeadStatus] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OperationLeadMgmt] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OperationQuotation]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationQuotation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OperationQuotation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[TourCode] [nvarchar](50) NOT NULL,
	[QuotationDate] [datetime] NOT NULL,
	[StaffId] [int] NULL,
	[InquiryStaffId] [int] NULL,
	[CompanyIndividual] [int] NULL,
	[Client_City] [nvarchar](100) NULL,
	[Client_Country] [nvarchar](100) NULL,
	[Client_Telephone] [nvarchar](15) NULL,
	[Client_MobileNumber] [nvarchar](15) NULL,
	[Client_Fax] [nvarchar](15) NULL,
	[Client_WeChat] [nvarchar](25) NULL,
	[Client_Email] [nvarchar](50) NULL,
	[SalesId] [int] NULL,
	[QuotationStaff] [nvarchar](50) NULL,
	[DepartmentID] [int] NULL,
	[Office_City] [nvarchar](100) NULL,
	[Office_Country] [nvarchar](100) NULL,
	[Office_Telephone] [nvarchar](15) NULL,
	[Office_MobileNumber] [nvarchar](15) NULL,
	[Office_Fax] [nvarchar](15) NULL,
	[Office_WeChat] [nvarchar](25) NULL,
	[Office_Email] [nvarchar](50) NULL,
	[TourStartDate] [datetime] NULL,
	[TourEndDate] [datetime] NULL,
	[NoOfServiceDay] [int] NULL,
	[TotalClient] [int] NULL,
	[NoOfAdults] [int] NULL,
	[NoOfChild5To12] [int] NULL,
	[NoOfChild2To5] [int] NULL,
	[NoOfInfant1To2] [int] NULL,
	[Comments] [nvarchar](max) NULL,
	[MessageToClient] [nvarchar](max) NULL,
	[QuotationStatus] [nvarchar](100) NULL,
	[QuotationReport] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__Operatio__3214EC270B0CDB31] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[OperationQuotationDocuments]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OperationQuotationDocuments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OperationQuotationDocuments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OperationQuotationId] [int] NOT NULL,
	[DocumentName] [nvarchar](1500) NOT NULL,
	[FileType] [nvarchar](1500) NULL,
	[FileName] [nvarchar](1500) NULL,
	[FileData] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OperationQuotationDocuments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quotation_Summary]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quotation_Summary](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[NoOfDays] [int] NULL,
	[QuotationDate] [datetime] NULL,
	[TourCity] [nvarchar](100) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[MarkupPercentage] [decimal](10, 2) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[TotalAmount] [decimal](10, 2) NULL,
	[NoOfPeople] [int] NULL,
	[NoOfAdult] [int] NULL,
	[NoOfChild] [int] NULL,
	[NoOfInfants] [int] NULL,
	[ExtraField1] [nvarchar](50) NULL,
	[ExtraField2] [nvarchar](50) NULL,
	[ExtraField3] [nvarchar](50) NULL,
	[ExtraField4] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Quotation_Summary] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationId] [int] NOT NULL,
	[TourCode] [nvarchar](50) NOT NULL,
	[QuotationStaff] [int] NULL,
	[QuotationDate] [datetime] NULL,
	[NoOfPeople] [int] NULL,
	[Confirmation] [nvarchar](100) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[MarkupPercentage] [decimal](10, 2) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__Quotatio__3214EC27D0E9D9B6] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Basic]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Basic]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Basic](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceId] [int] NOT NULL,
	[NoOfDay] [int] NULL,
	[TourDate] [datetime] NULL,
	[TourCity] [nvarchar](100) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[MarkupPercentage] [decimal](10, 2) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__Quotatio__3214EC27F10AC8BE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Coach]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Coach]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Coach](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[TypeOfCoach] [nvarchar](100) NULL,
	[CoachRatePerDay] [decimal](10, 2) NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Dinner]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Dinner]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Dinner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[TypeOfMeal] [nvarchar](100) NULL,
	[MealPerPerson] [decimal](10, 2) NULL,
	[NoOfPeople] [int] NULL,
	[MealSupplementForGuid] [decimal](10, 2) NULL,
	[NoOfTourGuidePeople] [int] NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_DriveGuide]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_DriveGuide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_DriveGuide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[RatePerRoom] [decimal](10, 2) NULL,
	[NoOfRoom] [int] NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__Quotatio__3214EC27E5A06705] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Entrance]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Entrance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Entrance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[EnteranceName] [nvarchar](150) NULL,
	[AdultPrice] [decimal](10, 2) NULL,
	[NoOfPeople] [int] NULL,
	[ChildPrice] [decimal](10, 2) NULL,
	[NoOfChild] [int] NULL,
	[Concession_student_elderly] [decimal](10, 2) NULL,
	[NoOfStudentForConcession] [int] NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Hotel]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Hotel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Hotel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[StartHotel] [int] NULL,
	[RecommendedHotel] [nvarchar](200) NULL,
	[TwinPerPerson] [decimal](10, 2) NULL,
	[NoOfPeople] [int] NULL,
	[SingleRoomSupplementPerPerson] [decimal](10, 2) NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRatePerPerson] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__Quotatio__3214EC2726EFBD0F] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Lunch]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Lunch]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Lunch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[TypeOfMeal] [nvarchar](100) NULL,
	[MealPerPerson] [decimal](10, 2) NULL,
	[NoOfPeople] [int] NULL,
	[MealSupplementForGuid] [decimal](10, 2) NULL,
	[NoOfTourGuidePeople] [int] NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_OtherServices]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_OtherServices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_OtherServices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[OtherServices] [nvarchar](250) NULL,
	[PricePerPerson] [decimal](10, 2) NULL,
	[NoOfPeople] [int] NULL,
	[Notes] [nvarchar](max) NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Overview]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Overview]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Overview](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceId] [int] NOT NULL,
	[NetRatePerPerson] [decimal](10, 2) NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRatePerPerson] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[ProfitPercentage] [decimal](10, 2) NULL,
	[SingleRoomSupplementPerPerson] [decimal](10, 2) NULL,
	[MarkUpPercentage2] [decimal](10, 2) NULL,
	[GrossRatePerPerson2] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_Tips]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Tips]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_Tips](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[TipsPerDay] [int] NULL,
	[NoOfPeople] [int] NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_TourGuide]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_TourGuide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_TourGuide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[RatePerDay] [decimal](10, 2) NULL,
	[NoOfRoom] [int] NULL,
	[TypeOfGuide] [nvarchar](100) NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_QuotationPrice_TourGuide] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[QuotationPrice_VanHire]    Script Date: 24-07-2017 13:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QuotationPrice_VanHire]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QuotationPrice_VanHire](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationPriceBasicId] [int] NOT NULL,
	[TypeOfVehicle] [nvarchar](100) NULL,
	[VehicleRatePerDay] [decimal](10, 2) NULL,
	[NetRateInTotal] [decimal](10, 2) NULL,
	[MarkUpPercentage] [decimal](10, 2) NULL,
	[GrossRateInTotal] [decimal](10, 2) NULL,
	[Profit] [decimal](10, 2) NULL,
	[Remark] [nvarchar](max) NULL,
	[QuotationCurrency] [nvarchar](100) NULL,
	[ConfirmationStatus] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OperationQuotationDocuments_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[OperationQuotationDocuments]'))
ALTER TABLE [dbo].[OperationQuotationDocuments]  WITH CHECK ADD  CONSTRAINT [FK_OperationQuotationDocuments_OperationQuotation] FOREIGN KEY([OperationQuotationId])
REFERENCES [dbo].[OperationQuotation] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OperationQuotationDocuments_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[OperationQuotationDocuments]'))
ALTER TABLE [dbo].[OperationQuotationDocuments] CHECK CONSTRAINT [FK_OperationQuotationDocuments_OperationQuotation]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Quotation_Summary_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]'))
ALTER TABLE [dbo].[Quotation_Summary]  WITH CHECK ADD  CONSTRAINT [FK_Quotation_Summary_OperationQuotation] FOREIGN KEY([QuotationId])
REFERENCES [dbo].[OperationQuotation] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Quotation_Summary_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]'))
ALTER TABLE [dbo].[Quotation_Summary] CHECK CONSTRAINT [FK_Quotation_Summary_OperationQuotation]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Quotation_Summary_Quotation_Summary]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]'))
ALTER TABLE [dbo].[Quotation_Summary]  WITH CHECK ADD  CONSTRAINT [FK_Quotation_Summary_Quotation_Summary] FOREIGN KEY([ID])
REFERENCES [dbo].[Quotation_Summary] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Quotation_Summary_Quotation_Summary]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quotation_Summary]'))
ALTER TABLE [dbo].[Quotation_Summary] CHECK CONSTRAINT [FK_Quotation_Summary_Quotation_Summary]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice]'))
ALTER TABLE [dbo].[QuotationPrice]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_OperationQuotation] FOREIGN KEY([QuotationId])
REFERENCES [dbo].[OperationQuotation] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_OperationQuotation]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice]'))
ALTER TABLE [dbo].[QuotationPrice] CHECK CONSTRAINT [FK_QuotationPrice_OperationQuotation]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Basic_QuotationPrice]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Basic]'))
ALTER TABLE [dbo].[QuotationPrice_Basic]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Basic_QuotationPrice] FOREIGN KEY([QuotationPriceId])
REFERENCES [dbo].[QuotationPrice] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Basic_QuotationPrice]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Basic]'))
ALTER TABLE [dbo].[QuotationPrice_Basic] CHECK CONSTRAINT [FK_QuotationPrice_Basic_QuotationPrice]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Coach_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Coach]'))
ALTER TABLE [dbo].[QuotationPrice_Coach]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Coach_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Coach_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Coach]'))
ALTER TABLE [dbo].[QuotationPrice_Coach] CHECK CONSTRAINT [FK_QuotationPrice_Coach_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Dinner_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Dinner]'))
ALTER TABLE [dbo].[QuotationPrice_Dinner]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Dinner_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Dinner_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Dinner]'))
ALTER TABLE [dbo].[QuotationPrice_Dinner] CHECK CONSTRAINT [FK_QuotationPrice_Dinner_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_DriveGuide_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_DriveGuide]'))
ALTER TABLE [dbo].[QuotationPrice_DriveGuide]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_DriveGuide_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_DriveGuide_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_DriveGuide]'))
ALTER TABLE [dbo].[QuotationPrice_DriveGuide] CHECK CONSTRAINT [FK_QuotationPrice_DriveGuide_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Entrance_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Entrance]'))
ALTER TABLE [dbo].[QuotationPrice_Entrance]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Entrance_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Entrance_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Entrance]'))
ALTER TABLE [dbo].[QuotationPrice_Entrance] CHECK CONSTRAINT [FK_QuotationPrice_Entrance_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Hotel_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Hotel]'))
ALTER TABLE [dbo].[QuotationPrice_Hotel]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Hotel_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Hotel_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Hotel]'))
ALTER TABLE [dbo].[QuotationPrice_Hotel] CHECK CONSTRAINT [FK_QuotationPrice_Hotel_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Lunch_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Lunch]'))
ALTER TABLE [dbo].[QuotationPrice_Lunch]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Lunch_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Lunch_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Lunch]'))
ALTER TABLE [dbo].[QuotationPrice_Lunch] CHECK CONSTRAINT [FK_QuotationPrice_Lunch_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_OtherServices_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_OtherServices]'))
ALTER TABLE [dbo].[QuotationPrice_OtherServices]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_OtherServices_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_OtherServices_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_OtherServices]'))
ALTER TABLE [dbo].[QuotationPrice_OtherServices] CHECK CONSTRAINT [FK_QuotationPrice_OtherServices_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Overview_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Overview]'))
ALTER TABLE [dbo].[QuotationPrice_Overview]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Overview_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceId])
REFERENCES [dbo].[QuotationPrice] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Overview_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Overview]'))
ALTER TABLE [dbo].[QuotationPrice_Overview] CHECK CONSTRAINT [FK_QuotationPrice_Overview_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Tips_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Tips]'))
ALTER TABLE [dbo].[QuotationPrice_Tips]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_Tips_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_Tips_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_Tips]'))
ALTER TABLE [dbo].[QuotationPrice_Tips] CHECK CONSTRAINT [FK_QuotationPrice_Tips_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_TourGuide_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_TourGuide]'))
ALTER TABLE [dbo].[QuotationPrice_TourGuide]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_TourGuide_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_TourGuide_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_TourGuide]'))
ALTER TABLE [dbo].[QuotationPrice_TourGuide] CHECK CONSTRAINT [FK_QuotationPrice_TourGuide_QuotationPrice_Basic]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_VanHire_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_VanHire]'))
ALTER TABLE [dbo].[QuotationPrice_VanHire]  WITH CHECK ADD  CONSTRAINT [FK_QuotationPrice_VanHire_QuotationPrice_Basic] FOREIGN KEY([QuotationPriceBasicId])
REFERENCES [dbo].[QuotationPrice_Basic] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QuotationPrice_VanHire_QuotationPrice_Basic]') AND parent_object_id = OBJECT_ID(N'[dbo].[QuotationPrice_VanHire]'))
ALTER TABLE [dbo].[QuotationPrice_VanHire] CHECK CONSTRAINT [FK_QuotationPrice_VanHire_QuotationPrice_Basic]
GO
