IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierMaster', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierMaster', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Breakfast'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Breakfast'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'FamilyRoom'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'FamilyRoom'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Triple'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Triple'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Twin_DoubleForSoleUse'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Twin_DoubleForSoleUse'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Single'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Single'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'HalfTwin'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'HalfTwin'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'Cabin'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'Cabin'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'GroupRate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'GroupRate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'FitRate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'FitRate'

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate] DROP CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate] DROP CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate] DROP CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] DROP CONSTRAINT [FK_SuppliersDetails_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] DROP CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement] DROP CONSTRAINT [FK_SupplierMealSupplement_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster] DROP CONSTRAINT [FK_SupplierMaster_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate] DROP CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation] DROP CONSTRAINT [FK_SupplierInformation_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate] DROP CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate] DROP CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts] DROP CONSTRAINT [FK_SupplierContacts_SuppliersDetails]
GO
/****** Object:  Table [dbo].[SupplierVanHireRate]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierVanHireRate]
GO
/****** Object:  Table [dbo].[SupplierTrainRate]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTrainRate]
GO
/****** Object:  Table [dbo].[SupplierTheaterRate]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTheaterRate]
GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
DROP TABLE [dbo].[SuppliersDetails]
GO
/****** Object:  Table [dbo].[SupplierMealSupplement]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierMealSupplement]
GO
/****** Object:  Table [dbo].[SupplierMaster]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMaster]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierMaster]
GO
/****** Object:  Table [dbo].[SupplierInsuranceRate]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierInsuranceRate]
GO
/****** Object:  Table [dbo].[SupplierInformation]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInformation]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierInformation]
GO
/****** Object:  Table [dbo].[SupplierHotelRate]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierHotelRate]
GO
/****** Object:  Table [dbo].[SupplierFerryRate]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierFerryRate]
GO
/****** Object:  Table [dbo].[SupplierContacts]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierContacts]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierContacts]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].`    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Update]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Insert]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Delete]    Script Date: 06-04-2017 15:01:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierContacts''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierContacts''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierContacts]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierContacts''
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_Insert]
	@iSupplierDetailID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierContacts]
(
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sTitle,
	@sSurName,
	@sFirstName,
	@bMainContact,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierContacts''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierContacts]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierContacts''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierContacts]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierContacts''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierContacts''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_Update]
	@iID int,
	@iSupplierDetailID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierContacts]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[MainContact] = @bMainContact,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierContacts''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierContacts]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierFerryRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierFerryRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierFerryRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierFerryRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierFerryRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sCountryFrom nvarchar(50)
-- Gets: @sPortFrom nvarchar(50)
-- Gets: @sCountryTo nvarchar(50)
-- Gets: @sPortTo nvarchar(50)
-- Gets: @daDepartTime datetime
-- Gets: @daArrivalTime datetime
-- Gets: @bOneWayOrReturn bit
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sFitRate nvarchar(10)
-- Gets: @sGroupRate nvarchar(10)
-- Gets: @sRemark nvarchar(MAX)
-- Gets: @iCabin int
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sCountryFrom nvarchar(50),
	@sPortFrom nvarchar(50),
	@sCountryTo nvarchar(50),
	@sPortTo nvarchar(50),
	@daDepartTime datetime,
	@daArrivalTime datetime,
	@bOneWayOrReturn bit,
	@sDescription nvarchar(MAX),
	@sFitRate nvarchar(10),
	@sGroupRate nvarchar(10),
	@sRemark nvarchar(MAX),
	@iCabin int,
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierFerryRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sCountryFrom,
	@sPortFrom,
	@sCountryTo,
	@sPortTo,
	@daDepartTime,
	@daArrivalTime,
	@bOneWayOrReturn,
	@sDescription,
	@sFitRate,
	@sGroupRate,
	@sRemark,
	@iCabin,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierFerryRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierFerryRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierFerryRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierFerryRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierFerryRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierFerryRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierFerryRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sCountryFrom nvarchar(50)
-- Gets: @sPortFrom nvarchar(50)
-- Gets: @sCountryTo nvarchar(50)
-- Gets: @sPortTo nvarchar(50)
-- Gets: @daDepartTime datetime
-- Gets: @daArrivalTime datetime
-- Gets: @bOneWayOrReturn bit
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sFitRate nvarchar(10)
-- Gets: @sGroupRate nvarchar(10)
-- Gets: @sRemark nvarchar(MAX)
-- Gets: @iCabin int
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sCountryFrom nvarchar(50),
	@sPortFrom nvarchar(50),
	@sCountryTo nvarchar(50),
	@sPortTo nvarchar(50),
	@daDepartTime datetime,
	@daArrivalTime datetime,
	@bOneWayOrReturn bit,
	@sDescription nvarchar(MAX),
	@sFitRate nvarchar(10),
	@sGroupRate nvarchar(10),
	@sRemark nvarchar(MAX),
	@iCabin int,
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierFerryRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[CountryFrom] = @sCountryFrom,
	[PortFrom] = @sPortFrom,
	[CountryTo] = @sCountryTo,
	[PortTo] = @sPortTo,
	[DepartTime] = @daDepartTime,
	[ArrivalTime] = @daArrivalTime,
	[OneWayOrReturn] = @bOneWayOrReturn,
	[Description] = @sDescription,
	[FitRate] = @sFitRate,
	[GroupRate] = @sGroupRate,
	[Remark] = @sRemark,
	[Cabin] = @iCabin,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierFerryRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierFerryRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierHotelRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierHotelRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierHotelRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierHotelRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierHotelRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @sHalfTwin nvarchar(150)
-- Gets: @sSingle nvarchar(50)
-- Gets: @sTwin_DoubleForSoleUse nvarchar(50)
-- Gets: @sTriple nvarchar(50)
-- Gets: @sFamilyRoom nvarchar(50)
-- Gets: @sBreakfast nvarchar(50)
-- Gets: @sDinnerAtHotel nvarchar(50)
-- Gets: @sRatePerPerson nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@daFromDate datetime,
	@daToDate datetime,
	@sHalfTwin nvarchar(150),
	@sSingle nvarchar(50),
	@sTwin_DoubleForSoleUse nvarchar(50),
	@sTriple nvarchar(50),
	@sFamilyRoom nvarchar(50),
	@sBreakfast nvarchar(50),
	@sDinnerAtHotel nvarchar(50),
	@sRatePerPerson nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierHotelRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@daFromDate,
	@daToDate,
	@sHalfTwin,
	@sSingle,
	@sTwin_DoubleForSoleUse,
	@sTriple,
	@sFamilyRoom,
	@sBreakfast,
	@sDinnerAtHotel,
	@sRatePerPerson,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierHotelRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierHotelRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierHotelRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierHotelRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierHotelRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierHotelRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierHotelRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @sHalfTwin nvarchar(150)
-- Gets: @sSingle nvarchar(50)
-- Gets: @sTwin_DoubleForSoleUse nvarchar(50)
-- Gets: @sTriple nvarchar(50)
-- Gets: @sFamilyRoom nvarchar(50)
-- Gets: @sBreakfast nvarchar(50)
-- Gets: @sDinnerAtHotel nvarchar(50)
-- Gets: @sRatePerPerson nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@daFromDate datetime,
	@daToDate datetime,
	@sHalfTwin nvarchar(150),
	@sSingle nvarchar(50),
	@sTwin_DoubleForSoleUse nvarchar(50),
	@sTriple nvarchar(50),
	@sFamilyRoom nvarchar(50),
	@sBreakfast nvarchar(50),
	@sDinnerAtHotel nvarchar(50),
	@sRatePerPerson nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierHotelRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[FromDate] = @daFromDate,
	[ToDate] = @daToDate,
	[HalfTwin] = @sHalfTwin,
	[Single] = @sSingle,
	[Twin_DoubleForSoleUse] = @sTwin_DoubleForSoleUse,
	[Triple] = @sTriple,
	[FamilyRoom] = @sFamilyRoom,
	[Breakfast] = @sBreakfast,
	[DinnerAtHotel] = @sDinnerAtHotel,
	[RatePerPerson] = @sRatePerPerson,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierHotelRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierHotelRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierInformation''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierInformation]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierInformation''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierInformation]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierInformation''
-- Gets: @iSupplierDetailID int
-- Gets: @sCurrency nvarchar(10)
-- Gets: @iNoofBedRooms int
-- Gets: @sHotelFacility nvarchar(MAX)
-- Gets: @sParking nvarchar(50)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_Insert]
	@iSupplierDetailID int,
	@sCurrency nvarchar(10),
	@iNoofBedRooms int,
	@sHotelFacility nvarchar(MAX),
	@sParking nvarchar(50),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierInformation]
(
	[SupplierDetailID],
	[Currency],
	[NoofBedRooms],
	[HotelFacility],
	[Parking],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sCurrency,
	@iNoofBedRooms,
	@sHotelFacility,
	@sParking,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierInformation''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Currency],
	[NoofBedRooms],
	[HotelFacility],
	[Parking],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInformation]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierInformation''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[SupplierInformation].[ID],
	[SupplierInformation].[SupplierDetailID],
	[SupplierInformation].[Currency],
	[SupplierInformation].[NoofBedRooms],
	[SupplierInformation].[HotelFacility],
	[SupplierInformation].[Parking],
	[SupplierInformation].[Remarks],
	[SupplierInformation].[ExtraField1],
	[SupplierInformation].[ExtraField2],
	[SupplierInformation].[ExtraField3],
	[SupplierInformation].[ExtraField4],
	[SupplierInformation].[ExtraField5],
	[SupplierInformation].[CreatedBy],
	[SupplierInformation].[CreatedDate],
	[SupplierInformation].[UpdatedBy],
	[SupplierInformation].[UpdatedDate] 

	,SM.[Name] AS   Name
	 ,SM.City  as City
	 ,SM.[State] as  State
	 ,SM.Country as  Country
	 ,SM.Telephone as Telephone
	 ,SM.PostalCode as  PostalCode
	 ,SM.Address1 as  Address1
	 ,SM.Address2 as  Address2
FROM [dbo].[SupplierInformation]
JOIN dbo.SuppliersDetails SD on  [SupplierInformation].[SupplierDetailID]=SD.ID
JOIN SupplierMaster SM on SD.[SupplierMasterID] = SM.ID
WHERE
	[SupplierDetailID] = @iSupplierDetailID

-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierInformation''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	SI.[ID],
	SI.[SupplierDetailID],
	SI.[Currency],
	SI.[NoofBedRooms],
	SI.[HotelFacility],
	SI.[Parking],
	SI.[Remarks],
	SI.[ExtraField1],
	SI.[ExtraField2],
	SI.[ExtraField3],
	SI.[ExtraField4],
	SI.[ExtraField5],
	SI.[CreatedBy],
	SI.[CreatedDate],
	SI.[UpdatedBy],
	SI.[UpdatedDate] 
	 ,SM.[Name] AS SupplierName
	 ,SM.[Address1] as InfoAddress1
	 ,SM.[Address2] as InfoAddress2
	 ,SM.City as InfoCity
	 ,SM.[State] as InfoState
	 ,SM.Country as InfoCountry
	 ,SM.Telephone as InfoTelephone
	 ,SM.PostalCode as InfoPostalCode
FROM [dbo].[SupplierInformation] SI
JOIN dbo.SuppliersDetails SD on  SI.[SupplierDetailID]=SD.ID
JOIN SupplierMaster SM on SD.[SupplierMasterID] = SM.ID
WHERE
	SI.[ID] = @iID
 
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierInformation''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sCurrency nvarchar(10)
-- Gets: @iNoofBedRooms int
-- Gets: @sHotelFacility nvarchar(MAX)
-- Gets: @sParking nvarchar(50)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_Update]
	@iID int,
	@iSupplierDetailID int,
	@sCurrency nvarchar(10),
	@iNoofBedRooms int,
	@sHotelFacility nvarchar(MAX),
	@sParking nvarchar(50),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInformation]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[Currency] = @sCurrency,
	[NoofBedRooms] = @iNoofBedRooms,
	[HotelFacility] = @sHotelFacility,
	[Parking] = @sParking,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierInformation''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInformation]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierInsuranceRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierInsuranceRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierInsuranceRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierInsuranceRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierInsuranceRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRateDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRateDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierInsuranceRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sRateDescription,
	@sRate,
	@sComments,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierInsuranceRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInsuranceRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierInsuranceRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInsuranceRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierInsuranceRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInsuranceRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierInsuranceRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRateDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRateDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInsuranceRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[RateDescription] = @sRateDescription,
	[Rate] = @sRate,
	[Comments] = @sComments,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierInsuranceRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInsuranceRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierMaster''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierMaster]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierMaster''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierMaster]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierMaster''
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeID int
-- Gets: @sName nvarchar(250)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(MAX)
-- Gets: @daContractEndingDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_Insert]
	@iCompanyID int,
	@iSupplierTypeID int,
	@sName nvarchar(250),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sPostalCode nvarchar(6),
	@sEmail nvarchar(50),
	@sCreditToLetsTravel nvarchar(50),
	@sPaymentTerms nvarchar(MAX),
	@daContractEndingDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierMaster]
(
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierTypeID,
	@sName,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sTelephone,
	@sPostalCode,
	@sEmail,
	@sCreditToLetsTravel,
	@sPaymentTerms,
	@daContractEndingDate,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierMaster''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMaster]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierMaster''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]
	@iSupplierTypeID int= 0,
	@iID int= 0,
	@iCompanyID  int= 0,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[SupplierMaster].[ID],
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate],
	ST.SupplierType
FROM [dbo].[SupplierMaster]
LEFT JOIN SupplierType ST On [SupplierMaster].SupplierTypeID = ST.ID
WHERE
	([SupplierTypeID] = @iSupplierTypeID or @iSupplierTypeID = 0 )
	AND ( [SupplierMaster].ID = @iID OR @iID =0)
	AND ( CompanyID = @iCompanyID OR @iCompanyID=0)
Order by [CreatedDate] desc
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR


' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierMaster''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMaster]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierMaster''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeID int
-- Gets: @sName nvarchar(250)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(MAX)
-- Gets: @daContractEndingDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierTypeID int,
	@sName nvarchar(250),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sPostalCode nvarchar(6),
	@sEmail nvarchar(50),
	@sCreditToLetsTravel nvarchar(50),
	@sPaymentTerms nvarchar(MAX),
	@daContractEndingDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMaster]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierTypeID] = @iSupplierTypeID,
	[Name] = @sName,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[Telephone] = @sTelephone,
	[PostalCode] = @sPostalCode,
	[Email] = @sEmail,
	[CreditToLetsTravel] = @sCreditToLetsTravel,
	[PaymentTerms] = @sPaymentTerms,
	[ContractEndingDate] = @daContractEndingDate,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierMaster''.
-- Will reset field [SupplierTypeID] with value @iSupplierTypeIDOld  to value @iSupplierTypeID
-- Gets: @iSupplierTypeID int
-- Gets: @iSupplierTypeIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iSupplierTypeIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMaster]
SET
	[SupplierTypeID] = @iSupplierTypeID
WHERE
	[SupplierTypeID] = @iSupplierTypeIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierMealSupplement''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierMealSupplement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierMealSupplement''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierMealSupplement]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierMealSupplement''
-- Gets: @iSupplierTypeID int
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sPurpose nvarchar(MAX)
-- Gets: @sMealSupplementType nvarchar(50)
-- Gets: @dcUK decimal(18, 0)
-- Gets: @dcIreland decimal(18, 0)
-- Gets: @dcEurope decimal(18, 0)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_Insert]
	@iSupplierTypeID int,
	@sAddedBy nvarchar(50),
	@sPurpose nvarchar(MAX),
	@sMealSupplementType nvarchar(50),
	@dcUK decimal(18, 0),
	@dcIreland decimal(18, 0),
	@dcEurope decimal(18, 0),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierMealSupplement]
(
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierTypeID,
	@sAddedBy,
	@sPurpose,
	@sMealSupplementType,
	@dcUK,
	@dcIreland,
	@dcEurope,
	@sCity,
	@sState,
	@sCountry,
	@sPostalCode,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierMealSupplement''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMealSupplement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierMealSupplement''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMealSupplement]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierMealSupplement''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMealSupplement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierMealSupplement''
-- Gets: @iID int
-- Gets: @iSupplierTypeID int
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sPurpose nvarchar(MAX)
-- Gets: @sMealSupplementType nvarchar(50)
-- Gets: @dcUK decimal(18, 0)
-- Gets: @dcIreland decimal(18, 0)
-- Gets: @dcEurope decimal(18, 0)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_Update]
	@iID int,
	@iSupplierTypeID int,
	@sAddedBy nvarchar(50),
	@sPurpose nvarchar(MAX),
	@sMealSupplementType nvarchar(50),
	@dcUK decimal(18, 0),
	@dcIreland decimal(18, 0),
	@dcEurope decimal(18, 0),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMealSupplement]
SET 
	[SupplierTypeID] = @iSupplierTypeID,
	[AddedBy] = @sAddedBy,
	[Purpose] = @sPurpose,
	[MealSupplementType] = @sMealSupplementType,
	[UK] = @dcUK,
	[Ireland] = @dcIreland,
	[Europe] = @dcEurope,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[PostalCode] = @sPostalCode,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierMealSupplement''.
-- Will reset field [SupplierTypeID] with value @iSupplierTypeIDOld  to value @iSupplierTypeID
-- Gets: @iSupplierTypeID int
-- Gets: @iSupplierTypeIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iSupplierTypeIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMealSupplement]
SET
	[SupplierTypeID] = @iSupplierTypeID
WHERE
	[SupplierTypeID] = @iSupplierTypeIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SuppliersDetails''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON

--Delete from refs

Delete from SupplierInformation where [SupplierDetailID] =@iID
Delete from SupplierContacts where [SupplierDetailID] =@iID
Delete from SupplierHotelRate where [SupplierDetailID] =@iID
Delete from SupplierFerryRate where [SupplierDetailID] =@iID
Delete from SupplierInsuranceRate where [SupplierDetailID] =@iID
Delete from SupplierTheaterRate where [SupplierDetailID] =@iID
Delete from SupplierTrainRate where [SupplierDetailID] =@iID
Delete from SupplierVanHireRate where [SupplierDetailID] =@iID


-- DELETE an existing row from the table.
DELETE FROM [dbo].[SuppliersDetails]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierMasterID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierMasterID] = @iSupplierMasterID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SuppliersDetails''
-- Gets: @iCompanyID int
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierTypeID int
-- Gets: @daSupplierDate datetime
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sName nvarchar(50)
-- Gets: @sStarRating nvarchar(10)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sAgency nvarchar(100)
-- Gets: @sClientCode nvarchar(50)
-- Gets: @sUserName nvarchar(50)
-- Gets: @sPassword nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFaxNumber nvarchar(15)
-- Gets: @sEmergencyNo nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sHotelChain nvarchar(50)
-- Gets: @sFranchised nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(MAX)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @daContractStartDate datetime
-- Gets: @daContractEndingDate datetime
-- Gets: @daContractRenewalDate datetime
-- Gets: @sRating nvarchar(10)
-- Gets: @sMainBusiness nvarchar(200)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sHowToBook nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Insert]
	@iCompanyID int,
	@iSupplierMasterID int,
	@iSupplierTypeID int,
	@daSupplierDate datetime,
	@sContractedBy nvarchar(50),
	@sName nvarchar(50),
	@sStarRating nvarchar(10),
	@sWebsite nvarchar(150),
	@sAgency nvarchar(100),
	@sClientCode nvarchar(50),
	@sUserName nvarchar(50),
	@sPassword nvarchar(50),
	@sTelephone nvarchar(15),
	@sFaxNumber nvarchar(15),
	@sEmergencyNo nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sHotelChain nvarchar(50),
	@sFranchised nvarchar(50),
	@sPaymentTerms nvarchar(MAX),
	@sCreditToLetsTravel nvarchar(50),
	@sBlackList nvarchar(200),
	@daContractStartDate datetime,
	@daContractEndingDate datetime,
	@daContractRenewalDate datetime,
	@sRating nvarchar(10),
	@sMainBusiness nvarchar(200),
	@sComments nvarchar(MAX),
	@biFileData varbinary(MAX),
	@sHowToBook nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SuppliersDetails]
(
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierMasterID,
	@iSupplierTypeID,
	@daSupplierDate,
	@sContractedBy,
	@sName,
	@sStarRating,
	@sWebsite,
	@sAgency,
	@sClientCode,
	@sUserName,
	@sPassword,
	@sTelephone,
	@sFaxNumber,
	@sEmergencyNo,
	@sEmail,
	@sAlternateEmail,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sPostalCode,
	@sHotelChain,
	@sFranchised,
	@sPaymentTerms,
	@sCreditToLetsTravel,
	@sBlackList,
	@daContractStartDate,
	@daContractEndingDate,
	@daContractRenewalDate,
	@sRating,
	@sMainBusiness,
	@sComments,
	@biFileData,
	@sHowToBook,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SuppliersDetails''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierMasterID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierMasterID] = @iSupplierMasterID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SuppliersDetails''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SuppliersDetails''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierTypeID int
-- Gets: @daSupplierDate datetime
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sName nvarchar(50)
-- Gets: @sStarRating nvarchar(10)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sAgency nvarchar(100)
-- Gets: @sClientCode nvarchar(50)
-- Gets: @sUserName nvarchar(50)
-- Gets: @sPassword nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFaxNumber nvarchar(15)
-- Gets: @sEmergencyNo nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sHotelChain nvarchar(50)
-- Gets: @sFranchised nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(MAX)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @daContractStartDate datetime
-- Gets: @daContractEndingDate datetime
-- Gets: @daContractRenewalDate datetime
-- Gets: @sRating nvarchar(10)
-- Gets: @sMainBusiness nvarchar(200)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sHowToBook nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierMasterID int,
	@iSupplierTypeID int,
	@daSupplierDate datetime,
	@sContractedBy nvarchar(50),
	@sName nvarchar(50),
	@sStarRating nvarchar(10),
	@sWebsite nvarchar(150),
	@sAgency nvarchar(100),
	@sClientCode nvarchar(50),
	@sUserName nvarchar(50),
	@sPassword nvarchar(50),
	@sTelephone nvarchar(15),
	@sFaxNumber nvarchar(15),
	@sEmergencyNo nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sHotelChain nvarchar(50),
	@sFranchised nvarchar(50),
	@sPaymentTerms nvarchar(MAX),
	@sCreditToLetsTravel nvarchar(50),
	@sBlackList nvarchar(200),
	@daContractStartDate datetime,
	@daContractEndingDate datetime,
	@daContractRenewalDate datetime,
	@sRating nvarchar(10),
	@sMainBusiness nvarchar(200),
	@sComments nvarchar(MAX),
	@biFileData varbinary(MAX),
	@sHowToBook nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierMasterID] = @iSupplierMasterID,
	[SupplierTypeID] = @iSupplierTypeID,
	[SupplierDate] = @daSupplierDate,
	[ContractedBy] = @sContractedBy,
	[Name] = @sName,
	[StarRating] = @sStarRating,
	[Website] = @sWebsite,
	[Agency] = @sAgency,
	[ClientCode] = @sClientCode,
	[UserName] = @sUserName,
	[Password] = @sPassword,
	[Telephone] = @sTelephone,
	[FaxNumber] = @sFaxNumber,
	[EmergencyNo] = @sEmergencyNo,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[PostalCode] = @sPostalCode,
	[HotelChain] = @sHotelChain,
	[Franchised] = @sFranchised,
	[PaymentTerms] = @sPaymentTerms,
	[CreditToLetsTravel] = @sCreditToLetsTravel,
	[BlackList] = @sBlackList,
	[ContractStartDate] = @daContractStartDate,
	[ContractEndingDate] = @daContractEndingDate,
	[ContractRenewalDate] = @daContractRenewalDate,
	[Rating] = @sRating,
	[MainBusiness] = @sMainBusiness,
	[Comments] = @sComments,
	[FileData] = @biFileData,
	[HowToBook] = @sHowToBook,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SuppliersDetails''.
-- Will reset field [SupplierMasterID] with value @iSupplierMasterIDOld  to value @iSupplierMasterID
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierMasterIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iSupplierMasterIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET
	[SupplierMasterID] = @iSupplierMasterID
WHERE
	[SupplierMasterID] = @iSupplierMasterIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SuppliersDetails''.
-- Will reset field [SupplierTypeID] with value @iSupplierTypeIDOld  to value @iSupplierTypeID
-- Gets: @iSupplierTypeID int
-- Gets: @iSupplierTypeIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iSupplierTypeIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET
	[SupplierTypeID] = @iSupplierTypeID
WHERE
	[SupplierTypeID] = @iSupplierTypeIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTheaterRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTheaterRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTheaterRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTheaterRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTheaterRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTheaterRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sDescription,
	@sRate,
	@sComments,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTheaterRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTheaterRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTheaterRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTheaterRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTheaterRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTheaterRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTheaterRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTheaterRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[Description] = @sDescription,
	[Rate] = @sRate,
	[Comments] = @sComments,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTheaterRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTheaterRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTrainRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTrainRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTrainRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTrainRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTrainRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRouteFrom nvarchar(150)
-- Gets: @sRouteTo nvarchar(150)
-- Gets: @sRate nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRouteFrom nvarchar(150),
	@sRouteTo nvarchar(150),
	@sRate nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTrainRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sRouteFrom,
	@sRouteTo,
	@sRate,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTrainRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTrainRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTrainRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTrainRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTrainRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTrainRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTrainRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRouteFrom nvarchar(150)
-- Gets: @sRouteTo nvarchar(150)
-- Gets: @sRate nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRouteFrom nvarchar(150),
	@sRouteTo nvarchar(150),
	@sRate nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTrainRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[RouteFrom] = @sRouteFrom,
	[RouteTo] = @sRouteTo,
	[Rate] = @sRate,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTrainRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTrainRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierType''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierType]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierType''
-- Gets: @sSupplierType varchar(250)
-- Gets: @bIsActive bit
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_Insert]
	@sSupplierType varchar(250),
	@bIsActive bit,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierType]
(
	[SupplierType],
	[IsActive]
)
VALUES
(
	@sSupplierType,
	ISNULL(@bIsActive, ((1)))
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierType''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierType],
	[IsActive]
FROM [dbo].[SupplierType]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierType''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierType],
	[IsActive]
FROM [dbo].[SupplierType]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierType''
-- Gets: @iID int
-- Gets: @sSupplierType varchar(250)
-- Gets: @bIsActive bit
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_Update]
	@iID int,
	@sSupplierType varchar(250),
	@bIsActive bit,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierType]
SET 
	[SupplierType] = @sSupplierType,
	[IsActive] = @bIsActive
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Delete]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierVanHireRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierVanHireRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierVanHireRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierVanHireRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Insert]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierVanHireRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sNoOfSeater nvarchar(10)
-- Gets: @sMakeAndModel nvarchar(50)
-- Gets: @sRatePerDay nvarchar(10)
-- Gets: @sAgeofVanInGeneral nvarchar(50)
-- Gets: @sIdealNoOfPassengers nvarchar(10)
-- Gets: @sMaxNoWithoutluggage nvarchar(10)
-- Gets: @sMaxNoWithluggage nvarchar(10)
-- Gets: @sInsurancePolicy nvarchar(50)
-- Gets: @sAccessFeeInsurance nvarchar(10)
-- Gets: @sDriverRequirement nvarchar(150)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sNoOfSeater nvarchar(10),
	@sMakeAndModel nvarchar(50),
	@sRatePerDay nvarchar(10),
	@sAgeofVanInGeneral nvarchar(50),
	@sIdealNoOfPassengers nvarchar(10),
	@sMaxNoWithoutluggage nvarchar(10),
	@sMaxNoWithluggage nvarchar(10),
	@sInsurancePolicy nvarchar(50),
	@sAccessFeeInsurance nvarchar(10),
	@sDriverRequirement nvarchar(150),
	@biFileData varbinary(MAX),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierVanHireRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sNoOfSeater,
	@sMakeAndModel,
	@sRatePerDay,
	@sAgeofVanInGeneral,
	@sIdealNoOfPassengers,
	@sMaxNoWithoutluggage,
	@sMaxNoWithluggage,
	@sInsurancePolicy,
	@sAccessFeeInsurance,
	@sDriverRequirement,
	@biFileData,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAll]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierVanHireRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierVanHireRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierVanHireRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierVanHireRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectOne]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierVanHireRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierVanHireRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Update]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierVanHireRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sNoOfSeater nvarchar(10)
-- Gets: @sMakeAndModel nvarchar(50)
-- Gets: @sRatePerDay nvarchar(10)
-- Gets: @sAgeofVanInGeneral nvarchar(50)
-- Gets: @sIdealNoOfPassengers nvarchar(10)
-- Gets: @sMaxNoWithoutluggage nvarchar(10)
-- Gets: @sMaxNoWithluggage nvarchar(10)
-- Gets: @sInsurancePolicy nvarchar(50)
-- Gets: @sAccessFeeInsurance nvarchar(10)
-- Gets: @sDriverRequirement nvarchar(150)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sNoOfSeater nvarchar(10),
	@sMakeAndModel nvarchar(50),
	@sRatePerDay nvarchar(10),
	@sAgeofVanInGeneral nvarchar(50),
	@sIdealNoOfPassengers nvarchar(10),
	@sMaxNoWithoutluggage nvarchar(10),
	@sMaxNoWithluggage nvarchar(10),
	@sInsurancePolicy nvarchar(50),
	@sAccessFeeInsurance nvarchar(10),
	@sDriverRequirement nvarchar(150),
	@biFileData varbinary(MAX),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierVanHireRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[NoOfSeater] = @sNoOfSeater,
	[MakeAndModel] = @sMakeAndModel,
	[RatePerDay] = @sRatePerDay,
	[AgeofVanInGeneral] = @sAgeofVanInGeneral,
	[IdealNoOfPassengers] = @sIdealNoOfPassengers,
	[MaxNoWithoutluggage] = @sMaxNoWithoutluggage,
	[MaxNoWithluggage] = @sMaxNoWithluggage,
	[InsurancePolicy] = @sInsurancePolicy,
	[AccessFeeInsurance] = @sAccessFeeInsurance,
	[DriverRequirement] = @sDriverRequirement,
	[FileData] = @biFileData,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierVanHireRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierVanHireRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  Table [dbo].[SupplierContacts]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierContacts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierContacts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](510) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierContacts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierFerryRate]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierFerryRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[CountryFrom] [nvarchar](50) NULL,
	[PortFrom] [nvarchar](50) NULL,
	[CountryTo] [nvarchar](50) NULL,
	[PortTo] [nvarchar](50) NULL,
	[DepartTime] [datetime] NULL,
	[ArrivalTime] [datetime] NULL,
	[OneWayOrReturn] [bit] NULL,
	[Description] [nvarchar](max) NULL,
	[FitRate] [nvarchar](10) NULL,
	[GroupRate] [nvarchar](10) NULL,
	[Remark] [nvarchar](max) NULL,
	[Cabin] [int] NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierFerryRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierHotelRate]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierHotelRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[HalfTwin] [nvarchar](150) NULL,
	[Single] [nvarchar](50) NULL,
	[Twin_DoubleForSoleUse] [nvarchar](50) NULL,
	[Triple] [nvarchar](50) NULL,
	[FamilyRoom] [nvarchar](50) NULL,
	[Breakfast] [nvarchar](50) NULL,
	[DinnerAtHotel] [nvarchar](50) NULL,
	[RatePerPerson] [nvarchar](10) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierHotelRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierInformation]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInformation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierInformation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[Currency] [nvarchar](10) NULL,
	[NoofBedRooms] [int] NULL,
	[HotelFacility] [nvarchar](max) NULL,
	[Parking] [nvarchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierInformation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierInsuranceRate]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierInsuranceRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[RateDescription] [nvarchar](max) NULL,
	[Rate] [nvarchar](10) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierInsuranceRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierMaster]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierTypeID] [int] NULL,
	[Name] [nvarchar](250) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[Email] [nvarchar](50) NULL,
	[CreditToLetsTravel] [nvarchar](50) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[ContractEndingDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierMealSupplement]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierMealSupplement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierTypeID] [int] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[Purpose] [nvarchar](max) NULL,
	[MealSupplementType] [nvarchar](50) NULL,
	[UK] [decimal](18, 0) NULL,
	[Ireland] [decimal](18, 0) NULL,
	[Europe] [decimal](18, 0) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierMealSupplement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SuppliersDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierMasterID] [int] NULL,
	[SupplierTypeID] [int] NULL,
	[SupplierDate] [datetime] NULL,
	[ContractedBy] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[StarRating] [nvarchar](10) NULL,
	[Website] [nvarchar](150) NULL,
	[Agency] [nvarchar](100) NULL,
	[ClientCode] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[FaxNumber] [nvarchar](15) NULL,
	[EmergencyNo] [nvarchar](15) NULL,
	[Email] [nvarchar](50) NULL,
	[AlternateEmail] [nvarchar](50) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[HotelChain] [nvarchar](50) NULL,
	[Franchised] [nvarchar](50) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[CreditToLetsTravel] [nvarchar](50) NULL,
	[BlackList] [nvarchar](200) NULL,
	[ContractStartDate] [datetime] NULL,
	[ContractEndingDate] [datetime] NULL,
	[ContractRenewalDate] [datetime] NULL,
	[Rating] [nvarchar](10) NULL,
	[MainBusiness] [nvarchar](200) NULL,
	[Comments] [nvarchar](max) NULL,
	[FileData] [varbinary](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SuppliersDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTheaterRate]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTheaterRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[Rate] [nvarchar](10) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTheaterRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierTrainRate]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTrainRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[RouteFrom] [nvarchar](150) NULL,
	[RouteTo] [nvarchar](150) NULL,
	[Rate] [nvarchar](10) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTrainRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierVanHireRate]    Script Date: 06-04-2017 15:01:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierVanHireRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[NoOfSeater] [nvarchar](10) NULL,
	[MakeAndModel] [nvarchar](50) NULL,
	[RatePerDay] [nvarchar](10) NULL,
	[AgeofVanInGeneral] [nvarchar](50) NULL,
	[IdealNoOfPassengers] [nvarchar](10) NULL,
	[MaxNoWithoutluggage] [nvarchar](10) NULL,
	[MaxNoWithluggage] [nvarchar](10) NULL,
	[InsurancePolicy] [nvarchar](50) NULL,
	[AccessFeeInsurance] [nvarchar](10) NULL,
	[DriverRequirement] [nvarchar](150) NULL,
	[FileData] [varbinary](max) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierVanHireRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts]  WITH CHECK ADD  CONSTRAINT [FK_SupplierContacts_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts] CHECK CONSTRAINT [FK_SupplierContacts_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate] CHECK CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate] CHECK CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation]  WITH CHECK ADD  CONSTRAINT [FK_SupplierInformation_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation] CHECK CONSTRAINT [FK_SupplierInformation_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate] CHECK CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster]  WITH CHECK ADD  CONSTRAINT [FK_SupplierMaster_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster] CHECK CONSTRAINT [FK_SupplierMaster_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement]  WITH CHECK ADD  CONSTRAINT [FK_SupplierMealSupplement_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement] CHECK CONSTRAINT [FK_SupplierMealSupplement_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails]  WITH CHECK ADD  CONSTRAINT [FK_SuppliersDetails_SupplierMaster] FOREIGN KEY([SupplierMasterID])
REFERENCES [dbo].[SupplierMaster] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] CHECK CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails]  WITH CHECK ADD  CONSTRAINT [FK_SuppliersDetails_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] CHECK CONSTRAINT [FK_SuppliersDetails_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate] CHECK CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate] CHECK CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate] CHECK CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'FitRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FIT Rate (less than 10 people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'FitRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'GroupRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Rate (more than 10 people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'GroupRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'Cabin'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cabin ( 4people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'Cabin'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'HalfTwin'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half Twin(2 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'HalfTwin'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Single'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Single ( 1 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Single'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Twin_DoubleForSoleUse'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Twin/Double for sole use ( 1 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Twin_DoubleForSoleUse'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Triple'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Triple ( 2 Adults + 1 Child)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Triple'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'FamilyRoom'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Triple ( 2 Adults + 1 Child)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'FamilyRoom'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Breakfast'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Family Room ( Adults + 1 Child) ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Breakfast'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierMaster', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierMaster', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
