 
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SupplierType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SupplierType] DROP CONSTRAINT [DF_SupplierType_IsActive]
END

GO
/****** Object:  Table [dbo].[SupplierType]    Script Date: 19-05-2017 17:38:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierType]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierType]
GO
/****** Object:  Table [dbo].[SupplierType]    Script Date: 19-05-2017 17:38:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierType] [varchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SupplierType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[SupplierType] ON 

GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (1, N'Hotel', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (2, N'Hotel Wholesaler', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (3, N'Van Hire', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (4, N'Coach Hire', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (5, N'Tour Guide', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (6, N'Entrance', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (7, N'Restaurant', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (8, N'Theatre ', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (9, N'Ferry', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (10, N'Train', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (11, N'Flight', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (12, N'DMC', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (13, N'Insurance', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (14, N'Tip', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (15, N'Meal Supplements for driver and guide', 1)
GO
SET IDENTITY_INSERT [dbo].[SupplierType] OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SupplierType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SupplierType] ADD  CONSTRAINT [DF_SupplierType_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
