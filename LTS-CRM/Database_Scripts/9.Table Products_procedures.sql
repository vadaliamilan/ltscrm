 
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDocument_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDocument]'))
ALTER TABLE [dbo].[ProductDocument] DROP CONSTRAINT [FK_ProductDocument_Product]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDeliveryDetail_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDeliveryDetail]'))
ALTER TABLE [dbo].[ProductDeliveryDetail] DROP CONSTRAINT [FK_ProductDeliveryDetail_Product]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductContact_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductContact]'))
ALTER TABLE [dbo].[ProductContact] DROP CONSTRAINT [FK_ProductContact_Product]
GO
/****** Object:  Table [dbo].[ProductDocument]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductDocument]') AND type in (N'U'))
DROP TABLE [dbo].[ProductDocument]
GO
/****** Object:  Table [dbo].[ProductDeliveryDetail]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductDeliveryDetail]') AND type in (N'U'))
DROP TABLE [dbo].[ProductDeliveryDetail]
GO
/****** Object:  Table [dbo].[ProductContact]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductContact]') AND type in (N'U'))
DROP TABLE [dbo].[ProductContact]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
DROP TABLE [dbo].[Product]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Update]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Insert]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Delete]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Update]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Insert]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Delete]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Update]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAllWProductlIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Insert]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Delete]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Update]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Insert]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Delete]    Script Date: 04-05-2017 17:52:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Delete]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''Product''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[Product]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Insert]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''Product''
-- Gets: @daCreationDate datetime
-- Gets: @sContractedBy nvarchar(250)
-- Gets: @sDataEntryBy nvarchar(250)
-- Gets: @sProductType nvarchar(250)
-- Gets: @iSupplierId int
-- Gets: @sSupplierName nvarchar(250)
-- Gets: @sContract nvarchar(250)
-- Gets: @sContractWith nvarchar(250)
-- Gets: @sEmergencyNo nvarchar(250)
-- Gets: @sReservationEmail nvarchar(250)
-- Gets: @sAlternationEmail nvarchar(250)
-- Gets: @sCurrency nvarchar(250)
-- Gets: @sContractType nvarchar(250)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sCountry nvarchar(250)
-- Gets: @sRegion nvarchar(250)
-- Gets: @sTypeOfProduct nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sTitle nvarchar(250)
-- Gets: @dcPrice decimal(18, 2)
-- Gets: @sComment nvarchar(MAX)
-- Gets: @sHowToBook nvarchar(MAX)
-- Gets: @sPriceIncluding nvarchar(MAX)
-- Gets: @sPriceExcluding nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Insert]
	@daCreationDate datetime,
	@sContractedBy nvarchar(250),
	@sDataEntryBy nvarchar(250),
	@sProductType nvarchar(250),
	@iSupplierId int,
	@sSupplierName nvarchar(250),
	@sContract nvarchar(250),
	@sContractWith nvarchar(250),
	@sEmergencyNo nvarchar(250),
	@sReservationEmail nvarchar(250),
	@sAlternationEmail nvarchar(250),
	@sCurrency nvarchar(250),
	@sContractType nvarchar(250),
	@dcNetRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sCountry nvarchar(250),
	@sRegion nvarchar(250),
	@sTypeOfProduct nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sTitle nvarchar(250),
	@dcPrice decimal(18, 2),
	@sComment nvarchar(MAX),
	@sHowToBook nvarchar(MAX),
	@sPriceIncluding nvarchar(MAX),
	@sPriceExcluding nvarchar(MAX),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[Product]
(
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[Title],
	[Price],
	[Comment],
	[HowToBook],
	[PriceIncluding],
	[PriceExcluding],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@daCreationDate,
	@sContractedBy,
	@sDataEntryBy,
	@sProductType,
	@iSupplierId,
	@sSupplierName,
	@sContract,
	@sContractWith,
	@sEmergencyNo,
	@sReservationEmail,
	@sAlternationEmail,
	@sCurrency,
	@sContractType,
	@dcNetRate,
	@dcCommission,
	@sCountry,
	@sRegion,
	@sTypeOfProduct,
	@iNoOfDays,
	@iNoOfNights,
	@sTitle,
	@dcPrice,
	@sComment,
	@sHowToBook,
	@sPriceIncluding,
	@sPriceExcluding,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''Product''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[Title],
	[Price],
	[Comment],
	[HowToBook],
	[PriceIncluding],
	[PriceExcluding],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''Product''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[Title],
	[Price],
	[Comment],
	[HowToBook],
	[PriceIncluding],
	[PriceExcluding],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Update]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''Product''
-- Gets: @iID int
-- Gets: @daCreationDate datetime
-- Gets: @sContractedBy nvarchar(250)
-- Gets: @sDataEntryBy nvarchar(250)
-- Gets: @sProductType nvarchar(250)
-- Gets: @iSupplierId int
-- Gets: @sSupplierName nvarchar(250)
-- Gets: @sContract nvarchar(250)
-- Gets: @sContractWith nvarchar(250)
-- Gets: @sEmergencyNo nvarchar(250)
-- Gets: @sReservationEmail nvarchar(250)
-- Gets: @sAlternationEmail nvarchar(250)
-- Gets: @sCurrency nvarchar(250)
-- Gets: @sContractType nvarchar(250)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sCountry nvarchar(250)
-- Gets: @sRegion nvarchar(250)
-- Gets: @sTypeOfProduct nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sTitle nvarchar(250)
-- Gets: @dcPrice decimal(18, 2)
-- Gets: @sComment nvarchar(MAX)
-- Gets: @sHowToBook nvarchar(MAX)
-- Gets: @sPriceIncluding nvarchar(MAX)
-- Gets: @sPriceExcluding nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Update]
	@iID int,
	@daCreationDate datetime,
	@sContractedBy nvarchar(250),
	@sDataEntryBy nvarchar(250),
	@sProductType nvarchar(250),
	@iSupplierId int,
	@sSupplierName nvarchar(250),
	@sContract nvarchar(250),
	@sContractWith nvarchar(250),
	@sEmergencyNo nvarchar(250),
	@sReservationEmail nvarchar(250),
	@sAlternationEmail nvarchar(250),
	@sCurrency nvarchar(250),
	@sContractType nvarchar(250),
	@dcNetRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sCountry nvarchar(250),
	@sRegion nvarchar(250),
	@sTypeOfProduct nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sTitle nvarchar(250),
	@dcPrice decimal(18, 2),
	@sComment nvarchar(MAX),
	@sHowToBook nvarchar(MAX),
	@sPriceIncluding nvarchar(MAX),
	@sPriceExcluding nvarchar(MAX),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Product]
SET 
	[CreationDate] = @daCreationDate,
	[ContractedBy] = @sContractedBy,
	[DataEntryBy] = @sDataEntryBy,
	[ProductType] = @sProductType,
	[SupplierId] = @iSupplierId,
	[SupplierName] = @sSupplierName,
	[Contract] = @sContract,
	[ContractWith] = @sContractWith,
	[EmergencyNo] = @sEmergencyNo,
	[ReservationEmail] = @sReservationEmail,
	[AlternationEmail] = @sAlternationEmail,
	[Currency] = @sCurrency,
	[ContractType] = @sContractType,
	[NetRate] = @dcNetRate,
	[Commission] = @dcCommission,
	[Country] = @sCountry,
	[Region] = @sRegion,
	[TypeOfProduct] = @sTypeOfProduct,
	[NoOfDays] = @iNoOfDays,
	[NoOfNights] = @iNoOfNights,
	[Title] = @sTitle,
	[Price] = @dcPrice,
	[Comment] = @sComment,
	[HowToBook] = @sHowToBook,
	[PriceIncluding] = @sPriceIncluding,
	[PriceExcluding] = @sPriceExcluding,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Delete]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ProductContact''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ProductContact]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ProductContact''
-- based on a foreign key field.
-- Gets: @iProductlID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]
	@iProductlID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ProductContact]
WHERE
	[ProductlID] = @iProductlID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Insert]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ProductContact''
-- Gets: @iProductlID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_Insert]
	@iProductlID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ProductContact]
(
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iProductlID,
	@sTitle,
	@sSurName,
	@sFirstName,
	@bMainContact,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ProductContact''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductContact]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAllWProductlIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ProductContact''
-- based on a foreign key field.
-- Gets: @iProductlID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]
	@iProductlID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductContact]
WHERE
	[ProductlID] = @iProductlID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ProductContact''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductContact]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Update]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ProductContact''
-- Gets: @iID int
-- Gets: @iProductlID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_Update]
	@iID int,
	@iProductlID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductContact]
SET 
	[ProductlID] = @iProductlID,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[MainContact] = @bMainContact,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ProductContact''.
-- Will reset field [ProductlID] with value @iProductlIDOld  to value @iProductlID
-- Gets: @iProductlID int
-- Gets: @iProductlIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]
	@iProductlID int,
	@iProductlIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductContact]
SET
	[ProductlID] = @iProductlID
WHERE
	[ProductlID] = @iProductlIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Delete]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ProductDeliveryDetail''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ProductDeliveryDetail''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Insert]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ProductDeliveryDetail''
-- Gets: @iProductID int
-- Gets: @sRoute nvarchar(250)
-- Gets: @sItinerary nvarchar(MAX)
-- Gets: @sHotel nvarchar(250)
-- Gets: @sBreakFast nvarchar(250)
-- Gets: @sLunch nchar(10)
-- Gets: @sDinner nchar(10)
-- Gets: @sTransportation nchar(10)
-- Gets: @biPhotoData varbinary(MAX)
-- Gets: @biPhotoData1 varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_Insert]
	@iProductID int,
	@sRoute nvarchar(250),
	@sItinerary nvarchar(MAX),
	@sHotel nvarchar(250),
	@sBreakFast nvarchar(250),
	@sLunch nchar(10),
	@sDinner nchar(10),
	@sTransportation nchar(10),
	@biPhotoData varbinary(MAX),
	@biPhotoData1 varbinary(MAX),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ProductDeliveryDetail]
(
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iProductID,
	@sRoute,
	@sItinerary,
	@sHotel,
	@sBreakFast,
	@sLunch,
	@sDinner,
	@sTransportation,
	@biPhotoData,
	@biPhotoData1,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ProductDeliveryDetail''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDeliveryDetail]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ProductDeliveryDetail''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ProductDeliveryDetail''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Update]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ProductDeliveryDetail''
-- Gets: @iID int
-- Gets: @iProductID int
-- Gets: @sRoute nvarchar(250)
-- Gets: @sItinerary nvarchar(MAX)
-- Gets: @sHotel nvarchar(250)
-- Gets: @sBreakFast nvarchar(250)
-- Gets: @sLunch nchar(10)
-- Gets: @sDinner nchar(10)
-- Gets: @sTransportation nchar(10)
-- Gets: @biPhotoData varbinary(MAX)
-- Gets: @biPhotoData1 varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_Update]
	@iID int,
	@iProductID int,
	@sRoute nvarchar(250),
	@sItinerary nvarchar(MAX),
	@sHotel nvarchar(250),
	@sBreakFast nvarchar(250),
	@sLunch nchar(10),
	@sDinner nchar(10),
	@sTransportation nchar(10),
	@biPhotoData varbinary(MAX),
	@biPhotoData1 varbinary(MAX),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDeliveryDetail]
SET 
	[ProductID] = @iProductID,
	[Route] = @sRoute,
	[Itinerary] = @sItinerary,
	[Hotel] = @sHotel,
	[BreakFast] = @sBreakFast,
	[Lunch] = @sLunch,
	[Dinner] = @sDinner,
	[Transportation] = @sTransportation,
	[PhotoData] = @biPhotoData,
	[PhotoData1] = @biPhotoData1,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ProductDeliveryDetail''.
-- Will reset field [ProductID] with value @iProductIDOld  to value @iProductID
-- Gets: @iProductID int
-- Gets: @iProductIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]
	@iProductID int,
	@iProductIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDeliveryDetail]
SET
	[ProductID] = @iProductID
WHERE
	[ProductID] = @iProductIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Delete]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ProductDocument''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ProductDocument]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ProductDocument''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ProductDocument]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Insert]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ProductDocument''
-- Gets: @iProductID int
-- Gets: @sFileName nvarchar(250)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_Insert]
	@iProductID int,
	@sFileName nvarchar(250),
	@biFileData varbinary(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ProductDocument]
(
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iProductID,
	@sFileName,
	@biFileData,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAll]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ProductDocument''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDocument]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ProductDocument''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDocument]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectOne]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ProductDocument''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDocument]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Update]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ProductDocument''
-- Gets: @iID int
-- Gets: @iProductID int
-- Gets: @sFileName nvarchar(250)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_Update]
	@iID int,
	@iProductID int,
	@sFileName nvarchar(250),
	@biFileData varbinary(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDocument]
SET 
	[ProductID] = @iProductID,
	[FileName] = @sFileName,
	[FileData] = @biFileData,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ProductDocument''.
-- Will reset field [ProductID] with value @iProductIDOld  to value @iProductID
-- Gets: @iProductID int
-- Gets: @iProductIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]
	@iProductID int,
	@iProductIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDocument]
SET
	[ProductID] = @iProductID
WHERE
	[ProductID] = @iProductIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  Table [dbo].[Product]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NULL,
	[ContractedBy] [nvarchar](250) NULL,
	[DataEntryBy] [nvarchar](250) NULL,
	[ProductType] [nvarchar](250) NULL,
	[SupplierId] [int] NULL,
	[SupplierName] [nvarchar](250) NULL,
	[Contract] [nvarchar](250) NULL,
	[ContractWith] [nvarchar](250) NULL,
	[EmergencyNo] [nvarchar](250) NULL,
	[ReservationEmail] [nvarchar](250) NULL,
	[AlternationEmail] [nvarchar](250) NULL,
	[Currency] [nvarchar](250) NULL,
	[ContractType] [nvarchar](250) NULL,
	[NetRate] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Country] [nvarchar](250) NULL,
	[Region] [nvarchar](250) NULL,
	[TypeOfProduct] [nvarchar](50) NULL,
	[NoOfDays] [int] NULL,
	[NoOfNights] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[Price] [decimal](18, 2) NULL,
	[Comment] [nvarchar](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[PriceIncluding] [nvarchar](max) NULL,
	[PriceExcluding] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](250) NULL,
	[ExtraField2] [nvarchar](250) NULL,
	[ExtraField3] [nvarchar](250) NULL,
	[ExtraField4] [nvarchar](250) NULL,
	[ExtraField5] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductContact]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductContact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductlID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](510) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductContact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductDeliveryDetail]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductDeliveryDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductDeliveryDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[Route] [nvarchar](250) NULL,
	[Itinerary] [nvarchar](max) NULL,
	[Hotel] [nvarchar](250) NULL,
	[BreakFast] [nvarchar](250) NULL,
	[Lunch] [nchar](10) NULL,
	[Dinner] [nchar](10) NULL,
	[Transportation] [nchar](10) NULL,
	[PhotoData] [varbinary](max) NULL,
	[PhotoData1] [varbinary](max) NULL,
	[ExtraField1] [nvarchar](250) NULL,
	[ExtraField2] [nvarchar](250) NULL,
	[ExtraField3] [nvarchar](250) NULL,
	[ExtraField4] [nvarchar](250) NULL,
	[ExtraField5] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductDeliveryDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductDocument]    Script Date: 04-05-2017 17:52:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductDocument]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductDocument](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[FileName] [nvarchar](250) NULL,
	[FileData] [varbinary](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductDocument] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductContact_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductContact]'))
ALTER TABLE [dbo].[ProductContact]  WITH CHECK ADD  CONSTRAINT [FK_ProductContact_Product] FOREIGN KEY([ProductlID])
REFERENCES [dbo].[Product] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductContact_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductContact]'))
ALTER TABLE [dbo].[ProductContact] CHECK CONSTRAINT [FK_ProductContact_Product]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDeliveryDetail_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDeliveryDetail]'))
ALTER TABLE [dbo].[ProductDeliveryDetail]  WITH CHECK ADD  CONSTRAINT [FK_ProductDeliveryDetail_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDeliveryDetail_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDeliveryDetail]'))
ALTER TABLE [dbo].[ProductDeliveryDetail] CHECK CONSTRAINT [FK_ProductDeliveryDetail_Product]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDocument_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDocument]'))
ALTER TABLE [dbo].[ProductDocument]  WITH CHECK ADD  CONSTRAINT [FK_ProductDocument_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDocument_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDocument]'))
ALTER TABLE [dbo].[ProductDocument] CHECK CONSTRAINT [FK_ProductDocument_Product]
GO
