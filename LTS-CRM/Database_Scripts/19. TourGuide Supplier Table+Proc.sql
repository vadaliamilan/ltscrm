IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'TourGuidePhotos', N'COLUMN',N'Photo'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TourGuidePhotos', @level2type=N'COLUMN',@level2name=N'Photo'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_Without_vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_Without_vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_tips'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_tips'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_Day_coach_Guide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_Day_coach_Guide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_tips'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_tips'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_coach_Guide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_coach_Guide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'ToDate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'ToDate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'FromDate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'FromDate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Season'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Season'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'HowToBook'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'HowToBook'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'CVOfGuide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'CVOfGuide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'GuideLicence'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'GuideLicence'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AddressProof'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AddressProof'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'PassportCopy'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'PassportCopy'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'VehiclePhoto'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'VehiclePhoto'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'DrivingLicence'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'DrivingLicence'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'Comments'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'Comments'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpecialRequirement'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpecialRequirement'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupNotWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupNotWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'TypeOfWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'TypeOfWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AreaOfWorking'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AreaOfWorking'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'NoOfYearTourGuide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'NoOfYearTourGuide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpeakingLanguage'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpeakingLanguage'

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TourGuidePhotos_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[TourGuidePhotos]'))
ALTER TABLE [dbo].[TourGuidePhotos] DROP CONSTRAINT [FK_TourGuidePhotos_SupplierTourGuide]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate] DROP CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide] DROP CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid]
GO
/****** Object:  Table [dbo].[TourGuidePhotos]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TourGuidePhotos]') AND type in (N'U'))
DROP TABLE [dbo].[TourGuidePhotos]
GO
/****** Object:  Table [dbo].[SupplierTourGuideRate]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTourGuideRate]
GO
/****** Object:  Table [dbo].[SupplierTourGuide]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTourGuide]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_UpdateAllWTourGuideIdLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_UpdateAllWTourGuideIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_UpdateAllWTourGuideIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_Update]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_SelectOne]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_SelectAllWTourGuideIdLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_SelectAllWTourGuideIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_SelectAllWTourGuideIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_SelectAll]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_Insert]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_DeleteAllWTourGuideIdLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_DeleteAllWTourGuideIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_DeleteAllWTourGuideIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_Delete]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TourGuidePhotos_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Update]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectOne]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAll]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Insert]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Delete]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Update]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectOne]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAll]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Insert]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Delete]    Script Date: 07-06-2017 20:04:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Delete]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTourGuide''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTourGuide''
-- based on a foreign key field.
-- Gets: @iTypeOfSupplier int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTourGuide]
WHERE
	[TypeOfSupplier] = @iTypeOfSupplier
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Insert]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTourGuide''
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @iTypeOfSupplier int
-- Gets: @sRecruitedBy nvarchar(250)
-- Gets: @sSurName nvarchar(250)
-- Gets: @sSurNameInChinese nvarchar(250)
-- Gets: @sFirstName nvarchar(250)
-- Gets: @sFirstNameInChinese nvarchar(250)
-- Gets: @sSex nvarchar(10)
-- Gets: @daDateOfBirth datetime
-- Gets: @sOrigin nvarchar(50)
-- Gets: @sNationality nvarchar(50)
-- Gets: @sMarried nvarchar(50)
-- Gets: @iChildren int
-- Gets: @sVisaStatus nvarchar(250)
-- Gets: @sHomePhone nvarchar(15)
-- Gets: @sMobilePhone nvarchar(15)
-- Gets: @sWeChat nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(500)
-- Gets: @sAddress2 nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(10)
-- Gets: @sSpeakingLanguage nvarchar(1000)
-- Gets: @sGuideLicenceInit nvarchar(50)
-- Gets: @iNoOfYearTourGuide int
-- Gets: @sAreaOfWorking nvarchar(250)
-- Gets: @sTypeOfWork nvarchar(250)
-- Gets: @sVehicleOwned nvarchar(250)
-- Gets: @sKindOfGroupWork nvarchar(Max)
-- Gets: @sKindOfGroupNotWork nvarchar(150)
-- Gets: @sSpecialRequirement nvarchar(Max)
-- Gets: @iRating int
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biDrivingLicence varbinary(Max)
-- Gets: @biVehiclePhoto varbinary(Max)
-- Gets: @biPassportCopy varbinary(Max)
-- Gets: @biAddressProof varbinary(Max)
-- Gets: @biGuideLicence varbinary(Max)
-- Gets: @biCVOfGuide varbinary(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(Max)
-- Gets: @sExtraField2 nvarchar(Max)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Insert]
	@iCompanyID int,
	@iSupplierTypeId int,
	@iTypeOfSupplier int,
	@sRecruitedBy nvarchar(250),
	@sSurName nvarchar(250),
	@sSurNameInChinese nvarchar(250),
	@sFirstName nvarchar(250),
	@sFirstNameInChinese nvarchar(250),
	@sSex nvarchar(10),
	@daDateOfBirth datetime,
	@sOrigin nvarchar(50),
	@sNationality nvarchar(50),
	@sMarried nvarchar(50),
	@iChildren int,
	@sVisaStatus nvarchar(250),
	@sHomePhone nvarchar(15),
	@sMobilePhone nvarchar(15),
	@sWeChat nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(500),
	@sAddress2 nvarchar(500),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(10),
	@sSpeakingLanguage nvarchar(1000),
	@sGuideLicenceInit nvarchar(50),
	@iNoOfYearTourGuide int,
	@sAreaOfWorking nvarchar(250),
	@sTypeOfWork nvarchar(250),
	@sVehicleOwned nvarchar(250),
	@sKindOfGroupWork nvarchar(Max),
	@sKindOfGroupNotWork nvarchar(150),
	@sSpecialRequirement nvarchar(Max),
	@iRating int,
	@sBlackList nvarchar(50),
	@sComments nvarchar(Max),
	@biDrivingLicence varbinary(Max),
	@biVehiclePhoto varbinary(Max),
	@biPassportCopy varbinary(Max),
	@biAddressProof varbinary(Max),
	@biGuideLicence varbinary(Max),
	@biCVOfGuide varbinary(Max),
	@sHowToBook nvarchar(Max),
	@sExtraField1 nvarchar(Max),
	@sExtraField2 nvarchar(Max),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTourGuide]
(
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[Sex],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[GuideLicenceInit],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierTypeId,
	@iTypeOfSupplier,
	@sRecruitedBy,
	@sSurName,
	@sSurNameInChinese,
	@sFirstName,
	@sFirstNameInChinese,
	@sSex,
	@daDateOfBirth,
	@sOrigin,
	@sNationality,
	@sMarried,
	@iChildren,
	@sVisaStatus,
	@sHomePhone,
	@sMobilePhone,
	@sWeChat,
	@sEmail,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sCountry,
	@sPostCode,
	@sSpeakingLanguage,
	@sGuideLicenceInit,
	@iNoOfYearTourGuide,
	@sAreaOfWorking,
	@sTypeOfWork,
	@sVehicleOwned,
	@sKindOfGroupWork,
	@sKindOfGroupNotWork,
	@sSpecialRequirement,
	@iRating,
	@sBlackList,
	@sComments,
	@biDrivingLicence,
	@biVehiclePhoto,
	@biPassportCopy,
	@biAddressProof,
	@biGuideLicence,
	@biCVOfGuide,
	@sHowToBook,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAll]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTourGuide''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[Sex],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[GuideLicenceInit],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTourGuide''
-- based on a foreign key field.
-- Gets: @iTypeOfSupplier int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[Sex],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[GuideLicenceInit],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
WHERE
	[TypeOfSupplier] = @iTypeOfSupplier
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectOne]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTourGuide''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[Sex],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[GuideLicenceInit],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[HowToBook],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Update]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTourGuide''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @iTypeOfSupplier int
-- Gets: @sRecruitedBy nvarchar(250)
-- Gets: @sSurName nvarchar(250)
-- Gets: @sSurNameInChinese nvarchar(250)
-- Gets: @sFirstName nvarchar(250)
-- Gets: @sFirstNameInChinese nvarchar(250)
-- Gets: @sSex nvarchar(10)
-- Gets: @daDateOfBirth datetime
-- Gets: @sOrigin nvarchar(50)
-- Gets: @sNationality nvarchar(50)
-- Gets: @sMarried nvarchar(50)
-- Gets: @iChildren int
-- Gets: @sVisaStatus nvarchar(250)
-- Gets: @sHomePhone nvarchar(15)
-- Gets: @sMobilePhone nvarchar(15)
-- Gets: @sWeChat nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(500)
-- Gets: @sAddress2 nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(10)
-- Gets: @sSpeakingLanguage nvarchar(1000)
-- Gets: @sGuideLicenceInit nvarchar(50)
-- Gets: @iNoOfYearTourGuide int
-- Gets: @sAreaOfWorking nvarchar(250)
-- Gets: @sTypeOfWork nvarchar(250)
-- Gets: @sVehicleOwned nvarchar(250)
-- Gets: @sKindOfGroupWork nvarchar(Max)
-- Gets: @sKindOfGroupNotWork nvarchar(150)
-- Gets: @sSpecialRequirement nvarchar(Max)
-- Gets: @iRating int
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biDrivingLicence varbinary(Max)
-- Gets: @biVehiclePhoto varbinary(Max)
-- Gets: @biPassportCopy varbinary(Max)
-- Gets: @biAddressProof varbinary(Max)
-- Gets: @biGuideLicence varbinary(Max)
-- Gets: @biCVOfGuide varbinary(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(Max)
-- Gets: @sExtraField2 nvarchar(Max)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierTypeId int,
	@iTypeOfSupplier int,
	@sRecruitedBy nvarchar(250),
	@sSurName nvarchar(250),
	@sSurNameInChinese nvarchar(250),
	@sFirstName nvarchar(250),
	@sFirstNameInChinese nvarchar(250),
	@sSex nvarchar(10),
	@daDateOfBirth datetime,
	@sOrigin nvarchar(50),
	@sNationality nvarchar(50),
	@sMarried nvarchar(50),
	@iChildren int,
	@sVisaStatus nvarchar(250),
	@sHomePhone nvarchar(15),
	@sMobilePhone nvarchar(15),
	@sWeChat nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(500),
	@sAddress2 nvarchar(500),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(10),
	@sSpeakingLanguage nvarchar(1000),
	@sGuideLicenceInit nvarchar(50),
	@iNoOfYearTourGuide int,
	@sAreaOfWorking nvarchar(250),
	@sTypeOfWork nvarchar(250),
	@sVehicleOwned nvarchar(250),
	@sKindOfGroupWork nvarchar(Max),
	@sKindOfGroupNotWork nvarchar(150),
	@sSpecialRequirement nvarchar(Max),
	@iRating int,
	@sBlackList nvarchar(50),
	@sComments nvarchar(Max),
	@biDrivingLicence varbinary(Max),
	@biVehiclePhoto varbinary(Max),
	@biPassportCopy varbinary(Max),
	@biAddressProof varbinary(Max),
	@biGuideLicence varbinary(Max),
	@biCVOfGuide varbinary(Max),
	@sHowToBook nvarchar(Max),
	@sExtraField1 nvarchar(Max),
	@sExtraField2 nvarchar(Max),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuide]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierTypeId] = @iSupplierTypeId,
	[TypeOfSupplier] = @iTypeOfSupplier,
	[RecruitedBy] = @sRecruitedBy,
	[SurName] = @sSurName,
	[SurNameInChinese] = @sSurNameInChinese,
	[FirstName] = @sFirstName,
	[FirstNameInChinese] = @sFirstNameInChinese,
	[Sex] = @sSex,
	[DateOfBirth] = @daDateOfBirth,
	[Origin] = @sOrigin,
	[Nationality] = @sNationality,
	[Married] = @sMarried,
	[Children] = @iChildren,
	[VisaStatus] = @sVisaStatus,
	[HomePhone] = @sHomePhone,
	[MobilePhone] = @sMobilePhone,
	[WeChat] = @sWeChat,
	[Email] = @sEmail,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[Country] = @sCountry,
	[PostCode] = @sPostCode,
	[SpeakingLanguage] = @sSpeakingLanguage,
	[GuideLicenceInit] = @sGuideLicenceInit,
	[NoOfYearTourGuide] = @iNoOfYearTourGuide,
	[AreaOfWorking] = @sAreaOfWorking,
	[TypeOfWork] = @sTypeOfWork,
	[VehicleOwned] = @sVehicleOwned,
	[KindOfGroupWork] = @sKindOfGroupWork,
	[KindOfGroupNotWork] = @sKindOfGroupNotWork,
	[SpecialRequirement] = @sSpecialRequirement,
	[Rating] = @iRating,
	[BlackList] = @sBlackList,
	[Comments] = @sComments,
	[DrivingLicence] = @biDrivingLicence,
	[VehiclePhoto] = @biVehiclePhoto,
	[PassportCopy] = @biPassportCopy,
	[AddressProof] = @biAddressProof,
	[GuideLicence] = @biGuideLicence,
	[CVOfGuide] = @biCVOfGuide,
	[HowToBook] = @sHowToBook,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTourGuide''.
-- Will reset field [TypeOfSupplier] with value @iTypeOfSupplierOld  to value @iTypeOfSupplier
-- Gets: @iTypeOfSupplier int
-- Gets: @iTypeOfSupplierOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iTypeOfSupplierOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuide]
SET
	[TypeOfSupplier] = @iTypeOfSupplier
WHERE
	[TypeOfSupplier] = @iTypeOfSupplierOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Delete]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTourGuideRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTourGuideRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTourGuideRate''
-- based on a foreign key field.
-- Gets: @iSupplierTourGuideID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Insert]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTourGuideRate''
-- Gets: @iSupplierTourGuideID int
-- Gets: @sSeason nvarchar(150)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_coach_Guide decimal(10, 2)
-- Gets: @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_tips decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_Day_coach_Guide decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2)
-- Gets: @dcFull_day_tips decimal(10, 2)
-- Gets: @dcExtra_hour_With_Vehicle decimal(10, 2)
-- Gets: @dcExtra_hour_Without_vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_With_Vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_Without_Vehicle decimal(10, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_Insert]
	@iSupplierTourGuideID int,
	@sSeason nvarchar(150),
	@daFromDate datetime,
	@daToDate datetime,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_coach_Guide decimal(10, 2),
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_tips decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_Day_coach_Guide decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2),
	@dcFull_day_tips decimal(10, 2),
	@dcExtra_hour_With_Vehicle decimal(10, 2),
	@dcExtra_hour_Without_vehicle decimal(10, 2),
	@dcLondon_Edinburgh_With_Vehicle decimal(10, 2),
	@dcLondon_Edinburgh_Without_Vehicle decimal(10, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTourGuideRate]
(
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierTourGuideID,
	@sSeason,
	@daFromDate,
	@daToDate,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle,
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle,
	@dcHalf_day_coach_Guide,
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle,
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle,
	@dcHalf_day_tips,
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle,
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle,
	@dcFull_Day_coach_Guide,
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle,
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle,
	@dcFull_day_tips,
	@dcExtra_hour_With_Vehicle,
	@dcExtra_hour_Without_vehicle,
	@dcLondon_Edinburgh_With_Vehicle,
	@dcLondon_Edinburgh_Without_Vehicle,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAll]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTourGuideRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTourGuideRate''
-- based on a foreign key field.
-- Gets: @iSupplierTourGuideID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectOne]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTourGuideRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Update]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTourGuideRate''
-- Gets: @iID int
-- Gets: @iSupplierTourGuideID int
-- Gets: @sSeason nvarchar(150)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_coach_Guide decimal(10, 2)
-- Gets: @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_tips decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_Day_coach_Guide decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2)
-- Gets: @dcFull_day_tips decimal(10, 2)
-- Gets: @dcExtra_hour_With_Vehicle decimal(10, 2)
-- Gets: @dcExtra_hour_Without_vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_With_Vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_Without_Vehicle decimal(10, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierTourGuideRate_Update]
	@iID int,
	@iSupplierTourGuideID int,
	@sSeason nvarchar(150),
	@daFromDate datetime,
	@daToDate datetime,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_coach_Guide decimal(10, 2),
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_tips decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_Day_coach_Guide decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2),
	@dcFull_day_tips decimal(10, 2),
	@dcExtra_hour_With_Vehicle decimal(10, 2),
	@dcExtra_hour_Without_vehicle decimal(10, 2),
	@dcLondon_Edinburgh_With_Vehicle decimal(10, 2),
	@dcLondon_Edinburgh_Without_Vehicle decimal(10, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuideRate]
SET 
	[SupplierTourGuideID] = @iSupplierTourGuideID,
	[Season] = @sSeason,
	[FromDate] = @daFromDate,
	[ToDate] = @daToDate,
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle] = @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle,
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle] = @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle,
	[Half_day_coach_Guide] = @dcHalf_day_coach_Guide,
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle] = @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle,
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle] = @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle,
	[Half_day_tips] = @dcHalf_day_tips,
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle] = @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle,
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle] = @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle,
	[Full_Day_coach_Guide] = @dcFull_Day_coach_Guide,
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle] = @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle,
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle] = @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle,
	[Full_day_tips] = @dcFull_day_tips,
	[Extra_hour_With_Vehicle] = @dcExtra_hour_With_Vehicle,
	[Extra_hour_Without_vehicle] = @dcExtra_hour_Without_vehicle,
	[London_Edinburgh_With_Vehicle] = @dcLondon_Edinburgh_With_Vehicle,
	[London_Edinburgh_Without_Vehicle] = @dcLondon_Edinburgh_Without_Vehicle,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTourGuideRate''.
-- Will reset field [SupplierTourGuideID] with value @iSupplierTourGuideIDOld  to value @iSupplierTourGuideID
-- Gets: @iSupplierTourGuideID int
-- Gets: @iSupplierTourGuideIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iSupplierTourGuideIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuideRate]
SET
	[SupplierTourGuideID] = @iSupplierTourGuideID
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_Delete]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''TourGuidePhotos''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[TourGuidePhotos]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_DeleteAllWTourGuideIdLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_DeleteAllWTourGuideIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''TourGuidePhotos''
-- based on a foreign key field.
-- Gets: @iTourGuideId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_DeleteAllWTourGuideIdLogic]
	@iTourGuideId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[TourGuidePhotos]
WHERE
	[TourGuideId] = @iTourGuideId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_Insert]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''TourGuidePhotos''
-- Gets: @iTourGuideId int
-- Gets: @biPhoto varbinary(Max)
-- Gets: @sFilename nvarchar(50)
-- Gets: @sRealFileName nvarchar(50)
-- Gets: @sFileExt nvarchar(10)
-- Gets: @sFileType nvarchar(10)
-- Gets: @sExtraField1 nvarchar(Max)
-- Gets: @sExtraField2 nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_Insert]
	@iTourGuideId int,
	@biPhoto varbinary(Max),
	@sFilename nvarchar(50),
	@sRealFileName nvarchar(50),
	@sFileExt nvarchar(10),
	@sFileType nvarchar(10),
	@sExtraField1 nvarchar(Max),
	@sExtraField2 nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[TourGuidePhotos]
(
	[TourGuideId],
	[Photo],
	[Filename],
	[RealFileName],
	[FileExt],
	[FileType],
	[ExtraField1],
	[ExtraField2],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iTourGuideId,
	@biPhoto,
	@sFilename,
	@sRealFileName,
	@sFileExt,
	@sFileType,
	@sExtraField1,
	@sExtraField2,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_SelectAll]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''TourGuidePhotos''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[TourGuideId],
	[Photo],
	[Filename],
	[RealFileName],
	[FileExt],
	[FileType],
	[ExtraField1],
	[ExtraField2],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[TourGuidePhotos]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_SelectAllWTourGuideIdLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_SelectAllWTourGuideIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''TourGuidePhotos''
-- based on a foreign key field.
-- Gets: @iTourGuideId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_SelectAllWTourGuideIdLogic]
	@iTourGuideId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[TourGuideId],
	[Photo],
	[Filename],
	[RealFileName],
	[FileExt],
	[FileType],
	[ExtraField1],
	[ExtraField2],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[TourGuidePhotos]
WHERE
	[TourGuideId] = @iTourGuideId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_SelectOne]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''TourGuidePhotos''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[TourGuideId],
	[Photo],
	[Filename],
	[RealFileName],
	[FileExt],
	[FileType],
	[ExtraField1],
	[ExtraField2],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[TourGuidePhotos]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_Update]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''TourGuidePhotos''
-- Gets: @iID int
-- Gets: @iTourGuideId int
-- Gets: @biPhoto varbinary(Max)
-- Gets: @sFilename nvarchar(50)
-- Gets: @sRealFileName nvarchar(50)
-- Gets: @sFileExt nvarchar(10)
-- Gets: @sFileType nvarchar(10)
-- Gets: @sExtraField1 nvarchar(Max)
-- Gets: @sExtraField2 nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_Update]
	@iID int,
	@iTourGuideId int,
	@biPhoto varbinary(Max),
	@sFilename nvarchar(50),
	@sRealFileName nvarchar(50),
	@sFileExt nvarchar(10),
	@sFileType nvarchar(10),
	@sExtraField1 nvarchar(Max),
	@sExtraField2 nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[TourGuidePhotos]
SET 
	[TourGuideId] = @iTourGuideId,
	[Photo] = @biPhoto,
	[Filename] = @sFilename,
	[RealFileName] = @sRealFileName,
	[FileExt] = @sFileExt,
	[FileType] = @sFileType,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TourGuidePhotos_UpdateAllWTourGuideIdLogic]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TourGuidePhotos_UpdateAllWTourGuideIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''TourGuidePhotos''.
-- Will reset field [TourGuideId] with value @iTourGuideIdOld  to value @iTourGuideId
-- Gets: @iTourGuideId int
-- Gets: @iTourGuideIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TourGuidePhotos_UpdateAllWTourGuideIdLogic]
	@iTourGuideId int,
	@iTourGuideIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[TourGuidePhotos]
SET
	[TourGuideId] = @iTourGuideId
WHERE
	[TourGuideId] = @iTourGuideIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  Table [dbo].[SupplierTourGuide]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTourGuide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[SupplierTypeId] [int] NULL,
	[TypeOfSupplier] [int] NULL,
	[RecruitedBy] [nvarchar](250) NULL,
	[SurName] [nvarchar](250) NULL,
	[SurNameInChinese] [nvarchar](250) NULL,
	[FirstName] [nvarchar](250) NULL,
	[FirstNameInChinese] [nvarchar](250) NULL,
	[Sex] [nvarchar](10) NULL,
	[DateOfBirth] [datetime] NULL,
	[Origin] [nvarchar](50) NULL,
	[Nationality] [nvarchar](50) NULL,
	[Married] [nvarchar](50) NULL,
	[Children] [int] NULL,
	[VisaStatus] [nvarchar](250) NULL,
	[HomePhone] [nvarchar](15) NULL,
	[MobilePhone] [nvarchar](15) NULL,
	[WeChat] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Address1] [nvarchar](500) NULL,
	[Address2] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostCode] [nvarchar](10) NULL,
	[SpeakingLanguage] [nvarchar](1000) NULL,
	[GuideLicenceInit] [nvarchar](50) NULL,
	[NoOfYearTourGuide] [int] NULL,
	[AreaOfWorking] [nvarchar](250) NULL,
	[TypeOfWork] [nvarchar](250) NULL,
	[VehicleOwned] [nvarchar](250) NULL,
	[KindOfGroupWork] [nvarchar](max) NULL,
	[KindOfGroupNotWork] [nvarchar](150) NULL,
	[SpecialRequirement] [nvarchar](max) NULL,
	[Rating] [int] NULL,
	[BlackList] [nvarchar](50) NULL,
	[Comments] [nvarchar](max) NULL,
	[DrivingLicence] [varbinary](max) NULL,
	[VehiclePhoto] [varbinary](max) NULL,
	[PassportCopy] [varbinary](max) NULL,
	[AddressProof] [varbinary](max) NULL,
	[GuideLicence] [varbinary](max) NULL,
	[CVOfGuide] [varbinary](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](max) NULL,
	[ExtraField2] [nvarchar](max) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTourGuid] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTourGuideRate]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTourGuideRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierTourGuideID] [int] NULL,
	[Season] [nvarchar](150) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle] [decimal](10, 2) NULL,
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle] [decimal](10, 2) NULL,
	[Half_day_coach_Guide] [decimal](10, 2) NULL,
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Half_day_tips] [decimal](10, 2) NULL,
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle] [decimal](10, 2) NULL,
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle] [decimal](10, 2) NULL,
	[Full_Day_coach_Guide] [decimal](10, 2) NULL,
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle] [decimal](10, 2) NULL,
	[Full_day_tips] [decimal](10, 2) NULL,
	[Extra_hour_With_Vehicle] [decimal](10, 2) NULL,
	[Extra_hour_Without_vehicle] [decimal](10, 2) NULL,
	[London_Edinburgh_With_Vehicle] [decimal](10, 2) NULL,
	[London_Edinburgh_Without_Vehicle] [decimal](10, 2) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTourGuideRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TourGuidePhotos]    Script Date: 07-06-2017 20:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TourGuidePhotos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TourGuidePhotos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TourGuideId] [int] NULL,
	[Photo] [varbinary](max) NULL,
	[Filename] [nvarchar](50) NULL,
	[RealFileName] [nvarchar](50) NULL,
	[FileExt] [nvarchar](10) NULL,
	[FileType] [nvarchar](10) NULL,
	[ExtraField1] [nvarchar](max) NULL,
	[ExtraField2] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_TourGuidePhotos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid] FOREIGN KEY([TypeOfSupplier])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide] CHECK CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide] FOREIGN KEY([SupplierTourGuideID])
REFERENCES [dbo].[SupplierTourGuide] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate] CHECK CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TourGuidePhotos_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[TourGuidePhotos]'))
ALTER TABLE [dbo].[TourGuidePhotos]  WITH CHECK ADD  CONSTRAINT [FK_TourGuidePhotos_SupplierTourGuide] FOREIGN KEY([TourGuideId])
REFERENCES [dbo].[SupplierTourGuide] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TourGuidePhotos_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[TourGuidePhotos]'))
ALTER TABLE [dbo].[TourGuidePhotos] CHECK CONSTRAINT [FK_TourGuidePhotos_SupplierTourGuide]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpeakingLanguage'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Speaking Languages' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpeakingLanguage'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'NoOfYearTourGuide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No of years as tour guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'NoOfYearTourGuide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AreaOfWorking'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Area of Working' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AreaOfWorking'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'TypeOfWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Work' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'TypeOfWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What kind of groups that you have been working for?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupNotWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What kind of groups that you do not wish to work for?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupNotWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpecialRequirement'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special requirement from tour guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpecialRequirement'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'Comments'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comments from company staff' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'Comments'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'DrivingLicence'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Driving Licence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'DrivingLicence'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'VehiclePhoto'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle Photo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'VehiclePhoto'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'PassportCopy'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Passport Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'PassportCopy'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AddressProof'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Proof' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AddressProof'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'GuideLicence'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Guide Licence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'GuideLicence'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'CVOfGuide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CV of Guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'CVOfGuide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'HowToBook'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comments from company staff' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'HowToBook'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Season'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Winter season | Summer Season | Summer High season
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Season'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'FromDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'From Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'FromDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'ToDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'To Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'ToDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 4-9 seater driver guide without vehicle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 12 - 17 seater driver guide without vehicle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_coach_Guide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day coach guide
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_coach_Guide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 4-9 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 12 - 17 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_tips'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day tips
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_tips'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 4-9 seater driver guide without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 12- 17 seater driver guide without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_Day_coach_Guide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full Day coach guide
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_Day_coach_Guide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 4-9 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 12- 17 seater driver guide withvehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_tips'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day tips
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_tips'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extra hour with Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_Without_vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extra hour without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_Without_vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'London - Edinburgh with Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'London - Edinburgh Without Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_Without_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'TourGuidePhotos', N'COLUMN',N'Photo'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Driving Licence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TourGuidePhotos', @level2type=N'COLUMN',@level2name=N'Photo'
GO
