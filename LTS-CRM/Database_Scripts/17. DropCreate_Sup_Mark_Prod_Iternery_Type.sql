IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_Without_vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_Without_vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_tips'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_tips'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_Day_coach_Guide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_Day_coach_Guide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_tips'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_tips'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_coach_Guide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_coach_Guide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'ToDate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'ToDate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'FromDate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'FromDate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Season'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Season'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'CVOfGuide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'CVOfGuide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'GuideLicence'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'GuideLicence'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AddressProof'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AddressProof'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'PassportCopy'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'PassportCopy'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'VehiclePhoto'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'VehiclePhoto'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'DrivingLicence'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'DrivingLicence'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'Comments'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'Comments'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpecialRequirement'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpecialRequirement'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupNotWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupNotWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'TypeOfWork'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'TypeOfWork'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AreaOfWorking'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AreaOfWorking'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'NoOfYearTourGuide'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'NoOfYearTourGuide'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpeakingLanguage'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpeakingLanguage'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CreatedBy'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'FoodPictureFile'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'FoodPictureFile'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'Rate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'Rate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'TypeOfFood'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'TypeOfFood'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierMaster', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierMaster', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Breakfast'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Breakfast'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'FamilyRoom'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'FamilyRoom'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Triple'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Triple'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Twin_DoubleForSoleUse'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Twin_DoubleForSoleUse'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Single'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Single'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'HalfTwin'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'HalfTwin'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'Cabin'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'Cabin'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'GroupRate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'GroupRate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'FitRate'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'FitRate'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierEntranceTicketRate', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierEntranceTicketRate', @level2type=N'COLUMN',@level2name=N'CreatedBy'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierDMCRate', N'COLUMN',N'FileData'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierDMCRate', @level2type=N'COLUMN',@level2name=N'FileData'

GO
IF  EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ItineraryTemplate', N'COLUMN',N'CompanyID'))
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItineraryTemplate', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate] DROP CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate] DROP CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate] DROP CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide] DROP CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTipsDetail_SupplierTipsDetail]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]'))
ALTER TABLE [dbo].[SupplierTipsDetail] DROP CONSTRAINT [FK_SupplierTipsDetail_SupplierTipsDetail]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate] DROP CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] DROP CONSTRAINT [FK_SuppliersDetails_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] DROP CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierRestaurantRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]'))
ALTER TABLE [dbo].[SupplierRestaurantRate] DROP CONSTRAINT [FK_SupplierRestaurantRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement] DROP CONSTRAINT [FK_SupplierMealSupplement_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster] DROP CONSTRAINT [FK_SupplierMaster_SupplierType]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate] DROP CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation] DROP CONSTRAINT [FK_SupplierInformation_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate] DROP CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate] DROP CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierEntranceTicketRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]'))
ALTER TABLE [dbo].[SupplierEntranceTicketRate] DROP CONSTRAINT [FK_SupplierEntranceTicketRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierDMCRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]'))
ALTER TABLE [dbo].[SupplierDMCRate] DROP CONSTRAINT [FK_SupplierDMCRate_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts] DROP CONSTRAINT [FK_SupplierContacts_SuppliersDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDocument_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDocument]'))
ALTER TABLE [dbo].[ProductDocument] DROP CONSTRAINT [FK_ProductDocument_Product]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductContact_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductContact]'))
ALTER TABLE [dbo].[ProductContact] DROP CONSTRAINT [FK_ProductContact_Product]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet] DROP CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [FK_MarketingContacts_MarketingChannel]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Campaign_MarketingProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]'))
ALTER TABLE [dbo].[MarketingCampaign] DROP CONSTRAINT [FK_Campaign_MarketingProject]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItineraryPhotos_ItineraryPhotos]') AND parent_object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]'))
ALTER TABLE [dbo].[ItineraryPhotos] DROP CONSTRAINT [FK_ItineraryPhotos_ItineraryPhotos]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [DF_MarketingContacts_UpdatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [DF_MarketingContacts_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] DROP CONSTRAINT [DF_MarketingChannel_UpdatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] DROP CONSTRAINT [DF_MarketingChannel_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LetsGeneralType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LetsGeneralType] DROP CONSTRAINT [DF_LetsGeneralType_IsActive]
END

GO
/****** Object:  Table [dbo].[TaskList]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TaskList]') AND type in (N'U'))
DROP TABLE [dbo].[TaskList]
GO
/****** Object:  Table [dbo].[SupplierVanHireRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierVanHireRate]
GO
/****** Object:  Table [dbo].[SupplierTrainRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTrainRate]
GO
/****** Object:  Table [dbo].[SupplierTourGuideRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTourGuideRate]
GO
/****** Object:  Table [dbo].[SupplierTourGuide]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTourGuide]
GO
/****** Object:  Table [dbo].[SupplierTipsDetail]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTipsDetail]
GO
/****** Object:  Table [dbo].[SupplierTheaterRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierTheaterRate]
GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
DROP TABLE [dbo].[SuppliersDetails]
GO
/****** Object:  Table [dbo].[SupplierRestaurantRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierRestaurantRate]
GO
/****** Object:  Table [dbo].[SupplierMealSupplement]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierMealSupplement]
GO
/****** Object:  Table [dbo].[SupplierMaster]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMaster]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierMaster]
GO
/****** Object:  Table [dbo].[SupplierInsuranceRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierInsuranceRate]
GO
/****** Object:  Table [dbo].[SupplierInformation]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInformation]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierInformation]
GO
/****** Object:  Table [dbo].[SupplierHotelRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierHotelRate]
GO
/****** Object:  Table [dbo].[SupplierFerryRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierFerryRate]
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierEntranceTicketRate]
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketFit]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketFit]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierEntranceTicketFit]
GO
/****** Object:  Table [dbo].[SupplierDMCRate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierDMCRate]
GO
/****** Object:  Table [dbo].[SupplierContacts]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierContacts]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierContacts]
GO
/****** Object:  Table [dbo].[ProductDocument]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductDocument]') AND type in (N'U'))
DROP TABLE [dbo].[ProductDocument]
GO
/****** Object:  Table [dbo].[ProductContact]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductContact]') AND type in (N'U'))
DROP TABLE [dbo].[ProductContact]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
DROP TABLE [dbo].[Product]
GO
/****** Object:  Table [dbo].[PriceTemplate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PriceTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[PriceTemplate]
GO
/****** Object:  Table [dbo].[OpinionSheetCategory]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheetCategory]') AND type in (N'U'))
DROP TABLE [dbo].[OpinionSheetCategory]
GO
/****** Object:  Table [dbo].[OpinionSheet]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheet]') AND type in (N'U'))
DROP TABLE [dbo].[OpinionSheet]
GO
/****** Object:  Table [dbo].[MarketingProject]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingProject]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingProject]
GO
/****** Object:  Table [dbo].[MarketingContacts]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingContacts]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingContacts]
GO
/****** Object:  Table [dbo].[MarketingChannel]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingChannel]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingChannel]
GO
/****** Object:  Table [dbo].[MarketingCampaign]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingCampaign]
GO
/****** Object:  Table [dbo].[LetsGeneralType]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LetsGeneralType]') AND type in (N'U'))
DROP TABLE [dbo].[LetsGeneralType]
GO
/****** Object:  Table [dbo].[LeadManagement]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LeadManagement]') AND type in (N'U'))
DROP TABLE [dbo].[LeadManagement]
GO
/****** Object:  Table [dbo].[ItineraryTemplate]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[ItineraryTemplate]
GO
/****** Object:  Table [dbo].[ItineraryPhotos]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]') AND type in (N'U'))
DROP TABLE [dbo].[ItineraryPhotos]
GO
/****** Object:  Table [dbo].[GeneralCategoryItems]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralCategoryItems]') AND type in (N'U'))
DROP TABLE [dbo].[GeneralCategoryItems]
GO
/****** Object:  Table [dbo].[ContactManagement]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactManagement]') AND type in (N'U'))
DROP TABLE [dbo].[ContactManagement]
GO
/****** Object:  Table [dbo].[AppointmentList]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppointmentList]') AND type in (N'U'))
DROP TABLE [dbo].[AppointmentList]
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TaskList_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TaskList_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TaskList_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TaskList_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_TaskList_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierVanHireRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierType_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTrainRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuideRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTourGuide_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTipsDetail_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierTheaterRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SuppliersDetails_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierRestaurantRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMealSupplement_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierMaster_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInsuranceRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierInformation_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierHotelRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierFerryRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierDMCRate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_SupplierContacts_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDocument_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductDeliveryDetail_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAllWProductlIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ProductContact_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Product_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PriceTemplate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PriceTemplate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PriceTemplate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PriceTemplate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PriceTemplate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingProject_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingProject_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingProject_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingProject_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingProject_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingContacts_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingChannel_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingChannel_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingChannel_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingChannel_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingChannel_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MarketingCampaign_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LeadManagement_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LeadManagement_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LeadManagement_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LeadManagement_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_LeadManagement_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryTemplate_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryTemplate_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryTemplate_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryTemplate_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryTemplate_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ItineraryPhotos_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GeneralCategoryItems_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GeneralCategoryItems_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GeneralCategoryItems_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GeneralCategoryItems_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_GeneralCategoryItems_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ContactManagement_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ContactManagement_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ContactManagement_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ContactManagement_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ContactManagement_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_Update]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AppointmentList_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AppointmentList_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AppointmentList_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_Insert]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AppointmentList_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_Delete]    Script Date: 19-05-2017 20:58:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AppointmentList_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''AppointmentList''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[AppointmentList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''AppointmentList''
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daAppointmentDate datetime
-- Gets: @daAppointmentTime datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_Insert]
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daAppointmentDate datetime,
	@daAppointmentTime datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[AppointmentList]
(
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sSubject,
	@sStatus,
	@sDescription,
	@daAppointmentDate,
	@daAppointmentTime,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''AppointmentList''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[AppointmentList]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''AppointmentList''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_AppointmentList_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[Status],
	[Description],
	[AppointmentDate],
	[AppointmentTime],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[AppointmentList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_AppointmentList_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AppointmentList_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''AppointmentList''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daAppointmentDate datetime
-- Gets: @daAppointmentTime datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_AppointmentList_Update]
	@iID int,
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daAppointmentDate datetime,
	@daAppointmentTime datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[AppointmentList]
SET 
	[CompanyID] = @iCompanyID,
	[Subject] = @sSubject,
	[Status] = @sStatus,
	[Description] = @sDescription,
	[AppointmentDate] = @daAppointmentDate,
	[AppointmentTime] = @daAppointmentTime,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ContactManagement''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ContactManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ContactManagement''
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ContactManagement]
(
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@sCompany,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sStatus,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ContactManagement''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ContactManagement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ContactManagement''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ContactManagement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[Company],
	[Title],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[Status],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ContactManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ContactManagement_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ContactManagement_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ContactManagement''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_ContactManagement_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sStatus nvarchar(50),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ContactManagement]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[Status] = @sStatus,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''GeneralCategoryItems''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[GeneralCategoryItems]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''GeneralCategoryItems''
-- Gets: @sCategory nvarchar(150)
-- Gets: @sTitle nvarchar(150)
-- Gets: @sDescription nvarchar(500)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_Insert]
	@sCategory nvarchar(150),
	@sTitle nvarchar(150),
	@sDescription nvarchar(500),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[GeneralCategoryItems]
(
	[Category],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@sCategory,
	@sTitle,
	@sDescription,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''GeneralCategoryItems''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[Category],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[GeneralCategoryItems]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''GeneralCategoryItems''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_GeneralCategoryItems_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[Category],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[GeneralCategoryItems]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralCategoryItems_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_GeneralCategoryItems_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''GeneralCategoryItems''
-- Gets: @iID int
-- Gets: @sCategory nvarchar(150)
-- Gets: @sTitle nvarchar(150)
-- Gets: @sDescription nvarchar(500)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_GeneralCategoryItems_Update]
	@iID int,
	@sCategory nvarchar(150),
	@sTitle nvarchar(150),
	@sDescription nvarchar(500),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[GeneralCategoryItems]
SET 
	[Category] = @sCategory,
	[Title] = @sTitle,
	[Description] = @sDescription,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ItineraryPhotos''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ItineraryPhotos]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ItineraryPhotos''
-- based on a foreign key field.
-- Gets: @iItineraryID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_DeleteAllWItineraryIDLogic]
	@iItineraryID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ItineraryPhotos]
WHERE
	[ItineraryID] = @iItineraryID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ItineraryPhotos''
-- Gets: @iItineraryID int
-- Gets: @biPhotos varbinary(Max)
-- Gets: @sDocType nvarchar(20)
-- Gets: @sExtension nvarchar(10)
-- Gets: @sFileName nvarchar(50)
-- Gets: @sRealFileName nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_Insert]
	@iItineraryID int,
	@biPhotos varbinary(Max),
	@sDocType nvarchar(20),
	@sExtension nvarchar(10),
	@sFileName nvarchar(50),
	@sRealFileName nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ItineraryPhotos]
(
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iItineraryID,
	@biPhotos,
	@sDocType,
	@sExtension,
	@sFileName,
	@sRealFileName,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ItineraryPhotos''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryPhotos]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ItineraryPhotos''
-- based on a foreign key field.
-- Gets: @iItineraryID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_SelectAllWItineraryIDLogic]
	@iItineraryID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryPhotos]
WHERE
	[ItineraryID] = @iItineraryID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ItineraryPhotos''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ItineraryID],
	[Photos],
	[DocType],
	[Extension],
	[FileName],
	[RealFileName],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryPhotos]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ItineraryPhotos''
-- Gets: @iID int
-- Gets: @iItineraryID int
-- Gets: @biPhotos varbinary(Max)
-- Gets: @sDocType nvarchar(20)
-- Gets: @sExtension nvarchar(10)
-- Gets: @sFileName nvarchar(50)
-- Gets: @sRealFileName nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_ItineraryPhotos_Update]
	@iID int,
	@iItineraryID int,
	@biPhotos varbinary(Max),
	@sDocType nvarchar(20),
	@sExtension nvarchar(10),
	@sFileName nvarchar(50),
	@sRealFileName nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ItineraryPhotos]
SET 
	[ItineraryID] = @iItineraryID,
	[Photos] = @biPhotos,
	[DocType] = @sDocType,
	[Extension] = @sExtension,
	[FileName] = @sFileName,
	[RealFileName] = @sRealFileName,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ItineraryPhotos''.
-- Will reset field [ItineraryID] with value @iItineraryIDOld  to value @iItineraryID
-- Gets: @iItineraryID int
-- Gets: @iItineraryIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryPhotos_UpdateAllWItineraryIDLogic]
	@iItineraryID int,
	@iItineraryIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ItineraryPhotos]
SET
	[ItineraryID] = @iItineraryID
WHERE
	[ItineraryID] = @iItineraryIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ItineraryTemplate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ItineraryTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ItineraryTemplate''
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sTourType nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sSubjectTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @iItineraryPhotoId int
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_Insert]
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sCountry nvarchar(50),
	@sRegion nvarchar(50),
	@sTourType nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sSubjectTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@iItineraryPhotoId int,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ItineraryTemplate]
(
	[CompanyID],
	[Staff],
	[Country],
	[Region],
	[TourType],
	[NoOfDays],
	[NoOfNights],
	[SubjectTitle],
	[ItineraryTemplate],
	[ItineraryPhotoId],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sStaff,
	@sCountry,
	@sRegion,
	@sTourType,
	@iNoOfDays,
	@iNoOfNights,
	@sSubjectTitle,
	@sItineraryTemplate,
	@iItineraryPhotoId,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ItineraryTemplate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Country],
	[Region],
	[TourType],
	[NoOfDays],
	[NoOfNights],
	[SubjectTitle],
	[ItineraryTemplate],
	[ItineraryPhotoId],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryTemplate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ItineraryTemplate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ItineraryTemplate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Country],
	[Region],
	[TourType],
	[NoOfDays],
	[NoOfNights],
	[SubjectTitle],
	[ItineraryTemplate],
	[ItineraryPhotoId],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ItineraryTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ItineraryTemplate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ItineraryTemplate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ItineraryTemplate''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sTourType nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sSubjectTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @iItineraryPhotoId int
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_ItineraryTemplate_Update]
	@iID int,
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sCountry nvarchar(50),
	@sRegion nvarchar(50),
	@sTourType nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sSubjectTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@iItineraryPhotoId int,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ItineraryTemplate]
SET 
	[CompanyID] = @iCompanyID,
	[Staff] = @sStaff,
	[Country] = @sCountry,
	[Region] = @sRegion,
	[TourType] = @sTourType,
	[NoOfDays] = @iNoOfDays,
	[NoOfNights] = @iNoOfNights,
	[SubjectTitle] = @sSubjectTitle,
	[ItineraryTemplate] = @sItineraryTemplate,
	[ItineraryPhotoId] = @iItineraryPhotoId,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''LeadManagement''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[LeadManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''LeadManagement''
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_Insert]
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[LeadManagement]
(
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContactType,
	@iCampaignID,
	@sTourType,
	@sCompany,
	@sTitle,
	@sGender,
	@sSurName,
	@sFirstName,
	@sEmail,
	@sAlternateEmail,
	@sHomePhone,
	@sMobileNo,
	@sFaxNumber,
	@sBusinessPhone,
	@sAddress,
	@sPreferredContactTime,
	@sPreferredContactMethod,
	@daPlannedDepartureDate,
	@iHowManyDays,
	@iHowManyPeople,
	@iHowManyChild,
	@sPlaceOfDeparture,
	@sPlaceOfReturn,
	@dcTotalBudget,
	@sCurrency,
	@sTourGuide,
	@bFlightsNeeded,
	@sVehicle,
	@sHotelType,
	@sMeals,
	@sAdmissionTickets,
	@sInsurance,
	@sLeadSource,
	@sLeadOwner,
	@sLeadStatus,
	@sCity,
	@sState,
	@sZipCode,
	@sCountry,
	@sWebPage,
	@sNotes,
	@sReferredBy,
	@sHomeTown,
	@daBirthDate,
	@sContactsInterests,
	@biAttachment,
	@sBDM,
	@sRegion,
	@sResponse,
	@sClientType,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''LeadManagement''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[LeadManagement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''LeadManagement''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_LeadManagement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContactType],
	[CampaignID],
	[TourType],
	[Company],
	[Title],
	[Gender],
	[SurName],
	[FirstName],
	[Email],
	[AlternateEmail],
	[HomePhone],
	[MobileNo],
	[FaxNumber],
	[BusinessPhone],
	[Address],
	[PreferredContactTime],
	[PreferredContactMethod],
	[PlannedDepartureDate],
	[HowManyDays],
	[HowManyPeople],
	[HowManyChild],
	[PlaceOfDeparture],
	[PlaceOfReturn],
	[TotalBudget],
	[Currency],
	[TourGuide],
	[FlightsNeeded],
	[Vehicle],
	[HotelType],
	[Meals],
	[AdmissionTickets],
	[Insurance],
	[LeadSource],
	[LeadOwner],
	[LeadStatus],
	[City],
	[State],
	[ZipCode],
	[Country],
	[WebPage],
	[Notes],
	[ReferredBy],
	[HomeTown],
	[BirthDate],
	[ContactsInterests],
	[Attachment],
	[BDM],
	[Region],
	[Response],
	[ClientType],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[LeadManagement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_LeadManagement_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_LeadManagement_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''LeadManagement''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContactType nvarchar(50)
-- Gets: @iCampaignID int
-- Gets: @sTourType nvarchar(50)
-- Gets: @sCompany nvarchar(50)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sGender nvarchar(10)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sAlternateEmail nvarchar(150)
-- Gets: @sHomePhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNumber nvarchar(50)
-- Gets: @sBusinessPhone nvarchar(50)
-- Gets: @sAddress nvarchar(500)
-- Gets: @sPreferredContactTime nvarchar(50)
-- Gets: @sPreferredContactMethod nvarchar(50)
-- Gets: @daPlannedDepartureDate datetime
-- Gets: @iHowManyDays int
-- Gets: @iHowManyPeople int
-- Gets: @iHowManyChild int
-- Gets: @sPlaceOfDeparture nvarchar(50)
-- Gets: @sPlaceOfReturn nvarchar(50)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @sCurrency nvarchar(15)
-- Gets: @sTourGuide nvarchar(50)
-- Gets: @bFlightsNeeded bit
-- Gets: @sVehicle nvarchar(50)
-- Gets: @sHotelType nvarchar(50)
-- Gets: @sMeals nvarchar(50)
-- Gets: @sAdmissionTickets nvarchar(50)
-- Gets: @sInsurance nvarchar(50)
-- Gets: @sLeadSource nvarchar(50)
-- Gets: @sLeadOwner nvarchar(50)
-- Gets: @sLeadStatus nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sZipCode nvarchar(10)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sWebPage nvarchar(250)
-- Gets: @sNotes nvarchar(Max)
-- Gets: @sReferredBy nvarchar(50)
-- Gets: @sHomeTown nvarchar(50)
-- Gets: @daBirthDate datetime
-- Gets: @sContactsInterests nvarchar(250)
-- Gets: @biAttachment varbinary(Max)
-- Gets: @sBDM nvarchar(50)
-- Gets: @sRegion nvarchar(50)
-- Gets: @sResponse nvarchar(500)
-- Gets: @sClientType nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_LeadManagement_Update]
	@iID int,
	@iCompanyID int,
	@sContactType nvarchar(50),
	@iCampaignID int,
	@sTourType nvarchar(50),
	@sCompany nvarchar(50),
	@sTitle nvarchar(50),
	@sGender nvarchar(10),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sEmail nvarchar(150),
	@sAlternateEmail nvarchar(150),
	@sHomePhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNumber nvarchar(50),
	@sBusinessPhone nvarchar(50),
	@sAddress nvarchar(500),
	@sPreferredContactTime nvarchar(50),
	@sPreferredContactMethod nvarchar(50),
	@daPlannedDepartureDate datetime,
	@iHowManyDays int,
	@iHowManyPeople int,
	@iHowManyChild int,
	@sPlaceOfDeparture nvarchar(50),
	@sPlaceOfReturn nvarchar(50),
	@dcTotalBudget decimal(18, 2),
	@sCurrency nvarchar(15),
	@sTourGuide nvarchar(50),
	@bFlightsNeeded bit,
	@sVehicle nvarchar(50),
	@sHotelType nvarchar(50),
	@sMeals nvarchar(50),
	@sAdmissionTickets nvarchar(50),
	@sInsurance nvarchar(50),
	@sLeadSource nvarchar(50),
	@sLeadOwner nvarchar(50),
	@sLeadStatus nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sZipCode nvarchar(10),
	@sCountry nvarchar(50),
	@sWebPage nvarchar(250),
	@sNotes nvarchar(Max),
	@sReferredBy nvarchar(50),
	@sHomeTown nvarchar(50),
	@daBirthDate datetime,
	@sContactsInterests nvarchar(250),
	@biAttachment varbinary(Max),
	@sBDM nvarchar(50),
	@sRegion nvarchar(50),
	@sResponse nvarchar(500),
	@sClientType nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[LeadManagement]
SET 
	[CompanyID] = @iCompanyID,
	[ContactType] = @sContactType,
	[CampaignID] = @iCampaignID,
	[TourType] = @sTourType,
	[Company] = @sCompany,
	[Title] = @sTitle,
	[Gender] = @sGender,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[HomePhone] = @sHomePhone,
	[MobileNo] = @sMobileNo,
	[FaxNumber] = @sFaxNumber,
	[BusinessPhone] = @sBusinessPhone,
	[Address] = @sAddress,
	[PreferredContactTime] = @sPreferredContactTime,
	[PreferredContactMethod] = @sPreferredContactMethod,
	[PlannedDepartureDate] = @daPlannedDepartureDate,
	[HowManyDays] = @iHowManyDays,
	[HowManyPeople] = @iHowManyPeople,
	[HowManyChild] = @iHowManyChild,
	[PlaceOfDeparture] = @sPlaceOfDeparture,
	[PlaceOfReturn] = @sPlaceOfReturn,
	[TotalBudget] = @dcTotalBudget,
	[Currency] = @sCurrency,
	[TourGuide] = @sTourGuide,
	[FlightsNeeded] = @bFlightsNeeded,
	[Vehicle] = @sVehicle,
	[HotelType] = @sHotelType,
	[Meals] = @sMeals,
	[AdmissionTickets] = @sAdmissionTickets,
	[Insurance] = @sInsurance,
	[LeadSource] = @sLeadSource,
	[LeadOwner] = @sLeadOwner,
	[LeadStatus] = @sLeadStatus,
	[City] = @sCity,
	[State] = @sState,
	[ZipCode] = @sZipCode,
	[Country] = @sCountry,
	[WebPage] = @sWebPage,
	[Notes] = @sNotes,
	[ReferredBy] = @sReferredBy,
	[HomeTown] = @sHomeTown,
	[BirthDate] = @daBirthDate,
	[ContactsInterests] = @sContactsInterests,
	[Attachment] = @biAttachment,
	[BDM] = @sBDM,
	[Region] = @sRegion,
	[Response] = @sResponse,
	[ClientType] = @sClientType,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''MarketingCampaign''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingCampaign]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''MarketingCampaign''
-- based on a foreign key field.
-- Gets: @iProjectID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_DeleteAllWProjectIDLogic]
	@iProjectID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[MarketingCampaign]
WHERE
	[ProjectID] = @iProjectID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''MarketingCampaign''
-- Gets: @iCompanyID int
-- Gets: @iProjectID int
-- Gets: @sCampaignName nvarchar(50)
-- Gets: @daStartDate datetime
-- Gets: @daEndDate datetime
-- Gets: @sCampaignDescription nvarchar(Max)
-- Gets: @dcEstimateBudget decimal(18, 2)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sFax nvarchar(50)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_Insert]
	@iCompanyID int,
	@iProjectID int,
	@sCampaignName nvarchar(50),
	@daStartDate datetime,
	@daEndDate datetime,
	@sCampaignDescription nvarchar(Max),
	@dcEstimateBudget decimal(18, 2),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sFax nvarchar(50),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingCampaign]
(
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iProjectID,
	@sCampaignName,
	@daStartDate,
	@daEndDate,
	@sCampaignDescription,
	@dcEstimateBudget,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sTelephone,
	@sEmail,
	@sAlternateEmail,
	@sFax,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''MarketingCampaign''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingCampaign]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''MarketingCampaign''
-- based on a foreign key field.
-- Gets: @iProjectID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_SelectAllWProjectIDLogic]
	@iProjectID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingCampaign]
WHERE
	[ProjectID] = @iProjectID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''MarketingCampaign''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ProjectID],
	[CampaignName],
	[StartDate],
	[EndDate],
	[CampaignDescription],
	[EstimateBudget],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[Email],
	[AlternateEmail],
	[Fax],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingCampaign]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''MarketingCampaign''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iProjectID int
-- Gets: @sCampaignName nvarchar(50)
-- Gets: @daStartDate datetime
-- Gets: @daEndDate datetime
-- Gets: @sCampaignDescription nvarchar(Max)
-- Gets: @dcEstimateBudget decimal(18, 2)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sFax nvarchar(50)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_MarketingCampaign_Update]
	@iID int,
	@iCompanyID int,
	@iProjectID int,
	@sCampaignName nvarchar(50),
	@daStartDate datetime,
	@daEndDate datetime,
	@sCampaignDescription nvarchar(Max),
	@dcEstimateBudget decimal(18, 2),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sFax nvarchar(50),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingCampaign]
SET 
	[CompanyID] = @iCompanyID,
	[ProjectID] = @iProjectID,
	[CampaignName] = @sCampaignName,
	[StartDate] = @daStartDate,
	[EndDate] = @daEndDate,
	[CampaignDescription] = @sCampaignDescription,
	[EstimateBudget] = @dcEstimateBudget,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[Telephone] = @sTelephone,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[Fax] = @sFax,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''MarketingCampaign''.
-- Will reset field [ProjectID] with value @iProjectIDOld  to value @iProjectID
-- Gets: @iProjectID int
-- Gets: @iProjectIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingCampaign_UpdateAllWProjectIDLogic]
	@iProjectID int,
	@iProjectIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingCampaign]
SET
	[ProjectID] = @iProjectID
WHERE
	[ProjectID] = @iProjectIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''MarketingChannel''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingChannel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''MarketingChannel''
-- Gets: @iCompanyID int
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sCompanyName nvarchar(100)
-- Gets: @sCompanyWebsite nvarchar(250)
-- Gets: @sUsername nvarchar(50)
-- Gets: @sPassword nvarchar(250)
-- Gets: @sCompanyTelephone nvarchar(50)
-- Gets: @sCompanyEmail nvarchar(50)
-- Gets: @sCompanyAddress1 nvarchar(100)
-- Gets: @sCompanyAddress2 nvarchar(100)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(50)
-- Gets: @sAdvertisingCost nvarchar(50)
-- Gets: @dcAmountOfFee decimal(18, 2)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sRating nvarchar(50)
-- Gets: @sMainBusiness nvarchar(Max)
-- Gets: @sHowToUseMarketingChannel nvarchar(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sFirstDayOfCollabration nvarchar(250)
-- Gets: @daContractExpireDate datetime
-- Gets: @sStatus nvarchar(50)
-- Gets: @biSignedAgreedContractUpload varbinary(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Insert]
	@iCompanyID int,
	@sContractedBy nvarchar(50),
	@sCompanyName nvarchar(100),
	@sCompanyWebsite nvarchar(250),
	@sUsername nvarchar(50),
	@sPassword nvarchar(250),
	@sCompanyTelephone nvarchar(50),
	@sCompanyEmail nvarchar(50),
	@sCompanyAddress1 nvarchar(100),
	@sCompanyAddress2 nvarchar(100),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(50),
	@sAdvertisingCost nvarchar(50),
	@dcAmountOfFee decimal(18, 2),
	@sPaymentTerms nvarchar(Max),
	@sBlackList nvarchar(50),
	@sRating nvarchar(50),
	@sMainBusiness nvarchar(Max),
	@sHowToUseMarketingChannel nvarchar(Max),
	@sComments nvarchar(Max),
	@sFirstDayOfCollabration nvarchar(250),
	@daContractExpireDate datetime,
	@sStatus nvarchar(50),
	@biSignedAgreedContractUpload varbinary(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingChannel]
(
	[CompanyID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sContractedBy,
	@sCompanyName,
	@sCompanyWebsite,
	@sUsername,
	@sPassword,
	@sCompanyTelephone,
	@sCompanyEmail,
	@sCompanyAddress1,
	@sCompanyAddress2,
	@sCity,
	@sCountry,
	@sPostCode,
	@sAdvertisingCost,
	@dcAmountOfFee,
	@sPaymentTerms,
	@sBlackList,
	@sRating,
	@sMainBusiness,
	@sHowToUseMarketingChannel,
	@sComments,
	@sFirstDayOfCollabration,
	@daContractExpireDate,
	@sStatus,
	@biSignedAgreedContractUpload,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''MarketingChannel''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingChannel]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''MarketingChannel''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingChannel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingChannel_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingChannel_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''MarketingChannel''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sCompanyName nvarchar(100)
-- Gets: @sCompanyWebsite nvarchar(250)
-- Gets: @sUsername nvarchar(50)
-- Gets: @sPassword nvarchar(250)
-- Gets: @sCompanyTelephone nvarchar(50)
-- Gets: @sCompanyEmail nvarchar(50)
-- Gets: @sCompanyAddress1 nvarchar(100)
-- Gets: @sCompanyAddress2 nvarchar(100)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(50)
-- Gets: @sAdvertisingCost nvarchar(50)
-- Gets: @dcAmountOfFee decimal(18, 2)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sRating nvarchar(50)
-- Gets: @sMainBusiness nvarchar(Max)
-- Gets: @sHowToUseMarketingChannel nvarchar(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sFirstDayOfCollabration nvarchar(250)
-- Gets: @daContractExpireDate datetime
-- Gets: @sStatus nvarchar(50)
-- Gets: @biSignedAgreedContractUpload varbinary(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_MarketingChannel_Update]
	@iID int,
	@iCompanyID int,
	@sContractedBy nvarchar(50),
	@sCompanyName nvarchar(100),
	@sCompanyWebsite nvarchar(250),
	@sUsername nvarchar(50),
	@sPassword nvarchar(250),
	@sCompanyTelephone nvarchar(50),
	@sCompanyEmail nvarchar(50),
	@sCompanyAddress1 nvarchar(100),
	@sCompanyAddress2 nvarchar(100),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(50),
	@sAdvertisingCost nvarchar(50),
	@dcAmountOfFee decimal(18, 2),
	@sPaymentTerms nvarchar(Max),
	@sBlackList nvarchar(50),
	@sRating nvarchar(50),
	@sMainBusiness nvarchar(Max),
	@sHowToUseMarketingChannel nvarchar(Max),
	@sComments nvarchar(Max),
	@sFirstDayOfCollabration nvarchar(250),
	@daContractExpireDate datetime,
	@sStatus nvarchar(50),
	@biSignedAgreedContractUpload varbinary(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingChannel]
SET 
	[CompanyID] = @iCompanyID,
	[ContractedBy] = @sContractedBy,
	[CompanyName] = @sCompanyName,
	[CompanyWebsite] = @sCompanyWebsite,
	[Username] = @sUsername,
	[Password] = @sPassword,
	[CompanyTelephone] = @sCompanyTelephone,
	[CompanyEmail] = @sCompanyEmail,
	[CompanyAddress1] = @sCompanyAddress1,
	[CompanyAddress2] = @sCompanyAddress2,
	[City] = @sCity,
	[Country] = @sCountry,
	[PostCode] = @sPostCode,
	[AdvertisingCost] = @sAdvertisingCost,
	[AmountOfFee] = @dcAmountOfFee,
	[PaymentTerms] = @sPaymentTerms,
	[BlackList] = @sBlackList,
	[Rating] = @sRating,
	[MainBusiness] = @sMainBusiness,
	[HowToUseMarketingChannel] = @sHowToUseMarketingChannel,
	[Comments] = @sComments,
	[FirstDayOfCollabration] = @sFirstDayOfCollabration,
	[ContractExpireDate] = @daContractExpireDate,
	[Status] = @sStatus,
	[SignedAgreedContractUpload] = @biSignedAgreedContractUpload,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''MarketingContacts''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''MarketingContacts''
-- based on a foreign key field.
-- Gets: @iMarketingChannelID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]
	@iMarketingChannelID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[MarketingContacts]
WHERE
	[MarketingChannelID] = @iMarketingChannelID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''MarketingContacts''
-- Gets: @iMarketingChannelID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_Insert]
	@iMarketingChannelID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingContacts]
(
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iMarketingChannelID,
	@sTitle,
	@sSurName,
	@sFirstName,
	@bMainContact,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''MarketingContacts''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingContacts]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''MarketingContacts''
-- based on a foreign key field.
-- Gets: @iMarketingChannelID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]
	@iMarketingChannelID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingContacts]
WHERE
	[MarketingChannelID] = @iMarketingChannelID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''MarketingContacts''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''MarketingContacts''
-- Gets: @iID int
-- Gets: @iMarketingChannelID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_MarketingContacts_Update]
	@iID int,
	@iMarketingChannelID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingContacts]
SET 
	[MarketingChannelID] = @iMarketingChannelID,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[MainContact] = @bMainContact,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''MarketingContacts''.
-- Will reset field [MarketingChannelID] with value @iMarketingChannelIDOld  to value @iMarketingChannelID
-- Gets: @iMarketingChannelID int
-- Gets: @iMarketingChannelIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]
	@iMarketingChannelID int,
	@iMarketingChannelIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingContacts]
SET
	[MarketingChannelID] = @iMarketingChannelID
WHERE
	[MarketingChannelID] = @iMarketingChannelIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''MarketingProject''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingProject]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''MarketingProject''
-- Gets: @iCompanyID int
-- Gets: @sReferenceNo nvarchar(100)
-- Gets: @sProjectName nvarchar(250)
-- Gets: @sProjectDescribution nvarchar(Max)
-- Gets: @sInitiateBy nvarchar(50)
-- Gets: @sStaffInvolved nvarchar(50)
-- Gets: @daProjectStartingDate datetime
-- Gets: @daProjectPlannedEndingDate datetime
-- Gets: @sWorkingProcessComment nvarchar(Max)
-- Gets: @daProjectActualEndDate datetime
-- Gets: @dcProjectIncome decimal(18, 2)
-- Gets: @dcProjectExpense decimal(18, 2)
-- Gets: @dcProjectGrossProfit decimal(18, 2)
-- Gets: @sRating nvarchar(50)
-- Gets: @sProjectReport nvarchar(Max)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @dcTotalEstimateBudget decimal(18, 2)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Insert]
	@iCompanyID int,
	@sReferenceNo nvarchar(100),
	@sProjectName nvarchar(250),
	@sProjectDescribution nvarchar(Max),
	@sInitiateBy nvarchar(50),
	@sStaffInvolved nvarchar(50),
	@daProjectStartingDate datetime,
	@daProjectPlannedEndingDate datetime,
	@sWorkingProcessComment nvarchar(Max),
	@daProjectActualEndDate datetime,
	@dcProjectIncome decimal(18, 2),
	@dcProjectExpense decimal(18, 2),
	@dcProjectGrossProfit decimal(18, 2),
	@sRating nvarchar(50),
	@sProjectReport nvarchar(Max),
	@dcTotalBudget decimal(18, 2),
	@dcTotalEstimateBudget decimal(18, 2),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingProject]
(
	[CompanyID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[TotalBudget],
	[TotalEstimateBudget],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sReferenceNo,
	@sProjectName,
	@sProjectDescribution,
	@sInitiateBy,
	@sStaffInvolved,
	@daProjectStartingDate,
	@daProjectPlannedEndingDate,
	@sWorkingProcessComment,
	@daProjectActualEndDate,
	@dcProjectIncome,
	@dcProjectExpense,
	@dcProjectGrossProfit,
	@sRating,
	@sProjectReport,
	@dcTotalBudget,
	@dcTotalEstimateBudget,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''MarketingProject''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[TotalBudget],
	[TotalEstimateBudget],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingProject]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''MarketingProject''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[TotalBudget],
	[TotalEstimateBudget],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingProject]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_MarketingProject_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MarketingProject_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''MarketingProject''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sReferenceNo nvarchar(100)
-- Gets: @sProjectName nvarchar(250)
-- Gets: @sProjectDescribution nvarchar(Max)
-- Gets: @sInitiateBy nvarchar(50)
-- Gets: @sStaffInvolved nvarchar(50)
-- Gets: @daProjectStartingDate datetime
-- Gets: @daProjectPlannedEndingDate datetime
-- Gets: @sWorkingProcessComment nvarchar(Max)
-- Gets: @daProjectActualEndDate datetime
-- Gets: @dcProjectIncome decimal(18, 2)
-- Gets: @dcProjectExpense decimal(18, 2)
-- Gets: @dcProjectGrossProfit decimal(18, 2)
-- Gets: @sRating nvarchar(50)
-- Gets: @sProjectReport nvarchar(Max)
-- Gets: @dcTotalBudget decimal(18, 2)
-- Gets: @dcTotalEstimateBudget decimal(18, 2)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_MarketingProject_Update]
	@iID int,
	@iCompanyID int,
	@sReferenceNo nvarchar(100),
	@sProjectName nvarchar(250),
	@sProjectDescribution nvarchar(Max),
	@sInitiateBy nvarchar(50),
	@sStaffInvolved nvarchar(50),
	@daProjectStartingDate datetime,
	@daProjectPlannedEndingDate datetime,
	@sWorkingProcessComment nvarchar(Max),
	@daProjectActualEndDate datetime,
	@dcProjectIncome decimal(18, 2),
	@dcProjectExpense decimal(18, 2),
	@dcProjectGrossProfit decimal(18, 2),
	@sRating nvarchar(50),
	@sProjectReport nvarchar(Max),
	@dcTotalBudget decimal(18, 2),
	@dcTotalEstimateBudget decimal(18, 2),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingProject]
SET 
	[CompanyID] = @iCompanyID,
	[ReferenceNo] = @sReferenceNo,
	[ProjectName] = @sProjectName,
	[ProjectDescribution] = @sProjectDescribution,
	[InitiateBy] = @sInitiateBy,
	[StaffInvolved] = @sStaffInvolved,
	[ProjectStartingDate] = @daProjectStartingDate,
	[ProjectPlannedEndingDate] = @daProjectPlannedEndingDate,
	[WorkingProcessComment] = @sWorkingProcessComment,
	[ProjectActualEndDate] = @daProjectActualEndDate,
	[ProjectIncome] = @dcProjectIncome,
	[ProjectExpense] = @dcProjectExpense,
	[ProjectGrossProfit] = @dcProjectGrossProfit,
	[Rating] = @sRating,
	[ProjectReport] = @sProjectReport,
	[TotalBudget] = @dcTotalBudget,
	[TotalEstimateBudget] = @dcTotalEstimateBudget,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''OpinionSheet''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OpinionSheet]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''OpinionSheet''
-- based on a foreign key field.
-- Gets: @iOpinionSheetCategoryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[OpinionSheet]
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''OpinionSheet''
-- Gets: @iCompanyID int
-- Gets: @daDate datetime
-- Gets: @sStaff nvarchar(250)
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Insert]
	@iCompanyID int,
	@daDate datetime,
	@sStaff nvarchar(250),
	@iOpinionSheetCategoryId int,
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OpinionSheet]
(
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@daDate,
	@sStaff,
	@iOpinionSheetCategoryId,
	@biFileData,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''OpinionSheet''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectAll]
 @iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
 [dbo].[OpinionSheet].[ID],
 [dbo].[OpinionSheet].[CompanyID],
 [dbo].[OpinionSheet].[Date],
 [dbo].[OpinionSheet].[Staff],
 [dbo].[OpinionSheet].[OpinionSheetCategoryId],
 [dbo].[OpinionSheetCategory].[Category],
 [dbo].[OpinionSheet].[FileData],
 [dbo].[OpinionSheet].[CreatedBy],
 [dbo].[OpinionSheet].[CreatedDate],
 [dbo].[OpinionSheet].[UpdatedBy],
 [dbo].[OpinionSheet].[UpdatedDate]
FROM [dbo].[OpinionSheet]
INNER JOIN [dbo].[OpinionSheetCategory] ON [dbo].[OpinionSheet].OpinionSheetCategoryId =[dbo].[OpinionSheetCategory].ID
ORDER BY 
 [ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''OpinionSheet''
-- based on a foreign key field.
-- Gets: @iOpinionSheetCategoryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheet]
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''OpinionSheet''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheet]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''OpinionSheet''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @daDate datetime
-- Gets: @sStaff nvarchar(250)
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_OpinionSheet_Update]
	@iID int,
	@iCompanyID int,
	@daDate datetime,
	@sStaff nvarchar(250),
	@iOpinionSheetCategoryId int,
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheet]
SET 
	[CompanyID] = @iCompanyID,
	[Date] = @daDate,
	[Staff] = @sStaff,
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId,
	[FileData] = @biFileData,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''OpinionSheet''.
-- Will reset field [OpinionSheetCategoryId] with value @iOpinionSheetCategoryIdOld  to value @iOpinionSheetCategoryId
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @iOpinionSheetCategoryIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iOpinionSheetCategoryIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheet]
SET
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''OpinionSheetCategory''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OpinionSheetCategory]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''OpinionSheetCategory''
-- Gets: @sCategory nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_Insert]
	@sCategory nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OpinionSheetCategory]
(
	[Category],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@sCategory,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''OpinionSheetCategory''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[Category],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheetCategory]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''OpinionSheetCategory''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[Category],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheetCategory]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''OpinionSheetCategory''
-- Gets: @iID int
-- Gets: @sCategory nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_OpinionSheetCategory_Update]
	@iID int,
	@sCategory nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheetCategory]
SET 
	[Category] = @sCategory,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''PriceTemplate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[PriceTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''PriceTemplate''
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sLanguage nvarchar(50)
-- Gets: @sTitle nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_Insert]
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sLanguage nvarchar(50),
	@sTitle nvarchar(250),
	@sDescription nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[PriceTemplate]
(
	[CompanyID],
	[Staff],
	[Language],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sStaff,
	@sLanguage,
	@sTitle,
	@sDescription,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''PriceTemplate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Language],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[PriceTemplate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''PriceTemplate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_PriceTemplate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Staff],
	[Language],
	[Title],
	[Description],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[PriceTemplate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PriceTemplate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PriceTemplate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''PriceTemplate''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sStaff nvarchar(250)
-- Gets: @sLanguage nvarchar(50)
-- Gets: @sTitle nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_PriceTemplate_Update]
	@iID int,
	@iCompanyID int,
	@sStaff nvarchar(250),
	@sLanguage nvarchar(50),
	@sTitle nvarchar(250),
	@sDescription nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[PriceTemplate]
SET 
	[CompanyID] = @iCompanyID,
	[Staff] = @sStaff,
	[Language] = @sLanguage,
	[Title] = @sTitle,
	[Description] = @sDescription,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''Product''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[Product]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''Product''
-- Gets: @iCompanyID int
-- Gets: @daCreationDate datetime
-- Gets: @sContractedBy nvarchar(250)
-- Gets: @sDataEntryBy nvarchar(250)
-- Gets: @sProductType nvarchar(250)
-- Gets: @iSupplierId int
-- Gets: @sSupplierName nvarchar(250)
-- Gets: @sContract nvarchar(250)
-- Gets: @sContractWith nvarchar(250)
-- Gets: @sEmergencyNo nvarchar(250)
-- Gets: @sReservationEmail nvarchar(250)
-- Gets: @sAlternationEmail nvarchar(250)
-- Gets: @sCurrency nvarchar(250)
-- Gets: @sContractType nvarchar(250)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sCountry nvarchar(250)
-- Gets: @sRegion nvarchar(250)
-- Gets: @sTypeOfProduct nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sProductTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @dcPrice decimal(18, 2)
-- Gets: @sComment nvarchar(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sHowToSell nvarchar(Max)
-- Gets: @sPriceIncluding nvarchar(Max)
-- Gets: @sPriceExcluding nvarchar(Max)
-- Gets: @sContactTitle nvarchar(50)
-- Gets: @sContactSurName nvarchar(150)
-- Gets: @sContactFirstName nvarchar(510)
-- Gets: @sContactPosition nvarchar(50)
-- Gets: @sContactDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sContactDirectPhone nvarchar(50)
-- Gets: @sContactMobileNo nvarchar(50)
-- Gets: @sContactFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_Insert]
	@iCompanyID int,
	@daCreationDate datetime,
	@sContractedBy nvarchar(250),
	@sDataEntryBy nvarchar(250),
	@sProductType nvarchar(250),
	@iSupplierId int,
	@sSupplierName nvarchar(250),
	@sContract nvarchar(250),
	@sContractWith nvarchar(250),
	@sEmergencyNo nvarchar(250),
	@sReservationEmail nvarchar(250),
	@sAlternationEmail nvarchar(250),
	@sCurrency nvarchar(250),
	@sContractType nvarchar(250),
	@dcNetRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sCountry nvarchar(250),
	@sRegion nvarchar(250),
	@sTypeOfProduct nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sProductTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@dcPrice decimal(18, 2),
	@sComment nvarchar(Max),
	@sHowToBook nvarchar(Max),
	@sHowToSell nvarchar(Max),
	@sPriceIncluding nvarchar(Max),
	@sPriceExcluding nvarchar(Max),
	@sContactTitle nvarchar(50),
	@sContactSurName nvarchar(150),
	@sContactFirstName nvarchar(510),
	@sContactPosition nvarchar(50),
	@sContactDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sContactDirectPhone nvarchar(50),
	@sContactMobileNo nvarchar(50),
	@sContactFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[Product]
(
	[CompanyID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[ProductTitle],
	[ItineraryTemplate],
	[Price],
	[Comment],
	[HowToBook],
	[HowToSell],
	[PriceIncluding],
	[PriceExcluding],
	[ContactTitle],
	[ContactSurName],
	[ContactFirstName],
	[ContactPosition],
	[ContactDepartment],
	[ContactEmail],
	[ContactDirectPhone],
	[ContactMobileNo],
	[ContactFaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@daCreationDate,
	@sContractedBy,
	@sDataEntryBy,
	@sProductType,
	@iSupplierId,
	@sSupplierName,
	@sContract,
	@sContractWith,
	@sEmergencyNo,
	@sReservationEmail,
	@sAlternationEmail,
	@sCurrency,
	@sContractType,
	@dcNetRate,
	@dcCommission,
	@sCountry,
	@sRegion,
	@sTypeOfProduct,
	@iNoOfDays,
	@iNoOfNights,
	@sProductTitle,
	@sItineraryTemplate,
	@dcPrice,
	@sComment,
	@sHowToBook,
	@sHowToSell,
	@sPriceIncluding,
	@sPriceExcluding,
	@sContactTitle,
	@sContactSurName,
	@sContactFirstName,
	@sContactPosition,
	@sContactDepartment,
	@sContactEmail,
	@sContactDirectPhone,
	@sContactMobileNo,
	@sContactFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''Product''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[ProductTitle],
	[ItineraryTemplate],
	[Price],
	[Comment],
	[HowToBook],
	[HowToSell],
	[PriceIncluding],
	[PriceExcluding],
	[ContactTitle],
	[ContactSurName],
	[ContactFirstName],
	[ContactPosition],
	[ContactDepartment],
	[ContactEmail],
	[ContactDirectPhone],
	[ContactMobileNo],
	[ContactFaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''Product''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[CreationDate],
	[ContractedBy],
	[DataEntryBy],
	[ProductType],
	[SupplierId],
	[SupplierName],
	[Contract],
	[ContractWith],
	[EmergencyNo],
	[ReservationEmail],
	[AlternationEmail],
	[Currency],
	[ContractType],
	[NetRate],
	[Commission],
	[Country],
	[Region],
	[TypeOfProduct],
	[NoOfDays],
	[NoOfNights],
	[ProductTitle],
	[ItineraryTemplate],
	[Price],
	[Comment],
	[HowToBook],
	[HowToSell],
	[PriceIncluding],
	[PriceExcluding],
	[ContactTitle],
	[ContactSurName],
	[ContactFirstName],
	[ContactPosition],
	[ContactDepartment],
	[ContactEmail],
	[ContactDirectPhone],
	[ContactMobileNo],
	[ContactFaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Product_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Product_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''Product''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @daCreationDate datetime
-- Gets: @sContractedBy nvarchar(250)
-- Gets: @sDataEntryBy nvarchar(250)
-- Gets: @sProductType nvarchar(250)
-- Gets: @iSupplierId int
-- Gets: @sSupplierName nvarchar(250)
-- Gets: @sContract nvarchar(250)
-- Gets: @sContractWith nvarchar(250)
-- Gets: @sEmergencyNo nvarchar(250)
-- Gets: @sReservationEmail nvarchar(250)
-- Gets: @sAlternationEmail nvarchar(250)
-- Gets: @sCurrency nvarchar(250)
-- Gets: @sContractType nvarchar(250)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sCountry nvarchar(250)
-- Gets: @sRegion nvarchar(250)
-- Gets: @sTypeOfProduct nvarchar(50)
-- Gets: @iNoOfDays int
-- Gets: @iNoOfNights int
-- Gets: @sProductTitle nvarchar(250)
-- Gets: @sItineraryTemplate nvarchar(Max)
-- Gets: @dcPrice decimal(18, 2)
-- Gets: @sComment nvarchar(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sHowToSell nvarchar(Max)
-- Gets: @sPriceIncluding nvarchar(Max)
-- Gets: @sPriceExcluding nvarchar(Max)
-- Gets: @sContactTitle nvarchar(50)
-- Gets: @sContactSurName nvarchar(150)
-- Gets: @sContactFirstName nvarchar(510)
-- Gets: @sContactPosition nvarchar(50)
-- Gets: @sContactDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sContactDirectPhone nvarchar(50)
-- Gets: @sContactMobileNo nvarchar(50)
-- Gets: @sContactFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_Product_Update]
	@iID int,
	@iCompanyID int,
	@daCreationDate datetime,
	@sContractedBy nvarchar(250),
	@sDataEntryBy nvarchar(250),
	@sProductType nvarchar(250),
	@iSupplierId int,
	@sSupplierName nvarchar(250),
	@sContract nvarchar(250),
	@sContractWith nvarchar(250),
	@sEmergencyNo nvarchar(250),
	@sReservationEmail nvarchar(250),
	@sAlternationEmail nvarchar(250),
	@sCurrency nvarchar(250),
	@sContractType nvarchar(250),
	@dcNetRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sCountry nvarchar(250),
	@sRegion nvarchar(250),
	@sTypeOfProduct nvarchar(50),
	@iNoOfDays int,
	@iNoOfNights int,
	@sProductTitle nvarchar(250),
	@sItineraryTemplate nvarchar(Max),
	@dcPrice decimal(18, 2),
	@sComment nvarchar(Max),
	@sHowToBook nvarchar(Max),
	@sHowToSell nvarchar(Max),
	@sPriceIncluding nvarchar(Max),
	@sPriceExcluding nvarchar(Max),
	@sContactTitle nvarchar(50),
	@sContactSurName nvarchar(150),
	@sContactFirstName nvarchar(510),
	@sContactPosition nvarchar(50),
	@sContactDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sContactDirectPhone nvarchar(50),
	@sContactMobileNo nvarchar(50),
	@sContactFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Product]
SET 
	[CompanyID] = @iCompanyID,
	[CreationDate] = @daCreationDate,
	[ContractedBy] = @sContractedBy,
	[DataEntryBy] = @sDataEntryBy,
	[ProductType] = @sProductType,
	[SupplierId] = @iSupplierId,
	[SupplierName] = @sSupplierName,
	[Contract] = @sContract,
	[ContractWith] = @sContractWith,
	[EmergencyNo] = @sEmergencyNo,
	[ReservationEmail] = @sReservationEmail,
	[AlternationEmail] = @sAlternationEmail,
	[Currency] = @sCurrency,
	[ContractType] = @sContractType,
	[NetRate] = @dcNetRate,
	[Commission] = @dcCommission,
	[Country] = @sCountry,
	[Region] = @sRegion,
	[TypeOfProduct] = @sTypeOfProduct,
	[NoOfDays] = @iNoOfDays,
	[NoOfNights] = @iNoOfNights,
	[ProductTitle] = @sProductTitle,
	[ItineraryTemplate] = @sItineraryTemplate,
	[Price] = @dcPrice,
	[Comment] = @sComment,
	[HowToBook] = @sHowToBook,
	[HowToSell] = @sHowToSell,
	[PriceIncluding] = @sPriceIncluding,
	[PriceExcluding] = @sPriceExcluding,
	[ContactTitle] = @sContactTitle,
	[ContactSurName] = @sContactSurName,
	[ContactFirstName] = @sContactFirstName,
	[ContactPosition] = @sContactPosition,
	[ContactDepartment] = @sContactDepartment,
	[ContactEmail] = @sContactEmail,
	[ContactDirectPhone] = @sContactDirectPhone,
	[ContactMobileNo] = @sContactMobileNo,
	[ContactFaxNo] = @sContactFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ProductContact''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ProductContact]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ProductContact''
-- based on a foreign key field.
-- Gets: @iProductlID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_DeleteAllWProductlIDLogic]
	@iProductlID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ProductContact]
WHERE
	[ProductlID] = @iProductlID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ProductContact''
-- Gets: @iProductlID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_Insert]
	@iProductlID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ProductContact]
(
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iProductlID,
	@sTitle,
	@sSurName,
	@sFirstName,
	@bMainContact,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ProductContact''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductContact]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectAllWProductlIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ProductContact''
-- based on a foreign key field.
-- Gets: @iProductlID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_SelectAllWProductlIDLogic]
	@iProductlID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductContact]
WHERE
	[ProductlID] = @iProductlID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ProductContact''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ProductlID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductContact]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ProductContact''
-- Gets: @iID int
-- Gets: @iProductlID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_ProductContact_Update]
	@iID int,
	@iProductlID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductContact]
SET 
	[ProductlID] = @iProductlID,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[MainContact] = @bMainContact,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ProductContact''.
-- Will reset field [ProductlID] with value @iProductlIDOld  to value @iProductlID
-- Gets: @iProductlID int
-- Gets: @iProductlIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductContact_UpdateAllWProductlIDLogic]
	@iProductlID int,
	@iProductlIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductContact]
SET
	[ProductlID] = @iProductlID
WHERE
	[ProductlID] = @iProductlIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ProductDeliveryDetail''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ProductDeliveryDetail''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_DeleteAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ProductDeliveryDetail''
-- Gets: @iProductID int
-- Gets: @sRoute nvarchar(250)
-- Gets: @sItinerary nvarchar(MAX)
-- Gets: @sHotel nvarchar(250)
-- Gets: @sBreakFast nvarchar(250)
-- Gets: @sLunch nchar(10)
-- Gets: @sDinner nchar(10)
-- Gets: @sTransportation nchar(10)
-- Gets: @biPhotoData varbinary(MAX)
-- Gets: @biPhotoData1 varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_Insert]
	@iProductID int,
	@sRoute nvarchar(250),
	@sItinerary nvarchar(MAX),
	@sHotel nvarchar(250),
	@sBreakFast nvarchar(250),
	@sLunch nchar(10),
	@sDinner nchar(10),
	@sTransportation nchar(10),
	@biPhotoData varbinary(MAX),
	@biPhotoData1 varbinary(MAX),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ProductDeliveryDetail]
(
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iProductID,
	@sRoute,
	@sItinerary,
	@sHotel,
	@sBreakFast,
	@sLunch,
	@sDinner,
	@sTransportation,
	@biPhotoData,
	@biPhotoData1,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ProductDeliveryDetail''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDeliveryDetail]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ProductDeliveryDetail''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ProductDeliveryDetail''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ProductID],
	[Route],
	[Itinerary],
	[Hotel],
	[BreakFast],
	[Lunch],
	[Dinner],
	[Transportation],
	[PhotoData],
	[PhotoData1],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDeliveryDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ProductDeliveryDetail''
-- Gets: @iID int
-- Gets: @iProductID int
-- Gets: @sRoute nvarchar(250)
-- Gets: @sItinerary nvarchar(MAX)
-- Gets: @sHotel nvarchar(250)
-- Gets: @sBreakFast nvarchar(250)
-- Gets: @sLunch nchar(10)
-- Gets: @sDinner nchar(10)
-- Gets: @sTransportation nchar(10)
-- Gets: @biPhotoData varbinary(MAX)
-- Gets: @biPhotoData1 varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(250)
-- Gets: @sExtraField2 nvarchar(250)
-- Gets: @sExtraField3 nvarchar(250)
-- Gets: @sExtraField4 nvarchar(250)
-- Gets: @sExtraField5 nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_ProductDeliveryDetail_Update]
	@iID int,
	@iProductID int,
	@sRoute nvarchar(250),
	@sItinerary nvarchar(MAX),
	@sHotel nvarchar(250),
	@sBreakFast nvarchar(250),
	@sLunch nchar(10),
	@sDinner nchar(10),
	@sTransportation nchar(10),
	@biPhotoData varbinary(MAX),
	@biPhotoData1 varbinary(MAX),
	@sExtraField1 nvarchar(250),
	@sExtraField2 nvarchar(250),
	@sExtraField3 nvarchar(250),
	@sExtraField4 nvarchar(250),
	@sExtraField5 nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDeliveryDetail]
SET 
	[ProductID] = @iProductID,
	[Route] = @sRoute,
	[Itinerary] = @sItinerary,
	[Hotel] = @sHotel,
	[BreakFast] = @sBreakFast,
	[Lunch] = @sLunch,
	[Dinner] = @sDinner,
	[Transportation] = @sTransportation,
	[PhotoData] = @biPhotoData,
	[PhotoData1] = @biPhotoData1,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ProductDeliveryDetail''.
-- Will reset field [ProductID] with value @iProductIDOld  to value @iProductID
-- Gets: @iProductID int
-- Gets: @iProductIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDeliveryDetail_UpdateAllWProductIDLogic]
	@iProductID int,
	@iProductIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDeliveryDetail]
SET
	[ProductID] = @iProductID
WHERE
	[ProductID] = @iProductIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ProductDocument''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ProductDocument]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ProductDocument''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_DeleteAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ProductDocument]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ProductDocument''
-- Gets: @iProductID int
-- Gets: @sFileName nvarchar(250)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_Insert]
	@iProductID int,
	@sFileName nvarchar(250),
	@biFileData varbinary(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ProductDocument]
(
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iProductID,
	@sFileName,
	@biFileData,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ProductDocument''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDocument]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ProductDocument''
-- based on a foreign key field.
-- Gets: @iProductID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_SelectAllWProductIDLogic]
	@iProductID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDocument]
WHERE
	[ProductID] = @iProductID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ProductDocument''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ProductID],
	[FileName],
	[FileData],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ProductDocument]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ProductDocument''
-- Gets: @iID int
-- Gets: @iProductID int
-- Gets: @sFileName nvarchar(250)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_ProductDocument_Update]
	@iID int,
	@iProductID int,
	@sFileName nvarchar(250),
	@biFileData varbinary(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDocument]
SET 
	[ProductID] = @iProductID,
	[FileName] = @sFileName,
	[FileData] = @biFileData,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ProductDocument''.
-- Will reset field [ProductID] with value @iProductIDOld  to value @iProductID
-- Gets: @iProductID int
-- Gets: @iProductIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ProductDocument_UpdateAllWProductIDLogic]
	@iProductID int,
	@iProductIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ProductDocument]
SET
	[ProductID] = @iProductID
WHERE
	[ProductID] = @iProductIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierContacts''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierContacts''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierContacts]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierContacts''
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_Insert]
	@iSupplierDetailID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierContacts]
(
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sTitle,
	@sSurName,
	@sFirstName,
	@bMainContact,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierContacts''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierContacts]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierContacts''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierContacts]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierContacts''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierContacts''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierContacts_Update]
	@iID int,
	@iSupplierDetailID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierContacts]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[MainContact] = @bMainContact,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierContacts''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierContacts_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierContacts]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierDMCRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierDMCRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierDMCRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierDMCRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierDMCRate''
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(250)
-- Gets: @biFileData varbinary(Max)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcGrossRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sRemarks nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_Insert]
	@iSupplierDetailID int,
	@sTitle nvarchar(250),
	@biFileData varbinary(Max),
	@dcNetRate decimal(18, 2),
	@dcGrossRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sRemarks nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierDMCRate]
(
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sTitle,
	@biFileData,
	@dcNetRate,
	@dcGrossRate,
	@dcCommission,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierDMCRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierDMCRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierDMCRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierDMCRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierDMCRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Title],
	[FileData],
	[NetRate],
	[GrossRate],
	[Commission],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierDMCRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierDMCRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sTitle nvarchar(250)
-- Gets: @biFileData varbinary(Max)
-- Gets: @dcNetRate decimal(18, 2)
-- Gets: @dcGrossRate decimal(18, 2)
-- Gets: @dcCommission decimal(18, 2)
-- Gets: @sRemarks nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierDMCRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@sTitle nvarchar(250),
	@biFileData varbinary(Max),
	@dcNetRate decimal(18, 2),
	@dcGrossRate decimal(18, 2),
	@dcCommission decimal(18, 2),
	@sRemarks nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierDMCRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[Title] = @sTitle,
	[FileData] = @biFileData,
	[NetRate] = @dcNetRate,
	[GrossRate] = @dcGrossRate,
	[Commission] = @dcCommission,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierDMCRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierDMCRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierDMCRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierEntranceTicketFit''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierEntranceTicketFit]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierEntranceTicketFit''
-- Gets: @iNoOfPeople int
-- Gets: @dcAdult decimal(18, 2)
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_Insert]
	@iNoOfPeople int,
	@dcAdult decimal(18, 2),
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierEntranceTicketFit]
(
	[NoOfPeople],
	[Adult]
)
VALUES
(
	@iNoOfPeople,
	@dcAdult
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierEntranceTicketFit''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[NoOfPeople],
	[Adult]
FROM [dbo].[SupplierEntranceTicketFit]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierEntranceTicketFit''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketFit_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[NoOfPeople],
	[Adult]
FROM [dbo].[SupplierEntranceTicketFit]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketFit_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketFit_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierEntranceTicketFit''
-- Gets: @iID int
-- Gets: @iNoOfPeople int
-- Gets: @dcAdult decimal(18, 2)
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierEntranceTicketFit_Update]
	@iID int,
	@iNoOfPeople int,
	@dcAdult decimal(18, 2),
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierEntranceTicketFit]
SET 
	[NoOfPeople] = @iNoOfPeople,
	[Adult] = @dcAdult
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierEntranceTicketRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierEntranceTicketRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierEntranceTicketRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daMonthFrom datetime
-- Gets: @daMonthTo datetime
-- Gets: @daDailyOpeningTime datetime
-- Gets: @daDailyCloseTime datetime
-- Gets: @sLastAdmission nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_Insert]
	@iSupplierDetailID int,
	@daMonthFrom datetime,
	@daMonthTo datetime,
	@daDailyOpeningTime datetime,
	@daDailyCloseTime datetime,
	@sLastAdmission nvarchar(250),
	@sStatus nvarchar(50),
	@sRemark nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierEntranceTicketRate]
(
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daMonthFrom,
	@daMonthTo,
	@daDailyOpeningTime,
	@daDailyCloseTime,
	@sLastAdmission,
	@sStatus,
	@sRemark,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierEntranceTicketRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierEntranceTicketRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierEntranceTicketRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierEntranceTicketRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[MonthFrom],
	[MonthTo],
	[DailyOpeningTime],
	[DailyCloseTime],
	[LastAdmission],
	[Status],
	[Remark],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierEntranceTicketRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierEntranceTicketRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daMonthFrom datetime
-- Gets: @daMonthTo datetime
-- Gets: @daDailyOpeningTime datetime
-- Gets: @daDailyCloseTime datetime
-- Gets: @sLastAdmission nvarchar(250)
-- Gets: @sStatus nvarchar(50)
-- Gets: @sRemark nvarchar(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierEntranceTicketRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daMonthFrom datetime,
	@daMonthTo datetime,
	@daDailyOpeningTime datetime,
	@daDailyCloseTime datetime,
	@sLastAdmission nvarchar(250),
	@sStatus nvarchar(50),
	@sRemark nvarchar(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierEntranceTicketRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[MonthFrom] = @daMonthFrom,
	[MonthTo] = @daMonthTo,
	[DailyOpeningTime] = @daDailyOpeningTime,
	[DailyCloseTime] = @daDailyCloseTime,
	[LastAdmission] = @sLastAdmission,
	[Status] = @sStatus,
	[Remark] = @sRemark,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierEntranceTicketRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierEntranceTicketRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierFerryRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierFerryRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierFerryRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierFerryRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierFerryRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sCountryFrom nvarchar(50)
-- Gets: @sPortFrom nvarchar(50)
-- Gets: @sCountryTo nvarchar(50)
-- Gets: @sPortTo nvarchar(50)
-- Gets: @daDepartTime datetime
-- Gets: @daArrivalTime datetime
-- Gets: @bOneWayOrReturn bit
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sFitRate nvarchar(10)
-- Gets: @sGroupRate nvarchar(10)
-- Gets: @sRemark nvarchar(MAX)
-- Gets: @iCabin int
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sCountryFrom nvarchar(50),
	@sPortFrom nvarchar(50),
	@sCountryTo nvarchar(50),
	@sPortTo nvarchar(50),
	@daDepartTime datetime,
	@daArrivalTime datetime,
	@bOneWayOrReturn bit,
	@sDescription nvarchar(MAX),
	@sFitRate nvarchar(10),
	@sGroupRate nvarchar(10),
	@sRemark nvarchar(MAX),
	@iCabin int,
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierFerryRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sCountryFrom,
	@sPortFrom,
	@sCountryTo,
	@sPortTo,
	@daDepartTime,
	@daArrivalTime,
	@bOneWayOrReturn,
	@sDescription,
	@sFitRate,
	@sGroupRate,
	@sRemark,
	@iCabin,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierFerryRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierFerryRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierFerryRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierFerryRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierFerryRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[CountryFrom],
	[PortFrom],
	[CountryTo],
	[PortTo],
	[DepartTime],
	[ArrivalTime],
	[OneWayOrReturn],
	[Description],
	[FitRate],
	[GroupRate],
	[Remark],
	[Cabin],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierFerryRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierFerryRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sCountryFrom nvarchar(50)
-- Gets: @sPortFrom nvarchar(50)
-- Gets: @sCountryTo nvarchar(50)
-- Gets: @sPortTo nvarchar(50)
-- Gets: @daDepartTime datetime
-- Gets: @daArrivalTime datetime
-- Gets: @bOneWayOrReturn bit
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sFitRate nvarchar(10)
-- Gets: @sGroupRate nvarchar(10)
-- Gets: @sRemark nvarchar(MAX)
-- Gets: @iCabin int
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierFerryRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sCountryFrom nvarchar(50),
	@sPortFrom nvarchar(50),
	@sCountryTo nvarchar(50),
	@sPortTo nvarchar(50),
	@daDepartTime datetime,
	@daArrivalTime datetime,
	@bOneWayOrReturn bit,
	@sDescription nvarchar(MAX),
	@sFitRate nvarchar(10),
	@sGroupRate nvarchar(10),
	@sRemark nvarchar(MAX),
	@iCabin int,
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierFerryRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[CountryFrom] = @sCountryFrom,
	[PortFrom] = @sPortFrom,
	[CountryTo] = @sCountryTo,
	[PortTo] = @sPortTo,
	[DepartTime] = @daDepartTime,
	[ArrivalTime] = @daArrivalTime,
	[OneWayOrReturn] = @bOneWayOrReturn,
	[Description] = @sDescription,
	[FitRate] = @sFitRate,
	[GroupRate] = @sGroupRate,
	[Remark] = @sRemark,
	[Cabin] = @iCabin,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierFerryRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierFerryRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierFerryRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierHotelRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierHotelRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierHotelRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierHotelRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierHotelRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @sHalfTwin nvarchar(150)
-- Gets: @sSingle nvarchar(50)
-- Gets: @sTwin_DoubleForSoleUse nvarchar(50)
-- Gets: @sTriple nvarchar(50)
-- Gets: @sFamilyRoom nvarchar(50)
-- Gets: @sBreakfast nvarchar(50)
-- Gets: @sDinnerAtHotel nvarchar(50)
-- Gets: @sRatePerPerson nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@daFromDate datetime,
	@daToDate datetime,
	@sHalfTwin nvarchar(150),
	@sSingle nvarchar(50),
	@sTwin_DoubleForSoleUse nvarchar(50),
	@sTriple nvarchar(50),
	@sFamilyRoom nvarchar(50),
	@sBreakfast nvarchar(50),
	@sDinnerAtHotel nvarchar(50),
	@sRatePerPerson nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierHotelRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@daFromDate,
	@daToDate,
	@sHalfTwin,
	@sSingle,
	@sTwin_DoubleForSoleUse,
	@sTriple,
	@sFamilyRoom,
	@sBreakfast,
	@sDinnerAtHotel,
	@sRatePerPerson,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierHotelRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierHotelRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierHotelRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierHotelRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierHotelRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[FromDate],
	[ToDate],
	[HalfTwin],
	[Single],
	[Twin_DoubleForSoleUse],
	[Triple],
	[FamilyRoom],
	[Breakfast],
	[DinnerAtHotel],
	[RatePerPerson],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierHotelRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierHotelRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @sHalfTwin nvarchar(150)
-- Gets: @sSingle nvarchar(50)
-- Gets: @sTwin_DoubleForSoleUse nvarchar(50)
-- Gets: @sTriple nvarchar(50)
-- Gets: @sFamilyRoom nvarchar(50)
-- Gets: @sBreakfast nvarchar(50)
-- Gets: @sDinnerAtHotel nvarchar(50)
-- Gets: @sRatePerPerson nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierHotelRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@daFromDate datetime,
	@daToDate datetime,
	@sHalfTwin nvarchar(150),
	@sSingle nvarchar(50),
	@sTwin_DoubleForSoleUse nvarchar(50),
	@sTriple nvarchar(50),
	@sFamilyRoom nvarchar(50),
	@sBreakfast nvarchar(50),
	@sDinnerAtHotel nvarchar(50),
	@sRatePerPerson nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierHotelRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[FromDate] = @daFromDate,
	[ToDate] = @daToDate,
	[HalfTwin] = @sHalfTwin,
	[Single] = @sSingle,
	[Twin_DoubleForSoleUse] = @sTwin_DoubleForSoleUse,
	[Triple] = @sTriple,
	[FamilyRoom] = @sFamilyRoom,
	[Breakfast] = @sBreakfast,
	[DinnerAtHotel] = @sDinnerAtHotel,
	[RatePerPerson] = @sRatePerPerson,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierHotelRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierHotelRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierHotelRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierInformation''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierInformation]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierInformation''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierInformation]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierInformation''
-- Gets: @iSupplierDetailID int
-- Gets: @sCurrency nvarchar(10)
-- Gets: @iNoofBedRooms int
-- Gets: @sHotelFacility nvarchar(MAX)
-- Gets: @sParking nvarchar(50)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_Insert]
	@iSupplierDetailID int,
	@sCurrency nvarchar(10),
	@iNoofBedRooms int,
	@sHotelFacility nvarchar(MAX),
	@sParking nvarchar(50),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierInformation]
(
	[SupplierDetailID],
	[Currency],
	[NoofBedRooms],
	[HotelFacility],
	[Parking],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sCurrency,
	@iNoofBedRooms,
	@sHotelFacility,
	@sParking,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierInformation''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[Currency],
	[NoofBedRooms],
	[HotelFacility],
	[Parking],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInformation]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierInformation''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[SupplierInformation].[ID],
	[SupplierInformation].[SupplierDetailID],
	[SupplierInformation].[Currency],
	[SupplierInformation].[NoofBedRooms],
	[SupplierInformation].[HotelFacility],
	[SupplierInformation].[Parking],
	[SupplierInformation].[Remarks],
	[SupplierInformation].[ExtraField1],
	[SupplierInformation].[ExtraField2],
	[SupplierInformation].[ExtraField3],
	[SupplierInformation].[ExtraField4],
	[SupplierInformation].[ExtraField5],
	[SupplierInformation].[CreatedBy],
	[SupplierInformation].[CreatedDate],
	[SupplierInformation].[UpdatedBy],
	[SupplierInformation].[UpdatedDate] 

	,SM.[Name] AS   Name
	 ,SM.City  as City
	 ,SM.[State] as  State
	 ,SM.Country as  Country
	 ,SM.Telephone as Telephone
	 ,SM.PostalCode as  PostalCode
	 ,SM.Address1 as  Address1
	 ,SM.Address2 as  Address2
FROM [dbo].[SupplierInformation]
JOIN dbo.SuppliersDetails SD on  [SupplierInformation].[SupplierDetailID]=SD.ID
JOIN SupplierMaster SM on SD.[SupplierMasterID] = SM.ID
WHERE
	[SupplierDetailID] = @iSupplierDetailID

-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierInformation''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	SI.[ID],
	SI.[SupplierDetailID],
	SI.[Currency],
	SI.[NoofBedRooms],
	SI.[HotelFacility],
	SI.[Parking],
	SI.[Remarks],
	SI.[ExtraField1],
	SI.[ExtraField2],
	SI.[ExtraField3],
	SI.[ExtraField4],
	SI.[ExtraField5],
	SI.[CreatedBy],
	SI.[CreatedDate],
	SI.[UpdatedBy],
	SI.[UpdatedDate] 
	 ,SM.[Name] AS SupplierName
	 ,SM.[Address1] as InfoAddress1
	 ,SM.[Address2] as InfoAddress2
	 ,SM.City as InfoCity
	 ,SM.[State] as InfoState
	 ,SM.Country as InfoCountry
	 ,SM.Telephone as InfoTelephone
	 ,SM.PostalCode as InfoPostalCode
FROM [dbo].[SupplierInformation] SI
JOIN dbo.SuppliersDetails SD on  SI.[SupplierDetailID]=SD.ID
JOIN SupplierMaster SM on SD.[SupplierMasterID] = SM.ID
WHERE
	SI.[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierInformation''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sCurrency nvarchar(10)
-- Gets: @iNoofBedRooms int
-- Gets: @sHotelFacility nvarchar(MAX)
-- Gets: @sParking nvarchar(50)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierInformation_Update]
	@iID int,
	@iSupplierDetailID int,
	@sCurrency nvarchar(10),
	@iNoofBedRooms int,
	@sHotelFacility nvarchar(MAX),
	@sParking nvarchar(50),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInformation]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[Currency] = @sCurrency,
	[NoofBedRooms] = @iNoofBedRooms,
	[HotelFacility] = @sHotelFacility,
	[Parking] = @sParking,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierInformation''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInformation]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierInsuranceRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierInsuranceRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierInsuranceRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierInsuranceRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierInsuranceRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRateDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRateDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierInsuranceRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sRateDescription,
	@sRate,
	@sComments,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierInsuranceRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInsuranceRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierInsuranceRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInsuranceRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierInsuranceRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RateDescription],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierInsuranceRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierInsuranceRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRateDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierInsuranceRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRateDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInsuranceRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[RateDescription] = @sRateDescription,
	[Rate] = @sRate,
	[Comments] = @sComments,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierInsuranceRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierInsuranceRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierInsuranceRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierMaster''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierMaster]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierMaster''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_DeleteAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierMaster]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierMaster''
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeID int
-- Gets: @sName nvarchar(250)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(MAX)
-- Gets: @daContractEndingDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_Insert]
	@iCompanyID int,
	@iSupplierTypeID int,
	@sName nvarchar(250),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sPostalCode nvarchar(6),
	@sEmail nvarchar(50),
	@sCreditToLetsTravel nvarchar(50),
	@sPaymentTerms nvarchar(MAX),
	@daContractEndingDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierMaster]
(
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierTypeID,
	@sName,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sTelephone,
	@sPostalCode,
	@sEmail,
	@sCreditToLetsTravel,
	@sPaymentTerms,
	@daContractEndingDate,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierMaster''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMaster]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N' 
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierMaster''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_SelectAllWSupplierTypeIDLogic]
	@iSupplierTypeID int= 0,
	@iID int= 0,
	@iCompanyID  int= 0,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[SupplierMaster].[ID],
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate],
	ST.SupplierType
FROM [dbo].[SupplierMaster]
LEFT JOIN SupplierType ST On [SupplierMaster].SupplierTypeID = ST.ID
WHERE
	([SupplierTypeID] = @iSupplierTypeID or @iSupplierTypeID = 0 )
	AND ( [SupplierMaster].ID = @iID OR @iID =0)
	AND ( CompanyID = @iCompanyID OR @iCompanyID=0)
Order by [CreatedDate] desc
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierMaster''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeID],
	[Name],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[Telephone],
	[PostalCode],
	[Email],
	[CreditToLetsTravel],
	[PaymentTerms],
	[ContractEndingDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMaster]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierMaster''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeID int
-- Gets: @sName nvarchar(250)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(MAX)
-- Gets: @daContractEndingDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierMaster_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierTypeID int,
	@sName nvarchar(250),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sTelephone nvarchar(15),
	@sPostalCode nvarchar(6),
	@sEmail nvarchar(50),
	@sCreditToLetsTravel nvarchar(50),
	@sPaymentTerms nvarchar(MAX),
	@daContractEndingDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMaster]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierTypeID] = @iSupplierTypeID,
	[Name] = @sName,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[Telephone] = @sTelephone,
	[PostalCode] = @sPostalCode,
	[Email] = @sEmail,
	[CreditToLetsTravel] = @sCreditToLetsTravel,
	[PaymentTerms] = @sPaymentTerms,
	[ContractEndingDate] = @daContractEndingDate,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierMaster''.
-- Will reset field [SupplierTypeID] with value @iSupplierTypeIDOld  to value @iSupplierTypeID
-- Gets: @iSupplierTypeID int
-- Gets: @iSupplierTypeIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMaster_UpdateAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iSupplierTypeIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMaster]
SET
	[SupplierTypeID] = @iSupplierTypeID
WHERE
	[SupplierTypeID] = @iSupplierTypeIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierMealSupplement''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierMealSupplement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierMealSupplement''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_DeleteAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierMealSupplement]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierMealSupplement''
-- Gets: @iSupplierTypeID int
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sPurpose nvarchar(MAX)
-- Gets: @sMealSupplementType nvarchar(50)
-- Gets: @dcUK decimal(18, 0)
-- Gets: @dcIreland decimal(18, 0)
-- Gets: @dcEurope decimal(18, 0)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_Insert]
	@iSupplierTypeID int,
	@sAddedBy nvarchar(50),
	@sPurpose nvarchar(MAX),
	@sMealSupplementType nvarchar(50),
	@dcUK decimal(18, 0),
	@dcIreland decimal(18, 0),
	@dcEurope decimal(18, 0),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierMealSupplement]
(
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierTypeID,
	@sAddedBy,
	@sPurpose,
	@sMealSupplementType,
	@dcUK,
	@dcIreland,
	@dcEurope,
	@sCity,
	@sState,
	@sCountry,
	@sPostalCode,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierMealSupplement''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMealSupplement]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierMealSupplement''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMealSupplement]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierMealSupplement''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierTypeID],
	[AddedBy],
	[Purpose],
	[MealSupplementType],
	[UK],
	[Ireland],
	[Europe],
	[City],
	[State],
	[Country],
	[PostalCode],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierMealSupplement]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierMealSupplement''
-- Gets: @iID int
-- Gets: @iSupplierTypeID int
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sPurpose nvarchar(MAX)
-- Gets: @sMealSupplementType nvarchar(50)
-- Gets: @dcUK decimal(18, 0)
-- Gets: @dcIreland decimal(18, 0)
-- Gets: @dcEurope decimal(18, 0)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierMealSupplement_Update]
	@iID int,
	@iSupplierTypeID int,
	@sAddedBy nvarchar(50),
	@sPurpose nvarchar(MAX),
	@sMealSupplementType nvarchar(50),
	@dcUK decimal(18, 0),
	@dcIreland decimal(18, 0),
	@dcEurope decimal(18, 0),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMealSupplement]
SET 
	[SupplierTypeID] = @iSupplierTypeID,
	[AddedBy] = @sAddedBy,
	[Purpose] = @sPurpose,
	[MealSupplementType] = @sMealSupplementType,
	[UK] = @dcUK,
	[Ireland] = @dcIreland,
	[Europe] = @dcEurope,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[PostalCode] = @sPostalCode,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierMealSupplement''.
-- Will reset field [SupplierTypeID] with value @iSupplierTypeIDOld  to value @iSupplierTypeID
-- Gets: @iSupplierTypeID int
-- Gets: @iSupplierTypeIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierMealSupplement_UpdateAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iSupplierTypeIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierMealSupplement]
SET
	[SupplierTypeID] = @iSupplierTypeID
WHERE
	[SupplierTypeID] = @iSupplierTypeIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierRestaurantRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierRestaurantRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierRestaurantRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierRestaurantRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierRestaurantRate''
-- Gets: @iSupplierDetailID int
-- Gets: @sTypeOfFood nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @dcRate decimal(18, 2)
-- Gets: @sSampleOfMenu nvarchar(Max)
-- Gets: @biSampleMenuFile varbinary(Max)
-- Gets: @biFoodPictureFile varbinary(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_Insert]
	@iSupplierDetailID int,
	@sTypeOfFood nvarchar(250),
	@sDescription nvarchar(Max),
	@dcRate decimal(18, 2),
	@sSampleOfMenu nvarchar(Max),
	@biSampleMenuFile varbinary(Max),
	@biFoodPictureFile varbinary(Max),
	@sComments nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierRestaurantRate]
(
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@sTypeOfFood,
	@sDescription,
	@dcRate,
	@sSampleOfMenu,
	@biSampleMenuFile,
	@biFoodPictureFile,
	@sComments,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierRestaurantRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierRestaurantRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierRestaurantRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierRestaurantRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierRestaurantRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[TypeOfFood],
	[Description],
	[Rate],
	[SampleOfMenu],
	[SampleMenuFile],
	[FoodPictureFile],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierRestaurantRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierRestaurantRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @sTypeOfFood nvarchar(250)
-- Gets: @sDescription nvarchar(Max)
-- Gets: @dcRate decimal(18, 2)
-- Gets: @sSampleOfMenu nvarchar(Max)
-- Gets: @biSampleMenuFile varbinary(Max)
-- Gets: @biFoodPictureFile varbinary(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierRestaurantRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@sTypeOfFood nvarchar(250),
	@sDescription nvarchar(Max),
	@dcRate decimal(18, 2),
	@sSampleOfMenu nvarchar(Max),
	@biSampleMenuFile varbinary(Max),
	@biFoodPictureFile varbinary(Max),
	@sComments nvarchar(Max),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierRestaurantRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[TypeOfFood] = @sTypeOfFood,
	[Description] = @sDescription,
	[Rate] = @dcRate,
	[SampleOfMenu] = @sSampleOfMenu,
	[SampleMenuFile] = @biSampleMenuFile,
	[FoodPictureFile] = @biFoodPictureFile,
	[Comments] = @sComments,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierRestaurantRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierRestaurantRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SuppliersDetails''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SuppliersDetails]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierMasterID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierMasterID] = @iSupplierMasterID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SuppliersDetails''
-- Gets: @iCompanyID int
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierTypeID int
-- Gets: @daSupplierDate datetime
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sName nvarchar(50)
-- Gets: @sStarRating nvarchar(10)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sAgency nvarchar(100)
-- Gets: @sClientCode nvarchar(50)
-- Gets: @sUserName nvarchar(50)
-- Gets: @sPassword nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFaxNumber nvarchar(15)
-- Gets: @sEmergencyNo nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sHotelChain nvarchar(50)
-- Gets: @sFranchised nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @daContractStartDate datetime
-- Gets: @daContractEndingDate datetime
-- Gets: @daContractRenewalDate datetime
-- Gets: @sRating nvarchar(10)
-- Gets: @sMainBusiness nvarchar(200)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biFileData varbinary(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_Insert]
	@iCompanyID int,
	@iSupplierMasterID int,
	@iSupplierTypeID int,
	@daSupplierDate datetime,
	@sContractedBy nvarchar(50),
	@sName nvarchar(50),
	@sStarRating nvarchar(10),
	@sWebsite nvarchar(150),
	@sAgency nvarchar(100),
	@sClientCode nvarchar(50),
	@sUserName nvarchar(50),
	@sPassword nvarchar(50),
	@sTelephone nvarchar(15),
	@sFaxNumber nvarchar(15),
	@sEmergencyNo nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sHotelChain nvarchar(50),
	@sFranchised nvarchar(50),
	@sPaymentTerms nvarchar(Max),
	@sCreditToLetsTravel nvarchar(50),
	@sBlackList nvarchar(200),
	@daContractStartDate datetime,
	@daContractEndingDate datetime,
	@daContractRenewalDate datetime,
	@sRating nvarchar(10),
	@sMainBusiness nvarchar(200),
	@sComments nvarchar(Max),
	@biFileData varbinary(Max),
	@sHowToBook nvarchar(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SuppliersDetails]
(
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierMasterID,
	@iSupplierTypeID,
	@daSupplierDate,
	@sContractedBy,
	@sName,
	@sStarRating,
	@sWebsite,
	@sAgency,
	@sClientCode,
	@sUserName,
	@sPassword,
	@sTelephone,
	@sFaxNumber,
	@sEmergencyNo,
	@sEmail,
	@sAlternateEmail,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sPostalCode,
	@sHotelChain,
	@sFranchised,
	@sPaymentTerms,
	@sCreditToLetsTravel,
	@sBlackList,
	@daContractStartDate,
	@daContractEndingDate,
	@daContractRenewalDate,
	@sRating,
	@sMainBusiness,
	@sComments,
	@biFileData,
	@sHowToBook,
	@sTitle,
	@sSurName,
	@sFirstName,
	@sPosition,
	@sDepartment,
	@sContactEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SuppliersDetails''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierMasterID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierMasterID] = @iSupplierMasterID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SuppliersDetails''
-- based on a foreign key field.
-- Gets: @iSupplierTypeID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[SupplierTypeID] = @iSupplierTypeID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SuppliersDetails''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierMasterID],
	[SupplierTypeID],
	[SupplierDate],
	[ContractedBy],
	[Name],
	[StarRating],
	[Website],
	[Agency],
	[ClientCode],
	[UserName],
	[Password],
	[Telephone],
	[FaxNumber],
	[EmergencyNo],
	[Email],
	[AlternateEmail],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[HotelChain],
	[Franchised],
	[PaymentTerms],
	[CreditToLetsTravel],
	[BlackList],
	[ContractStartDate],
	[ContractEndingDate],
	[ContractRenewalDate],
	[Rating],
	[MainBusiness],
	[Comments],
	[FileData],
	[HowToBook],
	[Title],
	[SurName],
	[FirstName],
	[Position],
	[Department],
	[ContactEmail],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SuppliersDetails]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SuppliersDetails''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierTypeID int
-- Gets: @daSupplierDate datetime
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sName nvarchar(50)
-- Gets: @sStarRating nvarchar(10)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sAgency nvarchar(100)
-- Gets: @sClientCode nvarchar(50)
-- Gets: @sUserName nvarchar(50)
-- Gets: @sPassword nvarchar(50)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFaxNumber nvarchar(15)
-- Gets: @sEmergencyNo nvarchar(15)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAlternateEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(250)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sHotelChain nvarchar(50)
-- Gets: @sFranchised nvarchar(50)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sCreditToLetsTravel nvarchar(50)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @daContractStartDate datetime
-- Gets: @daContractEndingDate datetime
-- Gets: @daContractRenewalDate datetime
-- Gets: @sRating nvarchar(10)
-- Gets: @sMainBusiness nvarchar(200)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biFileData varbinary(Max)
-- Gets: @sHowToBook nvarchar(Max)
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sContactEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SuppliersDetails_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierMasterID int,
	@iSupplierTypeID int,
	@daSupplierDate datetime,
	@sContractedBy nvarchar(50),
	@sName nvarchar(50),
	@sStarRating nvarchar(10),
	@sWebsite nvarchar(150),
	@sAgency nvarchar(100),
	@sClientCode nvarchar(50),
	@sUserName nvarchar(50),
	@sPassword nvarchar(50),
	@sTelephone nvarchar(15),
	@sFaxNumber nvarchar(15),
	@sEmergencyNo nvarchar(15),
	@sEmail nvarchar(50),
	@sAlternateEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(250),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sHotelChain nvarchar(50),
	@sFranchised nvarchar(50),
	@sPaymentTerms nvarchar(Max),
	@sCreditToLetsTravel nvarchar(50),
	@sBlackList nvarchar(200),
	@daContractStartDate datetime,
	@daContractEndingDate datetime,
	@daContractRenewalDate datetime,
	@sRating nvarchar(10),
	@sMainBusiness nvarchar(200),
	@sComments nvarchar(Max),
	@biFileData varbinary(Max),
	@sHowToBook nvarchar(Max),
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sContactEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierMasterID] = @iSupplierMasterID,
	[SupplierTypeID] = @iSupplierTypeID,
	[SupplierDate] = @daSupplierDate,
	[ContractedBy] = @sContractedBy,
	[Name] = @sName,
	[StarRating] = @sStarRating,
	[Website] = @sWebsite,
	[Agency] = @sAgency,
	[ClientCode] = @sClientCode,
	[UserName] = @sUserName,
	[Password] = @sPassword,
	[Telephone] = @sTelephone,
	[FaxNumber] = @sFaxNumber,
	[EmergencyNo] = @sEmergencyNo,
	[Email] = @sEmail,
	[AlternateEmail] = @sAlternateEmail,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[PostalCode] = @sPostalCode,
	[HotelChain] = @sHotelChain,
	[Franchised] = @sFranchised,
	[PaymentTerms] = @sPaymentTerms,
	[CreditToLetsTravel] = @sCreditToLetsTravel,
	[BlackList] = @sBlackList,
	[ContractStartDate] = @daContractStartDate,
	[ContractEndingDate] = @daContractEndingDate,
	[ContractRenewalDate] = @daContractRenewalDate,
	[Rating] = @sRating,
	[MainBusiness] = @sMainBusiness,
	[Comments] = @sComments,
	[FileData] = @biFileData,
	[HowToBook] = @sHowToBook,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[ContactEmail] = @sContactEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SuppliersDetails''.
-- Will reset field [SupplierMasterID] with value @iSupplierMasterIDOld  to value @iSupplierMasterID
-- Gets: @iSupplierMasterID int
-- Gets: @iSupplierMasterIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]
	@iSupplierMasterID int,
	@iSupplierMasterIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET
	[SupplierMasterID] = @iSupplierMasterID
WHERE
	[SupplierMasterID] = @iSupplierMasterIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SuppliersDetails''.
-- Will reset field [SupplierTypeID] with value @iSupplierTypeIDOld  to value @iSupplierTypeID
-- Gets: @iSupplierTypeID int
-- Gets: @iSupplierTypeIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]
	@iSupplierTypeID int,
	@iSupplierTypeIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SuppliersDetails]
SET
	[SupplierTypeID] = @iSupplierTypeID
WHERE
	[SupplierTypeID] = @iSupplierTypeIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTheaterRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTheaterRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTheaterRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTheaterRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTheaterRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTheaterRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sDescription,
	@sRate,
	@sComments,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTheaterRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTheaterRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTheaterRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTheaterRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTheaterRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[Description],
	[Rate],
	[Comments],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTheaterRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTheaterRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sRate nvarchar(10)
-- Gets: @sComments nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierTheaterRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sDescription nvarchar(MAX),
	@sRate nvarchar(10),
	@sComments nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTheaterRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[Description] = @sDescription,
	[Rate] = @sRate,
	[Comments] = @sComments,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTheaterRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTheaterRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTheaterRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTipsDetail''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTipsDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTipsDetail''
-- based on a foreign key field.
-- Gets: @iSupplierTypeId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_DeleteAllWSupplierTypeIdLogic]
	@iSupplierTypeId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTipsDetail]
WHERE
	[SupplierTypeId] = @iSupplierTypeId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTipsDetail''
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @dcTips decimal(18, 2)
-- Gets: @sCountry nvarchar(50)
-- Gets: @iCountryId int
-- Gets: @dcAirportPickUp decimal(18, 2)
-- Gets: @dcAirportDropOff decimal(18, 2)
-- Gets: @dcWholeDay decimal(18, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_Insert]
	@iCompanyID int,
	@iSupplierTypeId int,
	@dcTips decimal(18, 2),
	@sCountry nvarchar(50),
	@iCountryId int,
	@dcAirportPickUp decimal(18, 2),
	@dcAirportDropOff decimal(18, 2),
	@dcWholeDay decimal(18, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTipsDetail]
(
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierTypeId,
	@dcTips,
	@sCountry,
	@iCountryId,
	@dcAirportPickUp,
	@dcAirportDropOff,
	@dcWholeDay,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTipsDetail''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTipsDetail]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTipsDetail''
-- based on a foreign key field.
-- Gets: @iSupplierTypeId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectAllWSupplierTypeIdLogic]
	@iSupplierTypeId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTipsDetail]
WHERE
	[SupplierTypeId] = @iSupplierTypeId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTipsDetail''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[Tips],
	[Country],
	[CountryId],
	[AirportPickUp],
	[AirportDropOff],
	[WholeDay],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTipsDetail]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTipsDetail''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @dcTips decimal(18, 2)
-- Gets: @sCountry nvarchar(50)
-- Gets: @iCountryId int
-- Gets: @dcAirportPickUp decimal(18, 2)
-- Gets: @dcAirportDropOff decimal(18, 2)
-- Gets: @dcWholeDay decimal(18, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierTipsDetail_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierTypeId int,
	@dcTips decimal(18, 2),
	@sCountry nvarchar(50),
	@iCountryId int,
	@dcAirportPickUp decimal(18, 2),
	@dcAirportDropOff decimal(18, 2),
	@dcWholeDay decimal(18, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTipsDetail]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierTypeId] = @iSupplierTypeId,
	[Tips] = @dcTips,
	[Country] = @sCountry,
	[CountryId] = @iCountryId,
	[AirportPickUp] = @dcAirportPickUp,
	[AirportDropOff] = @dcAirportDropOff,
	[WholeDay] = @dcWholeDay,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTipsDetail''.
-- Will reset field [SupplierTypeId] with value @iSupplierTypeIdOld  to value @iSupplierTypeId
-- Gets: @iSupplierTypeId int
-- Gets: @iSupplierTypeIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTipsDetail_UpdateAllWSupplierTypeIdLogic]
	@iSupplierTypeId int,
	@iSupplierTypeIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTipsDetail]
SET
	[SupplierTypeId] = @iSupplierTypeId
WHERE
	[SupplierTypeId] = @iSupplierTypeIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTourGuide''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTourGuide''
-- based on a foreign key field.
-- Gets: @iTypeOfSupplier int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTourGuide]
WHERE
	[TypeOfSupplier] = @iTypeOfSupplier
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTourGuide''
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @iTypeOfSupplier int
-- Gets: @sRecruitedBy nvarchar(250)
-- Gets: @sSurName nvarchar(250)
-- Gets: @sSurNameInChinese nvarchar(250)
-- Gets: @sFirstName nvarchar(250)
-- Gets: @sFirstNameInChinese nvarchar(250)
-- Gets: @daDateOfBirth datetime
-- Gets: @sOrigin nvarchar(50)
-- Gets: @sNationality nvarchar(50)
-- Gets: @sMarried nvarchar(50)
-- Gets: @iChildren int
-- Gets: @sVisaStatus nvarchar(250)
-- Gets: @sHomePhone nvarchar(15)
-- Gets: @sMobilePhone nvarchar(15)
-- Gets: @sWeChat nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(500)
-- Gets: @sAddress2 nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(10)
-- Gets: @sSpeakingLanguage nvarchar(1000)
-- Gets: @iNoOfYearTourGuide int
-- Gets: @sAreaOfWorking nvarchar(250)
-- Gets: @sTypeOfWork nvarchar(250)
-- Gets: @sVehicleOwned nvarchar(250)
-- Gets: @sKindOfGroupWork nvarchar(Max)
-- Gets: @sKindOfGroupNotWork nvarchar(150)
-- Gets: @sSpecialRequirement nvarchar(Max)
-- Gets: @iRating int
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biDrivingLicence varbinary(Max)
-- Gets: @biVehiclePhoto varbinary(Max)
-- Gets: @biPassportCopy varbinary(Max)
-- Gets: @biAddressProof varbinary(Max)
-- Gets: @biGuideLicence varbinary(Max)
-- Gets: @biCVOfGuide varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_Insert]
	@iCompanyID int,
	@iSupplierTypeId int,
	@iTypeOfSupplier int,
	@sRecruitedBy nvarchar(250),
	@sSurName nvarchar(250),
	@sSurNameInChinese nvarchar(250),
	@sFirstName nvarchar(250),
	@sFirstNameInChinese nvarchar(250),
	@daDateOfBirth datetime,
	@sOrigin nvarchar(50),
	@sNationality nvarchar(50),
	@sMarried nvarchar(50),
	@iChildren int,
	@sVisaStatus nvarchar(250),
	@sHomePhone nvarchar(15),
	@sMobilePhone nvarchar(15),
	@sWeChat nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(500),
	@sAddress2 nvarchar(500),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(10),
	@sSpeakingLanguage nvarchar(1000),
	@iNoOfYearTourGuide int,
	@sAreaOfWorking nvarchar(250),
	@sTypeOfWork nvarchar(250),
	@sVehicleOwned nvarchar(250),
	@sKindOfGroupWork nvarchar(Max),
	@sKindOfGroupNotWork nvarchar(150),
	@sSpecialRequirement nvarchar(Max),
	@iRating int,
	@sBlackList nvarchar(50),
	@sComments nvarchar(Max),
	@biDrivingLicence varbinary(Max),
	@biVehiclePhoto varbinary(Max),
	@biPassportCopy varbinary(Max),
	@biAddressProof varbinary(Max),
	@biGuideLicence varbinary(Max),
	@biCVOfGuide varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTourGuide]
(
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@iSupplierTypeId,
	@iTypeOfSupplier,
	@sRecruitedBy,
	@sSurName,
	@sSurNameInChinese,
	@sFirstName,
	@sFirstNameInChinese,
	@daDateOfBirth,
	@sOrigin,
	@sNationality,
	@sMarried,
	@iChildren,
	@sVisaStatus,
	@sHomePhone,
	@sMobilePhone,
	@sWeChat,
	@sEmail,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sCountry,
	@sPostCode,
	@sSpeakingLanguage,
	@iNoOfYearTourGuide,
	@sAreaOfWorking,
	@sTypeOfWork,
	@sVehicleOwned,
	@sKindOfGroupWork,
	@sKindOfGroupNotWork,
	@sSpecialRequirement,
	@iRating,
	@sBlackList,
	@sComments,
	@biDrivingLicence,
	@biVehiclePhoto,
	@biPassportCopy,
	@biAddressProof,
	@biGuideLicence,
	@biCVOfGuide,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTourGuide''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTourGuide''
-- based on a foreign key field.
-- Gets: @iTypeOfSupplier int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
WHERE
	[TypeOfSupplier] = @iTypeOfSupplier
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTourGuide''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[SupplierTypeId],
	[TypeOfSupplier],
	[RecruitedBy],
	[SurName],
	[SurNameInChinese],
	[FirstName],
	[FirstNameInChinese],
	[DateOfBirth],
	[Origin],
	[Nationality],
	[Married],
	[Children],
	[VisaStatus],
	[HomePhone],
	[MobilePhone],
	[WeChat],
	[Email],
	[Address1],
	[Address2],
	[City],
	[Country],
	[PostCode],
	[SpeakingLanguage],
	[NoOfYearTourGuide],
	[AreaOfWorking],
	[TypeOfWork],
	[VehicleOwned],
	[KindOfGroupWork],
	[KindOfGroupNotWork],
	[SpecialRequirement],
	[Rating],
	[BlackList],
	[Comments],
	[DrivingLicence],
	[VehiclePhoto],
	[PassportCopy],
	[AddressProof],
	[GuideLicence],
	[CVOfGuide],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuide]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTourGuide''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @iSupplierTypeId int
-- Gets: @iTypeOfSupplier int
-- Gets: @sRecruitedBy nvarchar(250)
-- Gets: @sSurName nvarchar(250)
-- Gets: @sSurNameInChinese nvarchar(250)
-- Gets: @sFirstName nvarchar(250)
-- Gets: @sFirstNameInChinese nvarchar(250)
-- Gets: @daDateOfBirth datetime
-- Gets: @sOrigin nvarchar(50)
-- Gets: @sNationality nvarchar(50)
-- Gets: @sMarried nvarchar(50)
-- Gets: @iChildren int
-- Gets: @sVisaStatus nvarchar(250)
-- Gets: @sHomePhone nvarchar(15)
-- Gets: @sMobilePhone nvarchar(15)
-- Gets: @sWeChat nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(500)
-- Gets: @sAddress2 nvarchar(500)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(10)
-- Gets: @sSpeakingLanguage nvarchar(1000)
-- Gets: @iNoOfYearTourGuide int
-- Gets: @sAreaOfWorking nvarchar(250)
-- Gets: @sTypeOfWork nvarchar(250)
-- Gets: @sVehicleOwned nvarchar(250)
-- Gets: @sKindOfGroupWork nvarchar(Max)
-- Gets: @sKindOfGroupNotWork nvarchar(150)
-- Gets: @sSpecialRequirement nvarchar(Max)
-- Gets: @iRating int
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sComments nvarchar(Max)
-- Gets: @biDrivingLicence varbinary(Max)
-- Gets: @biVehiclePhoto varbinary(Max)
-- Gets: @biPassportCopy varbinary(Max)
-- Gets: @biAddressProof varbinary(Max)
-- Gets: @biGuideLicence varbinary(Max)
-- Gets: @biCVOfGuide varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierTourGuide_Update]
	@iID int,
	@iCompanyID int,
	@iSupplierTypeId int,
	@iTypeOfSupplier int,
	@sRecruitedBy nvarchar(250),
	@sSurName nvarchar(250),
	@sSurNameInChinese nvarchar(250),
	@sFirstName nvarchar(250),
	@sFirstNameInChinese nvarchar(250),
	@daDateOfBirth datetime,
	@sOrigin nvarchar(50),
	@sNationality nvarchar(50),
	@sMarried nvarchar(50),
	@iChildren int,
	@sVisaStatus nvarchar(250),
	@sHomePhone nvarchar(15),
	@sMobilePhone nvarchar(15),
	@sWeChat nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(500),
	@sAddress2 nvarchar(500),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(10),
	@sSpeakingLanguage nvarchar(1000),
	@iNoOfYearTourGuide int,
	@sAreaOfWorking nvarchar(250),
	@sTypeOfWork nvarchar(250),
	@sVehicleOwned nvarchar(250),
	@sKindOfGroupWork nvarchar(Max),
	@sKindOfGroupNotWork nvarchar(150),
	@sSpecialRequirement nvarchar(Max),
	@iRating int,
	@sBlackList nvarchar(50),
	@sComments nvarchar(Max),
	@biDrivingLicence varbinary(Max),
	@biVehiclePhoto varbinary(Max),
	@biPassportCopy varbinary(Max),
	@biAddressProof varbinary(Max),
	@biGuideLicence varbinary(Max),
	@biCVOfGuide varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuide]
SET 
	[CompanyID] = @iCompanyID,
	[SupplierTypeId] = @iSupplierTypeId,
	[TypeOfSupplier] = @iTypeOfSupplier,
	[RecruitedBy] = @sRecruitedBy,
	[SurName] = @sSurName,
	[SurNameInChinese] = @sSurNameInChinese,
	[FirstName] = @sFirstName,
	[FirstNameInChinese] = @sFirstNameInChinese,
	[DateOfBirth] = @daDateOfBirth,
	[Origin] = @sOrigin,
	[Nationality] = @sNationality,
	[Married] = @sMarried,
	[Children] = @iChildren,
	[VisaStatus] = @sVisaStatus,
	[HomePhone] = @sHomePhone,
	[MobilePhone] = @sMobilePhone,
	[WeChat] = @sWeChat,
	[Email] = @sEmail,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[Country] = @sCountry,
	[PostCode] = @sPostCode,
	[SpeakingLanguage] = @sSpeakingLanguage,
	[NoOfYearTourGuide] = @iNoOfYearTourGuide,
	[AreaOfWorking] = @sAreaOfWorking,
	[TypeOfWork] = @sTypeOfWork,
	[VehicleOwned] = @sVehicleOwned,
	[KindOfGroupWork] = @sKindOfGroupWork,
	[KindOfGroupNotWork] = @sKindOfGroupNotWork,
	[SpecialRequirement] = @sSpecialRequirement,
	[Rating] = @iRating,
	[BlackList] = @sBlackList,
	[Comments] = @sComments,
	[DrivingLicence] = @biDrivingLicence,
	[VehiclePhoto] = @biVehiclePhoto,
	[PassportCopy] = @biPassportCopy,
	[AddressProof] = @biAddressProof,
	[GuideLicence] = @biGuideLicence,
	[CVOfGuide] = @biCVOfGuide,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTourGuide''.
-- Will reset field [TypeOfSupplier] with value @iTypeOfSupplierOld  to value @iTypeOfSupplier
-- Gets: @iTypeOfSupplier int
-- Gets: @iTypeOfSupplierOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]
	@iTypeOfSupplier int,
	@iTypeOfSupplierOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuide]
SET
	[TypeOfSupplier] = @iTypeOfSupplier
WHERE
	[TypeOfSupplier] = @iTypeOfSupplierOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTourGuideRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTourGuideRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTourGuideRate''
-- based on a foreign key field.
-- Gets: @iSupplierTourGuideID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTourGuideRate''
-- Gets: @iSupplierTourGuideID int
-- Gets: @sSeason nvarchar(150)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_coach_Guide decimal(10, 2)
-- Gets: @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_tips decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_Day_coach_Guide decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2)
-- Gets: @dcFull_day_tips decimal(10, 2)
-- Gets: @dcExtra_hour_With_Vehicle decimal(10, 2)
-- Gets: @dcExtra_hour_Without_vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_With_Vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_Without_Vehicle decimal(10, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_Insert]
	@iSupplierTourGuideID int,
	@sSeason nvarchar(150),
	@daFromDate datetime,
	@daToDate datetime,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_coach_Guide decimal(10, 2),
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_tips decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_Day_coach_Guide decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2),
	@dcFull_day_tips decimal(10, 2),
	@dcExtra_hour_With_Vehicle decimal(10, 2),
	@dcExtra_hour_Without_vehicle decimal(10, 2),
	@dcLondon_Edinburgh_With_Vehicle decimal(10, 2),
	@dcLondon_Edinburgh_Without_Vehicle decimal(10, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTourGuideRate]
(
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierTourGuideID,
	@sSeason,
	@daFromDate,
	@daToDate,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle,
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle,
	@dcHalf_day_coach_Guide,
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle,
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle,
	@dcHalf_day_tips,
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle,
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle,
	@dcFull_Day_coach_Guide,
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle,
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle,
	@dcFull_day_tips,
	@dcExtra_hour_With_Vehicle,
	@dcExtra_hour_Without_vehicle,
	@dcLondon_Edinburgh_With_Vehicle,
	@dcLondon_Edinburgh_Without_Vehicle,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTourGuideRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTourGuideRate''
-- based on a foreign key field.
-- Gets: @iSupplierTourGuideID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTourGuideRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierTourGuideID],
	[Season],
	[FromDate],
	[ToDate],
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle],
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle],
	[Half_day_coach_Guide],
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle],
	[Half_day_tips],
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle],
	[Full_Day_coach_Guide],
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle],
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle],
	[Full_day_tips],
	[Extra_hour_With_Vehicle],
	[Extra_hour_Without_vehicle],
	[London_Edinburgh_With_Vehicle],
	[London_Edinburgh_Without_Vehicle],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTourGuideRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTourGuideRate''
-- Gets: @iID int
-- Gets: @iSupplierTourGuideID int
-- Gets: @sSeason nvarchar(150)
-- Gets: @daFromDate datetime
-- Gets: @daToDate datetime
-- Gets: @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2)
-- Gets: @dcHalf_day_coach_Guide decimal(10, 2)
-- Gets: @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcHalf_day_tips decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2)
-- Gets: @dcFull_Day_coach_Guide decimal(10, 2)
-- Gets: @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2)
-- Gets: @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2)
-- Gets: @dcFull_day_tips decimal(10, 2)
-- Gets: @dcExtra_hour_With_Vehicle decimal(10, 2)
-- Gets: @dcExtra_hour_Without_vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_With_Vehicle decimal(10, 2)
-- Gets: @dcLondon_Edinburgh_Without_Vehicle decimal(10, 2)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierTourGuideRate_Update]
	@iID int,
	@iSupplierTourGuideID int,
	@sSeason nvarchar(150),
	@daFromDate datetime,
	@daToDate datetime,
	@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle decimal(10, 2),
	@dcHalf_day_coach_Guide decimal(10, 2),
	@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcHalf_day_tips decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle decimal(10, 2),
	@dcFull_Day_coach_Guide decimal(10, 2),
	@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle decimal(10, 2),
	@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle decimal(10, 2),
	@dcFull_day_tips decimal(10, 2),
	@dcExtra_hour_With_Vehicle decimal(10, 2),
	@dcExtra_hour_Without_vehicle decimal(10, 2),
	@dcLondon_Edinburgh_With_Vehicle decimal(10, 2),
	@dcLondon_Edinburgh_Without_Vehicle decimal(10, 2),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuideRate]
SET 
	[SupplierTourGuideID] = @iSupplierTourGuideID,
	[Season] = @sSeason,
	[FromDate] = @daFromDate,
	[ToDate] = @daToDate,
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle] = @dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle,
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle] = @dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle,
	[Half_day_coach_Guide] = @dcHalf_day_coach_Guide,
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle] = @dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle,
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle] = @dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle,
	[Half_day_tips] = @dcHalf_day_tips,
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle] = @dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle,
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle] = @dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle,
	[Full_Day_coach_Guide] = @dcFull_Day_coach_Guide,
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle] = @dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle,
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle] = @dcFull_day_12_17_Seater_Driver_Guide_WithVehicle,
	[Full_day_tips] = @dcFull_day_tips,
	[Extra_hour_With_Vehicle] = @dcExtra_hour_With_Vehicle,
	[Extra_hour_Without_vehicle] = @dcExtra_hour_Without_vehicle,
	[London_Edinburgh_With_Vehicle] = @dcLondon_Edinburgh_With_Vehicle,
	[London_Edinburgh_Without_Vehicle] = @dcLondon_Edinburgh_Without_Vehicle,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTourGuideRate''.
-- Will reset field [SupplierTourGuideID] with value @iSupplierTourGuideIDOld  to value @iSupplierTourGuideID
-- Gets: @iSupplierTourGuideID int
-- Gets: @iSupplierTourGuideIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]
	@iSupplierTourGuideID int,
	@iSupplierTourGuideIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTourGuideRate]
SET
	[SupplierTourGuideID] = @iSupplierTourGuideID
WHERE
	[SupplierTourGuideID] = @iSupplierTourGuideIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierTrainRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierTrainRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierTrainRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierTrainRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierTrainRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRouteFrom nvarchar(150)
-- Gets: @sRouteTo nvarchar(150)
-- Gets: @sRate nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRouteFrom nvarchar(150),
	@sRouteTo nvarchar(150),
	@sRate nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierTrainRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sRouteFrom,
	@sRouteTo,
	@sRate,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierTrainRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTrainRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierTrainRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTrainRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierTrainRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[RouteFrom],
	[RouteTo],
	[Rate],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierTrainRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierTrainRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sRouteFrom nvarchar(150)
-- Gets: @sRouteTo nvarchar(150)
-- Gets: @sRate nvarchar(10)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierTrainRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sRouteFrom nvarchar(150),
	@sRouteTo nvarchar(150),
	@sRate nvarchar(10),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTrainRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[RouteFrom] = @sRouteFrom,
	[RouteTo] = @sRouteTo,
	[Rate] = @sRate,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierTrainRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierTrainRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierTrainRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierType''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierType]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierType''
-- Gets: @sSupplierType varchar(250)
-- Gets: @bIsActive bit
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_Insert]
	@sSupplierType varchar(250),
	@bIsActive bit,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierType]
(
	[SupplierType],
	[IsActive]
)
VALUES
(
	@sSupplierType,
	ISNULL(@bIsActive, ((1)))
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierType''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierType],
	[IsActive]
FROM [dbo].[SupplierType]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierType''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierType_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierType],
	[IsActive]
FROM [dbo].[SupplierType]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierType_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierType_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierType''
-- Gets: @iID int
-- Gets: @sSupplierType varchar(250)
-- Gets: @bIsActive bit
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierType_Update]
	@iID int,
	@sSupplierType varchar(250),
	@bIsActive bit,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierType]
SET 
	[SupplierType] = @sSupplierType,
	[IsActive] = @bIsActive
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''SupplierVanHireRate''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[SupplierVanHireRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''SupplierVanHireRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_DeleteAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[SupplierVanHireRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''SupplierVanHireRate''
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sNoOfSeater nvarchar(10)
-- Gets: @sMakeAndModel nvarchar(50)
-- Gets: @sRatePerDay nvarchar(10)
-- Gets: @sAgeofVanInGeneral nvarchar(50)
-- Gets: @sIdealNoOfPassengers nvarchar(10)
-- Gets: @sMaxNoWithoutluggage nvarchar(10)
-- Gets: @sMaxNoWithluggage nvarchar(10)
-- Gets: @sInsurancePolicy nvarchar(50)
-- Gets: @sAccessFeeInsurance nvarchar(10)
-- Gets: @sDriverRequirement nvarchar(150)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_Insert]
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sNoOfSeater nvarchar(10),
	@sMakeAndModel nvarchar(50),
	@sRatePerDay nvarchar(10),
	@sAgeofVanInGeneral nvarchar(50),
	@sIdealNoOfPassengers nvarchar(10),
	@sMaxNoWithoutluggage nvarchar(10),
	@sMaxNoWithluggage nvarchar(10),
	@sInsurancePolicy nvarchar(50),
	@sAccessFeeInsurance nvarchar(10),
	@sDriverRequirement nvarchar(150),
	@biFileData varbinary(MAX),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[SupplierVanHireRate]
(
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iSupplierDetailID,
	@daSavedDate,
	@sAddedBy,
	@sNoOfSeater,
	@sMakeAndModel,
	@sRatePerDay,
	@sAgeofVanInGeneral,
	@sIdealNoOfPassengers,
	@sMaxNoWithoutluggage,
	@sMaxNoWithluggage,
	@sInsurancePolicy,
	@sAccessFeeInsurance,
	@sDriverRequirement,
	@biFileData,
	@sRemarks,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''SupplierVanHireRate''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierVanHireRate]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''SupplierVanHireRate''
-- based on a foreign key field.
-- Gets: @iSupplierDetailID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierVanHireRate]
WHERE
	[SupplierDetailID] = @iSupplierDetailID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''SupplierVanHireRate''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[SupplierDetailID],
	[SavedDate],
	[AddedBy],
	[NoOfSeater],
	[MakeAndModel],
	[RatePerDay],
	[AgeofVanInGeneral],
	[IdealNoOfPassengers],
	[MaxNoWithoutluggage],
	[MaxNoWithluggage],
	[InsurancePolicy],
	[AccessFeeInsurance],
	[DriverRequirement],
	[FileData],
	[Remarks],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[SupplierVanHireRate]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''SupplierVanHireRate''
-- Gets: @iID int
-- Gets: @iSupplierDetailID int
-- Gets: @daSavedDate datetime
-- Gets: @sAddedBy nvarchar(50)
-- Gets: @sNoOfSeater nvarchar(10)
-- Gets: @sMakeAndModel nvarchar(50)
-- Gets: @sRatePerDay nvarchar(10)
-- Gets: @sAgeofVanInGeneral nvarchar(50)
-- Gets: @sIdealNoOfPassengers nvarchar(10)
-- Gets: @sMaxNoWithoutluggage nvarchar(10)
-- Gets: @sMaxNoWithluggage nvarchar(10)
-- Gets: @sInsurancePolicy nvarchar(50)
-- Gets: @sAccessFeeInsurance nvarchar(10)
-- Gets: @sDriverRequirement nvarchar(150)
-- Gets: @biFileData varbinary(MAX)
-- Gets: @sRemarks nvarchar(MAX)
-- Gets: @sExtraField1 nvarchar(100)
-- Gets: @sExtraField2 nvarchar(100)
-- Gets: @sExtraField3 nvarchar(100)
-- Gets: @sExtraField4 nvarchar(100)
-- Gets: @sExtraField5 nvarchar(100)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_SupplierVanHireRate_Update]
	@iID int,
	@iSupplierDetailID int,
	@daSavedDate datetime,
	@sAddedBy nvarchar(50),
	@sNoOfSeater nvarchar(10),
	@sMakeAndModel nvarchar(50),
	@sRatePerDay nvarchar(10),
	@sAgeofVanInGeneral nvarchar(50),
	@sIdealNoOfPassengers nvarchar(10),
	@sMaxNoWithoutluggage nvarchar(10),
	@sMaxNoWithluggage nvarchar(10),
	@sInsurancePolicy nvarchar(50),
	@sAccessFeeInsurance nvarchar(10),
	@sDriverRequirement nvarchar(150),
	@biFileData varbinary(MAX),
	@sRemarks nvarchar(MAX),
	@sExtraField1 nvarchar(100),
	@sExtraField2 nvarchar(100),
	@sExtraField3 nvarchar(100),
	@sExtraField4 nvarchar(100),
	@sExtraField5 nvarchar(100),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierVanHireRate]
SET 
	[SupplierDetailID] = @iSupplierDetailID,
	[SavedDate] = @daSavedDate,
	[AddedBy] = @sAddedBy,
	[NoOfSeater] = @sNoOfSeater,
	[MakeAndModel] = @sMakeAndModel,
	[RatePerDay] = @sRatePerDay,
	[AgeofVanInGeneral] = @sAgeofVanInGeneral,
	[IdealNoOfPassengers] = @sIdealNoOfPassengers,
	[MaxNoWithoutluggage] = @sMaxNoWithoutluggage,
	[MaxNoWithluggage] = @sMaxNoWithluggage,
	[InsurancePolicy] = @sInsurancePolicy,
	[AccessFeeInsurance] = @sAccessFeeInsurance,
	[DriverRequirement] = @sDriverRequirement,
	[FileData] = @biFileData,
	[Remarks] = @sRemarks,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''SupplierVanHireRate''.
-- Will reset field [SupplierDetailID] with value @iSupplierDetailIDOld  to value @iSupplierDetailID
-- Gets: @iSupplierDetailID int
-- Gets: @iSupplierDetailIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_SupplierVanHireRate_UpdateAllWSupplierDetailIDLogic]
	@iSupplierDetailID int,
	@iSupplierDetailIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[SupplierVanHireRate]
SET
	[SupplierDetailID] = @iSupplierDetailID
WHERE
	[SupplierDetailID] = @iSupplierDetailIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_Delete]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''TaskList''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[TaskList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_Insert]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''TaskList''
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sAssignedTo nvarchar(150)
-- Gets: @sTaskStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daDueDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_Insert]
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sAssignedTo nvarchar(150),
	@sTaskStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daDueDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[TaskList]
(
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@sSubject,
	@sAssignedTo,
	@sTaskStatus,
	@sDescription,
	@daDueDate,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_SelectAll]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''TaskList''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[TaskList]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_SelectOne]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''TaskList''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_TaskList_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Subject],
	[AssignedTo],
	[TaskStatus],
	[Description],
	[DueDate],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[TaskList]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_TaskList_Update]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_TaskList_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''TaskList''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @sSubject nvarchar(250)
-- Gets: @sAssignedTo nvarchar(150)
-- Gets: @sTaskStatus nvarchar(50)
-- Gets: @sDescription nvarchar(500)
-- Gets: @daDueDate datetime
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE  [dbo].[sp_TaskList_Update]
	@iID int,
	@iCompanyID int,
	@sSubject nvarchar(250),
	@sAssignedTo nvarchar(150),
	@sTaskStatus nvarchar(50),
	@sDescription nvarchar(500),
	@daDueDate datetime,
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[TaskList]
SET 
	[CompanyID] = @iCompanyID,
	[Subject] = @sSubject,
	[AssignedTo] = @sAssignedTo, 
	[TaskStatus] = @sTaskStatus,
	 
	[Description] = @sDescription,
	[DueDate] = @daDueDate,
	--[CreatedBy]  = @sCreatedBy,
	--[CreatedDate]  = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  Table [dbo].[AppointmentList]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppointmentList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AppointmentList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Subject] [nvarchar](250) NULL,
	[Status] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[AppointmentDate] [datetime] NULL,
	[AppointmentTime] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_AppointmentList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContactManagement]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactManagement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContactManagement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[Status] [nvarchar](50) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ContactManagement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GeneralCategoryItems]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GeneralCategoryItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GeneralCategoryItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](150) NULL,
	[Title] [nvarchar](150) NULL,
	[Description] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_GeneralCategoryItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ItineraryPhotos]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ItineraryPhotos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ItineraryID] [int] NULL,
	[Photos] [varbinary](max) NULL,
	[DocType] [nvarchar](20) NULL,
	[Extension] [nvarchar](10) NULL,
	[FileName] [nvarchar](50) NULL,
	[RealFileName] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ItineraryPhotos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItineraryTemplate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItineraryTemplate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ItineraryTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[Staff] [nvarchar](250) NULL,
	[Country] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[TourType] [nvarchar](50) NULL,
	[NoOfDays] [int] NULL,
	[NoOfNights] [int] NULL,
	[SubjectTitle] [nvarchar](250) NULL,
	[ItineraryTemplate] [nvarchar](max) NULL,
	[ItineraryPhotoId] [int] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ItineraryTemplate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[LeadManagement]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LeadManagement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LeadManagement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContactType] [nvarchar](50) NULL,
	[CampaignID] [int] NULL,
	[TourType] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[Gender] [nvarchar](10) NOT NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[AlternateEmail] [nvarchar](150) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNumber] [nvarchar](50) NULL,
	[BusinessPhone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[PreferredContactTime] [nvarchar](50) NULL,
	[PreferredContactMethod] [nvarchar](50) NULL,
	[PlannedDepartureDate] [datetime] NULL,
	[HowManyDays] [int] NULL,
	[HowManyPeople] [int] NULL,
	[HowManyChild] [int] NULL,
	[PlaceOfDeparture] [nvarchar](50) NULL,
	[PlaceOfReturn] [nvarchar](50) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[Currency] [nvarchar](15) NULL,
	[TourGuide] [nvarchar](50) NULL,
	[FlightsNeeded] [bit] NULL,
	[Vehicle] [nvarchar](50) NULL,
	[HotelType] [nvarchar](50) NULL,
	[Meals] [nvarchar](50) NULL,
	[AdmissionTickets] [nvarchar](50) NULL,
	[Insurance] [nvarchar](50) NULL,
	[LeadSource] [nvarchar](50) NULL,
	[LeadOwner] [nvarchar](50) NULL,
	[LeadStatus] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NULL,
	[WebPage] [nvarchar](250) NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferredBy] [nvarchar](50) NULL,
	[HomeTown] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[ContactsInterests] [nvarchar](250) NULL,
	[Attachment] [varbinary](max) NULL,
	[BDM] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[Response] [nvarchar](500) NULL,
	[ClientType] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_LeadManagement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LetsGeneralType]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LetsGeneralType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LetsGeneralType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](50) NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_LetsGeneralType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MarketingCampaign]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingCampaign](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ProjectID] [int] NULL,
	[CampaignName] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CampaignDescription] [nvarchar](max) NULL,
	[EstimateBudget] [decimal](18, 2) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[Email] [nvarchar](50) NULL,
	[AlternateEmail] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MarketingChannel]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingChannel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingChannel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ContractedBy] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](100) NULL,
	[CompanyWebsite] [nvarchar](250) NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](250) NULL,
	[CompanyTelephone] [nvarchar](50) NULL,
	[CompanyEmail] [nvarchar](50) NULL,
	[CompanyAddress1] [nvarchar](100) NULL,
	[CompanyAddress2] [nvarchar](100) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostCode] [nvarchar](50) NULL,
	[AdvertisingCost] [nvarchar](50) NULL,
	[AmountOfFee] [decimal](18, 2) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[BlackList] [nvarchar](50) NULL,
	[Rating] [nvarchar](50) NULL,
	[MainBusiness] [nvarchar](max) NULL,
	[HowToUseMarketingChannel] [nvarchar](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[FirstDayOfCollabration] [nvarchar](250) NULL,
	[ContractExpireDate] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
	[SignedAgreedContractUpload] [varbinary](max) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[ExtraField1] [nvarchar](50) NULL,
	[ExtraField2] [nvarchar](50) NULL,
	[ExtraField3] [nvarchar](50) NULL,
	[ExtraField4] [nvarchar](50) NULL,
	[ExtraField5] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingChannel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MarketingContacts]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingContacts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingContacts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MarketingChannelID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingContacts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MarketingProject]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingProject]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingProject](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[ProjectName] [nvarchar](250) NULL,
	[ProjectDescribution] [nvarchar](max) NULL,
	[InitiateBy] [nvarchar](50) NULL,
	[StaffInvolved] [nvarchar](50) NULL,
	[ProjectStartingDate] [datetime] NULL,
	[ProjectPlannedEndingDate] [datetime] NULL,
	[WorkingProcessComment] [nvarchar](max) NULL,
	[ProjectActualEndDate] [datetime] NULL,
	[ProjectIncome] [decimal](18, 2) NULL,
	[ProjectExpense] [decimal](18, 2) NULL,
	[ProjectGrossProfit] [decimal](18, 2) NULL,
	[Rating] [nvarchar](50) NULL,
	[ProjectReport] [nvarchar](max) NULL,
	[TotalBudget] [decimal](18, 2) NULL,
	[TotalEstimateBudget] [decimal](18, 2) NULL,
	[ExtraField1] [nvarchar](50) NULL,
	[ExtraField2] [nvarchar](50) NULL,
	[ExtraField3] [nvarchar](50) NULL,
	[ExtraField4] [nvarchar](50) NULL,
	[ExtraField5] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingProject] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[OpinionSheet]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheet]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpinionSheet](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Date] [datetime] NULL,
	[Staff] [nvarchar](250) NULL,
	[OpinionSheetCategoryId] [int] NULL,
	[FileData] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OpinionSheet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpinionSheetCategory]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpinionSheetCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpinionSheetCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_OpinionSheetCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PriceTemplate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PriceTemplate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PriceTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Staff] [nvarchar](250) NULL,
	[Language] [nvarchar](50) NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_PriceTemplate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Product]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[CreationDate] [datetime] NULL,
	[ContractedBy] [nvarchar](250) NULL,
	[DataEntryBy] [nvarchar](250) NULL,
	[ProductType] [nvarchar](250) NULL,
	[SupplierId] [int] NULL,
	[SupplierName] [nvarchar](250) NULL,
	[Contract] [nvarchar](250) NULL,
	[ContractWith] [nvarchar](250) NULL,
	[EmergencyNo] [nvarchar](250) NULL,
	[ReservationEmail] [nvarchar](250) NULL,
	[AlternationEmail] [nvarchar](250) NULL,
	[Currency] [nvarchar](250) NULL,
	[ContractType] [nvarchar](250) NULL,
	[NetRate] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Country] [nvarchar](250) NULL,
	[Region] [nvarchar](250) NULL,
	[TypeOfProduct] [nvarchar](50) NULL,
	[NoOfDays] [int] NULL,
	[NoOfNights] [int] NULL,
	[ProductTitle] [nvarchar](250) NULL,
	[ItineraryTemplate] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NULL,
	[Comment] [nvarchar](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[HowToSell] [nvarchar](max) NULL,
	[PriceIncluding] [nvarchar](max) NULL,
	[PriceExcluding] [nvarchar](max) NULL,
	[ContactTitle] [nvarchar](50) NULL,
	[ContactSurName] [nvarchar](150) NULL,
	[ContactFirstName] [nvarchar](510) NULL,
	[ContactPosition] [nvarchar](50) NULL,
	[ContactDepartment] [nvarchar](150) NULL,
	[ContactEmail] [nvarchar](150) NULL,
	[ContactDirectPhone] [nvarchar](50) NULL,
	[ContactMobileNo] [nvarchar](50) NULL,
	[ContactFaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[ExtraField1] [nvarchar](250) NULL,
	[ExtraField2] [nvarchar](250) NULL,
	[ExtraField3] [nvarchar](250) NULL,
	[ExtraField4] [nvarchar](250) NULL,
	[ExtraField5] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductContact]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductContact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductlID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](510) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductContact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductDocument]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductDocument]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductDocument](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[FileName] [nvarchar](250) NULL,
	[FileData] [varbinary](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductDocument] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierContacts]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierContacts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierContacts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](510) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierContacts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierDMCRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierDMCRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[FileData] [varbinary](max) NULL,
	[NetRate] [decimal](18, 2) NULL,
	[GrossRate] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierDMCRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketFit]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketFit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierEntranceTicketFit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NoOfPeople] [int] NULL,
	[Adult] [decimal](18, 2) NULL,
 CONSTRAINT [PK_SupplierEntranceTicketFit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierEntranceTicketRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierEntranceTicketRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[MonthFrom] [datetime] NULL,
	[MonthTo] [datetime] NULL,
	[DailyOpeningTime] [datetime] NULL,
	[DailyCloseTime] [datetime] NULL,
	[LastAdmission] [nvarchar](250) NULL,
	[Status] [nvarchar](50) NULL,
	[Remark] [nvarchar](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierEntranceTicketRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierFerryRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierFerryRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[CountryFrom] [nvarchar](50) NULL,
	[PortFrom] [nvarchar](50) NULL,
	[CountryTo] [nvarchar](50) NULL,
	[PortTo] [nvarchar](50) NULL,
	[DepartTime] [datetime] NULL,
	[ArrivalTime] [datetime] NULL,
	[OneWayOrReturn] [bit] NULL,
	[Description] [nvarchar](max) NULL,
	[FitRate] [nvarchar](10) NULL,
	[GroupRate] [nvarchar](10) NULL,
	[Remark] [nvarchar](max) NULL,
	[Cabin] [int] NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierFerryRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierHotelRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierHotelRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[HalfTwin] [nvarchar](150) NULL,
	[Single] [nvarchar](50) NULL,
	[Twin_DoubleForSoleUse] [nvarchar](50) NULL,
	[Triple] [nvarchar](50) NULL,
	[FamilyRoom] [nvarchar](50) NULL,
	[Breakfast] [nvarchar](50) NULL,
	[DinnerAtHotel] [nvarchar](50) NULL,
	[RatePerPerson] [nvarchar](10) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierHotelRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierInformation]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInformation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierInformation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[Currency] [nvarchar](10) NULL,
	[NoofBedRooms] [int] NULL,
	[HotelFacility] [nvarchar](max) NULL,
	[Parking] [nvarchar](50) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierInformation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierInsuranceRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierInsuranceRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[RateDescription] [nvarchar](max) NULL,
	[Rate] [nvarchar](10) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierInsuranceRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierMaster]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierTypeID] [int] NULL,
	[Name] [nvarchar](250) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[Email] [nvarchar](50) NULL,
	[CreditToLetsTravel] [nvarchar](50) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[ContractEndingDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierMealSupplement]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierMealSupplement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierTypeID] [int] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[Purpose] [nvarchar](max) NULL,
	[MealSupplementType] [nvarchar](50) NULL,
	[UK] [decimal](18, 0) NULL,
	[Ireland] [decimal](18, 0) NULL,
	[Europe] [decimal](18, 0) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierMealSupplement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierRestaurantRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierRestaurantRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[TypeOfFood] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[Rate] [decimal](18, 2) NULL,
	[SampleOfMenu] [nvarchar](max) NULL,
	[SampleMenuFile] [varbinary](max) NULL,
	[FoodPictureFile] [varbinary](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierRestaurantRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SuppliersDetails]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SuppliersDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierMasterID] [int] NULL,
	[SupplierTypeID] [int] NULL,
	[SupplierDate] [datetime] NULL,
	[ContractedBy] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[StarRating] [nvarchar](10) NULL,
	[Website] [nvarchar](150) NULL,
	[Agency] [nvarchar](100) NULL,
	[ClientCode] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Telephone] [nvarchar](15) NULL,
	[FaxNumber] [nvarchar](15) NULL,
	[EmergencyNo] [nvarchar](15) NULL,
	[Email] [nvarchar](50) NULL,
	[AlternateEmail] [nvarchar](50) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](250) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[HotelChain] [nvarchar](50) NULL,
	[Franchised] [nvarchar](50) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[CreditToLetsTravel] [nvarchar](50) NULL,
	[BlackList] [nvarchar](200) NULL,
	[ContractStartDate] [datetime] NULL,
	[ContractEndingDate] [datetime] NULL,
	[ContractRenewalDate] [datetime] NULL,
	[Rating] [nvarchar](10) NULL,
	[MainBusiness] [nvarchar](200) NULL,
	[Comments] [nvarchar](max) NULL,
	[FileData] [varbinary](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NULL,
	[FirstName] [nvarchar](510) NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[ContactEmail] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SuppliersDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTheaterRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTheaterRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[Rate] [nvarchar](10) NULL,
	[Comments] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTheaterRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierTipsDetail]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTipsDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[SupplierTypeId] [int] NULL,
	[Tips] [decimal](18, 2) NULL,
	[Country] [nvarchar](50) NULL,
	[CountryId] [int] NULL,
	[AirportPickUp] [decimal](18, 2) NULL,
	[AirportDropOff] [decimal](18, 2) NULL,
	[WholeDay] [decimal](18, 2) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTipsDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierTourGuide]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTourGuide](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[SupplierTypeId] [int] NULL,
	[TypeOfSupplier] [int] NULL,
	[RecruitedBy] [nvarchar](250) NULL,
	[SurName] [nvarchar](250) NULL,
	[SurNameInChinese] [nvarchar](250) NULL,
	[FirstName] [nvarchar](250) NULL,
	[FirstNameInChinese] [nvarchar](250) NULL,
	[DateOfBirth] [datetime] NULL,
	[Origin] [nvarchar](50) NULL,
	[Nationality] [nvarchar](50) NULL,
	[Married] [nvarchar](50) NULL,
	[Children] [int] NULL,
	[VisaStatus] [nvarchar](250) NULL,
	[HomePhone] [nvarchar](15) NULL,
	[MobilePhone] [nvarchar](15) NULL,
	[WeChat] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Address1] [nvarchar](500) NULL,
	[Address2] [nvarchar](500) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostCode] [nvarchar](10) NULL,
	[SpeakingLanguage] [nvarchar](1000) NULL,
	[NoOfYearTourGuide] [int] NULL,
	[AreaOfWorking] [nvarchar](250) NULL,
	[TypeOfWork] [nvarchar](250) NULL,
	[VehicleOwned] [nvarchar](250) NULL,
	[KindOfGroupWork] [nvarchar](max) NULL,
	[KindOfGroupNotWork] [nvarchar](150) NULL,
	[SpecialRequirement] [nvarchar](max) NULL,
	[Rating] [int] NULL,
	[BlackList] [nvarchar](50) NULL,
	[Comments] [nvarchar](max) NULL,
	[DrivingLicence] [varbinary](max) NULL,
	[VehiclePhoto] [varbinary](max) NULL,
	[PassportCopy] [varbinary](max) NULL,
	[AddressProof] [varbinary](max) NULL,
	[GuideLicence] [varbinary](max) NULL,
	[CVOfGuide] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTourGuid] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SupplierTourGuideRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTourGuideRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierTourGuideID] [int] NULL,
	[Season] [nvarchar](150) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[Half_day_4_9_SeaterDriverGuideWithoutVehicle] [decimal](10, 2) NULL,
	[Half_day_12_17_Seater_Driver_GuideWithoutVehicle] [decimal](10, 2) NULL,
	[Half_day_coach_Guide] [decimal](10, 2) NULL,
	[Half_day_4_9_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Half_day_12_17_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Half_day_tips] [decimal](10, 2) NULL,
	[Full_day_4_9_Seater_Driver_Guide_Without_Vehicle] [decimal](10, 2) NULL,
	[Full_day_12_17_Seater_Driver_Guide_Without_Vehicle] [decimal](10, 2) NULL,
	[Full_Day_coach_Guide] [decimal](10, 2) NULL,
	[Full_day_4_9_Seater_Driver_Guide_With_Vehicle] [decimal](10, 2) NULL,
	[Full_day_12_17_Seater_Driver_Guide_WithVehicle] [decimal](10, 2) NULL,
	[Full_day_tips] [decimal](10, 2) NULL,
	[Extra_hour_With_Vehicle] [decimal](10, 2) NULL,
	[Extra_hour_Without_vehicle] [decimal](10, 2) NULL,
	[London_Edinburgh_With_Vehicle] [decimal](10, 2) NULL,
	[London_Edinburgh_Without_Vehicle] [decimal](10, 2) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTourGuideRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierTrainRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierTrainRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[RouteFrom] [nvarchar](150) NULL,
	[RouteTo] [nvarchar](150) NULL,
	[Rate] [nvarchar](10) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierTrainRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SupplierVanHireRate]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierVanHireRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierDetailID] [int] NULL,
	[SavedDate] [datetime] NULL,
	[AddedBy] [nvarchar](50) NULL,
	[NoOfSeater] [nvarchar](10) NULL,
	[MakeAndModel] [nvarchar](50) NULL,
	[RatePerDay] [nvarchar](10) NULL,
	[AgeofVanInGeneral] [nvarchar](50) NULL,
	[IdealNoOfPassengers] [nvarchar](10) NULL,
	[MaxNoWithoutluggage] [nvarchar](10) NULL,
	[MaxNoWithluggage] [nvarchar](10) NULL,
	[InsurancePolicy] [nvarchar](50) NULL,
	[AccessFeeInsurance] [nvarchar](10) NULL,
	[DriverRequirement] [nvarchar](150) NULL,
	[FileData] [varbinary](max) NULL,
	[Remarks] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_SupplierVanHireRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaskList]    Script Date: 19-05-2017 20:58:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TaskList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TaskList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NULL,
	[Subject] [nvarchar](250) NULL,
	[AssignedTo] [nvarchar](150) NULL,
	[TaskStatus] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[DueDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_TaskList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_LetsGeneralType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[LetsGeneralType] ADD  CONSTRAINT [DF_LetsGeneralType_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] ADD  CONSTRAINT [DF_MarketingChannel_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] ADD  CONSTRAINT [DF_MarketingChannel_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] ADD  CONSTRAINT [DF_MarketingContacts_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] ADD  CONSTRAINT [DF_MarketingContacts_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItineraryPhotos_ItineraryPhotos]') AND parent_object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]'))
ALTER TABLE [dbo].[ItineraryPhotos]  WITH CHECK ADD  CONSTRAINT [FK_ItineraryPhotos_ItineraryPhotos] FOREIGN KEY([ItineraryID])
REFERENCES [dbo].[ItineraryTemplate] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ItineraryPhotos_ItineraryPhotos]') AND parent_object_id = OBJECT_ID(N'[dbo].[ItineraryPhotos]'))
ALTER TABLE [dbo].[ItineraryPhotos] CHECK CONSTRAINT [FK_ItineraryPhotos_ItineraryPhotos]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Campaign_MarketingProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]'))
ALTER TABLE [dbo].[MarketingCampaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_MarketingProject] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[MarketingProject] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Campaign_MarketingProject]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingCampaign]'))
ALTER TABLE [dbo].[MarketingCampaign] CHECK CONSTRAINT [FK_Campaign_MarketingProject]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts]  WITH CHECK ADD  CONSTRAINT [FK_MarketingContacts_MarketingChannel] FOREIGN KEY([MarketingChannelID])
REFERENCES [dbo].[MarketingChannel] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts] CHECK CONSTRAINT [FK_MarketingContacts_MarketingChannel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet]  WITH CHECK ADD  CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory] FOREIGN KEY([OpinionSheetCategoryId])
REFERENCES [dbo].[OpinionSheetCategory] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpinionSheet_OpinionSheetCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpinionSheet]'))
ALTER TABLE [dbo].[OpinionSheet] CHECK CONSTRAINT [FK_OpinionSheet_OpinionSheetCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductContact_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductContact]'))
ALTER TABLE [dbo].[ProductContact]  WITH CHECK ADD  CONSTRAINT [FK_ProductContact_Product] FOREIGN KEY([ProductlID])
REFERENCES [dbo].[Product] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductContact_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductContact]'))
ALTER TABLE [dbo].[ProductContact] CHECK CONSTRAINT [FK_ProductContact_Product]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDocument_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDocument]'))
ALTER TABLE [dbo].[ProductDocument]  WITH CHECK ADD  CONSTRAINT [FK_ProductDocument_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProductDocument_Product]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProductDocument]'))
ALTER TABLE [dbo].[ProductDocument] CHECK CONSTRAINT [FK_ProductDocument_Product]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts]  WITH CHECK ADD  CONSTRAINT [FK_SupplierContacts_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierContacts_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierContacts]'))
ALTER TABLE [dbo].[SupplierContacts] CHECK CONSTRAINT [FK_SupplierContacts_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierDMCRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]'))
ALTER TABLE [dbo].[SupplierDMCRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierDMCRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierDMCRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierDMCRate]'))
ALTER TABLE [dbo].[SupplierDMCRate] CHECK CONSTRAINT [FK_SupplierDMCRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierEntranceTicketRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]'))
ALTER TABLE [dbo].[SupplierEntranceTicketRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierEntranceTicketRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierEntranceTicketRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierEntranceTicketRate]'))
ALTER TABLE [dbo].[SupplierEntranceTicketRate] CHECK CONSTRAINT [FK_SupplierEntranceTicketRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierFerryRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierFerryRate]'))
ALTER TABLE [dbo].[SupplierFerryRate] CHECK CONSTRAINT [FK_SupplierFerryRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierHotelRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierHotelRate]'))
ALTER TABLE [dbo].[SupplierHotelRate] CHECK CONSTRAINT [FK_SupplierHotelRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation]  WITH CHECK ADD  CONSTRAINT [FK_SupplierInformation_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInformation_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInformation]'))
ALTER TABLE [dbo].[SupplierInformation] CHECK CONSTRAINT [FK_SupplierInformation_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierInsuranceRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierInsuranceRate]'))
ALTER TABLE [dbo].[SupplierInsuranceRate] CHECK CONSTRAINT [FK_SupplierInsuranceRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster]  WITH CHECK ADD  CONSTRAINT [FK_SupplierMaster_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMaster_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMaster]'))
ALTER TABLE [dbo].[SupplierMaster] CHECK CONSTRAINT [FK_SupplierMaster_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement]  WITH CHECK ADD  CONSTRAINT [FK_SupplierMealSupplement_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierMealSupplement_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierMealSupplement]'))
ALTER TABLE [dbo].[SupplierMealSupplement] CHECK CONSTRAINT [FK_SupplierMealSupplement_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierRestaurantRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]'))
ALTER TABLE [dbo].[SupplierRestaurantRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierRestaurantRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierRestaurantRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierRestaurantRate]'))
ALTER TABLE [dbo].[SupplierRestaurantRate] CHECK CONSTRAINT [FK_SupplierRestaurantRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails]  WITH CHECK ADD  CONSTRAINT [FK_SuppliersDetails_SupplierMaster] FOREIGN KEY([SupplierMasterID])
REFERENCES [dbo].[SupplierMaster] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] CHECK CONSTRAINT [FK_SuppliersDetails_SupplierMaster]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails]  WITH CHECK ADD  CONSTRAINT [FK_SuppliersDetails_SupplierType] FOREIGN KEY([SupplierTypeID])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SuppliersDetails_SupplierType]') AND parent_object_id = OBJECT_ID(N'[dbo].[SuppliersDetails]'))
ALTER TABLE [dbo].[SuppliersDetails] CHECK CONSTRAINT [FK_SuppliersDetails_SupplierType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTheaterRate_SupplierTheaterRate]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTheaterRate]'))
ALTER TABLE [dbo].[SupplierTheaterRate] CHECK CONSTRAINT [FK_SupplierTheaterRate_SupplierTheaterRate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTipsDetail_SupplierTipsDetail]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]'))
ALTER TABLE [dbo].[SupplierTipsDetail]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTipsDetail_SupplierTipsDetail] FOREIGN KEY([SupplierTypeId])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTipsDetail_SupplierTipsDetail]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTipsDetail]'))
ALTER TABLE [dbo].[SupplierTipsDetail] CHECK CONSTRAINT [FK_SupplierTipsDetail_SupplierTipsDetail]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid] FOREIGN KEY([TypeOfSupplier])
REFERENCES [dbo].[SupplierType] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuid_SupplierTourGuid]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuide]'))
ALTER TABLE [dbo].[SupplierTourGuide] CHECK CONSTRAINT [FK_SupplierTourGuid_SupplierTourGuid]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide] FOREIGN KEY([SupplierTourGuideID])
REFERENCES [dbo].[SupplierTourGuide] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTourGuideRate_SupplierTourGuide]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTourGuideRate]'))
ALTER TABLE [dbo].[SupplierTourGuideRate] CHECK CONSTRAINT [FK_SupplierTourGuideRate_SupplierTourGuide]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierTrainRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierTrainRate]'))
ALTER TABLE [dbo].[SupplierTrainRate] CHECK CONSTRAINT [FK_SupplierTrainRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate]  WITH CHECK ADD  CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails] FOREIGN KEY([SupplierDetailID])
REFERENCES [dbo].[SuppliersDetails] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SupplierVanHireRate_SuppliersDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[SupplierVanHireRate]'))
ALTER TABLE [dbo].[SupplierVanHireRate] CHECK CONSTRAINT [FK_SupplierVanHireRate_SuppliersDetails]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ItineraryTemplate', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItineraryTemplate', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierDMCRate', N'COLUMN',N'FileData'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Itinerary' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierDMCRate', @level2type=N'COLUMN',@level2name=N'FileData'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierEntranceTicketRate', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Added By' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierEntranceTicketRate', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'FitRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FIT Rate (less than 10 people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'FitRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'GroupRate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group Rate (more than 10 people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'GroupRate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierFerryRate', N'COLUMN',N'Cabin'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cabin ( 4people)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierFerryRate', @level2type=N'COLUMN',@level2name=N'Cabin'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'HalfTwin'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half Twin(2 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'HalfTwin'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Single'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Single ( 1 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Single'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Twin_DoubleForSoleUse'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Twin/Double for sole use ( 1 Person)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Twin_DoubleForSoleUse'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Triple'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Triple ( 2 Adults + 1 Child)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Triple'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'FamilyRoom'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Triple ( 2 Adults + 1 Child)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'FamilyRoom'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierHotelRate', N'COLUMN',N'Breakfast'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Family Room ( Adults + 1 Child) ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierHotelRate', @level2type=N'COLUMN',@level2name=N'Breakfast'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierMaster', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierMaster', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'TypeOfFood'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Food' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'TypeOfFood'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'Rate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rate /person' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'Rate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierRestaurantRate', N'COLUMN',N'FoodPictureFile'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Upload picuture of Food' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierRestaurantRate', @level2type=N'COLUMN',@level2name=N'FoodPictureFile'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SuppliersDetails', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SuppliersDetails', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CompanyID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTipsDetail', N'COLUMN',N'CreatedBy'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Added By' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTipsDetail', @level2type=N'COLUMN',@level2name=N'CreatedBy'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpeakingLanguage'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Speaking Languages' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpeakingLanguage'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'NoOfYearTourGuide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'No of years as tour guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'NoOfYearTourGuide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AreaOfWorking'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Area of Working' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AreaOfWorking'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'TypeOfWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Work' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'TypeOfWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What kind of groups that you have been working for?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'KindOfGroupNotWork'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'What kind of groups that you do not wish to work for?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'KindOfGroupNotWork'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'SpecialRequirement'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Special requirement from tour guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'SpecialRequirement'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'Comments'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comments from company staff' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'Comments'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'DrivingLicence'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Driving Licence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'DrivingLicence'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'VehiclePhoto'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vehicle Photo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'VehiclePhoto'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'PassportCopy'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Passport Copy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'PassportCopy'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'AddressProof'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Address Proof' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'AddressProof'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'GuideLicence'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Guide Licence' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'GuideLicence'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuide', N'COLUMN',N'CVOfGuide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CV of Guide' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuide', @level2type=N'COLUMN',@level2name=N'CVOfGuide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Season'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Winter season | Summer Season | Summer High season
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Season'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'FromDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'From Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'FromDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'ToDate'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'To Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'ToDate'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 4-9 seater driver guide without vehicle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_SeaterDriverGuideWithoutVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 12 - 17 seater driver guide without vehicle' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_GuideWithoutVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_coach_Guide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day coach guide
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_coach_Guide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 4-9 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_4_9_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day 12 - 17 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_12_17_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Half_day_tips'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Half day tips
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Half_day_tips'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 4-9 seater driver guide without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_Without_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 12- 17 seater driver guide without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_Without_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_Day_coach_Guide'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full Day coach guide
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_Day_coach_Guide'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 4-9 seater driver guide with vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_4_9_Seater_Driver_Guide_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day 12- 17 seater driver guide withvehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_12_17_Seater_Driver_Guide_WithVehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Full_day_tips'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full day tips
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Full_day_tips'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extra hour with Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'Extra_hour_Without_vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Extra hour without vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'Extra_hour_Without_vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_With_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'London - Edinburgh with Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_With_Vehicle'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SupplierTourGuideRate', N'COLUMN',N'London_Edinburgh_Without_Vehicle'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'London - Edinburgh Without Vehicle
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SupplierTourGuideRate', @level2type=N'COLUMN',@level2name=N'London_Edinburgh_Without_Vehicle'
GO
 -- Supplier Type
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SupplierType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SupplierType] DROP CONSTRAINT [DF_SupplierType_IsActive]
END

GO
/****** Object:  Table [dbo].[SupplierType]    Script Date: 19-05-2017 17:38:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierType]') AND type in (N'U'))
DROP TABLE [dbo].[SupplierType]
GO
/****** Object:  Table [dbo].[SupplierType]    Script Date: 19-05-2017 17:38:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SupplierType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SupplierType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierType] [varchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_SupplierType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[SupplierType] ON 

GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (1, N'Hotel', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (2, N'Hotel Wholesaler', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (3, N'Van Hire', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (4, N'Coach Hire', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (5, N'Tour Guide', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (6, N'Entrance', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (7, N'Restaurant', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (8, N'Theatre ', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (9, N'Ferry', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (10, N'Train', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (11, N'Flight', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (12, N'DMC', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (13, N'Insurance', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (14, N'Tip', 1)
GO
INSERT [dbo].[SupplierType] ([ID], [SupplierType], [IsActive]) VALUES (15, N'Meal Supplements for driver and guide', 1)
GO
SET IDENTITY_INSERT [dbo].[SupplierType] OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_SupplierType_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SupplierType] ADD  CONSTRAINT [DF_SupplierType_IsActive]  DEFAULT ((1)) FOR [IsActive]
END

GO
