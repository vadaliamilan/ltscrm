﻿ /****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Update]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectOne]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectAll]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Insert]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Delete]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheetCategory_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Update]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectOne]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAll]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Insert]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Delete]    Script Date: 13-05-2017 11:03:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_OpinionSheet_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Delete]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''OpinionSheet''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OpinionSheet]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''OpinionSheet''
-- based on a foreign key field.
-- Gets: @iOpinionSheetCategoryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_DeleteAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[OpinionSheet]
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Insert]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''OpinionSheet''
-- Gets: @iCompanyID int
-- Gets: @daDate datetime
-- Gets: @sStaff nvarchar(250)
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Insert]
	@iCompanyID int,
	@daDate datetime,
	@sStaff nvarchar(250),
	@iOpinionSheetCategoryId int,
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OpinionSheet]
(
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@daDate,
	@sStaff,
	@iOpinionSheetCategoryId,
	@biFileData,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAll]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''OpinionSheet''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectAll]
 @iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
 [dbo].[OpinionSheet].[ID],
 [dbo].[OpinionSheet].[CompanyID],
 [dbo].[OpinionSheet].[Date],
 [dbo].[OpinionSheet].[Staff],
 [dbo].[OpinionSheet].[OpinionSheetCategoryId],
 [dbo].[OpinionSheetCategory].[Category],
 [dbo].[OpinionSheet].[FileData],
 [dbo].[OpinionSheet].[CreatedBy],
 [dbo].[OpinionSheet].[CreatedDate],
 [dbo].[OpinionSheet].[UpdatedBy],
 [dbo].[OpinionSheet].[UpdatedDate]
FROM [dbo].[OpinionSheet]
INNER JOIN [dbo].[OpinionSheetCategory] ON [dbo].[OpinionSheet].OpinionSheetCategoryId =[dbo].[OpinionSheetCategory].ID
ORDER BY 
 [ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''OpinionSheet''
-- based on a foreign key field.
-- Gets: @iOpinionSheetCategoryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheet]
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_SelectOne]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''OpinionSheet''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[CompanyID],
	[Date],
	[Staff],
	[OpinionSheetCategoryId],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheet]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_Update]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''OpinionSheet''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @daDate datetime
-- Gets: @sStaff nvarchar(250)
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_Update]
	@iID int,
	@iCompanyID int,
	@daDate datetime,
	@sStaff nvarchar(250),
	@iOpinionSheetCategoryId int,
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheet]
SET 
	[CompanyID] = @iCompanyID,
	[Date] = @daDate,
	[Staff] = @sStaff,
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId,
	[FileData] = @biFileData,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''OpinionSheet''.
-- Will reset field [OpinionSheetCategoryId] with value @iOpinionSheetCategoryIdOld  to value @iOpinionSheetCategoryId
-- Gets: @iOpinionSheetCategoryId int
-- Gets: @iOpinionSheetCategoryIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheet_UpdateAllWOpinionSheetCategoryIdLogic]
	@iOpinionSheetCategoryId int,
	@iOpinionSheetCategoryIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheet]
SET
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryId
WHERE
	[OpinionSheetCategoryId] = @iOpinionSheetCategoryIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Delete]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''OpinionSheetCategory''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[OpinionSheetCategory]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Insert]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''OpinionSheetCategory''
-- Gets: @sCategory nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_Insert]
	@sCategory nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[OpinionSheetCategory]
(
	[Category],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@sCategory,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectAll]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''OpinionSheetCategory''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[Category],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheetCategory]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_SelectOne]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''OpinionSheetCategory''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[Category],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[OpinionSheetCategory]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_OpinionSheetCategory_Update]    Script Date: 13-05-2017 11:03:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_OpinionSheetCategory_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''OpinionSheetCategory''
-- Gets: @iID int
-- Gets: @sCategory nvarchar(250)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_OpinionSheetCategory_Update]
	@iID int,
	@sCategory nvarchar(250),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[OpinionSheetCategory]
SET 
	[Category] = @sCategory,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
