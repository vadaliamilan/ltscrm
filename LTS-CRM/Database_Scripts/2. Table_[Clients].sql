
EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Clients', @level2type=N'COLUMN',@level2name=N'CompanyID'

GO

/****** Object:  Table [dbo].[Clients]    Script Date: 05-04-2017 18:20:39 ******/
DROP TABLE [dbo].[Clients]
GO

/****** Object:  Table [dbo].[Clients]    Script Date: 05-04-2017 18:20:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Clients](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyID] [int] NOT NULL,
	[ClientDate] [datetime] NULL,
	[SalesStaff] [nvarchar](50) NULL,
	[CompanyOrInd] [nvarchar](50) NOT NULL,
	[DirectOrAgenet] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](50) NULL,
	[Website] [nvarchar](150) NULL,
	[Telephone] [nvarchar](15) NULL,
	[Fax] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](6) NULL,
	[PaymentTerms] [nvarchar](100) NULL,
	[BlackList] [nvarchar](200) NULL,
	[Rating] [float] NULL,
	[Comment] [nvarchar](150) NULL,
	[FileData] [varbinary](max) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Company or Office ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Clients', @level2type=N'COLUMN',@level2name=N'CompanyID'
GO

