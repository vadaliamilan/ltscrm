
/****** Object:  Table [dbo].[Product]    Script Date: 04/29/2017 13:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreationDate] [datetime] NULL,
	[ContractedBy] [nvarchar](250) NULL,
	[DataEntryBy] [nvarchar](250) NULL,
	[ProductType] [nvarchar](250) NULL,
	[SupplierId] [int] NULL,
	[SupplierName] [nvarchar](250) NULL,
	[Contract] [nvarchar](250) NULL,
	[ContractWith] [nvarchar](250) NULL,
	[EmergencyNo] [nvarchar](250) NULL,
	[ReservationEmail] [nvarchar](250) NULL,
	[AlternationEmail] [nvarchar](250) NULL,
	[Currency] [nvarchar](250) NULL,
	[ContractType] [nvarchar](250) NULL,
	[NetRate] [decimal](18, 2) NULL,
	[Commission] [decimal](18, 2) NULL,
	[Country] [nvarchar](250) NULL,
	[Region] [nvarchar](250) NULL,
	[TypeOfProduct] [nvarchar](50) NULL,
	[NoOfDays] [int] NULL,
	[NoOfNights] [int] NULL,
	[Title] [nvarchar](250) NULL,
	[Price] [decimal](18, 2) NULL,
	[Comment] [nvarchar](max) NULL,
	[HowToBook] [nvarchar](max) NULL,
	[PriceIncluding] [nvarchar](max) NULL,
	[PriceExcluding] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](250) NULL,
	[ExtraField2] [nvarchar](250) NULL,
	[ExtraField3] [nvarchar](250) NULL,
	[ExtraField4] [nvarchar](250) NULL,
	[ExtraField5] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductDocument]    Script Date: 04/29/2017 13:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductDocument](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[FileName] [nvarchar](250) NULL,
	[FileData] [varbinary](max) NULL,
	[ExtraField1] [nvarchar](100) NULL,
	[ExtraField2] [nvarchar](100) NULL,
	[ExtraField3] [nvarchar](100) NULL,
	[ExtraField4] [nvarchar](100) NULL,
	[ExtraField5] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductDocument] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductDeliveryDetail]    Script Date: 04/29/2017 13:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductDeliveryDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[Route] [nvarchar](250) NULL,
	[Itinerary] [nvarchar](max) NULL,
	[Hotel] [nvarchar](250) NULL,
	[BreakFast] [nvarchar](250) NULL,
	[Lunch] [nchar](10) NULL,
	[Dinner] [nchar](10) NULL,
	[Transportation] [nchar](10) NULL,
	[PhotoData] [varbinary](max) NULL,
	[ExtraField1] [nvarchar](250) NULL,
	[ExtraField2] [nvarchar](250) NULL,
	[ExtraField3] [nvarchar](250) NULL,
	[ExtraField4] [nvarchar](250) NULL,
	[ExtraField5] [nvarchar](250) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductDeliveryDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductContact]    Script Date: 04/29/2017 13:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductContact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProductlID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](510) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_ProductContact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_ProductContact_Product]    Script Date: 04/29/2017 13:39:34 ******/
ALTER TABLE [dbo].[ProductContact]  WITH CHECK ADD  CONSTRAINT [FK_ProductContact_Product] FOREIGN KEY([ProductlID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[ProductContact] CHECK CONSTRAINT [FK_ProductContact_Product]
GO
/****** Object:  ForeignKey [FK_ProductDeliveryDetail_Product]    Script Date: 04/29/2017 13:39:34 ******/
ALTER TABLE [dbo].[ProductDeliveryDetail]  WITH CHECK ADD  CONSTRAINT [FK_ProductDeliveryDetail_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[ProductDeliveryDetail] CHECK CONSTRAINT [FK_ProductDeliveryDetail_Product]
GO
/****** Object:  ForeignKey [FK_ProductDocument_Product]    Script Date: 04/29/2017 13:39:34 ******/
ALTER TABLE [dbo].[ProductDocument]  WITH CHECK ADD  CONSTRAINT [FK_ProductDocument_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[ProductDocument] CHECK CONSTRAINT [FK_ProductDocument_Product]
GO
