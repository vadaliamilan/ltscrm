
-- ========================================================================================================
-- [Stored Procedures generated for table:  Product_ItineraryTemplate]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplate_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplate_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'Product_ItineraryTemplate'
-- Gets: @sTitle nvarchar(500)
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sRoute nvarchar(150)
-- Gets: @sDay nvarchar(50)
-- Gets: @sAccommodation nvarchar(150)
-- Gets: @sMeals nvarchar(150)
-- Gets: @sTransport nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplate_Insert]
	@sTitle nvarchar(500),
	@sDescription nvarchar(MAX),
	@sRoute nvarchar(150),
	@sDay nvarchar(50),
	@sAccommodation nvarchar(150),
	@sMeals nvarchar(150),
	@sTransport nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iId int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[Product_ItineraryTemplate]
(
	[Title],
	[Description],
	[Route],
	[Day],
	[Accommodation],
	[Meals],
	[Transport],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@sTitle,
	@sDescription,
	@sRoute,
	@sDay,
	@sAccommodation,
	@sMeals,
	@sTransport,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iId=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplate_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplate_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'Product_ItineraryTemplate'
-- Gets: @iId int
-- Gets: @sTitle nvarchar(500)
-- Gets: @sDescription nvarchar(MAX)
-- Gets: @sRoute nvarchar(150)
-- Gets: @sDay nvarchar(50)
-- Gets: @sAccommodation nvarchar(150)
-- Gets: @sMeals nvarchar(150)
-- Gets: @sTransport nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplate_Update]
	@iId int,
	@sTitle nvarchar(500),
	@sDescription nvarchar(MAX),
	@sRoute nvarchar(150),
	@sDay nvarchar(50),
	@sAccommodation nvarchar(150),
	@sMeals nvarchar(150),
	@sTransport nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Product_ItineraryTemplate]
SET 
	[Title] = @sTitle,
	[Description] = @sDescription,
	[Route] = @sRoute,
	[Day] = @sDay,
	[Accommodation] = @sAccommodation,
	[Meals] = @sMeals,
	[Transport] = @sTransport,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplate_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplate_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'Product_ItineraryTemplate'
-- using the Primary Key. 
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplate_Delete]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[Product_ItineraryTemplate]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplate_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplate_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'Product_ItineraryTemplate'
-- based on the Primary Key.
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplate_SelectOne]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[Id],
	[Title],
	[Description],
	[Route],
	[Day],
	[Accommodation],
	[Meals],
	[Transport],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product_ItineraryTemplate]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplate_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplate_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'Product_ItineraryTemplate'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplate_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[Id],
	[Title],
	[Description],
	[Route],
	[Day],
	[Accommodation],
	[Meals],
	[Transport],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product_ItineraryTemplate]
ORDER BY 
	[Id] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     Product_ItineraryTemplate]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  Product_ItineraryTemplatePhotos]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'Product_ItineraryTemplatePhotos'
-- Gets: @iItineraryId int
-- Gets: @biPhotos varbinary(MAX)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_Insert]
	@iItineraryId int,
	@biPhotos varbinary(MAX),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iId int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[Product_ItineraryTemplatePhotos]
(
	[ItineraryId],
	[Photos],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iItineraryId,
	@biPhotos,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iId=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'Product_ItineraryTemplatePhotos'
-- Gets: @iId int
-- Gets: @iItineraryId int
-- Gets: @biPhotos varbinary(MAX)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_Update]
	@iId int,
	@iItineraryId int,
	@biPhotos varbinary(MAX),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Product_ItineraryTemplatePhotos]
SET 
	[ItineraryId] = @iItineraryId,
	[Photos] = @biPhotos,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [ItineraryId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_UpdateAllWItineraryIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_UpdateAllWItineraryIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'Product_ItineraryTemplatePhotos'.
-- Will reset field [ItineraryId] with value @iItineraryIdOld  to value @iItineraryId
-- Gets: @iItineraryId int
-- Gets: @iItineraryIdOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_UpdateAllWItineraryIdLogic]
	@iItineraryId int,
	@iItineraryIdOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Product_ItineraryTemplatePhotos]
SET
	[ItineraryId] = @iItineraryId
WHERE
	[ItineraryId] = @iItineraryIdOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'Product_ItineraryTemplatePhotos'
-- using the Primary Key. 
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_Delete]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[Product_ItineraryTemplatePhotos]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [ItineraryId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_DeleteAllWItineraryIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_DeleteAllWItineraryIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'Product_ItineraryTemplatePhotos'
-- based on a foreign key field.
-- Gets: @iItineraryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_DeleteAllWItineraryIdLogic]
	@iItineraryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[Product_ItineraryTemplatePhotos]
WHERE
	[ItineraryId] = @iItineraryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'Product_ItineraryTemplatePhotos'
-- based on the Primary Key.
-- Gets: @iId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_SelectOne]
	@iId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[Id],
	[ItineraryId],
	[Photos],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product_ItineraryTemplatePhotos]
WHERE
	[Id] = @iId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'Product_ItineraryTemplatePhotos'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[Id],
	[ItineraryId],
	[Photos],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product_ItineraryTemplatePhotos]
ORDER BY 
	[Id] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [ItineraryId].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Product_ItineraryTemplatePhotos_SelectAllWItineraryIdLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_Product_ItineraryTemplatePhotos_SelectAllWItineraryIdLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'Product_ItineraryTemplatePhotos'
-- based on a foreign key field.
-- Gets: @iItineraryId int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Product_ItineraryTemplatePhotos_SelectAllWItineraryIdLogic]
	@iItineraryId int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[Id],
	[ItineraryId],
	[Photos],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Product_ItineraryTemplatePhotos]
WHERE
	[ItineraryId] = @iItineraryId
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     Product_ItineraryTemplatePhotos]
-- ========================================================================================================
GO
