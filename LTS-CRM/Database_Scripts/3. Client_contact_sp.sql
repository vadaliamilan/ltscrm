GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_Update]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Clients_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_SelectOneByEmail]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_SelectOneByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Clients_SelectOneByEmail]
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_SelectOne]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Clients_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_SelectAll]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Clients_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_Insert]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Clients_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_Delete]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Clients_Delete]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_CheckDuplicateEmail]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_CheckDuplicateEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].sp_ClientContacts_CheckDuplicateEmail
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_UpdateAllWClientIDLogic]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_UpdateAllWClientIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_UpdateAllWClientIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_Update]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_Update]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_SelectOne]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_SelectOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_SelectOne]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_SelectAllWClientIDLogic]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_SelectAllWClientIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_SelectAllWClientIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_SelectAll]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_SelectAll]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_Insert]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_Insert]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_DeleteAllWClientIDLogic]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_DeleteAllWClientIDLogic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_DeleteAllWClientIDLogic]
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_Delete]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ClientContacts_Delete]
GO
/****** Object:  StoredProcedure [dbo].[GetOnlineForm]    Script Date: 05-04-2017 18:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetOnlineForm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetOnlineForm]
GO
/****** Object:  StoredProcedure [dbo].[GetOnlineForm]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetOnlineForm]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetOnlineForm]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @cols AS NVARCHAR(MAX);
DECLARE @query AS NVARCHAR(MAX);

select @cols = STUFF((SELECT distinct '','' +
                        QUOTENAME(DepartmentName)
                      FROM Department
                      FOR XML PATH(''''), TYPE
                     ).value(''.'', ''NVARCHAR(MAX)'') 
                        , 1, 1, '''');

SELECT @query =

''SELECT *
FROM
(
  SELECT
    DU.ID,
    D.DepartmentName as Department,C.CompanyName as Company,
    IsNull(DU.FileName ,'''''''') as FileName
  FROM DocumentUpload DU Inner Join Department D on Du.DepartmentID=D.ID Inner Join Company C on Du.CompanyID=C.ID where DocumentCategory=3
) AS t
PIVOT 
(
  MAX(FileName) 
  FOR Department IN( '' + @cols + '' )'' +
'' ) AS p ; '';

 execute(@query);
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_Delete]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''ClientContacts''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[ClientContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_DeleteAllWClientIDLogic]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_DeleteAllWClientIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table ''ClientContacts''
-- based on a foreign key field.
-- Gets: @iClientID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_DeleteAllWClientIDLogic]
	@iClientID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[ClientContacts]
WHERE
	[ClientID] = @iClientID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_Insert]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''ClientContacts''
-- Gets: @iClientID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_Insert]
	@iClientID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[ClientContacts]
(
	[ClientID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iClientID,
	@sTitle,
	@sSurName,
	@sFirstName,
	@bMainContact,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_SelectAll]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ClientContacts''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ClientID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ClientContacts]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_SelectAllWClientIDLogic]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_SelectAllWClientIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table ''ClientContacts''
-- based on a foreign key field.
-- Gets: @iClientID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_SelectAllWClientIDLogic]
	@iClientID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[ClientID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ClientContacts]
WHERE
	[ClientID] = @iClientID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_SelectOne]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''ClientContacts''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ClientID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[ClientContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_CheckDuplicateEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[sp_ClientContacts_CheckDuplicateEmail]
	@iID int=null,
	@iClientID int,
	@sEmail nvarchar(150),
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	CC.[ID],
	CC.[ClientID],
	CC.[Title],
	CC.MainContact,
	 CC.Email as ContactEmail,
	 C.Email as ClientEmail
FROM 
dbo.Clients C join [dbo].[ClientContacts] CC on C.ID=cc.ClientID
WHERE
	[ClientID] = @iClientID
	and ( CC.ID  <> @iID OR @iID is null)
	 and 
	 (
		CC.Email = @sEmail
	 OR C.Email = @sEmail 
	 )
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR

' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_Update]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''ClientContacts''
-- Gets: @iID int
-- Gets: @iClientID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(510)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_Update]
	@iID int,
	@iClientID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(510),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ClientContacts]
SET 
	[ClientID] = @iClientID,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[MainContact] = @bMainContact,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ClientContacts_UpdateAllWClientIDLogic]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ClientContacts_UpdateAllWClientIDLogic]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table ''ClientContacts''.
-- Will reset field [ClientID] with value @iClientIDOld  to value @iClientID
-- Gets: @iClientID int
-- Gets: @iClientIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_ClientContacts_UpdateAllWClientIDLogic]
	@iClientID int,
	@iClientIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[ClientContacts]
SET
	[ClientID] = @iClientID
WHERE
	[ClientID] = @iClientIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_Delete]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table ''Clients''
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Clients_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON

DELETE FROM [dbo].[ClientContacts]
WHERE
	ClientID = @iID

-- DELETE an existing row from the table.
DELETE FROM [dbo].[Clients]
WHERE
	[ID] = @iID


-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_Insert]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_Insert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table ''Clients''
-- Gets: @iCompanyID int
-- Gets: @daClientDate datetime
-- Gets: @sSalesStaff nvarchar(50)
-- Gets: @sCompanyOrInd nvarchar(50)
-- Gets: @sDirectOrAgenet nvarchar(50)
-- Gets: @sCompanyName nvarchar(50)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFax nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sPaymentTerms nvarchar(100)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @fRating float(53)
-- Gets: @sComment nvarchar(150)
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Clients_Insert]
	@iCompanyID int,
	@daClientDate datetime,
	@sSalesStaff nvarchar(50),
	@sCompanyOrInd nvarchar(50),
	@sDirectOrAgenet nvarchar(50),
	@sCompanyName nvarchar(50),
	@sWebsite nvarchar(150),
	@sTelephone nvarchar(15),
	@sFax nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sPaymentTerms nvarchar(100),
	@sBlackList nvarchar(200),
	@fRating float(53),
	@sComment nvarchar(150),
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[Clients]
(
	[CompanyID],
	[ClientDate],
	[SalesStaff],
	[CompanyOrInd],
	[DirectOrAgenet],
	[CompanyName],
	[Website],
	[Telephone],
	[Fax],
	[Email],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[Comment],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iCompanyID,
	@daClientDate,
	@sSalesStaff,
	@sCompanyOrInd,
	@sDirectOrAgenet,
	@sCompanyName,
	@sWebsite,
	@sTelephone,
	@sFax,
	@sEmail,
	@sAddress1,
	@sAddress2,
	@sCity,
	@sState,
	@sCountry,
	@sPostalCode,
	@sPaymentTerms,
	@sBlackList,
	@fRating,
	@sComment,
	@biFileData,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_SelectAll]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_SelectAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''Clients''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Clients_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[CompanyID],
	[ClientDate],
	[SalesStaff],
	[CompanyOrInd],
	[DirectOrAgenet],
	[CompanyName],
	[Website],
	[Telephone],
	[Fax],
	[Email],
	[Address1],
	[Address2],
	[City],
	[State],
	[Country],
	[PostalCode],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[Comment],
	[FileData],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[Clients]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_SelectOne]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_SelectOne]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table ''Clients''
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Clients_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[Clients].* ,
	[ClientContacts].ID AS ClientContactID
,[ClientContacts].ClientID
,[ClientContacts].Title
,[ClientContacts].SurName
,[ClientContacts].FirstName
,[ClientContacts].MainContact
,[ClientContacts].Position
,[ClientContacts].Department
,[ClientContacts].Email AS ClientContactEmail
,[ClientContacts].DirectPhone
,[ClientContacts].MobileNo
,[ClientContacts].FaxNo
,[ClientContacts].WeChat
,[ClientContacts].Whatsup
,[ClientContacts].Skype
,[ClientContacts].Facebook
,[ClientContacts].Twitter
,[ClientContacts].LinkedIn
,[ClientContacts].CreatedBy
,[ClientContacts].CreatedDate
,[ClientContacts].UpdatedDate
,[ClientContacts].UpdatedBy
	 
FROM [dbo].[Clients]
left join [dbo].[ClientContacts] on [Clients].ID =  [ClientContacts].ClientID and ClientContacts.MainContact = 1

WHERE
	[Clients].[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR

' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_SelectOneByEmail]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_SelectOneByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'  

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table ''ClientContacts''
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Clients_SelectOneByEmail]
	@sEmail nvarchar(50) ,
	@iID int=0,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
 
	[Email]
FROM [dbo].[Clients]
where Email = @sEmail
and (ID != @iID )
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR

' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Clients_Update]    Script Date: 05-04-2017 18:35:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Clients_Update]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table ''Clients''
-- Gets: @iID int
-- Gets: @iCompanyID int
-- Gets: @daClientDate datetime
-- Gets: @sSalesStaff nvarchar(50)
-- Gets: @sCompanyOrInd nvarchar(50)
-- Gets: @sDirectOrAgenet nvarchar(50)
-- Gets: @sCompanyName nvarchar(50)
-- Gets: @sWebsite nvarchar(150)
-- Gets: @sTelephone nvarchar(15)
-- Gets: @sFax nvarchar(50)
-- Gets: @sEmail nvarchar(50)
-- Gets: @sAddress1 nvarchar(250)
-- Gets: @sAddress2 nvarchar(50)
-- Gets: @sCity nvarchar(50)
-- Gets: @sState nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostalCode nvarchar(6)
-- Gets: @sPaymentTerms nvarchar(100)
-- Gets: @sBlackList nvarchar(200)
-- Gets: @fRating float(53)
-- Gets: @sComment nvarchar(150)
-- Gets: @biFileData varbinary(Max)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_Clients_Update]
	@iID int,
	@iCompanyID int,
	@daClientDate datetime,
	@sSalesStaff nvarchar(50),
	@sCompanyOrInd nvarchar(50),
	@sDirectOrAgenet nvarchar(50),
	@sCompanyName nvarchar(50),
	@sWebsite nvarchar(150),
	@sTelephone nvarchar(15),
	@sFax nvarchar(50),
	@sEmail nvarchar(50),
	@sAddress1 nvarchar(250),
	@sAddress2 nvarchar(50),
	@sCity nvarchar(50),
	@sState nvarchar(50),
	@sCountry nvarchar(50),
	@sPostalCode nvarchar(6),
	@sPaymentTerms nvarchar(100),
	@sBlackList nvarchar(200),
	@fRating float(53),
	@sComment nvarchar(150),
	@biFileData varbinary(Max),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[Clients]
SET 
	[CompanyID] = @iCompanyID,
	[ClientDate] = @daClientDate,
	[SalesStaff] = @sSalesStaff,
	[CompanyOrInd] = @sCompanyOrInd,
	[DirectOrAgenet] = @sDirectOrAgenet,
	[CompanyName] = @sCompanyName,
	[Website] = @sWebsite,
	[Telephone] = @sTelephone,
	[Fax] = @sFax,
	[Email] = @sEmail,
	[Address1] = @sAddress1,
	[Address2] = @sAddress2,
	[City] = @sCity,
	[State] = @sState,
	[Country] = @sCountry,
	[PostalCode] = @sPostalCode,
	[PaymentTerms] = @sPaymentTerms,
	[BlackList] = @sBlackList,
	[Rating] = @fRating,
	[Comment] = @sComment,
	[FileData] = @biFileData,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
' 
END
GO
