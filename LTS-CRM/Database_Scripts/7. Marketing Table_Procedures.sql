-- Start Table Script 
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [FK_MarketingContacts_MarketingChannel]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [DF_MarketingContacts_UpdatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] DROP CONSTRAINT [DF_MarketingContacts_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] DROP CONSTRAINT [DF_MarketingChannel_UpdatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] DROP CONSTRAINT [DF_MarketingChannel_CreatedDate]
END

GO
/****** Object:  Table [dbo].[MarketingProject]    Script Date: 19-04-2017 15:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingProject]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingProject]
GO
/****** Object:  Table [dbo].[MarketingContacts]    Script Date: 19-04-2017 15:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingContacts]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingContacts]
GO
/****** Object:  Table [dbo].[MarketingChannel]    Script Date: 19-04-2017 15:00:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingChannel]') AND type in (N'U'))
DROP TABLE [dbo].[MarketingChannel]
GO
/****** Object:  Table [dbo].[MarketingChannel]    Script Date: 19-04-2017 15:00:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingChannel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingChannel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ContractedBy] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](100) NULL,
	[CompanyWebsite] [nvarchar](250) NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](250) NULL,
	[CompanyTelephone] [nvarchar](50) NULL,
	[CompanyEmail] [nvarchar](50) NULL,
	[CompanyAddress1] [nvarchar](100) NULL,
	[CompanyAddress2] [nvarchar](100) NULL,
	[City] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[PostCode] [nvarchar](50) NULL,
	[AdvertisingCost] [nvarchar](50) NULL,
	[AmountOfFee] [decimal](18, 2) NULL,
	[PaymentTerms] [nvarchar](max) NULL,
	[BlackList] [nvarchar](50) NULL,
	[Rating] [nvarchar](50) NULL,
	[MainBusiness] [nvarchar](max) NULL,
	[HowToUseMarketingChannel] [nvarchar](max) NULL,
	[Comments] [nvarchar](max) NULL,
	[FirstDayOfCollabration] [nvarchar](250) NULL,
	[ContractExpireDate] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
	[SignedAgreedContractUpload] [varbinary](max) NULL,
	[ExtraField1] [nvarchar](50) NULL,
	[ExtraField2] [nvarchar](50) NULL,
	[ExtraField3] [nvarchar](50) NULL,
	[ExtraField4] [nvarchar](50) NULL,
	[ExtraField5] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingChannel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MarketingContacts]    Script Date: 19-04-2017 15:00:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingContacts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingContacts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MarketingChannelID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[MainContact] [bit] NOT NULL,
	[Position] [nvarchar](50) NULL,
	[Department] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[DirectPhone] [nvarchar](50) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[FaxNo] [nvarchar](50) NULL,
	[WeChat] [nvarchar](150) NULL,
	[Whatsup] [nvarchar](150) NULL,
	[Skype] [nvarchar](150) NULL,
	[Facebook] [nvarchar](150) NULL,
	[Twitter] [nvarchar](150) NULL,
	[LinkedIn] [nvarchar](150) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingContacts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[MarketingProject]    Script Date: 19-04-2017 15:00:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MarketingProject]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MarketingProject](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[ProjectName] [nvarchar](250) NULL,
	[ProjectDescribution] [nvarchar](max) NULL,
	[InitiateBy] [nvarchar](50) NULL,
	[StaffInvolved] [nvarchar](50) NULL,
	[ProjectStartingDate] [datetime] NULL,
	[ProjectPlannedEndingDate] [datetime] NULL,
	[WorkingProcessComment] [nvarchar](max) NULL,
	[ProjectActualEndDate] [datetime] NULL,
	[ProjectIncome] [decimal](18, 2) NULL,
	[ProjectExpense] [decimal](18, 2) NULL,
	[ProjectGrossProfit] [decimal](18, 2) NULL,
	[Rating] [nvarchar](50) NULL,
	[ProjectReport] [nvarchar](max) NULL,
	[ExtraField1] [nvarchar](50) NULL,
	[ExtraField2] [nvarchar](50) NULL,
	[ExtraField3] [nvarchar](50) NULL,
	[ExtraField4] [nvarchar](50) NULL,
	[ExtraField5] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketingProject] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] ADD  CONSTRAINT [DF_MarketingChannel_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingChannel_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingChannel] ADD  CONSTRAINT [DF_MarketingChannel_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] ADD  CONSTRAINT [DF_MarketingContacts_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_MarketingContacts_UpdatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MarketingContacts] ADD  CONSTRAINT [DF_MarketingContacts_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts]  WITH CHECK ADD  CONSTRAINT [FK_MarketingContacts_MarketingChannel] FOREIGN KEY([MarketingChannelID])
REFERENCES [dbo].[MarketingChannel] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketingContacts_MarketingChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketingContacts]'))
ALTER TABLE [dbo].[MarketingContacts] CHECK CONSTRAINT [FK_MarketingContacts_MarketingChannel]
GO

-- End Table Script
GO
SET NOCOUNT ON

-- ========================================================================================================
-- [Stored Procedures generated for table:  MarketingChannel]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'MarketingChannel'
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sCompanyName nvarchar(100)
-- Gets: @sCompanyWebsite nvarchar(250)
-- Gets: @sUsername nvarchar(50)
-- Gets: @sPassword nvarchar(250)
-- Gets: @sCompanyTelephone nvarchar(50)
-- Gets: @sCompanyEmail nvarchar(50)
-- Gets: @sCompanyAddress1 nvarchar(100)
-- Gets: @sCompanyAddress2 nvarchar(100)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(50)
-- Gets: @sAdvertisingCost nvarchar(50)
-- Gets: @dcAmountOfFee decimal(18, 2)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sRating nvarchar(50)
-- Gets: @sMainBusiness nvarchar(Max)
-- Gets: @sHowToUseMarketingChannel nvarchar(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sFirstDayOfCollabration nvarchar(250)
-- Gets: @daContractExpireDate datetime
-- Gets: @sStatus nvarchar(50)
-- Gets: @biSignedAgreedContractUpload varbinary(Max)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Insert]
	@sContractedBy nvarchar(50),
	@sCompanyName nvarchar(100),
	@sCompanyWebsite nvarchar(250),
	@sUsername nvarchar(50),
	@sPassword nvarchar(250),
	@sCompanyTelephone nvarchar(50),
	@sCompanyEmail nvarchar(50),
	@sCompanyAddress1 nvarchar(100),
	@sCompanyAddress2 nvarchar(100),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(50),
	@sAdvertisingCost nvarchar(50),
	@dcAmountOfFee decimal(18, 2),
	@sPaymentTerms nvarchar(Max),
	@sBlackList nvarchar(50),
	@sRating nvarchar(50),
	@sMainBusiness nvarchar(Max),
	@sHowToUseMarketingChannel nvarchar(Max),
	@sComments nvarchar(Max),
	@sFirstDayOfCollabration nvarchar(250),
	@daContractExpireDate datetime,
	@sStatus nvarchar(50),
	@biSignedAgreedContractUpload varbinary(Max),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingChannel]
(
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@sContractedBy,
	@sCompanyName,
	@sCompanyWebsite,
	@sUsername,
	@sPassword,
	@sCompanyTelephone,
	@sCompanyEmail,
	@sCompanyAddress1,
	@sCompanyAddress2,
	@sCity,
	@sCountry,
	@sPostCode,
	@sAdvertisingCost,
	@dcAmountOfFee,
	@sPaymentTerms,
	@sBlackList,
	@sRating,
	@sMainBusiness,
	@sHowToUseMarketingChannel,
	@sComments,
	@sFirstDayOfCollabration,
	@daContractExpireDate,
	@sStatus,
	@biSignedAgreedContractUpload,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'MarketingChannel'
-- Gets: @iID int
-- Gets: @sContractedBy nvarchar(50)
-- Gets: @sCompanyName nvarchar(100)
-- Gets: @sCompanyWebsite nvarchar(250)
-- Gets: @sUsername nvarchar(50)
-- Gets: @sPassword nvarchar(250)
-- Gets: @sCompanyTelephone nvarchar(50)
-- Gets: @sCompanyEmail nvarchar(50)
-- Gets: @sCompanyAddress1 nvarchar(100)
-- Gets: @sCompanyAddress2 nvarchar(100)
-- Gets: @sCity nvarchar(50)
-- Gets: @sCountry nvarchar(50)
-- Gets: @sPostCode nvarchar(50)
-- Gets: @sAdvertisingCost nvarchar(50)
-- Gets: @dcAmountOfFee decimal(18, 2)
-- Gets: @sPaymentTerms nvarchar(Max)
-- Gets: @sBlackList nvarchar(50)
-- Gets: @sRating nvarchar(50)
-- Gets: @sMainBusiness nvarchar(Max)
-- Gets: @sHowToUseMarketingChannel nvarchar(Max)
-- Gets: @sComments nvarchar(Max)
-- Gets: @sFirstDayOfCollabration nvarchar(250)
-- Gets: @daContractExpireDate datetime
-- Gets: @sStatus nvarchar(50)
-- Gets: @biSignedAgreedContractUpload varbinary(Max)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Update]
	@iID int,
	@sContractedBy nvarchar(50),
	@sCompanyName nvarchar(100),
	@sCompanyWebsite nvarchar(250),
	@sUsername nvarchar(50),
	@sPassword nvarchar(250),
	@sCompanyTelephone nvarchar(50),
	@sCompanyEmail nvarchar(50),
	@sCompanyAddress1 nvarchar(100),
	@sCompanyAddress2 nvarchar(100),
	@sCity nvarchar(50),
	@sCountry nvarchar(50),
	@sPostCode nvarchar(50),
	@sAdvertisingCost nvarchar(50),
	@dcAmountOfFee decimal(18, 2),
	@sPaymentTerms nvarchar(Max),
	@sBlackList nvarchar(50),
	@sRating nvarchar(50),
	@sMainBusiness nvarchar(Max),
	@sHowToUseMarketingChannel nvarchar(Max),
	@sComments nvarchar(Max),
	@sFirstDayOfCollabration nvarchar(250),
	@daContractExpireDate datetime,
	@sStatus nvarchar(50),
	@biSignedAgreedContractUpload varbinary(Max),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingChannel]
SET 
	[ContractedBy] = @sContractedBy,
	[CompanyName] = @sCompanyName,
	[CompanyWebsite] = @sCompanyWebsite,
	[Username] = @sUsername,
	[Password] = @sPassword,
	[CompanyTelephone] = @sCompanyTelephone,
	[CompanyEmail] = @sCompanyEmail,
	[CompanyAddress1] = @sCompanyAddress1,
	[CompanyAddress2] = @sCompanyAddress2,
	[City] = @sCity,
	[Country] = @sCountry,
	[PostCode] = @sPostCode,
	[AdvertisingCost] = @sAdvertisingCost,
	[AmountOfFee] = @dcAmountOfFee,
	[PaymentTerms] = @sPaymentTerms,
	[BlackList] = @sBlackList,
	[Rating] = @sRating,
	[MainBusiness] = @sMainBusiness,
	[HowToUseMarketingChannel] = @sHowToUseMarketingChannel,
	[Comments] = @sComments,
	[FirstDayOfCollabration] = @sFirstDayOfCollabration,
	[ContractExpireDate] = @daContractExpireDate,
	[Status] = @sStatus,
	[SignedAgreedContractUpload] = @biSignedAgreedContractUpload,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'MarketingChannel'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingChannel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'MarketingChannel'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingChannel]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingChannel_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingChannel_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'MarketingChannel'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingChannel_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ContractedBy],
	[CompanyName],
	[CompanyWebsite],
	[Username],
	[Password],
	[CompanyTelephone],
	[CompanyEmail],
	[CompanyAddress1],
	[CompanyAddress2],
	[City],
	[Country],
	[PostCode],
	[AdvertisingCost],
	[AmountOfFee],
	[PaymentTerms],
	[BlackList],
	[Rating],
	[MainBusiness],
	[HowToUseMarketingChannel],
	[Comments],
	[FirstDayOfCollabration],
	[ContractExpireDate],
	[Status],
	[SignedAgreedContractUpload],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingChannel]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     MarketingChannel]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  MarketingContacts]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'MarketingContacts'
-- Gets: @iMarketingChannelID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_Insert]
	@iMarketingChannelID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingContacts]
(
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@iMarketingChannelID,
	@sTitle,
	@sSurName,
	@sFirstName,
	@bMainContact,
	@sPosition,
	@sDepartment,
	@sEmail,
	@sDirectPhone,
	@sMobileNo,
	@sFaxNo,
	@sWeChat,
	@sWhatsup,
	@sSkype,
	@sFacebook,
	@sTwitter,
	@sLinkedIn,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'MarketingContacts'
-- Gets: @iID int
-- Gets: @iMarketingChannelID int
-- Gets: @sTitle nvarchar(50)
-- Gets: @sSurName nvarchar(150)
-- Gets: @sFirstName nvarchar(150)
-- Gets: @bMainContact bit
-- Gets: @sPosition nvarchar(50)
-- Gets: @sDepartment nvarchar(150)
-- Gets: @sEmail nvarchar(150)
-- Gets: @sDirectPhone nvarchar(50)
-- Gets: @sMobileNo nvarchar(50)
-- Gets: @sFaxNo nvarchar(50)
-- Gets: @sWeChat nvarchar(150)
-- Gets: @sWhatsup nvarchar(150)
-- Gets: @sSkype nvarchar(150)
-- Gets: @sFacebook nvarchar(150)
-- Gets: @sTwitter nvarchar(150)
-- Gets: @sLinkedIn nvarchar(150)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_Update]
	@iID int,
	@iMarketingChannelID int,
	@sTitle nvarchar(50),
	@sSurName nvarchar(150),
	@sFirstName nvarchar(150),
	@bMainContact bit,
	@sPosition nvarchar(50),
	@sDepartment nvarchar(150),
	@sEmail nvarchar(150),
	@sDirectPhone nvarchar(50),
	@sMobileNo nvarchar(50),
	@sFaxNo nvarchar(50),
	@sWeChat nvarchar(150),
	@sWhatsup nvarchar(150),
	@sSkype nvarchar(150),
	@sFacebook nvarchar(150),
	@sTwitter nvarchar(150),
	@sLinkedIn nvarchar(150),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingContacts]
SET 
	[MarketingChannelID] = @iMarketingChannelID,
	[Title] = @sTitle,
	[SurName] = @sSurName,
	[FirstName] = @sFirstName,
	[MainContact] = @bMainContact,
	[Position] = @sPosition,
	[Department] = @sDepartment,
	[Email] = @sEmail,
	[DirectPhone] = @sDirectPhone,
	[MobileNo] = @sMobileNo,
	[FaxNo] = @sFaxNo,
	[WeChat] = @sWeChat,
	[Whatsup] = @sWhatsup,
	[Skype] = @sSkype,
	[Facebook] = @sFacebook,
	[Twitter] = @sTwitter,
	[LinkedIn] = @sLinkedIn,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Update Stored procedure for updating one or more rows using field [MarketingChannelID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update one or more existing rows in the table 'MarketingContacts'.
-- Will reset field [MarketingChannelID] with value @iMarketingChannelIDOld  to value @iMarketingChannelID
-- Gets: @iMarketingChannelID int
-- Gets: @iMarketingChannelIDOld int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_UpdateAllWMarketingChannelIDLogic]
	@iMarketingChannelID int,
	@iMarketingChannelIDOld int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingContacts]
SET
	[MarketingChannelID] = @iMarketingChannelID
WHERE
	[MarketingChannelID] = @iMarketingChannelIDOld
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'MarketingContacts'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure, based on field [MarketingChannelID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete one or more existing rows from the table 'MarketingContacts'
-- based on a foreign key field.
-- Gets: @iMarketingChannelID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_DeleteAllWMarketingChannelIDLogic]
	@iMarketingChannelID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE one or more existing rows from the table.
DELETE
FROM [dbo].[MarketingContacts]
WHERE
	[MarketingChannelID] = @iMarketingChannelID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'MarketingContacts'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingContacts]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'MarketingContacts'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingContacts]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on field [MarketingChannelID].
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select one or more existing rows from the table 'MarketingContacts'
-- based on a foreign key field.
-- Gets: @iMarketingChannelID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingContacts_SelectAllWMarketingChannelIDLogic]
	@iMarketingChannelID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT one or more existing rows from the table.
SELECT
	[ID],
	[MarketingChannelID],
	[Title],
	[SurName],
	[FirstName],
	[MainContact],
	[Position],
	[Department],
	[Email],
	[DirectPhone],
	[MobileNo],
	[FaxNo],
	[WeChat],
	[Whatsup],
	[Skype],
	[Facebook],
	[Twitter],
	[LinkedIn],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingContacts]
WHERE
	[MarketingChannelID] = @iMarketingChannelID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     MarketingContacts]
-- ========================================================================================================
GO

-- ========================================================================================================
-- [Stored Procedures generated for table:  MarketingProject]
GO

-- //// Insert Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_Insert]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'MarketingProject'
-- Gets: @sReferenceNo nvarchar(100)
-- Gets: @sProjectName nvarchar(250)
-- Gets: @sProjectDescribution nvarchar(Max)
-- Gets: @sInitiateBy nvarchar(50)
-- Gets: @sStaffInvolved nvarchar(50)
-- Gets: @daProjectStartingDate datetime
-- Gets: @daProjectPlannedEndingDate datetime
-- Gets: @sWorkingProcessComment nvarchar(Max)
-- Gets: @daProjectActualEndDate datetime
-- Gets: @dcProjectIncome decimal(18, 2)
-- Gets: @dcProjectExpense decimal(18, 2)
-- Gets: @dcProjectGrossProfit decimal(18, 2)
-- Gets: @sRating nvarchar(50)
-- Gets: @sProjectReport nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Insert]
	@sReferenceNo nvarchar(100),
	@sProjectName nvarchar(250),
	@sProjectDescribution nvarchar(Max),
	@sInitiateBy nvarchar(50),
	@sStaffInvolved nvarchar(50),
	@daProjectStartingDate datetime,
	@daProjectPlannedEndingDate datetime,
	@sWorkingProcessComment nvarchar(Max),
	@daProjectActualEndDate datetime,
	@dcProjectIncome decimal(18, 2),
	@dcProjectExpense decimal(18, 2),
	@dcProjectGrossProfit decimal(18, 2),
	@sRating nvarchar(50),
	@sProjectReport nvarchar(Max),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iID int OUTPUT,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- INSERT a new row in the table.
INSERT [dbo].[MarketingProject]
(
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
)
VALUES
(
	@sReferenceNo,
	@sProjectName,
	@sProjectDescribution,
	@sInitiateBy,
	@sStaffInvolved,
	@daProjectStartingDate,
	@daProjectPlannedEndingDate,
	@sWorkingProcessComment,
	@daProjectActualEndDate,
	@dcProjectIncome,
	@dcProjectExpense,
	@dcProjectGrossProfit,
	@sRating,
	@sProjectReport,
	@sExtraField1,
	@sExtraField2,
	@sExtraField3,
	@sExtraField4,
	@sExtraField5,
	@sCreatedBy,
	@daCreatedDate,
	@sUpdatedBy,
	@daUpdatedDate
)
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @iID=SCOPE_IDENTITY()
GO


-- //// Update Stored procedure for updating one single row.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_Update]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will update an existing row in the table 'MarketingProject'
-- Gets: @iID int
-- Gets: @sReferenceNo nvarchar(100)
-- Gets: @sProjectName nvarchar(250)
-- Gets: @sProjectDescribution nvarchar(Max)
-- Gets: @sInitiateBy nvarchar(50)
-- Gets: @sStaffInvolved nvarchar(50)
-- Gets: @daProjectStartingDate datetime
-- Gets: @daProjectPlannedEndingDate datetime
-- Gets: @sWorkingProcessComment nvarchar(Max)
-- Gets: @daProjectActualEndDate datetime
-- Gets: @dcProjectIncome decimal(18, 2)
-- Gets: @dcProjectExpense decimal(18, 2)
-- Gets: @dcProjectGrossProfit decimal(18, 2)
-- Gets: @sRating nvarchar(50)
-- Gets: @sProjectReport nvarchar(Max)
-- Gets: @sExtraField1 nvarchar(50)
-- Gets: @sExtraField2 nvarchar(50)
-- Gets: @sExtraField3 nvarchar(50)
-- Gets: @sExtraField4 nvarchar(50)
-- Gets: @sExtraField5 nvarchar(50)
-- Gets: @sCreatedBy nvarchar(50)
-- Gets: @daCreatedDate datetime
-- Gets: @sUpdatedBy nvarchar(50)
-- Gets: @daUpdatedDate datetime
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Update]
	@iID int,
	@sReferenceNo nvarchar(100),
	@sProjectName nvarchar(250),
	@sProjectDescribution nvarchar(Max),
	@sInitiateBy nvarchar(50),
	@sStaffInvolved nvarchar(50),
	@daProjectStartingDate datetime,
	@daProjectPlannedEndingDate datetime,
	@sWorkingProcessComment nvarchar(Max),
	@daProjectActualEndDate datetime,
	@dcProjectIncome decimal(18, 2),
	@dcProjectExpense decimal(18, 2),
	@dcProjectGrossProfit decimal(18, 2),
	@sRating nvarchar(50),
	@sProjectReport nvarchar(Max),
	@sExtraField1 nvarchar(50),
	@sExtraField2 nvarchar(50),
	@sExtraField3 nvarchar(50),
	@sExtraField4 nvarchar(50),
	@sExtraField5 nvarchar(50),
	@sCreatedBy nvarchar(50),
	@daCreatedDate datetime,
	@sUpdatedBy nvarchar(50),
	@daUpdatedDate datetime,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- UPDATE an existing row in the table.
UPDATE [dbo].[MarketingProject]
SET 
	[ReferenceNo] = @sReferenceNo,
	[ProjectName] = @sProjectName,
	[ProjectDescribution] = @sProjectDescribution,
	[InitiateBy] = @sInitiateBy,
	[StaffInvolved] = @sStaffInvolved,
	[ProjectStartingDate] = @daProjectStartingDate,
	[ProjectPlannedEndingDate] = @daProjectPlannedEndingDate,
	[WorkingProcessComment] = @sWorkingProcessComment,
	[ProjectActualEndDate] = @daProjectActualEndDate,
	[ProjectIncome] = @dcProjectIncome,
	[ProjectExpense] = @dcProjectExpense,
	[ProjectGrossProfit] = @dcProjectGrossProfit,
	[Rating] = @sRating,
	[ProjectReport] = @sProjectReport,
	[ExtraField1] = @sExtraField1,
	[ExtraField2] = @sExtraField2,
	[ExtraField3] = @sExtraField3,
	[ExtraField4] = @sExtraField4,
	[ExtraField5] = @sExtraField5,
	[CreatedBy] = @sCreatedBy,
	[CreatedDate] = @daCreatedDate,
	[UpdatedBy] = @sUpdatedBy,
	[UpdatedDate] = @daUpdatedDate
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Delete Stored procedure using Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_Delete]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will delete an existing row from the table 'MarketingProject'
-- using the Primary Key. 
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_Delete]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- DELETE an existing row from the table.
DELETE FROM [dbo].[MarketingProject]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select Stored procedure, based on Primary Key.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_SelectOne]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_SelectOne]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select an existing row from the table 'MarketingProject'
-- based on the Primary Key.
-- Gets: @iID int
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_SelectOne]
	@iID int,
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT an existing row from the table.
SELECT
	[ID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingProject]
WHERE
	[ID] = @iID
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- //// Select All Stored procedure.
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_MarketingProject_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[sp_MarketingProject_SelectAll]
GO

---------------------------------------------------------------------------------
-- Stored procedure that will select all rows from the table 'MarketingProject'
-- Returns: @iErrorCode int
---------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_MarketingProject_SelectAll]
	@iErrorCode int OUTPUT
AS
SET NOCOUNT ON
-- SELECT all rows from the table.
SELECT
	[ID],
	[ReferenceNo],
	[ProjectName],
	[ProjectDescribution],
	[InitiateBy],
	[StaffInvolved],
	[ProjectStartingDate],
	[ProjectPlannedEndingDate],
	[WorkingProcessComment],
	[ProjectActualEndDate],
	[ProjectIncome],
	[ProjectExpense],
	[ProjectGrossProfit],
	[Rating],
	[ProjectReport],
	[ExtraField1],
	[ExtraField2],
	[ExtraField3],
	[ExtraField4],
	[ExtraField5],
	[CreatedBy],
	[CreatedDate],
	[UpdatedBy],
	[UpdatedDate]
FROM [dbo].[MarketingProject]
ORDER BY 
	[ID] ASC
-- Get the Error Code for the statement just executed.
SELECT @iErrorCode=@@ERROR
GO


-- [End of Stored Procedures for table:     MarketingProject]
-- ========================================================================================================
GO
