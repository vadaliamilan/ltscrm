﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Admin
{
    public partial class OrganizationEdit : System.Web.UI.Page
    {
        //The Page_Load function is called every time the page is reloaded.
        //If a button is clicked Page_Load is called first, then the button event handler.
        protected void Page_Load(object sender, EventArgs e)
        {
            // 
            //A postback is when the user clicks a button after the page has loaded
            //if it's false then we do our one time load of data her if it exists
            //When True: The page is loaded for the first time
            //When False: More than likely a button was clicked or user action has happened, after the page was loaded.
            if (IsPostBack == false)
            {


                //Declare Connection Variable (Connection)
                //Create a new object SqlConnection with the connection string initialized
                //Set the Connection variable to point to the new object instance.
                //If connection goes out of scope the database connection won't be closed and must be closed with either ( Close or Disopose )
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                //Opens a connection to the database.
                //If available draws from an open conneciton from the connection pool(if available)
                Connection.Open();
                // 
                //if SessionValue("EditOrganization.ID") > 0 then this is an edit
                //if SessionValue("EditOrganization.ID") = 0 then this is an insert
                Session.Add("EditOrganization.ID", "1");
                Session.Add("UserName", "Nilesh");
                if (SessionValue("EditOrganization.ID").ToString().Length > 0)
                {
                    String sql = "select * from [dbo].[Organization] where [ID] = @ID";
                    // 
                    //Declare SQLCommand Object named Command
                    //Create a new Command object with a select statement that will open the row referenced by Request("ID")
                    SqlCommand Command = new SqlCommand(sql, Connection);
                    // 
                    // 'Set the @ID parameter in the Command select query
                    Command.Parameters.AddWithValue("@ID", SessionValue("EditOrganization.ID"));
                    // 
                    //Declare a SqlDataReader Ojbect
                    //Load it with the Command's select statement
                    SqlDataReader Reader = Command.ExecuteReader();
                    // 
                    //If at least one record was found
                    if (Reader.Read())
                    {
                        txtorganization1.Text  = Reader["OrganizationName"].ToString();
                        txtwebsite1.Text = Reader["Website"].ToString();
                        cmbemployee.Value  = Reader["TotalEmployees"].ToString();
                        txtphoneno1.Text = Reader["PhoneNumber"].ToString();
                        txtfaxno1.Text = Reader["FaxNumber"].ToString();
                        cmbcountry.Value = Reader["CountryCode"].ToString();
                        txtstate1.Text = Reader["StateCode"].ToString();
                        txtcity1.Text = Reader["CityCode"].ToString();
                        txtaddress.Value = Reader["Address"].ToString();
                        txtorganizationdes.Text = Reader["OrganizationDescription"].ToString();
                        Session.Add("imagesrc", Reader["Image"]);
                        byte[] img = (byte[])(Reader["Image"]);                      

                        // If you want convert to a bitmap file
                        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                        Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(img);

                        string imgString = Convert.ToBase64String(img);
                        //Set the source with data:image/bmp
                        lmglogo.Src = String.Format("data:image/Bmp;base64,{0}\"", imgString);

                       
                    }
                    // 
                    //Close the Data Reader we are done with it.
                    Reader.Close();

                    //Closes the Connection to the back to the connection pool.
                    Connection.Close();
                    // 
                    //The length of ID equals zero.
                    //This is an insert so don't preload any data.
                }
               
              
            }
            // 
            //End of Page_Load Event Handler
        }


        protected void cmdSave_Click(object sender, EventArgs e)
        {

            //Declare String ID as set it to Request("ID") 
            String ID = "1";
            //Declare Connection Variable (Connection)
            //Create a new object SqlConnection with the connection string initialized
            //Set the Connection variable to point to the new object instance.
            //If connection goes out of scope the database connection won't be closed and must be closed with either ( Close or Disopose )
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            //Opens a connection to the database.
            //If available draws from an open conneciton from the connection pool(if available)
            Connection.Open();            // 
            //Declare SqlCommand Object named Command 
            //To be used to invoke the Update or Insert statements 
            SqlCommand Command;
            // 
            //Declare string InsertSQL 
            String InsertSQL = "Insert into [dbo].[Organization] ([OrganizationName], [Website], [TotalEmployees], [PhoneNumber], [FaxNumber], [CountryCode], [StateCode], [CityCode], [Address], [OrganizationDescription], [BusinessDomain], [CreatedBy], [CreatedDate], [UpdateBy], [UpdatedDate], [Image] ) VALUES ( @OrganizationName, @Website, @TotalEmployees, @PhoneNumber, @FaxNumber, @CountryCode, @StateCode, @CityCode, @Address, @OrganizationDescription, @BusinessDomain, @CreatedBy, @CreatedDate, @UpdateBy, @UpdatedDate, @Image ) ";
            // 
            //Declare String UpdateSQL 
            String UpdateSQL = "Update [dbo].[Organization] set [OrganizationName] = @OrganizationName, [Website] = @Website, [TotalEmployees] = @TotalEmployees, [PhoneNumber] = @PhoneNumber, [FaxNumber] = @FaxNumber, [CountryCode] = @CountryCode, [StateCode] = @StateCode, [CityCode] = @CityCode, [Address] = @Address, [OrganizationDescription] = @OrganizationDescription,  [UpdateBy] = @UpdateBy, [UpdatedDate] = @UpdatedDate, [Image] = @Image where [ID] = @ID ";
            //If the Length of ID > 0 then update the record in the database 
            //If the Length of ID = 0 then insert the record in the database. 
            if (String.IsNullOrEmpty(ID))
            {
                // 
                //Create a new Command object for inserting a new record. 
                Command = new SqlCommand(InsertSQL, Connection);
                // 
                //We are doing an insert 
            }
            else
            {
                // 
                //Set the command object with the update sql and connection. 
                Command = new SqlCommand(UpdateSQL, Connection);
                // 
                //Set the @ID field for updates. 
                Command.Parameters.AddWithValue("@ID", ID);

            }
            Command.Parameters.AddWithValue("@OrganizationName", txtorganization1.Text);
            Command.Parameters.AddWithValue("@Website", txtwebsite1.Text);
            Command.Parameters.AddWithValue("@TotalEmployees", cmbemployee.Value);
            Command.Parameters.AddWithValue("@PhoneNumber", txtphoneno1.Text);
            Command.Parameters.AddWithValue("@FaxNumber", txtfaxno1.Text);
            Command.Parameters.AddWithValue("@CountryCode", cmbcountry.Value);
            Command.Parameters.AddWithValue("@StateCode", txtstate1.Text);
            Command.Parameters.AddWithValue("@CityCode", txtcity1.Text);
            Command.Parameters.AddWithValue("@Address", txtaddress.Value);
            Command.Parameters.AddWithValue("@OrganizationDescription", txtorganizationdes.Text);
            Command.Parameters.AddWithValue("@UpdateBy", SessionValue("UserName"));
            Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

            FileUpload img = (FileUpload)imageupload;
            Byte[] imgByte = null;
            if (img.HasFile && img.PostedFile != null)
            {
                //To create a PostedFile
                HttpPostedFile File = imageupload.PostedFile;
                //Create byte Array with file len
                imgByte = new Byte[File.ContentLength];
                //force the control to load data in array
                File.InputStream.Read(imgByte, 0, File.ContentLength);
                Command.Parameters.AddWithValue("@Image", imgByte);
            }
            else
            {
                Command.CommandText = Command.CommandText.Replace(", [Image] = @Image", " ");
            }
            //Run the sql command against the database with no return values 
            // 
            //Run the statement ExecuteNonQuery returns no Results.
            Command.ExecuteNonQuery();

            //Closes the Connection to the back to the connection pool.
            Connection.Close();
            // 
            //Go to the Summary page 
           // Response.Redirect("Organization.aspx");

            // 
            //End event Save click 
        }


        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Organization.aspx");
        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        // 
        //End Class EditOrganization
    }
}