﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;





namespace LTS_CRM.Admin
{
    public partial class CompanyAddEdit : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
          
            
            if (IsPostBack == false)
            {
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();
               
                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                    String sql = "select * from [dbo].[Company] where [ID] = @ID";
                   
                    SqlCommand Command = new SqlCommand(sql, Connection);                   
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));                  
                    SqlDataReader Reader = Command.ExecuteReader();
                   
                    if (Reader.Read())
                    {
                        txtCompany.Text = Reader["CompanyName"].ToString();
                        txtwebsite1.Text = Reader["Website"].ToString();
                        cmbemployee.Value = Reader["TotalEmployees"].ToString();
                        txtphoneno1.Text = Reader["PhoneNumber"].ToString();
                        txtfaxno1.Text = Reader["FaxNumber"].ToString();
                        cmbcountry.Value = Reader["CountryCode"].ToString();
                        txtstate1.Text = Reader["StateCode"].ToString();
                        txtcity1.Text = Reader["CityCode"].ToString();
                        txtaddress.Value = Reader["Address"].ToString();
                        txtcompanydes.Text = Reader["CompanyDescription"].ToString();

                        if (Reader["Image"] != null && Reader["Image"] != DBNull.Value )
                        {
                            Session.Add("imagesrc", Reader["Image"]);
                            byte[] img = (byte[])(Reader["Image"]);

                            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                            Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(img);
                            string imgString = Convert.ToBase64String(img);
                            lmglogo.Src = String.Format("data:image/Bmp;base64,{0}\"", imgString);
                        }
                        
                    }
                   
                    Reader.Close();
                    Connection.Close();                   
                }

            }
          
        }


        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();

            string strsql =  "select * from [dbo].[Company] where CompanyName=@ID";
            
            if (checkDuplicate(strsql, txtCompany.Text) == true && ID == "")
            {
                lblheader.Text = "Record Exist";
                lblmsg.Text = "This Company Name exist in database.";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            else
            {
               
          
           
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();
                SqlCommand Command;
                String InsertSQL = "Insert into [dbo].[Company] ([CompanyName], [Website], [TotalEmployees], [PhoneNumber], [FaxNumber], [CountryCode], [StateCode], [CityCode], [Address], [CompanyDescription], [CreatedBy], [CreatedDate],  [Image] ) VALUES ( @CompanyName, @Website, @TotalEmployees, @PhoneNumber, @FaxNumber, @CountryCode, @StateCode, @CityCode, @Address, @CompanyDescription,  @CreatedBy, @CreatedDate, @Image ) ";

                String UpdateSQL = "Update [dbo].[Company] set [CompanyName] = @CompanyName, [Website] = @Website, [TotalEmployees] = @TotalEmployees, [PhoneNumber] = @PhoneNumber, [FaxNumber] = @FaxNumber, [CountryCode] = @CountryCode, [StateCode] = @StateCode, [CityCode] = @CityCode, [Address] = @Address, [CompanyDescription] = @CompanyDescription,  [UpdateBy] = @UpdateBy, [UpdatedDate] = @UpdatedDate, [Image] = @Image where [ID] = @ID ";

                if (String.IsNullOrEmpty(ID))
                {
                    // 
                    //Create a new Command object for inserting a new record. 
                    Command = new SqlCommand(InsertSQL, Connection);
                    // 
                    //We are doing an insert 
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                }
                else
                {
                    // 
                    //Set the command object with the update sql and connection. 
                    Command = new SqlCommand(UpdateSQL, Connection);
                    // 
                    //Set the @ID field for updates. 
                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@UpdateBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

                }
                Command.Parameters.AddWithValue("@CompanyName", txtCompany.Text);
                Command.Parameters.AddWithValue("@Website", txtwebsite1.Text);
                Command.Parameters.AddWithValue("@TotalEmployees", cmbemployee.Value);
                Command.Parameters.AddWithValue("@PhoneNumber", txtphoneno1.Text);
                Command.Parameters.AddWithValue("@FaxNumber", txtfaxno1.Text);
                Command.Parameters.AddWithValue("@CountryCode", cmbcountry.Value);
                Command.Parameters.AddWithValue("@StateCode", txtstate1.Text);
                Command.Parameters.AddWithValue("@CityCode", txtcity1.Text);
                Command.Parameters.AddWithValue("@Address", txtaddress.Value);
                Command.Parameters.AddWithValue("@CompanyDescription", txtcompanydes.Text);


               
                FileUpload img = (FileUpload)imageupload;
                Byte[] imgByte = null;
                if (img.HasFile && img.PostedFile != null)
                {
                    //To create a PostedFile
                    HttpPostedFile File = imageupload.PostedFile;
                    //Create byte Array with file len
                    imgByte = new Byte[File.ContentLength];
                    //force the control to load data in array
                    File.InputStream.Read(imgByte, 0, File.ContentLength);
                    Command.Parameters.AddWithValue("@Image", imgByte);
                }
                else
                {
                    Command.CommandText = Command.CommandText.Replace(", [Image] = @Image", " ");
                    Command.CommandText = Command.CommandText.Replace(",  [Image]", " ");
                    Command.CommandText = Command.CommandText.Replace(", @Image", " ");
                }
                //Run the sql command against the database with no return values 
                // 
                //Run the statement ExecuteNonQuery returns no Results.
                Command.ExecuteNonQuery();

                //Closes the Connection to the back to the connection pool.
                Connection.Close();
                // 
                //Go to the Summary page 
                Response.Redirect("Company.aspx");
           
            // 
            //End event Save click 
        }
        }

        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Company.aspx");
        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }

        protected Boolean checkDuplicate(string strsql,string ID)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();


            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            Command.Parameters.AddWithValue("@ID", ID);

            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
    }
}