﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="AnnouncementAddEdit.aspx.cs" Inherits="LTS_CRM.Admin.AnnouncementAddEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

           
            <ol class="breadcrumb">
                <li>Organization</li>
                <li>Announcement Add-Edit</li>
            </ol>
            

          
        </div>
          
        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
           
            <header>
                <h2>Announcement Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
           
            </div>
          
            <div class="widget-body">


                <div class="well">
                <p>
                        
                       
                    </p>
                    <p>
                       
                       Announcement Details: 
                    </p>
                   <p>
                    <CKEditor:CKEditorControl ID="txtannouncement" BasePath="~/ckeditor" runat="server"  Height="200" ToolbarStartupExpanded="False">
		            </CKEditor:CKEditorControl>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtannouncement" runat="server" ErrorMessage="Announcement Details Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                   </p>
                    
                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                        <tbody>
                            <tr>
                                <td style="width: 10%;"><img id="lmglogo" src ="../img/imageblank.png"   class="img-responsive" alt="img" runat="server" /></td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                      <asp:FileUpload class="btn btn-default" id="imageupload" runat="server" />
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Announcement Title</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txttitle" runat="server" class="form-control"  placeholder="Announcement Title" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="req1" ControlToValidate="txttitle" runat="server" ErrorMessage="Announcement Title Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td style="width: 10%;">Company</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <select multiple style="width:100%" class="select2" runat="server" id="cboCompanyID"   ></select>                                       
                                        
                                        
                                    </div>
                                </td>

                            </tr>
                            <tr style="display:none">
                                <td style="width: 10%;">Department</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <select multiple style="width:100%" class="select2" runat="server" id="cboDepartment" ></select>                                       
                                       
                                        
                                    </div>
                                </td>

                            </tr>
                             <tr>
                                <td style="width: 10%;">News expire After</td>
                                <td style="width: 10%">

                                    <div class="col-lg-8">
                                      
										<label class="class="input""> 
											<input type="text" name="txtExpiredDate" placeholder="News expire after" class="datepicker" data-dateformat='dd/mm/yy' runat="server" id="txtExpiredDate" style="height:30px" Font-Bold="true" ForeColor="Red">
										<i class="icon-append fa fa-calendar"></i>

										</label>
									
										 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtExpiredDate" runat="server" ErrorMessage="News expire after date Required" Display="Dynamic" Font-Bold="true" ForeColor="Red" ></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 10%;">Announcement From</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtannounceby" runat="server" class="form-control"  placeholder="Person Name/Department Name" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtannounceby" runat="server" ErrorMessage="Announcement From Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%"></td>
                </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%">
                        <div class="col-lg-4">
                            <button id="save" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  type="submit" onserverclick="cmdSave_Click">
                               <span style="width:80px">Save </span>   
                            </button>
                            <input action="action" type="button" class="btn bg-color-blueDark txt-color-white"   value="Cancel" style="width: 100px"  onclick="history.go(-1);" />   
                        </div>
                    </td>
                </tr>



                </tbody>
                        </table>
                 

            </div>










            <!-- end widget content -->

        </div>
    <!-- end widget div -->

    </div>

    </div>
</asp:Content>
