﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="Organization.aspx.cs" Inherits="LTS_CRM.Admin.Organization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                <li>Organization</li>
                <li>Organization Info</li>
            </ol>
            <!-- end breadcrumb -->

            <!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

        </div>

        <section id="Section1" class="">
            <div class="row">
                <div class="col-sm-12">

                    <div class="well">
                      <!-- <div class="row">
												
												<div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
													
														<img src="../img/logo_default.jpg" class="img-responsive" alt="img">
													
												</div>
												<div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right" >
													 <a class="btn bg-color-blueDark txt-color-white" href="OrganizationAddEdit.aspx">
													
														<i class="fa fa-plus"></i> <span class="hidden-mobile" >Edit </span>
													 </a>
                                                   

													  <button class="btn btn-success">
														<i class="fa fa-plus"></i> <span class="hidden-mobile">Export</span>
													</button>
												</div>
												
											</div>-->
                       
                      <p>
                        <img id="lmglogo" src ="../img/logo_default.jpg"   class="img-responsive" alt="img" runat="server" />
                       
                    </p>
                            
                        <p>
                            <div runat="server" id="txtorganizationdes" name="txtorganizationdes" style="width: 100%; height: 220px"></div>
                        </p>

                        <p>
                        </p>
                      
                        <table id="user" class="table table-bordered table-striped" style="clear: both">
                            <tbody>
                                <tr>
                                    <td style="width: 10%;">Organization</td>
                                    <td style="width: 40%">
                                        <asp:Label ID="lblOrganization" runat="server" Text="Label"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">Website</td>
                                    <td style="width: 40%"><asp:Label ID="lblWebsite" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">Total Employees</td>
                                    <td style="width: 40%"><asp:Label ID="lblemployee" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">Address</td>
                                    <td style="width: 40%">
                                        <address>
                                         
                                        <asp:Label ID="lbladdress" runat="server" Text=""></asp:Label>
                                          
                                        </address>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%;">Phone Number</td>
                                    <td style="width: 40%"><asp:Label ID="lblphoneno" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                     <td style="width: 10%;">Fax Number</td>
                                    <td style="width: 40%"><asp:Label ID="lblfaxno" runat="server" Text=""></asp:Label></td>
                                </tr>
                                 <tr>
                                     <td style="width: 10%;">City</td>
                                    <td style="width: 40%"><asp:Label ID="lblcity" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                     <td style="width: 10%;">State</td>
                                    <td style="width: 40%"><asp:Label ID="lblstate" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                     <td style="width: 10%;">Country</td>
                                    <td style="width: 40%"><asp:Label ID="lblcountry" runat="server" Text=""></asp:Label></td>
                                </tr>
                                
                               
                                 <tr>
                                     <td style="width: 10%;"></td>
                                    <td style="width: 40%"></td>
                                </tr>
                                <tr>
                                     <td style="width: 10%;"></td>
                                    <td style="width: 40%"><a class="btn bg-color-blueDark txt-color-white"  href="OrganizationEdit.aspx"> <span style="width:100px">Edit </span></a></td>
                                </tr>
                                
                                

                            </tbody>
                        </table>
                        

                         
                    </div>


                </div>
            </div>
        </section>
    
</asp:Content>
