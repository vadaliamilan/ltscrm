﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Admin
{
    public partial class AnnouncementAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboCompanyID();
                LoadcboDepartmentHead();

                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();

                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                    String sql = "select * from [dbo].[Announcement] where [ID] = @ID";

                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.Read())
                    {
                        txttitle.Text = Reader["AnnouncementTitle"].ToString();
                        cboCompanyID.Value = Reader["Company"].ToString();
                        cboDepartment.Value = ""; //Reader["Department"].ToString();
                        txtannounceby.Text = Reader["Announcementby"].ToString();
                        txtannouncement.Text = Reader["Description"].ToString();

                        String s;

                        s = Reader["ExpiredDate"].ToString();
                        DateTime dt = new DateTime();
                        dt = Convert.ToDateTime (s);
                        txtExpiredDate.Value = dt.ToString("dd/MM/yyyy");
                        
                        if (!Reader.IsDBNull(5))
                        {
                        Session.Add("imagesrc", Reader["Image"]);
                        byte[] img = (byte[])(Reader["Image"]);

                        TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                        Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(img);
                        string imgString = Convert.ToBase64String(img);
                        lmglogo.Src = String.Format("data:image/Bmp;base64,{0}\"", imgString);
                    }
                    }

                    Reader.Close();
                    Connection.Close();
                }
            }

        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            SqlCommand Command;

            String InsertSQL = "Insert into [dbo].[Announcement] ( [AnnouncementTitle], [Description], [Company], [Department], [Image], [ExpiredDate], [CreatedBy], [CreatedDate],[Announcementby] ) VALUES (  @AnnouncementTitle, @Description, @Company, @Department, @Image, @ExpiredDate, @CreatedBy, @CreatedDate,@Announcementby ) ";

            String UpdateSQL = "Update [dbo].[Announcement] set  [AnnouncementTitle] = @AnnouncementTitle, [Description] = @Description, [Company] = @Company, [Department] = @Department, [Image] = @Image, [ExpiredDate] = @ExpiredDate,  [UpdateBy] = @UpdateBy, [UpdatedDate] = @UpdatedDate,[Announcementby]=@Announcementby  where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {
                // 
                //Create a new Command object for inserting a new record. 
                Command = new SqlCommand(InsertSQL, Connection);
                // 
                //We are doing an insert 
                Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
            }
            else
            {
                // 
                //Set the command object with the update sql and connection. 
                Command = new SqlCommand(UpdateSQL, Connection);
                // 
                //Set the @ID field for updates. 
                Command.Parameters.AddWithValue("@ID", ID);
                Command.Parameters.AddWithValue("@UpdateBy", SessionValue("UserName"));
                Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

            }
            Command.Parameters.AddWithValue("@AnnouncementTitle", txttitle.Text);
            Command.Parameters.AddWithValue("@Description", txtannouncement.Text);
            Command.Parameters.AddWithValue("@Company", "");
            Command.Parameters.AddWithValue("@Department", "");
            Command.Parameters.AddWithValue("@Announcementby", txtannounceby.Text);
            
             //DateTime dt = Convert.ToDateTime(txtExpiredDate.Value.ToString("yyyy/MM/dd");  
            
            string res = txtExpiredDate.Value;
            DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //Console.WriteLine(d.ToString("MM/dd/yyyy"));
            
            Command.Parameters.AddWithValue("@ExpiredDate", d);
           


            FileUpload img = (FileUpload)imageupload;
            Byte[] imgByte = null;
            if (img.HasFile && img.PostedFile != null)
            {
                //To create a PostedFile
                HttpPostedFile File = imageupload.PostedFile;
                //Create byte Array with file len
                imgByte = new Byte[File.ContentLength];
                //force the control to load data in array
                File.InputStream.Read(imgByte, 0, File.ContentLength);
                Command.Parameters.AddWithValue("@Image", imgByte);
            }
            else
            {
                Command.CommandText = Command.CommandText.Replace(", [Image] = @Image", " ");
                Command.CommandText = Command.CommandText.Replace("[Image],", " ");
                Command.CommandText = Command.CommandText.Replace("@Image,", " ");
                
            }
            //Run the sql command against the database with no return values 
            // 
            //Run the statement ExecuteNonQuery returns no Results.
            Command.ExecuteNonQuery();

            //Closes the Connection to the back to the connection pool.
            Connection.Close();
            // 
            //Go to the Summary page 
            Response.Redirect("Announcement.aspx");

            // 
            //End event Save click 
        }


        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Announcement.aspx");
        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        protected void LoadcboCompanyID()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, CompanyName from [Company] order by CompanyName";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCompanyID.DataTextField = "CompanyName";
            cboCompanyID.DataValueField = "ID";
            cboCompanyID.DataSource = Command.ExecuteReader();
            cboCompanyID.DataBind();


            Connection.Close();
        }
       
        protected void LoadcboDepartmentHead()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

             
                 Connection.Open();
                 String sql = "select Distinct DepartmentName from Department ";
                 SqlCommand Command = new SqlCommand(sql, Connection);
                 cboDepartment.DataTextField = "DepartmentName";
                 cboDepartment.DataValueField = "DepartmentName";
                 cboDepartment.DataSource = Command.ExecuteReader();
                 cboDepartment.DataBind();


                 Connection.Close();
            
        }
    }
}