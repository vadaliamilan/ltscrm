﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LTS_CRM.master
{
    public partial class MasterAdmin : System.Web.UI.MasterPage
    {
        private Dictionary<string, HtmlGenericControl> ctrls = new Dictionary<string, HtmlGenericControl>();
        protected void Page_Load(object sender, EventArgs e)
        {
            setSelectedMenuItemClass();
        }
        public string GetActive()
        {
            
            return System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.Url.AbsolutePath).ToLower();

        }

        protected override void OnInit(EventArgs e)
        {
           //Organization
            ctrls.Add("Organization.aspx", Organization);
            ctrls.Add("Company.aspx", Office);
            ctrls.Add("EmployeeHandBook.aspx", EmployeeHandBook);
            ctrls.Add("UserEManual.aspx", UserEManual);
            ctrls.Add("Department.aspx", Department);
            ctrls.Add("Announcement.aspx", Announcement);
            ctrls.Add("ORGChart.aspx", ORGChart);
            ctrls.Add("ORGHie.aspx", ORGHie);
            //Online Form
            ctrls.Add("DocumentCategory.aspx", DocumentCategory);
            ctrls.Add("DocumentUpload.aspx", DocumentUpload);
            ctrls.Add("OnlineForm.aspx", OnlineForm);
            //Recruitment
            ctrls.Add("Channels.aspx", Channels);
            //ctrls.Add("JobDescription.aspx", JobDescription);
            //ctrls.Add("JobOpenings.aspx", JobOpenings);
            ctrls.Add("NewHireRequest.aspx", NewHireRequest);
            ctrls.Add("NewHirePendingRequest.aspx", NewHirePendingRequest);
           
            
            ctrls.Add("Candidates.aspx", Candidates);
            ctrls.Add("Interviews.aspx", Interviews);
            ctrls.Add("StaffCheckList.aspx", StaffCheckList);
            ctrls.Add("StaffCheckListOffer.aspx", StaffCheckListOffer);
            ctrls.Add("OfferManagement.aspx", OfferManagement);
            ctrls.Add("AppointmentLetter.aspx", AppointmentLetter);
            //ctrls.Add("NewStaff.aspx", NewStaff);
            ctrls.Add("NDALetter.aspx", NDALetter);

            //HR
            ctrls.Add("Employee.aspx", Employee);
            ctrls.Add("TripReport.aspx", TripReport);
        

            //Recruitment
            ctrls.Add("JobGrade.aspx", JobGrade);
            ctrls.Add("LeaveType.aspx", LeaveType);
            ctrls.Add("Holiday.aspx", Holiday);
            ctrls.Add("ApplyLeave.aspx", ApplyLeave);
            ctrls.Add("LeaveStatus.aspx", LeaveStatus);
            ctrls.Add("LeaveHistory.aspx", LeaveHistory);
            ctrls.Add("HODLeavePending.aspx", HODLeavePending);
            ctrls.Add("HODLeaveHistory.aspx", LeaveStatus);
            ctrls.Add("LeaveCalendar.aspx", LeaveCalendar);
            ctrls.Add("LeaveReport.aspx", LeaveReport);

            //Client
            ctrls.Add("Client.aspx", Client);

            //Supplier
            ctrls.Add("HotelWholseler.aspx", HotelWholseler );
            ctrls.Add("Coach.aspx", Coach);
            ctrls.Add("Ferry.aspx", Ferry);
            ctrls.Add("Flight.aspx", Flight);
            ctrls.Add("Hotel.aspx", Hotel);
            ctrls.Add("Insurance.aspx", Insurance);
            ctrls.Add("MealSuppliment.aspx", MealSuppliment);
            ctrls.Add("TheaterTicket.aspx", TheatreTicket);
            ctrls.Add("Train.aspx", Train);
            ctrls.Add("VanHire.aspx", VanHire);
            ctrls.Add("Tips.aspx", Tips);
            ctrls.Add("DMC.aspx", DMC);
            ctrls.Add("EntranceTicket.aspx", EntranceTicket);
            ctrls.Add("Restaurant.aspx", Resturant);
            ctrls.Add("TourGuide.aspx", SupplierTourGuide);


            //Marketing
            ctrls.Add("Channel.aspx", Channel);
            ctrls.Add("Project.aspx", Project);
            ctrls.Add("TaskList.aspx", TaskList);
            ctrls.Add("Campaign.aspx", Campaign);
            ctrls.Add("Appointment.aspx", AppointmentList);
            ctrls.Add("Contact.aspx", Contact);
            ctrls.Add("Lead.aspx", Lead);
            ctrls.Add("Schedular.aspx", MarketingSchedular);
            ctrls.Add("ExpenseRequest.aspx", MarketingExpenseRequest);
            ctrls.Add("ExpensePending.aspx", MarketingExpensePending);
            ctrls.Add("ExpenseHistory.aspx", MarketingExpenseHistory);
            ctrls.Add("GeneralCategoryManagement.aspx", GeneralCategoryManagement);

            //Sales
            ctrls.Add("SalesLeadManagement.aspx", SalesLead);
            ctrls.Add("SalesContactManagement.aspx", SalesContact);
            ctrls.Add("SalesAppointment.aspx", SalesAppointment);
            ctrls.Add("SalesTask.aspx", SalesTask);
            ctrls.Add("SalesSchedular.aspx", SalesSchedular);
            ctrls.Add("SalesExpenseRequest.aspx", SalesExpenseRequest);
            ctrls.Add("SalesExpensePending.aspx", SalesExpensePending);
            ctrls.Add("SalesExpenseHistory.aspx", SalesExpenseHistory);

            //Product
            ctrls.Add("Product.aspx", Product);
            ctrls.Add("ItineraryTemplate.aspx", ItineraryTemplate);
            ctrls.Add("Price.aspx", PriceManagement);

            //Operation
            ctrls.Add("LeadManagement.aspx", LeadManagement);
            ctrls.Add("ContactManagement.aspx", ContactManagement);
            ctrls.Add("Quotation.aspx", Quotation);
            ctrls.Add("Confirmation.aspx", Confirmation);
            ctrls.Add("BookingManagement.aspx", BookingManagement);
            ctrls.Add("TourGuides.aspx", TourGuides);
            ctrls.Add("OpinionSheet.aspx", OpinionSheet);
            ctrls.Add("Reminder.aspx", Reminder);
            ctrls.Add("Expense.aspx", Expense);
            ctrls.Add("Income.aspx", Income);
            ctrls.Add("ProfitLoss.aspx", ProfitLoss);
            base.OnInit(e);
        }

        /// <summary>
        /// Sets the selected menu item class.
        /// </summary>
        private void setSelectedMenuItemClass()
        {
            string requestedFile = Path.GetFileName(Request.Path);
            if (!string.IsNullOrEmpty(requestedFile))
            {
                requestedFile = requestedFile.Replace("AddEdit", "");
                requestedFile = requestedFile.Replace("Addedit", "");
                requestedFile = requestedFile.Replace("Edit", "");
                requestedFile = requestedFile.Replace("NewHirePendingView", "NewHirePendingRequest");
                foreach (KeyValuePair<string, HtmlGenericControl> ctrl in ctrls)
                {
                    HtmlGenericControl aCtrl = ctrl.Value;
                    aCtrl.Attributes.Remove("class");
                }

                HtmlGenericControl selectedMenuItem;
                if (ctrls.TryGetValue(requestedFile, out selectedMenuItem))
                    selectedMenuItem.Attributes.Add("class", "active");
            }
        }
    }
}