﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LTS_CRM.master
{
    public partial class Employee : System.Web.UI.MasterPage
    {
        private Dictionary<string, HtmlGenericControl> ctrls = new Dictionary<string, HtmlGenericControl>();
        

       
        protected void Page_Load(object sender, EventArgs e)
        {

            //string EMPID = "";
            //EMPID = SessionValue("EmployeeID").ToString();
            //if (EMPID.ToString().Length <= 0)
            //{
            //    Session.Remove("EmployeeID");
            //    Page.Response.Redirect("~/SignIn.aspx", true);
            //    return;
            //}
            setSelectedMenuItemClass();
        }
        public string GetActive()
        {

            return System.IO.Path.GetFileNameWithoutExtension(System.Web.HttpContext.Current.Request.Url.AbsolutePath).ToLower();

        }

        protected override void OnInit(EventArgs e)
        {
            //Organization
            ctrls.Add("Organization.aspx", Organization);           
            //ctrls.Add("Announcement.aspx", Announcement);
            ctrls.Add("ORGChart.aspx", ORGChart);
            ctrls.Add("ORGHie.aspx", ORGHie);
            ctrls.Add("EmployeeHandBook.aspx", EmployeeHandBook);
            ctrls.Add("UserEManual.aspx", UserEManual);
            //Online Form
            //ctrls.Add("DocumentCategory.aspx", DocumentCategory);
            //ctrls.Add("DocumentUpload.aspx", DocumentUpload);
            ctrls.Add("OnlineForm.aspx", OnlineForm);
            //Recruitment
            ctrls.Add("Channels.aspx", Channels);
            //ctrls.Add("JobDescription.aspx", JobDescription);
            //ctrls.Add("JobOpenings.aspx", JobOpenings);
            ctrls.Add("NewHireRequest.aspx", NewHireRequest);
            ctrls.Add("NewHirePendingRequest.aspx", NewHirePendingRequest);


            ctrls.Add("Candidates.aspx", Candidates);
            ctrls.Add("Interviews.aspx", Interviews);
            ctrls.Add("StaffCheckList.aspx", StaffCheckList);
            ctrls.Add("StaffCheckListOffer.aspx", StaffCheckListOffer);
            ctrls.Add("OfferManagement.aspx", OfferManagement);
            ctrls.Add("AppointmentLetter.aspx", AppointmentLetter);
            //ctrls.Add("NewStaff.aspx", NewStaff);
            ctrls.Add("NDALetter.aspx", NDALetter);

            //HR
            ctrls.Add("Employee.aspx", Emp);
            ctrls.Add("TripReport.aspx", TripReport);
            //ctrls.Add("EmployeeHandbook.aspx", EmployeeHandbook);

            //Recruitment
            ctrls.Add("JobGrade.aspx", JobGrade);
            ctrls.Add("LeaveType.aspx", LeaveType);
            ctrls.Add("Holiday.aspx", Holiday);
            ctrls.Add("ApplyLeave.aspx", ApplyLeave);
            ctrls.Add("LeaveStatus.aspx", LeaveStatus);
            ctrls.Add("LeaveHistory.aspx", LeaveHistory);
            ctrls.Add("HODLeavePending.aspx", HODLeavePending);
            ctrls.Add("HODLeaveHistory.aspx", LeaveStatus);
            ctrls.Add("LeaveCalendar.aspx", LeaveCalendar);
            ctrls.Add("LeaveReport.aspx", LeaveReport);

            base.OnInit(e);
        }

        /// <summary>
        /// Sets the selected menu item class.
        /// </summary>
        private void setSelectedMenuItemClass()
        {
            try
            {
                string requestedFile = Path.GetFileName(Request.Path);
                if (!string.IsNullOrEmpty(requestedFile))
                {
                    requestedFile = requestedFile.Replace("AddEdit", "");
                    requestedFile = requestedFile.Replace("Edit", "");
                    requestedFile = requestedFile.Replace("NewHirePendingView", "NewHirePendingRequest");
                    foreach (KeyValuePair<string, HtmlGenericControl> ctrl in ctrls)
                    {
                        HtmlGenericControl aCtrl = ctrl.Value;
                        aCtrl.Attributes.Remove("class");
                    }

                    HtmlGenericControl selectedMenuItem;
                    if (ctrls.TryGetValue(requestedFile, out selectedMenuItem))
                        selectedMenuItem.Attributes.Add("class", "active");
                }
            }
            catch
            {

            }
            finally
            { 
            
            }
        }


        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    }
}