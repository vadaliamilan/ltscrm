﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Recruitment
{
    public partial class CandidateAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboRequisitionCode();
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();
               

                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {


                    String sql = "select * from [dbo].[RecCandidate] where [ID] = @ID";
                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();
                    if (Reader.Read())
                    {

                        ListItem item = cboRequisitionCode.Items.FindByValue(Reader["RequisitionCode"].ToString());
                        cboRequisitionCode.SelectedIndex = cboRequisitionCode.Items.IndexOf(item);
                        cboRequisitionCode.Enabled = false;

                        txtFirstName.Text = Reader["FirstName"].ToString();
                        txtLastName.Text = Reader["LastName"].ToString();
                        txtEmail.Text = Reader["Email"].ToString();
                        txtContactNo.Text = Reader["ContactNo"].ToString();
                        txtSkillSet.Text = Reader["SkillSet"].ToString();
                        Session.Add("DocumentID", Reader["DocumentID"]);
                        if (SessionValue("DocumentID").ToString().Length <= 0)
                        {
                            downloadfile.Visible = false;
                        }
                        else
                        {
                            downloadfile.Visible = true;
                        }
                    }
                    
                    Reader.Close();
                    Connection.Close();
                    
                    
                    
                    
                   
                }
                else
                {
                    cboRequisitionCode.Enabled = true;
                    downloadfile.Visible = false;
                }
            }
            

        }


        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();



            //if (checkDuplicate() == true && ID == "")
            //{
            //    lblheader.Text = "Record Exist";
            //    lblmsg.Text = "This Candidate Name already exist.";

            //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            //}
            //else
            //{
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();

                SqlCommand Command;

                String InsertSQL = "Insert into [dbo].[RecCandidate] ( [RequisitionCode], [FirstName], [LastName], [Email], [ContactNo], [SkillSet], [DocumentID], [CreatedBy], [CreatedDate] ) VALUES ( @RequisitionCode, @FirstName, @LastName, @Email, @ContactNo, @SkillSet, @DocumentID, @CreatedBy, @CreatedDate ) ";

                String UpdateSQL = "Update [dbo].[RecCandidate] set  [RequisitionCode] = @RequisitionCode, [FirstName] = @FirstName, [LastName] = @LastName, [Email] = @Email, [ContactNo] = @ContactNo, [SkillSet] = @SkillSet, [DocumentID] = @DocumentID, [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate where [ID] = @ID ";

                if (String.IsNullOrEmpty(ID))
                {
                    // 
                    //Create a new Command object for inserting a new record. 
                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                }
                else
                {
                    // 
                    //Set the command object with the update sql and connection. 
                    Command = new SqlCommand(UpdateSQL, Connection);
                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@UpdateBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

                }
                
                Command.Parameters.AddWithValue("@RequisitionCode", cboRequisitionCode.SelectedValue);
                Command.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
                Command.Parameters.AddWithValue("@LastName", txtLastName.Text);
                Command.Parameters.AddWithValue("@Email", txtEmail.Text);
                Command.Parameters.AddWithValue("@ContactNo", txtContactNo.Text);
                Command.Parameters.AddWithValue("@SkillSet", txtSkillSet.Text);

                if (imageupload.HasFile && imageupload.PostedFile != null)
                {
                    int getid = DocumentUpload(imageupload);
                    if (getid > 0)
                    {
                        Command.Parameters.AddWithValue("@DocumentID", getid);
                    }
                    else
                    {
                        Command.Parameters.AddWithValue("@DocumentID", DBNull.Value);
                    }
                }
                else
                {
                    Command.Parameters.AddWithValue("@DocumentID", DBNull.Value);
                }

                Command.ExecuteNonQuery();
                Connection.Close();
                Response.Redirect("Candidates.aspx");
            //}
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("DocumentID").ToString();


            DataTable file = comman.GetAFile(Convert.ToInt16(ID));
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["FileType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }
        protected int DocumentUpload(FileUpload file)
        {
            string ID = SessionValue("DocumentID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            SqlCommand Command;

            String InsertSQL = "Insert into [dbo].[Document] (FileType,FileName,FileData) VALUES ( @FileType,@FileName,@FileData);SELECT CONVERT(int, SCOPE_IDENTITY()) ";

            String UpdateSQL = "Update [dbo].[Document] set [FileType] = @FileType, [FileName] = @FileName, [FileData] = @FileData where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {
                Command = new SqlCommand(InsertSQL, Connection);
                if (file.HasFile && file.PostedFile != null)
                {
                    HttpPostedFile File = file.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);


                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);

                    int newId = (int)Command.ExecuteScalar();
                    Connection.Close();
                    return newId;
                }
                else
                {
                    Connection.Close();
                    return 0;
                }
            }
            else
            {
                Command = new SqlCommand(UpdateSQL, Connection);
                if (file.HasFile && file.PostedFile != null)
                {
                    HttpPostedFile File = file.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteScalar();
                    Connection.Close();
                    return Convert.ToInt16(ID);
                }
                else
                {
                    Connection.Close();
                    return 0;
                }
            }




        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Candidates.aspx");
        }

        protected void LoadcboRequisitionCode()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select RequisitionCode,RequisitionCode + ' - ' + JobTitle as REQJOB from [RecNewHireRequest] order by RequisitionCode";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboRequisitionCode.DataTextField = "REQJOB";
            cboRequisitionCode.DataValueField = "RequisitionCode";
            cboRequisitionCode.DataSource = Command.ExecuteReader();
            cboRequisitionCode.DataBind();
            Connection.Close();
        }
        
        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }

        protected Boolean checkDuplicate()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            string strsql = "select * from [dbo].[RecInterview] where RequisitionCode=@ID And Email=@ID1";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            Command.Parameters.AddWithValue("@ID", cboRequisitionCode.SelectedValue);
            Command.Parameters.AddWithValue("@ID1", txtEmail.Text);
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
    }
}