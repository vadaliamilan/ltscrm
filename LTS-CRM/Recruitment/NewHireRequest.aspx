﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="NewHireRequest.aspx.cs" Inherits="LTS_CRM.Recruitment.NewHireRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Recruitment</li>
                <li>New Hire Request</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>New Hire Request </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">

                                    <div class="well well-lg">

                                        <h5>1) Please enter Position Name</h5>
                                        <h5>2) Please enter No of Position Require</h5>
                                        <h5>3) Please enter Department for position</h5>
                                        <h5>4) Please enter Department for position</h5>
                                    </div>
                                    <div>
                                        <br />
                                    </div>
                                    <div class="well well-lg">
                                        <table class="table table-striped table-bordered" width="100%">

                                            <tbody>
                                                <tr>
                                                    <td style="width: 10%;">Office</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblCompany" runat="server" Text="Let's Travel Services"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Department</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblDepartment" runat="server" Text="Operation"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 10%;">Reporting to</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblhod" runat="server" Text="Ms Rui"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Employee Name</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblEmployeeName" runat="server" Text="Chin Chan"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Apply Date</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblApplyDate" runat="server" Text="23/01/2017"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td colspan="3">
                                                        <div class="smart-form">


                                                            <fieldset>
                                                                <div class="row">
                                                                    <section class="col col-6">
                                                                        <label class="label">Position Name</label>
                                                                        <label class="input">

                                                                            <asp:TextBox ID="txtJobTitle" runat="server" class="form-control" placeholder="New Staff Position" Text=""></asp:TextBox>
                                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtJobTitle" runat="server" ErrorMessage="New Staff Position Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>

                                                                </div>
                                                                 <div class="row">
                                                                    <section class="col col-3">
                                                                        <label class="label">Position Type</label>
                                                                        <label class="input">
                                                                            <asp:DropDownList class="form-control" ID="cboEmploymentStatus" runat="server"></asp:DropDownList>
                                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ControlToValidate="cboEmploymentStatus" runat="server" ErrorMessage="Employment Status Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>
                                                                      <section class="col col-3">
                                                                        <label class="label">Position Priority</label>
                                                                        <label class="input">

                                                                            <asp:DropDownList class="form-control" ID="cboPriority" runat="server" >
                                                                                <asp:ListItem Value="High" Text="High"></asp:ListItem>
                                                                                 <asp:ListItem Value="Medium" Text="Medium"></asp:ListItem>
                                                                                <asp:ListItem Value="Low" Text="Low"></asp:ListItem>
                                                                            </asp:DropDownList>
                                        
                                                                        </label>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    
                                                                    <section class="col col-3">
                                                                        <label class="label">Position Require From</label>
                                                                        <label class="input">
                                                                            <i class="icon-append fa fa-calendar"></i>
                                                                            <input type="text" name="request" id="txtPositionReqDate" placeholder="Position Require From" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtPositionReqDate" runat="server" ErrorMessage="Position Require From date required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>

                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">No of People</label>
                                                                        <label class="input">

                                                                            <asp:TextBox ID="txtNoPosition" runat="server" class="form-control" placeholder="No Position"  onkeypress="if(isNaN(String.fromCharCode(event.keyCode))) return false;" Text=""></asp:TextBox>
                                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtNoPosition" runat="server" ErrorMessage="No of position required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-3">
                                                                        <label class="label">Department</label>
                                                                        <label class="input">
                                                                            <asp:DropDownList class="form-control" ID="cboDepartmentID" runat="server" OnSelectedIndexChanged = "Department_Changed" AutoPostBack = "true"></asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="cboDepartmentID" runat="server" ErrorMessage="Department Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        </label>
                                                                    </section>
                                                                    <section class="col col-3">
                                                                        <label class="label">Line Manager</label>
                                                                        <label class="input">
                                                                          <asp:DropDownList class="form-control" ID="cboDepartmentHead" runat="server"></asp:DropDownList>
                                                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="cboDepartmentHead" runat="server" ErrorMessage="Report To Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                        </label>
                                                                    </section>
                                                                </div>
                                                                <div class="row">
                                                                    <section class="col col-3">
                                                                        <label class="label">Reason for Hire</label>
                                                                        <label class="input">

                                                                            <asp:TextBox ID="txtreasonhire" runat="server" class="form-control" placeholder="Reason for Hire" Text="" TextMode="MultiLine" MaxLength="250"></asp:TextBox>

                                                                        </label>
                                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtreasonhire" runat="server" ErrorMessage="Please enter reason" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </section>                                                                   
                                                                    <section class="col col-3">
                                                                        <label class="label">Upload Job Description</label>
                                                                        <label class="input">
                                                                            <asp:FileUpload class="input" ID="imageupload" runat="server" />

                                                                        </label>
                                                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="imageupload" runat="server" ErrorMessage="Job Description Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </section>

                                                                </div>
                                                            </fieldset>

                                                            <footer>
                                                                <asp:Button ID="btnSave" runat="server" class="btn bg-color-blueDark txt-color-white" Text="Submit" style="width: 100px" OnClick="btnSave_Click" />                                                                
                                                             
                                                               
                                                                <button id="Button2" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px">
                                                                    <span style="width: 80px">Cancel </span>
                                                                </button>
                                                            </footer>


                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

      <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
