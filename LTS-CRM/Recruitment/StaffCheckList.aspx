﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="StaffCheckList.aspx.cs" Inherits="LTS_CRM.Recruitment.StaffCheckList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Recruitment</li>
                <li>Staff Check List</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Check List </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">
                                    <div class="row">
                                        <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                            <div class="input-group">
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                            <a id="A1" class="btn bg-color-blueDark txt-color-white" href="#" onserverclick="cmdAddNew_Click" runat="server">
                                                <i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span>
                                            </a>
                                           
                                        </div>

                                    </div>
                                    <asp:Repeater ID="MyRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                        <HeaderTemplate>
                                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">

                                                <thead>
                                                   
                                                    <tr>
                                                        <th data-hide="phone">No</th>                                                       
                                                        <th data-hide="phone">Staff Check List</th>
                                                       
                                                    

                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# DataBinder.Eval(Container.DataItem, "No")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "StaffCheckList")%></td>
                                                
                                                

                                                <td>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="getid" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnEdit_Click"><i class="fa fa-edit"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="btndel" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                </td>

                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

      <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
   
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                      <asp:Label ID="lblheader" runat="server" Text="Add-Edit Record"></asp:Label></h4>
                </div>
                <div class="modal-body">
                  <table class="table table-bordered table-striped">
                      <tr>
                          <td>
                              Description
                          </td>
                           <td>
                               <asp:TextBox ID="txtStaffCheckList" class="form-control" placeholder="Check List Description" runat="server"></asp:TextBox>
                          </td>
                      </tr>

                  </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <asp:Button ID="btnSave" CssClass="btn btn-default"  runat="server" Text="Save"
                    onclick="cmdSave_Click" />
                </div>
              </div>
     
            </div>
          </div>
</asp:Content>
