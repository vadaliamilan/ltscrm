﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Recruitment
{
    public partial class PositionAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboCompanyID();
                

                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();

                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                    String sql = "select * from [dbo].[Position] where [ID] = @ID";

                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.Read())
                    {
                        ListItem item = cboCompanyID.Items.FindByValue(Reader["CompanyID"].ToString());
                        cboCompanyID.SelectedIndex = cboCompanyID.Items.IndexOf(item);

                        txtPosition.Text = Reader["Position"].ToString();

                        ListItem item1 = cboDepartmentID.Items.FindByValue(Reader["DepartmentHead"].ToString());
                        cboDepartmentID.SelectedIndex = cboDepartmentID.Items.IndexOf(item1);
                        // txtCreatedBy.Text = Reader["CreatedBy"].ToString();
                        //txtCreatedDate.Text = Reader["CreatedDate"].ToString();
                        //txtUpdateBy.Text = Reader["UpdateBy"].ToString();
                        //txtUpdatedDate.Text = Reader["UpdatedDate"].ToString();
                    }

                    Reader.Close();
                    Connection.Close();
                }

            }

        }


        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();



            if (checkDuplicate() == true && ID == "")
            {
                lblheader.Text = "Record Exist";
                lblmsg.Text = "This Position Name exist with selected Company and Department.";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            else
            {
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();
                SqlCommand Command;
                //Declare string InsertSQL 
                String InsertSQL = "Insert into [dbo].[Position] ([CompanyID], [DepartmentID], [Position], [CreatedBy], [CreatedDate] ) VALUES ( @CompanyID, @DepartmentID, @Position, @CreatedBy, @CreatedDate ) ";

                //Declare String UpdateSQL 
                String UpdateSQL = "Update [dbo].[Department] set [CompanyID] = @CompanyID, [DepartmentID] = @DepartmentID, [Position] = @Position, [UpdateBy] = @UpdateBy, [UpdatedDate] = @UpdatedDate where [ID] = @ID ";

                if (String.IsNullOrEmpty(ID))
                {
                    // 
                    //Create a new Command object for inserting a new record. 
                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                }
                else
                {
                    // 
                    //Set the command object with the update sql and connection. 
                    Command = new SqlCommand(UpdateSQL, Connection);
                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@UpdateBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

                }
                Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
                Command.Parameters.AddWithValue("@DepartmentID", cboDepartmentID.SelectedValue);
                Command.Parameters.AddWithValue("@Position", txtPosition.Text);

                Command.ExecuteNonQuery();
                Connection.Close();
                Response.Redirect("Position.aspx");
            }
        }


        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Position.aspx");
        }

        protected void LoadcboCompanyID()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, CompanyName from [Company] order by CompanyName";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCompanyID.DataTextField = "CompanyName";
            cboCompanyID.DataValueField = "ID";
            cboCompanyID.DataSource = Command.ExecuteReader();
            cboCompanyID.DataBind();
            Connection.Close();
            LoadcboDepartmentID();
        }
        protected void Company_Changed(object sender, EventArgs e)
        {
            LoadcboDepartmentID();
        }
        protected void LoadcboDepartmentID()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, DepartmentName from [Department] where CompanyID='" + cboCompanyID.SelectedValue + "'"; ;
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboDepartmentID.DataTextField = "DepartmentName";
            cboDepartmentID.DataValueField = "ID";
            cboDepartmentID.DataSource = Command.ExecuteReader();
            cboDepartmentID.DataBind();
            Connection.Close();
        }

        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }

        protected Boolean checkDuplicate()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            string strsql = "select * from [dbo].[Position] where DepartmentID=@ID And CompanyID=@ID1 And Position=@ID2";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            Command.Parameters.AddWithValue("@ID", cboDepartmentID.SelectedValue);
            Command.Parameters.AddWithValue("@ID1", cboCompanyID.SelectedValue);
            Command.Parameters.AddWithValue("@ID2", txtPosition.Text);
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
    }
}