﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Globalization;

namespace LTS_CRM.Recruitment
{
    public partial class ChannelAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboCompanyID();
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();
                imageupload.AllowMultiple = false;
                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                   
                    
                    String sql = "select * from [dbo].[RecruitingChannel] where [ID] = @ID";

                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.Read())
                    {
                        
                        cboCompanyID.SelectedValue = Reader["CompanyID"].ToString();
                        txtCompanyName.Text = Reader["CompanyName"].ToString();
                        txtCompanyWebsite.Text = Reader["CompanyWebsite"].ToString();
                        txtUserName.Text = Reader["UserName"].ToString();
                        txtPassword.Text = Reader["Password"].ToString();
                        txtCompanyTelephoneNo.Text = Reader["CompanyTelephoneNo"].ToString();
                        txtAddress.Text = Reader["Address"].ToString();
                        txtPostcode.Text = Reader["Postcode"].ToString();
                        txtemail.Text = Reader["Email"].ToString();
                        txtCity.Text = Reader["City"].ToString();
                        txtCountry.Value  = Reader["Country"].ToString();
                        txtServiceType.SelectedValue = Reader["ServiceType"].ToString();
                        txtServiceFee.Text = Reader["ServiceFee"].ToString();
                        txtCredittoLTSL.Text = Reader["CredittoLTSL"].ToString();

                        if (Reader["BlackList"] == DBNull.Value)
                        {
                            blacklistno.Checked = true;
                            blacklistyes.Checked = false;
                        }
                        else
                        {
                            if (Reader["BlackList"].ToString() == "True")
                            {
                                blacklistno.Checked = false;
                                blacklistyes.Checked = true;
                            }
                            else
                            {
                                blacklistno.Checked = true ;
                                blacklistyes.Checked = false;
                            }
                           
                        }
                        txtRating.SelectedValue = Reader["Rating"].ToString();
                        txtMainBusiness.Text = Reader["MainBusiness"].ToString();
                        txtComment.Text = Reader["Comment"].ToString();
                        txtUseServiceDes.Text = Reader["UseServiceDes"].ToString();
                        String s;

                        s = Reader["ContractExpireDate"].ToString();
                        if (IsDate(s))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s);
                            txtContractExpireDate.Value = dt.ToString("dd/MM/yyyy");
                        }

                        
                        
                        Session.Add("DocumentID", Reader["DocumentUploadID"]);
                        if (SessionValue("DocumentID").ToString().Length <= 0)
                        {
                            downloadfile.Visible = false;
                        }
                        else
                        {
                            downloadfile.Visible = true;
                        }
                        Session.Add("UserName", "Nilesh");
                        
                        txtTitle.SelectedValue= Reader["Title"].ToString();
                        txtSurname.Text = Reader["Surname"].ToString();
                        txtFirstName.Text = Reader["FirstName"].ToString();
                        txtPosition.Text = Reader["Position"].ToString();
                        txtDepartment.Text = Reader["Department"].ToString();
                        txtContactEmail.Text = Reader["ContactEmail"].ToString();
                        txtTelephoneNo.Text = Reader["TelephoneNo"].ToString();
                        txtMobileNo.Text = Reader["MobileNo"].ToString();
                        txtFaxNo.Text = Reader["FaxNo"].ToString();
                        txtWeChatNo.Text = Reader["WeChatNo"].ToString();
                        txtWhatsup.Text = Reader["Whatsup"].ToString();
                        txtSkype.Text = Reader["Skype"].ToString();
                        txtFacebook.Text = Reader["Facebook"].ToString();
                        txtTwitter.Text = Reader["Twitter"].ToString();
                        txtLinkedIn.Text = Reader["LinkedIn"].ToString();
                        txtReminderBefor1.Text = Reader["ReminderBefor1"].ToString();
                        txtReminderBefor2.Text = Reader["ReminderBefor2"].ToString();
                        txtReminderBefor3.Text = Reader["ReminderBefor3"].ToString();
                       


                        //Session.Add("txtFileType", Reader["FileType"].ToString());
                        //Session.Add("txtFileName", Reader["FileName"].ToString());
                        //Session.Add("txtFileData", Reader["FileData"].ToString());


                    }

                    Reader.Close();
                    Connection.Close();
                }
                else
                {
                    imageupload.AllowMultiple = false;
                    downloadfile.Visible = false;
                }
            }

        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            SqlCommand Command;

            String InsertSQL = "Insert into [dbo].[RecruitingChannel] ([CompanyID], [CompanyName], [CompanyWebsite], [UserName], [Password], [CompanyTelephoneNo], [Address], [Postcode], [City], [Country], [ServiceType], [ServiceFee], [CredittoLTSL], [BlackList], [Rating], [MainBusiness], [Comment], [UseServiceDes], [ContractExpireDate], [RenewContract], [DocumentUploadID], [Title], [Surname], [FirstName], [Position], [Department], [Email], [TelephoneNo], [MobileNo], [FaxNo], [WeChatNo], [Whatsup], [Skype], [Facebook], [Twitter], [LinkedIn], [ReminderBefor1], [ReminderBefor2], [ReminderBefor3], [CreatedBy], [CreatedDate], [ContactEmail] ) VALUES ( @CompanyID, @CompanyName, @CompanyWebsite, @UserName, @Password, @CompanyTelephoneNo, @Address, @Postcode, @City, @Country, @ServiceType, @ServiceFee, @CredittoLTSL, @BlackList, @Rating, @MainBusiness, @Comment, @UseServiceDes, @ContractExpireDate, @RenewContract, @DocumentUploadID, @Title, @Surname, @FirstName, @Position, @Department, @Email, @TelephoneNo, @MobileNo, @FaxNo, @WeChatNo, @Whatsup, @Skype, @Facebook, @Twitter, @LinkedIn, @ReminderBefor1, @ReminderBefor2, @ReminderBefor3, @CreatedBy, @CreatedDate, @ContactEmail ) ";

            String UpdateSQL = "Update [dbo].[RecruitingChannel] set [CompanyID] = @CompanyID, [CompanyName] = @CompanyName, [CompanyWebsite] = @CompanyWebsite, [UserName] = @UserName, [Password] = @Password, [CompanyTelephoneNo] = @CompanyTelephoneNo, [Address] = @Address, [Postcode] = @Postcode, [City] = @City, [Country] = @Country, [ServiceType] = @ServiceType, [ServiceFee] = @ServiceFee, [CredittoLTSL] = @CredittoLTSL, [BlackList] = @BlackList, [Rating] = @Rating, [MainBusiness] = @MainBusiness, [Comment] = @Comment, [UseServiceDes] = @UseServiceDes, [ContractExpireDate] = @ContractExpireDate, [RenewContract] = @RenewContract, [DocumentUploadID] = @DocumentUploadID, [Title] = @Title, [Surname] = @Surname, [FirstName] = @FirstName, [Position] = @Position, [Department] = @Department, [Email] = @Email, [TelephoneNo] = @TelephoneNo, [MobileNo] = @MobileNo, [FaxNo] = @FaxNo, [WeChatNo] = @WeChatNo, [Whatsup] = @Whatsup, [Skype] = @Skype, [Facebook] = @Facebook, [Twitter] = @Twitter, [LinkedIn] = @LinkedIn, [ReminderBefor1] = @ReminderBefor1, [ReminderBefor2] = @ReminderBefor2, [ReminderBefor3] = @ReminderBefor3, [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {
                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
            }
            else
            {              
                Command = new SqlCommand(UpdateSQL, Connection);                
                Command.Parameters.AddWithValue("@ID", ID);
                Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);
            }
            Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
            if (imageupload.HasFile && imageupload.PostedFile != null)
            {
                int getid = DocumentUpload(imageupload);
                if (getid > 0)
                {
                    Command.Parameters.AddWithValue("@DocumentUploadID", getid);
                }
                else
                {
                    Command.Parameters.AddWithValue("@DocumentUploadID", DBNull.Value);
                }
            }
            else
            {
                string did = SessionValue("DocumentID");
                if (String.IsNullOrEmpty(did))
                {
                    Command.Parameters.AddWithValue("@DocumentUploadID", DBNull.Value);
                }
                else
                {
                    Command.Parameters.AddWithValue("@DocumentUploadID", did);
                }
            }


            Command.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
            Command.Parameters.AddWithValue("@CompanyWebsite", txtCompanyWebsite.Text);
            Command.Parameters.AddWithValue("@UserName", txtUserName.Text);
            Command.Parameters.AddWithValue("@Password", txtPassword.Text);
            Command.Parameters.AddWithValue("@CompanyTelephoneNo", txtCompanyTelephoneNo.Text);
            Command.Parameters.AddWithValue("@Email", txtemail.Text);
            Command.Parameters.AddWithValue("@ContactEmail", txtContactEmail.Text);
            Command.Parameters.AddWithValue("@Address", txtAddress.Text);
            Command.Parameters.AddWithValue("@Postcode", txtPostcode.Text);
            Command.Parameters.AddWithValue("@City", txtCity.Text);
            Command.Parameters.AddWithValue("@Country", txtCountry.Value );
            Command.Parameters.AddWithValue("@ServiceType", txtServiceType.Text);
            if (IsNumeric(txtServiceFee.Text))
            {
                Command.Parameters.AddWithValue("@ServiceFee", txtServiceFee.Text);
            }
            else
            {
                Command.Parameters.AddWithValue("@ServiceFee", DBNull.Value);
            }
            if (IsNumeric(txtCredittoLTSL.Text))
            {
                Command.Parameters.AddWithValue("@CredittoLTSL", txtCredittoLTSL.Text);
            }
            else
            {
                Command.Parameters.AddWithValue("@CredittoLTSL", DBNull.Value);
            }
            if (blacklistno.Checked==false )
            {
                Command.Parameters.AddWithValue("@BlackList", true);
            }
            else
            {
                Command.Parameters.AddWithValue("@BlackList", false);
            }
            
            if (IsNumeric(txtRating.Text))
            {
                Command.Parameters.AddWithValue("@Rating", txtRating.Text);
            }
            else
            {
                Command.Parameters.AddWithValue("@Rating", DBNull.Value);
            }

            Command.Parameters.AddWithValue("@MainBusiness", txtMainBusiness.Text);
            Command.Parameters.AddWithValue("@Comment", txtComment.Text);
            Command.Parameters.AddWithValue("@UseServiceDes", txtUseServiceDes.Text);
            string res = txtContractExpireDate.Value;
           
          
           
                if (res != "")
                {
                    DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Command.Parameters.AddWithValue("@ContractExpireDate", d);
                }
                else
                {
                    Command.Parameters.AddWithValue("@ContractExpireDate", DBNull.Value);
                }
           
            
            
           
           Command.Parameters.AddWithValue("@RenewContract", "yes");
            Command.Parameters.AddWithValue("@Title", txtTitle.Text);
            Command.Parameters.AddWithValue("@Surname", txtSurname.Text);
            Command.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            Command.Parameters.AddWithValue("@Position", txtPosition.Text);
            Command.Parameters.AddWithValue("@Department", txtDepartment.Text);
            
            Command.Parameters.AddWithValue("@TelephoneNo", txtTelephoneNo.Text);
            Command.Parameters.AddWithValue("@MobileNo", txtMobileNo.Text);
            Command.Parameters.AddWithValue("@FaxNo", txtFaxNo.Text);
            Command.Parameters.AddWithValue("@WeChatNo", txtWeChatNo.Text);
            Command.Parameters.AddWithValue("@Whatsup", txtWhatsup.Text);
            Command.Parameters.AddWithValue("@Skype", txtSkype.Text);
            Command.Parameters.AddWithValue("@Facebook", txtFacebook.Text);
            Command.Parameters.AddWithValue("@Twitter", txtTwitter.Text);
            Command.Parameters.AddWithValue("@LinkedIn", txtLinkedIn.Text);
            if (IsNumeric(txtReminderBefor1.Text))
            {
                Command.Parameters.AddWithValue("@ReminderBefor1", txtReminderBefor1.Text);
            }
            else
            {
                Command.Parameters.AddWithValue("@ReminderBefor1", DBNull.Value);
            }
            if (IsNumeric(txtReminderBefor2.Text))
            {
                Command.Parameters.AddWithValue("@ReminderBefor2", txtReminderBefor2.Text);
            }
            else
            {
                Command.Parameters.AddWithValue("@ReminderBefor2", DBNull.Value);
            }
            if (IsNumeric(txtReminderBefor3.Text))
            {
                Command.Parameters.AddWithValue("@ReminderBefor3", txtReminderBefor3.Text);
            }
            else
            {
                Command.Parameters.AddWithValue("@ReminderBefor3", DBNull.Value);
            }
            
            Command.ExecuteNonQuery();
            Connection.Close();
            Response.Redirect("Channels.aspx");
        }
        protected int DocumentUpload(FileUpload file)
        {
            string ID = SessionValue("DocumentID").ToString();
         
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            SqlCommand Command;

            String InsertSQL = "Insert into [dbo].[Document] (FileType,FileName,FileData) VALUES ( @FileType,@FileName,@FileData);SELECT CONVERT(int, SCOPE_IDENTITY()) ";

            String UpdateSQL = "Update [dbo].[Document] set [FileType] = @FileType, [FileName] = @FileName, [FileData] = @FileData where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {
               Command = new SqlCommand(InsertSQL, Connection);
               if (file.HasFile && file.PostedFile != null)
               {
                   HttpPostedFile File = file.PostedFile;

                   int size = File.ContentLength;
                   string name = File.FileName;
                   int position = name.LastIndexOf("\\");
                   name = name.Substring(position + 1);
                   string contentType = File.ContentType;
                   byte[] fileData = new byte[size];
                   File.InputStream.Read(fileData, 0, size);

                   
                   Command.Parameters.AddWithValue("@FileType", contentType);
                   Command.Parameters.AddWithValue("@FileName", name);
                   Command.Parameters.AddWithValue("@FileData", fileData);

                   int newId = (int)Command.ExecuteScalar();
                   Connection.Close();
                   return newId;
               }
               else 
               {
                   Connection.Close();
                   return 0;
               }
            }
            else
            {
                Command = new SqlCommand(UpdateSQL, Connection);
                if (file.HasFile && file.PostedFile != null)
                {
                    HttpPostedFile File = file.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteScalar();
                    Connection.Close();
                    return Convert.ToInt16(ID);
                }
                else
                {
                    Connection.Close();
                    return 0;
                }
            }

           

            
        }
        protected void cmdDownload_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("DocumentID").ToString();


            DataTable file = GetAFile(Convert.ToInt16(ID));
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["FileType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }
        // Get a file from the database by ID
        public static DataTable GetAFile(int id)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            DataTable file = new DataTable();

            Connection.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Connection;
            cmd.CommandTimeout = 0;

            cmd.CommandText = "SELECT ID, FileName, FileType, FileData FROM Document "
                + "WHERE ID=@ID";
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();

            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters["@ID"].Value = id;

            adapter.SelectCommand = cmd;
            adapter.Fill(file);

            Connection.Close();

            return file;
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Channels.aspx");
        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        protected void LoadcboCompanyID()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, CompanyName from [Company]  ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCompanyID.DataTextField = "CompanyName";
            cboCompanyID.DataValueField = "ID";
            cboCompanyID.DataSource = Command.ExecuteReader();
            cboCompanyID.DataBind();

            Connection.Close();
           
        }
       
        
    }
}