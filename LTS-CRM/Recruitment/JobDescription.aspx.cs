﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Recruitment
{
    public partial class JobDescription : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                BindGrid();
            }
        }
        protected void BindGrid()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            String sql = "  Select row_number() OVER (ORDER BY Jd.ID) as No,c.CompanyName, DepartmentName ,Jd.JobTitle,convert(varchar, JD.Date, 103) as Date,JD.PrepareBy,JD.ID From RecJobDescription JD Inner Join Department D ON JD.DepartmentID= d.ID Inner Join Company c ON d.CompanyID=c.ID Inner Join Employee e ON d.DepartmentHead=e.ID";
            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = sql;

            MyRepeater.DataSource = Command.ExecuteReader();
            MyRepeater.DataBind();

            Connection.Close();

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            Response.Redirect("JobDescriptionAddEdit.aspx");
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            String sql = "delete from [dbo].[RecJobDescription] where [ID] = @ID";

            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
            Command.ExecuteNonQuery();
            BindGrid();
        }
        protected void cmdAddNew_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = "";
            Response.Redirect("JobDescriptionAddEdit.aspx");
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    }
}