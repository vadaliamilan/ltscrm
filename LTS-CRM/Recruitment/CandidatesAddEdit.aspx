﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="CandidatesAddEdit.aspx.cs" Inherits="LTS_CRM.Recruitment.CandidateAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>


            <ol class="breadcrumb">
                <li>Recruitment</li>
                <li>Candidate Add-Edit</li>
            </ol>



        </div>

        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">

            <header>
                <h2>Candidate Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
            </div>

            <div class="widget-body">


                <div class="well">


                    <table id="user" class="table table-bordered table-striped" style="clear: both;" width="100%">

                        <tbody>
                            <tr>
                                <td style="width: 10%;">Requisition Code</td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:DropDownList class="form-control" ID="cboRequisitionCode" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="cboRequisitionCode" runat="server" ErrorMessage="Requisition Code Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 10%;">First Name</td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtFirstName" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="req1" ControlToValidate="txtFirstName" runat="server" ErrorMessage="First Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 10%;">Last Name</td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtLastName" runat="server" class="form-control" placeholder="Last Name" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtLastName" runat="server" ErrorMessage="Last Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Email</td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" placeholder="Email" Text=""></asp:TextBox>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Contact No.</td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtContactNo" runat="server" class="form-control" placeholder="Contact No" Text=""></asp:TextBox>


                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Skillset </td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtSkillSet" runat="server" class="form-control" placeholder="Skillset" Text="" TextMode="MultiLine" Height="70px" MaxLength="250"></asp:TextBox>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Upload Resume </td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:FileUpload class="input" ID="imageupload" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="downloadfile" runat="server" class="btn bg-color-blueDark txt-color-white" Text="Download" Style="width: 100px" OnClick="btnDownload_Click" />
                                                </td>
                                            </tr>
                                        </table>


                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;"></td>
                                <td style="width: 40%"></td>
                            </tr>
                            <tr>
                                <td style="width: 10%;"></td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <button id="save" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="cmdSave_Click">
                                            <span style="width: 80px">Save </span>
                                        </button>
                                        <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>



            </div>










            <!-- end widget content -->

        </div>
    <!-- end widget div -->

    </div>

    </div>

    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">
                                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                </div>
                            </div>

                        </div>
                    </div>
</asp:Content>
