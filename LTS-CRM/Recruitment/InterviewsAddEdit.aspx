﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="InterviewsAddEdit.aspx.cs" Inherits="LTS_CRM.Recruitment.InterviewAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>


            <ol class="breadcrumb">
                <li>Recruitment</li>
                <li>Interview Add-Edit</li>
            </ol>



        </div>

        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">

            <header>
                <h2>Interview Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
            </div>

            <div class="widget-body">


               


                    <table id="user" class="table table-bordered table-striped" style="clear: both;" width="100%">

                        <tbody>
                            <tr>
                                <td style="width: 10%;">Requisition Code</td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:DropDownList class="form-control" ID="cboRequisitionCode" OnSelectedIndexChanged = "RequisitionCode_Changed" AutoPostBack = "true" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="cboRequisitionCode" runat="server" ErrorMessage="Requisition Code Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>

                            <tr>
                                <td style="width: 10%;">Candidate Name</td>

                                <td style="width: 40%">
                                    <div class="col-lg-3">
                                        <asp:DropDownList class="form-control" ID="cboCandidate" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="cboCandidate" runat="server" ErrorMessage="Candidate Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>
                            </tr>
                             <tr>
                                <td style="width: 10%;">Interviewer Name</td>

                                <td style="width: 40%">
                                    <div class="col-lg-3">
                                        <asp:DropDownList class="form-control" ID="cboInterviewerName" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="cboInterviewerName" runat="server" ErrorMessage="Interviewer Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                           
                            <tr>
                                <td style="width: 10%;">Interview Date/Time</td>

                                <td style="width: 60%">
                                   <table>
                                       <tr>
                                           <td>&nbsp&nbsp&nbsp</td>
                                           <td>  <div class="input-group bootstrap-timepicker timepicker">
            <input type="text" name="txtInterviewDate" placeholder="Interview Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" id="txtInterviewDate" style="height:30px" Font-Bold="true" ForeColor="Red" />
            <span class="input-group-addon"><i class="icon-append fa fa-calendar"></i></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtInterviewDate" runat="server" ErrorMessage="Interview Date" Display="Dynamic" Font-Bold="true" ForeColor="Red" ></asp:RequiredFieldValidator>
        </div></td> <td>
                                             
                                           </td>
                                           <td>&nbsp&nbsp Start Time&nbsp</td>
                                           <td>  <div class="input-group bootstrap-timepicker timepicker">
            <input id="timepicker1" name="timepicker1" type="text" class="form-control input-small" ClientIDMode="static" runat="server"/>
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
        </div></td> <td>
                                               
                                           </td>
                                          <td>&nbsp&nbsp End Time&nbsp</td>
                                           <td>  <div class="input-group bootstrap-timepicker timepicker">
            <input id="timepicker2" name="timepicker2" type="text" class="form-control input-small" ClientIDMode="static" runat="server"/>
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
        </div></td><td>
                                               
                                           </td>
                                       </tr>
                                       <tr>
                                           
                                       </tr>
                                   </table>

                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Interview Type </td>

                                <td style="width: 40%">
                                    <div class="col-lg-3">
                                         <asp:DropDownList class="form-control" ID="cboInterviewType" runat="server">
                                             <asp:ListItem Text="In Person" Value="In Person" Selected="True"> </asp:ListItem>
                                             <asp:ListItem Text="Video" Value="Video"> </asp:ListItem>
                                             <asp:ListItem Text="Phone" Value="Phone"> </asp:ListItem>
                                         </asp:DropDownList>
                                       

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Interview Status </td>

                                <td style="width: 40%">
                                    <div class="col-lg-3">
                                         <asp:DropDownList class="form-control" ID="cboInterviewStatus" runat="server">
                                             <asp:ListItem Text="Scheduled" Value="Scheduled" Selected="True"> </asp:ListItem>
                                              <asp:ListItem Text="In Process" Value="In Process" > </asp:ListItem>
                                             <asp:ListItem Text="On Hold" Value="On Hold"> </asp:ListItem>
                                             <asp:ListItem Text="Completed" Value="Completed"> </asp:ListItem>
                                         </asp:DropDownList>
                                       


                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Result Status </td>

                                <td style="width: 40%">
                                    <div class="col-lg-3">
                                         <asp:DropDownList class="form-control" ID="cboStatus" runat="server">
                                             <asp:ListItem Text="Selected" Value="Selected"> </asp:ListItem>
                                             <asp:ListItem Text="Rejected" Value="Rejected"> </asp:ListItem>
                                             <asp:ListItem Text="Shortlisted" Value="Shortlisted"> </asp:ListItem>
                                             <asp:ListItem Text="No Status" Value="No Status" Selected="True"> </asp:ListItem>
                                         </asp:DropDownList>
                                       


                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Interview Feedback </td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtFeedback" runat="server" class="form-control" placeholder="Interview Feedback" Text="" TextMode="MultiLine" Height="70px" MaxLength="250"></asp:TextBox>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Interview Comment </td>

                                <td style="width: 40%">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtComment" runat="server" class="form-control" placeholder="Interview Comment" Text="" TextMode="MultiLine" Height="70px" MaxLength="250"></asp:TextBox>

                                    </div>
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="width: 10%;"></td>
                                <td style="width: 40%"></td>
                            </tr>
                            <tr>
                                <td style="width: 10%;"></td>

                                <td style="width: 40%">
                                    <div class="col-lg-3">
                                        <button id="save" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="cmdSave_Click">
                                            <span style="width: 80px">Save </span>
                                        </button>
                                        <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>



          










                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>

    </div>

    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
 
</asp:Content>
