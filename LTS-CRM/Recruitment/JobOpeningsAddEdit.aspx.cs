﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Recruitment
{
    public partial class JobOpeAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboCompanyID();
                LoadcboReportTo();
                LoadcboEmploymentStatus();
                LoadcboReportTo1();
                LoadcboReportTo2();
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();

                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                    String sql = "select * from [dbo].[RecJobOpening] where [ID] = @ID";

                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.Read())
                    {
                        ListItem item = cboCompanyID.Items.FindByValue(Reader["CompanyID"].ToString());
                        cboCompanyID.SelectedIndex = cboCompanyID.Items.IndexOf(item);

                        ListItem item1 = cboDepartmentHead.Items.FindByValue(Reader["ReportTo"].ToString());
                        cboDepartmentHead.SelectedIndex = cboDepartmentHead.Items.IndexOf(item1);

                        ListItem item2 = cboJobTitle.Items.FindByText(Reader["JobTitle"].ToString());
                        cboJobTitle.SelectedIndex = cboJobTitle.Items.IndexOf(item2);

                        ListItem item3 = cboApprover1.Items.FindByValue(Reader["Approver1"].ToString());
                        cboApprover1.SelectedIndex = cboApprover1.Items.IndexOf(item3);

                        ListItem item4 = cboApprover2.Items.FindByValue(Reader["Approver2"].ToString());
                        cboApprover2.SelectedIndex = cboApprover2.Items.IndexOf(item4);

                        ListItem item5 = cboEmploymentStatus.Items.FindByValue(Reader["EmploymentStatusID"].ToString());
                        cboEmploymentStatus.SelectedIndex = cboEmploymentStatus.Items.IndexOf(item5);

                        txtnoofposition.Text = Reader["NoPosition"].ToString();

                        //ListItem item2 = cboReportTo.Items.FindByValue(Reader["ReportTo"].ToString());
                        //cboReportTo.SelectedIndex = cboReportTo.Items.IndexOf(item2);      

                       string s = Reader["Date"].ToString();
                        if (IsDate(s))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s);
                            txtdate.Value = dt.ToString("dd/MM/yyyy");
                        }

                        txtPrepareBy.Text = Reader["PrepareBy"].ToString();
                        //txtJobTitle.Text = Reader["JobTitle"].ToString();
                        cboDepartmentID.SelectedValue = Reader["DepartmentID"].ToString();                        
                        txtLineManagmentResponsibility.Text = Reader["LineManagmentResponsibility"].ToString();
                        txtJobPurposeSummary.Text = Reader["JobPurposeSummary"].ToString();
                        txtKeyResponsibility.Text = Reader["KeyResponsibility"].ToString();
                        txtPersonSpecSkills.Text = Reader["PersonSpecSkills"].ToString();
                        txtRemuneration.Text = Reader["Remuneration"].ToString();
                        txtNote.Text = Reader["Note"].ToString();
                       
                    }

                    Reader.Close();
                    Connection.Close();
                }

            }

        }


        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();



            if (checkDuplicate() == true && ID == "")
            {
                lblheader.Text = "Record Exist";
                lblmsg.Text = "This Job offer record exist.";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            else
            {
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();
                SqlCommand Command;
                String InsertSQL = "Insert into [dbo].[RecJobOpening] ([CompanyID], [Date], [PrepareBy], [JobTitle], [DepartmentID], [ReportTo], [LineManagmentResponsibility], [JobPurposeSummary], [KeyResponsibility], [PersonSpecSkills], [Remuneration], [Note], [CreatedBy], [CreatedDate],[Approver1],[Approver2],EmploymentStatusID,NoPosition,RequisitionStatus ) VALUES ( @CompanyID, @Date, @PrepareBy, @JobTitle, @DepartmentID, @ReportTo, @LineManagmentResponsibility, @JobPurposeSummary, @KeyResponsibility, @PersonSpecSkills, @Remuneration, @Note, @CreatedBy, @CreatedDate,@Approver1,@Approver2,@EmploymentStatusID,@NoPosition,@RequisitionStatus ) ";

                String UpdateSQL = "Update [dbo].[RecJobOpening] set [CompanyID] = @CompanyID, [Date] = @Date, [PrepareBy] = @PrepareBy, [JobTitle] = @JobTitle, [DepartmentID] = @DepartmentID, [ReportTo] = @ReportTo, [LineManagmentResponsibility] = @LineManagmentResponsibility, [JobPurposeSummary] = @JobPurposeSummary, [KeyResponsibility] = @KeyResponsibility, [PersonSpecSkills] = @PersonSpecSkills, [Remuneration] = @Remuneration, [Note] = @Note, [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate,[Approver1]=@Approver1,[Approver2]=@Approver2,EmploymentStatusID=@EmploymentStatusID,NoPosition=@NoPosition,RequisitionStatus=@RequisitionStatus  where [ID] = @ID ";

                if (String.IsNullOrEmpty(ID))
                {
                    // 
                    //Create a new Command object for inserting a new record. 
                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                }
                else
                {
                    // 
                    //Set the command object with the update sql and connection. 
                    Command = new SqlCommand(UpdateSQL, Connection);
                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

                }
                Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
                Command.Parameters.AddWithValue("@DepartmentID", cboDepartmentID.SelectedValue );
                Command.Parameters.AddWithValue("@ReportTo", cboDepartmentHead.SelectedValue);
                Command.Parameters.AddWithValue("@Approver1", cboApprover1.SelectedValue);
                Command.Parameters.AddWithValue("@Approver2", cboApprover2.SelectedValue);
                Command.Parameters.AddWithValue("@EmploymentStatusID", cboEmploymentStatus.SelectedValue);
                Command.Parameters.AddWithValue("@NoPosition", txtnoofposition.Text);
                Command.Parameters.AddWithValue("@RequisitionStatus", 1);
                string res = txtdate.Value;
                if (res != "")
                {
                    DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Command.Parameters.AddWithValue("@Date", d);
                }
                else
                {
                    Command.Parameters.AddWithValue("@Date", DBNull.Value);
                }
                Command.Parameters.AddWithValue("@PrepareBy", txtPrepareBy.Text);
                Command.Parameters.AddWithValue("@JobTitle", cboJobTitle.SelectedItem.Text);   
                Command.Parameters.AddWithValue("@LineManagmentResponsibility", txtLineManagmentResponsibility.Text);
                Command.Parameters.AddWithValue("@JobPurposeSummary", txtJobPurposeSummary.Text);
                Command.Parameters.AddWithValue("@KeyResponsibility", txtKeyResponsibility.Text);
                Command.Parameters.AddWithValue("@PersonSpecSkills", txtPersonSpecSkills.Text);
                Command.Parameters.AddWithValue("@Remuneration", txtRemuneration.Text);
                Command.Parameters.AddWithValue("@Note", txtNote.Text); 
                Command.ExecuteNonQuery();
                Connection.Close();
                Response.Redirect("JobOpenings.aspx");
            }
        }


        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("JobOpenings.aspx");
        }

        protected void LoadcboCompanyID()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, CompanyName from [Company] order by CompanyName";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCompanyID.DataTextField = "CompanyName";
            cboCompanyID.DataValueField = "ID";
            cboCompanyID.DataSource = Command.ExecuteReader();
            cboCompanyID.DataBind();
            Connection.Close();
            LoadcboDepartmentID();
        }
        protected void Company_Changed(object sender, EventArgs e)
        {
            LoadcboDepartmentID();
        }
        protected void LoadcboDepartmentID()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, DepartmentName from [Department] where CompanyID='" + cboCompanyID.SelectedValue + "'"; ;
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboDepartmentID.DataTextField = "DepartmentName";
            cboDepartmentID.DataValueField = "ID";
            cboDepartmentID.DataSource = Command.ExecuteReader();
            cboDepartmentID.DataBind();
            Connection.Close();
            LoadcboDepartmentHead();
            txtJobTitle();
        }
        protected void Department_Changed(object sender, EventArgs e)
        {
            LoadcboDepartmentHead();
            txtJobTitle();
        }
        protected void LoadcboDepartmentHead()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select E.ID, E.FirstName from [Employee] E Inner Join Department D On E.ID=D.DepartmentHead where  D.ID='" + cboDepartmentID.SelectedValue + "'";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboDepartmentHead.DataTextField = "FirstName";
            cboDepartmentHead.DataValueField = "ID";
            cboDepartmentHead.DataSource = Command.ExecuteReader();
            cboDepartmentHead.DataBind();
            Connection.Close();
         
        }
        protected void LoadcboEmploymentStatus()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, EmploymentStatus from [EmploymentStatus] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboEmploymentStatus.DataTextField = "EmploymentStatus";
            cboEmploymentStatus.DataValueField = "ID";
            cboEmploymentStatus.DataSource = Command.ExecuteReader();
            cboEmploymentStatus.DataBind();
            Connection.Close();

        }
        protected void LoadcboReportTo()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, FirstName from [Employee] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboReportTo.DataTextField = "FirstName";
            cboReportTo.DataValueField = "ID";
            cboReportTo.DataSource = Command.ExecuteReader();
            cboReportTo.DataBind();
            Connection.Close();
           
           
        }

        protected void LoadcboReportTo1()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, FirstName from [Employee] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboApprover1.DataTextField = "FirstName";
            cboApprover1.DataValueField = "ID";
            cboApprover1.DataSource = Command.ExecuteReader();
            cboApprover1.DataBind();
            Connection.Close();

           
        }
        protected void LoadcboReportTo2()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, FirstName from [Employee] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboApprover2.DataTextField = "FirstName";
            cboApprover2.DataValueField = "ID";
            cboApprover2.DataSource = Command.ExecuteReader();
            cboApprover2.DataBind();
            Connection.Close();
        }
        protected void txtJobTitle()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, Position from [Position] where CompanyID='" + cboCompanyID.SelectedValue + "' And DepartmentID='" + cboDepartmentID.SelectedValue + "'"; 
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboJobTitle.DataTextField = "Position";
            cboJobTitle.DataValueField = "ID";
            cboJobTitle.DataSource = Command.ExecuteReader();
            cboJobTitle.DataBind();
            Connection.Close();
            
        }
        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }

        protected Boolean checkDuplicate()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            string strsql = "select * from [dbo].[RecJobOpening] where DepartmentID=@ID And CompanyID=@ID1 And JobTitle=@JobTitle";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            Command.Parameters.AddWithValue("@ID", cboDepartmentID.SelectedValue );
            Command.Parameters.AddWithValue("@ID1", cboCompanyID.SelectedValue);
            Command.Parameters.AddWithValue("@JobTitle",cboJobTitle.SelectedItem.Text);
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
    }
}