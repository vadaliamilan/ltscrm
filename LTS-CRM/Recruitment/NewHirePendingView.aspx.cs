﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Recruitment
{
    public partial class NewHirePendingView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                Session.Add("CompanyID", "1");

               

                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();

                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                    String sql = "SELECT R.[ID],convert(varchar, R.[Date], 103) as JobRequireDate,R.[RequisitionCode],R.[JobTitle],D.DepartmentName,ReportTo.FirstName as ReportTo,R.ApplicationPriority,ES.EmploymentStatus,R.[DocumentID],convert(varchar, R.[CreatedBy], 103) as [CreatedBy],R.[CreatedDate],RES.[RequisitionStatus],R.NoPosition FROM [RecNewHireRequest] R Inner Join Department D ON R.[DepartmentID]=D.ID Inner Join Employee ReportTo ON R.ReportTo=ReportTo.ID Inner Join EmploymentStatus ES ON R.[EmploymentStatusID]=ES.ID  Inner Join [RecRequisitionStatus] RES ON R.RequisitionStatus=RES.ID where R.[ID] = @ID";
                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();
                    if (Reader.Read())
                    {

                        cboEmploymentStatus.Text = Reader["EmploymentStatus"].ToString();
                        cboDepartmentHead.Text = Reader["ReportTo"].ToString();
                        cboDepartmentID.Text = Reader["DepartmentName"].ToString();
                        txtRequisitionCode.Text = Reader["RequisitionCode"].ToString();
                        txtJobTitle.Text = Reader["JobTitle"].ToString();
                        txtNoPosition.Text = Reader["NoPosition"].ToString();
                        lblEmployeeName.Text = Reader["CreatedBy"].ToString();
                        cboPriority.Text = Reader["ApplicationPriority"].ToString();
                        
                        Session.Add("DocumentID", Reader["DocumentID"]);
                        if (SessionValue("DocumentID").ToString().Length <= 0)
                        {
                            downloadfile.Visible = false;
                        }
                        else
                        {
                            downloadfile.Visible = true;
                        }

                        txtPositionReqDate.Text = Reader["JobRequireDate"].ToString();                          
                        lblApplyDate.Text = Reader["CreatedDate"].ToString();
                        
                    }
                    Reader.Close();
                    Connection.Close();
                }
                else
                {
                    Response.Redirect("NewHirePendingRequest.aspx");
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            ApproveReject("Approved");
           

        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            ApproveReject("Rejected");

        }

        protected void ApproveReject(string status)
        {
            string ID = SessionValue("AutoID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            SqlCommand Command;
            // String InsertSQL = "Insert into [dbo].[RecNewHireRequest] ([CompanyID], [Date], [RequisitionCode], [JobTitle], [DepartmentID], [ReportTo], [NoPosition],[EmploymentStatusID], [ApplicationPriority], [RequisitionStatus], [DocumentID], [CreatedBy], [CreatedDate]) VALUES ( @CompanyID, @Date, @RequisitionCode, @JobTitle, @DepartmentID, @ReportTo, @NoPosition, @EmploymentStatusID, @ApplicationPriority, @RequisitionStatus, @DocumentID, @CreatedBy, @CreatedDate ) ";

            String UpdateSQL = "Update [dbo].[RecNewHireRequest] set Approver1=@Approver1,Approver1Status=@Approver1Status,Approver1Reason=@Approver1Reason,[UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate where [ID] = @ID ";



            Command = new SqlCommand(UpdateSQL, Connection);
            Command.Parameters.AddWithValue("@Approver1", SessionValue("UserName"));
            Command.Parameters.AddWithValue("@Approver1Status", status);
            Command.Parameters.AddWithValue("@Approver1Reason", txtreasonhire.Text);
            Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
            Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

            Command.Parameters.AddWithValue("@ID", ID);
            Command.Parameters.AddWithValue("@RequisitionStatus", 1);


            Command.ExecuteNonQuery();
            Connection.Close();
            Response.Redirect("NewHirePendingRequest.aspx");
        }



        protected void btnDownload_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("DocumentID").ToString();


            DataTable file = comman.GetAFile(Convert.ToInt16(ID));
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["FileType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }
        protected int DocumentUpload(FileUpload file)
        {
            string ID = SessionValue("DocumentID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            SqlCommand Command;

            String InsertSQL = "Insert into [dbo].[Document] (FileType,FileName,FileData) VALUES ( @FileType,@FileName,@FileData);SELECT CONVERT(int, SCOPE_IDENTITY()) ";

            String UpdateSQL = "Update [dbo].[Document] set [FileType] = @FileType, [FileName] = @FileName, [FileData] = @FileData where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {
                Command = new SqlCommand(InsertSQL, Connection);
                if (file.HasFile && file.PostedFile != null)
                {
                    HttpPostedFile File = file.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);


                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);

                    int newId = (int)Command.ExecuteScalar();
                    Connection.Close();
                    return newId;
                }
                else
                {
                    Connection.Close();
                    return 0;
                }
            }
            else
            {
                Command = new SqlCommand(UpdateSQL, Connection);
                if (file.HasFile && file.PostedFile != null)
                {
                    HttpPostedFile File = file.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteScalar();
                    Connection.Close();
                    return Convert.ToInt16(ID);
                }
                else
                {
                    Connection.Close();
                    return 0;
                }
            }




        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }

        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    }
}