﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="NewHirePendingView.aspx.cs" Inherits="LTS_CRM.Recruitment.NewHirePendingView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:HiddenField ID="lblcompanyid" runat="server"   />
      
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Recruitment</li>
                <li>New Hire Request</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>New Hire Request </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">

                                   
                                    
                                        <table class="table table-striped table-bordered" width="100%">

                                            <tbody>
                                                <tr>
                                                    <td style="width: 10%;">Office</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblCompany" runat="server" Text="Let's Travel Services"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Department</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblDepartment" runat="server" Text="Operation"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 10%;">Approver Name</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblhod" runat="server" Text="Ms Rui"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Request By</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblEmployeeName" runat="server" Text="Chin Chan"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Request Date</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="lblApplyDate" runat="server" Text="23/01/2017"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 10%;">Requisition Code </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="txtRequisitionCode" runat="server" Text="23/01/2017"></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 10%;">Position Name </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="txtJobTitle" runat="server" Text=""></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>

                                                 <tr>
                                                    <td style="width: 10%;">Position Type </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="cboEmploymentStatus" runat="server" Text=""></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Position Priority </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                             <asp:Label ForeColor="Black" ID="cboPriority" runat="server" Text=""></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 10%;">Position Require From </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                             <asp:Label ForeColor="Black" ID="txtPositionReqDate" runat="server" Text=""></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">No of Position </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="txtNoPosition" runat="server" Text=""></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 10%;">Department </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                          <asp:Label ForeColor="Black" ID="cboDepartmentID" runat="server" Text=""></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td style="width: 10%;">Line Manager </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:Label ForeColor="Black" ID="cboDepartmentHead" runat="server" Text=""></asp:Label>

                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                 <tr>
                                                    <td style="width: 10%;">Job Description </td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                             <asp:Button ID="downloadfile" runat="server" class="btn bg-color-blueDark txt-color-white" Text="Download" style="width: 100px" OnClick="btnDownload_Click" />                                 

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td colspan="3">
                                                        <div class="smart-form">


                                                            <fieldset>
                                                               
                                                                <div class="row">
                                                                    <section class="col col-6">
                                                                        <label class="label">Reason</label>
                                                                        <label class="input">
                                                                            
                                                                           <asp:TextBox ID="txtreasonhire" runat="server" class="form-control" placeholder="Reason for Approve/Reject" Text="" TextMode="MultiLine" Height="70px" MaxLength="250"></asp:TextBox>

                                                                        </label>
                                                                        
                                                                    </section>                                                                   
                                                       
                                                                  
                                                                </div>
                                                            </fieldset>

                                                            <footer>
                                                                 <button id="btncancel" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px">
                                                                    <span style="width: 80px">Cancel </span>
                                                                </button>
                                                                 <asp:Button ID="btnReject" runat="server" class="btn bg-color-blueDark txt-color-white" Text="Reject" style="width: 100px" OnClick="btnReject_Click" />   
                                                                <asp:Button ID="btnSave" runat="server" class="btn bg-color-blueDark txt-color-white" Text="Approve" style="width: 100px" OnClick="btnSave_Click" />                                                                
                                                               
                                                               
                                                               
                                                            </footer>


                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
   

      <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
