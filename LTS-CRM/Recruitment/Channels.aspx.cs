﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Recruitment
{
    public partial class Channels : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                BindGrid();
            }
        }
        protected void BindGrid()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            String sql = "  Select row_number() OVER (ORDER BY RC.CompanyName) as No, RC.CompanyName as ChannelName ,C.CompanyName,CompanyWebsite as Website,RC.Email,RC.UserName,Rc.Password,  convert(varchar, RC.ContractExpireDate, 103) as ExpiryDate,RC.ID From RecruitingChannel  RC Inner Join Company  C ON RC.CompanyID=C.ID ";
            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = sql;

            MyRepeater.DataSource = Command.ExecuteReader();
            MyRepeater.DataBind();

            Connection.Close();

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            Response.Redirect("ChannelsAddEdit.aspx");
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            String sql = "delete from [dbo].[RecruitingChannel] where [ID] = @ID";

            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
            Command.ExecuteNonQuery();
            BindGrid();
        }
        protected void cmdAddNew_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = "";
            Response.Redirect("ChannelsAddEdit.aspx");
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    }
}