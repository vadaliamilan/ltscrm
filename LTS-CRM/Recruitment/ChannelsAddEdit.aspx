﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="ChannelsAddEdit.aspx.cs" Inherits="LTS_CRM.Recruitment.ChannelAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




    <div id="ribbon">

        <span class="ribbon-button-alignment">
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>


        <ol class="breadcrumb">
            <li>Recruitment</li>
            <li>Chennel </li>
        </ol>



    </div>

    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">

        <header>
            <h2>Chennel Add-Edit</h2>
        </header>
        <div class="widget-body">

            <div id="checkout-form1" class="smart-form" >
                <div class="col-sm-12" >
                    <fieldset >
                       
                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="cboCompanyID" runat="server" ErrorMessage="Company Name Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                </label>
                            </section>
                           
                        </div>
                         <div class="row">
                            <section class="col col-12">
                                <h3 class="text-primary">Company Information</h3>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa  fa-bank"></i>
                                    <asp:TextBox ID="txtCompanyName" runat="server" class="form-control" placeholder="Channel Company Name" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtCompanyName" runat="server" ErrorMessage="Channel Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-globe"></i>
                                    <asp:TextBox ID="txtCompanyWebsite" runat="server" class="form-control" placeholder="Website Address" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtCompanyWebsite" runat="server" ErrorMessage="Website Address Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-user"></i>
                                    <asp:TextBox ID="txtUserName" runat="server" class="form-control" placeholder="User Name"></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-key"></i>
                                    <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Password" Text=""></asp:TextBox>

                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-building"></i>
                                    <asp:TextBox ID="txtAddress" runat="server" class="form-control" placeholder="Channel Company Address" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtAddress" runat="server" ErrorMessage="Channel Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-inbox"></i>
                                    <asp:TextBox ID="txtPostcode" runat="server" class="form-control" placeholder="Postcode" Text=""></asp:TextBox>
                                   
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-cubes"></i>
                                    <asp:TextBox ID="txtCity" runat="server" class="form-control" placeholder="City Name"></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                   
                                    <select id="txtCountry" class="form-control"  runat="server" required >

                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antartica">Antarctica</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">Bouvet Island</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">Christmas Island</option>
                                    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo">Congo, the Democratic Republic of the</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                    <option value="Croatia">Croatia (Hrvatska)</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="East Timor">East Timor</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="France Metropolitan">France, Metropolitan</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="French Southern Territories">French Southern Territories</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                    <option value="Holy See">Holy See (Vatican City State)</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran">Iran (Islamic Republic of)</option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                    <option value="Korea">Korea, Republic of</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Lao">Lao People's Democratic Republic</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macau">Macau</option>
                                    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">Marshall Islands</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia">Micronesia, Federated States of</option>
                                    <option value="Moldova">Moldova, Republic of</option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                    <option value="New Caledonia">New Caledonia</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">Norfolk Island</option>
                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russia">Russian Federation</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                    <option value="Saint LUCIA">Saint LUCIA</option>
                                    <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                    <option value="Samoa">Samoa</option>
                                    <option value="San Marino">San Marino</option>
                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                    <option value="Span">Spain</option>
                                    <option value="SriLanka">Sri Lanka</option>
                                    <option value="St. Helena">St. Helena</option>
                                    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Syria">Syrian Arab Republic</option>
                                    <option value="Taiwan">Taiwan, Province of China</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania">Tanzania, United Republic of</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom" selected>United Kingdom</option>
                                    <option value="United States">United States</option>
                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Vietnam">Viet Nam</option>
                                    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                    <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                    <option value="Western Sahara">Western Sahara</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Yugoslavia">Yugoslavia</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>



                                </select>
                                     
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-envelope-o"></i>

                                    <asp:TextBox ID="txtemail" runat="server" class="form-control" placeholder="Email Address" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtemail" runat="server" ErrorMessage="Email Address Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator   id="regEmail" ControlToValidate="txtEmail"    Text="Invalid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"   Runat="server" Font-Bold="true" ForeColor="Red"/> 
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-phone"></i>
                                    <asp:TextBox ID="txtCompanyTelephoneNo" runat="server" class="form-control" placeholder="Phone Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-2">
                                <label class="input">

                                    <asp:DropDownList class="form-control" ID="txtServiceType" runat="server">
                                        <asp:ListItem Value="Free" Text="Free" Selected="True">Free</asp:ListItem>
                                        <asp:ListItem Value="With Fees" Text="With Fees"></asp:ListItem>
                                    </asp:DropDownList>
                                </label>
                            </section>
                            <section class="col col-2">
                                <label class="input">
                                    <i class="icon-prepend fa fa-money"></i>
                                    <asp:TextBox ID="txtServiceFee" runat="server" class="form-control" placeholder="Service Fees" Text=""></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="revNumber" runat="server" ControlToValidate="txtServiceFee"
           ErrorMessage="Please enter only numbers like 100 or 100.00" ValidationExpression="^\d+(\.\d\d)?$" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                </label>
                            </section>
                            <section class="col col-2">
                                <label class="input">

                                    <i class="icon-prepend fa fa-money"></i>
                                    <asp:TextBox ID="txtCredittoLTSL" runat="server" class="form-control" placeholder="Credit to LTSL" Text=""></asp:TextBox>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCredittoLTSL"
           ErrorMessage="Please enter only numbers like 100 or 100.00" ValidationExpression="^\d+(\.\d\d)?$" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                </label>
                            </section>
                        </div>


                        <div class="row">
                            
                            <section class="col col-4">
                                <label class="textarea">
                                    <table>
                                <tr>
                                     <tb>Main Business</tb>
                                    <tb><CKEditor:CKEditorControl ID="txtMainBusiness" BasePath="~/ckeditor" runat="server"  Height="200" placeholder="Enter Main Business" ToolbarStartupExpanded="False">
		            </CKEditor:CKEditorControl></tb>
                                    
                                </tr>
                               
                            </table>
                                     
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="textarea">
                                     <table>
                                <tr>
                                    <tb>Comment</tb>
                                     <tb><CKEditor:CKEditorControl ID="txtComment" BasePath="~/ckeditor" runat="server"  Height="200" placeholder="Enter Comment" ToolbarStartupExpanded="False">
		                          </CKEditor:CKEditorControl></tb>
                                </tr>
                               
                            </table>
                                     
                                </label>
                            </section>
                            <section class="col col-4">
                                <label class="textarea">
                                    <table>
                                <tr>
                                    <tb>How to use Service</tb>
                                     <tb> <CKEditor:CKEditorControl ID="txtUseServiceDes" BasePath="~/ckeditor" runat="server"  Height="200" placeholder="How to use Service" ToolbarStartupExpanded="False">
		                          </CKEditor:CKEditorControl></tb>
                                </tr>
                               
                            </table>
                                  
                                </label>
                            </section>

                        </div>
                    </fieldset>


                    <fieldset>

                        <div class="row">
                            <section class="col col-12">
                                <h3 class="text-primary">Contract Information</h3>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-2">
										<label class="input"> <i class="icon-append fa fa-calendar"></i>
											<input type="text" name="request" id="txtContractExpireDate" placeholder="Request activation on" class="datepicker" data-dateformat='dd/mm/yy' runat="server"/>
										</label>
									</section>
                            <section class="col col-3">
                                <label class="input">
                                    <asp:FileUpload class="input" ID="imageupload" runat="server" />

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    
                                     <button id="downloadfile" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  type="submit" onserverclick="cmdDownload_Click">
                                       <span >Download File </span>   
                                    </button>  
                                </label>
                            </section>
                             
                            <section class="col col-2">
                                <label class="input">

                                    <asp:DropDownList class="form-control" ID="txtRating" placeholder="Rating" runat="server">
                                        <asp:ListItem Value="Select Rating" Text="Select Rating" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="3"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                    </asp:DropDownList>


                                </label>

                            </section>

                            <section class="col col-5">
                                <label class="label col col-4">Black List</label>
                                <div class="inline-group">
                                    <label class="radio">
                                        <input type="radio" name="radio-inline" id="blacklistno" checked="" runat="server" />
                                        <i></i>No</label>
                                    <label class="radio">
                                        <input type="radio" name="radio-inline" id="blacklistyes" runat="server" />
                                        <i></i>Yes</label>

                                </div>
                            </section>
                        </div>
                    </fieldset>
                    <fieldset>

                        <div class="row">
                            <section class="col col-12">
                                <h3 class="text-primary">Contract Expiry Notification Before</h3>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-2">
                                <label class="input">

                                    <asp:TextBox ID="txtReminderBefor1" runat="server" class="form-control" placeholder="First" Text=""></asp:TextBox>
                                    <b class="tooltip tooltip-bottom-right">Enter Number of Day for 1st Notification</b>
                                </label>
                            </section>
                            <section class="col col-2">
                                <label class="input">

                                    <asp:TextBox ID="txtReminderBefor2" runat="server" class="form-control" placeholder="Second" Text=""></asp:TextBox>
                                     <b class="tooltip tooltip-bottom-right">Enter Number of Day for 2nd Notification</b>
                                </label>
                            </section>
                            <section class="col col-2">
                                <label class="input">

                                    <asp:TextBox ID="txtReminderBefor3" runat="server" class="form-control" placeholder="Third" Text=""></asp:TextBox>
                                     <b class="tooltip tooltip-bottom-right">Enter Number of Day for 3rd Notification</b>
                                </label>
                            </section>
                        </div>

                    </fieldset>




                    <fieldset>
                        <div class="row">
                            <section class="col col-12">
                                <h3 class="text-primary">Contact Person</h3>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-2">
                                <label class="input">

                                    <asp:DropDownList class="form-control" ID="txtTitle" runat="server">
                                        <asp:ListItem Value="Mr." Text="Mr."></asp:ListItem>
                                        <asp:ListItem Value="Miss." Text="Miss."></asp:ListItem>
                                        <asp:ListItem Value="Mrs." Text="Mrs."></asp:ListItem>
                                    </asp:DropDownList>


                                </label>
                            </section>
                            <section class="col col-3">
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-user"></i>

                                    <asp:TextBox ID="txtFirstName" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtfirstname" runat="server" ErrorMessage="First Name Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-user"></i>
                                    <asp:TextBox ID="txtSurname" runat="server" class="form-control" placeholder="Surname" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtSurName" runat="server" ErrorMessage="SurName Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-map-marker"></i>
                                    <asp:TextBox ID="txtPosition" runat="server" class="form-control" placeholder="Position" Text=""></asp:TextBox>
                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa-building-o"></i>
                                    <asp:TextBox ID="txtDepartment" runat="server" class="form-control" placeholder="Department" Text=""></asp:TextBox>
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-envelope-o"></i>

                                    <asp:TextBox ID="txtContactEmail" runat="server" class="form-control" placeholder="Email Address" Text=""></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-phone"></i>
                                    <asp:TextBox ID="txtTelephoneNo" runat="server" class="form-control" placeholder="Phone Number" Text="" data-mask="(999) 999-9999"></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-mobile"></i>

                                    <asp:TextBox ID="txtMobileNo" runat="server" class="form-control" placeholder="Mobile" Text=""></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-fax"></i>
                                    <asp:TextBox ID="txtFaxNo" runat="server" class="form-control" placeholder="Phone Number" Text="" data-mask="(999) 999-9999"></asp:TextBox>

                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-wechat"></i>

                                    <asp:TextBox ID="txtWeChatNo" runat="server" class="form-control" placeholder="WeChat" Text=""></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-whatsapp"></i>
                                    <asp:TextBox ID="txtWhatsup" runat="server" class="form-control" placeholder="What'sup" Text="" data-mask="(999) 999-9999"></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-skype"></i>

                                    <asp:TextBox ID="txtSkype" runat="server" class="form-control" placeholder="Skype" Text=""></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-facebook"></i>
                                    <asp:TextBox ID="txtFacebook" runat="server" class="form-control" placeholder="Facebook" Text=""></asp:TextBox>

                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-twitter"></i>

                                    <asp:TextBox ID="txtTwitter" runat="server" class="form-control" placeholder="Skype" Text=""></asp:TextBox>

                                </label>
                            </section>
                            <section class="col col-3">
                                <label class="input">
                                    <i class="icon-prepend fa fa-linkedin"></i>
                                    <asp:TextBox ID="txtLinkedIn" runat="server" class="form-control" placeholder="Facebook" Text=""></asp:TextBox>

                                </label>
                            </section>
                        </div>




                    </fieldset>



                    <footer>
                        <button id="save" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  type="submit" onserverclick="cmdSave_Click">
                               <span style="width:80px">Save </span>   
                            </button>
                            <button id="cancel" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  onserverclick="cmdCancel_Click">
                               <span style="width:80px">Cancel </span>   
                            </button>
                      
                    </footer>

                </div>



            </div>










            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>

</asp:Content>
