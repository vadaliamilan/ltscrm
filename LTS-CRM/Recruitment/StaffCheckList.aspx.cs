﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Recruitment
{
    public partial class StaffCheckList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                BindGrid();
            }
        }
        protected void BindGrid()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            String sql = "   SELECT ROW_NUMBER() OVER (ORDER BY  ID ) AS No,StaffCheckList,ID   FROM [RecNewStaffCheckList]  ";
            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = sql;

            MyRepeater.DataSource = Command.ExecuteReader();
            MyRepeater.DataBind();

            Connection.Close();

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            if (SessionValue("AutoID").ToString().Length > 0)
            {
                String sql = "select * from [dbo].[RecNewStaffCheckList] where [ID] = @ID";
                SqlCommand Command = new SqlCommand(sql, Connection);
                Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                SqlDataReader Reader = Command.ExecuteReader();
                if (Reader.Read())
                {
                    txtStaffCheckList.Text = Reader["StaffCheckList"].ToString();
                  
                }
                Reader.Close();
                Connection.Close();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            else
            {
                
            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            String sql = "delete from [dbo].[StaffCheckList] where [ID] = @ID";

            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
            Command.ExecuteNonQuery();
            BindGrid();
        }
        protected void cmdAddNew_Click(object sender, EventArgs e)
        {

            //lblheader.Text = "Record Exist";
            //lblmsg.Text = "This Department Name exist with selected company.";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            
            string ID = SessionValue("AutoID").ToString();



            if (checkDuplicate() == true && ID == "")
            {
                //lblheader.Text = "Record Exist";
                //lblmsg.Text = "This Job offer record exist.";

                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            else
            {
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();
                SqlCommand Command;
                String InsertSQL = "Insert into [dbo].[RecNewStaffCheckList] (StaffCheckList, [CreatedBy], [CreatedDate] ) VALUES (@StaffCheckList, @CreatedBy, @CreatedDate ) ";

                String UpdateSQL = "Update [dbo].[RecNewStaffCheckList] set [StaffCheckList] = @StaffCheckList, [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate where [ID] = @ID ";

                if (String.IsNullOrEmpty(ID))
                {
                    // 
                    //Create a new Command object for inserting a new record. 
                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                }
                else
                {
                    // 
                    //Set the command object with the update sql and connection. 
                    Command = new SqlCommand(UpdateSQL, Connection);
                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

                }
                Command.Parameters.AddWithValue("@StaffCheckList", txtStaffCheckList.Text);
                Command.ExecuteNonQuery();
                txtStaffCheckList.Text = "";

            }
            BindGrid();
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);

        }
      
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected Boolean checkDuplicate()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            string strsql = "select * from [dbo].[RecNewStaffCheckList] where ID=@ID";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            Command.Parameters.AddWithValue("@ID", txtStaffCheckList.Text);          
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
    }
}