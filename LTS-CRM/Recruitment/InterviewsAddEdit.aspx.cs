﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace LTS_CRM.Recruitment
{
    public partial class InterviewAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboRequisitionCode();
                LoadcboReportTo();
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();


                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {


                    String sql = "select * from [dbo].[RecInterview] where [ID] = @ID";
                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();
                    if (Reader.Read())
                    {

                        ListItem item = cboRequisitionCode.Items.FindByValue(Reader["RequisitionCode"].ToString());
                        cboRequisitionCode.SelectedIndex = cboRequisitionCode.Items.IndexOf(item);
                        cboRequisitionCode.Enabled = false;

                        ListItem item1 = cboCandidate.Items.FindByValue(Reader["CandidateName"].ToString());
                        cboCandidate.SelectedIndex = cboCandidate.Items.IndexOf(item1);

                        ListItem item2 = cboInterviewerName.Items.FindByValue(Reader["InterviewerName"].ToString());
                        cboInterviewerName.SelectedIndex = cboInterviewerName.Items.IndexOf(item2);

                        string s = Reader["InterviewDate"].ToString();
                        if (IsDate(s))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s);
                            txtInterviewDate.Value = dt.ToString("dd/MM/yyyy");
                        }


                        timepicker1.Value = Reader["InteviewStartTime"].ToString();
                        timepicker2.Value = Reader["InterviewEndTime"].ToString();

                        cboInterviewStatus.SelectedValue = Reader["InterviewStatus"].ToString();
                        cboInterviewType.SelectedValue= Reader["InterviewType"].ToString();

                        txtFeedback.Text = Reader["InterviewerFeedback"].ToString();
                        txtComment.Text = Reader["InterviewerComment"].ToString();
                       
                    }

                    Reader.Close();
                    Connection.Close();





                }
                else
                {
                    cboRequisitionCode.Enabled = true;
                    //downloadfile.Visible = false;
                }
            }


        }


        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();



            //if (checkDuplicate() == true && ID == "")
            //{
            //    lblheader.Text = "Record Exist";
            //    lblmsg.Text = "This Candidate Name already exist.";

            //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            //}
            //else
            //{
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();

                SqlCommand Command;

                String InsertSQL = "Insert into [dbo].[RecInterview] ([RequisitionCode], [CandidateName], [InterviewerName], [InterviewDate], [InteviewStartTime], [InterviewEndTime], [InterviewStatus], [InterviewType], [InterviewerFeedback], [InterviewerComment], [CreatedBy], [CreatedDate] ) VALUES ( @RequisitionCode, @CandidateName, @InterviewerName, @InterviewDate, @InteviewStartTime, @InterviewEndTime, @InterviewStatus, @InterviewType,  @InterviewerFeedback, @InterviewerComment, @CreatedBy, @CreatedDate ) ";

                String UpdateSQL = "Update [dbo].[RecInterview] set [RequisitionCode] = @RequisitionCode, [CandidateName] = @CandidateName, [InterviewerName] = @InterviewerName, [InterviewDate] = @InterviewDate, [InteviewStartTime] = @InteviewStartTime, [InterviewEndTime] = @InterviewEndTime, [InterviewStatus] = @InterviewStatus, [InterviewType] = @InterviewType,  [InterviewerFeedback] = @InterviewerFeedback, [InterviewerComment] = @InterviewerComment,  [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate where [ID] = @ID ";

                if (String.IsNullOrEmpty(ID))
                {
                    // 
                    //Create a new Command object for inserting a new record. 
                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                }
                else
                {
                    // 
                    //Set the command object with the update sql and connection. 
                    Command = new SqlCommand(UpdateSQL, Connection);
                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);

                }

                Command.Parameters.AddWithValue("@RequisitionCode", cboRequisitionCode.SelectedValue);
                Command.Parameters.AddWithValue("@CandidateName", cboCandidate.SelectedValue);

                Command.Parameters.AddWithValue("@InterviewerName", cboInterviewerName.SelectedValue);

                string res = txtInterviewDate.Value;



                if (res != "")
                {
                    DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Command.Parameters.AddWithValue("@InterviewDate", d);
                }
                else
                {
                    Command.Parameters.AddWithValue("@InterviewDate", DBNull.Value);
                }
               

                Command.Parameters.AddWithValue("@InteviewStartTime", timepicker1.Value );
                Command.Parameters.AddWithValue("@InterviewEndTime", timepicker2.Value);
                Command.Parameters.AddWithValue("@InterviewStatus", cboInterviewStatus.SelectedValue);
                Command.Parameters.AddWithValue("@InterviewType", cboInterviewType.SelectedValue);
                Command.Parameters.AddWithValue("@InterviewerFeedback", txtFeedback.Text);
                Command.Parameters.AddWithValue("@InterviewerComment", txtComment.Text);

                Command.ExecuteNonQuery();
                Connection.Close();
                Response.Redirect("Interviews.aspx");
            //}
        }

     
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
           
        }

        protected void LoadcboRequisitionCode()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select RequisitionCode,RequisitionCode + ' - ' + JobTitle as REQJOB from [RecNewHireRequest] order by RequisitionCode";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboRequisitionCode.DataTextField = "REQJOB";
            cboRequisitionCode.DataValueField = "RequisitionCode";
            cboRequisitionCode.DataSource = Command.ExecuteReader();
            cboRequisitionCode.DataBind();
            Connection.Close();
            LoadcboCandidate();
        }
        protected void RequisitionCode_Changed(object sender, EventArgs e)
        {
            LoadcboCandidate();

        }
        protected void LoadcboCandidate()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select FirstName + ' - ' + LastName as Candidate,ID from [dbo].[RecCandidate] where RequisitionCode='" + cboRequisitionCode.SelectedValue + "'";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCandidate.DataTextField = "Candidate";
            cboCandidate.DataValueField = "ID";
            cboCandidate.DataSource = Command.ExecuteReader();
            cboCandidate.DataBind();
            Connection.Close();

        }
        protected void LoadcboReportTo()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, FirstName from [Employee] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboInterviewerName.DataTextField = "FirstName";
            cboInterviewerName.DataValueField = "ID";
            cboInterviewerName.DataSource = Command.ExecuteReader();
            cboInterviewerName.DataBind();
            Connection.Close();
        }
        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }

        protected Boolean checkDuplicate()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            string strsql = "select * from [dbo].[RecCandidate] where RequisitionCode=@ID And Email=@ID1";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            Command.Parameters.AddWithValue("@ID", cboRequisitionCode.SelectedValue);
           // Command.Parameters.AddWithValue("@ID1", txtEmail.Text);
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
    }
}