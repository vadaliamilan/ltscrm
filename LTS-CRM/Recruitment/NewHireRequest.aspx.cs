﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;

namespace LTS_CRM.Recruitment
{
    public partial class NewHireRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                Session.Add("CompanyID", "1");

                LoadcboDepartmentID();
                LoadcboEmploymentStatus();

                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();

                Session.Add("UserName", "Nilesh");
                //if (SessionValue("AutoID").ToString().Length > 0)
                //{
                //    String sql = "select * from [dbo].[Recruitment] where [ID] = @ID";

                //    SqlCommand Command = new SqlCommand(sql, Connection);
                //    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                //    SqlDataReader Reader = Command.ExecuteReader();

                //    if (Reader.Read())
                //    {


                //        ListItem item1 = cboDepartmentHead.Items.FindByValue(Reader["ReportTo"].ToString());
                //        cboDepartmentHead.SelectedIndex = cboDepartmentHead.Items.IndexOf(item1);

                //        ListItem item2 = cboDepartmentID.Items.FindByText(Reader["DepartmentID"].ToString());
                //        cboDepartmentID.SelectedIndex = cboDepartmentID.Items.IndexOf(item2);


                //        ListItem item5 = cboEmploymentStatus.Items.FindByValue(Reader["EmploymentStatusID"].ToString());
                //        cboEmploymentStatus.SelectedIndex = cboEmploymentStatus.Items.IndexOf(item5);

                //        txtNoPeople.Text = Reader["NoPosition"].ToString();

                //        //ListItem item2 = cboReportTo.Items.FindByValue(Reader["ReportTo"].ToString());
                //        //cboReportTo.SelectedIndex = cboReportTo.Items.IndexOf(item2);      


                //    }

                //    Reader.Close();
                //    Connection.Close();
                //}

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();



            if (checkDuplicate() == true && ID == "")
            {
                lblheader.Text = "Record Exist";
                lblmsg.Text = "This Job offer record exist.";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            else
            {
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();
                SqlCommand Command;
                 String InsertSQL = "Insert into [dbo].[RecNewHireRequest] ([CompanyID], [Date], [RequisitionCode], [JobTitle], [DepartmentID], [ReportTo], [NoPosition],[EmploymentStatusID], [ApplicationPriority], [RequisitionStatus], [DocumentID], [CreatedBy], [CreatedDate]) VALUES ( @CompanyID, @Date, @RequisitionCode, @JobTitle, @DepartmentID, @ReportTo, @NoPosition, @EmploymentStatusID, @ApplicationPriority, @RequisitionStatus, @DocumentID, @CreatedBy, @CreatedDate ) ";

                // String UpdateSQL  = "Update [dbo].[RecNewHireRequest] set [CompanyID] = @CompanyID, [Date] = @Date, [RequisitionCode] = @RequisitionCode, [JobTitle] = @JobTitle, [DepartmentID] = @DepartmentID, [ReportTo] = @ReportTo, [NoPosition] = @NoPosition,  [EmploymentStatusID] = @EmploymentStatusID, [ApplicationPriority] = @ApplicationPriority, [RequisitionStatus] = @RequisitionStatus, [DocumentID] = @DocumentID,  [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate where [ID] = @ID ";



                Command = new SqlCommand(InsertSQL, Connection);
                Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                Command.Parameters.AddWithValue("@CompanyID", SessionValue("CompanyID"));
                string getreqno=comman.JobReqRunNo();
                Command.Parameters.AddWithValue("@RequisitionCode", getreqno);
                Command.Parameters.AddWithValue("@JobTitle", txtJobTitle.Text);
                Command.Parameters.AddWithValue("@NoPosition", txtNoPosition.Text);             
                Command.Parameters.AddWithValue("@DepartmentID", cboDepartmentID.SelectedValue);
                Command.Parameters.AddWithValue("@ReportTo", cboDepartmentHead.SelectedValue);  
                Command.Parameters.AddWithValue("@ApplicationPriority", cboPriority.SelectedValue);
                Command.Parameters.AddWithValue("@EmploymentStatusID", cboEmploymentStatus.SelectedValue);            
              
                Command.Parameters.AddWithValue("@RequisitionStatus", 1);
                string res = txtPositionReqDate.Value;
                if (res != "")
                {
                    DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Command.Parameters.AddWithValue("@Date", d);
                }
                else
                {
                    Command.Parameters.AddWithValue("@Date", DBNull.Value);
                }

                if (imageupload.HasFile && imageupload.PostedFile != null)
                {
                    int getid = DocumentUpload(imageupload);
                    if (getid > 0)
                    {
                        Command.Parameters.AddWithValue("@DocumentID", getid);
                    }
                    else
                    {
                        Command.Parameters.AddWithValue("@DocumentID", DBNull.Value);
                    }
                }
                else
                {
                    Command.Parameters.AddWithValue("@DocumentID", DBNull.Value);
                }
                Command.ExecuteNonQuery();
                Connection.Close();
               
            }

        }
        protected int DocumentUpload(FileUpload file)
        {
            string ID = SessionValue("DocumentID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            SqlCommand Command;

            String InsertSQL = "Insert into [dbo].[Document] (FileType,FileName,FileData) VALUES ( @FileType,@FileName,@FileData);SELECT CONVERT(int, SCOPE_IDENTITY()) ";

            String UpdateSQL = "Update [dbo].[Document] set [FileType] = @FileType, [FileName] = @FileName, [FileData] = @FileData where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {
                Command = new SqlCommand(InsertSQL, Connection);
                if (file.HasFile && file.PostedFile != null)
                {
                    HttpPostedFile File = file.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);


                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);

                    int newId = (int)Command.ExecuteScalar();
                    Connection.Close();
                    return newId;
                }
                else
                {
                    Connection.Close();
                    return 0;
                }
            }
            else
            {
                Command = new SqlCommand(UpdateSQL, Connection);
                if (file.HasFile && file.PostedFile != null)
                {
                    HttpPostedFile File = file.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteScalar();
                    Connection.Close();
                    return Convert.ToInt16(ID);
                }
                else
                {
                    Connection.Close();
                    return 0;
                }
            }
        }
        protected void LoadcboDepartmentID()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, DepartmentName from [Department] where CompanyID='" + SessionValue("CompanyID") + "'"; ;
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboDepartmentID.DataTextField = "DepartmentName";
            cboDepartmentID.DataValueField = "ID";
            cboDepartmentID.DataSource = Command.ExecuteReader();
            cboDepartmentID.DataBind();
            Connection.Close();
            LoadcboDepartmentHead();

        }
        protected void Department_Changed(object sender, EventArgs e)
        {
            LoadcboDepartmentHead();

        }
        protected void LoadcboDepartmentHead()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select E.ID, E.FirstName from [Employee] E Inner Join Department D On E.ID=D.DepartmentHead where  D.ID='" + cboDepartmentID.SelectedValue + "'";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboDepartmentHead.DataTextField = "FirstName";
            cboDepartmentHead.DataValueField = "ID";
            cboDepartmentHead.DataSource = Command.ExecuteReader();
            cboDepartmentHead.DataBind();
            Connection.Close();

        }
        protected void LoadcboEmploymentStatus()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, EmploymentStatus from [EmploymentStatus] Where EmploymentStatus In ('Temporary','Part Time','Full Time','Contract','Internship','Apprentice','Permanent')";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboEmploymentStatus.DataTextField = "EmploymentStatus";
            cboEmploymentStatus.DataValueField = "ID";
            cboEmploymentStatus.DataSource = Command.ExecuteReader();
            cboEmploymentStatus.DataBind();
            Connection.Close();

        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }

        protected Boolean checkDuplicate()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            string strsql = "select * from [dbo].[RecNewHireRequest] where DepartmentID=@ID And CompanyID=@ID1 And JobTitle=@JobTitle";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            Command.Parameters.AddWithValue("@ID", cboDepartmentID.SelectedValue);
            Command.Parameters.AddWithValue("@ID1", SessionValue("CompanyID"));
            Command.Parameters.AddWithValue("@JobTitle", txtJobTitle.Text);
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }


        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }




    }
}