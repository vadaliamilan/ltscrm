﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="JobOpeningsAddEdit.aspx.cs" Inherits="LTS_CRM.Recruitment.JobOpeAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>


            <ol class="breadcrumb">
                <li>Recruitment</li>
                <li>Job Offer Add-Edit</li>
            </ol>



        </div>

        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">

            <header>
                <h2>Job Position Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
            </div>

            <div class="widget-body">


                <div class="well">


                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                        <tbody>

                            <tr>
                                <td style="width: 10%;">Office</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" OnSelectedIndexChanged = "Company_Changed" AutoPostBack = "true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="cboCompanyID" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Department Name</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboDepartmentID" runat="server" OnSelectedIndexChanged = "Department_Changed" AutoPostBack = "true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="cboDepartmentID" runat="server" ErrorMessage="Department Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>


                                    </div>
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td style="width: 10%;">Department Head</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboDepartmentHead" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="cboDepartmentHead" runat="server" ErrorMessage="Department Head Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 10%;">Position</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboJobTitle" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="cboJobTitle" runat="server" ErrorMessage="Position Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                             <tr>
                                <td style="width: 10%;">Position Type</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboEmploymentStatus" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ControlToValidate="cboEmploymentStatus" runat="server" ErrorMessage="Employment Status Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                             <tr>
                                <td style="width: 10%;">Reporting Manager</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboReportTo" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="cboReportTo" runat="server" ErrorMessage="Report To Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 10%;">Request Date</td>
                                <td style="width: 10%">
                                     <div class="col-lg-8">
                                      
										<label class="class="input""> 
											<input type="text" name="txtdate" placeholder="Prepare Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" id="txtdate" style="height:30px" Font-Bold="true" ForeColor="Red">
										<i class="icon-append fa fa-calendar"></i>

										</label>
									
										 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtdate" runat="server" ErrorMessage="Date Required" Display="Dynamic" Font-Bold="true" ForeColor="Red" ></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 10%;">Request By</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtPrepareBy" runat="server" class="form-control" placeholder="Prepare By" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtPrepareBy" runat="server" ErrorMessage="Prepare By Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                             <tr>
                                <td style="width: 10%;">Required Position(No.)</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtnoofposition" runat="server" class="form-control" placeholder="Number of Position" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ControlToValidate="txtnoofposition" runat="server" ErrorMessage="No of Position Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 10%;">Approver 1</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboApprover1" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ControlToValidate="cboApprover1" runat="server" ErrorMessage="Approver1 Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 10%;">Approver 2</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboApprover2" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ControlToValidate="cboApprover2" runat="server" ErrorMessage="Approver2 Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                    </div>
                                </td>

                            </tr>

                            <tr>
                                <td style="width: 10%;">Line Management Responsibility</td>
                                <td style="width: 40%">
                                    <CKEditor:CKEditorControl ID="txtLineManagmentResponsibility" BasePath="~/ckeditor" runat="server" Height="200" placeholder="Enter Line Management Responsibility" ToolbarStartupExpanded="False">
                                    </CKEditor:CKEditorControl>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtLineManagmentResponsibility" runat="server" ErrorMessage="Line Management Responsibility Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Line Management Responsibility</td>
                                <td style="width: 40%">
                                    <CKEditor:CKEditorControl ID="txtJobPurposeSummary" BasePath="~/ckeditor" runat="server" Height="200" placeholder="Job Purpose Summary" ToolbarStartupExpanded="False">
                                    </CKEditor:CKEditorControl>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtJobPurposeSummary" runat="server" ErrorMessage="Job Purpose Summary Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>

                             <tr>
                                <td style="width: 10%;">Key Responsibities and Accountabilitie</td>
                                <td style="width: 40%">
                                    <CKEditor:CKEditorControl ID="txtKeyResponsibility" BasePath="~/ckeditor" runat="server" Height="200" placeholder="Key Responsibities and Accountabilities" ToolbarStartupExpanded="False">
                                    </CKEditor:CKEditorControl>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtKeyResponsibility" runat="server" ErrorMessage="Key Responsibities and Accountabilities Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                             <tr>
                                <td style="width: 10%;">Person Spec and Skills</td>
                                <td style="width: 40%">
                                    <CKEditor:CKEditorControl ID="txtPersonSpecSkills" BasePath="~/ckeditor" runat="server" Height="200" placeholder="Person Spec and Skills" ToolbarStartupExpanded="False">
                                    </CKEditor:CKEditorControl>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtPersonSpecSkills" runat="server" ErrorMessage="Person Spec and Skills Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>

                             <tr>
                                <td style="width: 10%;">Remuneration</td>
                                <td style="width: 40%">
                                    <CKEditor:CKEditorControl ID="txtRemuneration" BasePath="~/ckeditor" runat="server" Height="200" placeholder="Remuneration" ToolbarStartupExpanded="False">
                                    </CKEditor:CKEditorControl>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtRemuneration" runat="server" ErrorMessage="Remuneration Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                             <tr>
                                <td style="width: 10%;">Notes</td>
                                <td style="width: 40%">
                                    <CKEditor:CKEditorControl ID="txtNote" BasePath="~/ckeditor" runat="server" Height="200" placeholder="Note" ToolbarStartupExpanded="False">
                                    </CKEditor:CKEditorControl>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;"></td>
                                <td style="width: 40%">
                                    <div class="col-lg-4">
                                        <button id="save" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="cmdSave_Click">
                                            <span style="width: 80px">Save </span>
                                        </button>
                                        <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                    </div>
                                </td>
                            </tr>



                        </tbody>
                    </table>


                </div>










                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>

    </div>


    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
