﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Sales
{
    public partial class TaskAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTableByValue(ddlStatus, "TaskStatus", "Select Status");
                BindFormDetails();
            }
        }

        private void BindFormDetails()
        {
            if (TravelSession.Sales.TaskId.HasValue)
            {
                DLSalesTaskList obj = new DLSalesTaskList();
                obj = BLSalesTaskList.GetTaskById(TravelSession.Sales.TaskId.Value);
                txtSubject.Text = obj.Subject.Value;
                txtAssignedTo.Text = obj.AssignedTo.Value;
                if (ddlStatus.Items.FindByValue(Convert.ToString(obj.TaskStatus.Value)) != null)
                {
                    ddlStatus.ClearSelection();
                    ddlStatus.Items.FindByValue(Convert.ToString(obj.TaskStatus.Value)).Selected = true;
                }
                txtDescription.Text = obj.Description.Value;
                if (!obj.DueDate.IsNull && IsDate(Convert.ToString(obj.DueDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.DueDate.Value));
                    txtDueDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
            }
        }
        private void SaveDetails()
        {
            DLSalesTaskList obj = new DLSalesTaskList();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.Subject = txtSubject.Text;
            obj.AssignedTo = txtAssignedTo.Text;
            obj.TaskStatus = ddlStatus.SelectedItem.Value;
            obj.Description = txtDescription.Text;
            string resDueDate = txtDueDate.Text.Trim();
            if (resDueDate != "")
            {
                obj.DueDate = DateTime.ParseExact(resDueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (TravelSession.Sales.TaskId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Sales.TaskId.Value;
                BLSalesTaskList.UpdateTask(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLSalesTaskList.SaveTask(obj);
            }

            ClearFormDetails();
            Response.Redirect("SalesTask.aspx", true);

        }
        private void ClearFormDetails()
        {
            txtSubject.Text = "";
            txtAssignedTo.Text = "";
            ddlStatus.ClearSelection();
            txtDescription.Text = "";
            txtDueDate.Text = "";
        }

        protected void btnTaskListSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void Cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("SalesTask.aspx", true);
        }
    }
}