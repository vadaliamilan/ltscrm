﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Sales
{
    public partial class Task : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            rptOpinionSheet.DataSource = BLSalesTaskList.GetTaskList();
            rptOpinionSheet.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Sales.TaskId = null;
            Response.Redirect("~/Sales/SalesTaskAddEdit.aspx", true);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Sales.TaskId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Sales/SalesTaskAddEdit.aspx", true);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLSalesTaskList.DeleteTask(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();
        }
    }
}