﻿<%@ Page Title="" Language="C#" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="SalesTaskAddEdit.aspx.cs" Inherits="LTS_CRM.Sales.TaskAddEdit" %>
<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Sales</li>
                    <li>Task List</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Task List Add-Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">

                                    <div class="tab-content">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="TaskListform" class="row smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Company 
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addBasicgroup"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addBasicgroup" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                         <section class="col col-4">
                                                            <label class="label">
                                                                Subject
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtSubject" runat="server" placeholder="Subject" ValidationGroup="addTaskList" Text="" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSubject" ControlToValidate="txtSubject" ValidationGroup="addTaskList" runat="server" ErrorMessage="Subject is Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                       
                                                        <section class="col col-4">
                                                            <label class="label">
                                                                Assigned To
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtAssignedTo" runat="server" placeholder="Assigned To" ValidationGroup="addTaskList" Text="" class="form-control"></asp:TextBox>
                                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidatorAssignedTo" ControlToValidate="txtAssignedTo" ValidationGroup="addTaskList" runat="server" ErrorMessage="Assign To is Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Task Status
                                                            </label>
                                                            <label class="input">
                                                                 <asp:DropDownList ID="ddlStatus" runat="server" placeholder="Status" ValidationGroup="addTaskList"  class="form-control"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTaskStatus" ControlToValidate="ddlStatus" ValidationGroup="addTaskList" runat="server" ErrorMessage="Task Status is Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                         <section class="col col-3">
                                                            <label class="label">
                                                                Due Date
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtDueDate" runat="server" placeholder="Due Date" ValidationGroup="addTaskList" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDueDate" ControlToValidate="txtDueDate" ValidationGroup="addTaskList" runat="server" ErrorMessage="Due Date is Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                       
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Description
                                                            </label>
                                                            <label class="input">
                                                                <CKEditor:CKEditorControl ID="txtDescription"  BasePath="~/ckeditor" runat="server" Height="100" Width="700" ToolbarStartupExpanded="False">
                                                                </CKEditor:CKEditorControl>
                                                            </label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                                <footer>
                                                    <button id="btnTaskListSave" runat="server" validationgroup="addTaskList" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnTaskListSave_ServerClick">
                                                        <span style="width: 80px">Save </span>
                                                    </button>
                                                    <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" runat="server" id="Cancel" onserverclick="Cancel_ServerClick" />
                                                </footer>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </article>
                </div>
            </section>
        </div>
</asp:Content>

