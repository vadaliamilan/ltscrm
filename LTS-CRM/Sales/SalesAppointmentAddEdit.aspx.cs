﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Sales
{
    public partial class AppointmentAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTableByValue(ddlStatus, "AppointmentStatus", "Select Status");
                BindFormDetails();
            }
        }
        private void BindFormDetails()
        {
            if (TravelSession.Sales.AppointmentId.HasValue)
            {
                DLSalesAppointmentList obj = new DLSalesAppointmentList();
                obj = BLSalesAppointmentList.GetAppointmentById(TravelSession.Sales.AppointmentId.Value);
                txtSubject.Text = obj.Subject.Value;
                txtDescription.Text = obj.Description.Value;
                if (ddlStatus.Items.FindByValue(Convert.ToString(obj.Status.Value)) != null)
                {
                    ddlStatus.ClearSelection();
                    ddlStatus.Items.FindByValue(Convert.ToString(obj.Status.Value)).Selected = true;
                }
                if (!obj.AppointmentDate.IsNull && IsDate(Convert.ToString(obj.AppointmentDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.AppointmentDate.Value));
                    txtAppointmentDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.AppointmentTime.IsNull && IsDate(Convert.ToString(obj.AppointmentTime.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.AppointmentTime.Value));
                    txtAppointmentTime.Text = dt.ToString("h:mm");
                }
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
            }
        }
        private void SaveDetails()
        {
            DLSalesAppointmentList obj = new DLSalesAppointmentList();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.Subject = txtSubject.Text;
            obj.Status = ddlStatus.SelectedItem.Value;
            obj.Description = txtDescription.Text;
            string resAppointmentDate = txtAppointmentDate.Text.Trim();
            if (resAppointmentDate != "")
            {
                obj.AppointmentDate = DateTime.ParseExact(resAppointmentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            string resAppointmentTime = txtAppointmentTime.Text.Trim();
            if (resAppointmentTime != "")
            {
                obj.AppointmentTime = DateTime.ParseExact(resAppointmentTime, "h:mm tt", CultureInfo.InvariantCulture);
            }
            if (TravelSession.Sales.AppointmentId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Sales.AppointmentId.Value;
                BLSalesAppointmentList.UpdateAppointment(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLSalesAppointmentList.SaveAppointment(obj);
            }

            ClearFormDetails();
            Response.Redirect("SalesAppointment.aspx", true);

        }
        private void ClearFormDetails()
        {
            txtSubject.Text = "";
            ddlStatus.ClearSelection();
            txtDescription.Text = "";
            txtAppointmentDate.Text = "";
            txtAppointmentTime.Text = "";
        }
        protected void btnAppointmentListSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void Cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("SalesAppointment.aspx", true);
        }
    }

}