﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="DocumentCategoryAddEdit.aspx.cs" Inherits="LTS_CRM.OnlineForm.DocumentCategoryAddEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

           
            <ol class="breadcrumb">
                 <li>Online Form</li>
                <li>Document Add-EditCategory</li>
            </ol>
            

          
        </div>
          
        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
           
            <header>
                <h2>Document Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
           
            </div>
          
            <div class="widget-body">


                <div class="well">
          
                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                        <tbody>
                            <tr>
                                <td style="width: 10%;">Category Name</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtCategoryName" runat="server" class="form-control"  placeholder="Category Name" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="req1" ControlToValidate="txtCategoryName" runat="server" ErrorMessage="Category Name Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                          
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%"></td>
                </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%">
                        <div class="col-lg-4">
                            <button id="save" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  type="submit" onserverclick="cmdSave_Click">
                               <span style="width:80px">Save </span>   
                            </button>
                            <button id="cancel" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  onserverclick="cmdCancel_Click">
                               <span style="width:80px">Cancel </span>   
                            </button>
                        </div>
                    </td>
                </tr>



                </tbody>
                        </table>
                 

            </div>










            <!-- end widget content -->

        </div>
    <!-- end widget div -->

    </div>

    </div>
     <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
   
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                      <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                  <p><asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
     
            </div>
          </div>
</asp:Content>
