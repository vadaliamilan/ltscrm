﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace LTS_CRM.OnlineForm
{
    public partial class DocumentUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                BindGrid();
            }
        }
        protected void BindGrid()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            String sql = " SELECT ROW_NUMBER() OVER (ORDER BY  DocumentUpload.ID ) AS No, [dbo].[DocumentUpload].FormName, CompanyCompanyID.CompanyName as CompanyName, DepartmentDepartmentID.DepartmentName as DepartmentName,DocumentUpload.ID,DocumentCategory.CategoryName as DocumentCategory FROM ([dbo].[DocumentUpload] left join [Company] CompanyCompanyID on [dbo].[DocumentUpload].CompanyID = CompanyCompanyID.ID )  left join [Department] DepartmentDepartmentID on [dbo].[DocumentUpload].DepartmentID = DepartmentDepartmentID.ID left join [DocumentCategory]  on [dbo].[DocumentUpload].DocumentCategory = DocumentCategory.ID ";
            
            
            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = sql;

            MyRepeater.DataSource = Command.ExecuteReader();
            MyRepeater.DataBind();

            Connection.Close();

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            Response.Redirect("DocumentUploadAddEdit.aspx");
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            String sql = "delete from [dbo].[DocumentUpload] where [ID] = @ID";

            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
            Command.ExecuteNonQuery();
            BindGrid();
        }
        protected void cmdAddNew_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = "";
            Response.Redirect("DocumentUploadAddEdit.aspx");
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    }
}