﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="DocumentUploadAddEdit.aspx.cs" Inherits="LTS_CRM.OnlineForm.DocumentUploadAddEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

           
            <ol class="breadcrumb">
                <li>Online Form</li>
                <li>Online Form Add-Edit</li>
            </ol>
            

          
        </div>
          
        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
           
            <header>
                <h2>Online Form Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
           
            </div>
          
            <div class="widget-body">


                <div class="well">
                
                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                        <tbody>
                          <tr >
                                <td style="width: 10%;">Document Category</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control"  id="cboDocumentCategory" runat="server"  ></asp:DropDownList>                                          
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="cboDocumentCategory" runat="server" ErrorMessage="Document Category Required"  Display="Dynamic" ></asp:RequiredFieldValidator>                                                                              
                                        
                                        
                                    </div>
                                </td>

                            </tr>
                            <tr >
                                <td style="width: 10%;">Office</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control"  id="cboCompanyID" runat="server" OnSelectedIndexChanged = "Company_Changed" AutoPostBack = "true"></asp:DropDownList>                                          
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="cboCompanyID" runat="server" ErrorMessage="Company Name Required"  Display="Dynamic" ></asp:RequiredFieldValidator>                                                                              
                                        
                                        
                                    </div>
                                </td>

                            </tr>
                            <tr >
                                <td style="width: 10%;">Department Name</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                         <asp:DropDownList class="form-control"  id="cboDepartmentID" runat="server"></asp:DropDownList>                                          
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="cboDepartmentID" runat="server" ErrorMessage="Department Name Required"  Display="Dynamic"></asp:RequiredFieldValidator>                                                                              
                                       
                                        
                                    </div>
                                </td>

                            </tr>
                              
                            <tr>
                                <td style="width: 10%;">Form Display Name</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtFormName" runat="server" class="form-control"  placeholder="Form Display Name" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="req1" ControlToValidate="txtFormName" runat="server" ErrorMessage="Form Display Name Required"  Display="Dynamic"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                             <tr>
                                <td style="width: 10%;"><button id="downloadfile" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  type="submit" onserverclick="cmdDownload_Click">
                                       <span >Download File </span>   
                                    </button>  </td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                      <asp:FileUpload class="btn btn-default" id="imageupload" runat="server"   /> 
                                      
                                    </div>
                                </td>
                            </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%"></td>
                </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%">
                        <div class="col-lg-4">
                            <button id="save" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  type="submit" onserverclick="cmdSave_Click">
                               <span style="width:80px">Save </span>   
                            </button>
                            <button id="cancel" runat="server" class="btn bg-color-blueDark txt-color-white" style="width: 100px"  onserverclick="cmdCancel_Click">
                               <span style="width:80px">Cancel </span>   
                            </button>
                        </div>
                    </td>
                </tr>



                </tbody>
                        </table>
                 

            </div>










            <!-- end widget content -->

        </div>
    <!-- end widget div -->

    </div>

    </div>

</asp:Content>
