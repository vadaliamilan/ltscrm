﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace LTS_CRM.OnlineForm
{
    public partial class DocumentUploadAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboCompanyID();
                LoadcboDocumentCategory();
                //LoadcboDepartmentID();

                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();

                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                    imageupload.AllowMultiple = false;
                    downloadfile.Visible = true;
                    String sql = "select * from [dbo].[DocumentUpload] where [ID] = @ID";

                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.Read())
                    {
                        txtFormName.Text = Reader["FormName"].ToString();
                        cboCompanyID.SelectedValue = Reader["CompanyID"].ToString();
                        LoadcboDepartmentID();
                        cboDepartmentID.SelectedValue = Reader["DepartmentID"].ToString();
                        cboDocumentCategory.SelectedValue = Reader["DocumentCategory"].ToString();
                        Session.Add("txtFileType", Reader["FileType"].ToString());
                        Session.Add("txtFileName", Reader["FileName"].ToString());
                        Session.Add("txtFileData", Reader["FileData"].ToString());


                    }

                    Reader.Close();
                    Connection.Close();
                }
                else
                {
                    imageupload.AllowMultiple = true;
                    downloadfile.Visible = false;
                }
            }

        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            SqlCommand Command;

            //Declare string InsertSQL 
            String InsertSQL = "Insert into [dbo].[DocumentUpload] ([FormName], [CompanyID], [DepartmentID], [FileType], [FileName], [FileData], [CreatedBy], [CreatedDate] ,[DocumentCategory]) VALUES ( @FormName, @CompanyID, @DepartmentID, @FileType, @FileName, @FileData, @CreatedBy, @CreatedDate,@DocumentCategory) ";

            // 
            //Declare String UpdateSQL 
            String UpdateSQL = "Update [dbo].[DocumentUpload] set [FormName] = @FormName, [CompanyID] = @CompanyID, [DepartmentID] = @DepartmentID, [FileType] = @FileType, [FileName] = @FileName, [FileData] = @FileData, [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate,DocumentCategory=@DocumentCategory where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {



                foreach (HttpPostedFile postedFile in imageupload.PostedFiles)
                {
                    //To create a PostedFile

                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);

                    Command.Parameters.AddWithValue("@DocumentCategory", cboDocumentCategory.SelectedValue);
                    Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
                    Command.Parameters.AddWithValue("@DepartmentID", cboDepartmentID.SelectedValue);

                    HttpPostedFile File = postedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    Command.Parameters.AddWithValue("@FormName", name.Replace(".pdf",""));
                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteNonQuery();
                   

                }

                Connection.Close();
            }
            else
            {
                // 
                //Set the command object with the update sql and connection. 
                Command = new SqlCommand(UpdateSQL, Connection);
                // 
                //Set the @ID field for updates. 
                Command.Parameters.AddWithValue("@ID", ID);
                Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);
                Command.Parameters.AddWithValue("@FormName", txtFormName.Text);
                Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
                Command.Parameters.AddWithValue("@DepartmentID", cboDepartmentID.SelectedValue);
                Command.Parameters.AddWithValue("@DocumentCategory", cboDocumentCategory.SelectedValue);
                if (imageupload.HasFile && imageupload.PostedFile != null)
                {
                    //To create a PostedFile
                    HttpPostedFile File = imageupload.PostedFile;                  
                   
                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);
                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteNonQuery();
                    Connection.Close();
                }
                else
                {
                    Command.CommandText = Command.CommandText.Replace("[FileType] = @FileType, [FileName] = @FileName, [FileData] = @FileData,", " ");
                    //Command.Parameters.AddWithValue("@FileType", SessionValue("txtFileType"));
                    //Command.Parameters.AddWithValue("@FileName", SessionValue("FileName"));
                    //Command.Parameters.AddWithValue("@FileData", SessionValue("FileData"));
                    Command.ExecuteNonQuery();
                    Connection.Close();
                }
               
            }
            Response.Redirect("DocumentUpload.aspx");
        }

        protected void cmdDownload_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();


            DataTable file = GetAFile(Convert.ToInt16(ID));
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["FileType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }
        // Get a file from the database by ID
        public static DataTable GetAFile(int id)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
           
                DataTable file = new DataTable();
           
                Connection.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = Connection;
                cmd.CommandTimeout = 0;

                cmd.CommandText = "SELECT ID, FileName, FileType, FileData FROM DocumentUpload "
                    + "WHERE ID=@ID";
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter adapter = new SqlDataAdapter();

                cmd.Parameters.Add("@ID", SqlDbType.Int);
                cmd.Parameters["@ID"].Value = id;

                adapter.SelectCommand = cmd;
                adapter.Fill(file);

                Connection.Close();
           
            return file;
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("DocumentUpload.aspx");
        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        protected void LoadcboCompanyID()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, CompanyName from [Company]  ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCompanyID.DataTextField = "CompanyName";
            cboCompanyID.DataValueField = "ID";
            cboCompanyID.DataSource = Command.ExecuteReader();
            cboCompanyID.DataBind();

            Connection.Close();
            LoadcboDepartmentID();
        }
        protected void Company_Changed(object sender, EventArgs e)
        {
            LoadcboDepartmentID();
        }
        protected void LoadcboDepartmentID()
        {           
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);           
            Connection.Open();
            String sql = "select ID, DepartmentName from [Department] where CompanyID=@CompanyID ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
            cboDepartmentID.DataTextField = "DepartmentName";
            cboDepartmentID.DataValueField = "ID";
            cboDepartmentID.DataSource = Command.ExecuteReader();
            cboDepartmentID.DataBind();            
            Connection.Close();
        }

   
    protected void LoadcboDocumentCategory()
        {           
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);           
            Connection.Open();
            String sql = "select ID, CategoryName from [DocumentCategory] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
            cboDocumentCategory.DataTextField = "CategoryName";
            cboDocumentCategory.DataValueField = "ID";
            cboDocumentCategory.DataSource = Command.ExecuteReader();
            cboDocumentCategory.DataBind();            
            Connection.Close();
        }
 }
}