﻿using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class GeneralCategoryAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadCategoryTitlesOnly(ddlCategory);
            }
        }

        protected void btnGeneralCategoryItemsSave_ServerClick(object sender, EventArgs e)
        {
            if(txtTitle.Text.Trim() !="")
            {
                LTSCRM_DL.DLGeneralCategoryItems obj = new LTSCRM_DL.DLGeneralCategoryItems();
                obj.Category = ddlCategory.SelectedValue;
                obj.Title = txtTitle.Text.Trim();
                obj.Description = txtDescription.Text.Trim();
                obj.UpdatedBy = obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                obj.UpdatedDate = DateTime.Now;
                BLL.BLGeneralCategory.SaveGeneralItem(obj);
                Response.Redirect("GeneralCategoryManagement.aspx", true);
            }
        }
    }
}