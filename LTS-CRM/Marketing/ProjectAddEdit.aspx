﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectAddEdit.aspx.cs" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" Inherits="LTS_CRM.Marketing.ProjectAddEdit" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function changePlan(obj) {
            $("#txtHowLongBefore").hide();
            $("#txtHowLongLater").hide();

            if ($(obj).val() == 1) {
                $("#txtHowLongBefore").show();
            }
            if ($(obj).val() == 2) {

                $("#txtHowLongLater").show();
            }

            return false;
        }

    </script>
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Management</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Project Add-Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="tab-content">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 15px;">
                                            <div id="MarketingProjectform" class="smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                         <section class="col col-3">
                                                            <label class="label">
                                                                Company 
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addBasicgroup"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addBasicgroup" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Reference No 
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtReferenceNo" runat="server" placeholder="Reference No" ValidationGroup="addMarketingProject" Text="" class="form-control"></asp:TextBox>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Project Name
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtProjectName" runat="server" placeholder="Project Name" ValidationGroup="addMarketingProject" Text="" class="form-control"></asp:TextBox>
                                                                   <asp:RequiredFieldValidator ID="rfvcboProjectName" ControlToValidate="txtProjectName" ValidationGroup="addMarketingProject" runat="server" ErrorMessage="Project Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Project Description
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtProjectDescribution" runat="server" placeholder="Project Description" ValidationGroup="addMarketingProject" Text="" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorProjectDescribution" ControlToValidate="txtProjectDescribution" ValidationGroup="addMarketingProject" runat="server" ErrorMessage="Project Description Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                       
                                                    </div>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Staff Involved
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtStaffInvolved" runat="server" placeholder="Staff Involved" ValidationGroup="addMarketingProject" Text="" class="form-control"></asp:TextBox>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Starting Date
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtProjectStartingDate" runat="server" placeholder="Starting Date" ValidationGroup="addMarketingProject" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Planned Ending Date
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtProjectPlannedEndingDate" runat="server" placeholder="Planned Ending Date" ValidationGroup="addMarketingProject" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Rating
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList ID="ddlRating" runat="server" class="form-control">
                                                                    <asp:ListItem Text="--Select--" Value="0" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">

                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Actual End Date
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtProjectActualEndDate" runat="server" placeholder="Actual End Date" ValidationGroup="addMarketingProject" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                            </label>
                                                        </section>

                                                        <section class="col col-9">
                                                            <div style="width: 100%; border: solid 0px">
                                                                <div class="col-xs-3">
                                                                    <input type="radio" name="planending" runat="server" id="planningOnTime" value="0" onchange="changePlan(this)" />
                                                                    On Time
                                                                </div>
                                                                <div class="col-xs-4 inline">
                                                                    <input type="radio" name="planending" runat="server" id="planningBefore" value="1" onchange="changePlan(this)" />
                                                                    Before Schedule :
                                                                        <asp:TextBox ID="txtHowLongBefore" runat="server" Style="display: none"></asp:TextBox>
                                                                </div>
                                                                <div class="col-xs-4 inline">

                                                                    <input type="radio" name="planending" runat="server" id="planningLater" value="2" onchange="changePlan(this)" />
                                                                    Late than Sechdule :
                                                                        <asp:TextBox ID="txtHowLongLater" runat="server" Style="display: none"></asp:TextBox>

                                                                </div>

                                                            </div>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                         <section class="col col-3">
                                                            <label class="label">
                                                                Total Project Budget
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtProjectBudget" runat="server" placeholder="Total Project Budget" ValidationGroup="addMarketingProject" Text="" class="form-control"></asp:TextBox>
                                                                 <asp:RegularExpressionValidator ID="RegularExpressionValidatorBudget" ControlToValidate="txtProjectBudget" ValidationGroup="addMarketingProject" runat="server" SetFocusOnError="true" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Total Project Estimate Budget
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtProjectEstimateBudget" runat="server" placeholder="Total Project Estimate Budget" ValidationGroup="addMarketingProject" Text="" class="form-control"></asp:TextBox>
                                                                  <asp:RegularExpressionValidator ID="RegularExpressionValidatorEstimateBudget" ControlToValidate="txtProjectEstimateBudget" ValidationGroup="addMarketingProject" runat="server" SetFocusOnError="true" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                                            </label>
                                                        </section>
                                                         <section class="col col-3">
                                                            <label class="label">
                                                                Initiate By
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtInitiateBy" runat="server" placeholder="Initiate By" ValidationGroup="addMarketingProject" Text="" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorInitiateBy" ControlToValidate="txtInitiateBy" ValidationGroup="addMarketingProject" runat="server" ErrorMessage="Initiate By Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">

                                                        <section class="col col-6">
                                                            <label class="input">
                                                                Process Comments
                                                                <CKEditor:CKEditorControl ID="txtWorkingProcessComment" BasePath="~/ckeditor" runat="server" Height="100" Width="400" ToolbarStartupExpanded="False">
                                                                </CKEditor:CKEditorControl>

                                                            </label>
                                                        </section>
                                                        <section class="col col-6">
                                                            <label class="input">
                                                                Project Report
                                                                          <CKEditor:CKEditorControl ID="txtProjectReport" BasePath="~/ckeditor" runat="server" Height="100" Width="400" ToolbarStartupExpanded="False">
                                                                          </CKEditor:CKEditorControl>
                                                            </label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                                <footer>
                                                    <button id="btnMarketingProjectSave" runat="server" onserverclick="btnMarketingProjectSave_ServerClick" validationgroup="addMarketingProject" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                        <span style="width: 80px">Save </span>
                                                    </button>
                                                    <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                                </footer>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>

            <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">
                                <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
