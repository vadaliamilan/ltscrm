﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LTS_CRM.Helper;
using System.Globalization;

namespace LTS_CRM.Marketing
{
    public partial class ProjectAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadcboCompanyID(cboCompanyID);
                BindFormDetails();
            }
        }
        private void BindFormDetails()
        {
            if (TravelSession.Marketing.ProjectId.HasValue)
            {
                LTSCRM_DL.DLMarketingProject obj = BLL.BLMarketingProject.GetProjectByPk(TravelSession.Marketing.ProjectId.Value);
                txtReferenceNo.Text = obj.ReferenceNo.Value;
                txtProjectName.Text = obj.ProjectName.Value;
                txtProjectDescribution.Text = obj.ProjectDescribution.Value;
                txtInitiateBy.Text = obj.InitiateBy.Value;
                txtStaffInvolved.Text = obj.StaffInvolved.Value;
                txtProjectBudget.Text = Convert.ToString(obj.TotalBudget.Value);
                txtProjectEstimateBudget.Text = Convert.ToString(obj.TotalEstimateBudget.Value);
                txtWorkingProcessComment.Text = obj.WorkingProcessComment.Value;
                txtProjectReport.Text = obj.ProjectReport.Value;
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                if (!obj.ProjectStartingDate.IsNull && IsDate(Convert.ToString(obj.ProjectStartingDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ProjectStartingDate.Value));
                    txtProjectStartingDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.ProjectPlannedEndingDate.IsNull && IsDate(Convert.ToString(obj.ProjectPlannedEndingDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ProjectPlannedEndingDate.Value));
                    txtProjectPlannedEndingDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtWorkingProcessComment.Text = obj.WorkingProcessComment.Value;
                if (!obj.ProjectActualEndDate.IsNull && IsDate(Convert.ToString(obj.ProjectActualEndDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ProjectActualEndDate.Value));
                    txtProjectActualEndDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.Rating.IsNull && ddlRating.Items.Count > 0 && ddlRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)) != null)
                {
                    ddlRating.ClearSelection();
                    ddlRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)).Selected = true;
                }

                txtProjectReport.Text = obj.ProjectReport.Value;

                if (!obj.ExtraField1.IsNull && obj.ExtraField1.Value.Trim() != "")
                {
                    string[] strValue = obj.ExtraField1.Value.Split('|');
                    if (strValue.Length > 0)
                    {
                        if (strValue[0] == "0")
                        {
                            planningOnTime.Checked = true;
                        }
                        else if (strValue[0] == "1")
                        {
                            planningBefore.Checked = true;
                            txtHowLongBefore.Style.Add("display", "");
                            if (strValue.Length > 1)
                                txtHowLongBefore.Text = strValue[1];
                        }
                        else if (strValue[0] == "2")
                        {
                            planningLater.Checked = true;
                            txtHowLongLater.Style.Add("display", "");
                            if (strValue.Length > 1)
                                txtHowLongLater.Text = strValue[1];
                        }
                    }
                }

            }
        }

        private void SaveDetails()
        {
            LTSCRM_DL.DLMarketingProject obj = new LTSCRM_DL.DLMarketingProject();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.ReferenceNo = txtReferenceNo.Text;
            obj.ProjectName = txtProjectName.Text;
            obj.ProjectDescribution = txtProjectDescribution.Text;
            obj.InitiateBy = txtInitiateBy.Text;
            obj.StaffInvolved = txtStaffInvolved.Text;
            obj.TotalEstimateBudget = !string.IsNullOrEmpty(txtProjectEstimateBudget.Text) ? Convert.ToDecimal(txtProjectEstimateBudget.Text) : 0;
            obj.TotalBudget = !string.IsNullOrEmpty(txtProjectBudget.Text) ? Convert.ToDecimal(txtProjectBudget.Text) : 0;
            obj.WorkingProcessComment = txtWorkingProcessComment.Text;
            obj.ProjectReport = txtProjectReport.Text;
            string resProjectStartingDate = txtProjectStartingDate.Text.Trim();
            if (resProjectStartingDate != "")
            {
                obj.ProjectStartingDate = DateTime.ParseExact(resProjectStartingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            string resProjectPlannedEndingDate = txtProjectPlannedEndingDate.Text.Trim();
            if (resProjectPlannedEndingDate != "")
            {
                obj.ProjectPlannedEndingDate = DateTime.ParseExact(resProjectPlannedEndingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.WorkingProcessComment = txtWorkingProcessComment.Text;
            string resProjectActualEndDate = txtProjectActualEndDate.Text.Trim();
            if (resProjectActualEndDate != "")
            {
                obj.ProjectActualEndDate = DateTime.ParseExact(resProjectActualEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.Rating = ddlRating.SelectedItem.Value;
            obj.ProjectReport = txtProjectReport.Text;

            string strValue = "";

            if (planningOnTime.Checked)
            {
                strValue = "0";
            }
            else if (planningBefore.Checked)
            {
                strValue = "1|" + txtHowLongBefore.Text.Trim();
            }
            else if (planningLater.Checked)
            {
                strValue = "2|" + txtHowLongLater.Text.Trim();
            }
            obj.ExtraField1 = strValue;

            if (TravelSession.Marketing.ProjectId.HasValue)
            {
                obj.ID = TravelSession.Marketing.ProjectId.Value;
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                BLL.BLMarketingProject.Update(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLL.BLMarketingProject.Save(obj);
            }


            ClearFormDetails();
            Response.Redirect("~/Marketing/Project.aspx", false);
        }

        private void ClearFormDetails()
        {
            TravelSession.Marketing.ProjectId = null;
            txtReferenceNo.Text = "";
            txtProjectName.Text = "";
            txtProjectDescribution.Text = "";
            txtInitiateBy.Text = "";
            txtStaffInvolved.Text = "";
            txtProjectStartingDate.Text = "";
            txtProjectPlannedEndingDate.Text = "";
            txtWorkingProcessComment.Text = "";
            txtProjectActualEndDate.Text = "";
            ddlRating.ClearSelection();
            txtProjectReport.Text = "";
            txtProjectBudget.Text = "";
            txtProjectEstimateBudget.Text = "";
            txtProjectReport.Text = "";
            txtWorkingProcessComment.Text = "";
        }

        protected void btnMarketingProjectSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }
    }
}