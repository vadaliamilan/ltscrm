﻿using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class GeneralCategoryManagement : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadCategoryTitlesOnly(ddlCategory, "-- Choose Category --");
                BindRepeater(BLL.BLGeneralCategory.GetALLCategory());
            }
        }

        private void BindRepeater(DataTable dt)
        {
            rptGeneralCategoryItems.DataSource = dt;
            rptGeneralCategoryItems.DataBind();
        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = ((DropDownList)sender).SelectedItem.Value.Trim();
            BindRepeater(BLL.BLGeneralCategory.GetALLCategory(value != "" ? value : ""));
        }
 

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("GeneralCategoryAddEdit.aspx", true);

        }
    }
}