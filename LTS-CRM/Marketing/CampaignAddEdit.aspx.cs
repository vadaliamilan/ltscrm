﻿using LTS_CRM.App_Code.BLL.Product;
using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class CampaignAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadProduct(ddlProduct);
                BindFormDetails();
            }
        }
        private void LoadProduct(DropDownList ddl)
        {
            ddlProduct.DataTextField = "ProductType";
            ddlProduct.DataValueField = "ID";
            ddlProduct.DataSource = BLProduct.GetProductList();
            ddlProduct.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem() { Selected = true, Text = "Select Product", Value = string.Empty });
        }
        private void BindFormDetails()
        {
            if (TravelSession.Marketing.CampaignId.HasValue)
            {
                DLMarketingCampaign obj = new DLMarketingCampaign();
                obj = BLCampaign.GetCampaignById(TravelSession.Marketing.CampaignId.Value);

                txtCampaignName.Text = obj.CampaignName.Value;
                if (!obj.StartDate.IsNull && IsDate(Convert.ToString(obj.StartDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.StartDate.Value));
                    txtStartDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (!obj.EndDate.IsNull && IsDate(Convert.ToString(obj.EndDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.EndDate.Value));
                    txtEndDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtCampaignDescription.Text = obj.CampaignDescription.Value;
                txtAddress1.Text = obj.Address1.Value;
                txtAddress2.Text = obj.Address2.Value;
                txtCity.Text = obj.City.Value;
                txtState.Text = obj.State.Value;
                ddlCountry.Value = obj.Country.Value;
                txtTelephone.Text = obj.Telephone.Value;
                txtEmail.Text = obj.Email.Value;
                txtAlternateEmail.Text = obj.AlternateEmail.Value;
                txtFax.Text = obj.Fax.Value;
                txtEstimateBudget.Text = Convert.ToString(obj.EstimateBudget.Value);
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                if (ddlProduct.Items.FindByValue(Convert.ToString(obj.ProjectID.Value)) != null)
                {
                    ddlProduct.ClearSelection();
                    ddlProduct.Items.FindByValue(Convert.ToString(obj.ProjectID.Value)).Selected = true;
                }
            }
        }
        private void SaveDetails()
        {
            DLMarketingCampaign obj = new DLMarketingCampaign();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.ProjectID = Convert.ToInt32(ddlProduct.SelectedValue);
            obj.CampaignName = txtCampaignName.Text;
            string resStartDate = txtStartDate.Text.Trim();
            if (resStartDate != "")
            {
                obj.StartDate = DateTime.ParseExact(resStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            string resEndDate = txtEndDate.Text.Trim();
            if (resEndDate != "")
            {
                obj.EndDate = DateTime.ParseExact(resEndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.CampaignDescription = txtCampaignDescription.Text;
            obj.Address1 = txtAddress1.Text;
            obj.Address2 = txtAddress2.Text;
            obj.City = txtCity.Text;
            obj.State = txtState.Text;
            obj.Country = ddlCountry.Value;
            obj.Telephone = txtTelephone.Text;
            obj.Email = txtEmail.Text;
            obj.AlternateEmail = txtAlternateEmail.Text;
            obj.Fax = txtFax.Text;
            obj.EstimateBudget = Convert.ToDecimal(txtEstimateBudget.Text);
            if (TravelSession.Marketing.CampaignId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Marketing.CampaignId.Value;
                BLCampaign.UpdateCampaign(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLCampaign.SaveCampaign(obj);
            }

            ClearFormDetails();
            Response.Redirect("Campaign.aspx", true);

        }
        private void ClearFormDetails()
        {
            txtCampaignName.Text = "";
            txtStartDate.Text = "";
            txtEndDate.Text = "";
            txtCampaignDescription.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            ddlCountry.SelectedIndex = -1;
            txtTelephone.Text = "";
            txtEmail.Text = "";
            txtAlternateEmail.Text = "";
            txtFax.Text = "";
           
        }

        protected void btnMarketingCampaignSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }
        protected void cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("Campaign.aspx", true);
        }
        
    }
}