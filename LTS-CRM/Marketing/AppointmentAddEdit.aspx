﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" CodeBehind="AppointmentAddEdit.aspx.cs" Inherits="LTS_CRM.Marketing.AppointmentAddEdit" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Marketing</li>
                    <li>Appointment List</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Appointment Add-Edit</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">

                                    <div class="tab-content">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div id="AppointmentListform" class="row smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Company 
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addBasicgroup"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addBasicgroup" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Subject
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtSubject" runat="server" placeholder="Subject" ValidationGroup="addAppointmentList" Text="" class="form-control"></asp:TextBox>
                                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidatorSubject" ControlToValidate="txtSubject" ValidationGroup="addAppointmentList" runat="server" ErrorMessage="Subject Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Status
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList ID="ddlStatus" runat="server" placeholder="Status" ValidationGroup="addAppointmentList"  class="form-control"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorStatus" ControlToValidate="ddlStatus" ValidationGroup="addAppointmentList" runat="server" ErrorMessage="Status Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                       
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Appointment Date
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtAppointmentDate" runat="server" placeholder="AppointmentDate" ValidationGroup="addAppointmentList" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorAppointmentDate" ControlToValidate="txtAppointmentDate" ValidationGroup="addAppointmentList" runat="server" ErrorMessage="Appointment date Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Appointment Time
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtAppointmentTime" runat="server" placeholder="AppointmentTime" ValidationGroup="addAppointmentList"  class="form-control timepicker" ></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorAppointmentTime" ControlToValidate="txtAppointmentTime" ValidationGroup="addAppointmentList" runat="server" ErrorMessage="Appointment time Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                         <section class="col col-3">
                                                            <label class="label">
                                                                Description
                                                            </label>
                                                            <label class="input">
                                                               <CKEditor:CKEditorControl ID="txtDescription"  BasePath="~/ckeditor" runat="server" Height="100" Width="700" ToolbarStartupExpanded="False">
                                                                </CKEditor:CKEditorControl>
                                                            </label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                                <footer>
                                                    <button id="btnAppointmentListSave" runat="server" validationgroup="addAppointmentList" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btnAppointmentListSave_ServerClick">
                                                        <span style="width: 80px">Save </span>
                                                    </button>
                                                    <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" runat="server" id="Cancel" onserverclick="Cancel_ServerClick" />
                                                </footer>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                </div>
            </article>
        </div>
        </section>
    </div>
</asp:Content>

