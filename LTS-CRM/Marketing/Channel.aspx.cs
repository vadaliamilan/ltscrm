﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LTS_CRM.BLL;
using LTS_CRM.Helper; 
namespace LTS_CRM.Marketing
{
    public partial class Channel : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            MarketingChannelRepeater.DataSource = BLMarketingChannel.GetChannelList();
            MarketingChannelRepeater.DataBind();
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Marketing.ChannelId = null; 
            Response.Redirect("~/Marketing/ChannelAddEdit.aspx", true);
        }
       

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Marketing.ChannelId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Marketing/ChannelAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        { 
            bool isSuccess = BLMarketingChannel.Delete(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();
        }
    }
}