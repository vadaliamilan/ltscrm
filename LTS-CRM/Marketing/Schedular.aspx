﻿<%@ Page Title="" Language="C#" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="Schedular.aspx.cs" Inherits="LTS_CRM.Marketing.Schedular" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="well">
        <asp:Label ID="msglbl" runat="server" Text=""></asp:Label>
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Marketing</li>
                <li>Scheduler</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Task & Appointment Scheduler</h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">

                                   
                                    <div class="well well-lg">
                                      
                                        <asp:calendar id="C1" runat="server"  Width="100%" BorderWidth="3px" DayNameFormat="Full" FirstDayOfWeek="Sunday" NextMonthText=" " PrevMonthText=" " BorderColor="Silver"
				Font-Names="Verdana" Font-Size="12pt" ForeColor="Black"  BorderStyle="Double" Font-Overline="False" Font-Underline="False" NextPrevFormat="FullMonth" OnDayRender="C1_DayRender" BackColor="White">
				<TodayDayStyle ForeColor="White" BackColor="#001F3E"></TodayDayStyle>
				<DayStyle HorizontalAlign="Left" Height="60px" BorderWidth="3px" BorderStyle="Double" BorderColor="Silver"
					VerticalAlign="Top" BackColor="Transparent" Width="260px"></DayStyle>
				<NextPrevStyle Font-Size="12pt" Font-Bold="True" ForeColor="White" Font-Underline="False"></NextPrevStyle>
				<DayHeaderStyle Font-Size="8pt" HorizontalAlign="Center" Height="8pt" BorderWidth="3px" ForeColor="DimGray"
					BorderStyle="Solid" BorderColor="Silver" BackColor="Transparent"></DayHeaderStyle>
				<SelectedDayStyle ForeColor="White" BackColor="White"></SelectedDayStyle>
				<TitleStyle Font-Size="14pt" Font-Bold="True" Height="14pt" BorderWidth="2px" ForeColor="white"
					BorderStyle="Solid" BackColor="#001F3E"></TitleStyle>
				<WeekendDayStyle BorderColor="Silver" BackColor="Transparent" Wrap="True"></WeekendDayStyle>
				<OtherMonthDayStyle ForeColor="#999999" BackColor="Transparent"></OtherMonthDayStyle>
			</asp:calendar>
                <table width="100%" style="padding: 8px; margin: 8px; font-size: 12pt; color: black; font-family: Verdena" class="table">
                    <tr>
                        <td bordercolor="#666666" colspan="8" style="height: 24px">
                            <strong>Legend</strong></td>
                    </tr>
                    <tr>
                        <td style="width: 13px; height: 24px">
                            <asp:Label ID="Label1" runat="server" BackColor="Red" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="Red" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Appointment</td>
                        <td style="width: 3px; height: 24px">
                            <asp:Label ID="Label5" runat="server" BackColor="Orange" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" ForeColor="Orange" Text="1" Width="20px"></asp:Label></td>
                        <td style="padding: 5px; margin: 5px; height: 24px">
                            Task</td>
                       
                        
                    </tr>
                  
                </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>


</asp:Content>
