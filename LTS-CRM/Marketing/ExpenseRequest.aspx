﻿<%@ Page Title="" Language="C#" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="ExpenseRequest.aspx.cs" Inherits="LTS_CRM.Marketing.ExpenseRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="well">
        <asp:Label ID="msglbl" runat="server" Text=""></asp:Label>
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Marketing</li>
                <li>Expence Request</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Expence Request </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">


                                    <div class="well well-lg">
                                        <table class="table table-striped table-bordered" width="100%">

                                            <tbody>

                                                <tr>
                                                    <td style="width: 10%;">Company</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server" ValidationGroup="addExpenseRequest"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfvcboCompanyID" ControlToValidate="cboCompanyID" ValidationGroup="addExpenseRequest" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Expence Date</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtExpenseDate" runat="server" ValidationGroup="addExpenseRequest" Text="" class="form-control datepicker" data-dateformat='dd/mm/yy'></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAppointmentDate" ControlToValidate="txtExpenseDate" ValidationGroup="addExpenseRequest" runat="server" ErrorMessage="Expence date Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Insert By</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="txtInsertBy" runat="server" ValidationGroup="addExpenseRequest" Text="" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 10%;">Category</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:DropDownList class="form-control" ID="ddlCategory" runat="server" ValidationGroup="addExpenseRequest"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlCategory" ControlToValidate="ddlCategory" ValidationGroup="addExpenseRequest" runat="server" ErrorMessage="Category Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Sub Category</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:DropDownList class="form-control" ID="ddlSubCategory" runat="server" ValidationGroup="addExpenseRequest"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlSubCategory" ControlToValidate="ddlSubCategory" ValidationGroup="addExpenseRequest" runat="server" ErrorMessage="Sub Category Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Currency</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:DropDownList class="form-control" ID="ddlCurrency" runat="server" ValidationGroup="addExpenseRequest"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlCurrency" ControlToValidate="ddlCurrency" ValidationGroup="addExpenseRequest" runat="server" ErrorMessage="Currency Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Purticular</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <textarea cols="30" rows="3" runat="server" id="txtPurticular"></textarea>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Amount</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <asp:TextBox runat="server" ID="txtAmount"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorAmount" ControlToValidate="txtAmount" ValidationGroup="addMarketingProject" runat="server" SetFocusOnError="true" Text="Only Digit & decimal point allowed." ErrorMessage="*" Display="Dynamic" ValidationExpression="[0-9]*\.?[0-9]*" Font-Bold="true" ForeColor="Red"></asp:RegularExpressionValidator>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%;">Note</td>
                                                    <td style="width: 1%;">:</td>
                                                    <td style="width: 40%">
                                                        <div class="col-lg-4">
                                                            <textarea cols="30" rows="3" runat="server" id="txtNotes"></textarea>
                                                        </div>
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
