﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class LeadAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTableByValue(ddlLeadStatus, "LeadStatus", "Select Lead Status");
                LoadCategoryFromGeneralTableByValue(ddlTourType, "LeadTourType", "Select Tour Type");
                LoadCategoryFromGeneralTableByValue(ddlPreferredContactTime, "LeadPreferredContactTime", "Select Preferred time");
                LoadCategoryFromGeneralTableByValue(ddlPreferredContactMethod, "LeadPreferredContactMethod", "Select Preferred contact method");
                LoadCategoryFromGeneralTableByValue(ddlTourGuide, "LeadTourGuide", "Select Tour guide");
                LoadCategoryFromGeneralTableByValue(ddlFilghtNeeded, "LeadFlightNeeded", "Select Flight needed");
                LoadCategoryFromGeneralTableByValue(ddlHotelType, "LeadHotelStar", "Select Hotel Type");
                LoadCategoryFromGeneralTableByValue(ddlMeals, "LeadMeal", "Select Meal");
                LoadCategoryFromGeneralTableByValue(ddlAdmissionTicket, "LeadAdmissionTicket", "Select Admission Ticket");
                LoadCategoryFromGeneralTableByValue(ddlInsurance, "LeadInsurance", "Select Insurance");
                LoadCategoryFromGeneralTableByValue(ddlCurrency, "Currency", "Select Currency");
                LoadCampaign(ddlCampaign);
                BindFormDetails();
            }
        }
       
        private void BindFormDetails()
        {
            if (TravelSession.Marketing.LeadId.HasValue)
            {
                DLLeadManagement obj = new DLLeadManagement();
                obj = BLLead.GetLeadById(TravelSession.Marketing.LeadId.Value);
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                if (ddlCampaign.Items.FindByValue(Convert.ToString(obj.CampaignID.Value)) != null)
                {
                    ddlCampaign.ClearSelection();
                    ddlCampaign.Items.FindByValue(Convert.ToString(obj.CampaignID.Value)).Selected = true;
                }
                if (ddlTourType.Items.FindByValue(Convert.ToString(obj.TourType.Value)) != null)
                {
                    ddlTourType.ClearSelection();
                    ddlTourType.Items.FindByValue(Convert.ToString(obj.TourType.Value)).Selected = true;
                }

                txtCompany.Text = obj.Company.Value;
                ddlTitle.Value = obj.Title.Value;
                txtGender.Text = obj.Gender.Value;
                txtSurName.Text = obj.SurName.Value;
                txtFirstName.Text = obj.FirstName.Value;
                txtEmail.Text = obj.Email.Value;
                txtAlternateEmail.Text = obj.AlternateEmail.Value;
                txtHomePhone.Text = obj.HomePhone.Value;
                txtMobileNo.Text = obj.MobileNo.Value;
                txtFaxNumber.Text = obj.FaxNumber.Value;
                txtBusinessPhone.Text = obj.BusinessPhone.Value;
                txtAddress.Text = obj.Address.Value;
                if (ddlPreferredContactTime.Items.FindByValue(Convert.ToString(obj.PreferredContactTime.Value)) != null)
                {
                    ddlPreferredContactTime.ClearSelection();
                    ddlPreferredContactTime.Items.FindByValue(Convert.ToString(obj.PreferredContactTime.Value)).Selected = true;
                }
                if (ddlPreferredContactMethod.Items.FindByValue(Convert.ToString(obj.PreferredContactMethod.Value)) != null)
                {
                    ddlPreferredContactMethod.ClearSelection();
                    ddlPreferredContactMethod.Items.FindByValue(Convert.ToString(obj.PreferredContactMethod.Value)).Selected = true;
                }
                if (!obj.PlannedDepartureDate.IsNull && IsDate(Convert.ToString(obj.PlannedDepartureDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.PlannedDepartureDate.Value));
                    txtPlannedDepartureDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtHowManyDays.Text = Convert.ToString(obj.HowManyDays.Value);
                txtHowManyPeople.Text = Convert.ToString(obj.HowManyPeople.Value);
                txtHowManyChild.Text = Convert.ToString(obj.HowManyChild.Value);
                txtPlaceOfDeparture.Text = obj.PlaceOfDeparture.Value;
                txtPlaceOfReturn.Text = obj.PlaceOfReturn.Value;
                if (ddlCurrency.Items.FindByValue(Convert.ToString(obj.Currency.Value)) != null)
                {
                    ddlCurrency.ClearSelection();
                    ddlCurrency.Items.FindByValue(Convert.ToString(obj.Currency.Value)).Selected = true;
                }
                if (ddlTourGuide.Items.FindByValue(Convert.ToString(obj.TourGuide.Value)) != null)
                {
                    ddlTourGuide.ClearSelection();
                    ddlTourGuide.Items.FindByValue(Convert.ToString(obj.TourGuide.Value)).Selected = true;
                }
                
                txtVehicle.Text = obj.Vehicle.Value;
                if (ddlHotelType.Items.FindByValue(Convert.ToString(obj.HotelType.Value)) != null)
                {
                    ddlHotelType.ClearSelection();
                    ddlHotelType.Items.FindByValue(Convert.ToString(obj.HotelType.Value)).Selected = true;
                }
                if (ddlMeals.Items.FindByValue(Convert.ToString(obj.Meals.Value)) != null)
                {
                    ddlMeals.ClearSelection();
                    ddlMeals.Items.FindByValue(Convert.ToString(obj.Meals.Value)).Selected = true;
                }
                if (ddlAdmissionTicket.Items.FindByValue(Convert.ToString(obj.AdmissionTickets.Value)) != null)
                {
                    ddlAdmissionTicket.ClearSelection();
                    ddlAdmissionTicket.Items.FindByValue(Convert.ToString(obj.AdmissionTickets.Value)).Selected = true;
                }
                if (ddlInsurance.Items.FindByValue(Convert.ToString(obj.Insurance.Value)) != null)
                {
                    ddlInsurance.ClearSelection();
                    ddlInsurance.Items.FindByValue(Convert.ToString(obj.Insurance.Value)).Selected = true;
                }
                txtLeadSource.Text = obj.LeadSource.Value;
                txtLeadOwner.Text = obj.LeadOwner.Value;
                if (ddlLeadStatus.Items.FindByValue(Convert.ToString(obj.LeadStatus.Value)) != null)
                {
                    ddlLeadStatus.ClearSelection();
                    ddlLeadStatus.Items.FindByValue(Convert.ToString(obj.LeadStatus.Value)).Selected = true;
                }
                
                txtCity.Text = obj.City.Value;
                txtState.Text = obj.State.Value;
                txtZipCode.Text = obj.ZipCode.Value;
                ddlCountry.Value = obj.Country.Value;
                txtWebPage.Text = obj.WebPage.Value;
                txtNotes.Text = obj.Notes.Value;
                txtReferredBy.Text = obj.ReferredBy.Value;
                txtHomeTown.Text = obj.HomeTown.Value;
                if (!obj.BirthDate.IsNull && IsDate(Convert.ToString(obj.BirthDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.BirthDate.Value));
                    txtBirthDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtContactsInterests.Text = obj.ContactsInterests.Value;
                txtRegion.Text = obj.Region.Value;
                txtResponse.Text = obj.Response.Value;
                if (!obj.Attachment.IsNull)
                    ucView.BindDocLink(obj.Attachment.Value);
            }
        }
        private void SaveDetails()
        {
            DLLeadManagement obj = new DLLeadManagement();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.CampaignID = Convert.ToInt32(ddlCampaign.SelectedValue);
            obj.TourType = ddlTourType.SelectedItem.Value;
            obj.Company = txtCompany.Text;
            obj.Title = ddlTitle.Value;
            obj.Gender = txtGender.Text;
            obj.SurName = txtSurName.Text;
            obj.FirstName = txtFirstName.Text;
            obj.Email = txtEmail.Text;
            obj.AlternateEmail = txtAlternateEmail.Text;
            obj.HomePhone = txtHomePhone.Text;
            obj.MobileNo = txtMobileNo.Text;
            obj.FaxNumber = txtFaxNumber.Text;
            obj.BusinessPhone = txtBusinessPhone.Text;
            obj.Address = txtAddress.Text;
            obj.PreferredContactTime = ddlPreferredContactTime.SelectedItem.Value;
            obj.PreferredContactMethod = ddlPreferredContactMethod.SelectedItem.Value;
            string resPlannedDepartureDate = txtPlannedDepartureDate.Text.Trim();
            if (resPlannedDepartureDate != "")
            {
                obj.PlannedDepartureDate = DateTime.ParseExact(resPlannedDepartureDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.HowManyDays =!string.IsNullOrEmpty(txtHowManyDays.Text) ? Convert.ToInt32(txtHowManyDays.Text) :0;
            obj.HowManyPeople = !string.IsNullOrEmpty(txtHowManyPeople.Text) ?Convert.ToInt32(txtHowManyPeople.Text) :0;
            obj.HowManyChild = !string.IsNullOrEmpty(txtHowManyChild.Text) ?Convert.ToInt32(txtHowManyChild.Text) :0;
            obj.PlaceOfDeparture = txtPlaceOfDeparture.Text;
            obj.PlaceOfReturn = txtPlaceOfReturn.Text;
            obj.Currency = ddlCurrency.SelectedItem.Value;
            obj.TourGuide = ddlTourGuide.SelectedItem.Value;
            obj.Vehicle = txtVehicle.Text;
            obj.HotelType = ddlHotelType.SelectedItem.Value;
            obj.Meals = ddlMeals.SelectedItem.Value;
            obj.AdmissionTickets = ddlAdmissionTicket.SelectedItem.Value;
            obj.Insurance = ddlInsurance.SelectedItem.Value;
            obj.LeadSource = txtLeadSource.Text;
            obj.LeadOwner = txtLeadOwner.Text;
            obj.LeadStatus = ddlLeadStatus.SelectedItem.Value;
            obj.City = txtCity.Text;
            obj.State = txtState.Text;
            obj.ZipCode = txtZipCode.Text;
            obj.Country = ddlCountry.Value;
            obj.WebPage = txtWebPage.Text;
            obj.Notes = txtNotes.Text;
            obj.ReferredBy = txtReferredBy.Text;
            obj.HomeTown = txtHomeTown.Text;
            string resBirthDate = txtBirthDate.Text.Trim();
            if (resBirthDate != "")
            {
                obj.BirthDate = DateTime.ParseExact(resBirthDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.ContactsInterests = txtContactsInterests.Text;
            obj.Region = txtRegion.Text;
            obj.Response = txtResponse.Text;
            if (flContractDoc.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = flContractDoc.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.Attachment = fileData;
            }
            if (TravelSession.Marketing.LeadId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Marketing.LeadId.Value;
                BLLead.UpdateLead(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLLead.SaveLead(obj);
            }

            ClearFormDetails();
            Response.Redirect("Lead.aspx", true);

        }
        private void ClearFormDetails()
        {

            ddlCampaign.ClearSelection();
            ddlTourType.ClearSelection();
            txtCompany.Text = "";
            txtGender.Text = "";
            txtSurName.Text = "";
            txtFirstName.Text = "";
            txtEmail.Text = "";
            txtAlternateEmail.Text = "";
            txtHomePhone.Text = "";
            txtMobileNo.Text = "";
            txtFaxNumber.Text = "";
            txtBusinessPhone.Text = "";
            txtAddress.Text = "";
            ddlPreferredContactMethod.ClearSelection();
            ddlPreferredContactTime.ClearSelection();
            txtPlannedDepartureDate.Text = "";
            txtHowManyDays.Text = "";
            txtHowManyPeople.Text = "";
            txtHowManyChild.Text = "";
            txtPlaceOfDeparture.Text = "";
            txtPlaceOfReturn.Text = "";
            ddlCurrency.ClearSelection();
            ddlTourGuide.ClearSelection();
            txtVehicle.Text = "";
            ddlHotelType.ClearSelection();
            ddlMeals.ClearSelection();
            ddlAdmissionTicket.ClearSelection();
            ddlInsurance.ClearSelection();
            txtLeadSource.Text = "";
            txtLeadOwner.Text = "";
            ddlLeadStatus.ClearSelection();
            txtCity.Text = "";
            txtState.Text = "";
            txtZipCode.Text = "";
            txtWebPage.Text = "";
            txtNotes.Text = "";
            txtReferredBy.Text = "";
            txtHomeTown.Text = "";
            txtBirthDate.Text = "";
            txtContactsInterests.Text = "";
            txtRegion.Text = "";
            txtResponse.Text = "";
        }
        protected void btnLeadManagementSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void Cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("Lead.aspx", true);
        }
    }
}