﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class ContactAddEdit : CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTableByValue(ddlStatus, "ContactStatus", "Select Status");
                BindFormDetails();
            }
        }
        private void BindFormDetails()
        {
            if (TravelSession.Marketing.ContactId.HasValue)
            {
                DLContactManagement obj = new DLContactManagement();
                obj = BLContact.GetContactById(TravelSession.Marketing.ContactId.Value);
                txtCompany.Text = obj.Company.Value;
                ddlTitle.Value = obj.Title.Value;
                txtSurName.Text = obj.SurName.Value;
                txtFirstName.Text = obj.FirstName.Value;
                txtEmail.Text = obj.Email.Value;
                txtAlternateEmail.Text = obj.AlternateEmail.Value;
                txtHomePhone.Text = obj.HomePhone.Value;
                txtMobileNo.Text = obj.MobileNo.Value;
                txtFaxNumber.Text = obj.FaxNumber.Value;
                txtBusinessPhone.Text = obj.BusinessPhone.Value;
                txtAddress.Text = obj.Address.Value;
                txtCity.Text = obj.City.Value;
                txtState.Text = obj.State.Value;
                txtZipCode.Text = obj.ZipCode.Value;
                ddlCountry.Value = obj.Country.Value;
                txtWebPage.Text = obj.WebPage.Value;
                txtNotes.Text = obj.Notes.Value;
                txtReferredBy.Text = obj.ReferredBy.Value;
                txtHomeTown.Text = obj.HomeTown.Value;
                if (!obj.BirthDate.IsNull && IsDate(Convert.ToString(obj.BirthDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.BirthDate.Value));
                    txtBirthDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                txtContactsInterests.Text = obj.ContactsInterests.Value;
                if (ddlStatus.Items.FindByValue(Convert.ToString(obj.Status.Value)) != null)
                {
                    ddlStatus.ClearSelection();
                    ddlStatus.Items.FindByValue(Convert.ToString(obj.Status.Value)).Selected = true;
                }
                txtRegion.Text = obj.Region.Value;
                txtResponse.Text = obj.Response.Value;
               
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                if (!obj.Attachment.IsNull)
                    ucView.BindDocLink(obj.Attachment.Value);
            }
        }
        private void SaveDetails()
        {
            DLContactManagement obj = new DLContactManagement();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.Company = txtCompany.Text;
            obj.Title = ddlTitle.Value;
            obj.SurName = txtSurName.Text;
            obj.FirstName = txtFirstName.Text;
            obj.Email = txtEmail.Text;
            obj.AlternateEmail = txtAlternateEmail.Text;
            obj.HomePhone = txtHomePhone.Text;
            obj.MobileNo = txtMobileNo.Text;
            obj.FaxNumber = txtFaxNumber.Text;
            obj.BusinessPhone = txtBusinessPhone.Text;
            obj.Address = txtAddress.Text;
            obj.City = txtCity.Text;
            obj.State = txtState.Text;
            obj.ZipCode = txtZipCode.Text;
            obj.Country = ddlCountry.Value;
            obj.WebPage = txtWebPage.Text;
            obj.Notes = txtNotes.Text;
            obj.ReferredBy = txtReferredBy.Text;
            obj.HomeTown = txtHomeTown.Text;
            string resBirthDate = txtBirthDate.Text.Trim();
            if (resBirthDate != "")
            {
                obj.BirthDate = DateTime.ParseExact(resBirthDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.ContactsInterests = txtContactsInterests.Text;
            obj.Status = ddlStatus.SelectedItem.Value;
            obj.Region = txtRegion.Text;
            obj.Response = txtResponse.Text;
            if (flContractDoc.PostedFile.ContentLength > 0)
            {
                HttpPostedFile File = flContractDoc.PostedFile;
                int size = File.ContentLength;
                string name = File.FileName;
                int position = name.LastIndexOf("\\");
                name = name.Substring(position + 1);
                string contentType = File.ContentType;
                byte[] fileData = new byte[size];
                File.InputStream.Read(fileData, 0, size);
                obj.Attachment = fileData;
            }

            if (TravelSession.Marketing.ContactId.HasValue)
            {
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.ID = TravelSession.Marketing.ContactId.Value;
                BLContact.UpdateContact(obj);
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                BLContact.SaveContact(obj);
            }

            ClearFormDetails();
            Response.Redirect("Contact.aspx", true);

        }
        private void ClearFormDetails()
        {
            txtCompany.Text = "";
            ddlTitle.SelectedIndex = 0;
            txtSurName.Text = "";
            txtFirstName.Text = "";
            txtEmail.Text = "";
            txtAlternateEmail.Text = "";
            txtHomePhone.Text = "";
            txtMobileNo.Text = "";
            txtFaxNumber.Text = "";
            txtBusinessPhone.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZipCode.Text = "";
            ddlCountry.SelectedIndex = -1;
            txtWebPage.Text = "";
            txtNotes.Text = "";
            txtReferredBy.Text = "";
            txtHomeTown.Text = "";
            txtBirthDate.Text = "";
            txtContactsInterests.Text = "";
            ddlStatus.ClearSelection();
            txtRegion.Text = "";
            txtResponse.Text = "";
        }
        protected void btnContactManagementSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        protected void cancel_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("Contact.aspx", true);
        }
    }
}