﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" CodeBehind="Project.aspx.cs" Inherits="LTS_CRM.Marketing.Project" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Project Management</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Project List</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">

                                      <div class="row">
                                            <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                <div class="input-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                                <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="lnkAddNew" OnClick="lnkAddNew_Click" runat="server"><i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span></asp:LinkButton>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="MarketingProjectRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                            <HeaderTemplate>
                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                    <thead>
                                                        <tr>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="No" />
                                                            </th>
                                                            <th class="hasinput" style="width: 15%">
                                                                <input type="text" class="form-control" placeholder="Project Name" />
                                                            </th>
                                                            <th class="hasinput" style="width: 10%">
                                                                <input type="text" class="form-control" placeholder="Initiate By" />
                                                            </th>
                                                            <th class="hasinput" style="width: 10%">
                                                                <input type="text" class="form-control" placeholder="Starting Date" />
                                                            </th>
                                                            <th class="hasinput" style="width: 10%">
                                                                <input type="text" class="form-control" placeholder="Planned Ending Date" />
                                                            </th>

                                                            <th class="hasinput" style="width: 10%"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Project Name</th>
                                                            <th data-hide="phone">Initiate By</th>
                                                            <th data-hide="phone">Starting Date</th>
                                                            <th data-hide="phone">Planned Ending Date</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "ProjectName")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "InitiateBy")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "ProjectStartingDate" ,"{0:dd/MM/yyyy}")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "ProjectPlannedEndingDate" ,"{0:dd/MM/yyyy}")%></td>
                                                    <td>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" OnClick="buttonEdit_Click" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true">
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" OnClick="buttonDelete_Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>      
            </table>                      
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>


        </div>
    </div>
</asp:Content>
