﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class ChannelAddEdit : CRMPage
    {
       
        #region Comment Page methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                LoadcboCompanyID(cboCompanyID);
                LoadCategoryFromGeneralTableByValue(ddlStatus, "ChannelStatus", "Select Status");
                LoadCategoryFromGeneralTableByValue(ddlStarRating, "ChannelStarRating", "Select Rating");
                BindFormDetails();
                BindContactGrid();
            }
            setTabIndex();
        }
        private void setTabIndex()
        {
            if (TravelSession.Marketing.ChannelId.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Marketing.ChannelId.Value)))
            {
                tab2.Visible = true;
                tab2_pan.Visible = tab2.Visible;
            }
            else
            {
                tab2.Visible = false;
                tab2_pan.Visible = tab2.Visible;
            }

            string selectedTab = "";
            if (Request.Cookies["activetab"] != null)
            {
                selectedTab = Request.Cookies["activetab"].Value;
            }

            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");

            tab1_pan.Attributes.Remove("class");
            tab2_pan.Attributes.Remove("class");

            tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white ");
            tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white ");

            tab1_pan.Attributes.Add("class", "tab-pane ");
            tab2_pan.Attributes.Add("class", "tab-pane ");

            switch (selectedTab)
            {
                case "#tab2_pan":
                    {
                        if (!tab2.Visible)
                            goto default;

                        tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                        tab2_pan.Attributes.Add("class", "tab-pane active");
                        break;
                    }

                default:
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                    tab1_pan.Attributes.Add("class", "tab-pane active");
                    break;
            }
        }
        #endregion

        #region Basic Details
        private void BindFormDetails()
        {
            if (TravelSession.Marketing.ChannelId.HasValue)
            {
                DLMarketingChannel obj = BLL.BLMarketingChannel.GetChannelByPk(TravelSession.Marketing.ChannelId.Value);
                if (cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)) != null)
                {
                    cboCompanyID.ClearSelection();
                    cboCompanyID.Items.FindByValue(Convert.ToString(obj.CompanyID.Value)).Selected = true;
                }
                txtContractedBy.Text = obj.ContractedBy.Value;
                txtCompanyName.Text = obj.CompanyName.Value;
                txtCompanyWebsite.Text = obj.CompanyWebsite.Value;
                txtUsername.Text = obj.Username.Value;
                txtPassword.Text = obj.Password.Value;
                txtCompanyTelephone.Text = obj.CompanyTelephone.Value;
                txtCompanyEmail.Text = obj.CompanyEmail.Value;
                txtCompanyAddress1.Text = obj.CompanyAddress1.Value;
                txtCompanyAddress2.Text = obj.CompanyAddress2.Value;
                txtCity.Text = obj.City.Value;
                cmbcountry.Value = obj.Country.Value;
                txtPostCode.Text = obj.PostCode.Value;
                txtAdvertisingCost.Text = obj.AdvertisingCost.Value;
                txtPaymentTerms.Text = obj.PaymentTerms.Value;
                txtBlackList.Text = obj.BlackList.Value;

                txtMainBusiness.Text = obj.MainBusiness.Value;
                txtHowToUseMarketingChannel.Text = obj.HowToUseMarketingChannel.Value;
                txtComments.Text = obj.Comments.Value;
                txtFirstDayOfCollabration.Text = obj.FirstDayOfCollabration.Value;
                if (!obj.ContractExpireDate.IsNull && IsDate(Convert.ToString(obj.ContractExpireDate.Value)))
                {
                    DateTime dt = new DateTime();
                    dt = Convert.ToDateTime(Convert.ToString(obj.ContractExpireDate.Value));
                    txtContractExpireDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                }
                if (ddlStatus.Items.FindByValue(Convert.ToString(obj.Status.Value)) != null)
                {
                    ddlStatus.ClearSelection();
                    ddlStatus.Items.FindByValue(Convert.ToString(obj.Status.Value)).Selected = true;
                }
                if (ddlStarRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)) != null)
                {
                    ddlStarRating.ClearSelection();
                    ddlStarRating.Items.FindByValue(Convert.ToString(obj.Rating.Value)).Selected = true;
                }

                ddlTitle1.Value = obj.Title.Value;
                txtcontactEmail1.Text = obj.Email.Value;
                txtcontactFax1.Text = obj.FaxNo.Value;
                txtcontactMobile1.Text = obj.MobileNo.Value;
                txtcontactTelephone1.Text = obj.DirectPhone.Value;
                txtDepartment1.Text = obj.Department.Value;
                txtFacebook1.Text = obj.Facebook.Value;
                txtFirstName1.Text = obj.FirstName.Value;
                txtLinkedin1.Text = obj.LinkedIn.Value;
                txtPosition1.Text = obj.Position.Value;
                txtSkype1.Text = obj.Skype.Value;
                txtSurName1.Text = obj.SurName.Value;
                txtTwitter1.Text = obj.Twitter.Value;
                txtWeChat1.Text = obj.WeChat.Value;
                txtWhatsapp1.Text = obj.Whatsup.Value;
            }
        }
        protected void btnMarketingChannelSave_ServerClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SaveDetails();
            }
        }

        private void SaveDetails()
        {
            LTSCRM_DL.DLMarketingChannel obj = new LTSCRM_DL.DLMarketingChannel();
            obj.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
            obj.ContractedBy = txtContractedBy.Text;
            obj.CompanyName = txtCompanyName.Text;
            obj.CompanyWebsite = txtCompanyWebsite.Text;
            obj.Username = txtUsername.Text;
            obj.Password = txtPassword.Text;
            obj.CompanyTelephone = txtCompanyTelephone.Text;
            obj.CompanyEmail = txtCompanyEmail.Text;
            obj.CompanyAddress1 = txtCompanyAddress1.Text;
            obj.CompanyAddress2 = txtCompanyAddress2.Text;
            obj.City = txtCity.Text;
            obj.Country = cmbcountry.Value;
            obj.PostCode = txtPostCode.Text;
            obj.AdvertisingCost = txtAdvertisingCost.Text;
            obj.PaymentTerms = txtPaymentTerms.Text;
            obj.BlackList = txtBlackList.Text;
            obj.Rating = ddlStarRating.SelectedItem.Value;
            obj.MainBusiness = txtMainBusiness.Text;
            obj.HowToUseMarketingChannel = txtHowToUseMarketingChannel.Text;
            obj.Comments = txtComments.Text;
            obj.FirstDayOfCollabration = txtFirstDayOfCollabration.Text;
            string resContractExpireDate = txtContractExpireDate.Text.Trim();
            if (resContractExpireDate != "")
            {
                obj.ContractExpireDate = DateTime.ParseExact(resContractExpireDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            obj.Status = ddlStatus.SelectedItem.Value;

            obj.Title = ddlTitle1.Value;
            obj.SurName = txtSurName1.Text;
            obj.FirstName = txtFirstName1.Text;
            obj.Email = txtcontactEmail1.Text;
            obj.FaxNo = txtcontactFax1.Text;
            obj.MobileNo = txtcontactMobile1.Text;
            obj.DirectPhone = txtcontactTelephone1.Text;
            obj.Department = txtDepartment1.Text;
            obj.Facebook = txtFacebook1.Text;
            obj.LinkedIn = txtLinkedin1.Text;
            obj.Position = txtPosition1.Text;
            obj.Skype = txtSkype1.Text;
            obj.Twitter = txtTwitter1.Text;
            obj.WeChat = txtWeChat1.Text;
            obj.Whatsup = txtWhatsapp1.Text;

            if (TravelSession.Marketing.ChannelId.HasValue)
            {
                obj.ID = TravelSession.Marketing.ChannelId.Value;
                obj.UpdatedBy = TravelSession.Current.UserName;
                obj.UpdatedDate = DateTime.Now;
                obj.Update();
            }
            else
            {
                obj.CreatedBy = TravelSession.Current.UserName;
                obj.CreatedDate = DateTime.Now;
                obj.Insert();
            }
            ClearFormDetails();
            Response.Redirect("~/Marketing/Channel.aspx", false);
        }

        private void ClearFormDetails()
        {
            TravelSession.Marketing.ChannelId = null;

            txtContractedBy.Text = "";
            txtCompanyName.Text = "";
            txtCompanyWebsite.Text = "";
            txtUsername.Text = "";
            txtPassword.Text = "";
            txtCompanyTelephone.Text = "";
            txtCompanyEmail.Text = "";
            txtCompanyAddress1.Text = "";
            txtCompanyAddress2.Text = "";
            txtCity.Text = "";
            cmbcountry.SelectedIndex = -1;
            txtPostCode.Text = "";
            txtAdvertisingCost.Text = "";
            txtPaymentTerms.Text = "";
            txtBlackList.Text = "";
            ddlStarRating.ClearSelection();
            txtMainBusiness.Text = "";
            txtHowToUseMarketingChannel.Text = "";
            txtComments.Text = "";
            txtFirstDayOfCollabration.Text = "";
            txtContractExpireDate.Text = "";
            ddlStatus.ClearSelection();

        }
        #endregion

        #region Contact Details
        private void BindContactGrid()
        {
            if (TravelSession.Marketing.ChannelId.HasValue)
            {
                rptContactRepeater.DataSource = BLMarketingContacts.GetChannelContactsByChannelId(TravelSession.Marketing.ChannelId.Value);
                rptContactRepeater.DataBind();
            }
        }
        protected void buttonEditContact_Click(object sender, EventArgs e)
        {
            TravelSession.Marketing.ChannelContactId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            BindContactForm();
        }

        private void BindContactForm()
        {
            DLMarketingContacts obj = new DLMarketingContacts();
            obj.ID = TravelSession.Marketing.ChannelContactId.Value;
            obj.SelectOne();
            ddlTitle.Value = obj.Title.Value;
            txtSurName.Text = obj.SurName.Value;
            txtFirstName.Text = obj.FirstName.Value;
            txtPosition.Text = obj.Position.Value;
            txtDepartment.Text = obj.Department.Value;
            txtEmail.Text = obj.Email.Value;
            txtDirectPhone.Text = obj.DirectPhone.Value;
            txtMobileNo.Text = obj.MobileNo.Value;
            txtFaxNo.Text = obj.FaxNo.Value;
            txtWeChat.Text = obj.WeChat.Value;
            txtWhatsup.Text = obj.Whatsup.Value;
            txtSkype.Text = obj.Skype.Value;
            txtFacebook.Text = obj.Facebook.Value;
            txtTwitter.Text = obj.Twitter.Value;
            txtLinkedIn.Text = obj.LinkedIn.Value;
        }

        protected void buttonDeleteContact_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLMarketingContacts.Delete(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindContactGrid();
        }

        protected void btnCancelContact_ServerClick(object sender, EventArgs e)
        {
            ClearFormContactDetails();
        }

        private void ClearFormContactDetails()
        {
            TravelSession.Marketing.ChannelContactId = null;
            ddlTitle.SelectedIndex = 0;
            txtSurName.Text = "";
            txtFirstName.Text = "";
            txtPosition.Text = "";
            txtDepartment.Text = "";
            txtEmail.Text = "";
            txtDirectPhone.Text = "";
            txtMobileNo.Text = "";
            txtFaxNo.Text = "";
            txtWeChat.Text = "";
            txtWhatsup.Text = "";
            txtSkype.Text = "";
            txtFacebook.Text = "";
            txtTwitter.Text = "";
            txtLinkedIn.Text = "";
        }

        private void SaveContactDetails()
        {
            if (TravelSession.Marketing.ChannelId.HasValue)
            {
                DLMarketingContacts obj = new DLMarketingContacts();
                obj.MarketingChannelID = TravelSession.Marketing.ChannelId.Value;
                obj.Title = ddlTitle.Value;
                obj.SurName = txtSurName.Text;
                obj.FirstName = txtFirstName.Text;
                obj.Position = txtPosition.Text;
                obj.Department = txtDepartment.Text;
                obj.Email = txtEmail.Text;
                obj.DirectPhone = txtDirectPhone.Text;
                obj.MobileNo = txtMobileNo.Text;
                obj.FaxNo = txtFaxNo.Text;
                obj.WeChat = txtWeChat.Text;
                obj.Whatsup = txtWhatsup.Text;
                obj.Skype = txtSkype.Text;
                obj.Facebook = txtFacebook.Text;
                obj.Twitter = txtTwitter.Text;
                obj.LinkedIn = txtLinkedIn.Text;
                obj.MainContact = false;
                if (TravelSession.Marketing.ChannelContactId.HasValue)
                {
                    obj.ID = TravelSession.Marketing.ChannelContactId.Value;
                    obj.UpdatedBy = TravelSession.Current.UserName;
                    obj.UpdatedDate = DateTime.Now;
                    obj.Update();
                }
                else
                {
                    obj.CreatedBy = TravelSession.Current.UserName;
                    obj.CreatedDate = DateTime.Now;
                    obj.Insert();
                }
                BindContactGrid();
                ClearFormContactDetails();
            }
        }
        protected void btnMarketingContactsSave_ServerClick(object sender, EventArgs e)
        {
            SaveContactDetails();
        }
        #endregion

    }
}