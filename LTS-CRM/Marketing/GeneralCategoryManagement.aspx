﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralCategoryManagement.aspx.cs" Inherits="LTS_CRM.Marketing.GeneralCategoryManagement" ClientIDMode="Static"  MasterPageFile="~/master/MasterAdmin.Master"%>

<asp:content id="cntGen" contentplaceholderid="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>General Category Management</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Item List</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                            <div class="tab-pane active" id="tab1_pan" runat="server">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>Category</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:DropDownList runat="server" ID="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
                                                    </div>
                                                    <div class="col-md-3"> 
                                                <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="lnkAddNew" OnClick="lnkAddNew_Click" runat="server"><i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span></asp:LinkButton>

                                                    </div>
                                                </div>
                                                
                                                <asp:Repeater ID="rptGeneralCategoryItems" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                    <HeaderTemplate>
                                                        <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                            <thead>
                                                                <tr>
                                                                    <th class="hasinput" style="width: 5%">
                                                                        <input type="text" class="form-control" placeholder="ID" />
                                                                    </th>
                                                                    <th class="hasinput" style="width: 5%">
                                                                        <input type="text" class="form-control" placeholder="Category" />
                                                                    </th>
                                                                    <th class="hasinput" style="width: 5%">
                                                                        <input type="text" class="form-control" placeholder="Title" />
                                                                    </th>
                                                                    <th class="hasinput" style="width: 5%">
                                                                        <input type="text" class="form-control" placeholder="Description" />
                                                                    </th> 
                                                                </tr>
                                                                <tr>
                                                                    <th>ID</th>
                                                                    <th>Category</th>
                                                                    <th>Title</th>
                                                                    <th>Description</th>
                                                                     
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# DataBinder.Eval(Container.DataItem, "Category")%></td>
                                                            <td><%# DataBinder.Eval(Container.DataItem, "Title")%></td>
                                                            <td><%# DataBinder.Eval(Container.DataItem, "Description")%></td>
                                                            
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
</table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                             
                                        </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
 
        </div>
    </div>
</asp:content>
