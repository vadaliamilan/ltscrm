﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" CodeBehind="GeneralCategoryAddEdit.aspx.cs" Inherits="LTS_CRM.Marketing.GeneralCategoryAddEdit" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>General Categories</li>
                </ol>
            </div>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Add General Items</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <div class="tab-pane active" id="tab1_pan" runat="server">
                                            <div id="GeneralCategoryItemsform" class="row smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Category
                                                            </label>
                                                            <label class="input">
                                                                <asp:DropDownList ID="ddlCategory" runat="server" ValidationGroup="addGeneralCategoryItems"  class="form-control"></asp:DropDownList>
                                                            </label>
                                                        </section>
                                                        <section class="col col-3">
                                                            <label class="label">
                                                                Title
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox ID="txtTitle" runat="server" placeholder="Title" ValidationGroup="addGeneralCategoryItems" Text="" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvtitle" runat="server"  ControlToValidate="txtTitle" Font-Bold="true" ValidationGroup="addGeneralCategoryItems" ForeColor="Red" SetFocusOnError="true" Display="Dynamic" ErrorMessage="please enter Title" Text="*" ></asp:RequiredFieldValidator>
                                                            </label>
                                                        </section>
                                                        <section class="col col-5">
                                                            <label class="label">
                                                                Description
                                                            </label>
                                                            <label class="input">
                                                                <asp:TextBox TextMode="MultiLine" Rows="4" Columns="25" ID="txtDescription" runat="server" placeholder="Description" ValidationGroup="addGeneralCategoryItems" Text="" class="form-control"></asp:TextBox>
                                                            </label>
                                                        </section>
                                                    </div>
                                                </fieldset>
                                                <footer>
                                                    <button id="btnGeneralCategoryItemsSave" onserverclick="btnGeneralCategoryItemsSave_ServerClick" runat="server" validationgroup="addGeneralCategoryItems" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit">
                                                        <span style="width: 80px">Save </span>
                                                    </button>
                                                    <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                                                </footer>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>

        </div>
    </div>
</asp:Content>
