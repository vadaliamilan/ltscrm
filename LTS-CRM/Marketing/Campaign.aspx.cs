﻿using LTS_CRM.BLL;
using LTS_CRM.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class Campaign :  CRMPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearTabCookies();
                BindGrid();
            }
        }
        protected void BindGrid()
        {
            rptMarketingCampaign.DataSource = BLCampaign.GetCampaignList();
            rptMarketingCampaign.DataBind();
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            TravelSession.Marketing.CampaignId = null;
            Response.Redirect("~/Marketing/CampaignAddEdit.aspx", true);
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Marketing.CampaignId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/Marketing/CampaignAddEdit.aspx", true);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            bool isSuccess = BLCampaign.DeleteCampaign(Convert.ToInt32((sender as LinkButton).CommandArgument));
            if (isSuccess)
                BindGrid();
        }
    }
}