﻿<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" MasterPageFile="~/master/MasterAdmin.Master" CodeBehind="Channel.aspx.cs" Inherits="LTS_CRM.Marketing.Channel" %>

<asp:Content ID="cntGen" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Literal ID="output" runat="server" />
        <div class="well">
            <div id="ribbon">
                <ol class="breadcrumb">
                    <li>Marketing Management</li>
                    <li>Channel</li>
                </ol>
            </div>
            <p>
                &nbsp;
            </p>

            <section id="Section1">
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-blueDark" data-widget-editbutton="false" id="empdir">
                            <header>
                                <h2>Channel List</h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    &nbsp;
                                </div>
                                <div class="widget-body no-padding">
                                    <div class="widget-body-toolbar">
                                        <div class="row">
                                            <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                                <div class="input-group">
                                                    &nbsp;
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                                <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="lnkAddNew" OnClick="lnkAddNew_Click" runat="server"><i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span></asp:LinkButton>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="MarketingChannelRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                            <HeaderTemplate>
                                                <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="ID" />
                                                            </th>

                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Company Name" />
                                                            </th>  
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Email" />
                                                            </th>
                                                             <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Mobile No" />
                                                            </th> 
                                                             <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="City" />
                                                            </th>  
                                                            <th class="hasinput" style="width: 5%">
                                                                <input type="text" class="form-control" placeholder="Country" />
                                                            </th> 
                                                            <th class="hasinput" style="width: 10%"></th>
                                                        </tr>
                                                        <tr>
                                                            <th>ID</th>
                                                            <th data-class="expand">Company Name</th>
                                                             <th data-hide="phone">Email</th>
                                                             <th data-hide="phone">Mobile No</th>
                                                            <th data-hide="phone">City</th>
                                                            <th data-hide="phone">Country</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "CompanyName")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "CompanyEmail")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "CompanyTelephone")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "City")%></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "Country")%></td>
                                                    <td>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" OnClick="buttonEdit_Click" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true">
<i class="fa fa-edit"></i><span class="hidden-mobile"></span></asp:LinkButton>
                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" OnClick="buttonDelete_Click" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;">
<i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>      
            </table>                      
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
            <p>
                &nbsp;
            </p>

        </div>
    </div>
</asp:Content>
