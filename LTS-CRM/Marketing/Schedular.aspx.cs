﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Marketing
{
    public partial class Schedular : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void C1_DayRender(object sender, System.Web.UI.WebControls.DayRenderEventArgs e)
        {
            bool notTouched = false;
            StringBuilder temp = new StringBuilder();
            //Add Task
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = BLL.BLTaskList.GetTaskList();
            notTouched = true;
            string strColor = null;
            foreach (DataRow row in dt.Rows)
            {

                if (e.Day.Date == Convert.ToDateTime(row["DueDate"]))
                {
                    strColor = GetColor("Task");
                    temp = new StringBuilder();
                    temp.Append("<br>");
                    temp.Append("<span style=\"font-family:Verdana; font-weight:bold;font-size:10px; color:");
                    temp.Append(strColor);
                    temp.Append("\">");
                    temp.Append(row["Subject"].ToString());
                    temp.Append("</span>");
                    e.Cell.Controls.Add(new LiteralControl(temp.ToString()));
                }

            }
            //Add Appointment
            System.Data.DataTable dt1 = new System.Data.DataTable();
            dt1 = BLL.BLAppointmentList.GetAppointmentList();
            notTouched = true;
            strColor = null;
            foreach (DataRow row in dt1.Rows)
            {

                if (e.Day.Date == Convert.ToDateTime(row["AppointmentDate"]))
                {
                    strColor = GetColor("Appointment");
                    temp = new StringBuilder();
                    temp.Append("<br>");
                    temp.Append("<span style=\"font-family:Verdana; font-weight:bold;font-size:10px; color:");
                    temp.Append(strColor);
                    temp.Append("\">");
                    temp.Append(row["Subject"].ToString());
                    temp.Append("</span>");
                    e.Cell.Controls.Add(new LiteralControl(temp.ToString()));
                }

            }
        }

        private string GetColor(string type)
        {
            string StrColor = string.Empty;
            if (type == "Task")
            {
                StrColor = "Orange";
            }
            if (type == "Appointment")
            {
                StrColor = "Red";
            }
            return StrColor;
        }
    }
}