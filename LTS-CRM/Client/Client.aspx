﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Client.aspx.cs" Inherits="LTS_CRM.Client.Client" EnableEventValidation="false" MasterPageFile="~/master/MasterAdmin.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Client Management</li>
                <li>Client Management</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Client List </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">
                                    <div class="row">
                                        <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                            <div class="input-group">
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                            <asp:LinkButton class="btn bg-color-blueDark txt-color-white" id="addClient"  runat="server" OnClick="addClient_Click" >
                                                <i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span>
                                            </asp:LinkButton>
                                           <%-- <a class="btn bg-color-blueDark txt-color-white" id="ExportClient" onserverclick="ExportClient_ServerClick" runat="server">
                                                <i class="fa fa-table"></i><span class="hidden-mobile">Export </span>
                                            </a>--%>
                                        </div>

                                    </div>
                                    <asp:Repeater ID="ClientRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                        <HeaderTemplate>
                                            <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="hasinput" style="width: 5%">
                                                            <input type="text" class="form-control" placeholder="No" />
                                                        </th>
                                                        <th class="hasinput" style="width: 20%">
                                                            <input type="text" class="form-control" placeholder="Company Name" />
                                                        </th>
                                                        <th class="hasinput" style="width: 20%">
                                                            <input type="text" class="form-control" placeholder="Country" />
                                                        </th>
                                                        <th class="hasinput" style="width: 15%">
                                                            <input type="text" class="form-control" placeholder="City" />
                                                        </th>
                                                        <th class="hasinput" style="width: 15%">
                                                            <input type="text" class="form-control" placeholder="Clients/Agent" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%">
                                                            <input type="text" class="form-control" placeholder="Company/Individual" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%"></th>
                                                    </tr>
                                                    <tr>
                                                        <th data-hide="phone">No</th>
                                                        <th data-class="expand">Company Name</th>
                                                        <th data-hide="phone">Country</th>
                                                        <th data-hide="phone">City</th>
                                                        <th data-hide="phone">Clients/Agent</th>
                                                        <th data-hide="phone">Company/Individual</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "CompanyName")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "Country")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "City")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "DirectOrAgenet")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "CompanyOrInd")%></td>
                                                <td>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonEdit_Click"><i class="fa fa-edit"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record, it will delete client as well as client contact?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                </td>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</asp:Content>
