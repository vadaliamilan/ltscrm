﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientContact.aspx.cs" MasterPageFile="~/master/MasterAdmin.Master" Inherits="LTS_CRM.Client.ClientContact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Client Management</li>
                <li>Client Contact Management</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Client Contact List </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">
                                    <div class="row">
                                        <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                            <div class="input-group">
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                            <a class="btn bg-color-blueDark txt-color-white" id="addClientContact"  onserverclick="addClientContact_ServerClick" runat="server">
                                                <i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span>
                                            </a>
                                        </div>

                                    </div>
                                    <asp:Repeater ID="ClientContactRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                        <HeaderTemplate>
                                            <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th class="hasinput" style="width: 5%">
                                                            <input type="text" class="form-control" placeholder="Sur Name" />
                                                        </th>
                                                        <th class="hasinput" style="width: 20%">
                                                            <input type="text" class="form-control" placeholder="First Name" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%">
                                                            <input type="text" class="form-control" placeholder="Email" />
                                                        </th>
                                                        <th class="hasinput" style="width: 25%">
                                                            <input type="text" class="form-control" placeholder="Department" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%">
                                                            <input type="text" class="form-control" placeholder="Contact No" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%"></th>
                                                    </tr>
                                                    <tr>
                                                        <th data-hide="phone">Sur Name</th>
                                                        <th data-class="expand">First Name</th>
                                                        <th data-hide="phone">Email</th>
                                                        <th data-hide="phone">Department</th>
                                                        <th data-hide="phone">Contact No</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# DataBinder.Eval(Container.DataItem, "SurName")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "FirstName")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "Email")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "Department")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "MobileNo")%></td>
                                                <td>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonEdit" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonEdit_Click"><i class="fa fa-edit"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="buttonDelete" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="buttonDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record, it will delete client contact?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                </td>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</asp:Content>
