﻿using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Client
{
    public partial class ClientContact : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindGrid();
        }

        protected void BindGrid()
        {
            if (TravelSession.Current.ClientID.HasValue)
            {
                DataTable dataTableClientContact = BLClientContacts.GetClientContactList(TravelSession.Current.ClientID.Value);
                ClientContactRepeater.DataSource = dataTableClientContact;
                ClientContactRepeater.DataBind();
            }
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.ClientContactID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/client/ClientContactAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int clientContactId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLClientContacts.DeleteClientContact(clientContactId);
            if (isSuccess)
                BindGrid();
        }

        protected void addClientContact_ServerClick(object sender, EventArgs e)
        {
            TravelSession.Current.ClientContactID = null;
            Response.Redirect("ClientContactAddEdit.aspx", false);
        }
    }
}