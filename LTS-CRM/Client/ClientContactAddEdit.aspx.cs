﻿using LTSCRM_DL;
using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Client
{
    public partial class ClientContactAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                //if (String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.UserName)))
                //{
                //    Response.Redirect("~/SignIn.aspx");
                //}

                if (TravelSession.Current.ClientContactID.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.ClientContactID.Value)))
                {
                    BindClientContact(Convert.ToInt32(TravelSession.Current.ClientContactID.Value));
                }

            }
        }

        private void BindClientContact(int ID)
        {
            DLClientContacts objClientConact= new DLClientContacts();
            objClientConact.ID = ID;
            objClientConact = BLClientContacts.GetClientContactById(objClientConact);
            ddlTitle1.Value = objClientConact.Title.Value;
            txtLastName1.Text = objClientConact.SurName.Value;
            txtFirstName1.Text = objClientConact.FirstName.Value;
            txtPosition1.Text = objClientConact.Position.Value;
            txtDepartment1.Text = objClientConact.Department.Value;
            txtcontactEmail1.Text = objClientConact.Email.Value;
            txtcontactTelephone1.Text = objClientConact.DirectPhone.Value;
            txtcontactMobile1.Text = objClientConact.MobileNo.Value;
            txtcontactFax1.Text = objClientConact.FaxNo.Value;
            txtWeChat1.Text = objClientConact.WeChat.Value;
            txtWhatsapp1.Text = objClientConact.Whatsup.Value;
            txtSkype1.Text = objClientConact.Skype.Value;
            txtFacebook1.Text = objClientConact.Facebook.Value;
            txtTwitter1.Text = objClientConact.Twitter.Value;
            txtLinkedin1.Text = objClientConact.LinkedIn.Value;
        }

        /// <summary>
        /// Save Client Another Contacts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btncontactAnotherSave_Click(object sender, EventArgs e)
        {
            DLClientContacts objClientContact = new DLClientContacts();
            if (Page.IsValid)
            {
                objClientContact.ClientID = TravelSession.Current.ClientID.Value;
                objClientContact.Title = ddlTitle1.Items[ddlTitle1.SelectedIndex].Text;
                objClientContact.SurName = txtLastName1.Text.Trim();
                objClientContact.FirstName = txtFirstName1.Text.Trim();
                objClientContact.MainContact = false;
                objClientContact.Position = txtPosition1.Text.Trim();
                objClientContact.Department = txtDepartment1.Text.Trim();
                objClientContact.Email = txtcontactEmail1.Text.Trim();
                objClientContact.DirectPhone = txtcontactTelephone1.Text.Trim();
                objClientContact.MobileNo = txtcontactMobile1.Text.Trim();
                objClientContact.FaxNo = txtcontactFax1.Text.Trim();
                objClientContact.WeChat = txtWeChat1.Text.Trim();
                objClientContact.Whatsup = txtWhatsapp1.Text.Trim();
                objClientContact.Skype = txtSkype1.Text.Trim();
                objClientContact.Facebook = txtFacebook1.Text.Trim();
                objClientContact.Twitter = txtTwitter1.Text.Trim();
                objClientContact.LinkedIn = txtLinkedin1.Text.Trim();
                if (TravelSession.Current.ClientContactID.HasValue)
                {
                    objClientContact.UpdatedBy = TravelSession.Current.UserName;
                    objClientContact.UpdatedDate = DateTime.Now;
                    objClientContact.ID = TravelSession.Current.ClientContactID.Value;
                    objClientContact.Update();
                }
                else
                {
                    objClientContact.CreatedBy = TravelSession.Current.UserName;
                    objClientContact.CreatedDate = DateTime.Now;
                    objClientContact.Insert();
                }
            }

            ClearControls();
            Response.Redirect("ClientContact.aspx", false);
        }

        private void ClearControls()
        {
            TravelSession.Current.ClientContactID = null;
            ddlTitle1.SelectedIndex = -1;
            txtLastName1.Text = "";
            txtFirstName1.Text = "";
            txtPosition1.Text = "";
            txtDepartment1.Text = "";
            txtcontactEmail1.Text = "";
            txtcontactTelephone1.Text = "";
            txtcontactMobile1.Text = "";
            txtcontactFax1.Text = "";
            txtWeChat1.Text = "";
            txtWhatsapp1.Text = "";
            txtSkype1.Text = "";
            txtFacebook1.Text = "";
            txtTwitter1.Text = "";
            txtLinkedin1.Text = "";
        }

    }
}