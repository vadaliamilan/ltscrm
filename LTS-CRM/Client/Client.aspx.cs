﻿using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LTS_CRM.App_Code.Helper;
using System.Web.Security;
namespace LTS_CRM.Client
{
    public partial class Client : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        protected void BindGrid()
        {
            DataTable dataTableClient = BLClients.GetClientList();
            ClientRepeater.DataSource = dataTableClient;
            ClientRepeater.DataBind();
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.ClientID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            Response.Redirect("~/client/ClientAddEdit.aspx", false);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int clientId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLClients.DeleteClient(clientId);
            if (isSuccess)
                BindGrid();
        }

        protected void addClient_Click(object sender, EventArgs e)
        {
            TravelSession.Current.ClientID = null;
            Response.Redirect("~/client/ClientAddEdit.aspx", false);

        }
    }
}