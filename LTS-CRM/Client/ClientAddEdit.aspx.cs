﻿using LTS_CRM.Helper;
using LTSCRM_DL;
using LTSCRM_DL.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Client
{
    public partial class ClientAddEdit : CRMPage
    {
        #region Page Method & Events
        /// <summary>
        /// Page  Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TravelSession.Current.UserName = "Nilesh";
                if (String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.UserName)))
                {
                    FormsAuthentication.RedirectToLoginPage();
                }


                BindContactGrid();
                LoadcboCompanyID(cboCompanyID);
                if (TravelSession.Current.ClientID.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.ClientID.Value)))
                {
                    tab2.Visible = true;
                    tab2_pan.Visible = true;
                    BindClient(Convert.ToInt32(TravelSession.Current.ClientID.Value));
                }
                else
                {
                    tab2.Visible = false;
                    tab2_pan.Visible = false;

                }
            }
            tabactive();

        }
        protected void tabactive()
        {
            string selectedTab = "";
            if (this.Master.FindControl("hdntabstatus") != null)
            {
                selectedTab = ((HiddenField)this.Master.FindControl("hdntabstatus")).Value;
            }
            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");
            tab1_pan.Attributes.Remove("class");
            tab2_pan.Attributes.Remove("class");
            switch (selectedTab)
            {

                case "#tab2_pan":
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white");
                    tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white active");

                    tab1_pan.Attributes.Add("class", "tab-pane ");
                    tab2_pan.Attributes.Add("class", "tab-pane active");
                    break;
                default:
                    tab1.Attributes.Add("class", "bg-color-blueDark txt-color-white active");
                    tab2.Attributes.Add("class", "bg-color-blueDark txt-color-white");
                    tab1_pan.Attributes.Add("class", "tab-pane active");
                    tab2_pan.Attributes.Add("class", "tab-pane ");
                    break;

            }


        }
        public void BindClient(int ID)
        {
            DLClients objClient = new DLClients();
            objClient.ID = ID;
            DataTable dtClient = BLClients.GetClientById(objClient);
            if (dtClient.Rows.Count > 0)
            {

                foreach (DataRow dr in dtClient.Rows)
                {
                    if (cboCompanyID.Items.FindByValue(Convert.ToString(dr["CompanyID"])) != null)
                    {
                        cboCompanyID.ClearSelection();
                        cboCompanyID.Items.FindByValue(Convert.ToString(dr["CompanyID"])).Selected = true;
                    }


                    if (IsDate(Convert.ToString(dr["ClientDate"])))
                    {
                        DateTime dt = new DateTime();
                        dt = Convert.ToDateTime(Convert.ToString(dr["ClientDate"]));
                        txtClientDate.Text = dt.ToString("dd'/'MM'/'yyyy");
                    }


                    // get main client contact id and set to session
                    //TravelSession.Current.MainContactID = Convert.ToInt32(dr["ClientContactID"]);
                    txtSalesStaff.Text = Convert.ToString(dr["SalesStaff"]);
                    ddlCompanyOrInd.Value = Convert.ToString(dr["CompanyOrInd"]);
                    ddlDirectOrAgenet.Value = Convert.ToString(dr["DirectOrAgenet"]);
                    txtCompanyName.Text = Convert.ToString(dr["CompanyName"]);
                    txtwebsite.Text = Convert.ToString(dr["Website"]);
                    txtphoneno.Text = Convert.ToString(dr["Telephone"]);
                    txtfaxno.Text = Convert.ToString(dr["Fax"]);
                    txtEmail.Text = Convert.ToString(dr["Email"]);
                    txtAddress.Text = Convert.ToString(dr["Address1"]);
                    txtAddress2.Text = Convert.ToString(dr["Address2"]);
                    txtcity.Text = Convert.ToString(dr["City"]);
                    txtstate.Text = Convert.ToString(dr["State"]);
                    cmbcountry.Value = Convert.ToString(dr["Country"]);
                    txtPostCode.Text = Convert.ToString(dr["PostalCode"]);
                    cke_txtpaymentTerms.Text = Convert.ToString(dr["PaymentTerms"]);
                    txtBlackList.Text = Convert.ToString(dr["BlackList"]);
                    if (ddlRating.Items.FindByValue(Convert.ToString(dr["Rating"])) != null)
                    {
                        ddlRating.ClearSelection();
                        ddlRating.Items.FindByValue(Convert.ToString(dr["Rating"])).Selected = true;
                    }
                    txtComments.Text = Convert.ToString(dr["Comment"]);

                    //ddlTitle.Value = Convert.ToString(dr["Title"]);
                    //txtLastName.Text = Convert.ToString(dr["SurName"]);
                    //txtFirstName.Text = Convert.ToString(dr["FirstName"]);
                    //txtPosition.Text = Convert.ToString(dr["Position"]);
                    //txtDepartment.Text = Convert.ToString(dr["Department"]);
                    //txtcontactEmail.Text = Convert.ToString(dr["ClientContactEmail"]);
                    //txtcontactTelephone.Text = Convert.ToString(dr["DirectPhone"]);
                    //txtcontactMobile.Text = Convert.ToString(dr["MobileNo"]);
                    //txtcontactFax.Text = Convert.ToString(dr["FaxNo"]);
                    //txtWeChat.Text = Convert.ToString(dr["WeChat"]);
                    //txtWhatsapp.Text = Convert.ToString(dr["Whatsup"]);
                    //txtSkype.Text = Convert.ToString(dr["Skype"]);
                    //txtFacebook.Text = Convert.ToString(dr["Facebook"]);
                    //txtTwitter.Text = Convert.ToString(dr["Twitter"]);
                    //txtLinkedin.Text = Convert.ToString(dr["LinkedIn"]);
                }
            }
        }

        /// <summary>
        /// Save Client & Main Contact
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btncontactSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DLClients objClient = new DLClients();
                DLClientContacts objClientContact = new DLClientContacts();
                #region Save Client Details

                if (TravelSession.Current.ClientID.HasValue && BLClients.CheckDuplicateByName(txtEmail.Text, Convert.ToInt32(TravelSession.Current.ClientID)))
                {
                    lblheader.Text = "Record Exist";
                    lblmsg.Text = "This Client Email exist in database.";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                    return;
                }
                else
                {
                    string res = txtClientDate.Text.Trim();
                    if (res != "")
                    {
                        objClient.ClientDate = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    objClient.CompanyID = Convert.ToInt32(cboCompanyID.SelectedValue);
                    objClient.SalesStaff = txtSalesStaff.Text.Trim();
                    objClient.CompanyOrInd = ddlCompanyOrInd.Items[ddlCompanyOrInd.SelectedIndex].Value.ToString();
                    objClient.DirectOrAgenet = ddlDirectOrAgenet.Items[ddlCompanyOrInd.SelectedIndex].Value.ToString();
                    objClient.CompanyName = txtCompanyName.Text.Trim();
                    objClient.Website = txtwebsite.Text.Trim();
                    objClient.Telephone = txtphoneno.Text.Trim();
                    objClient.Fax = txtfaxno.Text.Trim();
                    objClient.Email = txtEmail.Text.Trim();
                    objClient.Address1 = txtAddress.Text.Trim();
                    objClient.Address2 = txtAddress2.Text.Trim();
                    objClient.City = txtcity.Text.Trim();
                    objClient.State = txtstate.Text.Trim();
                    objClient.Country = cmbcountry.Items[cmbcountry.SelectedIndex].Value;
                    objClient.PostalCode = txtPostCode.Text.Trim();
                    objClient.PaymentTerms = cke_txtpaymentTerms.Text.Trim();
                    objClient.BlackList = txtBlackList.Text.Trim();
                    objClient.Rating = Convert.ToInt32(ddlRating.SelectedItem.Value);
                    objClient.Comment = txtComments.Text.Trim();

                    HttpPostedFile File = flContractDoc.PostedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    objClient.FileData = fileData;
                    objClient.CreatedBy = TravelSession.Current.UserName;
                    objClient.CreatedDate = DateTime.Now;
                    objClient.UpdatedBy = objClient.CreatedBy;
                    objClient.UpdatedDate = DateTime.Now;
                    if (TravelSession.Current.ClientID.HasValue && !String.IsNullOrEmpty(Convert.ToString(TravelSession.Current.ClientID.Value)))
                    {
                        objClient.ID = TravelSession.Current.ClientID.Value;
                        objClient.Update();
                    }
                    else
                    {
                        BLClients.SaveClientInfo(objClient);

                        //lblheader.Text = "Record Saved Successfully";
                        //lblmsg.Text = "Your Account has been created. <br />Your User ID is " + (Int32)objClient.ID;
                        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                    }

                #endregion


                    ClearControls();
                    //Redirect to listing page
                    Response.Redirect("Client.aspx", true);
                }
            }
        }
        private void ClearControls()
        {
            TravelSession.Current.ClientID = null;
            TravelSession.Current.MainContactID = null;
            txtClientDate.Text = "";
            txtSalesStaff.Text = "";
            ddlCompanyOrInd.SelectedIndex = 0;
            ddlDirectOrAgenet.SelectedIndex = 0;
            txtCompanyName.Text = "";
            txtwebsite.Text = "";
            txtphoneno.Text = "";
            txtfaxno.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtAddress2.Text = "";
            txtcity.Text = "";
            txtstate.Text = "";
            cmbcountry.SelectedIndex = -1;
            txtPostCode.Text = "";
            cke_txtpaymentTerms.Text = "";
            txtBlackList.Text = "";
            ddlRating.ClearSelection();
            txtComments.Text = "";

            //ddlTitle.SelectedIndex = -1;
            //txtLastName.Text = "";
            //txtFirstName.Text = "";
            //txtPosition.Text = "";
            //txtDepartment.Text = "";
            //txtcontactEmail.Text = "";
            //txtcontactTelephone.Text = "";
            //txtcontactMobile.Text = "";
            //txtcontactFax.Text = "";
            //txtWeChat.Text = "";
            //txtWhatsapp.Text = "";
            //txtSkype.Text = "";
            //txtFacebook.Text = "";
            //txtTwitter.Text = "";
            //txtLinkedin.Text = "";
        }


        #endregion

        #region Client Contact

        private void BindClientContact(int ID)
        {
            DLClientContacts objClientConact = new DLClientContacts();
            objClientConact.ID = ID;
            objClientConact = BLClientContacts.GetClientContactById(objClientConact);
            ddlTitle1.Value = objClientConact.Title.Value;
            txtLastName1.Text = objClientConact.SurName.Value;
            txtFirstName1.Text = objClientConact.FirstName.Value;
            txtPosition1.Text = objClientConact.Position.Value;
            txtDepartment1.Text = objClientConact.Department.Value;
            txtcontactEmail1.Text = objClientConact.Email.Value;
            txtcontactTelephone1.Text = objClientConact.DirectPhone.Value;
            txtcontactMobile1.Text = objClientConact.MobileNo.Value;
            txtcontactFax1.Text = objClientConact.FaxNo.Value;
            txtWeChat1.Text = objClientConact.WeChat.Value;
            txtWhatsapp1.Text = objClientConact.Whatsup.Value;
            txtSkype1.Text = objClientConact.Skype.Value;
            txtFacebook1.Text = objClientConact.Facebook.Value;
            txtTwitter1.Text = objClientConact.Twitter.Value;
            txtLinkedin1.Text = objClientConact.LinkedIn.Value;
        }
        protected void BindContactGrid()
        {
            if (TravelSession.Current.ClientID.HasValue)
            {
                DataTable dataTableClientContact = BLClientContacts.GetClientContactList(TravelSession.Current.ClientID.Value);
                ClientContactRepeater.DataSource = dataTableClientContact;
                ClientContactRepeater.DataBind();
            }
        }

        protected void buttonEdit_Click(object sender, EventArgs e)
        {
            TravelSession.Current.ClientContactID = Convert.ToInt32((sender as LinkButton).CommandArgument);
            BindClientContact(TravelSession.Current.ClientContactID.Value);
        }

        protected void buttonDelete_Click(object sender, EventArgs e)
        {
            int clientContactId = Convert.ToInt32((sender as LinkButton).CommandArgument);
            bool isSuccess = BLClientContacts.DeleteClientContact(clientContactId);
            if (isSuccess)
                BindContactGrid();
        }

        /// <summary>
        /// Save Client Another Contacts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btncontactAnotherSave_Click(object sender, EventArgs e)
        {
            DLClientContacts objClientContact = new DLClientContacts();
            if (Page.IsValid)
            {
                if (TravelSession.Current.ClientID.HasValue)
                {
                    objClientContact.ClientID = TravelSession.Current.ClientID.Value;
                    objClientContact.Title = ddlTitle1.Items[ddlTitle1.SelectedIndex].Text;
                    objClientContact.SurName = txtLastName1.Text.Trim();
                    objClientContact.FirstName = txtFirstName1.Text.Trim();
                    objClientContact.MainContact = false;
                    objClientContact.Position = txtPosition1.Text.Trim();
                    objClientContact.Department = txtDepartment1.Text.Trim();
                    objClientContact.Email = txtcontactEmail1.Text.Trim();
                    objClientContact.DirectPhone = txtcontactTelephone1.Text.Trim();
                    objClientContact.MobileNo = txtcontactMobile1.Text.Trim();
                    objClientContact.FaxNo = txtcontactFax1.Text.Trim();
                    objClientContact.WeChat = txtWeChat1.Text.Trim();
                    objClientContact.Whatsup = txtWhatsapp1.Text.Trim();
                    objClientContact.Skype = txtSkype1.Text.Trim();
                    objClientContact.Facebook = txtFacebook1.Text.Trim();
                    objClientContact.Twitter = txtTwitter1.Text.Trim();
                    objClientContact.LinkedIn = txtLinkedin1.Text.Trim();


                    if (BLClientContacts.CheckDuplicateContactByEmail((objClientContact.ID.IsNull ? (int?)null : objClientContact.ID.Value), txtcontactEmail1.Text.Trim(), objClientContact.ClientID.Value))
                    {
                        lblheader.Text = "Contact Record Exist";
                        lblmsg.Text = "This Client Contact Email exist in database.";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                        return;
                    }
                    else
                    {
                        if (TravelSession.Current.ClientContactID.HasValue)
                        {
                            objClientContact.UpdatedBy = TravelSession.Current.UserName;
                            objClientContact.UpdatedDate = DateTime.Now;
                            objClientContact.ID = TravelSession.Current.ClientContactID.Value;
                            objClientContact.Update();
                        }
                        else
                        {
                            objClientContact.CreatedBy = TravelSession.Current.UserName;
                            objClientContact.CreatedDate = DateTime.Now;
                            objClientContact.Insert();
                        }
                    }
                    ClearClientContactControls();
                    BindContactGrid();
                }


            }
        }

        private void ClearClientContactControls()
        {
            TravelSession.Current.ClientContactID = null;
            ddlTitle1.SelectedIndex = -1;
            txtLastName1.Text = "";
            txtFirstName1.Text = "";
            txtPosition1.Text = "";
            txtDepartment1.Text = "";
            txtcontactEmail1.Text = "";
            txtcontactTelephone1.Text = "";
            txtcontactMobile1.Text = "";
            txtcontactFax1.Text = "";
            txtWeChat1.Text = "";
            txtWhatsapp1.Text = "";
            txtSkype1.Text = "";
            txtFacebook1.Text = "";
            txtTwitter1.Text = "";
            txtLinkedin1.Text = "";
        }
        #endregion

    }
}