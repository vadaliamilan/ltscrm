﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientContactAddEdit.aspx.cs" MasterPageFile="~/master/MasterAdmin.Master" Inherits="LTS_CRM.Client.ClientContactAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-sm-12">

        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>


            <ol class="breadcrumb">
                <li>Client Contacts</li>
                <li>Client Contacts Add-Edit</li>
            </ol>

        </div>
        <div class="row">
            <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                <div class="input-group">
                </div>
            </div>
            <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
            </div>
        </div>
        
        <div class="col-sm-12">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-12">
                                            <h3 class="text-primary">Contact Person</h3>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-2">
                                            <label class="input">
                                                <select id="ddlTitle1" class="form-control" runat="server">
                                                    <option>Mr</option>
                                                    <option>Ms</option>
                                                    <option>Mrs</option>
                                                </select>
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                        </section>
                                    </div>

                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-user"></i>
                                                <asp:TextBox ID="txtLastName1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Sur Name" Text=""></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvtxtLastName" ValidationGroup="addSubClientContact" ControlToValidate="txtLastName" runat="server" ErrorMessage="Last Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>


                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-user"></i>
                                                <asp:TextBox ID="txtFirstName1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="First Name" Text=""></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="req1" ValidationGroup="addSubClientContact" ControlToValidate="txtFirstName" runat="server" ErrorMessage="First Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-map-marker"></i>
                                                <asp:TextBox ID="txtPosition1" class="form-control" placeholder="Position" ValidationGroup="addSubClientContact" runat="server">
                                                </asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa-building-o"></i>
                                                <asp:TextBox ID="txtDepartment1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Department" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                    </div>


                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-envelope-o"></i>

                                                <asp:TextBox ID="txtcontactEmail1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Email" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-phone"></i>
                                                <asp:TextBox ID="txtcontactTelephone1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Phone Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-mobile"></i>
                                                <asp:TextBox ID="txtcontactMobile1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Mobile Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-fax"></i>
                                                <asp:TextBox ID="txtcontactFax1" runat="server" class="form-control" ValidationGroup="addSubClientContact" placeholder="Fax Number" Text="" data-mask="(999) 999-99999"></asp:TextBox>

                                            </label>
                                        </section>
                                    </div>



                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-wechat"></i>

                                                <asp:TextBox ID="txtWeChat1" runat="server" class="form-control" placeholder="Enter WeChat" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-whatsapp"></i>
                                                <asp:TextBox ID="txtWhatsapp1" runat="server" class="form-control" placeholder="Enter What's up" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-skype"></i>

                                                <asp:TextBox ID="txtSkype1" runat="server" class="form-control" placeholder="Enter Skype" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-facebook"></i>
                                                <asp:TextBox ID="txtFacebook1" runat="server" class="form-control" placeholder="Enter Facebook" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                    </div>


                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-twitter"></i>

                                                <asp:TextBox ID="txtTwitter1" runat="server" class="form-control" placeholder="Enter Twitter" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="input">
                                                <i class="icon-prepend fa fa-linkedin"></i>
                                                <asp:TextBox ID="txtLinkedin1" runat="server" class="form-control" placeholder="Enter Linked in" Text=""></asp:TextBox>

                                            </label>
                                        </section>
                                    </div>

                                </fieldset>
                                <footer>


                                    <button id="btncontactSave" runat="server" validationgroup="addSubClientContact" class="btn bg-color-blueDark txt-color-white" style="width: 100px" type="submit" onserverclick="btncontactSave_Click">
                                        <span style="width: 80px">Save </span>
                                    </button>
                                    <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />


                                </footer>
                            </div>
        <asp:Panel ID="pnlContact" runat="server" Visible="true">
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
                <header>
                    <h2>Client Contact</h2>
                </header>

                <div class="jarviswidget-editbox">
                </div>

                <div class="widget-body">


                    <div class="well">


                       


                    </div>










                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </asp:Panel>
    </div>

    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" href="client.aspx">Close</a>
                </div>
            </div>

        </div>
    </div>

</asp:Content>
