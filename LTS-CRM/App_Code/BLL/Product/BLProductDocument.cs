﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLProductDocument
    {
        public static DataTable GetProductDocumentList()
        {
            try
            {
                DLProductDocument obj = new DLProductDocument();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetProductDocumentListByProductId(int proudctId)
        {
            try
            {
                DLProductDocument obj = new DLProductDocument();
                obj.ProductID = proudctId;
                return obj.SelectAllWProductIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool DeleteProductDocument(int id)
        {
            try
            {
                DLProductDocument productDocument = new DLProductDocument();
                productDocument.ID = id;
                return productDocument.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLProductDocument GetProductDocumentById(int Id)
        {
            try
            {
                DLProductDocument obj = new DLProductDocument();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Product Document
        /// </summary>
        /// <param name="objProductDocument"></param>
        /// <returns></returns>
        public static int UpdateProductDocument(DLProductDocument objProductDocument)
        {
            try
            {
                objProductDocument.Update();
                return (int)objProductDocument.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Product Document
        /// </summary>
        /// <param name="objProductDocument"></param>
        /// <returns></returns>
        public static int SaveProductDocument(DLProductDocument objProductDocument)
        {
            try
            {
                objProductDocument.Insert();
                return (int)objProductDocument.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}