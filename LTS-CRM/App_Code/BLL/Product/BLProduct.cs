﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.App_Code.BLL.Product
{
    public class BLProduct
    {

        #region Basic Functions

        /// <summary>
        /// Get Product list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetProductList()
        {
            try
            {
                DLProduct product = new DLProduct();
                return product.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>

        public static bool DeleteProduct(int productId)
        {
            try
            {
                DLProduct product = new DLProduct();
                product.ID = productId;
                return product.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveProduct(DLProduct product)
        {
            try
            {
                return product.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateProduct(DLProduct product)
        {
            try
            {
                if (!product.ID.IsNull && product.ID.Value != 0)
                {
                    return product.Update();
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Ferry supplier By Id
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public static DLProduct GetProductById(int ID)
        {
            try
            {
                DLProduct obj = new DLProduct();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Product Contacts Operations

        /// <summary>
        /// Get Product Contact list by ProductId 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetProductContactsbyProductID(int ID)
        {
            try
            {
                DLProductContact obj = new DLProductContact();
                obj.ProductlID = ID;
                return obj.SelectAllWProductlIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Product Contact list by Product Id 
        /// </summary>
        /// <returns></returns>
        public static DLProductContact GetProductContactsbyPk(int ID)
        {
            try
            {
                DLProductContact obj = new DLProductContact();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveProductContact(DLProductContact obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLProductContact obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteProductContactByPk(int ID)
        {
            try
            {
                DLProductContact obj = new DLProductContact();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}