﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLPriceTemplate
    {
        public static DataTable GetPriceTemplateList()
        {
            try
            {
                DLPriceTemplate obj = new DLPriceTemplate();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeletePriceTemplate(int id)
        {
            try
            {
                DLPriceTemplate priceTemplate = new DLPriceTemplate();
                priceTemplate.ID = id;
                return priceTemplate.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLPriceTemplate GetPriceTemplateById(int Id)
        {
            try
            {
                DLPriceTemplate obj = new DLPriceTemplate();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Price Template
        /// </summary>
        /// <param name="objPriceTemplate"></param>
        /// <returns></returns>
        public static int UpdatePriceTemplate(DLPriceTemplate objPriceTemplate)
        {
            try
            {
                objPriceTemplate.Update();
                return (int)objPriceTemplate.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Price Template
        /// </summary>
        /// <param name="objPriceTemplate"></param>
        /// <returns></returns>
        public static int SavePriceTempate(DLPriceTemplate objPriceTemplate)
        {
            try
            {
                objPriceTemplate.Insert();
                return (int)objPriceTemplate.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
