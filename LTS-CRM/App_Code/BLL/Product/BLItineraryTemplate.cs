﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLItineraryTemplate
    {
        public static DataTable GetItineraryTemplateList()
        {
            try
            {
                DLItineraryTemplate obj = new DLItineraryTemplate();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteItineraryTemplate(int id)
        {
            try
            {
                DLItineraryTemplate itineraryTemplate = new DLItineraryTemplate();
                itineraryTemplate.ID = id;
                return itineraryTemplate.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLItineraryTemplate GetItineraryTemplateById(int Id)
        {
            try
            {
                DLItineraryTemplate obj = new DLItineraryTemplate();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Itinerary Template
        /// </summary>
        /// <param name="objItineraryTemplate"></param>
        /// <returns></returns>
        public static int UpdateItineraryTemplate(DLItineraryTemplate objItineraryTemplate)
        {
            try
            {
                objItineraryTemplate.Update();
                return (int)objItineraryTemplate.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Itinerary Template
        /// </summary>
        /// <param name="objItineraryTemplate"></param>
        /// <returns></returns>
        public static int SaveItineraryTempate(DLItineraryTemplate objItineraryTemplate)
        {
            try
            {
                objItineraryTemplate.Insert();
                return (int)objItineraryTemplate.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}