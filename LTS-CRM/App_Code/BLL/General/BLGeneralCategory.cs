﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace LTS_CRM.BLL
{
    public class BLGeneralCategory
    {
        public static DataTable GetALLCategory(string Category = "")
        {
            LTSCRM_DL.DLGeneralCategoryItems obj = new LTSCRM_DL.DLGeneralCategoryItems();
            obj.Category = Category.Trim();
            DataTable dt = obj.SelectAllByCategory();
            return dt;
        }
        public static bool SaveGeneralItem(LTSCRM_DL.DLGeneralCategoryItems obj)
        {
            try
            { 
                DataTable dt = obj.SelectAllByCategory();
                DataView dv = dt.DefaultView;
                dv.RowFilter= "(Title ='"+ obj.Title.Value +"')";

                if (dv.ToTable().Rows.Count > 0)
                    return false;
                else 
                    return  obj.Insert();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}