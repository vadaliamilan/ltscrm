﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSalesContact
    {
        public static DataTable GetContactList()
        {
            try
            {
                DLSalesContactManagement obj = new DLSalesContactManagement();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteContact(int id)
        {
            try
            {
                DLSalesContactManagement contact = new DLSalesContactManagement();
                contact.ID = id;
                return contact.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLSalesContactManagement GetContactById(int Id)
        {
            try
            {
                DLSalesContactManagement obj = new DLSalesContactManagement();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Contact
        /// </summary>
        /// <param name="objContact"></param>
        /// <returns></returns>
        public static int UpdateContact(DLSalesContactManagement objContact)
        {
            try
            {
                objContact.Update();
                return (int)objContact.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Contact
        /// </summary>
        /// <param name="objContact"></param>
        /// <returns></returns>
        public static int SaveContact(DLSalesContactManagement objContact)
        {
            try
            {
                objContact.Insert();
                return (int)objContact.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
