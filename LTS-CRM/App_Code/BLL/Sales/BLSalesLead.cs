﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSalesLead
    {
        public static DataTable GetLeadList()
        {
            try
            {
                DLSalesLeadManagement obj = new DLSalesLeadManagement();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteLead(int id)
        {
            try
            {
                DLSalesLeadManagement lead = new DLSalesLeadManagement();
                lead.ID = id;
                return lead.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLSalesLeadManagement GetLeadById(int Id)
        {
            try
            {
                DLSalesLeadManagement obj = new DLSalesLeadManagement();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Lead
        /// </summary>
        /// <param name="objLead"></param>
        /// <returns></returns>
        public static int UpdateLead(DLSalesLeadManagement objLead)
        {
            try
            {
                objLead.Update();
                return (int)objLead.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Lead
        /// </summary>
        /// <param name="objLead"></param>
        /// <returns></returns>
        public static int SaveLead(DLSalesLeadManagement objLead)
        {
            try
            {
                objLead.Insert();
                return (int)objLead.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
