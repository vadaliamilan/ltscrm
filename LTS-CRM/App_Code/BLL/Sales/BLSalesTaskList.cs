﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSalesTaskList
    {
        public static DataTable GetTaskList()
        {
            try
            {
                DLSalesTaskList obj = new DLSalesTaskList();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteTask(int id)
        {
            try
            {
                DLSalesTaskList taskList = new DLSalesTaskList();
                taskList.ID = id;
                return taskList.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLSalesTaskList GetTaskById(int Id)
        {
            try
            {
                DLSalesTaskList obj = new DLSalesTaskList();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Task
        /// </summary>
        /// <param name="objTask"></param>
        /// <returns></returns>
        public static int UpdateTask(DLSalesTaskList objTask)
        {
            try
            {
                objTask.Update();
                return (int)objTask.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Task
        /// </summary>
        /// <param name="objTask"></param>
        /// <returns></returns>
        public static int SaveTask(DLSalesTaskList objTask)
        {
            try
            {
                objTask.Insert();
                return (int)objTask.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}