﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSalesAppointmentList
    {
        public static DataTable GetAppointmentList()
        {
            try
            {
                DLSalesAppointmentList obj = new DLSalesAppointmentList();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteAppointment(int id)
        {
            try
            {
                DLSalesAppointmentList appointmentList = new DLSalesAppointmentList();
                appointmentList.ID = id;
                return appointmentList.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLSalesAppointmentList GetAppointmentById(int Id)
        {
            try
            {
                DLSalesAppointmentList obj = new DLSalesAppointmentList();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Appointment
        /// </summary>
        /// <param name="objAppointment"></param>
        /// <returns></returns>
        public static int UpdateAppointment(DLSalesAppointmentList objAppointment)
        {
            try
            {
                objAppointment.Update();
                return (int)objAppointment.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Appointment
        /// </summary>
        /// <param name="objAppointment"></param>
        /// <returns></returns>
        public static int SaveAppointment(DLSalesAppointmentList objAppointment)
        {
            try
            {
                objAppointment.Insert();
                return (int)objAppointment.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}