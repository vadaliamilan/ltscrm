﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTSCRM_DL.BLL
{
    public class BLClientContacts
    {

        /// <summary>
        /// Get Client Contact List
        /// </summary>
        /// <returns></returns>
        public static DataTable GetClientContactList(int clientId)
        {
            try
            {
                DLClientContacts clientContact = new DLClientContacts();
                clientContact.ClientID = clientId;
                return clientContact.SelectAllWClientIDLogic();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteClientContact(int clientContactId)
        {
            try
            {
                DLClientContacts clientContacts = new DLClientContacts();
                clientContacts.ID = clientContactId;
                return clientContacts.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLClientContacts GetClientContactById(DLClientContacts objClientConact)
        {
            try
            {
                objClientConact.SelectOne();
                return objClientConact;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check duplicate contact email from the client contact listing
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Email"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        public static bool CheckDuplicateContactByEmail(int? ID,string Email, int ClientID)
        {
            try
            {
                DLClientContacts obj = new DLClientContacts();
                obj.Email = Email;
                if(ID.HasValue)
                obj.ID = ID.Value;
                obj.ClientID = ClientID;
                return obj.CheckDuplicateEmail().Rows.Count > 0;// True means found duplicate in other entry of same clients contacts

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}