﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LTSCRM_DL;
using System.Data;
namespace LTSCRM_DL.BLL
{
    public class BLClients
    {
        /// <summary>
        /// Save Client Details
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns></returns>
        public static int SaveClientInfo(DLClients objClient)
        {
            try
            {
                objClient.Insert();
                return (int)objClient.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get Client List
        /// </summary>
        /// <param name="objClient"></param>
        /// <returns></returns>
        public static DataTable GetClientList()
        {
            try
            {
                DLClients clients = new DLClients();
                return clients.SelectAll();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetClientById(DLClients clients)
        {
            try
            {
                return clients.SelectOne();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLClients GetClientByIdObj(DLClients clients)
        {
            try
            {
                clients.SelectOne();
                return clients;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteClient(int id)
        {
            try
            {
                DLClients clients = new DLClients();
                clients.ID = id;
                return clients.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool CheckDuplicateByName(string Email, int ID)
        {
            try
            {
                DLClients clients = new DLClients();
                clients.Email = Email;
                clients.ID = ID;
                return clients.SelectOneByEmail().Rows.Count > 0;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}