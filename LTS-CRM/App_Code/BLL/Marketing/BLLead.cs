﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLLead
    {
        public static DataTable GetLeadList()
        {
            try
            {
                DLLeadManagement obj = new DLLeadManagement();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteLead(int id)
        {
            try
            {
                DLLeadManagement lead = new DLLeadManagement();
                lead.ID = id;
                return lead.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLLeadManagement GetLeadById(int Id)
        {
            try
            {
                DLLeadManagement obj = new DLLeadManagement();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Lead
        /// </summary>
        /// <param name="objLead"></param>
        /// <returns></returns>
        public static int UpdateLead(DLLeadManagement objLead)
        {
            try
            {
                objLead.Update();
                return (int)objLead.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Lead
        /// </summary>
        /// <param name="objLead"></param>
        /// <returns></returns>
        public static int SaveLead(DLLeadManagement objLead)
        {
            try
            {
                objLead.Insert();
                return (int)objLead.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
