﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLContact
    {
        public static DataTable GetContactList()
        {
            try
            {
                DLContactManagement obj = new DLContactManagement();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteContact(int id)
        {
            try
            {
                DLContactManagement contact = new DLContactManagement();
                contact.ID = id;
                return contact.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLContactManagement GetContactById(int Id)
        {
            try
            {
                DLContactManagement obj = new DLContactManagement();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Contact
        /// </summary>
        /// <param name="objContact"></param>
        /// <returns></returns>
        public static int UpdateContact(DLContactManagement objContact)
        {
            try
            {
                objContact.Update();
                return (int)objContact.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Contact
        /// </summary>
        /// <param name="objContact"></param>
        /// <returns></returns>
        public static int SaveContact(DLContactManagement objContact)
        {
            try
            {
                objContact.Insert();
                return (int)objContact.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
