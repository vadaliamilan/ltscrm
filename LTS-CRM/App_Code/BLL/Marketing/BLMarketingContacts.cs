﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LTSCRM_DL;
using System.Data;

namespace LTS_CRM.BLL
{
    public class BLMarketingContacts
    {
        public static DataTable GetChannelContactsByChannelId(int ChannelID)
        {
            DLMarketingContacts obj = new DLMarketingContacts();
            obj.MarketingChannelID = ChannelID;
            return obj.SelectAllWMarketingChannelIDLogic();
        }
        public static DLMarketingContacts GetChannelContactsByPk(int Id)
        {
            DLMarketingContacts obj = new DLMarketingContacts();
            obj.ID = Id;
            obj.SelectOne();
            return obj;
        }
        public static Boolean Delete(int ID)
        {
            try
            {
                DLMarketingContacts obj = new DLMarketingContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}