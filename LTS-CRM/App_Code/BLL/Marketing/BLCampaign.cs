﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLCampaign
    {
        public static DataTable GetCampaignList()
        {
            try
            {
                DLMarketingCampaign obj = new DLMarketingCampaign();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteCampaign(int id)
        {
            try
            {
                DLMarketingCampaign campaign = new DLMarketingCampaign();
                campaign.ID = id;
                return campaign.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLMarketingCampaign GetCampaignById(int Id)
        {
            try
            {
                DLMarketingCampaign obj = new DLMarketingCampaign();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Campaign
        /// </summary>
        /// <param name="objCampaign"></param>
        /// <returns></returns>
        public static int UpdateCampaign(DLMarketingCampaign objCampaign)
        {
            try
            {
                objCampaign.Update();
                return (int)objCampaign.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Campaign
        /// </summary>
        /// <param name="objCampaign"></param>
        /// <returns></returns>
        public static int SaveCampaign(DLMarketingCampaign objCampaign)
        {
            try
            {
                objCampaign.Insert();
                return (int)objCampaign.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}