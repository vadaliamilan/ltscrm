﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LTSCRM_DL;
using System.Data;

namespace LTS_CRM.BLL
{
    public class BLMarketingProject
    {
        #region Basic Information
        public static DataTable GetAllList()
        {
            try
            {
                DLMarketingProject obj = new DLMarketingProject();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Delete(int Id)
        {
            try
            {
                DLMarketingProject obj = new DLMarketingProject();
                obj.ID = Id;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLMarketingProject GetProjectByPk(int ID)
        {
            try
            {
                DLMarketingProject obj = new DLMarketingProject();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Boolean Update(DLMarketingProject obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Boolean Save(DLMarketingProject obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        #endregion
    }
}