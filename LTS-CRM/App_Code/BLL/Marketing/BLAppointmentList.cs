﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLAppointmentList
    {
        public static DataTable GetAppointmentList()
        {
            try
            {
                DLAppointmentList obj = new DLAppointmentList();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteAppointment(int id)
        {
            try
            {
                DLAppointmentList appointmentList = new DLAppointmentList();
                appointmentList.ID = id;
                return appointmentList.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLAppointmentList GetAppointmentById(int Id)
        {
            try
            {
                DLAppointmentList obj = new DLAppointmentList();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Appointment
        /// </summary>
        /// <param name="objAppointment"></param>
        /// <returns></returns>
        public static int UpdateAppointment(DLAppointmentList objAppointment)
        {
            try
            {
                objAppointment.Update();
                return (int)objAppointment.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Appointment
        /// </summary>
        /// <param name="objAppointment"></param>
        /// <returns></returns>
        public static int SaveAppointment(DLAppointmentList objAppointment)
        {
            try
            {
                objAppointment.Insert();
                return (int)objAppointment.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}