﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLTaskList
    {
        public static DataTable GetTaskList()
        {
            try
            {
                DLTaskList obj = new DLTaskList();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteTask(int id)
        {
            try
            {
                DLTaskList taskList = new DLTaskList();
                taskList.ID = id;
                return taskList.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLTaskList GetTaskById(int Id)
        {
            try
            {
                DLTaskList obj = new DLTaskList();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Task
        /// </summary>
        /// <param name="objTask"></param>
        /// <returns></returns>
        public static int UpdateTask(DLTaskList objTask)
        {
            try
            {
                objTask.Update();
                return (int)objTask.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Task
        /// </summary>
        /// <param name="objTask"></param>
        /// <returns></returns>
        public static int SaveTask(DLTaskList objTask)
        {
            try
            {
                objTask.Insert();
                return (int)objTask.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}