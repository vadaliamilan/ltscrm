﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLMarketingChannel
    {
        public static DataTable GetChannelList()
        {
            try
            {
                DLMarketingChannel obj = new DLMarketingChannel();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLMarketingChannel GetChannelByPk(int ID)
        {
            try
            {
                DLMarketingChannel obj = new DLMarketingChannel();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Boolean Delete(int ID)
        {
            try
            {
                DLMarketingChannel obj = new DLMarketingChannel();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}