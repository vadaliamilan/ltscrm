﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLOperationContact
    {
        public static DataTable GetContactList()
        {
            try
            {
                DLOperationContactMgmt obj = new DLOperationContactMgmt();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteContact(int id)
        {
            try
            {
                DLOperationContactMgmt contact = new DLOperationContactMgmt();
                contact.ID = id;
                return contact.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLOperationContactMgmt GetContactById(int Id)
        {
            try
            {
                DLOperationContactMgmt obj = new DLOperationContactMgmt();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Contact
        /// </summary>
        /// <param name="objContact"></param>
        /// <returns></returns>
        public static int UpdateContact(DLOperationContactMgmt objContact)
        {
            try
            {
                objContact.Update();
                return (int)objContact.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Contact
        /// </summary>
        /// <param name="objContact"></param>
        /// <returns></returns>
        public static int SaveContact(DLOperationContactMgmt objContact)
        {
            try
            {
                objContact.Insert();
                return (int)objContact.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
