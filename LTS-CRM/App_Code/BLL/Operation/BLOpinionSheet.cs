﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTSCRM_DL.BLL
{
    public static class BLOpinionSheet
    {
        /// <summary>
        /// Save Opinion Sheet
        /// </summary>
        /// <param name="objOpinionSheet"></param>
        /// <returns></returns>
        public static int SaveOpinionSheet(DLOpinionSheet objOpinionSheet)
        {
            try
            {
                objOpinionSheet.Insert();
                return (int)objOpinionSheet.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Update Opinion Sheet
        /// </summary>
        /// <param name="objOpinionSheet"></param>
        /// <returns></returns>
        public static int UpdateOpinionSheet(DLOpinionSheet objOpinionSheet)
        {
            try
            {
                objOpinionSheet.Update();
                return (int)objOpinionSheet.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get Opinion Sheet List
        /// </summary>
        /// <returns></returns>
        public static DataTable GetOpinionSheet()
        {
            try
            {
                DLOpinionSheet opinionSheet = new DLOpinionSheet();
                return opinionSheet.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLOpinionSheet GetOpinionSheetById(int Id)
        {
            try
            {
                DLOpinionSheet obj = new DLOpinionSheet();
                obj.ID = Id;
                obj.SelectOne();
                return obj;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteOpinionSheet(int id)
        {
            try
            {
                DLOpinionSheet opinionSheet = new DLOpinionSheet();
                opinionSheet.ID = id;
                return opinionSheet.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}