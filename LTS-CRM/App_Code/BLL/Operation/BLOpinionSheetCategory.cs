﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLOpinionSheetCategory
    {
        /// <summary>
        /// Get Opinion Sheet Category List
        /// </summary>
        /// <returns></returns>
        public static DataTable GetOpinionSheetCategory()
        {
            try
            {
                DLOpinionSheetCategory opinionSheet = new DLOpinionSheetCategory();
                return opinionSheet.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}