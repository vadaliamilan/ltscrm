﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLOperationLead
    {
        public static DataTable GetLeadList()
        {
            try
            {
                DLOperationLeadMgmt obj = new DLOperationLeadMgmt();
                return obj.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteLead(int id)
        {
            try
            {
                DLOperationLeadMgmt lead = new DLOperationLeadMgmt();
                lead.ID = id;
                return lead.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLOperationLeadMgmt GetLeadById(int Id)
        {
            try
            {
                DLOperationLeadMgmt obj = new DLOperationLeadMgmt();
                obj.ID = Id;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Lead
        /// </summary>
        /// <param name="objLead"></param>
        /// <returns></returns>
        public static int UpdateLead(DLOperationLeadMgmt objLead)
        {
            try
            {
                objLead.Update();
                return (int)objLead.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save Lead
        /// </summary>
        /// <param name="objLead"></param>
        /// <returns></returns>
        public static int SaveLead(DLOperationLeadMgmt objLead)
        {
            try
            {
                objLead.Insert();
                return (int)objLead.ID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
