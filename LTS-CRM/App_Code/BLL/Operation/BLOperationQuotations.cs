﻿using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLOperationQuotations
    {
        #region Quotation Operations

        internal static DataTable SelectAll()
        {
            LTSCRM_DL.DLOperationQuotation obj = new LTSCRM_DL.DLOperationQuotation();
            return obj.SelectAll();

        }

        internal static bool Delete(int Qid)
        {
            LTSCRM_DL.DLOperationQuotation obj = new LTSCRM_DL.DLOperationQuotation();
            obj.ID = Qid;
            return obj.Delete();

        }

        internal static DLOperationQuotation SelectQuotationById(int p)
        {
            DLOperationQuotation obj = new DLOperationQuotation();
            obj.ID = p;
            obj.SelectOne();
            return obj;
        }

        public static DataTable SelectQuotationBookDetails(int quotationId)
        {
            DLOperationQuotation obj = new DLOperationQuotation();
            obj.ID = quotationId;
            return obj.SelectBookingDetails();
        }

        public static bool SaveQuotation(DLOperationQuotation obj)
        {
            return obj.Insert();
        }
        public static bool UpdateQuotation(DLOperationQuotation obj)
        {
            if (!obj.ID.IsNull)
                return obj.Update();
            else return false;
        }

        public static DLOperationQuotation SelectById(int id)
        {
            var obj = new DLOperationQuotation();
            obj.SelectOne();
            return obj;
        }
        #endregion

        #region QUotation Price Operations

        /// <summary>
        /// NOTE: Here pass QuotationPrice ID not the basicID
        /// </summary>
        /// <param name="id">QuotationPrice ID</param>
        /// <returns>DataTable</returns>
        public static DataTable GetQuotationPriceHotel(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                LTSCRM_DL.DLQuotationPrice_Hotel obj = new LTSCRM_DL.DLQuotationPrice_Hotel();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }

        internal static int GetQuotationPriceID(int p)
        {
            DLQuotationPrice obj = new DLQuotationPrice();
            obj.QuotationId = p;
            DataTable dt = obj.SelectAllWQuotationIdLogic();
            if (dt != null && dt.Rows.Count > 0)
                return (int)dt.Rows[0]["Id"];

            else return 0;
        }
        //internal static int GetQuotationPriceBasicIDFromPriceId(int QuotationPriceId)
        //{
        //    DLQuotationPrice_Basic obj = new DLQuotationPrice_Basic();
        //    obj.QuotationPriceId = QuotationPriceId;
        //    DataTable dt = obj.SelectAllWQuotationPriceIdLogic();
        //    if (dt != null && dt.Rows.Count > 0)
        //        return (int)dt.Rows[0]["ID"];

        //    else return 0;
        //}


        internal static DLQuotationPrice GetQuotationPriceDetailsById(int p)
        {
            DLQuotationPrice obj = new DLQuotationPrice();
            obj.ID = p;
            obj.SelectOne();
            return obj;
        }

        public static bool SaveQuotationPrice(DLQuotationPrice obj)
        {
            return obj.Insert();
        }

        public static bool UpdateQuotationPrice(DLQuotationPrice obj)
        {
            if (!obj.ID.IsNull)
                return obj.Update();
            else return false;
        }

        #endregion

        #region QuotationBasic Operations

        internal static bool SaveQuotationPriceBasicDetails(DLQuotationPrice_Basic obj)
        {
            return obj.Insert();
        }
        internal static bool UpdateQuotationPriceBasicDetails(DLQuotationPrice_Basic obj)
        {
            if (!obj.ID.IsNull)
                return obj.Update();
            else return false;
        }

        /// <summary>
        /// user to get the quoation basic details 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        internal static Dictionary<int, int> GetQuotationBasicDetailsPair(int quotationPriceId, int noofdays, ref DataTable _dt)
        {
            try
            {
                Dictionary<int, int> dic = new Dictionary<int, int>();
                DLQuotationPrice_Basic obj = new DLQuotationPrice_Basic();
                obj.QuotationPriceId = quotationPriceId;
                DataTable dt = new DataTable();
                dt = obj.SelectAllWQuotationPriceIdLogic();
                _dt = dt;
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    //dic.Add(Convert.ToInt32(dr["Id"]), Convert.ToInt32(dr["QuotationPriceId"]));
                    if (i < noofdays)
                        dic.Add(i, Convert.ToInt32(dr["Id"]));
                    i++;
                }
                return dic;
            }
            catch
            {
                return null;
            }
        }

        internal static DLQuotationPrice_Basic GetQuotationPriceBasicDetailsById(int basicId)
        {
            DLQuotationPrice_Basic obj = new DLQuotationPrice_Basic();
            obj.ID = basicId;
            obj.SelectOne();
            return obj;
        }
        #endregion

        #region OtherServices

        internal static DataTable GetOtherservices(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                LTSCRM_DL.DLQuotationPrice_OtherServices obj = new DLQuotationPrice_OtherServices();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Enterance Operation

        internal static DataTable GetEnteranceDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                LTSCRM_DL.DLQuotationPrice_Entrance obj = new DLQuotationPrice_Entrance();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Tips
        public static DataTable GetTipsDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                DLQuotationPrice_Tips obj = new DLQuotationPrice_Tips();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Dinner Details
        internal static DataTable GetDinnerDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                DLQuotationPrice_Dinner obj = new DLQuotationPrice_Dinner();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Lunch Details

        internal static DataTable GetLunchDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                DLQuotationPrice_Lunch obj = new DLQuotationPrice_Lunch();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Tour Guide
        internal static DataTable GetTourGuideDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                DLQuotationPrice_TourGuide obj = new DLQuotationPrice_TourGuide();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region VanHire
        internal static DataTable GetVanHireDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                DLQuotationPrice_VanHire obj = new DLQuotationPrice_VanHire();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Driver & GUIDE accomodation
        internal static DataTable GetDriverGuideDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                DLQuotationPrice_DriveGuide obj = new DLQuotationPrice_DriveGuide();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Coach Details
        internal static DataTable GetCoachDetails(int quotationPriceId)
        {
            try
            {
                DataTable dt = new DataTable();
                DLQuotationPrice_Coach obj = new DLQuotationPrice_Coach();
                obj.QPID = quotationPriceId;
                return obj.SelectAllWQuotationPriceIdLogic();
            }
            catch
            {
                return null;
            }
        }
        #endregion


        #region Quotation Confirmation To Client

        public static DataTable GetQuotationConfirmationListByQID(int quotationId)
        {
            DLQuotationConfirmationToClient obj = new DLQuotationConfirmationToClient();
            obj.QuotationId = quotationId;
            return obj.SelectAllWQuotationIdLogic();
        }

        public static bool SaveClientConfirmationData(DLQuotationConfirmationToClient obj)
        {
            return obj.Insert();
        }
        public static bool DeleteClientConfirmationData(DLQuotationConfirmationToClient obj)
        {
            if (!obj.Id.IsNull)
            {
                obj.Delete();
            }
            return true;
        }
        #endregion


        internal static bool SaveClientConfirmationDataFiles(DLQuotationConfirmationDocument objDoc)
        {
            return objDoc.Insert();
        }
    }
}