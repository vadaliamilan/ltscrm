﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierTheaterTicket
    {

        #region Basic Functions

        /// <summary>
        /// Get Supplier TheaterTicket list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierTheaterTicketList()
        {
            try
            {
                DLSuppliersDetails theaterTicket = new DLSuppliersDetails();
                theaterTicket.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Theatre.ToString());
                return theaterTicket.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete TheaterTicket supplier
        /// </summary>
        /// <param name="supplierTheaterTicketId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierTheaterTicket(int supplierTheaterTicketId)
        {
            try
            {
                DLSuppliersDetails theaterTicket = new DLSuppliersDetails();
                theaterTicket.ID = supplierTheaterTicketId;
                return theaterTicket.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveTheaterTicketSupplier(DLSuppliersDetails supplierTheaterTicket)
        {
            try
            {
                supplierTheaterTicket.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Theatre.ToString());
                return supplierTheaterTicket.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateTheaterTicketSupplier(DLSuppliersDetails supplierTheaterTicket)
        {
            try
            {
                if (!supplierTheaterTicket.ID.IsNull && supplierTheaterTicket.ID.Value != 0)
                    return supplierTheaterTicket.Update();
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get TheaterTicket supplier By Id
        /// </summary>
        /// <param name="supplierTheaterTicket"></param>
        /// <returns></returns>
        public static DLSuppliersDetails GetSupplierTheaterTicketById(DLSuppliersDetails supplierTheaterTicket)
        {
            try
            {
                supplierTheaterTicket.SelectOne();
                return supplierTheaterTicket;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  Rate Function
        /// <summary>
        /// Get Supplier TheaterTicket Rate list by TheaterTicket Id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierTheaterTicketRateListByTheaterTicketId(int supplierTheaterTicketId)
        {
            try
            {
                DLSupplierTheaterRate theaterTicketRate = new DLSupplierTheaterRate();
                theaterTicketRate.SupplierDetailID = supplierTheaterTicketId;
                return theaterTicketRate.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete TheaterTicket supplier rate
        /// </summary>
        /// <param name="supplierTheaterTicketId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierTheaterTicketRate(int supplierTheaterTicketRateId)
        {
            try
            {
                DLSupplierTheaterRate theaterTicket = new DLSupplierTheaterRate();
                theaterTicket.ID = supplierTheaterTicketRateId;
                return theaterTicket.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get TheaterTicket supplier Rate By Id
        /// </summary>
        /// <param name="supplierTheaterTicket"></param>
        /// <returns></returns>
        public static DLSupplierTheaterRate GetSupplierTheaterTicketRateById(DLSupplierTheaterRate supplierTheaterTicketRate)
        {
            try
            {
                supplierTheaterTicketRate.SelectOne();
                return supplierTheaterTicketRate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertSupplierRate(DLSupplierTheaterRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierRate(DLSupplierTheaterRate obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Information Tab function

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static DataTable GetTheaterTicketSupplierInformation(int SupplierDetailId)
        //{
        //    try
        //    {
        //        DLSupplierInformation obj = new DLSupplierInformation();
        //        obj.SupplierDetailID = SupplierDetailId;
        //        return obj.SelectAllWSupplierDetailIDLogic();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DLSupplierInformation GetTheaterTicketSupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierInformation GetTheaterTicketSupplierInformationByPk(int ID)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by TheaterTicket ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by TheaterTicket ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}