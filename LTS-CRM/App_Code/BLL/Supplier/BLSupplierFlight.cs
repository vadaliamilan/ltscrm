﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierFlight
    {

        #region Basic Functions

        /// <summary>
        /// Get Supplier Flight list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierFlightList()
        {
            try
            {
                DLSuppliersDetails flight = new DLSuppliersDetails();
                flight.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Flight.ToString());
                return flight.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Flight supplier
        /// </summary>
        /// <param name="supplierFlightId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierFlight(int supplierFlightId)
        {
            try
            {
                DLSuppliersDetails flight = new DLSuppliersDetails();
                flight.ID = supplierFlightId;
                return flight.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveFlightSupplier(DLSuppliersDetails supplierFlight)
        {
            try
            {
                supplierFlight.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Flight.ToString());
                return supplierFlight.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateFlightSupplier(DLSuppliersDetails supplierFlight)
        {
            try
            {
                if (!supplierFlight.ID.IsNull && supplierFlight.ID.Value != 0)
                    return supplierFlight.Update();
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Flight supplier By Id
        /// </summary>
        /// <param name="supplierFlight"></param>
        /// <returns></returns>
        public static DLSuppliersDetails GetSupplierFlightById(DLSuppliersDetails supplierFlight)
        {
            try
            {
                supplierFlight.SelectOne();
                return supplierFlight;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  Rate Function



        #endregion

        #region Information Tab function

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static DataTable GetFlightSupplierInformation(int SupplierDetailId)
        //{
        //    try
        //    {
        //        DLSupplierInformation obj = new DLSupplierInformation();
        //        obj.SupplierDetailID = SupplierDetailId;
        //        return obj.SelectAllWSupplierDetailIDLogic();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DLSupplierInformation GetFlightSupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierInformation GetFlightSupplierInformationByPk(int ID)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Flight ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Flight ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}