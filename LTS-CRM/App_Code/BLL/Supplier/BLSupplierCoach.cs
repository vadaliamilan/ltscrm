﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierCoach
    {
        #region Basic Functions

        /// <summary>
        /// Get Supplier Coach list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierCoachList()
        {
            try
            {
                DLSuppliersDetails coach = new DLSuppliersDetails();
                coach.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.CoachHire.ToString());
                return coach.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static DataTable GetSupplierCoachMaster(int companyId)
        {
            try
            {
                DLSupplierMaster coach = new DLSupplierMaster();
                coach.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.CoachHire.ToString());
                coach.CompanyID = companyId;
                return coach.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Coach supplier
        /// </summary>
        /// <param name="supplierCoachId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierCoach(int supplierCoachId)
        {
            try
            {
                DLSuppliersDetails coach = new DLSuppliersDetails();
                coach.ID = supplierCoachId;
                return coach.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveCoachSupplier(DLSuppliersDetails supplierCoach)
        {
            try
            {
                supplierCoach.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.CoachHire.ToString());
                return supplierCoach.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateCoachSupplier(DLSuppliersDetails supplierCoach)
        {
            try
            {
                if (!supplierCoach.ID.IsNull && supplierCoach.ID.Value != 0)
                    return supplierCoach.Update();
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Coach supplier By Id
        /// </summary>
        /// <param name="supplierCoach"></param>
        /// <returns></returns>
        public static DLSuppliersDetails GetSupplierCoachById(DLSuppliersDetails supplierCoach)
        {
            try
            {
                supplierCoach.SelectOne();
                return supplierCoach;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  Rate Function
       

       
        #endregion

        #region Information Tab function

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static DataTable GetCoachSupplierInformation(int SupplierDetailId)
        //{
        //    try
        //    {
        //        DLSupplierInformation obj = new DLSupplierInformation();
        //        obj.SupplierDetailID = SupplierDetailId;
        //        return obj.SelectAllWSupplierDetailIDLogic();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DLSupplierInformation GetCoachSupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierInformation GetCoachSupplierInformationByPk(int ID)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Coach ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Coach ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

}