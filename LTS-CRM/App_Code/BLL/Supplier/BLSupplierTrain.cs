﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierTrain
    {
        #region Basic Details Operation

        public static bool UpdateTrainSupplier(DLSuppliersDetails supplier)
        {
            try
            {
                if (!supplier.ID.IsNull && supplier.ID.Value != 0)
                {
                    supplier.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Train.ToString());
                    return supplier.Update();
                }
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplier(DLSuppliersDetails supplier)
        {
            try
            {
                supplier.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Train.ToString());
                return supplier.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
           /// <summary>
        /// Get Supplier Train
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierTrainList()
        {
            try
            {
                DLSuppliersDetails hotelTrain = new DLSuppliersDetails();
                hotelTrain.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Train.ToString());
                return hotelTrain.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
  /// <summary>
        /// Delete Train supplier
        /// </summary>
        /// <param name="supplierTrainId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierTrain(int supplierTrainId)
        {
            try
            {
                DLSuppliersDetails train = new DLSuppliersDetails();
                train.ID = supplierTrainId;
                return train.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Train supplier By Id
        /// </summary>
        /// <param name="supplierTrain"></param>
        /// <returns></returns>
        public static DLSuppliersDetails GetSupplierTrainById(DLSuppliersDetails supplierTrain)
        {
            try
            {
                supplierTrain.SelectOne();
                return supplierTrain;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Rate OPerations

           /// <summary>
        /// Get Supplier Train Rate list by Train Id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierTrainRateListByTrainId(int supplierTrainId)
        {
            try
            {
                DLSupplierTrainRate TrainRate = new DLSupplierTrainRate();
                TrainRate.SupplierDetailID = supplierTrainId;
                return TrainRate.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Train supplier rate
        /// </summary>
        /// <param name="supplierTrainId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierTrainRate(int supplierTrainRateId)
        {
            try
            {
                DLSupplierTrainRate Train = new DLSupplierTrainRate();
                Train.ID = supplierTrainRateId;
                return Train.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Train supplier Rate By Id
        /// </summary>
        /// <param name="supplierTrain"></param>
        /// <returns></returns>
        public static DataTable GetSupplierTrainRateById(DLSupplierTrainRate supplierTrain)
        {
            try
            {
                return supplierTrain.SelectOne();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static bool InsertSupplierRate(DLSupplierTrainRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierRate(DLSupplierTrainRate obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Information Tab Operation

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

        public static DLSupplierInformation GetTrainSupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierInformation GetTrainSupplierInformationByPk(int ID)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion 
    }
}