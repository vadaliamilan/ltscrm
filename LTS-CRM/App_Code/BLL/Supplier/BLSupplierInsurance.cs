﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierInsurance
    {
        #region Basic Functions

        /// <summary>
        /// Get Supplier Insurance list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierInsuranceList()
        {
            try
            {
                DLSuppliersDetails insurance = new DLSuppliersDetails();
                insurance.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Insurance.ToString());
                return insurance.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Insurance supplier
        /// </summary>
        /// <param name="supplierInsuranceId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierInsurance(int supplierInsuranceId)
        {
            try
            {
                DLSuppliersDetails insurance = new DLSuppliersDetails();
                insurance.ID = supplierInsuranceId;
                return insurance.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveInsuranceSupplier(DLSuppliersDetails supplierInsurance)
        {
            try
            {
                supplierInsurance.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Insurance.ToString());
                return supplierInsurance.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateInsuranceSupplier(DLSuppliersDetails supplierInsurance)
        {
            try
            {
                if (!supplierInsurance.ID.IsNull && supplierInsurance.ID.Value != 0)
                    return supplierInsurance.Update();
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Insurance supplier By Id
        /// </summary>
        /// <param name="supplierInsurance"></param>
        /// <returns></returns>
        public static DLSuppliersDetails GetSupplierInsuranceById(DLSuppliersDetails supplierInsurance)
        {
            try
            {
                supplierInsurance.SelectOne();
                return supplierInsurance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  Rate Function
        /// <summary>
        /// Get Supplier Insurance Rate list by Insurance Id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierInsuranceRateListByInsuranceId(int supplierInsuranceId)
        {
            try
            {
                DLSupplierInsuranceRate insuranceRate = new DLSupplierInsuranceRate();
                insuranceRate.SupplierDetailID = supplierInsuranceId;
                return insuranceRate.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Insurance supplier rate
        /// </summary>
        /// <param name="supplierInsuranceId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierInsuranceRate(int supplierInsuranceRateId)
        {
            try
            {
                DLSupplierInsuranceRate insurance = new DLSupplierInsuranceRate();
                insurance.ID = supplierInsuranceRateId;
                return insurance.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Insurance supplier Rate By Id
        /// </summary>
        /// <param name="supplierInsurance"></param>
        /// <returns></returns>
        public static DLSupplierInsuranceRate GetSupplierInsuranceRateById(DLSupplierInsuranceRate supplierInsuranceRate)
        {
            try
            {
                supplierInsuranceRate.SelectOne();
                return supplierInsuranceRate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertSupplierRate(DLSupplierInsuranceRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierRate(DLSupplierInsuranceRate obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Information Tab function

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static DataTable GetInsuranceSupplierInformation(int SupplierDetailId)
        //{
        //    try
        //    {
        //        DLSupplierInformation obj = new DLSupplierInformation();
        //        obj.SupplierDetailID = SupplierDetailId;
        //        return obj.SelectAllWSupplierDetailIDLogic();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DLSupplierInformation GetInsuranceSupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierInformation GetInsuranceSupplierInformationByPk(int ID)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Insurance ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Insurance ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}