﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierHotelWholseler
    {
        #region Basic Details Operations

        /// <summary>
        /// Get Supplier Hotel Wholeselar list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierHotelWholeSalerList()
        {
            try
            {
                DLSuppliersDetails hotelWholeSaler = new DLSuppliersDetails();
                hotelWholeSaler.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.HotelWholesaler.ToString());
                return hotelWholeSaler.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete HotelWholseler supplier
        /// </summary>
        /// <param name="supplierHotelWholselerId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierHotelWholseler(int supplierHotelWholselerId)
        {
            try
            {
                DLSuppliersDetails hotelWholseler = new DLSuppliersDetails();
                hotelWholseler.ID = supplierHotelWholselerId;
                return hotelWholseler.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get HotelWholseler supplier By Id
        /// </summary>
        /// <param name="supplierHotelWholseler"></param>
        /// <returns></returns>
        public static DataTable GetSupplierHotelWholselerById(DLSuppliersDetails supplierHotelWholseler)
        {
            try
            {
                return supplierHotelWholseler.SelectOne();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateWholeseller(DLSuppliersDetails obj)
        {
            try
            {
                obj.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.HotelWholesaler.ToString());
                if (obj.ID != 0)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool SaveSupplier(DLSuppliersDetails obj)
        {
            try
            {
                obj.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.HotelWholesaler.ToString());
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLSuppliersDetails GetSupplierByID(int Id)
        {
            try
            {
                DLSuppliersDetails wholesaler = new DLSuppliersDetails();
                wholesaler.ID = Id;
                wholesaler.SelectOne();
                return wholesaler;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new  DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                  obj.SelectOne();
                  return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj= new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}