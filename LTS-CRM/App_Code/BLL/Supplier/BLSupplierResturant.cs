﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierResturant
    { 
        #region Grid Function
        public static DataTable GetRestaurant()
        {
            try
            {
                DLSuppliersDetails obj = new DLSuppliersDetails();
                obj.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Restaurant.ToString());
                return obj.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     
        public static bool DeleteResturant(int id)
        {
            try
            {
                DLSuppliersDetails obj = new DLSuppliersDetails();
                obj.ID = id;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region AddEdit Form
        public static DLSuppliersDetails GetSupplierByID(int ID)
        {
            try
            {
                DLSuppliersDetails obj = new DLSuppliersDetails();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool SaveSupplierDetails(DLSuppliersDetails supplier)
        {
            try
            {
                supplier.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Restaurant.ToString());
                return supplier.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateSupplierDetail(DLSuppliersDetails obj)
        {
            try
            {
                if (obj.ID != 0)
                {
                    obj.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Restaurant.ToString());
                    return obj.Update();
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Rate

        public static bool UpdateSupplierRate(DLSupplierRestaurantRate obj)
        {
            try
            {
                return obj.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierRate(DLSupplierRestaurantRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSupplierRateListBySupplierID(int supplier)
        {
            try
            {
                DLSupplierRestaurantRate obj = new DLSupplierRestaurantRate();
                obj.SupplierDetailID = supplier;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSupplierRateListByPk(DLSupplierRestaurantRate obj)
        {
            try
            {
                return obj.SelectOne();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool DeleteSupplierRateByPk(int ID)
        {
            try
            {
                DLSupplierRestaurantRate obj = new DLSupplierRestaurantRate();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Supplier Information

        public static bool UpdateHotelSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertHotelSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


    }
}