﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierTips
    {
        #region Grid Function
        public static DataTable GetTips()
        {
            try
            {
                DLSupplierTipsDetail tips = new DLSupplierTipsDetail();
                return tips.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteTips(int id)
        {
            try
            {
                DLSupplierTipsDetail tips = new DLSupplierTipsDetail();
                tips.ID = id;
                return tips.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLSupplierTipsDetail GetSupplierByID(int ID)
        {
            try
            {
                DLSupplierTipsDetail obj = new DLSupplierTipsDetail();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        internal static bool UpdateSupplierDetail(DLSupplierTipsDetail obj)
        {

            try
            {
                if (obj.ID != 0)
                {
                    obj.SupplierTypeId = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Tip.ToString());
                    return obj.Update();
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static bool SaveSupplierDetails(DLSupplierTipsDetail obj)
        {
            try
            {
                obj.SupplierTypeId = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Tip.ToString());
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }
    }
}