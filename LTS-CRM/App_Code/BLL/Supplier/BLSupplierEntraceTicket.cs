﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierEntraceTicket
    {
        #region Grid Function
        public static DataTable GetEntranceTicket()
        {
            try
            {
                DLSuppliersDetails entraceTicket = new DLSuppliersDetails();
                entraceTicket.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Entrance.ToString());
                return entraceTicket.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteEntranceTicket(int id)
        {
            try
            {
                DLSuppliersDetails dmc = new DLSuppliersDetails();
                dmc.ID = id;
                return dmc.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region AddEdit Form
        public static DLSuppliersDetails GetSupplierByID(int ID)
        {
            try
            {
                DLSuppliersDetails obj = new DLSuppliersDetails();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool SaveSupplierDetails(DLSuppliersDetails supplier)
        {
            try
            {
                supplier.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Entrance.ToString());
                return supplier.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateSupplierDetail(DLSuppliersDetails obj)
        {
            try
            {
                if (obj.ID != 0)
                {
                    obj.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Entrance.ToString());
                    return obj.Update();
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Rate

        public static bool UpdateSupplierRate(DLSupplierEntranceTicketRate obj)
        {
            try
            {
                return obj.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierRate(DLSupplierEntranceTicketRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSupplierRateListBySupplierID(int supplier)
        {
            try
            {
                DLSupplierEntranceTicketRate obj = new DLSupplierEntranceTicketRate();
                obj.SupplierDetailID = supplier;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSupplierRateListByPk(DLSupplierEntranceTicketRate obj)
        {
            try
            {
                return obj.SelectOne();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool DeleteSupplierRateByPk(int ID)
        {
            try
            {
                DLSupplierEntranceTicketRate obj = new DLSupplierEntranceTicketRate();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        #region Supplier Information

        public static bool UpdateHotelSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertHotelSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        

    }
}