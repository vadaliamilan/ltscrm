﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierHotel
    {


        /// <summary>
        /// Get Supplier Hotel list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierHoteltList()
        {
            try
            {
                DLSuppliersDetails hotel = new DLSuppliersDetails();
                hotel.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Hotel.ToString());
                return hotel.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DLSuppliersDetails GetSupplierHoteltByID(int ID)
        {
            try
            {
                DLSuppliersDetails hotel = new DLSuppliersDetails();
                hotel.ID = ID;
                hotel.SelectOne();
                return hotel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Hotel supplier
        /// </summary>
        /// <param name="supplierHotelId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierHotel(int supplierHotelId)
        {
            try
            {
                DLSuppliersDetails hotel = new DLSuppliersDetails();
                hotel.ID = supplierHotelId;
                return hotel.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete hotel information details by ID
        /// </summary>
        /// <param name="HotelInfoId"></param>
        /// <returns></returns>
        public static bool DeleteSupplierHotelInfo(int HotelInfoId)
        {
            try
            {
                DLSupplierInformation hotelinfo = new  DLSupplierInformation();
                hotelinfo.ID = HotelInfoId;
                return hotelinfo.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get Hotel supplier By Id
        /// </summary>
        /// <param name="supplierHotel"></param>
        /// <returns></returns>
        public static DataTable GetSupplierHotelById(DLSuppliersDetails supplierHotel)
        {
            try
            {
                return supplierHotel.SelectOne();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Hotel Rate list by Hotel Id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierHotelRateListByHotelId(int supplierHotelId)
        {
            try
            {
                DLSupplierHotelRate HotelRate = new DLSupplierHotelRate();
                HotelRate.SupplierDetailID = supplierHotelId;
                return HotelRate.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Hotel information list by Hotel Id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierHotelInfoListByHotelId(int hoteldetailID)
        {
            try
            {
                DLSupplierInformation hotelInfo= new DLSupplierInformation();
                hotelInfo.SupplierDetailID = hoteldetailID;
                return hotelInfo.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Hotel information list by Hotel Id
        /// </summary>
        /// <returns></returns>
        public static DLSupplierInformation GetSupplierHotelInfoListByPk(DLSupplierInformation hotelInfo)
        {
            try
            {
                hotelInfo.SelectOne();
                return hotelInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Hotel supplier rate
        /// </summary>
        /// <param name="supplierHotelId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierHotelRate(int supplierHotelRateId)
        {
            try
            {
                DLSupplierHotelRate Hotel = new DLSupplierHotelRate();
                Hotel.ID = supplierHotelRateId;
                return Hotel.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Hotel supplier Rate By Id
        /// </summary>
        /// <param name="supplierHotel"></param>
        /// <returns></returns>
        public static DataTable GetSupplierHotelRateById(DLSupplierHotelRate supplierHotel)
        {
            try
            {
                return supplierHotel.SelectOne();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateHotelSupplier(DLSuppliersDetails supplierHotel)
        {
            try
            {
                if (supplierHotel.ID != 0)
                {
                    supplierHotel.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Hotel.ToString());
                    return supplierHotel.Update();
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool SaveHotelSupplier(DLSuppliersDetails supplierHotel)
        {
            try
            {
                supplierHotel.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Hotel.ToString());
                return supplierHotel.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertHotelSupplierRate(DLSupplierHotelRate supplierHotelRate)
        {
            try
            { 
                return supplierHotelRate.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateHotelSupplierRate(DLSupplierHotelRate supplierHotelRate)
        {
            try
            {
                if (!supplierHotelRate.ID.IsNull)
                    return supplierHotelRate.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static bool InsertHotelSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateHotelSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}