﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierVanHire
    {
        #region Basic Functions

        /// <summary>
        /// Get Supplier Van list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierVanList()
        {
            try
            {
                DLSuppliersDetails Van = new DLSuppliersDetails();
                Van.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.VanHire.ToString());
                return Van.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Van supplier
        /// </summary>
        /// <param name="supplierVanId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierVan(int supplierVanId)
        {
            try
            {
                DLSuppliersDetails van = new DLSuppliersDetails();
                van.ID = supplierVanId;
                return van.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveVanSupplier(DLSuppliersDetails supplierVan)
        {
            try
            {
                supplierVan.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.VanHire.ToString());
                return supplierVan.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateVanSupplier(DLSuppliersDetails supplierVan)
        {
            try
            {
                if (!supplierVan.ID.IsNull && supplierVan.ID.Value != 0)
                    return supplierVan.Update();
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Van supplier By Id
        /// </summary>
        /// <param name="supplierVan"></param>
        /// <returns></returns>
        public static DLSuppliersDetails GetSupplierVanById(DLSuppliersDetails supplierVan)
        {
            try
            {
                supplierVan.SelectOne();
                return supplierVan;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  Rate Function
        /// <summary>
        /// Get Supplier Van Rate list by Van Id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierVanRateListByVanId(int supplierVanId)
        {
            try
            {
                DLSupplierVanHireRate VanRate = new DLSupplierVanHireRate();
                VanRate.SupplierDetailID = supplierVanId;
                return VanRate.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Van supplier rate
        /// </summary>
        /// <param name="supplierVanId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierVanRate(int supplierVanRateId)
        {
            try
            {
                DLSupplierVanHireRate Van = new DLSupplierVanHireRate();
                Van.ID = supplierVanRateId;
                return Van.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Van supplier Rate By Id
        /// </summary>
        /// <param name="supplierVan"></param>
        /// <returns></returns>
        public static DLSupplierVanHireRate GetSupplierVanRateById(DLSupplierVanHireRate supplierVanRate)
        {
            try
            {
                supplierVanRate.SelectOne();
                return supplierVanRate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertSupplierRate(DLSupplierVanHireRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierRate(DLSupplierVanHireRate obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Information Tab function

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static DataTable GetVanSupplierInformation(int SupplierDetailId)
        //{
        //    try
        //    {
        //        DLSupplierInformation obj = new DLSupplierInformation();
        //        obj.SupplierDetailID = SupplierDetailId;
        //        return obj.SelectAllWSupplierDetailIDLogic();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DLSupplierInformation GetVanSupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierInformation GetVanSupplierInformationByPk(int ID)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Van ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Van ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  BindDropdowns 
        public static DataTable GetSupplierVanMaster(int companyId)
        {
            try
            {
                DLSupplierMaster van = new DLSupplierMaster();
                van.CompanyID = companyId;
                van.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.VanHire.ToString());
                return van.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
	#endregion
    }
}