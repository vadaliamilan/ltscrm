﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierMealSuppliment
    {

        #region Basic Functions

        /// <summary>
        /// Get Supplier MealSuppliment list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierMealSupplimentList()
        {
            try
            {
                DLSupplierMealSupplement MealSuppliment = new DLSupplierMealSupplement();
                MealSuppliment.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.MealSupplementsfordriverandguide.ToString());
                return MealSuppliment.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete MealSuppliment supplier
        /// </summary>
        /// <param name="supplierMealSupplimentId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierMealSuppliment(int supplierMealSupplimentId)
        {
            try
            {
                DLSupplierMealSupplement MealSuppliment = new DLSupplierMealSupplement();
                MealSuppliment.ID = supplierMealSupplimentId;
                return MealSuppliment.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveMealSupplimentSupplier(DLSupplierMealSupplement supplierMealSuppliment)
        {
            try
            {
                supplierMealSuppliment.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.MealSupplementsfordriverandguide.ToString());
                return supplierMealSuppliment.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateMealSupplimentSupplier(DLSupplierMealSupplement supplierMealSuppliment)
        {
            try
            {
                if (!supplierMealSuppliment.ID.IsNull && supplierMealSuppliment.ID.Value != 0)
                {
                    supplierMealSuppliment.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.MealSupplementsfordriverandguide.ToString());
                    return supplierMealSuppliment.Update();
                }
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get MealSuppliment supplier By Id
        /// </summary>
        /// <param name="supplierMealSuppliment"></param>
        /// <returns></returns>
        public static DLSupplierMealSupplement GetSupplierMealSupplimentById(DLSupplierMealSupplement supplierMealSuppliment)
        {
            try
            {
                supplierMealSuppliment.SelectOne();
                return supplierMealSuppliment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  Rate Function
      
        #endregion

        #region Information Tab function

        
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}