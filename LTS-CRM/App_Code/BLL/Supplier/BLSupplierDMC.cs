﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierDMC
    {
        #region Grid Function
        public static DataTable GetDMC()
        {
            try
            {
                DLSuppliersDetails dmc = new DLSuppliersDetails();
                dmc.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.DMC.ToString());
                return dmc.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteDmc(int id)
        {
            try
            {
                DLSuppliersDetails dmc = new DLSuppliersDetails();
                dmc.ID = id;
                return dmc.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateSupplierRate(DLSupplierDMCRate obj)
        {
            try
            {
              return  obj.Update();
            }
            catch(Exception  ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierRate(DLSupplierDMCRate obj)
        {
            try
            {
              return  obj.Insert();
            }
            catch(Exception  ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSupplierRateListByDMCID(int DMCID)
        {
            try
            {
                DLSupplierDMCRate DMC = new DLSupplierDMCRate();
                DMC.SupplierDetailID = DMCID;
                return DMC.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSupplierRateListByPk(DLSupplierDMCRate obj)
        {
            try
            {
                return obj.SelectOne();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateDMCSupplier(DLSuppliersDetails dmc)
        {
            try
            {
                if (dmc.ID != 0)
                {
                    dmc.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.DMC.ToString());
                    return dmc.Update();
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool SaveDMCSupplier(DLSuppliersDetails DMCSupplier)
        {
            try
            {
                DMCSupplier.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.DMC.ToString());
                return DMCSupplier.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}