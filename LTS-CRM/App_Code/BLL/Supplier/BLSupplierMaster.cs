﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierMaster
    {
        /// <summary>
        /// Get Supplier list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierMasterByID(int Id, int typeId, int companyId)
        {
            try
            {
                DLSupplierMaster obj = new DLSupplierMaster();
                obj.SupplierTypeID = typeId;
                obj.ID = Id;
                obj.CompanyID = companyId;
                DataTable dt = new DataTable();
                dt = obj.SelectAllWSupplierTypeIDLogic();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get all master supplier details
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="SupplierType"></param>
        /// <returns></returns>
        public static DataTable GetSupplierMasterList(int CompanyId = 0, int SupplierType = 0)
        {
            try
            {
                DataTable dt = new DataTable();

                DLSupplierMaster obj = new DLSupplierMaster();
                obj.SupplierTypeID = 0;
                obj.ID = 0;
                if (SupplierType != 0)
                {
                    obj.SupplierTypeID = SupplierType;
                }
                if (CompanyId != 0)
                {
                    obj.CompanyID = CompanyId;
                }
                dt = obj.SelectAllWSupplierTypeIDLogic();

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete the master entry
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static bool DeleteSupplierMaster(int Id)
        {
            try
            {
                DLSupplierMaster obj = new DLSupplierMaster();
                obj.ID = Id;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Supplier Form Add Edit function

        public static DLSupplierMaster GetSupplierMasterDetailByPk(DLSupplierMaster obj)
        {
            try
            {
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateMasterDetails(DLSupplierMaster obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                {
                    obj.Update();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertMasterDetails(DLSupplierMaster obj)
        {
            try
            {
                if (obj.ID.IsNull )
                {
                    obj.Insert();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

       
    }
}