﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierFerry
    {
        #region Basic Functions

        /// <summary>
        /// Get Supplier Ferry list
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierFerryList()
        {
            try
            {
                DLSuppliersDetails ferry = new DLSuppliersDetails();
                ferry.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Ferry.ToString());
                return ferry.SelectAllWSupplierTypeIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Ferry supplier
        /// </summary>
        /// <param name="supplierFerryId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierFerry(int supplierFerryId)
        {
            try
            {
                DLSuppliersDetails ferry = new DLSuppliersDetails();
                ferry.ID = supplierFerryId;
                return ferry.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveFerrySupplier(DLSuppliersDetails supplierFerry)
        {
            try
            {
                supplierFerry.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Ferry.ToString());
                return supplierFerry.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateFerrySupplier(DLSuppliersDetails supplierFerry)
        {
            try
            {
                if (!supplierFerry.ID.IsNull && supplierFerry.ID.Value != 0)
                {
                    supplierFerry.SupplierTypeID = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Ferry.ToString());
                    return supplierFerry.Update();
                }
                else return false;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Ferry supplier By Id
        /// </summary>
        /// <param name="supplierFerry"></param>
        /// <returns></returns>
        public static DLSuppliersDetails GetSupplierFerryById(DLSuppliersDetails supplierFerry)
        {
            try
            {
                supplierFerry.SelectOne();
                return supplierFerry;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region  Rate Function
        /// <summary>
        /// Get Supplier Ferry Rate list by Ferry Id
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierFerryRateListByFerryId(int supplierFerryId)
        {
            try
            {
                DLSupplierFerryRate ferryRate = new DLSupplierFerryRate();
                ferryRate.SupplierDetailID = supplierFerryId;
                return ferryRate.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Ferry supplier rate
        /// </summary>
        /// <param name="supplierFerryId"></param>
        /// <returns></returns>

        public static bool DeleteSupplierFerryRate(int supplierFerryRateId)
        {
            try
            {
                DLSupplierFerryRate ferry = new DLSupplierFerryRate();
                ferry.ID = supplierFerryRateId;
                return ferry.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Ferry supplier Rate By Id
        /// </summary>
        /// <param name="supplierFerry"></param>
        /// <returns></returns>
        public static DLSupplierFerryRate GetSupplierFerryRateById(DLSupplierFerryRate supplierFerryRate)
        {
            try
            {
                supplierFerryRate.SelectOne();
                return supplierFerryRate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertSupplierRate(DLSupplierFerryRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierRate(DLSupplierFerryRate obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Information Tab function

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {

            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static DataTable GetFerrySupplierInformation(int SupplierDetailId)
        //{
        //    try
        //    {
        //        DLSupplierInformation obj = new DLSupplierInformation();
        //        obj.SupplierDetailID = SupplierDetailId;
        //        return obj.SelectAllWSupplierDetailIDLogic();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DLSupplierInformation GetFerrySupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierInformation GetFerrySupplierInformationByPk(int ID)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Supplier Other Contacts Operations

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSupplierContactsbySupplierID(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.SupplierDetailID = ID;
                return obj.SelectAllWSupplierDetailIDLogic();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Supplier Contact list by Wholeselar ID 
        /// </summary>
        /// <returns></returns>
        public static DLSupplierContacts GetSupplierContactsbyPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierContact(DLSupplierContacts obj)
        {
            try
            {
                if (!obj.ID.IsNull && obj.ID.Value != 0)
                    return obj.Update();
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSupplierContactByPk(int ID)
        {
            try
            {
                DLSupplierContacts obj = new DLSupplierContacts();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}