﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LTS_CRM.BLL
{
    public class BLSupplierTourGuide
    {
        #region Grid Function
        public static DataTable GetTourGuide()
        {
            try
            {
                DLSupplierTourGuide tourGuide = new DLSupplierTourGuide();
                return tourGuide.SelectAll();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteTourGuide(int id)
        {
            try
            {
                DLSupplierTourGuide tourGuide = new DLSupplierTourGuide();
                tourGuide.ID = id;
                return tourGuide.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region AddEdit Form
        public static DLSupplierTourGuide GetSupplierByID(DLSupplierTourGuide obj)
        {
            try
            {
                obj.SelectOne();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool SaveSupplierDetails(DLSupplierTourGuide supplier)
        {
            try
            {
                supplier.SupplierTypeId = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.TourGuide.ToString());
                return supplier.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool UpdateSupplierDetail(DLSupplierTourGuide obj)
        {
            try
            {
                if (obj.ID != 0)
                {
                    obj.SupplierTypeId = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.TourGuide.ToString());
                    return obj.Update();
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Rate

        public static bool UpdateSupplierRate(DLSupplierTourGuideRate obj)
        {
            try
            {
                return obj.Update();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierRate(DLSupplierTourGuideRate obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetSupplierRateListBySupplierID(int supplier)
        {
            try
            {
                DLSupplierTourGuideRate obj = new DLSupplierTourGuideRate();
                obj.SupplierTourGuideID = supplier;
                return obj.SelectAllWSupplierTourGuideIDLogicObj();
                //return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DLSupplierTourGuideRate GetSupplierRateListByPk(int id)
        {
            try
            {
                DLSupplierTourGuideRate obj = new DLSupplierTourGuideRate();
                obj.ID = id;
                  obj.SelectOne();
                  return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool DeleteSupplierRateByPk(int ID)
        {
            try
            {
                DLSupplierTourGuideRate obj = new DLSupplierTourGuideRate();
                obj.ID = ID;
                return obj.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region Information Tab function

        public static bool UpdateSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                if (!obj.ID.IsNull)
                    return obj.Update();
                else return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool InsertSupplierInfo(DLSupplierInformation obj)
        {
            try
            {
                return obj.Insert();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static DLSupplierInformation GetSupplierInformation(int SupplierDetailId)
        {
            try
            {
                DLSupplierInformation obj = new DLSupplierInformation();
                obj.SupplierDetailID = SupplierDetailId;
                obj.SelectAllWSupplierDetailIDLogic();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public static DLSupplierInformation GetSupplierInformationByPk(int ID)
        //{
        //    try
        //    {
        //        DLSupplierInformation obj = new DLSupplierInformation();
        //        obj.ID = ID;
        //        obj.SelectOne();
        //        return obj;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion

        #region Uploadphotos
        public static bool DeleteTourGuidePhoto(int id)
        {
            try
            {
                DLTourGuidePhotos tourGuide = new DLTourGuidePhotos();
                tourGuide.ID = id;
                return tourGuide.Delete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Byte[] GetBytesOfTourPhotoById(int TourPhotoid)
        {
            try {
                DLTourGuidePhotos obj = new DLTourGuidePhotos();
                obj.ID = TourPhotoid;
                obj.SelectOne();
                return obj.Photo.IsNull? null: obj.Photo.Value;
            }
            finally { }
        }
        public static DataTable GetAllPhotos(int TourGuideId)
        {

            try {

                DLTourGuidePhotos obj = new DLTourGuidePhotos();
                obj.TourGuideId = TourGuideId;
                return obj.SelectAllWTourGuideIdLogic();
            }
            catch
            {
                return null;
            }
        }
        internal static bool InsertPhotos(DLTourGuidePhotos obj)
        { 
            return obj.Insert();
        }

        #endregion

       
    }
}