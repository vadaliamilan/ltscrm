﻿using LTS_CRM.App_Code.Helper;
using LTSCRM_DL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.Helper
{
    public class CRMPage : System.Web.UI.Page
    {
        public enum Category
        {
            Sex = 0,
            Married,
            SpeakingLang,
            AreaOfWorking,
            TypeOfWork,
            VehicleType,
            NotWorkWithGroup
            ,AppointmentStatus
            ,ChannelStarRating
            ,ChannelStatus
            ,ContactStatus
            ,Contract
            ,ContractWith
            ,Currency
            ,ItineraryType
            ,Language
            ,LeadAdmissionTicket
            ,LeadFlightNeeded
            ,LeadHotelStar
            ,LeadInsurance
            ,LeadMeal
            ,LeadPreferredContactMethod
            ,LeadPreferredContactTime
            ,LeadStatus
            ,LeadTourGuide
            ,LeadTourType
            ,ProductContractType
            ,ProductType
            ,TaskStatus
            ,Seasons
        }
        public string getDescriptionByEnum(Category val)
        {
            try
            {
                var type = typeof(Category);
                var memInfo = type.GetMember(val.ToString());
                var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                var description = ((DescriptionAttribute)attributes[0]).Description;
                if (description != null)
                {
                    return description;
                }
                else
                  return  val.ToString();
            }
            catch
            {
                return val.ToString();
            }
        }
        public const String TabCookieName = "activetab";
        public const String SubTabCookieName = "activesubtab";
        protected void LoadcboCompanyID(DropDownList ddl)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, CompanyName from [Company]  ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "CompanyName";
            ddl.DataValueField = "ID";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            Connection.Close();

        }
        protected void LoadCampaign(DropDownList ddl)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, CampaignName from [MarketingCampaign]  ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "CampaignName";
            ddl.DataValueField = "ID";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem() { Selected = true, Text = "Select Campaign", Value = string.Empty });
            Connection.Close();
            Connection.Close();

        }
        public void LoadCategoryFromGeneralTable(DropDownList ddl,string category,string defaultSelect="--Select--")
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = string.Format("select ID, Title from [GeneralCategoryItems] where Category='{0}'", category);
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "Title";
            ddl.DataValueField = "ID";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem() { Selected = true, Text = defaultSelect, Value = string.Empty });
            Connection.Close();

        }

        public void LoadCategoryFromGeneralTableByValue(DropDownList ddl, string category, string defaultSelect="-- Select --")
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = string.Format("select ID, Title from [GeneralCategoryItems] where Category='{0}'", category);
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "Title";
            ddl.DataValueField = "Title";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem() { Selected = true, Text = defaultSelect, Value = string.Empty });
            Connection.Close();
        }
        
        protected void LoadCategoryTitlesOnly(DropDownList ddl, string defaultSelect="--Choose Category--")
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = string.Format("select Distinct Category from [GeneralCategoryItems] order by Category");
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "Category";
            ddl.DataValueField = "Category";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem() { Selected = true, Text = defaultSelect, Value = string.Empty });
            Connection.Close();
        }
        protected void LoadSupplierType(DropDownList ddl, EnumSupplierType type, bool isEnabled = false)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, SupplierType from [SupplierType] Where IsActive=1  ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "SupplierType";
            ddl.DataValueField = "ID";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            string id = ParseEnum.GetValueOf<EnumSupplierType>(type.ToString()).ToString();
            ddl.Items.FindByValue(id).Selected = true;
            ddl.Enabled = isEnabled;
            Connection.Close();

        }

        public bool isNonZeroAndBlank(DropDownList ddl)
        {
            try
            {
                if (ddl.Items.Count > 0 && !String.IsNullOrEmpty(Convert.ToString(ddl.SelectedValue)) && ddl.SelectedValue != "" && ddl.SelectedValue != "0" && !ddl.SelectedValue.ToLower().StartsWith("--") && !ddl.SelectedValue.ToLower().StartsWith("select")  && ddl.SelectedValue != "-1")
                {
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
  
        #region OPeration quotations Listing.
         
        //public enum QuotationStatuses
        //{
        //    New=0,
        //    NotSent=1,
        //    Confirmed=2,
        //    Closed=3,
        //    NotConfirmed=4
        //}
        public enum Messages
        {
            [Description("Record has been saved successfully")]
            Inserted,
            [Description("Record has been updated successfully")]
            Updated,
            [Description("Record has been deleted successfully")]
            Deleted,
            [Description("something going wrong please try again.")]
            SomethingWrong
        }
        public enum MessageType
        {
            info=0,
            warning=1,
            success=2,
            error=3
        }
        /// <summary>
        /// Fade transition
        /// Slide transition
        /// Plain transition
        /// Closeable Toast
        /// Non closeable Toast
        /// </summary>
        /// <param name="type"></param>
        /// <param name="Message"></param>
        /// <param name="isLoader"></param>
        /// <param name="heading"></param>
        public void MessageBox(string message,MessageType type,string title="Status")
        {
            if (!Page.ClientScript.IsStartupScriptRegistered(this.GetType(), "alert"))
                this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(),"alert", "javascript:toast('" + title + "','" + message + "','" + type.ToString() + "')", true);
        }
        public void LoadEmployee(DropDownList ddl, string defaultSet = "0", int department = 0, bool isEnabled = true, string DefaultText = "-- Select Staff --")
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "";
            if (department == 0)
                sql = "select ID, FirstName+''+Surname as FUllName from [Employee] Where UserStatus='Active'  ";
            else
                sql = "select ID, FirstName+''+Surname as FUllName from [Employee] Where UserStatus='Active' AND DepartmentID= '" + department + "'  ";
            SqlCommand Command = new SqlCommand(sql, Connection);
           
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(Command);
            da.Fill(dt);
            ddl.Items.Clear();
            ddl.DataSource = dt;
            ddl.DataTextField = "FUllName";
            ddl.DataValueField = "ID";
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem(DefaultText,"0"));
            //string id = ParseEnum.GetValueOf<EnumSupplierType>(type.ToString()).ToString();
            if (defaultSet != "0" && ddl.Items.FindByValue(defaultSet)!=null)
            {
                ddl.ClearSelection();
                ddl.Items.FindByValue(defaultSet).Selected = true;
            }
            ddl.Enabled = isEnabled;
            Connection.Close();
        }
      
        public void LoadDepartments(DropDownList ddl, bool isEnabled = false, string DefaultText = "-- Select Department --")
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "";
            sql = "select ID, DepartmentName from [Department] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "DepartmentName";
            ddl.DataValueField = "ID";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem(DefaultText));
            ddl.Enabled = isEnabled;
            Connection.Close();
        }
        public void LoadCompanyIndividual(DropDownList ddl, bool isEnabled = true, string DefaultText = "-- Select Company/Individual --")
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "";
            sql = "select ID, companyName from [Clients] ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            ddl.DataTextField = "companyName";
            ddl.DataValueField = "ID";
            ddl.DataSource = Command.ExecuteReader();
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem(DefaultText));
            ddl.Enabled = isEnabled;
            Connection.Close();
        }

        public void BindRecommendHotels(int companyId,DropDownList ddl,string selectedVal="",string DefaultText = "-- Select Star Hotel --")
        {
            //DLSuppliersDetails hotel = new DLSuppliersDetails();
            DLSupplierMaster hotel = new DLSupplierMaster();
            hotel.CompanyID = companyId;
            hotel.SupplierTypeID  = ParseEnum.GetValueOf<EnumSupplierType>(EnumSupplierType.Hotel.ToString());
            DataTable dt= hotel.SelectAllWSupplierTypeIDLogic();
            ddl.DataTextField = "Name";
            ddl.DataValueField = "ID";
            ddl.DataSource = dt;
            ddl.DataBind();
            ddl.AppendDataBoundItems = true;
            ddl.Items.Insert(0, new ListItem(DefaultText));
            
            if(selectedVal!="")
            {
                ddl.SelectedValue = selectedVal;
            }
        } 
        #endregion
        
        protected void LoadSupplierMaster(DropDownList ddl, EnumSupplierType type, string defaultSelect = "-- Select --", string defaultvalue = "0")
        {
            try
            {
                using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    Connection.Open();
                    String sql = "select [ID], [CompanyID], [SupplierTypeID], [Name] from [SupplierMaster] Where SupplierTypeID='" + Convert.ToInt32(type) + "' ";
                    SqlCommand Command = new SqlCommand(sql, Connection);
                    ddl.DataTextField = "Name";
                    ddl.DataValueField = "ID";
                    ddl.DataSource = Command.ExecuteReader();
                    ddl.DataBind();
                    ddl.AppendDataBoundItems = true;
                    ddl.Items.Insert(0, new ListItem() { Selected = true, Text = defaultSelect, Value = defaultvalue });
                    Connection.Close();
                    Connection.Dispose();
                }
            }
            finally { }
        }
        
        /// <summary>
        /// Load All suppliers
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="type"></param>
        /// <param name="defaultSelect"></param>
        /// <param name="defaultvalue"></param>
        /// <param name="DataTable">return datatable with fillout all supplier Details</param>
        protected void LoadAllSuppliers(DropDownList ddl, out DataTable dtReturn, string defaultSelect = "-- Select --", string defaultvalue = "0")
        {
            try
            {
                using (SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    Connection.Open();
                    String sql = "select * from [SupplierMaster]";
                    SqlCommand Command = new SqlCommand(sql, Connection);
                    SqlDataAdapter sad = new SqlDataAdapter(Command);
                    DataSet retDS=new DataSet();
                    sad.Fill(retDS);
                    dtReturn = retDS.Tables[0];
                    ddl.DataTextField = "Name";
                    ddl.DataValueField = "ID";
                    ddl.DataSource = dtReturn;
                    ddl.DataBind();
                    ddl.ClearSelection();
                    ddl.AppendDataBoundItems = true;
                    ddl.Items.Insert(0, new ListItem() { Selected = true, Text = defaultSelect, Value = defaultvalue });
                    Connection.Close();
                    Connection.Dispose();
                }
            }
            finally { }
        }

        protected void GetAllSupplierType(DropDownList ddl, string defaultSelect = "-- Select --", string defaultvalue = "0")
        {
            try
            {
                Array itemNames = System.Enum.GetNames(typeof(EnumSupplierType));
                ddl.ClearSelection();
                ddl.Items.Clear();
                foreach (String name in itemNames)
                {
                    //get the enum item value
                    Int32 value = (Int32)Enum.Parse(typeof(EnumSupplierType), name);
                    ListItem listItem = new ListItem(name, value.ToString());
                   
                    ddl.Items.Add(listItem);
                }
                ddl.AppendDataBoundItems = true;
                ddl.Items.Insert(0, new ListItem() { Selected = true, Text = defaultSelect, Value = defaultvalue });
                    
            }
            finally { }
        }
        protected override void OnLoad(EventArgs e)
        {
            // Set session for testing.
            TravelSession.Current.UserName = "Nilesh";
            base.OnLoad(e);
        }
        public bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        protected bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }
        protected void ClearTabCookies(string activetab = TabCookieName)
        {
            HttpCookie currentTabCookie = HttpContext.Current.Request.Cookies[activetab];
            if (currentTabCookie != null)
            {
                HttpContext.Current.Response.Cookies.Remove(activetab);
                currentTabCookie.Expires = DateTime.Now.AddDays(-10);
                currentTabCookie.Value = null;
                HttpContext.Current.Response.SetCookie(currentTabCookie);
             }
        }

        public string isNullCheck(SqlString o)
        {
            try
            {
                if (o.IsNull)
                {
                    return "";

                }
                else
                    return o.Value;
            }
            catch
            {
                return "";
            }
        }  
        public string isNullCheck(SqlDecimal o)
        {
            try
            {
                if (o.IsNull)
                {
                    return "";

                }
                else
                    return  o.Value.ToString();
            }
            catch
            {
                return "";
            }
        }
        public string isNullCheck(SqlInt32 o)
        {
            try
            {
                if (o.IsNull)
                {
                    return "";

                }
                else
                    return o.Value.ToString();

            }
            catch
            {
                return "";
            }
        }

        public Decimal getDecimal(string value)
        {
            try
            {
                if ( !String.IsNullOrEmpty(value.Trim()))
                {
                    return Convert.ToDecimal(value.Trim());

                }
                else
                    return 0;
            }
            catch
            {
                return 0;
            }
        }
    }

    public class LTSUserControl: UserControl
    {
        public bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        protected bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }
        public bool isNonZeroAndBlank(DropDownList ddl)
        {
            try
            {
                if (ddl.Items.Count > 0 && !String.IsNullOrEmpty(Convert.ToString(ddl.SelectedValue)) && ddl.SelectedValue != "" && ddl.SelectedValue != "0" && !ddl.SelectedValue.ToLower().StartsWith("select") && !ddl.SelectedValue.ToLower().StartsWith("--") && ddl.SelectedValue != "-1")
                {
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
  
        public enum Messages
        {
            [Description("Record has been saved successfully")]
            Inserted,
            [Description("Record has been updated successfully")]
            Updated,
            [Description("Record has been deleted successfully")]
            Deleted,
            [Description("Mail has been successfully sent!")]
            MailSent
        }
        public enum MessageType
        {
            info = 0,
            warning = 1,
            success = 2,
            error = 3
        }
        /// <summary>
        /// Fade transition
        /// Slide transition
        /// Plain transition
        /// Closeable Toast
        /// Non closeable Toast
        /// </summary>
        /// <param name="type"></param>
        /// <param name="Message"></param>
        /// <param name="isLoader"></param>
        /// <param name="heading"></param>
        public void MessageBox(string message, MessageType type, string title = "Status")
        {
            if (!Page.ClientScript.IsStartupScriptRegistered(this.GetType(), "alert"))
                this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "alert", "javascript:toast('" + title + "','" + message + "','" + type.ToString() + "')", true);
        }
    }

      
      

     
}