﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace LTS_CRM.App_Code.Helper
{
    [Serializable]
    public class OperationSession : IRequiresSessionState
    {
        private int? _OpinionSheetId;

        public int? OpinionSheetId
        {
            get
            {
                return _OpinionSheetId;
            }
            set { _OpinionSheetId = value; }
        }
        private int? _ContactId;

        public int? ContactId
        {
            get
            {
                return _ContactId;
            }
            set { _ContactId = value; }
        }

        private int? _LeadId;

        public int? LeadId
        {
            get
            {
                return _LeadId;
            }
            set { _LeadId = value; }
        }

     
    }
}