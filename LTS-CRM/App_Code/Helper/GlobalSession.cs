﻿using LTS_CRM.App_Code.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace LTS_CRM
{
    [Serializable]
    public class TravelSession : IRequiresSessionState
    {
        //Name that will be used as key for Session object
        private const string SESSION_SINGLETON = "__Session__";
        static readonly MarketingSession _ms = new MarketingSession();
        static readonly ProductSession _ps = new ProductSession();
        static readonly OperationSession _os = new OperationSession();
        static readonly SalesSession _ss = new SalesSession();
        public static MarketingSession Marketing
        {
            get
            {
                return _ms;
            }
        }
        public static SalesSession Sales
        {
            get
            {
                return _ss;
            }
        }
        public static ProductSession Product
        {
            get
            {
                return _ps;
            }
        }
        public static OperationSession Operation
        {
            get
            {
                return _os;
            }
        }
        #region Lazy load object - best way

        //private static readonly Lazy<MarketingSession> ms = new Lazy<MarketingSession>(() => new MarketingSession());
        //public static MarketingSession Marketing { get { return ms.Value; } } 
        #endregion


        // private constructor
        //private TravelSession()
        //{

        //}

        //Variables to store the data (used to be individual
        // session key/value pairs)
        string _UserName;
        int?
            _SupplierMasterID
            , _ClientID
            , _UserID, _LoginId
            , _hotelWholeselarID, _HotelWholeselarContactID
            , _hotelID, _HotelRateID, _HotelInformationID, _HotelContactID
            , _FlightID, _FlightInformationID, _FlightRateID, _FlightContactID
            , _FerryID, _FerryInformationID, _FerryRateID, _FeryContactID
            , _CoachID, _CoachRateID, _CoachInformationID, _CoachContactID
            , _VanHireID, _VanHireRateID, _VanHireInformationID, _VanContactID
            , _InsuranceID, _InsuranceInformationID, _InsuranceRateID, _InsuranceContactID
            , _MealSuppliementID
            , _TrainID, _TrainInfoId, _TrainRateId, _TrainContactId
           , _TheaterTicketID, __TheaterTicketInformationID, _TheaterTicketRateID, _TheaterTicketContactID
            , _ClientContactID
            , _MainContactID
            , _MealSuppliementContactID
            , _TipsID
            , _DMCID, _DMCRateID, _DMCContactID, _DMCInformationID
            , _EntranceTicketID, _EntranceTicketRateID, _EntranceTicketInfoID, _EntranceTicketContactID
            , _RestaurantID, _RestaurantRateID, _RestaurantInfoID, _RestaurantContactID
            , _TourGuideID, _TourGuideRateID, _TourGuideInformationId
            , _QuotationId, _QuotationPriceId,_QuotationPriceBasicId
            ;

        // Gets the current session.
        public static TravelSession Current
        {
            get
            {
                TravelSession session = (TravelSession)HttpContext.Current.Session[SESSION_SINGLETON];
                if (session == null)
                {
                    session = new TravelSession();
                    HttpContext.Current.Session[SESSION_SINGLETON] = session;
                }
                return session;
            }
        }
        public static void Dispose()
        {
            //Cleanup this object so that GC can reclaim space
            System.Web.HttpContext.Current.Session.Remove(SESSION_SINGLETON);
        }
        // **** add your session properties here, e.g like this:
       

        #region User Session
        public int? UserID
        {
            get
            {
                return _UserID;
            }
            set
            {
                _UserID = value;
            }
        }
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }
        public int? LoginId
        {
            get
            {
                return _LoginId;
            }
            set
            {
                _LoginId = value;
            }
        }

        #endregion

        #region Supplier Sessions
        public int? TipsID
        {

            get
            {
                return _TipsID;
            }
            set
            {
                _TipsID = value;
            }
        }
        public int? EntranceTicketID
        {

            get
            {
                return _EntranceTicketID;
            }
            set
            {
                _EntranceTicketID = value;
            }
        }

        public int? EntranceTicketRateID
        {

            get
            {
                return _EntranceTicketRateID;
            }
            set
            {
                _EntranceTicketRateID = value;
            }
        }
        public int? EntranceTicketInfoID
        {

            get
            {
                return _EntranceTicketInfoID;
            }
            set
            {
                _EntranceTicketInfoID = value;
            }
        }
        public int? EntranceTicketContactID
        {

            get
            {
                return _EntranceTicketContactID;
            }
            set
            {
                _EntranceTicketContactID = value;
            }
        }
        public int? RestaurantID
        {

            get
            {
                return _RestaurantID;
            }
            set
            {
                _RestaurantID = value;
            }
        }  
        public int? RestaurantRateID
        {

            get
            {
                return _RestaurantRateID;
            }
            set
            {
                _RestaurantRateID = value;
            }
        }
         public int?  RestaurantInfoID
        {

            get
            {
                return _RestaurantInfoID;
            }
            set
            {
                _RestaurantInfoID = value;
            }
        }
         public int?  RestaurantContactID
        {

            get
            {
                return _RestaurantContactID;
            }
            set
            {
                _RestaurantContactID = value;
            }
        }
        public int? DMCID
        {

            get
            {
                return _DMCID;
            }
            set
            {
                _DMCID = value;
            }
        }
        public int? DMCRateID
        {

            get
            {
                return _DMCRateID;
            }
            set
            {
                _DMCRateID = value;
            }
        }
        public int? DMCInformationID
        {

            get
            {
                return _DMCInformationID;
            }
            set
            {
                _DMCInformationID = value;
            }
        }
        public int?  DMCContactID
        {

            get
            {
                return _DMCContactID;
            }
            set
            {
                _DMCContactID = value;
            }
        }
        public int? TourGuideID
        {

            get
            {
                return _TourGuideID;
            }
            set
            {
                _TourGuideID = value;
            }
        }
        public int? TourGuideRateID
        {

            get
            {
                return _TourGuideRateID;
            }
            set
            {
                _TourGuideRateID = value;
            }
        }
        public int?  TourGuideInformationId
        {

            get
            {
                return _TourGuideInformationId;
            }
            set
            {
                _TourGuideInformationId = value;
            }
        }
        public int? SupplierMasterID
        {

            get
            {
                return _SupplierMasterID;
            }
            set
            {
                _SupplierMasterID = value;
            }
        }
        public int? ClientID
        {

            get
            {
                return _ClientID;
            }
            set
            {
                _ClientID = value;
            }
        }
        public int? HotelWholeselarID
        {
            get
            {
                return _hotelWholeselarID;
            }
            set
            {
                _hotelWholeselarID = value;
            }
        }
        public int? HotelWholeselarContactID
        {
            get
            {
                return _HotelWholeselarContactID;
            }
            set
            {
                _HotelWholeselarContactID = value;
            }
        }
        public int? HotelRateID
        {
            get
            {
                return _HotelRateID;
            }
            set
            {
                _HotelRateID = value;
            }
        }
        public int? HotelInformationID
        {
            get
            {
                return _HotelInformationID;
            }
            set
            {
                _HotelInformationID = value;
            }
        }
        public int?  HotelContactID
        {
            get
            {
                return _HotelContactID;
            }
            set
            {
                _HotelContactID = value;
            }
        }
        public int? HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                _hotelID = value;
            }
        }
        public int? FlightID
        {
            get
            {
                return _FlightID;
            }
            set
            {
                _FlightID = value;
            }
        }
        public int? FlightRateID
        {
            get
            {
                return _FlightRateID;
            }
            set
            {
                _FlightRateID = value;
            }
        }
        public int? FlightInformationID
        {
            get
            {
                return _FlightInformationID;
            }
            set
            {
                _FlightInformationID = value;
            }
        }
        public int? FlightContactID
        {
            get
            {
                return _FlightContactID;
            }
            set
            {
                _FlightContactID = value;
            }
        }
        public int? CoachID
        {
            get
            {
                return _CoachID;
            }
            set
            {
                _CoachID = value;
            }
        }
        public int? CoachContactID
        {
            get
            {
                return _CoachContactID;
            }
            set
            {
                _CoachContactID = value;
            }
        }
        public int? CoachRateID
        {
            get
            {
                return _CoachRateID;
            }
            set
            {
                _CoachRateID = value;
            }
        }
        public int? CoachInformationID
        {
            get
            {
                return _CoachInformationID;
            }
            set
            {
                _CoachInformationID = value;
            }
        }

        public int? FerryID
        {
            get
            {
                return _FerryID;
            }
            set
            {
                _FerryID = value;
            }
        }
        public int? FeryContactID
        {
            get
            {
                return _FeryContactID;
            }
            set
            {
                _FeryContactID = value;
            }
        }
        public int? FerryRateID
        {
            get
            {
                return _FerryRateID;
            }
            set
            {
                _FerryRateID = value;
            }
        }
        public int? FerryInformationID
        {
            get
            {
                return _FerryInformationID;
            }
            set
            {
                _FerryInformationID = value;
            }
        }
        public int? VanHireID
        {
            get
            {
                return _VanHireID;
            }
            set
            {
                _VanHireID = value;
            }
        }
        public int? VanContactID
        {
            get
            {
                return _VanContactID;
            }
            set
            {
                _VanContactID = value;
            }
        }
        public int? VanHireRateID
        {
            get
            {
                return _VanHireRateID;
            }
            set
            {
                _VanHireRateID = value;
            }
        }
        public int? VanHireInformationID
        {
            get
            {
                return _VanHireInformationID;
            }
            set
            {
                _VanHireInformationID = value;
            }
        }
        public int? InsuranceID
        {
            get
            {
                return _InsuranceID;
            }
            set
            {
                _InsuranceID = value;
            }
        }
        public int? InsuranceContactID
        {
            get
            {
                return _InsuranceContactID;
            }
            set
            {
                _InsuranceContactID = value;
            }
        }
        public int? InsuranceInformationID
        {
            get
            {
                return _InsuranceInformationID;
            }
            set
            {
                _InsuranceInformationID = value;
            }
        }
        public int? InsuranceRateID
        {
            get
            {
                return _InsuranceRateID;
            }
            set
            {
                _InsuranceRateID = value;
            }
        }
        public int? MealSuppliementID
        {

            get
            {
                return _MealSuppliementID;
            }
            set
            {
                _MealSuppliementID = value;
            }
        }
        public int? TheaterTicketID
        {
            get
            {
                return _TheaterTicketID;
            }
            set
            {
                _TheaterTicketID = value;
            }
        }
        public int? TheaterTicketInformationID
        {
            get
            {
                return __TheaterTicketInformationID;
            }
            set
            {
                __TheaterTicketInformationID = value;
            }
        }
        public int? TheaterTicketContactID
        {
            get
            {
                return _TheaterTicketContactID;
            }
            set
            {
                _TheaterTicketContactID = value;
            }
        }
        public int? TheaterTicketRateID
        {
            get
            {
                return _TheaterTicketRateID;
            }
            set
            {
                _TheaterTicketRateID = value;
            }
        }
        public int? TrainID
        {
            get
            {
                return _TrainID;
            }
            set
            {
                _TrainID = value;
            }
        }

        public int? TrainInfoId
        {
            get
            {
                return _TrainInfoId;
            }
            set
            {
                _TrainInfoId = value;
            }
        }

        public int? TrainRateId
        {
            get
            {
                return _TrainRateId;
            }
            set
            {
                _TrainRateId = value;
            }
        }
        public int? TrainContactId
        {
            get
            {
                return _TrainContactId;
            }
            set
            {
                _TrainContactId = value;
            }
        }
        public int? ClientContactID
        {
            get
            {
                return _ClientContactID;
            }
            set
            {
                _ClientContactID = value;
            }
        }
        public int? MainContactID
        {
            get
            {
                return _MainContactID;
            }
            set
            {
                _MainContactID = value;
            }
        }
        public int? MealSuppliementContactID
        {
            get
            {
                return _MealSuppliementContactID;
            }
            set
            {
                _MealSuppliementContactID = value;
            }
        }
        #endregion

        #region OperationQuotation
        public bool IsReadonlyQuotation { get; set; } 
        public int? QuotationId
        {

            get
            {
                return _QuotationId;
            }
            set
            {
                _QuotationId= value;
            }
        }
         public int? QuotationPriceId
        {

            get
            {
                return _QuotationPriceId;
            }
            set
            {
                _QuotationPriceId= value;
            }
        }
         public int? QuotationPriceBasicId
         {

             get
             {
                 return _QuotationPriceBasicId;
             }
             set
             {
                 _QuotationPriceBasicId = value;
             }
         }



        #endregion
    }
}