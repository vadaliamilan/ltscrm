﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace LTS_CRM.App_Code.Helper
{
    [Serializable]
    public class ProductSession : IRequiresSessionState
    {
        private int? _ProductId;

        public int? ProductId
        {
            get
            {
                return _ProductId;
            }
            set { _ProductId = value; }
        }

        private int? _ItineraryTemplateId;

        public int? ItineraryTemplateId
        {
            get
            {
                return _ItineraryTemplateId;
            }
            set { _ItineraryTemplateId = value; }
        }

        private int? _PriceTemplateId;

        public int? PriceTemplateId
        {
            get
            {
                return _PriceTemplateId;
            }
            set { _PriceTemplateId = value; }
        }

        private int? _ProductSelfDocumentId;

        public int? ProductSelfDocumentId
        {
            get
            {
                return _ProductSelfDocumentId;
            }
            set { _ProductSelfDocumentId = value; }
        }

    }
}