﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LTS_CRM.App_Code.Helper
{
    public enum EnumSupplierType
    {
        Hotel = 1,
        HotelWholesaler = 2,
        VanHire = 3,
        CoachHire = 4,
        TourGuide = 5,
        Entrance = 6,
        Restaurant = 7,
        Theatre = 8,
        Ferry = 9,
        Train = 10,
        Flight = 11,
        DMC = 12,
        Insurance = 13,
        Tip = 14,
        MealSupplementsfordriverandguide = 15
    }
    public static class ParseEnum
    {
        public static int GetValueOf<T>(string enumValue)
        {
            //Type enumType = Type.GetType(enumName);
            //if (enumType == null)
            //{
            //    throw new ArgumentException("Specified enum type could not be found", "enumName");
            //}

            object value = Enum.Parse(typeof(T), enumValue);
            return Convert.ToInt32(value);
        }
    }
}