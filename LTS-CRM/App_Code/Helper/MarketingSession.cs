﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace LTS_CRM
{
    [Serializable]
    public class MarketingSession : IRequiresSessionState
    {
        private int? _ChannelId, _ChannelContactId, _projectId;
        private int?  _TaskId;
        private int? _CampaignId;
        private int? _AppointmentId;
        private int? _ContactId;
        private int? _LeadId;
        public int? TaskId
        {
            get
            {
                return _TaskId;
            }
            set { _TaskId = value; }
        }
        public int? CampaignId
        {
            get
            {
                return _CampaignId;
            }
            set { _CampaignId = value; }
        }
        public int? ChannelId { get 
        { 
            return _ChannelId; }
            set { _ChannelId = value; }
        }
        public int? ChannelContactId
        {
            get
            {
                return _ChannelContactId;
            }
            set { _ChannelContactId = value; }
        }
        public int? ProjectId
        {
            get
            {
                return _projectId;
            }
            set { _projectId = value; }
        }
        public int? AppointmentId
        {
            get
            {
                return _AppointmentId;
            }
            set { _AppointmentId = value; }
        }
        public int? ContactId
        {
            get
            {
                return _ContactId;
            }
            set { _ContactId = value; }
        }
        public int? LeadId
        {
            get
            {
                return _LeadId;
            }
            set { _LeadId = value; }
        }
    }
}