﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.App_Code.Helper
{
    public static class Exporthelper
    {
        public static void ExportToExcel(Repeater repeaterControl, string filename, string[] headerColumn)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment;filename={0}.xls", filename));
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

            LiteralControl literal = (LiteralControl)repeaterControl.Controls[0].Controls[0];
            StringBuilder header = new StringBuilder();
            header.Append("<tr>");
            foreach (string headercolum in headerColumn)
            {
                header.Append("<th>" + headercolum + "</th>");
            }
            header.Append("</tr>");
            literal.Text = header.ToString();
            foreach (RepeaterItem itemEquipment in repeaterControl.Items)
            {
                if (itemEquipment.ItemType == ListItemType.Item || itemEquipment.ItemType == ListItemType.AlternatingItem)
                {
                    //to get the dropdown of each line
                    LinkButton editLinkBiutton = (LinkButton)itemEquipment.FindControl("buttonEdit");
                    if (editLinkBiutton != null)
                        editLinkBiutton.Visible = false;

                    LinkButton deleteLinkBiutton = (LinkButton)itemEquipment.FindControl("buttonDelete");
                    if (deleteLinkBiutton != null)
                        deleteLinkBiutton.Visible = false;
                }
            }
            //     Your Repeater Name Mine is "Rep"
            repeaterControl.RenderControl(htmlWrite);
            HttpContext.Current.Response.Write("<table>");
            HttpContext.Current.Response.Write(stringWrite.ToString());
            HttpContext.Current.Response.Write("</table>");
            HttpContext.Current.Response.End();
        }
    }
}