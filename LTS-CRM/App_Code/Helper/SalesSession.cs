﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace LTS_CRM
{
    [Serializable]
    public class SalesSession : IRequiresSessionState
    {
     
        private int?  _TaskId;
        private int? _CampaignId;
        private int? _AppointmentId;
        private int? _ContactId;
        private int? _LeadId;
        public int? TaskId
        {
            get
            {
                return _TaskId;
            }
            set { _TaskId = value; }
        }
        public int? CampaignId
        {
            get
            {
                return _CampaignId;
            }
            set { _CampaignId = value; }
        }
        public int? AppointmentId
        {
            get
            {
                return _AppointmentId;
            }
            set { _AppointmentId = value; }
        }
        public int? ContactId
        {
            get
            {
                return _ContactId;
            }
            set { _ContactId = value; }
        }
        public int? LeadId
        {
            get
            {
                return _LeadId;
            }
            set { _LeadId = value; }
        }
    }
}