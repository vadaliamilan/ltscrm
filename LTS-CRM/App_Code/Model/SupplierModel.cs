﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LTS_CRM.Model
{
    public class SupplierModel
    {
        public int id { get; set; }

        public int companyid { get; set; }
        public int suppliertypeid { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string telephone { get; set; }
        public string postalcode { get; set; }
        public string email { get; set; }
        public string credittoletstravel { get; set; }
        public string paymentterms { get; set; }
        public DateTime contractendingdate { get; set; }

    }
}