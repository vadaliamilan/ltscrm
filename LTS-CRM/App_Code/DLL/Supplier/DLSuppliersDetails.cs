using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'SuppliersDetails'.
	/// </summary>
	public class DLSuppliersDetails : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_updatedDate, _contractStartDate, _createdDate, _contractRenewalDate, _supplierDate, _contractEndingDate;
			private SqlInt32		_supplierTypeID, _supplierTypeIDOld, _companyID, _supplierMasterID, _supplierMasterIDOld, _iD;
			private SqlString		_contactEmail, _department, _directPhone, _faxNo, _mobileNo, _title, _howToBook, _surName, _position, _firstName, _weChat, _extraField5, _extraField4, _extraField3, _contractedBy, _updatedBy, _createdBy, _extraField2, _facebook, _skype, _whatsup, _extraField1, _linkedIn, _twitter, _comments, _emergencyNo, _faxNumber, _telephone, _address1, _alternateEmail, _email, _agency, _website, _starRating, _password, _userName, _clientCode, _name, _blackList, _creditToLetsTravel, _paymentTerms, _address2, _mainBusiness, _rating, _country, _state, _city, _franchised, _hotelChain, _postalCode;
			private SqlBinary		_fileData;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLSuppliersDetails()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>CompanyID</LI>
		///		 <LI>SupplierMasterID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierTypeID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractedBy. May be SqlString.Null</LI>
		///		 <LI>Name. May be SqlString.Null</LI>
		///		 <LI>StarRating. May be SqlString.Null</LI>
		///		 <LI>Website. May be SqlString.Null</LI>
		///		 <LI>Agency. May be SqlString.Null</LI>
		///		 <LI>ClientCode. May be SqlString.Null</LI>
		///		 <LI>UserName. May be SqlString.Null</LI>
		///		 <LI>Password. May be SqlString.Null</LI>
		///		 <LI>Telephone. May be SqlString.Null</LI>
		///		 <LI>FaxNumber. May be SqlString.Null</LI>
		///		 <LI>EmergencyNo. May be SqlString.Null</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>AlternateEmail. May be SqlString.Null</LI>
		///		 <LI>Address1. May be SqlString.Null</LI>
		///		 <LI>Address2. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>State. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>PostalCode. May be SqlString.Null</LI>
		///		 <LI>HotelChain. May be SqlString.Null</LI>
		///		 <LI>Franchised. May be SqlString.Null</LI>
		///		 <LI>PaymentTerms. May be SqlString.Null</LI>
		///		 <LI>CreditToLetsTravel. May be SqlString.Null</LI>
		///		 <LI>BlackList. May be SqlString.Null</LI>
		///		 <LI>ContractStartDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractEndingDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractRenewalDate. May be SqlDateTime.Null</LI>
		///		 <LI>Rating. May be SqlString.Null</LI>
		///		 <LI>MainBusiness. May be SqlString.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>FileData. May be SqlBinary.Null</LI>
		///		 <LI>HowToBook. May be SqlString.Null</LI>
		///		 <LI>Title. May be SqlString.Null</LI>
		///		 <LI>SurName. May be SqlString.Null</LI>
		///		 <LI>FirstName. May be SqlString.Null</LI>
		///		 <LI>Position. May be SqlString.Null</LI>
		///		 <LI>Department. May be SqlString.Null</LI>
		///		 <LI>ContactEmail. May be SqlString.Null</LI>
		///		 <LI>DirectPhone. May be SqlString.Null</LI>
		///		 <LI>MobileNo. May be SqlString.Null</LI>
		///		 <LI>FaxNo. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Whatsup. May be SqlString.Null</LI>
		///		 <LI>Skype. May be SqlString.Null</LI>
		///		 <LI>Facebook. May be SqlString.Null</LI>
		///		 <LI>Twitter. May be SqlString.Null</LI>
		///		 <LI>LinkedIn. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierMasterID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierMasterID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTypeID));
				cmdToExecute.Parameters.Add(new SqlParameter("@daSupplierDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _supplierDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sName", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _name));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStarRating", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _starRating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWebsite", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _website));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAgency", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _agency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClientCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _clientCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUserName", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _userName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPassword", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _password));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTelephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _telephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNumber", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmergencyNo", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _emergencyNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAlternateEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _alternateEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress1", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress2", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sState", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _state));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPostalCode", SqlDbType.NVarChar, 6, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postalCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHotelChain", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _hotelChain));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFranchised", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _franchised));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPaymentTerms", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _paymentTerms));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreditToLetsTravel", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _creditToLetsTravel));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 200, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractStartDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractStartDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractEndingDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractEndingDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractRenewalDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractRenewalDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRating", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMainBusiness", SqlDbType.NVarChar, 200, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mainBusiness));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@biFileData", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _fileData));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToBook", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToBook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _title));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 510, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPosition", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _position));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDepartment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _department));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDirectPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _directPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWhatsup", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _whatsup));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSkype", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _skype));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFacebook", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _facebook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTwitter", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _twitter));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLinkedIn", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _linkedIn));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>SupplierMasterID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierTypeID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractedBy. May be SqlString.Null</LI>
		///		 <LI>Name. May be SqlString.Null</LI>
		///		 <LI>StarRating. May be SqlString.Null</LI>
		///		 <LI>Website. May be SqlString.Null</LI>
		///		 <LI>Agency. May be SqlString.Null</LI>
		///		 <LI>ClientCode. May be SqlString.Null</LI>
		///		 <LI>UserName. May be SqlString.Null</LI>
		///		 <LI>Password. May be SqlString.Null</LI>
		///		 <LI>Telephone. May be SqlString.Null</LI>
		///		 <LI>FaxNumber. May be SqlString.Null</LI>
		///		 <LI>EmergencyNo. May be SqlString.Null</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>AlternateEmail. May be SqlString.Null</LI>
		///		 <LI>Address1. May be SqlString.Null</LI>
		///		 <LI>Address2. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>State. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>PostalCode. May be SqlString.Null</LI>
		///		 <LI>HotelChain. May be SqlString.Null</LI>
		///		 <LI>Franchised. May be SqlString.Null</LI>
		///		 <LI>PaymentTerms. May be SqlString.Null</LI>
		///		 <LI>CreditToLetsTravel. May be SqlString.Null</LI>
		///		 <LI>BlackList. May be SqlString.Null</LI>
		///		 <LI>ContractStartDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractEndingDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractRenewalDate. May be SqlDateTime.Null</LI>
		///		 <LI>Rating. May be SqlString.Null</LI>
		///		 <LI>MainBusiness. May be SqlString.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>FileData. May be SqlBinary.Null</LI>
		///		 <LI>HowToBook. May be SqlString.Null</LI>
		///		 <LI>Title. May be SqlString.Null</LI>
		///		 <LI>SurName. May be SqlString.Null</LI>
		///		 <LI>FirstName. May be SqlString.Null</LI>
		///		 <LI>Position. May be SqlString.Null</LI>
		///		 <LI>Department. May be SqlString.Null</LI>
		///		 <LI>ContactEmail. May be SqlString.Null</LI>
		///		 <LI>DirectPhone. May be SqlString.Null</LI>
		///		 <LI>MobileNo. May be SqlString.Null</LI>
		///		 <LI>FaxNo. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Whatsup. May be SqlString.Null</LI>
		///		 <LI>Skype. May be SqlString.Null</LI>
		///		 <LI>Facebook. May be SqlString.Null</LI>
		///		 <LI>Twitter. May be SqlString.Null</LI>
		///		 <LI>LinkedIn. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierMasterID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierMasterID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTypeID));
				cmdToExecute.Parameters.Add(new SqlParameter("@daSupplierDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _supplierDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sName", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _name));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStarRating", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _starRating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWebsite", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _website));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAgency", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _agency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClientCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _clientCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUserName", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _userName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPassword", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _password));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTelephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _telephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNumber", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmergencyNo", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _emergencyNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAlternateEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _alternateEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress1", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress2", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sState", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _state));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPostalCode", SqlDbType.NVarChar, 6, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postalCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHotelChain", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _hotelChain));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFranchised", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _franchised));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPaymentTerms", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _paymentTerms));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreditToLetsTravel", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _creditToLetsTravel));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 200, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractStartDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractStartDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractEndingDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractEndingDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractRenewalDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractRenewalDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRating", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMainBusiness", SqlDbType.NVarChar, 200, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mainBusiness));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@biFileData", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _fileData));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToBook", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToBook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _title));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 510, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPosition", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _position));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDepartment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _department));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDirectPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _directPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWhatsup", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _whatsup));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSkype", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _skype));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFacebook", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _facebook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTwitter", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _twitter));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLinkedIn", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _linkedIn));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'SupplierMasterID.
		/// This method will Update one or more existing rows in the database. It will reset the field 'SupplierMasterID' in
		/// all rows which have as value for this field the value as set in property 'SupplierMasterIDOld' to 
		/// the value as set in property 'SupplierMasterID'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierMasterID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierMasterIDOld. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWSupplierMasterIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierMasterID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierMasterID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierMasterIDOld", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierMasterIDOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_UpdateAllWSupplierMasterIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::UpdateAllWSupplierMasterIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'SupplierTypeID.
		/// This method will Update one or more existing rows in the database. It will reset the field 'SupplierTypeID' in
		/// all rows which have as value for this field the value as set in property 'SupplierTypeIDOld' to 
		/// the value as set in property 'SupplierTypeID'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierTypeID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierTypeIDOld. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWSupplierTypeIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierTypeID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeIDOld", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTypeIDOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_UpdateAllWSupplierTypeIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::UpdateAllWSupplierTypeIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'SupplierMasterID'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierMasterID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWSupplierMasterIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierMasterID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierMasterID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_DeleteAllWSupplierMasterIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::DeleteAllWSupplierMasterIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'SupplierTypeID'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierTypeID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWSupplierTypeIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierTypeID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_DeleteAllWSupplierTypeIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::DeleteAllWSupplierTypeIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>SupplierMasterID</LI>
		///		 <LI>SupplierTypeID</LI>
		///		 <LI>SupplierDate</LI>
		///		 <LI>ContractedBy</LI>
		///		 <LI>Name</LI>
		///		 <LI>StarRating</LI>
		///		 <LI>Website</LI>
		///		 <LI>Agency</LI>
		///		 <LI>ClientCode</LI>
		///		 <LI>UserName</LI>
		///		 <LI>Password</LI>
		///		 <LI>Telephone</LI>
		///		 <LI>FaxNumber</LI>
		///		 <LI>EmergencyNo</LI>
		///		 <LI>Email</LI>
		///		 <LI>AlternateEmail</LI>
		///		 <LI>Address1</LI>
		///		 <LI>Address2</LI>
		///		 <LI>City</LI>
		///		 <LI>State</LI>
		///		 <LI>Country</LI>
		///		 <LI>PostalCode</LI>
		///		 <LI>HotelChain</LI>
		///		 <LI>Franchised</LI>
		///		 <LI>PaymentTerms</LI>
		///		 <LI>CreditToLetsTravel</LI>
		///		 <LI>BlackList</LI>
		///		 <LI>ContractStartDate</LI>
		///		 <LI>ContractEndingDate</LI>
		///		 <LI>ContractRenewalDate</LI>
		///		 <LI>Rating</LI>
		///		 <LI>MainBusiness</LI>
		///		 <LI>Comments</LI>
		///		 <LI>FileData</LI>
		///		 <LI>HowToBook</LI>
		///		 <LI>Title</LI>
		///		 <LI>SurName</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>Position</LI>
		///		 <LI>Department</LI>
		///		 <LI>ContactEmail</LI>
		///		 <LI>DirectPhone</LI>
		///		 <LI>MobileNo</LI>
		///		 <LI>FaxNo</LI>
		///		 <LI>WeChat</LI>
		///		 <LI>Whatsup</LI>
		///		 <LI>Skype</LI>
		///		 <LI>Facebook</LI>
		///		 <LI>Twitter</LI>
		///		 <LI>LinkedIn</LI>
		///		 <LI>ExtraField1</LI>
		///		 <LI>ExtraField2</LI>
		///		 <LI>ExtraField3</LI>
		///		 <LI>ExtraField4</LI>
		///		 <LI>ExtraField5</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SuppliersDetails");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_companyID = (Int32)toReturn.Rows[0]["CompanyID"];
					_supplierMasterID = toReturn.Rows[0]["SupplierMasterID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierMasterID"];
					_supplierTypeID = toReturn.Rows[0]["SupplierTypeID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierTypeID"];
					_supplierDate = toReturn.Rows[0]["SupplierDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["SupplierDate"];
					_contractedBy = toReturn.Rows[0]["ContractedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContractedBy"];
					_name = toReturn.Rows[0]["Name"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Name"];
					_starRating = toReturn.Rows[0]["StarRating"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["StarRating"];
					_website = toReturn.Rows[0]["Website"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Website"];
					_agency = toReturn.Rows[0]["Agency"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Agency"];
					_clientCode = toReturn.Rows[0]["ClientCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ClientCode"];
					_userName = toReturn.Rows[0]["UserName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UserName"];
					_password = toReturn.Rows[0]["Password"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Password"];
					_telephone = toReturn.Rows[0]["Telephone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Telephone"];
					_faxNumber = toReturn.Rows[0]["FaxNumber"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FaxNumber"];
					_emergencyNo = toReturn.Rows[0]["EmergencyNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["EmergencyNo"];
					_email = toReturn.Rows[0]["Email"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Email"];
					_alternateEmail = toReturn.Rows[0]["AlternateEmail"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["AlternateEmail"];
					_address1 = toReturn.Rows[0]["Address1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address1"];
					_address2 = toReturn.Rows[0]["Address2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address2"];
					_city = toReturn.Rows[0]["City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["City"];
					_state = toReturn.Rows[0]["State"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["State"];
					_country = toReturn.Rows[0]["Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Country"];
					_postalCode = toReturn.Rows[0]["PostalCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PostalCode"];
					_hotelChain = toReturn.Rows[0]["HotelChain"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HotelChain"];
					_franchised = toReturn.Rows[0]["Franchised"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Franchised"];
					_paymentTerms = toReturn.Rows[0]["PaymentTerms"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PaymentTerms"];
					_creditToLetsTravel = toReturn.Rows[0]["CreditToLetsTravel"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreditToLetsTravel"];
					_blackList = toReturn.Rows[0]["BlackList"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["BlackList"];
					_contractStartDate = toReturn.Rows[0]["ContractStartDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ContractStartDate"];
					_contractEndingDate = toReturn.Rows[0]["ContractEndingDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ContractEndingDate"];
					_contractRenewalDate = toReturn.Rows[0]["ContractRenewalDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ContractRenewalDate"];
					_rating = toReturn.Rows[0]["Rating"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Rating"];
					_mainBusiness = toReturn.Rows[0]["MainBusiness"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["MainBusiness"];
					_comments = toReturn.Rows[0]["Comments"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Comments"];
					_fileData = toReturn.Rows[0]["FileData"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["FileData"];
					_howToBook = toReturn.Rows[0]["HowToBook"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HowToBook"];
					_title = toReturn.Rows[0]["Title"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Title"];
					_surName = toReturn.Rows[0]["SurName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SurName"];
					_firstName = toReturn.Rows[0]["FirstName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FirstName"];
					_position = toReturn.Rows[0]["Position"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Position"];
					_department = toReturn.Rows[0]["Department"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Department"];
					_contactEmail = toReturn.Rows[0]["ContactEmail"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactEmail"];
					_directPhone = toReturn.Rows[0]["DirectPhone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["DirectPhone"];
					_mobileNo = toReturn.Rows[0]["MobileNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["MobileNo"];
					_faxNo = toReturn.Rows[0]["FaxNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FaxNo"];
					_weChat = toReturn.Rows[0]["WeChat"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["WeChat"];
					_whatsup = toReturn.Rows[0]["Whatsup"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Whatsup"];
					_skype = toReturn.Rows[0]["Skype"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Skype"];
					_facebook = toReturn.Rows[0]["Facebook"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Facebook"];
					_twitter = toReturn.Rows[0]["Twitter"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Twitter"];
					_linkedIn = toReturn.Rows[0]["LinkedIn"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["LinkedIn"];
					_extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
					_extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
					_extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
					_extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
					_extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SuppliersDetails");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'SupplierMasterID'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierMasterID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWSupplierMasterIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SuppliersDetails");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierMasterID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierMasterID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_SelectAllWSupplierMasterIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::SelectAllWSupplierMasterIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'SupplierTypeID'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierTypeID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWSupplierTypeIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SuppliersDetails");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTypeID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SuppliersDetails_SelectAllWSupplierTypeIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSuppliersDetails::SelectAllWSupplierTypeIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 CompanyID
		{
			get
			{
				return _companyID;
			}
			set
			{
				SqlInt32 companyIDTmp = (SqlInt32)value;
				if(companyIDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("CompanyID", "CompanyID can't be NULL");
				}
				_companyID = value;
			}
		}


		public SqlInt32 SupplierMasterID
		{
			get
			{
				return _supplierMasterID;
			}
			set
			{
				_supplierMasterID = value;
			}
		}
		public SqlInt32 SupplierMasterIDOld
		{
			get
			{
				return _supplierMasterIDOld;
			}
			set
			{
				_supplierMasterIDOld = value;
			}
		}


		public SqlInt32 SupplierTypeID
		{
			get
			{
				return _supplierTypeID;
			}
			set
			{
				_supplierTypeID = value;
			}
		}
		public SqlInt32 SupplierTypeIDOld
		{
			get
			{
				return _supplierTypeIDOld;
			}
			set
			{
				_supplierTypeIDOld = value;
			}
		}


		public SqlDateTime SupplierDate
		{
			get
			{
				return _supplierDate;
			}
			set
			{
				_supplierDate = value;
			}
		}


		public SqlString ContractedBy
		{
			get
			{
				return _contractedBy;
			}
			set
			{
				_contractedBy = value;
			}
		}


		public SqlString Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}


		public SqlString StarRating
		{
			get
			{
				return _starRating;
			}
			set
			{
				_starRating = value;
			}
		}


		public SqlString Website
		{
			get
			{
				return _website;
			}
			set
			{
				_website = value;
			}
		}


		public SqlString Agency
		{
			get
			{
				return _agency;
			}
			set
			{
				_agency = value;
			}
		}


		public SqlString ClientCode
		{
			get
			{
				return _clientCode;
			}
			set
			{
				_clientCode = value;
			}
		}


		public SqlString UserName
		{
			get
			{
				return _userName;
			}
			set
			{
				_userName = value;
			}
		}


		public SqlString Password
		{
			get
			{
				return _password;
			}
			set
			{
				_password = value;
			}
		}


		public SqlString Telephone
		{
			get
			{
				return _telephone;
			}
			set
			{
				_telephone = value;
			}
		}


		public SqlString FaxNumber
		{
			get
			{
				return _faxNumber;
			}
			set
			{
				_faxNumber = value;
			}
		}


		public SqlString EmergencyNo
		{
			get
			{
				return _emergencyNo;
			}
			set
			{
				_emergencyNo = value;
			}
		}


		public SqlString Email
		{
			get
			{
				return _email;
			}
			set
			{
				_email = value;
			}
		}


		public SqlString AlternateEmail
		{
			get
			{
				return _alternateEmail;
			}
			set
			{
				_alternateEmail = value;
			}
		}


		public SqlString Address1
		{
			get
			{
				return _address1;
			}
			set
			{
				_address1 = value;
			}
		}


		public SqlString Address2
		{
			get
			{
				return _address2;
			}
			set
			{
				_address2 = value;
			}
		}


		public SqlString City
		{
			get
			{
				return _city;
			}
			set
			{
				_city = value;
			}
		}


		public SqlString State
		{
			get
			{
				return _state;
			}
			set
			{
				_state = value;
			}
		}


		public SqlString Country
		{
			get
			{
				return _country;
			}
			set
			{
				_country = value;
			}
		}


		public SqlString PostalCode
		{
			get
			{
				return _postalCode;
			}
			set
			{
				_postalCode = value;
			}
		}


		public SqlString HotelChain
		{
			get
			{
				return _hotelChain;
			}
			set
			{
				_hotelChain = value;
			}
		}


		public SqlString Franchised
		{
			get
			{
				return _franchised;
			}
			set
			{
				_franchised = value;
			}
		}


		public SqlString PaymentTerms
		{
			get
			{
				return _paymentTerms;
			}
			set
			{
				_paymentTerms = value;
			}
		}


		public SqlString CreditToLetsTravel
		{
			get
			{
				return _creditToLetsTravel;
			}
			set
			{
				_creditToLetsTravel = value;
			}
		}


		public SqlString BlackList
		{
			get
			{
				return _blackList;
			}
			set
			{
				_blackList = value;
			}
		}


		public SqlDateTime ContractStartDate
		{
			get
			{
				return _contractStartDate;
			}
			set
			{
				_contractStartDate = value;
			}
		}


		public SqlDateTime ContractEndingDate
		{
			get
			{
				return _contractEndingDate;
			}
			set
			{
				_contractEndingDate = value;
			}
		}


		public SqlDateTime ContractRenewalDate
		{
			get
			{
				return _contractRenewalDate;
			}
			set
			{
				_contractRenewalDate = value;
			}
		}


		public SqlString Rating
		{
			get
			{
				return _rating;
			}
			set
			{
				_rating = value;
			}
		}


		public SqlString MainBusiness
		{
			get
			{
				return _mainBusiness;
			}
			set
			{
				_mainBusiness = value;
			}
		}


		public SqlString Comments
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}


		public SqlBinary FileData
		{
			get
			{
				return _fileData;
			}
			set
			{
				_fileData = value;
			}
		}


		public SqlString HowToBook
		{
			get
			{
				return _howToBook;
			}
			set
			{
				_howToBook = value;
			}
		}


		public SqlString Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
			}
		}


		public SqlString SurName
		{
			get
			{
				return _surName;
			}
			set
			{
				_surName = value;
			}
		}


		public SqlString FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				_firstName = value;
			}
		}


		public SqlString Position
		{
			get
			{
				return _position;
			}
			set
			{
				_position = value;
			}
		}


		public SqlString Department
		{
			get
			{
				return _department;
			}
			set
			{
				_department = value;
			}
		}


		public SqlString ContactEmail
		{
			get
			{
				return _contactEmail;
			}
			set
			{
				_contactEmail = value;
			}
		}


		public SqlString DirectPhone
		{
			get
			{
				return _directPhone;
			}
			set
			{
				_directPhone = value;
			}
		}


		public SqlString MobileNo
		{
			get
			{
				return _mobileNo;
			}
			set
			{
				_mobileNo = value;
			}
		}


		public SqlString FaxNo
		{
			get
			{
				return _faxNo;
			}
			set
			{
				_faxNo = value;
			}
		}


		public SqlString WeChat
		{
			get
			{
				return _weChat;
			}
			set
			{
				_weChat = value;
			}
		}


		public SqlString Whatsup
		{
			get
			{
				return _whatsup;
			}
			set
			{
				_whatsup = value;
			}
		}


		public SqlString Skype
		{
			get
			{
				return _skype;
			}
			set
			{
				_skype = value;
			}
		}


		public SqlString Facebook
		{
			get
			{
				return _facebook;
			}
			set
			{
				_facebook = value;
			}
		}


		public SqlString Twitter
		{
			get
			{
				return _twitter;
			}
			set
			{
				_twitter = value;
			}
		}


		public SqlString LinkedIn
		{
			get
			{
				return _linkedIn;
			}
			set
			{
				_linkedIn = value;
			}
		}


		public SqlString ExtraField1
		{
			get
			{
				return _extraField1;
			}
			set
			{
				_extraField1 = value;
			}
		}


		public SqlString ExtraField2
		{
			get
			{
				return _extraField2;
			}
			set
			{
				_extraField2 = value;
			}
		}


		public SqlString ExtraField3
		{
			get
			{
				return _extraField3;
			}
			set
			{
				_extraField3 = value;
			}
		}


		public SqlString ExtraField4
		{
			get
			{
				return _extraField4;
			}
			set
			{
				_extraField4 = value;
			}
		}


		public SqlString ExtraField5
		{
			get
			{
				return _extraField5;
			}
			set
			{
				_extraField5 = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
