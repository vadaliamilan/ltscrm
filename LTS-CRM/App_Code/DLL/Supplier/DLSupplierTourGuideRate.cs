 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'SupplierTourGuideRate'.
	/// </summary>
	public class DLSupplierTourGuideRate : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_fromDate, _toDate, _createdDate, _updatedDate;
			private SqlDecimal		_full_day_12_17_Seater_Driver_Guide_WithVehicle, _full_day_tips, _full_Day_coach_Guide, _full_day_4_9_Seater_Driver_Guide_With_Vehicle, _london_Edinburgh_With_Vehicle, _london_Edinburgh_Without_Vehicle, _extra_hour_With_Vehicle, _extra_hour_Without_vehicle, _full_day_12_17_Seater_Driver_Guide_Without_Vehicle, _half_day_coach_Guide, _half_day_12_17_Seater_Driver_GuideWithoutVehicle, _half_day_4_9_SeaterDriverGuideWithoutVehicle, _half_day_4_9_Seater_Driver_Guide_With_Vehicle, _full_day_4_9_Seater_Driver_Guide_Without_Vehicle, _half_day_tips, _half_day_12_17_Seater_Driver_Guide_With_Vehicle;
			private SqlInt32		_supplierTourGuideID, _supplierTourGuideIDOld, _iD;
			private SqlString		_season, _updatedBy, _createdBy;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLSupplierTourGuideRate()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierTourGuideID. May be SqlInt32.Null</LI>
		///		 <LI>Season. May be SqlString.Null</LI>
		///		 <LI>FromDate. May be SqlDateTime.Null</LI>
		///		 <LI>ToDate. May be SqlDateTime.Null</LI>
		///		 <LI>Half_day_4_9_SeaterDriverGuideWithoutVehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_12_17_Seater_Driver_GuideWithoutVehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_coach_Guide. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_4_9_Seater_Driver_Guide_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_12_17_Seater_Driver_Guide_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_tips. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_4_9_Seater_Driver_Guide_Without_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_12_17_Seater_Driver_Guide_Without_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_Day_coach_Guide. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_4_9_Seater_Driver_Guide_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_12_17_Seater_Driver_Guide_WithVehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_tips. May be SqlDecimal.Null</LI>
		///		 <LI>Extra_hour_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Extra_hour_Without_vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>London_Edinburgh_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>London_Edinburgh_Without_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTourGuideID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTourGuideID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSeason", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _season));
				cmdToExecute.Parameters.Add(new SqlParameter("@daFromDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _fromDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daToDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _toDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_4_9_SeaterDriverGuideWithoutVehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_12_17_Seater_Driver_GuideWithoutVehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_coach_Guide", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_coach_Guide));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_4_9_Seater_Driver_Guide_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_12_17_Seater_Driver_Guide_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_tips", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_tips));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_4_9_Seater_Driver_Guide_Without_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_12_17_Seater_Driver_Guide_Without_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_Day_coach_Guide", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_Day_coach_Guide));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_4_9_Seater_Driver_Guide_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_12_17_Seater_Driver_Guide_WithVehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_tips", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_tips));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcExtra_hour_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _extra_hour_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcExtra_hour_Without_vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _extra_hour_Without_vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcLondon_Edinburgh_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _london_Edinburgh_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcLondon_Edinburgh_Without_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _london_Edinburgh_Without_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>SupplierTourGuideID. May be SqlInt32.Null</LI>
		///		 <LI>Season. May be SqlString.Null</LI>
		///		 <LI>FromDate. May be SqlDateTime.Null</LI>
		///		 <LI>ToDate. May be SqlDateTime.Null</LI>
		///		 <LI>Half_day_4_9_SeaterDriverGuideWithoutVehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_12_17_Seater_Driver_GuideWithoutVehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_coach_Guide. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_4_9_Seater_Driver_Guide_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_12_17_Seater_Driver_Guide_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Half_day_tips. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_4_9_Seater_Driver_Guide_Without_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_12_17_Seater_Driver_Guide_Without_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_Day_coach_Guide. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_4_9_Seater_Driver_Guide_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_12_17_Seater_Driver_Guide_WithVehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Full_day_tips. May be SqlDecimal.Null</LI>
		///		 <LI>Extra_hour_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>Extra_hour_Without_vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>London_Edinburgh_With_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>London_Edinburgh_Without_Vehicle. May be SqlDecimal.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTourGuideID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTourGuideID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSeason", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _season));
				cmdToExecute.Parameters.Add(new SqlParameter("@daFromDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _fromDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daToDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _toDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_4_9_SeaterDriverGuideWithoutVehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_4_9_SeaterDriverGuideWithoutVehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_12_17_Seater_Driver_GuideWithoutVehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_12_17_Seater_Driver_GuideWithoutVehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_coach_Guide", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_coach_Guide));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_4_9_Seater_Driver_Guide_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_4_9_Seater_Driver_Guide_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_12_17_Seater_Driver_Guide_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_12_17_Seater_Driver_Guide_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcHalf_day_tips", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _half_day_tips));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_4_9_Seater_Driver_Guide_Without_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_4_9_Seater_Driver_Guide_Without_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_12_17_Seater_Driver_Guide_Without_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_12_17_Seater_Driver_Guide_Without_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_Day_coach_Guide", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_Day_coach_Guide));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_4_9_Seater_Driver_Guide_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_4_9_Seater_Driver_Guide_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_12_17_Seater_Driver_Guide_WithVehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_12_17_Seater_Driver_Guide_WithVehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcFull_day_tips", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _full_day_tips));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcExtra_hour_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _extra_hour_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcExtra_hour_Without_vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _extra_hour_Without_vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcLondon_Edinburgh_With_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _london_Edinburgh_With_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcLondon_Edinburgh_Without_Vehicle", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _london_Edinburgh_Without_Vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'SupplierTourGuideID.
		/// This method will Update one or more existing rows in the database. It will reset the field 'SupplierTourGuideID' in
		/// all rows which have as value for this field the value as set in property 'SupplierTourGuideIDOld' to 
		/// the value as set in property 'SupplierTourGuideID'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierTourGuideID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierTourGuideIDOld. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWSupplierTourGuideIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTourGuideID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierTourGuideID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTourGuideIDOld", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTourGuideIDOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_UpdateAllWSupplierTourGuideIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::UpdateAllWSupplierTourGuideIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'SupplierTourGuideID'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierTourGuideID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWSupplierTourGuideIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTourGuideID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierTourGuideID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_DeleteAllWSupplierTourGuideIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::DeleteAllWSupplierTourGuideIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>SupplierTourGuideID</LI>
		///		 <LI>Season</LI>
		///		 <LI>FromDate</LI>
		///		 <LI>ToDate</LI>
		///		 <LI>Half_day_4_9_SeaterDriverGuideWithoutVehicle</LI>
		///		 <LI>Half_day_12_17_Seater_Driver_GuideWithoutVehicle</LI>
		///		 <LI>Half_day_coach_Guide</LI>
		///		 <LI>Half_day_4_9_Seater_Driver_Guide_With_Vehicle</LI>
		///		 <LI>Half_day_12_17_Seater_Driver_Guide_With_Vehicle</LI>
		///		 <LI>Half_day_tips</LI>
		///		 <LI>Full_day_4_9_Seater_Driver_Guide_Without_Vehicle</LI>
		///		 <LI>Full_day_12_17_Seater_Driver_Guide_Without_Vehicle</LI>
		///		 <LI>Full_Day_coach_Guide</LI>
		///		 <LI>Full_day_4_9_Seater_Driver_Guide_With_Vehicle</LI>
		///		 <LI>Full_day_12_17_Seater_Driver_Guide_WithVehicle</LI>
		///		 <LI>Full_day_tips</LI>
		///		 <LI>Extra_hour_With_Vehicle</LI>
		///		 <LI>Extra_hour_Without_vehicle</LI>
		///		 <LI>London_Edinburgh_With_Vehicle</LI>
		///		 <LI>London_Edinburgh_Without_Vehicle</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierTourGuideRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_supplierTourGuideID = toReturn.Rows[0]["SupplierTourGuideID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierTourGuideID"];
					_season = toReturn.Rows[0]["Season"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Season"];
					_fromDate = toReturn.Rows[0]["FromDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["FromDate"];
					_toDate = toReturn.Rows[0]["ToDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ToDate"];
					_half_day_4_9_SeaterDriverGuideWithoutVehicle = toReturn.Rows[0]["Half_day_4_9_SeaterDriverGuideWithoutVehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_4_9_SeaterDriverGuideWithoutVehicle"];
					_half_day_12_17_Seater_Driver_GuideWithoutVehicle = toReturn.Rows[0]["Half_day_12_17_Seater_Driver_GuideWithoutVehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_12_17_Seater_Driver_GuideWithoutVehicle"];
					_half_day_coach_Guide = toReturn.Rows[0]["Half_day_coach_Guide"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_coach_Guide"];
					_half_day_4_9_Seater_Driver_Guide_With_Vehicle = toReturn.Rows[0]["Half_day_4_9_Seater_Driver_Guide_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_4_9_Seater_Driver_Guide_With_Vehicle"];
					_half_day_12_17_Seater_Driver_Guide_With_Vehicle = toReturn.Rows[0]["Half_day_12_17_Seater_Driver_Guide_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_12_17_Seater_Driver_Guide_With_Vehicle"];
					_half_day_tips = toReturn.Rows[0]["Half_day_tips"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_tips"];
					_full_day_4_9_Seater_Driver_Guide_Without_Vehicle = toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_Without_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_Without_Vehicle"];
					_full_day_12_17_Seater_Driver_Guide_Without_Vehicle = toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_Without_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_Without_Vehicle"];
					_full_Day_coach_Guide = toReturn.Rows[0]["Full_Day_coach_Guide"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_Day_coach_Guide"];
					_full_day_4_9_Seater_Driver_Guide_With_Vehicle = toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_With_Vehicle"];
					_full_day_12_17_Seater_Driver_Guide_WithVehicle = toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_WithVehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_WithVehicle"];
					_full_day_tips = toReturn.Rows[0]["Full_day_tips"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_tips"];
					_extra_hour_With_Vehicle = toReturn.Rows[0]["Extra_hour_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Extra_hour_With_Vehicle"];
					_extra_hour_Without_vehicle = toReturn.Rows[0]["Extra_hour_Without_vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Extra_hour_Without_vehicle"];
					_london_Edinburgh_With_Vehicle = toReturn.Rows[0]["London_Edinburgh_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["London_Edinburgh_With_Vehicle"];
					_london_Edinburgh_Without_Vehicle = toReturn.Rows[0]["London_Edinburgh_Without_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["London_Edinburgh_Without_Vehicle"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierTourGuideRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'SupplierTourGuideID'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierTourGuideID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWSupplierTourGuideIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierTourGuideRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTourGuideID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTourGuideID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuideRate::SelectAllWSupplierTourGuideIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}
        public DataTable SelectAllWSupplierTourGuideIDLogicObj()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("SupplierTourGuideRate");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTourGuideID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTourGuideID));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (toReturn.Rows.Count > 0)
                {
                    _iD = (Int32)toReturn.Rows[0]["ID"];
                    _supplierTourGuideID = toReturn.Rows[0]["SupplierTourGuideID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierTourGuideID"];
                    _season = toReturn.Rows[0]["Season"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Season"];
                    _fromDate = toReturn.Rows[0]["FromDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["FromDate"];
                    _toDate = toReturn.Rows[0]["ToDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ToDate"];
                    _half_day_4_9_SeaterDriverGuideWithoutVehicle = toReturn.Rows[0]["Half_day_4_9_SeaterDriverGuideWithoutVehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_4_9_SeaterDriverGuideWithoutVehicle"];
                    _half_day_12_17_Seater_Driver_GuideWithoutVehicle = toReturn.Rows[0]["Half_day_12_17_Seater_Driver_GuideWithoutVehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_12_17_Seater_Driver_GuideWithoutVehicle"];
                    _half_day_coach_Guide = toReturn.Rows[0]["Half_day_coach_Guide"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_coach_Guide"];
                    _half_day_4_9_Seater_Driver_Guide_With_Vehicle = toReturn.Rows[0]["Half_day_4_9_Seater_Driver_Guide_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_4_9_Seater_Driver_Guide_With_Vehicle"];
                    _half_day_12_17_Seater_Driver_Guide_With_Vehicle = toReturn.Rows[0]["Half_day_12_17_Seater_Driver_Guide_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_12_17_Seater_Driver_Guide_With_Vehicle"];
                    _half_day_tips = toReturn.Rows[0]["Half_day_tips"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Half_day_tips"];
                    _full_day_4_9_Seater_Driver_Guide_Without_Vehicle = toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_Without_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_Without_Vehicle"];
                    _full_day_12_17_Seater_Driver_Guide_Without_Vehicle = toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_Without_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_Without_Vehicle"];
                    _full_Day_coach_Guide = toReturn.Rows[0]["Full_Day_coach_Guide"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_Day_coach_Guide"];
                    _full_day_4_9_Seater_Driver_Guide_With_Vehicle = toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_4_9_Seater_Driver_Guide_With_Vehicle"];
                    _full_day_12_17_Seater_Driver_Guide_WithVehicle = toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_WithVehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_12_17_Seater_Driver_Guide_WithVehicle"];
                    _full_day_tips = toReturn.Rows[0]["Full_day_tips"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Full_day_tips"];
                    _extra_hour_With_Vehicle = toReturn.Rows[0]["Extra_hour_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Extra_hour_With_Vehicle"];
                    _extra_hour_Without_vehicle = toReturn.Rows[0]["Extra_hour_Without_vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Extra_hour_Without_vehicle"];
                    _london_Edinburgh_With_Vehicle = toReturn.Rows[0]["London_Edinburgh_With_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["London_Edinburgh_With_Vehicle"];
                    _london_Edinburgh_Without_Vehicle = toReturn.Rows[0]["London_Edinburgh_Without_Vehicle"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["London_Edinburgh_Without_Vehicle"];
                    _createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
                    _createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
                    _updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
                    _updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
                } 
                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_SupplierTourGuideRate_SelectAllWSupplierTourGuideIDLogic' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLSupplierTourGuideRate::SelectAllWSupplierTourGuideIDLogic::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 SupplierTourGuideID
		{
			get
			{
				return _supplierTourGuideID;
			}
			set
			{
				_supplierTourGuideID = value;
			}
		}
		public SqlInt32 SupplierTourGuideIDOld
		{
			get
			{
				return _supplierTourGuideIDOld;
			}
			set
			{
				_supplierTourGuideIDOld = value;
			}
		}


		public SqlString Season
		{
			get
			{
				return _season;
			}
			set
			{
				_season = value;
			}
		}


		public SqlDateTime FromDate
		{
			get
			{
				return _fromDate;
			}
			set
			{
				_fromDate = value;
			}
		}


		public SqlDateTime ToDate
		{
			get
			{
				return _toDate;
			}
			set
			{
				_toDate = value;
			}
		}


		public SqlDecimal Half_day_4_9_SeaterDriverGuideWithoutVehicle
		{
			get
			{
				return _half_day_4_9_SeaterDriverGuideWithoutVehicle;
			}
			set
			{
				_half_day_4_9_SeaterDriverGuideWithoutVehicle = value;
			}
		}


		public SqlDecimal Half_day_12_17_Seater_Driver_GuideWithoutVehicle
		{
			get
			{
				return _half_day_12_17_Seater_Driver_GuideWithoutVehicle;
			}
			set
			{
				_half_day_12_17_Seater_Driver_GuideWithoutVehicle = value;
			}
		}


		public SqlDecimal Half_day_coach_Guide
		{
			get
			{
				return _half_day_coach_Guide;
			}
			set
			{
				_half_day_coach_Guide = value;
			}
		}


		public SqlDecimal Half_day_4_9_Seater_Driver_Guide_With_Vehicle
		{
			get
			{
				return _half_day_4_9_Seater_Driver_Guide_With_Vehicle;
			}
			set
			{
				_half_day_4_9_Seater_Driver_Guide_With_Vehicle = value;
			}
		}


		public SqlDecimal Half_day_12_17_Seater_Driver_Guide_With_Vehicle
		{
			get
			{
				return _half_day_12_17_Seater_Driver_Guide_With_Vehicle;
			}
			set
			{
				_half_day_12_17_Seater_Driver_Guide_With_Vehicle = value;
			}
		}


		public SqlDecimal Half_day_tips
		{
			get
			{
				return _half_day_tips;
			}
			set
			{
				_half_day_tips = value;
			}
		}


		public SqlDecimal Full_day_4_9_Seater_Driver_Guide_Without_Vehicle
		{
			get
			{
				return _full_day_4_9_Seater_Driver_Guide_Without_Vehicle;
			}
			set
			{
				_full_day_4_9_Seater_Driver_Guide_Without_Vehicle = value;
			}
		}


		public SqlDecimal Full_day_12_17_Seater_Driver_Guide_Without_Vehicle
		{
			get
			{
				return _full_day_12_17_Seater_Driver_Guide_Without_Vehicle;
			}
			set
			{
				_full_day_12_17_Seater_Driver_Guide_Without_Vehicle = value;
			}
		}


		public SqlDecimal Full_Day_coach_Guide
		{
			get
			{
				return _full_Day_coach_Guide;
			}
			set
			{
				_full_Day_coach_Guide = value;
			}
		}


		public SqlDecimal Full_day_4_9_Seater_Driver_Guide_With_Vehicle
		{
			get
			{
				return _full_day_4_9_Seater_Driver_Guide_With_Vehicle;
			}
			set
			{
				_full_day_4_9_Seater_Driver_Guide_With_Vehicle = value;
			}
		}


		public SqlDecimal Full_day_12_17_Seater_Driver_Guide_WithVehicle
		{
			get
			{
				return _full_day_12_17_Seater_Driver_Guide_WithVehicle;
			}
			set
			{
				_full_day_12_17_Seater_Driver_Guide_WithVehicle = value;
			}
		}


		public SqlDecimal Full_day_tips
		{
			get
			{
				return _full_day_tips;
			}
			set
			{
				_full_day_tips = value;
			}
		}


		public SqlDecimal Extra_hour_With_Vehicle
		{
			get
			{
				return _extra_hour_With_Vehicle;
			}
			set
			{
				_extra_hour_With_Vehicle = value;
			}
		}


		public SqlDecimal Extra_hour_Without_vehicle
		{
			get
			{
				return _extra_hour_Without_vehicle;
			}
			set
			{
				_extra_hour_Without_vehicle = value;
			}
		}


		public SqlDecimal London_Edinburgh_With_Vehicle
		{
			get
			{
				return _london_Edinburgh_With_Vehicle;
			}
			set
			{
				_london_Edinburgh_With_Vehicle = value;
			}
		}


		public SqlDecimal London_Edinburgh_Without_Vehicle
		{
			get
			{
				return _london_Edinburgh_Without_Vehicle;
			}
			set
			{
				_london_Edinburgh_Without_Vehicle = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
