using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'SupplierInformation'.
	/// </summary>
	public class DLSupplierInformation : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_createdDate, _updatedDate;
			private SqlInt32		_iD, _noofBedRooms, _supplierDetailID, _supplierDetailIDOld;
			private SqlString		_extraField5, _extraField4, _createdBy, _updatedBy, _extraField1, _remarks, _parking, _hotelFacility, _extraField3, _extraField2, _currency;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLSupplierInformation()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>Currency. May be SqlString.Null</LI>
		///		 <LI>NoofBedRooms. May be SqlInt32.Null</LI>
		///		 <LI>HotelFacility. May be SqlString.Null</LI>
		///		 <LI>Parking. May be SqlString.Null</LI>
		///		 <LI>Remarks. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCurrency", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _currency));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoofBedRooms", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noofBedRooms));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHotelFacility", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _hotelFacility));
				cmdToExecute.Parameters.Add(new SqlParameter("@sParking", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _parking));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemarks", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remarks));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>Currency. May be SqlString.Null</LI>
		///		 <LI>NoofBedRooms. May be SqlInt32.Null</LI>
		///		 <LI>HotelFacility. May be SqlString.Null</LI>
		///		 <LI>Parking. May be SqlString.Null</LI>
		///		 <LI>Remarks. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCurrency", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _currency));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoofBedRooms", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noofBedRooms));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHotelFacility", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _hotelFacility));
				cmdToExecute.Parameters.Add(new SqlParameter("@sParking", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _parking));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemarks", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remarks));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'SupplierDetailID.
		/// This method will Update one or more existing rows in the database. It will reset the field 'SupplierDetailID' in
		/// all rows which have as value for this field the value as set in property 'SupplierDetailIDOld' to 
		/// the value as set in property 'SupplierDetailID'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierDetailIDOld. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailIDOld", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailIDOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_UpdateAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::UpdateAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'SupplierDetailID'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_DeleteAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::DeleteAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>SupplierDetailID</LI>
		///		 <LI>Currency</LI>
		///		 <LI>NoofBedRooms</LI>
		///		 <LI>HotelFacility</LI>
		///		 <LI>Parking</LI>
		///		 <LI>Remarks</LI>
		///		 <LI>ExtraField1</LI>
		///		 <LI>ExtraField2</LI>
		///		 <LI>ExtraField3</LI>
		///		 <LI>ExtraField4</LI>
		///		 <LI>ExtraField5</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierInformation");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_supplierDetailID = toReturn.Rows[0]["SupplierDetailID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierDetailID"];
					_currency = toReturn.Rows[0]["Currency"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Currency"];
					_noofBedRooms = toReturn.Rows[0]["NoofBedRooms"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoofBedRooms"];
					_hotelFacility = toReturn.Rows[0]["HotelFacility"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HotelFacility"];
					_parking = toReturn.Rows[0]["Parking"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Parking"];
					_remarks = toReturn.Rows[0]["Remarks"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Remarks"];
					_extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
					_extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
					_extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
					_extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
					_extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];


                    SupplierName = toReturn.Rows[0]["SupplierName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SupplierName"];
					PostalCode = toReturn.Rows[0]["InfoPostalCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InfoPostalCode"];
					Telehpone = toReturn.Rows[0]["InfoTelephone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InfoTelephone"];
					City = toReturn.Rows[0]["InfoCity"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InfoCity"];
					State = toReturn.Rows[0]["InfoState"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InfoState"];
					Country = toReturn.Rows[0]["InfoCountry"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InfoCountry"];
                    Address1 = toReturn.Rows[0]["InfoAddress1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InfoAddress1"];
                    Address2 = toReturn.Rows[0]["InfoAddress2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InfoAddress2"];

				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierInformation");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'SupplierDetailID'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierInformation_SelectAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierInformation");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierInformation_SelectAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

                if (toReturn.Rows.Count > 0)
                {
                    _iD = (Int32)toReturn.Rows[0]["ID"];
                    _supplierDetailID = toReturn.Rows[0]["SupplierDetailID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierDetailID"];
                    _currency = toReturn.Rows[0]["Currency"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Currency"];
                    _noofBedRooms = toReturn.Rows[0]["NoofBedRooms"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoofBedRooms"];
                    _hotelFacility = toReturn.Rows[0]["HotelFacility"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HotelFacility"];
                    _parking = toReturn.Rows[0]["Parking"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Parking"];
                    _remarks = toReturn.Rows[0]["Remarks"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Remarks"];
                    _extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
                    _extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
                    _extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
                    _extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
                    _extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
                    _createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
                    _createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
                    _updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
                    _updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];


                    SupplierName = toReturn.Rows[0]["Name"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Name"];
                    PostalCode = toReturn.Rows[0]["PostalCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PostalCode"];
                    Telehpone = toReturn.Rows[0]["Telephone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Telephone"];
                    City = toReturn.Rows[0]["City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["City"];
                    State = toReturn.Rows[0]["State"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["State"];
                    Country = toReturn.Rows[0]["Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Country"];
                    Address1 = toReturn.Rows[0]["Address1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address1"];
                    Address2 = toReturn.Rows[0]["Address2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address2"];

                }
                return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierInformation::SelectAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 SupplierDetailID
		{
			get
			{
				return _supplierDetailID;
			}
			set
			{
				_supplierDetailID = value;
			}
		}
		public SqlInt32 SupplierDetailIDOld
		{
			get
			{
				return _supplierDetailIDOld;
			}
			set
			{
				_supplierDetailIDOld = value;
			}
		}


		public SqlString Currency
		{
			get
			{
				return _currency;
			}
			set
			{
				_currency = value;
			}
		}


		public SqlInt32 NoofBedRooms
		{
			get
			{
				return _noofBedRooms;
			}
			set
			{
				_noofBedRooms = value;
			}
		}


		public SqlString HotelFacility
		{
			get
			{
				return _hotelFacility;
			}
			set
			{
				_hotelFacility = value;
			}
		}


		public SqlString Parking
		{
			get
			{
				return _parking;
			}
			set
			{
				_parking = value;
			}
		}


		public SqlString Remarks
		{
			get
			{
				return _remarks;
			}
			set
			{
				_remarks = value;
			}
		}


		public SqlString ExtraField1
		{
			get
			{
				return _extraField1;
			}
			set
			{
				_extraField1 = value;
			}
		}


		public SqlString ExtraField2
		{
			get
			{
				return _extraField2;
			}
			set
			{
				_extraField2 = value;
			}
		}


		public SqlString ExtraField3
		{
			get
			{
				return _extraField3;
			}
			set
			{
				_extraField3 = value;
			}
		}


		public SqlString ExtraField4
		{
			get
			{
				return _extraField4;
			}
			set
			{
				_extraField4 = value;
			}
		}


		public SqlString ExtraField5
		{
			get
			{
				return _extraField5;
			}
			set
			{
				_extraField5 = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion


        public SqlString SupplierName { get; set; }
        public SqlString PostalCode { get; set; }

        public SqlString Telehpone { get; set; }

        public SqlString City { get; set; }
        public SqlString State { get; set; }
        public SqlString Country { get; set; } 
        public SqlString Address1 { get; set; }
         public SqlString Address2 { get; set; }
    }
}
