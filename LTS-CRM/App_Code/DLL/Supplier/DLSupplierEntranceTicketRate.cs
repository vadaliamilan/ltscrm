 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'SupplierEntranceTicketRate'.
	/// </summary>
	public class DLSupplierEntranceTicketRate : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_monthTo, _dailyOpeningTime, _dailyCloseTime, _updatedDate, _createdDate, _monthFrom;
			private SqlInt32		_iD, _supplierDetailID, _supplierDetailIDOld;
			private SqlString		_lastAdmission, _updatedBy, _status, _remark, _createdBy;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLSupplierEntranceTicketRate()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>MonthFrom. May be SqlDateTime.Null</LI>
		///		 <LI>MonthTo. May be SqlDateTime.Null</LI>
		///		 <LI>DailyOpeningTime. May be SqlDateTime.Null</LI>
		///		 <LI>DailyCloseTime. May be SqlDateTime.Null</LI>
		///		 <LI>LastAdmission. May be SqlString.Null</LI>
		///		 <LI>Status. May be SqlString.Null</LI>
		///		 <LI>Remark. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@daMonthFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _monthFrom));
				cmdToExecute.Parameters.Add(new SqlParameter("@daMonthTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _monthTo));
				cmdToExecute.Parameters.Add(new SqlParameter("@daDailyOpeningTime", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dailyOpeningTime));
				cmdToExecute.Parameters.Add(new SqlParameter("@daDailyCloseTime", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dailyCloseTime));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLastAdmission", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _lastAdmission));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStatus", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _status));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemark", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remark));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>MonthFrom. May be SqlDateTime.Null</LI>
		///		 <LI>MonthTo. May be SqlDateTime.Null</LI>
		///		 <LI>DailyOpeningTime. May be SqlDateTime.Null</LI>
		///		 <LI>DailyCloseTime. May be SqlDateTime.Null</LI>
		///		 <LI>LastAdmission. May be SqlString.Null</LI>
		///		 <LI>Status. May be SqlString.Null</LI>
		///		 <LI>Remark. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@daMonthFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _monthFrom));
				cmdToExecute.Parameters.Add(new SqlParameter("@daMonthTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _monthTo));
				cmdToExecute.Parameters.Add(new SqlParameter("@daDailyOpeningTime", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dailyOpeningTime));
				cmdToExecute.Parameters.Add(new SqlParameter("@daDailyCloseTime", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dailyCloseTime));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLastAdmission", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _lastAdmission));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStatus", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _status));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemark", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remark));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'SupplierDetailID.
		/// This method will Update one or more existing rows in the database. It will reset the field 'SupplierDetailID' in
		/// all rows which have as value for this field the value as set in property 'SupplierDetailIDOld' to 
		/// the value as set in property 'SupplierDetailID'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierDetailIDOld. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailIDOld", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailIDOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_UpdateAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::UpdateAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'SupplierDetailID'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_DeleteAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::DeleteAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>SupplierDetailID</LI>
		///		 <LI>MonthFrom</LI>
		///		 <LI>MonthTo</LI>
		///		 <LI>DailyOpeningTime</LI>
		///		 <LI>DailyCloseTime</LI>
		///		 <LI>LastAdmission</LI>
		///		 <LI>Status</LI>
		///		 <LI>Remark</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierEntranceTicketRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_supplierDetailID = toReturn.Rows[0]["SupplierDetailID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierDetailID"];
					_monthFrom = toReturn.Rows[0]["MonthFrom"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["MonthFrom"];
					_monthTo = toReturn.Rows[0]["MonthTo"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["MonthTo"];
					_dailyOpeningTime = toReturn.Rows[0]["DailyOpeningTime"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["DailyOpeningTime"];
					_dailyCloseTime = toReturn.Rows[0]["DailyCloseTime"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["DailyCloseTime"];
					_lastAdmission = toReturn.Rows[0]["LastAdmission"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["LastAdmission"];
					_status = toReturn.Rows[0]["Status"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Status"];
					_remark = toReturn.Rows[0]["Remark"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Remark"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierEntranceTicketRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'SupplierDetailID'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierEntranceTicketRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierEntranceTicketRate_SelectAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierEntranceTicketRate::SelectAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 SupplierDetailID
		{
			get
			{
				return _supplierDetailID;
			}
			set
			{
				_supplierDetailID = value;
			}
		}
		public SqlInt32 SupplierDetailIDOld
		{
			get
			{
				return _supplierDetailIDOld;
			}
			set
			{
				_supplierDetailIDOld = value;
			}
		}


		public SqlDateTime MonthFrom
		{
			get
			{
				return _monthFrom;
			}
			set
			{
				_monthFrom = value;
			}
		}


		public SqlDateTime MonthTo
		{
			get
			{
				return _monthTo;
			}
			set
			{
				_monthTo = value;
			}
		}


		public SqlDateTime DailyOpeningTime
		{
			get
			{
				return _dailyOpeningTime;
			}
			set
			{
				_dailyOpeningTime = value;
			}
		}


		public SqlDateTime DailyCloseTime
		{
			get
			{
				return _dailyCloseTime;
			}
			set
			{
				_dailyCloseTime = value;
			}
		}


		public SqlString LastAdmission
		{
			get
			{
				return _lastAdmission;
			}
			set
			{
				_lastAdmission = value;
			}
		}


		public SqlString Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}


		public SqlString Remark
		{
			get
			{
				return _remark;
			}
			set
			{
				_remark = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
