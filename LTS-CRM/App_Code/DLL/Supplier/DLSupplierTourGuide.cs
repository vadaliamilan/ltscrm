using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.InteropServices;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'SupplierTourGuide'.
	/// </summary>
	public class DLSupplierTourGuide : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_dateOfBirth, _createdDate, _updatedDate;
			private SqlInt32		_children, _noOfYearTourGuide, _rating, _iD, _companyID, _supplierTypeId, _typeOfSupplier, _typeOfSupplierOld;
			private SqlString		_kindOfGroupNotWork, _specialRequirement, _blackList, _kindOfGroupWork, _areaOfWorking, _guideLicenceInit, _vehicleOwned, _typeOfWork, _extraField5, _extraField4, _updatedBy, _createdBy, _extraField3, _howToBook, _comments, _extraField2, _extraField1, _surName, _married, _visaStatus, _firstName, _homePhone, _sex, _firstNameInChinese, _surNameInChinese, _nationality, _origin, _country, _weChat, _address1, _city, _address2, _postCode, _mobilePhone, _email, _speakingLanguage, _recruitedBy;
			private SqlBinary		_passportCopy, _addressProof, _vehiclePhoto, _guideLicence, _drivingLicence, _cVOfGuide;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLSupplierTourGuide()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierTypeId. May be SqlInt32.Null</LI>
		///		 <LI>TypeOfSupplier. May be SqlInt32.Null</LI>
		///		 <LI>RecruitedBy. May be SqlString.Null</LI>
		///		 <LI>SurName. May be SqlString.Null</LI>
		///		 <LI>SurNameInChinese. May be SqlString.Null</LI>
		///		 <LI>FirstName. May be SqlString.Null</LI>
		///		 <LI>FirstNameInChinese. May be SqlString.Null</LI>
		///		 <LI>Sex. May be SqlString.Null</LI>
		///		 <LI>DateOfBirth. May be SqlDateTime.Null</LI>
		///		 <LI>Origin. May be SqlString.Null</LI>
		///		 <LI>Nationality. May be SqlString.Null</LI>
		///		 <LI>Married. May be SqlString.Null</LI>
		///		 <LI>Children. May be SqlInt32.Null</LI>
		///		 <LI>VisaStatus. May be SqlString.Null</LI>
		///		 <LI>HomePhone. May be SqlString.Null</LI>
		///		 <LI>MobilePhone. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>Address1. May be SqlString.Null</LI>
		///		 <LI>Address2. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>PostCode. May be SqlString.Null</LI>
		///		 <LI>SpeakingLanguage. May be SqlString.Null</LI>
		///		 <LI>GuideLicenceInit. May be SqlString.Null</LI>
		///		 <LI>NoOfYearTourGuide. May be SqlInt32.Null</LI>
		///		 <LI>AreaOfWorking. May be SqlString.Null</LI>
		///		 <LI>TypeOfWork. May be SqlString.Null</LI>
		///		 <LI>VehicleOwned. May be SqlString.Null</LI>
		///		 <LI>KindOfGroupWork. May be SqlString.Null</LI>
		///		 <LI>KindOfGroupNotWork. May be SqlString.Null</LI>
		///		 <LI>SpecialRequirement. May be SqlString.Null</LI>
		///		 <LI>Rating. May be SqlInt32.Null</LI>
		///		 <LI>BlackList. May be SqlString.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>DrivingLicence. May be SqlBinary.Null</LI>
		///		 <LI>VehiclePhoto. May be SqlBinary.Null</LI>
		///		 <LI>PassportCopy. May be SqlBinary.Null</LI>
		///		 <LI>AddressProof. May be SqlBinary.Null</LI>
		///		 <LI>GuideLicence. May be SqlBinary.Null</LI>
		///		 <LI>CVOfGuide. May be SqlBinary.Null</LI>
		///		 <LI>HowToBook. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTypeId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iTypeOfSupplier", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _typeOfSupplier));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRecruitedBy", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _recruitedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurNameInChinese", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _surNameInChinese));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstNameInChinese", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstNameInChinese));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSex", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _sex));
				cmdToExecute.Parameters.Add(new SqlParameter("@daDateOfBirth", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dateOfBirth));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOrigin", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _origin));
				cmdToExecute.Parameters.Add(new SqlParameter("@sNationality", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _nationality));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMarried", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _married));
				cmdToExecute.Parameters.Add(new SqlParameter("@iChildren", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _children));
				cmdToExecute.Parameters.Add(new SqlParameter("@sVisaStatus", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _visaStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHomePhone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _homePhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobilePhone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobilePhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress1", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress2", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPostCode", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSpeakingLanguage", SqlDbType.NVarChar, 1000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _speakingLanguage));
				cmdToExecute.Parameters.Add(new SqlParameter("@sGuideLicenceInit", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _guideLicenceInit));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfYearTourGuide", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfYearTourGuide));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAreaOfWorking", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _areaOfWorking));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTypeOfWork", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _typeOfWork));
				cmdToExecute.Parameters.Add(new SqlParameter("@sVehicleOwned", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _vehicleOwned));
				cmdToExecute.Parameters.Add(new SqlParameter("@sKindOfGroupWork", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _kindOfGroupWork));
				cmdToExecute.Parameters.Add(new SqlParameter("@sKindOfGroupNotWork", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _kindOfGroupNotWork));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSpecialRequirement", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _specialRequirement));
				cmdToExecute.Parameters.Add(new SqlParameter("@iRating", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@biDrivingLicence", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _drivingLicence));
				cmdToExecute.Parameters.Add(new SqlParameter("@biVehiclePhoto", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _vehiclePhoto));
				cmdToExecute.Parameters.Add(new SqlParameter("@biPassportCopy", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _passportCopy));
				cmdToExecute.Parameters.Add(new SqlParameter("@biAddressProof", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _addressProof));
				cmdToExecute.Parameters.Add(new SqlParameter("@biGuideLicence", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _guideLicence));
				cmdToExecute.Parameters.Add(new SqlParameter("@biCVOfGuide", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _cVOfGuide));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToBook", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToBook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierTypeId. May be SqlInt32.Null</LI>
		///		 <LI>TypeOfSupplier. May be SqlInt32.Null</LI>
		///		 <LI>RecruitedBy. May be SqlString.Null</LI>
		///		 <LI>SurName. May be SqlString.Null</LI>
		///		 <LI>SurNameInChinese. May be SqlString.Null</LI>
		///		 <LI>FirstName. May be SqlString.Null</LI>
		///		 <LI>FirstNameInChinese. May be SqlString.Null</LI>
		///		 <LI>Sex. May be SqlString.Null</LI>
		///		 <LI>DateOfBirth. May be SqlDateTime.Null</LI>
		///		 <LI>Origin. May be SqlString.Null</LI>
		///		 <LI>Nationality. May be SqlString.Null</LI>
		///		 <LI>Married. May be SqlString.Null</LI>
		///		 <LI>Children. May be SqlInt32.Null</LI>
		///		 <LI>VisaStatus. May be SqlString.Null</LI>
		///		 <LI>HomePhone. May be SqlString.Null</LI>
		///		 <LI>MobilePhone. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>Address1. May be SqlString.Null</LI>
		///		 <LI>Address2. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>PostCode. May be SqlString.Null</LI>
		///		 <LI>SpeakingLanguage. May be SqlString.Null</LI>
		///		 <LI>GuideLicenceInit. May be SqlString.Null</LI>
		///		 <LI>NoOfYearTourGuide. May be SqlInt32.Null</LI>
		///		 <LI>AreaOfWorking. May be SqlString.Null</LI>
		///		 <LI>TypeOfWork. May be SqlString.Null</LI>
		///		 <LI>VehicleOwned. May be SqlString.Null</LI>
		///		 <LI>KindOfGroupWork. May be SqlString.Null</LI>
		///		 <LI>KindOfGroupNotWork. May be SqlString.Null</LI>
		///		 <LI>SpecialRequirement. May be SqlString.Null</LI>
		///		 <LI>Rating. May be SqlInt32.Null</LI>
		///		 <LI>BlackList. May be SqlString.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>DrivingLicence. May be SqlBinary.Null</LI>
		///		 <LI>VehiclePhoto. May be SqlBinary.Null</LI>
		///		 <LI>PassportCopy. May be SqlBinary.Null</LI>
		///		 <LI>AddressProof. May be SqlBinary.Null</LI>
		///		 <LI>GuideLicence. May be SqlBinary.Null</LI>
		///		 <LI>CVOfGuide. May be SqlBinary.Null</LI>
		///		 <LI>HowToBook. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierTypeId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierTypeId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iTypeOfSupplier", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _typeOfSupplier));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRecruitedBy", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _recruitedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurNameInChinese", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _surNameInChinese));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstNameInChinese", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstNameInChinese));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSex", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _sex));
				cmdToExecute.Parameters.Add(new SqlParameter("@daDateOfBirth", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dateOfBirth));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOrigin", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _origin));
				cmdToExecute.Parameters.Add(new SqlParameter("@sNationality", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _nationality));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMarried", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _married));
				cmdToExecute.Parameters.Add(new SqlParameter("@iChildren", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _children));
				cmdToExecute.Parameters.Add(new SqlParameter("@sVisaStatus", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _visaStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHomePhone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _homePhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobilePhone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobilePhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress1", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress2", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPostCode", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSpeakingLanguage", SqlDbType.NVarChar, 1000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _speakingLanguage));
				cmdToExecute.Parameters.Add(new SqlParameter("@sGuideLicenceInit", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _guideLicenceInit));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfYearTourGuide", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfYearTourGuide));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAreaOfWorking", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _areaOfWorking));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTypeOfWork", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _typeOfWork));
				cmdToExecute.Parameters.Add(new SqlParameter("@sVehicleOwned", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _vehicleOwned));
				cmdToExecute.Parameters.Add(new SqlParameter("@sKindOfGroupWork", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _kindOfGroupWork));
				cmdToExecute.Parameters.Add(new SqlParameter("@sKindOfGroupNotWork", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _kindOfGroupNotWork));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSpecialRequirement", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _specialRequirement));
				cmdToExecute.Parameters.Add(new SqlParameter("@iRating", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@biDrivingLicence", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _drivingLicence));
				cmdToExecute.Parameters.Add(new SqlParameter("@biVehiclePhoto", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _vehiclePhoto));
				cmdToExecute.Parameters.Add(new SqlParameter("@biPassportCopy", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _passportCopy));
				cmdToExecute.Parameters.Add(new SqlParameter("@biAddressProof", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _addressProof));
				cmdToExecute.Parameters.Add(new SqlParameter("@biGuideLicence", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _guideLicence));
				cmdToExecute.Parameters.Add(new SqlParameter("@biCVOfGuide", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _cVOfGuide));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToBook", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToBook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'TypeOfSupplier.
		/// This method will Update one or more existing rows in the database. It will reset the field 'TypeOfSupplier' in
		/// all rows which have as value for this field the value as set in property 'TypeOfSupplierOld' to 
		/// the value as set in property 'TypeOfSupplier'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>TypeOfSupplier. May be SqlInt32.Null</LI>
		///		 <LI>TypeOfSupplierOld. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWTypeOfSupplierLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iTypeOfSupplier", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _typeOfSupplier));
				cmdToExecute.Parameters.Add(new SqlParameter("@iTypeOfSupplierOld", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _typeOfSupplierOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_UpdateAllWTypeOfSupplierLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::UpdateAllWTypeOfSupplierLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'TypeOfSupplier'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>TypeOfSupplier. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWTypeOfSupplierLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iTypeOfSupplier", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _typeOfSupplier));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_DeleteAllWTypeOfSupplierLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::DeleteAllWTypeOfSupplierLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>SupplierTypeId</LI>
		///		 <LI>TypeOfSupplier</LI>
		///		 <LI>RecruitedBy</LI>
		///		 <LI>SurName</LI>
		///		 <LI>SurNameInChinese</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>FirstNameInChinese</LI>
		///		 <LI>Sex</LI>
		///		 <LI>DateOfBirth</LI>
		///		 <LI>Origin</LI>
		///		 <LI>Nationality</LI>
		///		 <LI>Married</LI>
		///		 <LI>Children</LI>
		///		 <LI>VisaStatus</LI>
		///		 <LI>HomePhone</LI>
		///		 <LI>MobilePhone</LI>
		///		 <LI>WeChat</LI>
		///		 <LI>Email</LI>
		///		 <LI>Address1</LI>
		///		 <LI>Address2</LI>
		///		 <LI>City</LI>
		///		 <LI>Country</LI>
		///		 <LI>PostCode</LI>
		///		 <LI>SpeakingLanguage</LI>
		///		 <LI>GuideLicenceInit</LI>
		///		 <LI>NoOfYearTourGuide</LI>
		///		 <LI>AreaOfWorking</LI>
		///		 <LI>TypeOfWork</LI>
		///		 <LI>VehicleOwned</LI>
		///		 <LI>KindOfGroupWork</LI>
		///		 <LI>KindOfGroupNotWork</LI>
		///		 <LI>SpecialRequirement</LI>
		///		 <LI>Rating</LI>
		///		 <LI>BlackList</LI>
		///		 <LI>Comments</LI>
		///		 <LI>DrivingLicence</LI>
		///		 <LI>VehiclePhoto</LI>
		///		 <LI>PassportCopy</LI>
		///		 <LI>AddressProof</LI>
		///		 <LI>GuideLicence</LI>
		///		 <LI>CVOfGuide</LI>
		///		 <LI>ExtraField1</LI>
		///		 <LI>ExtraField2</LI>
		///		 <LI>ExtraField3</LI>
		///		 <LI>ExtraField4</LI>
		///		 <LI>ExtraField5</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierTourGuide");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_companyID = toReturn.Rows[0]["CompanyID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["CompanyID"];
					_supplierTypeId = toReturn.Rows[0]["SupplierTypeId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierTypeId"];
					_typeOfSupplier = toReturn.Rows[0]["TypeOfSupplier"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["TypeOfSupplier"];
					_recruitedBy = toReturn.Rows[0]["RecruitedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["RecruitedBy"];
					_surName = toReturn.Rows[0]["SurName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SurName"];
					_surNameInChinese = toReturn.Rows[0]["SurNameInChinese"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SurNameInChinese"];
					_firstName = toReturn.Rows[0]["FirstName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FirstName"];
					_firstNameInChinese = toReturn.Rows[0]["FirstNameInChinese"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FirstNameInChinese"];
					_sex = toReturn.Rows[0]["Sex"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Sex"];
					_dateOfBirth = toReturn.Rows[0]["DateOfBirth"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["DateOfBirth"];
					_origin = toReturn.Rows[0]["Origin"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Origin"];
					_nationality = toReturn.Rows[0]["Nationality"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Nationality"];
					_married = toReturn.Rows[0]["Married"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Married"];
					_children = toReturn.Rows[0]["Children"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["Children"];
					_visaStatus = toReturn.Rows[0]["VisaStatus"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["VisaStatus"];
					_homePhone = toReturn.Rows[0]["HomePhone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HomePhone"];
					_mobilePhone = toReturn.Rows[0]["MobilePhone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["MobilePhone"];
					_weChat = toReturn.Rows[0]["WeChat"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["WeChat"];
					_email = toReturn.Rows[0]["Email"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Email"];
					_address1 = toReturn.Rows[0]["Address1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address1"];
					_address2 = toReturn.Rows[0]["Address2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address2"];
					_city = toReturn.Rows[0]["City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["City"];
					_country = toReturn.Rows[0]["Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Country"];
					_postCode = toReturn.Rows[0]["PostCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PostCode"];
					_speakingLanguage = toReturn.Rows[0]["SpeakingLanguage"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SpeakingLanguage"];
					_guideLicenceInit = toReturn.Rows[0]["GuideLicenceInit"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["GuideLicenceInit"];
					_noOfYearTourGuide = toReturn.Rows[0]["NoOfYearTourGuide"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfYearTourGuide"];
					_areaOfWorking = toReturn.Rows[0]["AreaOfWorking"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["AreaOfWorking"];
					_typeOfWork = toReturn.Rows[0]["TypeOfWork"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["TypeOfWork"];
					_vehicleOwned = toReturn.Rows[0]["VehicleOwned"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["VehicleOwned"];
					_kindOfGroupWork = toReturn.Rows[0]["KindOfGroupWork"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["KindOfGroupWork"];
					_kindOfGroupNotWork = toReturn.Rows[0]["KindOfGroupNotWork"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["KindOfGroupNotWork"];
					_specialRequirement = toReturn.Rows[0]["SpecialRequirement"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SpecialRequirement"];
					_rating = toReturn.Rows[0]["Rating"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["Rating"];
					_blackList = toReturn.Rows[0]["BlackList"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["BlackList"];
					_comments = toReturn.Rows[0]["Comments"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Comments"];
					_drivingLicence = toReturn.Rows[0]["DrivingLicence"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["DrivingLicence"];
					_vehiclePhoto = toReturn.Rows[0]["VehiclePhoto"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["VehiclePhoto"];
					_passportCopy = toReturn.Rows[0]["PassportCopy"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["PassportCopy"];
					_addressProof = toReturn.Rows[0]["AddressProof"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["AddressProof"];
					_guideLicence = toReturn.Rows[0]["GuideLicence"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["GuideLicence"];
					_cVOfGuide = toReturn.Rows[0]["CVOfGuide"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["CVOfGuide"];
					_howToBook = toReturn.Rows[0]["HowToBook"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HowToBook"];
					_extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
					_extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
					_extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
					_extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
					_extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierTourGuide");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'TypeOfSupplier'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>TypeOfSupplier. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWTypeOfSupplierLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierTourGuide");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iTypeOfSupplier", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _typeOfSupplier));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierTourGuide_SelectAllWTypeOfSupplierLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierTourGuide::SelectAllWTypeOfSupplierLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 CompanyID
		{
			get
			{
				return _companyID;
			}
			set
			{
				_companyID = value;
			}
		}


		public SqlInt32 SupplierTypeId
		{
			get
			{
				return _supplierTypeId;
			}
			set
			{
				_supplierTypeId = value;
			}
		}


		public SqlInt32 TypeOfSupplier
		{
			get
			{
				return _typeOfSupplier;
			}
			set
			{
				_typeOfSupplier = value;
			}
		}
		public SqlInt32 TypeOfSupplierOld
		{
			get
			{
				return _typeOfSupplierOld;
			}
			set
			{
				_typeOfSupplierOld = value;
			}
		}


		public SqlString RecruitedBy
		{
			get
			{
				return _recruitedBy;
			}
			set
			{
				_recruitedBy = value;
			}
		}


		public SqlString SurName
		{
			get
			{
				return _surName;
			}
			set
			{
				_surName = value;
			}
		}


		public SqlString SurNameInChinese
		{
			get
			{
				return _surNameInChinese;
			}
			set
			{
				_surNameInChinese = value;
			}
		}


		public SqlString FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				_firstName = value;
			}
		}


		public SqlString FirstNameInChinese
		{
			get
			{
				return _firstNameInChinese;
			}
			set
			{
				_firstNameInChinese = value;
			}
		}


		public SqlString Sex
		{
			get
			{
				return _sex;
			}
			set
			{
				_sex = value;
			}
		}


		public SqlDateTime DateOfBirth
		{
			get
			{
				return _dateOfBirth;
			}
			set
			{
				_dateOfBirth = value;
			}
		}


		public SqlString Origin
		{
			get
			{
				return _origin;
			}
			set
			{
				_origin = value;
			}
		}


		public SqlString Nationality
		{
			get
			{
				return _nationality;
			}
			set
			{
				_nationality = value;
			}
		}


		public SqlString Married
		{
			get
			{
				return _married;
			}
			set
			{
				_married = value;
			}
		}


		public SqlInt32 Children
		{
			get
			{
				return _children;
			}
			set
			{
				_children = value;
			}
		}


		public SqlString VisaStatus
		{
			get
			{
				return _visaStatus;
			}
			set
			{
				_visaStatus = value;
			}
		}


		public SqlString HomePhone
		{
			get
			{
				return _homePhone;
			}
			set
			{
				_homePhone = value;
			}
		}


		public SqlString MobilePhone
		{
			get
			{
				return _mobilePhone;
			}
			set
			{
				_mobilePhone = value;
			}
		}


		public SqlString WeChat
		{
			get
			{
				return _weChat;
			}
			set
			{
				_weChat = value;
			}
		}


		public SqlString Email
		{
			get
			{
				return _email;
			}
			set
			{
				_email = value;
			}
		}


		public SqlString Address1
		{
			get
			{
				return _address1;
			}
			set
			{
				_address1 = value;
			}
		}


		public SqlString Address2
		{
			get
			{
				return _address2;
			}
			set
			{
				_address2 = value;
			}
		}


		public SqlString City
		{
			get
			{
				return _city;
			}
			set
			{
				_city = value;
			}
		}


		public SqlString Country
		{
			get
			{
				return _country;
			}
			set
			{
				_country = value;
			}
		}


		public SqlString PostCode
		{
			get
			{
				return _postCode;
			}
			set
			{
				_postCode = value;
			}
		}


		public SqlString SpeakingLanguage
		{
			get
			{
				return _speakingLanguage;
			}
			set
			{
				_speakingLanguage = value;
			}
		}


		public SqlString GuideLicenceInit
		{
			get
			{
				return _guideLicenceInit;
			}
			set
			{
				_guideLicenceInit = value;
			}
		}


		public SqlInt32 NoOfYearTourGuide
		{
			get
			{
				return _noOfYearTourGuide;
			}
			set
			{
				_noOfYearTourGuide = value;
			}
		}


		public SqlString AreaOfWorking
		{
			get
			{
				return _areaOfWorking;
			}
			set
			{
				_areaOfWorking = value;
			}
		}


		public SqlString TypeOfWork
		{
			get
			{
				return _typeOfWork;
			}
			set
			{
				_typeOfWork = value;
			}
		}


		public SqlString VehicleOwned
		{
			get
			{
				return _vehicleOwned;
			}
			set
			{
				_vehicleOwned = value;
			}
		}


		public SqlString KindOfGroupWork
		{
			get
			{
				return _kindOfGroupWork;
			}
			set
			{
				_kindOfGroupWork = value;
			}
		}


		public SqlString KindOfGroupNotWork
		{
			get
			{
				return _kindOfGroupNotWork;
			}
			set
			{
				_kindOfGroupNotWork = value;
			}
		}


		public SqlString SpecialRequirement
		{
			get
			{
				return _specialRequirement;
			}
			set
			{
				_specialRequirement = value;
			}
		}


		public SqlInt32 Rating
		{
			get
			{
				return _rating;
			}
			set
			{
				_rating = value;
			}
		}


		public SqlString BlackList
		{
			get
			{
				return _blackList;
			}
			set
			{
				_blackList = value;
			}
		}


		public SqlString Comments
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}


		public SqlBinary DrivingLicence
		{
			get
			{
				return _drivingLicence;
			}
			set
			{
				_drivingLicence = value;
			}
		}


		public SqlBinary VehiclePhoto
		{
			get
			{
				return _vehiclePhoto;
			}
			set
			{
				_vehiclePhoto = value;
			}
		}


		public SqlBinary PassportCopy
		{
			get
			{
				return _passportCopy;
			}
			set
			{
				_passportCopy = value;
			}
		}


		public SqlBinary AddressProof
		{
			get
			{
				return _addressProof;
			}
			set
			{
				_addressProof = value;
			}
		}


		public SqlBinary GuideLicence
		{
			get
			{
				return _guideLicence;
			}
			set
			{
				_guideLicence = value;
			}
		}


		public SqlBinary CVOfGuide
		{
			get
			{
				return _cVOfGuide;
			}
			set
			{
				_cVOfGuide = value;
			}
		}


		public SqlString HowToBook
		{
			get
			{
				return _howToBook;
			}
			set
			{
				_howToBook = value;
			}
		}


		public SqlString ExtraField1
		{
			get
			{
				return _extraField1;
			}
			set
			{
				_extraField1 = value;
			}
		}


		public SqlString ExtraField2
		{
			get
			{
				return _extraField2;
			}
			set
			{
				_extraField2 = value;
			}
		}


		public SqlString ExtraField3
		{
			get
			{
				return _extraField3;
			}
			set
			{
				_extraField3 = value;
			}
		}


		public SqlString ExtraField4
		{
			get
			{
				return _extraField4;
			}
			set
			{
				_extraField4 = value;
			}
		}


		public SqlString ExtraField5
		{
			get
			{
				return _extraField5;
			}
			set
			{
				_extraField5 = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
