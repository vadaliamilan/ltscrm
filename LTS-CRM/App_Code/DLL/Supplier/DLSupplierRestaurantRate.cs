 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'SupplierRestaurantRate'.
	/// </summary>
	public class DLSupplierRestaurantRate : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_createdDate, _updatedDate;
			private SqlDecimal		_rate;
			private SqlInt32		_iD, _supplierDetailID, _supplierDetailIDOld;
			private SqlString		_extraField5, _extraField3, _extraField4, _comments, _updatedBy, _createdBy, _description, _sampleOfMenu, _typeOfFood, _extraField2, _extraField1;
			private SqlBinary		_foodPictureFile, _sampleMenuFile;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLSupplierRestaurantRate()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>TypeOfFood. May be SqlString.Null</LI>
		///		 <LI>Description. May be SqlString.Null</LI>
		///		 <LI>Rate. May be SqlDecimal.Null</LI>
		///		 <LI>SampleOfMenu. May be SqlString.Null</LI>
		///		 <LI>SampleMenuFile. May be SqlBinary.Null</LI>
		///		 <LI>FoodPictureFile. May be SqlBinary.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTypeOfFood", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _typeOfFood));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDescription", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _description));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcRate", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _rate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSampleOfMenu", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _sampleOfMenu));
				cmdToExecute.Parameters.Add(new SqlParameter("@biSampleMenuFile", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _sampleMenuFile));
				cmdToExecute.Parameters.Add(new SqlParameter("@biFoodPictureFile", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _foodPictureFile));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>TypeOfFood. May be SqlString.Null</LI>
		///		 <LI>Description. May be SqlString.Null</LI>
		///		 <LI>Rate. May be SqlDecimal.Null</LI>
		///		 <LI>SampleOfMenu. May be SqlString.Null</LI>
		///		 <LI>SampleMenuFile. May be SqlBinary.Null</LI>
		///		 <LI>FoodPictureFile. May be SqlBinary.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTypeOfFood", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _typeOfFood));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDescription", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _description));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcRate", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _rate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSampleOfMenu", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _sampleOfMenu));
				cmdToExecute.Parameters.Add(new SqlParameter("@biSampleMenuFile", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _sampleMenuFile));
				cmdToExecute.Parameters.Add(new SqlParameter("@biFoodPictureFile", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _foodPictureFile));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'SupplierDetailID.
		/// This method will Update one or more existing rows in the database. It will reset the field 'SupplierDetailID' in
		/// all rows which have as value for this field the value as set in property 'SupplierDetailIDOld' to 
		/// the value as set in property 'SupplierDetailID'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		///		 <LI>SupplierDetailIDOld. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailIDOld", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailIDOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_UpdateAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::UpdateAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'SupplierDetailID'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_DeleteAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::DeleteAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>SupplierDetailID</LI>
		///		 <LI>TypeOfFood</LI>
		///		 <LI>Description</LI>
		///		 <LI>Rate</LI>
		///		 <LI>SampleOfMenu</LI>
		///		 <LI>SampleMenuFile</LI>
		///		 <LI>FoodPictureFile</LI>
		///		 <LI>Comments</LI>
		///		 <LI>ExtraField1</LI>
		///		 <LI>ExtraField2</LI>
		///		 <LI>ExtraField3</LI>
		///		 <LI>ExtraField4</LI>
		///		 <LI>ExtraField5</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierRestaurantRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_supplierDetailID = toReturn.Rows[0]["SupplierDetailID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierDetailID"];
					_typeOfFood = toReturn.Rows[0]["TypeOfFood"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["TypeOfFood"];
					_description = toReturn.Rows[0]["Description"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Description"];
					_rate = toReturn.Rows[0]["Rate"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Rate"];
					_sampleOfMenu = toReturn.Rows[0]["SampleOfMenu"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SampleOfMenu"];
					_sampleMenuFile = toReturn.Rows[0]["SampleMenuFile"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["SampleMenuFile"];
					_foodPictureFile = toReturn.Rows[0]["FoodPictureFile"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["FoodPictureFile"];
					_comments = toReturn.Rows[0]["Comments"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Comments"];
					_extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
					_extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
					_extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
					_extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
					_extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierRestaurantRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'SupplierDetailID'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>SupplierDetailID. May be SqlInt32.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWSupplierDetailIDLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("SupplierRestaurantRate");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierDetailID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierDetailID));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_SupplierRestaurantRate_SelectAllWSupplierDetailIDLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLSupplierRestaurantRate::SelectAllWSupplierDetailIDLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 SupplierDetailID
		{
			get
			{
				return _supplierDetailID;
			}
			set
			{
				_supplierDetailID = value;
			}
		}
		public SqlInt32 SupplierDetailIDOld
		{
			get
			{
				return _supplierDetailIDOld;
			}
			set
			{
				_supplierDetailIDOld = value;
			}
		}


		public SqlString TypeOfFood
		{
			get
			{
				return _typeOfFood;
			}
			set
			{
				_typeOfFood = value;
			}
		}


		public SqlString Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}


		public SqlDecimal Rate
		{
			get
			{
				return _rate;
			}
			set
			{
				_rate = value;
			}
		}


		public SqlString SampleOfMenu
		{
			get
			{
				return _sampleOfMenu;
			}
			set
			{
				_sampleOfMenu = value;
			}
		}


		public SqlBinary SampleMenuFile
		{
			get
			{
				return _sampleMenuFile;
			}
			set
			{
				_sampleMenuFile = value;
			}
		}


		public SqlBinary FoodPictureFile
		{
			get
			{
				return _foodPictureFile;
			}
			set
			{
				_foodPictureFile = value;
			}
		}


		public SqlString Comments
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}


		public SqlString ExtraField1
		{
			get
			{
				return _extraField1;
			}
			set
			{
				_extraField1 = value;
			}
		}


		public SqlString ExtraField2
		{
			get
			{
				return _extraField2;
			}
			set
			{
				_extraField2 = value;
			}
		}


		public SqlString ExtraField3
		{
			get
			{
				return _extraField3;
			}
			set
			{
				_extraField3 = value;
			}
		}


		public SqlString ExtraField4
		{
			get
			{
				return _extraField4;
			}
			set
			{
				_extraField4 = value;
			}
		}


		public SqlString ExtraField5
		{
			get
			{
				return _extraField5;
			}
			set
			{
				_extraField5 = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
