 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.InteropServices;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'Product'.
	/// </summary>
	public class DLProduct : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_creationDate, _createdDate, _updatedDate;
			private SqlDecimal		_commission, _netRate, _price;
			private SqlInt32		_iD, _noOfNights, _noOfDays, _companyID, _supplierId;
			private SqlString		_contactFaxNo, _contactMobileNo, _weChat, _skype, _whatsup, _contactPosition, _contactFirstName, _contactDepartment, _contactDirectPhone, _contactEmail, _facebook, _extraField4, _extraField3, _extraField5, _updatedBy, _createdBy, _twitter, _howToBook, _linkedIn, _extraField2, _extraField1, _supplierName, _contract, _country, _typeOfProduct, _region, _contractType, _emergencyNo, _contractWith, _reservationEmail, _currency, _alternationEmail, _priceIncluding, _howToSell, _priceExcluding, _contactSurName, _contactTitle, _contractedBy, _productTitle, _productType, _itineraryTemplate, _comment, _dataEntryBy;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLProduct()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>CompanyID</LI>
		///		 <LI>CreationDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractedBy. May be SqlString.Null</LI>
		///		 <LI>DataEntryBy. May be SqlString.Null</LI>
		///		 <LI>ProductType. May be SqlString.Null</LI>
		///		 <LI>SupplierId. May be SqlInt32.Null</LI>
		///		 <LI>SupplierName. May be SqlString.Null</LI>
		///		 <LI>Contract. May be SqlString.Null</LI>
		///		 <LI>ContractWith. May be SqlString.Null</LI>
		///		 <LI>EmergencyNo. May be SqlString.Null</LI>
		///		 <LI>ReservationEmail. May be SqlString.Null</LI>
		///		 <LI>AlternationEmail. May be SqlString.Null</LI>
		///		 <LI>Currency. May be SqlString.Null</LI>
		///		 <LI>ContractType. May be SqlString.Null</LI>
		///		 <LI>NetRate. May be SqlDecimal.Null</LI>
		///		 <LI>Commission. May be SqlDecimal.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>Region. May be SqlString.Null</LI>
		///		 <LI>TypeOfProduct. May be SqlString.Null</LI>
		///		 <LI>NoOfDays. May be SqlInt32.Null</LI>
		///		 <LI>NoOfNights. May be SqlInt32.Null</LI>
		///		 <LI>ProductTitle. May be SqlString.Null</LI>
		///		 <LI>ItineraryTemplate. May be SqlString.Null</LI>
		///		 <LI>Price. May be SqlDecimal.Null</LI>
		///		 <LI>Comment. May be SqlString.Null</LI>
		///		 <LI>HowToBook. May be SqlString.Null</LI>
		///		 <LI>HowToSell. May be SqlString.Null</LI>
		///		 <LI>PriceIncluding. May be SqlString.Null</LI>
		///		 <LI>PriceExcluding. May be SqlString.Null</LI>
		///		 <LI>ContactTitle. May be SqlString.Null</LI>
		///		 <LI>ContactSurName. May be SqlString.Null</LI>
		///		 <LI>ContactFirstName. May be SqlString.Null</LI>
		///		 <LI>ContactPosition. May be SqlString.Null</LI>
		///		 <LI>ContactDepartment. May be SqlString.Null</LI>
		///		 <LI>ContactEmail. May be SqlString.Null</LI>
		///		 <LI>ContactDirectPhone. May be SqlString.Null</LI>
		///		 <LI>ContactMobileNo. May be SqlString.Null</LI>
		///		 <LI>ContactFaxNo. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Whatsup. May be SqlString.Null</LI>
		///		 <LI>Skype. May be SqlString.Null</LI>
		///		 <LI>Facebook. May be SqlString.Null</LI>
		///		 <LI>Twitter. May be SqlString.Null</LI>
		///		 <LI>LinkedIn. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_Product_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreationDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _creationDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractedBy", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDataEntryBy", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dataEntryBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProductType", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _productType));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierId));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSupplierName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _supplierName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContract", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contract));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractWith", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractWith));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmergencyNo", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _emergencyNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sReservationEmail", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _reservationEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAlternationEmail", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _alternationEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCurrency", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _currency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractType", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractType));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRate", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _netRate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcCommission", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _commission));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRegion", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _region));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTypeOfProduct", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _typeOfProduct));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfDays", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfDays));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfNights", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfNights));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProductTitle", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _productTitle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sItineraryTemplate", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _itineraryTemplate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcPrice", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _price));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComment", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comment));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToBook", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToBook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToSell", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToSell));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPriceIncluding", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _priceIncluding));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPriceExcluding", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _priceExcluding));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactTitle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactSurName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactFirstName", SqlDbType.NVarChar, 510, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactFirstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactPosition", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactPosition));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactDepartment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactDepartment));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactDirectPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactDirectPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactMobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactFaxNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactFaxNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWhatsup", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _whatsup));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSkype", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _skype));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFacebook", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _facebook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTwitter", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _twitter));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLinkedIn", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _linkedIn));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_Product_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLProduct::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>CreationDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContractedBy. May be SqlString.Null</LI>
		///		 <LI>DataEntryBy. May be SqlString.Null</LI>
		///		 <LI>ProductType. May be SqlString.Null</LI>
		///		 <LI>SupplierId. May be SqlInt32.Null</LI>
		///		 <LI>SupplierName. May be SqlString.Null</LI>
		///		 <LI>Contract. May be SqlString.Null</LI>
		///		 <LI>ContractWith. May be SqlString.Null</LI>
		///		 <LI>EmergencyNo. May be SqlString.Null</LI>
		///		 <LI>ReservationEmail. May be SqlString.Null</LI>
		///		 <LI>AlternationEmail. May be SqlString.Null</LI>
		///		 <LI>Currency. May be SqlString.Null</LI>
		///		 <LI>ContractType. May be SqlString.Null</LI>
		///		 <LI>NetRate. May be SqlDecimal.Null</LI>
		///		 <LI>Commission. May be SqlDecimal.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>Region. May be SqlString.Null</LI>
		///		 <LI>TypeOfProduct. May be SqlString.Null</LI>
		///		 <LI>NoOfDays. May be SqlInt32.Null</LI>
		///		 <LI>NoOfNights. May be SqlInt32.Null</LI>
		///		 <LI>ProductTitle. May be SqlString.Null</LI>
		///		 <LI>ItineraryTemplate. May be SqlString.Null</LI>
		///		 <LI>Price. May be SqlDecimal.Null</LI>
		///		 <LI>Comment. May be SqlString.Null</LI>
		///		 <LI>HowToBook. May be SqlString.Null</LI>
		///		 <LI>HowToSell. May be SqlString.Null</LI>
		///		 <LI>PriceIncluding. May be SqlString.Null</LI>
		///		 <LI>PriceExcluding. May be SqlString.Null</LI>
		///		 <LI>ContactTitle. May be SqlString.Null</LI>
		///		 <LI>ContactSurName. May be SqlString.Null</LI>
		///		 <LI>ContactFirstName. May be SqlString.Null</LI>
		///		 <LI>ContactPosition. May be SqlString.Null</LI>
		///		 <LI>ContactDepartment. May be SqlString.Null</LI>
		///		 <LI>ContactEmail. May be SqlString.Null</LI>
		///		 <LI>ContactDirectPhone. May be SqlString.Null</LI>
		///		 <LI>ContactMobileNo. May be SqlString.Null</LI>
		///		 <LI>ContactFaxNo. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Whatsup. May be SqlString.Null</LI>
		///		 <LI>Skype. May be SqlString.Null</LI>
		///		 <LI>Facebook. May be SqlString.Null</LI>
		///		 <LI>Twitter. May be SqlString.Null</LI>
		///		 <LI>LinkedIn. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_Product_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreationDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _creationDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractedBy", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDataEntryBy", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dataEntryBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProductType", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _productType));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSupplierId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _supplierId));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSupplierName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _supplierName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContract", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contract));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractWith", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractWith));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmergencyNo", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _emergencyNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sReservationEmail", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _reservationEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAlternationEmail", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _alternationEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCurrency", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _currency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractType", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractType));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRate", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _netRate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcCommission", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _commission));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRegion", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _region));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTypeOfProduct", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _typeOfProduct));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfDays", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfDays));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfNights", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfNights));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProductTitle", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _productTitle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sItineraryTemplate", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _itineraryTemplate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcPrice", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _price));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComment", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comment));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToBook", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToBook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToSell", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToSell));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPriceIncluding", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _priceIncluding));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPriceExcluding", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _priceExcluding));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactTitle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactSurName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactFirstName", SqlDbType.NVarChar, 510, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactFirstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactPosition", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactPosition));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactDepartment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactDepartment));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactDirectPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactDirectPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactMobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactFaxNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactFaxNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWhatsup", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _whatsup));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSkype", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _skype));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFacebook", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _facebook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTwitter", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _twitter));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLinkedIn", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _linkedIn));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_Product_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLProduct::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_Product_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_Product_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLProduct::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>CreationDate</LI>
		///		 <LI>ContractedBy</LI>
		///		 <LI>DataEntryBy</LI>
		///		 <LI>ProductType</LI>
		///		 <LI>SupplierId</LI>
		///		 <LI>SupplierName</LI>
		///		 <LI>Contract</LI>
		///		 <LI>ContractWith</LI>
		///		 <LI>EmergencyNo</LI>
		///		 <LI>ReservationEmail</LI>
		///		 <LI>AlternationEmail</LI>
		///		 <LI>Currency</LI>
		///		 <LI>ContractType</LI>
		///		 <LI>NetRate</LI>
		///		 <LI>Commission</LI>
		///		 <LI>Country</LI>
		///		 <LI>Region</LI>
		///		 <LI>TypeOfProduct</LI>
		///		 <LI>NoOfDays</LI>
		///		 <LI>NoOfNights</LI>
		///		 <LI>ProductTitle</LI>
		///		 <LI>ItineraryTemplate</LI>
		///		 <LI>Price</LI>
		///		 <LI>Comment</LI>
		///		 <LI>HowToBook</LI>
		///		 <LI>HowToSell</LI>
		///		 <LI>PriceIncluding</LI>
		///		 <LI>PriceExcluding</LI>
		///		 <LI>ContactTitle</LI>
		///		 <LI>ContactSurName</LI>
		///		 <LI>ContactFirstName</LI>
		///		 <LI>ContactPosition</LI>
		///		 <LI>ContactDepartment</LI>
		///		 <LI>ContactEmail</LI>
		///		 <LI>ContactDirectPhone</LI>
		///		 <LI>ContactMobileNo</LI>
		///		 <LI>ContactFaxNo</LI>
		///		 <LI>WeChat</LI>
		///		 <LI>Whatsup</LI>
		///		 <LI>Skype</LI>
		///		 <LI>Facebook</LI>
		///		 <LI>Twitter</LI>
		///		 <LI>LinkedIn</LI>
		///		 <LI>ExtraField1</LI>
		///		 <LI>ExtraField2</LI>
		///		 <LI>ExtraField3</LI>
		///		 <LI>ExtraField4</LI>
		///		 <LI>ExtraField5</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_Product_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("Product");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_Product_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_companyID = (Int32)toReturn.Rows[0]["CompanyID"];
					_creationDate = toReturn.Rows[0]["CreationDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreationDate"];
					_contractedBy = toReturn.Rows[0]["ContractedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContractedBy"];
					_dataEntryBy = toReturn.Rows[0]["DataEntryBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["DataEntryBy"];
					_productType = toReturn.Rows[0]["ProductType"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ProductType"];
					_supplierId = toReturn.Rows[0]["SupplierId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SupplierId"];
					_supplierName = toReturn.Rows[0]["SupplierName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SupplierName"];
					_contract = toReturn.Rows[0]["Contract"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Contract"];
					_contractWith = toReturn.Rows[0]["ContractWith"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContractWith"];
					_emergencyNo = toReturn.Rows[0]["EmergencyNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["EmergencyNo"];
					_reservationEmail = toReturn.Rows[0]["ReservationEmail"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ReservationEmail"];
					_alternationEmail = toReturn.Rows[0]["AlternationEmail"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["AlternationEmail"];
					_currency = toReturn.Rows[0]["Currency"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Currency"];
					_contractType = toReturn.Rows[0]["ContractType"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContractType"];
					_netRate = toReturn.Rows[0]["NetRate"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["NetRate"];
					_commission = toReturn.Rows[0]["Commission"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Commission"];
					_country = toReturn.Rows[0]["Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Country"];
					_region = toReturn.Rows[0]["Region"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Region"];
					_typeOfProduct = toReturn.Rows[0]["TypeOfProduct"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["TypeOfProduct"];
					_noOfDays = toReturn.Rows[0]["NoOfDays"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfDays"];
					_noOfNights = toReturn.Rows[0]["NoOfNights"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfNights"];
					_productTitle = toReturn.Rows[0]["ProductTitle"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ProductTitle"];
					_itineraryTemplate = toReturn.Rows[0]["ItineraryTemplate"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ItineraryTemplate"];
					_price = toReturn.Rows[0]["Price"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Price"];
					_comment = toReturn.Rows[0]["Comment"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Comment"];
					_howToBook = toReturn.Rows[0]["HowToBook"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HowToBook"];
					_howToSell = toReturn.Rows[0]["HowToSell"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HowToSell"];
					_priceIncluding = toReturn.Rows[0]["PriceIncluding"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PriceIncluding"];
					_priceExcluding = toReturn.Rows[0]["PriceExcluding"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PriceExcluding"];
					_contactTitle = toReturn.Rows[0]["ContactTitle"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactTitle"];
					_contactSurName = toReturn.Rows[0]["ContactSurName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactSurName"];
					_contactFirstName = toReturn.Rows[0]["ContactFirstName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactFirstName"];
					_contactPosition = toReturn.Rows[0]["ContactPosition"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactPosition"];
					_contactDepartment = toReturn.Rows[0]["ContactDepartment"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactDepartment"];
					_contactEmail = toReturn.Rows[0]["ContactEmail"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactEmail"];
					_contactDirectPhone = toReturn.Rows[0]["ContactDirectPhone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactDirectPhone"];
					_contactMobileNo = toReturn.Rows[0]["ContactMobileNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactMobileNo"];
					_contactFaxNo = toReturn.Rows[0]["ContactFaxNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactFaxNo"];
					_weChat = toReturn.Rows[0]["WeChat"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["WeChat"];
					_whatsup = toReturn.Rows[0]["Whatsup"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Whatsup"];
					_skype = toReturn.Rows[0]["Skype"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Skype"];
					_facebook = toReturn.Rows[0]["Facebook"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Facebook"];
					_twitter = toReturn.Rows[0]["Twitter"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Twitter"];
					_linkedIn = toReturn.Rows[0]["LinkedIn"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["LinkedIn"];
					_extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
					_extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
					_extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
					_extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
					_extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLProduct::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_Product_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("Product");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_Product_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLProduct::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 CompanyID
		{
			get
			{
				return _companyID;
			}
			set
			{
				SqlInt32 companyIDTmp = (SqlInt32)value;
				if(companyIDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("CompanyID", "CompanyID can't be NULL");
				}
				_companyID = value;
			}
		}


		public SqlDateTime CreationDate
		{
			get
			{
				return _creationDate;
			}
			set
			{
				_creationDate = value;
			}
		}


		public SqlString ContractedBy
		{
			get
			{
				return _contractedBy;
			}
			set
			{
				_contractedBy = value;
			}
		}


		public SqlString DataEntryBy
		{
			get
			{
				return _dataEntryBy;
			}
			set
			{
				_dataEntryBy = value;
			}
		}


		public SqlString ProductType
		{
			get
			{
				return _productType;
			}
			set
			{
				_productType = value;
			}
		}


		public SqlInt32 SupplierId
		{
			get
			{
				return _supplierId;
			}
			set
			{
				_supplierId = value;
			}
		}


		public SqlString SupplierName
		{
			get
			{
				return _supplierName;
			}
			set
			{
				_supplierName = value;
			}
		}


		public SqlString Contract
		{
			get
			{
				return _contract;
			}
			set
			{
				_contract = value;
			}
		}


		public SqlString ContractWith
		{
			get
			{
				return _contractWith;
			}
			set
			{
				_contractWith = value;
			}
		}


		public SqlString EmergencyNo
		{
			get
			{
				return _emergencyNo;
			}
			set
			{
				_emergencyNo = value;
			}
		}


		public SqlString ReservationEmail
		{
			get
			{
				return _reservationEmail;
			}
			set
			{
				_reservationEmail = value;
			}
		}


		public SqlString AlternationEmail
		{
			get
			{
				return _alternationEmail;
			}
			set
			{
				_alternationEmail = value;
			}
		}


		public SqlString Currency
		{
			get
			{
				return _currency;
			}
			set
			{
				_currency = value;
			}
		}


		public SqlString ContractType
		{
			get
			{
				return _contractType;
			}
			set
			{
				_contractType = value;
			}
		}


		public SqlDecimal NetRate
		{
			get
			{
				return _netRate;
			}
			set
			{
				_netRate = value;
			}
		}


		public SqlDecimal Commission
		{
			get
			{
				return _commission;
			}
			set
			{
				_commission = value;
			}
		}


		public SqlString Country
		{
			get
			{
				return _country;
			}
			set
			{
				_country = value;
			}
		}


		public SqlString Region
		{
			get
			{
				return _region;
			}
			set
			{
				_region = value;
			}
		}


		public SqlString TypeOfProduct
		{
			get
			{
				return _typeOfProduct;
			}
			set
			{
				_typeOfProduct = value;
			}
		}


		public SqlInt32 NoOfDays
		{
			get
			{
				return _noOfDays;
			}
			set
			{
				_noOfDays = value;
			}
		}


		public SqlInt32 NoOfNights
		{
			get
			{
				return _noOfNights;
			}
			set
			{
				_noOfNights = value;
			}
		}


		public SqlString ProductTitle
		{
			get
			{
				return _productTitle;
			}
			set
			{
				_productTitle = value;
			}
		}


		public SqlString ItineraryTemplate
		{
			get
			{
				return _itineraryTemplate;
			}
			set
			{
				_itineraryTemplate = value;
			}
		}


		public SqlDecimal Price
		{
			get
			{
				return _price;
			}
			set
			{
				_price = value;
			}
		}


		public SqlString Comment
		{
			get
			{
				return _comment;
			}
			set
			{
				_comment = value;
			}
		}


		public SqlString HowToBook
		{
			get
			{
				return _howToBook;
			}
			set
			{
				_howToBook = value;
			}
		}


		public SqlString HowToSell
		{
			get
			{
				return _howToSell;
			}
			set
			{
				_howToSell = value;
			}
		}


		public SqlString PriceIncluding
		{
			get
			{
				return _priceIncluding;
			}
			set
			{
				_priceIncluding = value;
			}
		}


		public SqlString PriceExcluding
		{
			get
			{
				return _priceExcluding;
			}
			set
			{
				_priceExcluding = value;
			}
		}


		public SqlString ContactTitle
		{
			get
			{
				return _contactTitle;
			}
			set
			{
				_contactTitle = value;
			}
		}


		public SqlString ContactSurName
		{
			get
			{
				return _contactSurName;
			}
			set
			{
				_contactSurName = value;
			}
		}


		public SqlString ContactFirstName
		{
			get
			{
				return _contactFirstName;
			}
			set
			{
				_contactFirstName = value;
			}
		}


		public SqlString ContactPosition
		{
			get
			{
				return _contactPosition;
			}
			set
			{
				_contactPosition = value;
			}
		}


		public SqlString ContactDepartment
		{
			get
			{
				return _contactDepartment;
			}
			set
			{
				_contactDepartment = value;
			}
		}


		public SqlString ContactEmail
		{
			get
			{
				return _contactEmail;
			}
			set
			{
				_contactEmail = value;
			}
		}


		public SqlString ContactDirectPhone
		{
			get
			{
				return _contactDirectPhone;
			}
			set
			{
				_contactDirectPhone = value;
			}
		}


		public SqlString ContactMobileNo
		{
			get
			{
				return _contactMobileNo;
			}
			set
			{
				_contactMobileNo = value;
			}
		}


		public SqlString ContactFaxNo
		{
			get
			{
				return _contactFaxNo;
			}
			set
			{
				_contactFaxNo = value;
			}
		}


		public SqlString WeChat
		{
			get
			{
				return _weChat;
			}
			set
			{
				_weChat = value;
			}
		}


		public SqlString Whatsup
		{
			get
			{
				return _whatsup;
			}
			set
			{
				_whatsup = value;
			}
		}


		public SqlString Skype
		{
			get
			{
				return _skype;
			}
			set
			{
				_skype = value;
			}
		}


		public SqlString Facebook
		{
			get
			{
				return _facebook;
			}
			set
			{
				_facebook = value;
			}
		}


		public SqlString Twitter
		{
			get
			{
				return _twitter;
			}
			set
			{
				_twitter = value;
			}
		}


		public SqlString LinkedIn
		{
			get
			{
				return _linkedIn;
			}
			set
			{
				_linkedIn = value;
			}
		}


		public SqlString ExtraField1
		{
			get
			{
				return _extraField1;
			}
			set
			{
				_extraField1 = value;
			}
		}


		public SqlString ExtraField2
		{
			get
			{
				return _extraField2;
			}
			set
			{
				_extraField2 = value;
			}
		}


		public SqlString ExtraField3
		{
			get
			{
				return _extraField3;
			}
			set
			{
				_extraField3 = value;
			}
		}


		public SqlString ExtraField4
		{
			get
			{
				return _extraField4;
			}
			set
			{
				_extraField4 = value;
			}
		}


		public SqlString ExtraField5
		{
			get
			{
				return _extraField5;
			}
			set
			{
				_extraField5 = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
