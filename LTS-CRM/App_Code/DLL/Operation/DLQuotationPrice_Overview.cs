
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.InteropServices;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'QuotationPrice_Overview'.
	/// </summary>
	public class DLQuotationPrice_Overview : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_createdDate, _updatedDate;
			private SqlDecimal		_profitPercentage, _profit, _singleRoomSupplementPerPerson, _grossRatePerPerson2, _markUpPercentage2, _netRateInTotal, _netRatePerPerson, _markUpPercentage, _grossRateInTotal, _grossRatePerPerson;
			private SqlInt32		_quotationPriceId, _quotationPriceIdOld, _iD;
			private SqlString		_remark, _updatedBy, _quotationCurrency, _confirmationStatus, _createdBy;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLQuotationPrice_Overview()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceId</LI>
		///		 <LI>NetRatePerPerson. May be SqlDecimal.Null</LI>
		///		 <LI>NetRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>MarkUpPercentage. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRatePerPerson. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>Profit. May be SqlDecimal.Null</LI>
		///		 <LI>ProfitPercentage. May be SqlDecimal.Null</LI>
		///		 <LI>SingleRoomSupplementPerPerson. May be SqlDecimal.Null</LI>
		///		 <LI>MarkUpPercentage2. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRatePerPerson2. May be SqlDecimal.Null</LI>
		///		 <LI>Remark. May be SqlString.Null</LI>
		///		 <LI>QuotationCurrency. May be SqlString.Null</LI>
		///		 <LI>ConfirmationStatus. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceId));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRatePerPerson", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _netRatePerPerson));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _netRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcMarkUpPercentage", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _markUpPercentage));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRatePerPerson", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRatePerPerson));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProfit", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _profit));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProfitPercentage", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _profitPercentage));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcSingleRoomSupplementPerPerson", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _singleRoomSupplementPerPerson));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcMarkUpPercentage2", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _markUpPercentage2));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRatePerPerson2", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRatePerPerson2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemark", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remark));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationCurrency", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationCurrency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sConfirmationStatus", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _confirmationStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>QuotationPriceId</LI>
		///		 <LI>NetRatePerPerson. May be SqlDecimal.Null</LI>
		///		 <LI>NetRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>MarkUpPercentage. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRatePerPerson. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>Profit. May be SqlDecimal.Null</LI>
		///		 <LI>ProfitPercentage. May be SqlDecimal.Null</LI>
		///		 <LI>SingleRoomSupplementPerPerson. May be SqlDecimal.Null</LI>
		///		 <LI>MarkUpPercentage2. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRatePerPerson2. May be SqlDecimal.Null</LI>
		///		 <LI>Remark. May be SqlString.Null</LI>
		///		 <LI>QuotationCurrency. May be SqlString.Null</LI>
		///		 <LI>ConfirmationStatus. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceId));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRatePerPerson", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _netRatePerPerson));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _netRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcMarkUpPercentage", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _markUpPercentage));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRatePerPerson", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRatePerPerson));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProfit", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _profit));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProfitPercentage", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _profitPercentage));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcSingleRoomSupplementPerPerson", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _singleRoomSupplementPerPerson));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcMarkUpPercentage2", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _markUpPercentage2));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRatePerPerson2", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRatePerPerson2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemark", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remark));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationCurrency", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationCurrency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sConfirmationStatus", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _confirmationStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'QuotationPriceId.
		/// This method will Update one or more existing rows in the database. It will reset the field 'QuotationPriceId' in
		/// all rows which have as value for this field the value as set in property 'QuotationPriceIdOld' to 
		/// the value as set in property 'QuotationPriceId'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceId</LI>
		///		 <LI>QuotationPriceIdOld</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWQuotationPriceIdLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceIdOld", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceIdOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_UpdateAllWQuotationPriceIdLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::UpdateAllWQuotationPriceIdLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'QuotationPriceId'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceId</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWQuotationPriceIdLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_DeleteAllWQuotationPriceIdLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::DeleteAllWQuotationPriceIdLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>QuotationPriceId</LI>
		///		 <LI>NetRatePerPerson</LI>
		///		 <LI>NetRateInTotal</LI>
		///		 <LI>MarkUpPercentage</LI>
		///		 <LI>GrossRatePerPerson</LI>
		///		 <LI>GrossRateInTotal</LI>
		///		 <LI>Profit</LI>
		///		 <LI>ProfitPercentage</LI>
		///		 <LI>SingleRoomSupplementPerPerson</LI>
		///		 <LI>MarkUpPercentage2</LI>
		///		 <LI>GrossRatePerPerson2</LI>
		///		 <LI>Remark</LI>
		///		 <LI>QuotationCurrency</LI>
		///		 <LI>ConfirmationStatus</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("QuotationPrice_Overview");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_quotationPriceId = (Int32)toReturn.Rows[0]["QuotationPriceId"];
					_netRatePerPerson = toReturn.Rows[0]["NetRatePerPerson"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["NetRatePerPerson"];
					_netRateInTotal = toReturn.Rows[0]["NetRateInTotal"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["NetRateInTotal"];
					_markUpPercentage = toReturn.Rows[0]["MarkUpPercentage"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["MarkUpPercentage"];
					_grossRatePerPerson = toReturn.Rows[0]["GrossRatePerPerson"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["GrossRatePerPerson"];
					_grossRateInTotal = toReturn.Rows[0]["GrossRateInTotal"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["GrossRateInTotal"];
					_profit = toReturn.Rows[0]["Profit"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Profit"];
					_profitPercentage = toReturn.Rows[0]["ProfitPercentage"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["ProfitPercentage"];
					_singleRoomSupplementPerPerson = toReturn.Rows[0]["SingleRoomSupplementPerPerson"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["SingleRoomSupplementPerPerson"];
					_markUpPercentage2 = toReturn.Rows[0]["MarkUpPercentage2"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["MarkUpPercentage2"];
					_grossRatePerPerson2 = toReturn.Rows[0]["GrossRatePerPerson2"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["GrossRatePerPerson2"];
					_remark = toReturn.Rows[0]["Remark"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Remark"];
					_quotationCurrency = toReturn.Rows[0]["QuotationCurrency"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["QuotationCurrency"];
					_confirmationStatus = toReturn.Rows[0]["ConfirmationStatus"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ConfirmationStatus"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("QuotationPrice_Overview");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'QuotationPriceId'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceId</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWQuotationPriceIdLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("QuotationPrice_Overview");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Overview::SelectAllWQuotationPriceIdLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}

        public DataSet SelectAllTotal()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Overview_Final]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataSet toReturn = new DataSet("QuotationPrice_Overview");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceId));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_QuotationPrice_Overview_SelectAllWQuotationPriceIdLogic' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLQuotationPrice_Overview::SelectAllWQuotationPriceIdLogic::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 QuotationPriceId
		{
			get
			{
				return _quotationPriceId;
			}
			set
			{
				SqlInt32 quotationPriceIdTmp = (SqlInt32)value;
				if(quotationPriceIdTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("QuotationPriceId", "QuotationPriceId can't be NULL");
				}
				_quotationPriceId = value;
			}
		}
		public SqlInt32 QuotationPriceIdOld
		{
			get
			{
				return _quotationPriceIdOld;
			}
			set
			{
				SqlInt32 quotationPriceIdOldTmp = (SqlInt32)value;
				if(quotationPriceIdOldTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("QuotationPriceIdOld", "QuotationPriceIdOld can't be NULL");
				}
				_quotationPriceIdOld = value;
			}
		}


		public SqlDecimal NetRatePerPerson
		{
			get
			{
				return _netRatePerPerson;
			}
			set
			{
				_netRatePerPerson = value;
			}
		}


		public SqlDecimal NetRateInTotal
		{
			get
			{
				return _netRateInTotal;
			}
			set
			{
				_netRateInTotal = value;
			}
		}


		public SqlDecimal MarkUpPercentage
		{
			get
			{
				return _markUpPercentage;
			}
			set
			{
				_markUpPercentage = value;
			}
		}


		public SqlDecimal GrossRatePerPerson
		{
			get
			{
				return _grossRatePerPerson;
			}
			set
			{
				_grossRatePerPerson = value;
			}
		}


		public SqlDecimal GrossRateInTotal
		{
			get
			{
				return _grossRateInTotal;
			}
			set
			{
				_grossRateInTotal = value;
			}
		}


		public SqlDecimal Profit
		{
			get
			{
				return _profit;
			}
			set
			{
				_profit = value;
			}
		}


		public SqlDecimal ProfitPercentage
		{
			get
			{
				return _profitPercentage;
			}
			set
			{
				_profitPercentage = value;
			}
		}


		public SqlDecimal SingleRoomSupplementPerPerson
		{
			get
			{
				return _singleRoomSupplementPerPerson;
			}
			set
			{
				_singleRoomSupplementPerPerson = value;
			}
		}


		public SqlDecimal MarkUpPercentage2
		{
			get
			{
				return _markUpPercentage2;
			}
			set
			{
				_markUpPercentage2 = value;
			}
		}


		public SqlDecimal GrossRatePerPerson2
		{
			get
			{
				return _grossRatePerPerson2;
			}
			set
			{
				_grossRatePerPerson2 = value;
			}
		}


		public SqlString Remark
		{
			get
			{
				return _remark;
			}
			set
			{
				_remark = value;
			}
		}


		public SqlString QuotationCurrency
		{
			get
			{
				return _quotationCurrency;
			}
			set
			{
				_quotationCurrency = value;
			}
		}


		public SqlString ConfirmationStatus
		{
			get
			{
				return _confirmationStatus;
			}
			set
			{
				_confirmationStatus = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
