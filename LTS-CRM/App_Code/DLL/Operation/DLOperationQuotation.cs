using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.InteropServices;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'OperationQuotation'.
	/// </summary>
	public class DLOperationQuotation : DLDBInteractionBase
	{
		#region Class Member Declarations
        private SqlDateTime _quotationDate, _tourStartDate, _tourEndDate, _createdDate, _updatedDate;
        private SqlInt32 _salesId, _departmentID, _iD, _noOfServiceDay, _noOfChild2To5, _noOfInfant1To2, _noOfChild5To12, _totalClient, _noOfAdults, _companyID, _staffId, _inquiryStaffId, _companyIndividual;
        private SqlString _comments, _tourCode, _client_City, _createdBy, _updatedBy, _office_Telephone, _messageToClient, _quotationStatus, _quotationReport, _office_City, _office_Country, _client_Fax, _client_Email, _quotationStaff, _client_WeChat, _office_MobileNumber, _client_MobileNumber, _client_Telephone, _client_Country, _office_Fax, _office_WeChat, _office_Email;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLOperationQuotation()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
        ///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>TourCode</LI>
		///		 <LI>QuotationDate</LI>
		///		 <LI>StaffId. May be SqlInt32.Null</LI>
		///		 <LI>InquiryStaffId. May be SqlInt32.Null</LI>
		///		 <LI>CompanyIndividual. May be SqlInt32.Null</LI>
		///		 <LI>Client_City. May be SqlString.Null</LI>
		///		 <LI>Client_Country. May be SqlString.Null</LI>
		///		 <LI>Client_Telephone. May be SqlString.Null</LI>
		///		 <LI>Client_MobileNumber. May be SqlString.Null</LI>
		///		 <LI>Client_Fax. May be SqlString.Null</LI>
		///		 <LI>Client_WeChat. May be SqlString.Null</LI>
		///		 <LI>Client_Email. May be SqlString.Null</LI>
		///		 <LI>SalesId. May be SqlInt32.Null</LI>
		///		 <LI>QuotationStaff. May be SqlString.Null</LI>
		///		 <LI>DepartmentID. May be SqlInt32.Null</LI>
		///		 <LI>Office_City. May be SqlString.Null</LI>
		///		 <LI>Office_Country. May be SqlString.Null</LI>
		///		 <LI>Office_Telephone. May be SqlString.Null</LI>
		///		 <LI>Office_MobileNumber. May be SqlString.Null</LI>
		///		 <LI>Office_Fax. May be SqlString.Null</LI>
		///		 <LI>Office_WeChat. May be SqlString.Null</LI>
		///		 <LI>Office_Email. May be SqlString.Null</LI>
		///		 <LI>TourStartDate. May be SqlDateTime.Null</LI>
		///		 <LI>TourEndDate. May be SqlDateTime.Null</LI>
		///		 <LI>NoOfServiceDay. May be SqlInt32.Null</LI>
		///		 <LI>TotalClient. May be SqlInt32.Null</LI>
		///		 <LI>NoOfAdults. May be SqlInt32.Null</LI>
		///		 <LI>NoOfChild5To12. May be SqlInt32.Null</LI>
		///		 <LI>NoOfChild2To5. May be SqlInt32.Null</LI>
		///		 <LI>NoOfInfant1To2. May be SqlInt32.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>MessageToClient. May be SqlString.Null</LI>
		///		 <LI>QuotationStatus. May be SqlString.Null</LI>
		///		 <LI>QuotationReport. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_OperationQuotation_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
                cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTourCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _tourCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@daQuotationDate", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _quotationDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iStaffId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _staffId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iInquiryStaffId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _inquiryStaffId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyIndividual", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyIndividual));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_City", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_City));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Country", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Telephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Telephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_MobileNumber", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_MobileNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Fax", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Fax));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_WeChat", SqlDbType.NVarChar, 25, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_WeChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Email", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Email));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSalesId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _salesId));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationStaff", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationStaff));
				cmdToExecute.Parameters.Add(new SqlParameter("@iDepartmentID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _departmentID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_City", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_City));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Country", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Telephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Telephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_MobileNumber", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_MobileNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Fax", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Fax));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_WeChat", SqlDbType.NVarChar, 25, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_WeChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Email", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Email));
				cmdToExecute.Parameters.Add(new SqlParameter("@daTourStartDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourStartDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daTourEndDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourEndDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfServiceDay", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfServiceDay));
				cmdToExecute.Parameters.Add(new SqlParameter("@iTotalClient", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _totalClient));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfAdults", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfAdults));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfChild5To12", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfChild5To12));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfChild2To5", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfChild2To5));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfInfant1To2", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfInfant1To2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMessageToClient", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _messageToClient));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationStatus", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationReport", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationReport));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_OperationQuotation_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLOperationQuotation::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
        ///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>TourCode</LI>
		///		 <LI>QuotationDate</LI>
		///		 <LI>StaffId. May be SqlInt32.Null</LI>
		///		 <LI>InquiryStaffId. May be SqlInt32.Null</LI>
		///		 <LI>CompanyIndividual. May be SqlInt32.Null</LI>
		///		 <LI>Client_City. May be SqlString.Null</LI>
		///		 <LI>Client_Country. May be SqlString.Null</LI>
		///		 <LI>Client_Telephone. May be SqlString.Null</LI>
		///		 <LI>Client_MobileNumber. May be SqlString.Null</LI>
		///		 <LI>Client_Fax. May be SqlString.Null</LI>
		///		 <LI>Client_WeChat. May be SqlString.Null</LI>
		///		 <LI>Client_Email. May be SqlString.Null</LI>
		///		 <LI>SalesId. May be SqlInt32.Null</LI>
		///		 <LI>QuotationStaff. May be SqlString.Null</LI>
		///		 <LI>DepartmentID. May be SqlInt32.Null</LI>
		///		 <LI>Office_City. May be SqlString.Null</LI>
		///		 <LI>Office_Country. May be SqlString.Null</LI>
		///		 <LI>Office_Telephone. May be SqlString.Null</LI>
		///		 <LI>Office_MobileNumber. May be SqlString.Null</LI>
		///		 <LI>Office_Fax. May be SqlString.Null</LI>
		///		 <LI>Office_WeChat. May be SqlString.Null</LI>
		///		 <LI>Office_Email. May be SqlString.Null</LI>
		///		 <LI>TourStartDate. May be SqlDateTime.Null</LI>
		///		 <LI>TourEndDate. May be SqlDateTime.Null</LI>
		///		 <LI>NoOfServiceDay. May be SqlInt32.Null</LI>
		///		 <LI>TotalClient. May be SqlInt32.Null</LI>
		///		 <LI>NoOfAdults. May be SqlInt32.Null</LI>
		///		 <LI>NoOfChild5To12. May be SqlInt32.Null</LI>
		///		 <LI>NoOfChild2To5. May be SqlInt32.Null</LI>
		///		 <LI>NoOfInfant1To2. May be SqlInt32.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>MessageToClient. May be SqlString.Null</LI>
		///		 <LI>QuotationStatus. May be SqlString.Null</LI>
		///		 <LI>QuotationReport. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_OperationQuotation_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
                cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTourCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _tourCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@daQuotationDate", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _quotationDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iStaffId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _staffId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iInquiryStaffId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _inquiryStaffId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyIndividual", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyIndividual));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_City", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_City));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Country", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Telephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Telephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_MobileNumber", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_MobileNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Fax", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Fax));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_WeChat", SqlDbType.NVarChar, 25, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_WeChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClient_Email", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _client_Email));
				cmdToExecute.Parameters.Add(new SqlParameter("@iSalesId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _salesId));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationStaff", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationStaff));
				cmdToExecute.Parameters.Add(new SqlParameter("@iDepartmentID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _departmentID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_City", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_City));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Country", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Telephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Telephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_MobileNumber", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_MobileNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Fax", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Fax));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_WeChat", SqlDbType.NVarChar, 25, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_WeChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sOffice_Email", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _office_Email));
				cmdToExecute.Parameters.Add(new SqlParameter("@daTourStartDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourStartDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daTourEndDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourEndDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfServiceDay", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfServiceDay));
				cmdToExecute.Parameters.Add(new SqlParameter("@iTotalClient", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _totalClient));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfAdults", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfAdults));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfChild5To12", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfChild5To12));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfChild2To5", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfChild2To5));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfInfant1To2", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfInfant1To2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMessageToClient", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _messageToClient));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationStatus", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationReport", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationReport));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_OperationQuotation_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLOperationQuotation::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_OperationQuotation_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_OperationQuotation_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLOperationQuotation::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
        ///		 <LI>CompanyID</LI>
		///		 <LI>TourCode</LI>
		///		 <LI>QuotationDate</LI>
		///		 <LI>StaffId</LI>
		///		 <LI>InquiryStaffId</LI>
		///		 <LI>CompanyIndividual</LI>
		///		 <LI>Client_City</LI>
		///		 <LI>Client_Country</LI>
		///		 <LI>Client_Telephone</LI>
		///		 <LI>Client_MobileNumber</LI>
		///		 <LI>Client_Fax</LI>
		///		 <LI>Client_WeChat</LI>
		///		 <LI>Client_Email</LI>
		///		 <LI>SalesId</LI>
		///		 <LI>QuotationStaff</LI>
		///		 <LI>DepartmentID</LI>
		///		 <LI>Office_City</LI>
		///		 <LI>Office_Country</LI>
		///		 <LI>Office_Telephone</LI>
		///		 <LI>Office_MobileNumber</LI>
		///		 <LI>Office_Fax</LI>
		///		 <LI>Office_WeChat</LI>
		///		 <LI>Office_Email</LI>
		///		 <LI>TourStartDate</LI>
		///		 <LI>TourEndDate</LI>
		///		 <LI>NoOfServiceDay</LI>
		///		 <LI>TotalClient</LI>
		///		 <LI>NoOfAdults</LI>
		///		 <LI>NoOfChild5To12</LI>
		///		 <LI>NoOfChild2To5</LI>
		///		 <LI>NoOfInfant1To2</LI>
		///		 <LI>Comments</LI>
		///		 <LI>MessageToClient</LI>
		///		 <LI>QuotationStatus</LI>
		///		 <LI>QuotationReport</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_OperationQuotation_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("OperationQuotation");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_OperationQuotation_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
                    _companyID = toReturn.Rows[0]["CompanyID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["CompanyID"];
					_tourCode = (string)toReturn.Rows[0]["TourCode"];
					_quotationDate = (DateTime)toReturn.Rows[0]["QuotationDate"];
					_staffId = toReturn.Rows[0]["StaffId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["StaffId"];
					_inquiryStaffId = toReturn.Rows[0]["InquiryStaffId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["InquiryStaffId"];
					_companyIndividual = toReturn.Rows[0]["CompanyIndividual"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["CompanyIndividual"];
					_client_City = toReturn.Rows[0]["Client_City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Client_City"];
					_client_Country = toReturn.Rows[0]["Client_Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Client_Country"];
					_client_Telephone = toReturn.Rows[0]["Client_Telephone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Client_Telephone"];
					_client_MobileNumber = toReturn.Rows[0]["Client_MobileNumber"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Client_MobileNumber"];
					_client_Fax = toReturn.Rows[0]["Client_Fax"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Client_Fax"];
					_client_WeChat = toReturn.Rows[0]["Client_WeChat"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Client_WeChat"];
					_client_Email = toReturn.Rows[0]["Client_Email"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Client_Email"];
					_salesId = toReturn.Rows[0]["SalesId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["SalesId"];
					_quotationStaff = toReturn.Rows[0]["QuotationStaff"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["QuotationStaff"];
					_departmentID = toReturn.Rows[0]["DepartmentID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["DepartmentID"];
					_office_City = toReturn.Rows[0]["Office_City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Office_City"];
					_office_Country = toReturn.Rows[0]["Office_Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Office_Country"];
					_office_Telephone = toReturn.Rows[0]["Office_Telephone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Office_Telephone"];
					_office_MobileNumber = toReturn.Rows[0]["Office_MobileNumber"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Office_MobileNumber"];
					_office_Fax = toReturn.Rows[0]["Office_Fax"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Office_Fax"];
					_office_WeChat = toReturn.Rows[0]["Office_WeChat"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Office_WeChat"];
					_office_Email = toReturn.Rows[0]["Office_Email"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Office_Email"];
					_tourStartDate = toReturn.Rows[0]["TourStartDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["TourStartDate"];
					_tourEndDate = toReturn.Rows[0]["TourEndDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["TourEndDate"];
					_noOfServiceDay = toReturn.Rows[0]["NoOfServiceDay"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfServiceDay"];
					_totalClient = toReturn.Rows[0]["TotalClient"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["TotalClient"];
					_noOfAdults = toReturn.Rows[0]["NoOfAdults"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfAdults"];
					_noOfChild5To12 = toReturn.Rows[0]["NoOfChild5To12"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfChild5To12"];
					_noOfChild2To5 = toReturn.Rows[0]["NoOfChild2To5"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfChild2To5"];
					_noOfInfant1To2 = toReturn.Rows[0]["NoOfInfant1To2"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfInfant1To2"];
					_comments = toReturn.Rows[0]["Comments"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Comments"];
					_messageToClient = toReturn.Rows[0]["MessageToClient"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["MessageToClient"];
					_quotationStatus = toReturn.Rows[0]["QuotationStatus"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["QuotationStatus"];
					_quotationReport = toReturn.Rows[0]["QuotationReport"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["QuotationReport"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLOperationQuotation::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}

        public DataTable SelectBookingDetails()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_OperationQuotation_GetBookingDetailsById]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OperationQuotation");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_OperationQuotation_SelectOne' reported the ErrorCode: " + _errorCode);
                }
                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLOperationQuotation::SelectOne::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_OperationQuotation_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("OperationQuotation");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_OperationQuotation_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLOperationQuotation::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


        public SqlInt32 CompanyID
        {
            get
            {
                return _companyID;
            }
            set
            {
                _companyID = value;
            }
        }


		public SqlString TourCode
		{
			get
			{
				return _tourCode;
			}
			set
			{
				SqlString tourCodeTmp = (SqlString)value;
				if(tourCodeTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("TourCode", "TourCode can't be NULL");
				}
				_tourCode = value;
			}
		}


		public SqlDateTime QuotationDate
		{
			get
			{
				return _quotationDate;
			}
			set
			{
				SqlDateTime quotationDateTmp = (SqlDateTime)value;
				if(quotationDateTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("QuotationDate", "QuotationDate can't be NULL");
				}
				_quotationDate = value;
			}
		}


		public SqlInt32 StaffId
		{
			get
			{
				return _staffId;
			}
			set
			{
				_staffId = value;
			}
		}


		public SqlInt32 InquiryStaffId
		{
			get
			{
				return _inquiryStaffId;
			}
			set
			{
				_inquiryStaffId = value;
			}
		}


		public SqlInt32 CompanyIndividual
		{
			get
			{
				return _companyIndividual;
			}
			set
			{
				_companyIndividual = value;
			}
		}


		public SqlString Client_City
		{
			get
			{
				return _client_City;
			}
			set
			{
				_client_City = value;
			}
		}


		public SqlString Client_Country
		{
			get
			{
				return _client_Country;
			}
			set
			{
				_client_Country = value;
			}
		}


		public SqlString Client_Telephone
		{
			get
			{
				return _client_Telephone;
			}
			set
			{
				_client_Telephone = value;
			}
		}


		public SqlString Client_MobileNumber
		{
			get
			{
				return _client_MobileNumber;
			}
			set
			{
				_client_MobileNumber = value;
			}
		}


		public SqlString Client_Fax
		{
			get
			{
				return _client_Fax;
			}
			set
			{
				_client_Fax = value;
			}
		}


		public SqlString Client_WeChat
		{
			get
			{
				return _client_WeChat;
			}
			set
			{
				_client_WeChat = value;
			}
		}


		public SqlString Client_Email
		{
			get
			{
				return _client_Email;
			}
			set
			{
				_client_Email = value;
			}
		}


		public SqlInt32 SalesId
		{
			get
			{
				return _salesId;
			}
			set
			{
				_salesId = value;
			}
		}


		public SqlString QuotationStaff
		{
			get
			{
				return _quotationStaff;
			}
			set
			{
				_quotationStaff = value;
			}
		}


		public SqlInt32 DepartmentID
		{
			get
			{
				return _departmentID;
			}
			set
			{
				_departmentID = value;
			}
		}


		public SqlString Office_City
		{
			get
			{
				return _office_City;
			}
			set
			{
				_office_City = value;
			}
		}


		public SqlString Office_Country
		{
			get
			{
				return _office_Country;
			}
			set
			{
				_office_Country = value;
			}
		}


		public SqlString Office_Telephone
		{
			get
			{
				return _office_Telephone;
			}
			set
			{
				_office_Telephone = value;
			}
		}


		public SqlString Office_MobileNumber
		{
			get
			{
				return _office_MobileNumber;
			}
			set
			{
				_office_MobileNumber = value;
			}
		}


		public SqlString Office_Fax
		{
			get
			{
				return _office_Fax;
			}
			set
			{
				_office_Fax = value;
			}
		}


		public SqlString Office_WeChat
		{
			get
			{
				return _office_WeChat;
			}
			set
			{
				_office_WeChat = value;
			}
		}


		public SqlString Office_Email
		{
			get
			{
				return _office_Email;
			}
			set
			{
				_office_Email = value;
			}
		}


		public SqlDateTime TourStartDate
		{
			get
			{
				return _tourStartDate;
			}
			set
			{
				_tourStartDate = value;
			}
		}


		public SqlDateTime TourEndDate
		{
			get
			{
				return _tourEndDate;
			}
			set
			{
				_tourEndDate = value;
			}
		}


		public SqlInt32 NoOfServiceDay
		{
			get
			{
				return _noOfServiceDay;
			}
			set
			{
				_noOfServiceDay = value;
			}
		}


		public SqlInt32 TotalClient
		{
			get
			{
				return _totalClient;
			}
			set
			{
				_totalClient = value;
			}
		}


		public SqlInt32 NoOfAdults
		{
			get
			{
				return _noOfAdults;
			}
			set
			{
				_noOfAdults = value;
			}
		}


		public SqlInt32 NoOfChild5To12
		{
			get
			{
				return _noOfChild5To12;
			}
			set
			{
				_noOfChild5To12 = value;
			}
		}


		public SqlInt32 NoOfChild2To5
		{
			get
			{
				return _noOfChild2To5;
			}
			set
			{
				_noOfChild2To5 = value;
			}
		}


		public SqlInt32 NoOfInfant1To2
		{
			get
			{
				return _noOfInfant1To2;
			}
			set
			{
				_noOfInfant1To2 = value;
			}
		}


		public SqlString Comments
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}


		public SqlString MessageToClient
		{
			get
			{
				return _messageToClient;
			}
			set
			{
				_messageToClient = value;
			}
		}


		public SqlString QuotationStatus
		{
			get
			{
				return _quotationStatus;
			}
			set
			{
				_quotationStatus = value;
			}
		}


		public SqlString QuotationReport
		{
			get
			{
				return _quotationReport;
			}
			set
			{
				_quotationReport = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
