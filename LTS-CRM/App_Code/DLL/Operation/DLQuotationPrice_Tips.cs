using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.InteropServices;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'QuotationPrice_Tips'.
	/// </summary>
	public class DLQuotationPrice_Tips : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_createdDate, _updatedDate;
			private SqlDecimal		_markUpPercentage, _grossRateInTotal, _profit, _netRateInTotal;
			private SqlInt32		_iD, _quotationPriceBasicId, _quotationPriceBasicIdOld, _noOfPeople, _tipsPerDay;
			private SqlString		_quotationCurrency, _updatedBy, _remark, _confirmationStatus, _createdBy;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLQuotationPrice_Tips()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceBasicId</LI>
		///		 <LI>TipsPerDay. May be SqlInt32.Null</LI>
		///		 <LI>NoOfPeople. May be SqlInt32.Null</LI>
		///		 <LI>NetRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>MarkUpPercentage. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>Profit. May be SqlDecimal.Null</LI>
		///		 <LI>Remark. May be SqlString.Null</LI>
		///		 <LI>QuotationCurrency. May be SqlString.Null</LI>
		///		 <LI>ConfirmationStatus. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceBasicId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceBasicId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iTipsPerDay", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _tipsPerDay));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfPeople", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfPeople));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _netRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcMarkUpPercentage", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _markUpPercentage));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProfit", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _profit));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemark", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remark));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationCurrency", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationCurrency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sConfirmationStatus", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _confirmationStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>QuotationPriceBasicId</LI>
		///		 <LI>TipsPerDay. May be SqlInt32.Null</LI>
		///		 <LI>NoOfPeople. May be SqlInt32.Null</LI>
		///		 <LI>NetRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>MarkUpPercentage. May be SqlDecimal.Null</LI>
		///		 <LI>GrossRateInTotal. May be SqlDecimal.Null</LI>
		///		 <LI>Profit. May be SqlDecimal.Null</LI>
		///		 <LI>Remark. May be SqlString.Null</LI>
		///		 <LI>QuotationCurrency. May be SqlString.Null</LI>
		///		 <LI>ConfirmationStatus. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceBasicId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceBasicId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iTipsPerDay", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _tipsPerDay));
				cmdToExecute.Parameters.Add(new SqlParameter("@iNoOfPeople", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _noOfPeople));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcNetRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _netRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcMarkUpPercentage", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _markUpPercentage));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcGrossRateInTotal", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _grossRateInTotal));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProfit", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 10, 2, "", DataRowVersion.Proposed, _profit));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRemark", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _remark));
				cmdToExecute.Parameters.Add(new SqlParameter("@sQuotationCurrency", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _quotationCurrency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sConfirmationStatus", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _confirmationStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method for updating one or more rows using the Foreign Key 'QuotationPriceBasicId.
		/// This method will Update one or more existing rows in the database. It will reset the field 'QuotationPriceBasicId' in
		/// all rows which have as value for this field the value as set in property 'QuotationPriceBasicIdOld' to 
		/// the value as set in property 'QuotationPriceBasicId'.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceBasicId</LI>
		///		 <LI>QuotationPriceBasicIdOld</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool UpdateAllWQuotationPriceBasicIdLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceBasicId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceBasicId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceBasicIdOld", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceBasicIdOld));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_UpdateAllWQuotationPriceBasicIdLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::UpdateAllWQuotationPriceBasicIdLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method for a foreign key. This method will Delete one or more rows from the database, based on the Foreign Key 'QuotationPriceBasicId'
		/// </summary>
		/// <returns>True if succeeded, false otherwise. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceBasicId</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public bool DeleteAllWQuotationPriceBasicIdLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceBasicId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceBasicId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_DeleteAllWQuotationPriceBasicIdLogic' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::DeleteAllWQuotationPriceBasicIdLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>QuotationPriceBasicId</LI>
		///		 <LI>TipsPerDay</LI>
		///		 <LI>NoOfPeople</LI>
		///		 <LI>NetRateInTotal</LI>
		///		 <LI>MarkUpPercentage</LI>
		///		 <LI>GrossRateInTotal</LI>
		///		 <LI>Profit</LI>
		///		 <LI>Remark</LI>
		///		 <LI>QuotationCurrency</LI>
		///		 <LI>ConfirmationStatus</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("QuotationPrice_Tips");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_quotationPriceBasicId = (Int32)toReturn.Rows[0]["QuotationPriceBasicId"];
					_tipsPerDay = toReturn.Rows[0]["TipsPerDay"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["TipsPerDay"];
					_noOfPeople = toReturn.Rows[0]["NoOfPeople"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["NoOfPeople"];
					_netRateInTotal = toReturn.Rows[0]["NetRateInTotal"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["NetRateInTotal"];
					_markUpPercentage = toReturn.Rows[0]["MarkUpPercentage"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["MarkUpPercentage"];
					_grossRateInTotal = toReturn.Rows[0]["GrossRateInTotal"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["GrossRateInTotal"];
					_profit = toReturn.Rows[0]["Profit"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["Profit"];
					_remark = toReturn.Rows[0]["Remark"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Remark"];
					_quotationCurrency = toReturn.Rows[0]["QuotationCurrency"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["QuotationCurrency"];
					_confirmationStatus = toReturn.Rows[0]["ConfirmationStatus"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ConfirmationStatus"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("QuotationPrice_Tips");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method for a foreign key. This method will Select one or more rows from the database, based on the Foreign Key 'QuotationPriceBasicId'
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>QuotationPriceBasicId</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public DataTable SelectAllWQuotationPriceBasicIdLogic()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("QuotationPrice_Tips");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceBasicId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _quotationPriceBasicId));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_SelectAllWQuotationPriceBasicIdLogic' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLQuotationPrice_Tips::SelectAllWQuotationPriceBasicIdLogic::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}

        public int QPID { get; set; }
        public DataTable SelectAllWQuotationPriceIdLogic()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_QuotationPrice_Tips_SelectByPriceIdLogic]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("QuotationOthers");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iQuotationPriceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, QPID));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_QuotationPrice_Tips_SelectByPriceIdLogic' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLQuotationPrice_Tips::sp_QuotationPrice_Tips_SelectByPriceIdLogic::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 QuotationPriceBasicId
		{
			get
			{
				return _quotationPriceBasicId;
			}
			set
			{
				SqlInt32 quotationPriceBasicIdTmp = (SqlInt32)value;
				if(quotationPriceBasicIdTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("QuotationPriceBasicId", "QuotationPriceBasicId can't be NULL");
				}
				_quotationPriceBasicId = value;
			}
		}
		public SqlInt32 QuotationPriceBasicIdOld
		{
			get
			{
				return _quotationPriceBasicIdOld;
			}
			set
			{
				SqlInt32 quotationPriceBasicIdOldTmp = (SqlInt32)value;
				if(quotationPriceBasicIdOldTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("QuotationPriceBasicIdOld", "QuotationPriceBasicIdOld can't be NULL");
				}
				_quotationPriceBasicIdOld = value;
			}
		}


		public SqlInt32 TipsPerDay
		{
			get
			{
				return _tipsPerDay;
			}
			set
			{
				_tipsPerDay = value;
			}
		}


		public SqlInt32 NoOfPeople
		{
			get
			{
				return _noOfPeople;
			}
			set
			{
				_noOfPeople = value;
			}
		}


		public SqlDecimal NetRateInTotal
		{
			get
			{
				return _netRateInTotal;
			}
			set
			{
				_netRateInTotal = value;
			}
		}


		public SqlDecimal MarkUpPercentage
		{
			get
			{
				return _markUpPercentage;
			}
			set
			{
				_markUpPercentage = value;
			}
		}


		public SqlDecimal GrossRateInTotal
		{
			get
			{
				return _grossRateInTotal;
			}
			set
			{
				_grossRateInTotal = value;
			}
		}


		public SqlDecimal Profit
		{
			get
			{
				return _profit;
			}
			set
			{
				_profit = value;
			}
		}


		public SqlString Remark
		{
			get
			{
				return _remark;
			}
			set
			{
				_remark = value;
			}
		}


		public SqlString QuotationCurrency
		{
			get
			{
				return _quotationCurrency;
			}
			set
			{
				_quotationCurrency = value;
			}
		}


		public SqlString ConfirmationStatus
		{
			get
			{
				return _confirmationStatus;
			}
			set
			{
				_confirmationStatus = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
