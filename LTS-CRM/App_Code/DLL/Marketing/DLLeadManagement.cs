 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'LeadManagement'.
	/// </summary>
	public class DLLeadManagement : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlBoolean		_flightsNeeded;
			private SqlDateTime		_plannedDepartureDate, _birthDate, _updatedDate, _createdDate;
			private SqlDecimal		_totalBudget;
			private SqlInt32		_howManyDays, _iD, _howManyChild, _howManyPeople, _companyID, _campaignID;
			private SqlString		_zipCode, _state, _country, _notes, _webPage, _leadOwner, _leadSource, _leadStatus, _city, _insurance, _clientType, _response, _createdBy, _tourGuide, _updatedBy, _region, _homeTown, _referredBy, _contactType, _bDM, _contactsInterests, _preferredContactTime, _address, _preferredContactMethod, _surName, _firstName, _businessPhone, _alternateEmail, _email, _homePhone, _faxNumber, _mobileNo, _vehicle, _tourType, _currency, _admissionTickets, _meals, _hotelType, _placeOfDeparture, _title, _gender, _company, _placeOfReturn;
			private SqlBinary		_attachment;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLLeadManagement()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>ContactType. May be SqlString.Null</LI>
		///		 <LI>CampaignID. May be SqlInt32.Null</LI>
		///		 <LI>TourType. May be SqlString.Null</LI>
		///		 <LI>Company. May be SqlString.Null</LI>
		///		 <LI>Title. May be SqlString.Null</LI>
		///		 <LI>Gender</LI>
		///		 <LI>SurName</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>AlternateEmail. May be SqlString.Null</LI>
		///		 <LI>HomePhone. May be SqlString.Null</LI>
		///		 <LI>MobileNo. May be SqlString.Null</LI>
		///		 <LI>FaxNumber. May be SqlString.Null</LI>
		///		 <LI>BusinessPhone. May be SqlString.Null</LI>
		///		 <LI>Address. May be SqlString.Null</LI>
		///		 <LI>PreferredContactTime. May be SqlString.Null</LI>
		///		 <LI>PreferredContactMethod. May be SqlString.Null</LI>
		///		 <LI>PlannedDepartureDate. May be SqlDateTime.Null</LI>
		///		 <LI>HowManyDays. May be SqlInt32.Null</LI>
		///		 <LI>HowManyPeople. May be SqlInt32.Null</LI>
		///		 <LI>HowManyChild. May be SqlInt32.Null</LI>
		///		 <LI>PlaceOfDeparture. May be SqlString.Null</LI>
		///		 <LI>PlaceOfReturn. May be SqlString.Null</LI>
		///		 <LI>TotalBudget. May be SqlDecimal.Null</LI>
		///		 <LI>Currency. May be SqlString.Null</LI>
		///		 <LI>TourGuide. May be SqlString.Null</LI>
		///		 <LI>FlightsNeeded. May be SqlBoolean.Null</LI>
		///		 <LI>Vehicle. May be SqlString.Null</LI>
		///		 <LI>HotelType. May be SqlString.Null</LI>
		///		 <LI>Meals. May be SqlString.Null</LI>
		///		 <LI>AdmissionTickets. May be SqlString.Null</LI>
		///		 <LI>Insurance. May be SqlString.Null</LI>
		///		 <LI>LeadSource. May be SqlString.Null</LI>
		///		 <LI>LeadOwner. May be SqlString.Null</LI>
		///		 <LI>LeadStatus. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>State. May be SqlString.Null</LI>
		///		 <LI>ZipCode. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>WebPage. May be SqlString.Null</LI>
		///		 <LI>Notes. May be SqlString.Null</LI>
		///		 <LI>ReferredBy. May be SqlString.Null</LI>
		///		 <LI>HomeTown. May be SqlString.Null</LI>
		///		 <LI>BirthDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContactsInterests. May be SqlString.Null</LI>
		///		 <LI>Attachment. May be SqlBinary.Null</LI>
		///		 <LI>BDM. May be SqlString.Null</LI>
		///		 <LI>Region. May be SqlString.Null</LI>
		///		 <LI>Response. May be SqlString.Null</LI>
		///		 <LI>ClientType. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_LeadManagement_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactType));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCampaignID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _campaignID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTourType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourType));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompany", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _company));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _title));
				cmdToExecute.Parameters.Add(new SqlParameter("@sGender", SqlDbType.NVarChar, 10, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _gender));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAlternateEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _alternateEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHomePhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _homePhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNumber", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBusinessPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _businessPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPreferredContactTime", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _preferredContactTime));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPreferredContactMethod", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _preferredContactMethod));
				cmdToExecute.Parameters.Add(new SqlParameter("@daPlannedDepartureDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _plannedDepartureDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iHowManyDays", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _howManyDays));
				cmdToExecute.Parameters.Add(new SqlParameter("@iHowManyPeople", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _howManyPeople));
				cmdToExecute.Parameters.Add(new SqlParameter("@iHowManyChild", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _howManyChild));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPlaceOfDeparture", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _placeOfDeparture));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPlaceOfReturn", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _placeOfReturn));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcTotalBudget", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _totalBudget));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCurrency", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _currency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTourGuide", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourGuide));
				cmdToExecute.Parameters.Add(new SqlParameter("@bFlightsNeeded", SqlDbType.Bit, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _flightsNeeded));
				cmdToExecute.Parameters.Add(new SqlParameter("@sVehicle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHotelType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _hotelType));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMeals", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _meals));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAdmissionTickets", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _admissionTickets));
				cmdToExecute.Parameters.Add(new SqlParameter("@sInsurance", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _insurance));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLeadSource", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _leadSource));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLeadOwner", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _leadOwner));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLeadStatus", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _leadStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sState", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _state));
				cmdToExecute.Parameters.Add(new SqlParameter("@sZipCode", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _zipCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWebPage", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _webPage));
				cmdToExecute.Parameters.Add(new SqlParameter("@sNotes", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _notes));
				cmdToExecute.Parameters.Add(new SqlParameter("@sReferredBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _referredBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHomeTown", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _homeTown));
				cmdToExecute.Parameters.Add(new SqlParameter("@daBirthDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _birthDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactsInterests", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactsInterests));
				cmdToExecute.Parameters.Add(new SqlParameter("@biAttachment", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _attachment));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBDM", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _bDM));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRegion", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _region));
				cmdToExecute.Parameters.Add(new SqlParameter("@sResponse", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _response));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClientType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _clientType));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
                _iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_LeadManagement_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLLeadManagement::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>ContactType. May be SqlString.Null</LI>
		///		 <LI>CampaignID. May be SqlInt32.Null</LI>
		///		 <LI>TourType. May be SqlString.Null</LI>
		///		 <LI>Company. May be SqlString.Null</LI>
		///		 <LI>Title. May be SqlString.Null</LI>
		///		 <LI>Gender</LI>
		///		 <LI>SurName</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>AlternateEmail. May be SqlString.Null</LI>
		///		 <LI>HomePhone. May be SqlString.Null</LI>
		///		 <LI>MobileNo. May be SqlString.Null</LI>
		///		 <LI>FaxNumber. May be SqlString.Null</LI>
		///		 <LI>BusinessPhone. May be SqlString.Null</LI>
		///		 <LI>Address. May be SqlString.Null</LI>
		///		 <LI>PreferredContactTime. May be SqlString.Null</LI>
		///		 <LI>PreferredContactMethod. May be SqlString.Null</LI>
		///		 <LI>PlannedDepartureDate. May be SqlDateTime.Null</LI>
		///		 <LI>HowManyDays. May be SqlInt32.Null</LI>
		///		 <LI>HowManyPeople. May be SqlInt32.Null</LI>
		///		 <LI>HowManyChild. May be SqlInt32.Null</LI>
		///		 <LI>PlaceOfDeparture. May be SqlString.Null</LI>
		///		 <LI>PlaceOfReturn. May be SqlString.Null</LI>
		///		 <LI>TotalBudget. May be SqlDecimal.Null</LI>
		///		 <LI>Currency. May be SqlString.Null</LI>
		///		 <LI>TourGuide. May be SqlString.Null</LI>
		///		 <LI>FlightsNeeded. May be SqlBoolean.Null</LI>
		///		 <LI>Vehicle. May be SqlString.Null</LI>
		///		 <LI>HotelType. May be SqlString.Null</LI>
		///		 <LI>Meals. May be SqlString.Null</LI>
		///		 <LI>AdmissionTickets. May be SqlString.Null</LI>
		///		 <LI>Insurance. May be SqlString.Null</LI>
		///		 <LI>LeadSource. May be SqlString.Null</LI>
		///		 <LI>LeadOwner. May be SqlString.Null</LI>
		///		 <LI>LeadStatus. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>State. May be SqlString.Null</LI>
		///		 <LI>ZipCode. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>WebPage. May be SqlString.Null</LI>
		///		 <LI>Notes. May be SqlString.Null</LI>
		///		 <LI>ReferredBy. May be SqlString.Null</LI>
		///		 <LI>HomeTown. May be SqlString.Null</LI>
		///		 <LI>BirthDate. May be SqlDateTime.Null</LI>
		///		 <LI>ContactsInterests. May be SqlString.Null</LI>
		///		 <LI>Attachment. May be SqlBinary.Null</LI>
		///		 <LI>BDM. May be SqlString.Null</LI>
		///		 <LI>Region. May be SqlString.Null</LI>
		///		 <LI>Response. May be SqlString.Null</LI>
		///		 <LI>ClientType. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_LeadManagement_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactType));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCampaignID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _campaignID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTourType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourType));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompany", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _company));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _title));
				cmdToExecute.Parameters.Add(new SqlParameter("@sGender", SqlDbType.NVarChar, 10, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _gender));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAlternateEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _alternateEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHomePhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _homePhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNumber", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNumber));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBusinessPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _businessPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAddress", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPreferredContactTime", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _preferredContactTime));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPreferredContactMethod", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _preferredContactMethod));
				cmdToExecute.Parameters.Add(new SqlParameter("@daPlannedDepartureDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _plannedDepartureDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iHowManyDays", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _howManyDays));
				cmdToExecute.Parameters.Add(new SqlParameter("@iHowManyPeople", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _howManyPeople));
				cmdToExecute.Parameters.Add(new SqlParameter("@iHowManyChild", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _howManyChild));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPlaceOfDeparture", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _placeOfDeparture));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPlaceOfReturn", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _placeOfReturn));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcTotalBudget", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _totalBudget));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCurrency", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _currency));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTourGuide", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _tourGuide));
				cmdToExecute.Parameters.Add(new SqlParameter("@bFlightsNeeded", SqlDbType.Bit, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _flightsNeeded));
				cmdToExecute.Parameters.Add(new SqlParameter("@sVehicle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _vehicle));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHotelType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _hotelType));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMeals", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _meals));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAdmissionTickets", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _admissionTickets));
				cmdToExecute.Parameters.Add(new SqlParameter("@sInsurance", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _insurance));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLeadSource", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _leadSource));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLeadOwner", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _leadOwner));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLeadStatus", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _leadStatus));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sState", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _state));
				cmdToExecute.Parameters.Add(new SqlParameter("@sZipCode", SqlDbType.NVarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _zipCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWebPage", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _webPage));
				cmdToExecute.Parameters.Add(new SqlParameter("@sNotes", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _notes));
				cmdToExecute.Parameters.Add(new SqlParameter("@sReferredBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _referredBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHomeTown", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _homeTown));
				cmdToExecute.Parameters.Add(new SqlParameter("@daBirthDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _birthDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContactsInterests", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contactsInterests));
				cmdToExecute.Parameters.Add(new SqlParameter("@biAttachment", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _attachment));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBDM", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _bDM));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRegion", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _region));
				cmdToExecute.Parameters.Add(new SqlParameter("@sResponse", SqlDbType.NVarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _response));
				cmdToExecute.Parameters.Add(new SqlParameter("@sClientType", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _clientType));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_LeadManagement_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLLeadManagement::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_LeadManagement_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_LeadManagement_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLLeadManagement::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>ContactType</LI>
		///		 <LI>CampaignID</LI>
		///		 <LI>TourType</LI>
		///		 <LI>Company</LI>
		///		 <LI>Title</LI>
		///		 <LI>Gender</LI>
		///		 <LI>SurName</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>Email</LI>
		///		 <LI>AlternateEmail</LI>
		///		 <LI>HomePhone</LI>
		///		 <LI>MobileNo</LI>
		///		 <LI>FaxNumber</LI>
		///		 <LI>BusinessPhone</LI>
		///		 <LI>Address</LI>
		///		 <LI>PreferredContactTime</LI>
		///		 <LI>PreferredContactMethod</LI>
		///		 <LI>PlannedDepartureDate</LI>
		///		 <LI>HowManyDays</LI>
		///		 <LI>HowManyPeople</LI>
		///		 <LI>HowManyChild</LI>
		///		 <LI>PlaceOfDeparture</LI>
		///		 <LI>PlaceOfReturn</LI>
		///		 <LI>TotalBudget</LI>
		///		 <LI>Currency</LI>
		///		 <LI>TourGuide</LI>
		///		 <LI>FlightsNeeded</LI>
		///		 <LI>Vehicle</LI>
		///		 <LI>HotelType</LI>
		///		 <LI>Meals</LI>
		///		 <LI>AdmissionTickets</LI>
		///		 <LI>Insurance</LI>
		///		 <LI>LeadSource</LI>
		///		 <LI>LeadOwner</LI>
		///		 <LI>LeadStatus</LI>
		///		 <LI>City</LI>
		///		 <LI>State</LI>
		///		 <LI>ZipCode</LI>
		///		 <LI>Country</LI>
		///		 <LI>WebPage</LI>
		///		 <LI>Notes</LI>
		///		 <LI>ReferredBy</LI>
		///		 <LI>HomeTown</LI>
		///		 <LI>BirthDate</LI>
		///		 <LI>ContactsInterests</LI>
		///		 <LI>Attachment</LI>
		///		 <LI>BDM</LI>
		///		 <LI>Region</LI>
		///		 <LI>Response</LI>
		///		 <LI>ClientType</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_LeadManagement_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("LeadManagement");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_LeadManagement_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_companyID = toReturn.Rows[0]["CompanyID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["CompanyID"];
					_contactType = toReturn.Rows[0]["ContactType"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactType"];
					_campaignID = toReturn.Rows[0]["CampaignID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["CampaignID"];
					_tourType = toReturn.Rows[0]["TourType"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["TourType"];
					_company = toReturn.Rows[0]["Company"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Company"];
					_title = toReturn.Rows[0]["Title"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Title"];
					_gender = (string)toReturn.Rows[0]["Gender"];
					_surName = (string)toReturn.Rows[0]["SurName"];
					_firstName = (string)toReturn.Rows[0]["FirstName"];
					_email = toReturn.Rows[0]["Email"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Email"];
					_alternateEmail = toReturn.Rows[0]["AlternateEmail"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["AlternateEmail"];
					_homePhone = toReturn.Rows[0]["HomePhone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HomePhone"];
					_mobileNo = toReturn.Rows[0]["MobileNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["MobileNo"];
					_faxNumber = toReturn.Rows[0]["FaxNumber"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FaxNumber"];
					_businessPhone = toReturn.Rows[0]["BusinessPhone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["BusinessPhone"];
					_address = toReturn.Rows[0]["Address"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address"];
					_preferredContactTime = toReturn.Rows[0]["PreferredContactTime"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PreferredContactTime"];
					_preferredContactMethod = toReturn.Rows[0]["PreferredContactMethod"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PreferredContactMethod"];
					_plannedDepartureDate = toReturn.Rows[0]["PlannedDepartureDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["PlannedDepartureDate"];
					_howManyDays = toReturn.Rows[0]["HowManyDays"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["HowManyDays"];
					_howManyPeople = toReturn.Rows[0]["HowManyPeople"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["HowManyPeople"];
					_howManyChild = toReturn.Rows[0]["HowManyChild"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["HowManyChild"];
					_placeOfDeparture = toReturn.Rows[0]["PlaceOfDeparture"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PlaceOfDeparture"];
					_placeOfReturn = toReturn.Rows[0]["PlaceOfReturn"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PlaceOfReturn"];
					_totalBudget = toReturn.Rows[0]["TotalBudget"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["TotalBudget"];
					_currency = toReturn.Rows[0]["Currency"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Currency"];
					_tourGuide = toReturn.Rows[0]["TourGuide"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["TourGuide"];
					_flightsNeeded = toReturn.Rows[0]["FlightsNeeded"] == System.DBNull.Value ? SqlBoolean.Null : (bool)toReturn.Rows[0]["FlightsNeeded"];
					_vehicle = toReturn.Rows[0]["Vehicle"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Vehicle"];
					_hotelType = toReturn.Rows[0]["HotelType"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HotelType"];
					_meals = toReturn.Rows[0]["Meals"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Meals"];
					_admissionTickets = toReturn.Rows[0]["AdmissionTickets"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["AdmissionTickets"];
					_insurance = toReturn.Rows[0]["Insurance"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Insurance"];
					_leadSource = toReturn.Rows[0]["LeadSource"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["LeadSource"];
					_leadOwner = toReturn.Rows[0]["LeadOwner"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["LeadOwner"];
					_leadStatus = toReturn.Rows[0]["LeadStatus"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["LeadStatus"];
					_city = toReturn.Rows[0]["City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["City"];
					_state = toReturn.Rows[0]["State"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["State"];
					_zipCode = toReturn.Rows[0]["ZipCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ZipCode"];
					_country = toReturn.Rows[0]["Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Country"];
					_webPage = toReturn.Rows[0]["WebPage"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["WebPage"];
					_notes = toReturn.Rows[0]["Notes"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Notes"];
					_referredBy = toReturn.Rows[0]["ReferredBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ReferredBy"];
					_homeTown = toReturn.Rows[0]["HomeTown"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HomeTown"];
					_birthDate = toReturn.Rows[0]["BirthDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["BirthDate"];
					_contactsInterests = toReturn.Rows[0]["ContactsInterests"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContactsInterests"];
					_attachment = toReturn.Rows[0]["Attachment"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["Attachment"];
					_bDM = toReturn.Rows[0]["BDM"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["BDM"];
					_region = toReturn.Rows[0]["Region"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Region"];
					_response = toReturn.Rows[0]["Response"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Response"];
					_clientType = toReturn.Rows[0]["ClientType"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ClientType"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLLeadManagement::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_LeadManagement_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("LeadManagement");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_LeadManagement_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLLeadManagement::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 CompanyID
		{
			get
			{
				return _companyID;
			}
			set
			{
				_companyID = value;
			}
		}


		public SqlString ContactType
		{
			get
			{
				return _contactType;
			}
			set
			{
				_contactType = value;
			}
		}


		public SqlInt32 CampaignID
		{
			get
			{
				return _campaignID;
			}
			set
			{
				_campaignID = value;
			}
		}


		public SqlString TourType
		{
			get
			{
				return _tourType;
			}
			set
			{
				_tourType = value;
			}
		}


		public SqlString Company
		{
			get
			{
				return _company;
			}
			set
			{
				_company = value;
			}
		}


		public SqlString Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
			}
		}


		public SqlString Gender
		{
			get
			{
				return _gender;
			}
			set
			{
				SqlString genderTmp = (SqlString)value;
				if(genderTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("Gender", "Gender can't be NULL");
				}
				_gender = value;
			}
		}


		public SqlString SurName
		{
			get
			{
				return _surName;
			}
			set
			{
				SqlString surNameTmp = (SqlString)value;
				if(surNameTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("SurName", "SurName can't be NULL");
				}
				_surName = value;
			}
		}


		public SqlString FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				SqlString firstNameTmp = (SqlString)value;
				if(firstNameTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("FirstName", "FirstName can't be NULL");
				}
				_firstName = value;
			}
		}


		public SqlString Email
		{
			get
			{
				return _email;
			}
			set
			{
				_email = value;
			}
		}


		public SqlString AlternateEmail
		{
			get
			{
				return _alternateEmail;
			}
			set
			{
				_alternateEmail = value;
			}
		}


		public SqlString HomePhone
		{
			get
			{
				return _homePhone;
			}
			set
			{
				_homePhone = value;
			}
		}


		public SqlString MobileNo
		{
			get
			{
				return _mobileNo;
			}
			set
			{
				_mobileNo = value;
			}
		}


		public SqlString FaxNumber
		{
			get
			{
				return _faxNumber;
			}
			set
			{
				_faxNumber = value;
			}
		}


		public SqlString BusinessPhone
		{
			get
			{
				return _businessPhone;
			}
			set
			{
				_businessPhone = value;
			}
		}


		public SqlString Address
		{
			get
			{
				return _address;
			}
			set
			{
				_address = value;
			}
		}


		public SqlString PreferredContactTime
		{
			get
			{
				return _preferredContactTime;
			}
			set
			{
				_preferredContactTime = value;
			}
		}


		public SqlString PreferredContactMethod
		{
			get
			{
				return _preferredContactMethod;
			}
			set
			{
				_preferredContactMethod = value;
			}
		}


		public SqlDateTime PlannedDepartureDate
		{
			get
			{
				return _plannedDepartureDate;
			}
			set
			{
				_plannedDepartureDate = value;
			}
		}


		public SqlInt32 HowManyDays
		{
			get
			{
				return _howManyDays;
			}
			set
			{
				_howManyDays = value;
			}
		}


		public SqlInt32 HowManyPeople
		{
			get
			{
				return _howManyPeople;
			}
			set
			{
				_howManyPeople = value;
			}
		}


		public SqlInt32 HowManyChild
		{
			get
			{
				return _howManyChild;
			}
			set
			{
				_howManyChild = value;
			}
		}


		public SqlString PlaceOfDeparture
		{
			get
			{
				return _placeOfDeparture;
			}
			set
			{
				_placeOfDeparture = value;
			}
		}


		public SqlString PlaceOfReturn
		{
			get
			{
				return _placeOfReturn;
			}
			set
			{
				_placeOfReturn = value;
			}
		}


		public SqlDecimal TotalBudget
		{
			get
			{
				return _totalBudget;
			}
			set
			{
				_totalBudget = value;
			}
		}


		public SqlString Currency
		{
			get
			{
				return _currency;
			}
			set
			{
				_currency = value;
			}
		}


		public SqlString TourGuide
		{
			get
			{
				return _tourGuide;
			}
			set
			{
				_tourGuide = value;
			}
		}


		public SqlBoolean FlightsNeeded
		{
			get
			{
				return _flightsNeeded;
			}
			set
			{
				_flightsNeeded = value;
			}
		}


		public SqlString Vehicle
		{
			get
			{
				return _vehicle;
			}
			set
			{
				_vehicle = value;
			}
		}


		public SqlString HotelType
		{
			get
			{
				return _hotelType;
			}
			set
			{
				_hotelType = value;
			}
		}


		public SqlString Meals
		{
			get
			{
				return _meals;
			}
			set
			{
				_meals = value;
			}
		}


		public SqlString AdmissionTickets
		{
			get
			{
				return _admissionTickets;
			}
			set
			{
				_admissionTickets = value;
			}
		}


		public SqlString Insurance
		{
			get
			{
				return _insurance;
			}
			set
			{
				_insurance = value;
			}
		}


		public SqlString LeadSource
		{
			get
			{
				return _leadSource;
			}
			set
			{
				_leadSource = value;
			}
		}


		public SqlString LeadOwner
		{
			get
			{
				return _leadOwner;
			}
			set
			{
				_leadOwner = value;
			}
		}


		public SqlString LeadStatus
		{
			get
			{
				return _leadStatus;
			}
			set
			{
				_leadStatus = value;
			}
		}


		public SqlString City
		{
			get
			{
				return _city;
			}
			set
			{
				_city = value;
			}
		}


		public SqlString State
		{
			get
			{
				return _state;
			}
			set
			{
				_state = value;
			}
		}


		public SqlString ZipCode
		{
			get
			{
				return _zipCode;
			}
			set
			{
				_zipCode = value;
			}
		}


		public SqlString Country
		{
			get
			{
				return _country;
			}
			set
			{
				_country = value;
			}
		}


		public SqlString WebPage
		{
			get
			{
				return _webPage;
			}
			set
			{
				_webPage = value;
			}
		}


		public SqlString Notes
		{
			get
			{
				return _notes;
			}
			set
			{
				_notes = value;
			}
		}


		public SqlString ReferredBy
		{
			get
			{
				return _referredBy;
			}
			set
			{
				_referredBy = value;
			}
		}


		public SqlString HomeTown
		{
			get
			{
				return _homeTown;
			}
			set
			{
				_homeTown = value;
			}
		}


		public SqlDateTime BirthDate
		{
			get
			{
				return _birthDate;
			}
			set
			{
				_birthDate = value;
			}
		}


		public SqlString ContactsInterests
		{
			get
			{
				return _contactsInterests;
			}
			set
			{
				_contactsInterests = value;
			}
		}


		public SqlBinary Attachment
		{
			get
			{
				return _attachment;
			}
			set
			{
				_attachment = value;
			}
		}


		public SqlString BDM
		{
			get
			{
				return _bDM;
			}
			set
			{
				_bDM = value;
			}
		}


		public SqlString Region
		{
			get
			{
				return _region;
			}
			set
			{
				_region = value;
			}
		}


		public SqlString Response
		{
			get
			{
				return _response;
			}
			set
			{
				_response = value;
			}
		}


		public SqlString ClientType
		{
			get
			{
				return _clientType;
			}
			set
			{
				_clientType = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
