 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'MarketingProject'.
	/// </summary>
	public class DLMarketingProject : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_projectActualEndDate, _projectStartingDate, _projectPlannedEndingDate, _createdDate, _updatedDate;
			private SqlDecimal		_projectGrossProfit, _projectIncome, _totalBudget, _totalEstimateBudget, _projectExpense;
			private SqlInt32		_companyID, _iD;
			private SqlString		_extraField4, _extraField2, _extraField3, _extraField5, _workingProcessComment, _updatedBy, _createdBy, _projectDescribution, _rating, _staffInvolved, _initiateBy, _referenceNo, _extraField1, _projectReport, _projectName;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLMarketingProject()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>ReferenceNo. May be SqlString.Null</LI>
		///		 <LI>ProjectName. May be SqlString.Null</LI>
		///		 <LI>ProjectDescribution. May be SqlString.Null</LI>
		///		 <LI>InitiateBy. May be SqlString.Null</LI>
		///		 <LI>StaffInvolved. May be SqlString.Null</LI>
		///		 <LI>ProjectStartingDate. May be SqlDateTime.Null</LI>
		///		 <LI>ProjectPlannedEndingDate. May be SqlDateTime.Null</LI>
		///		 <LI>WorkingProcessComment. May be SqlString.Null</LI>
		///		 <LI>ProjectActualEndDate. May be SqlDateTime.Null</LI>
		///		 <LI>ProjectIncome. May be SqlDecimal.Null</LI>
		///		 <LI>ProjectExpense. May be SqlDecimal.Null</LI>
		///		 <LI>ProjectGrossProfit. May be SqlDecimal.Null</LI>
		///		 <LI>Rating. May be SqlString.Null</LI>
		///		 <LI>ProjectReport. May be SqlString.Null</LI>
		///		 <LI>TotalBudget. May be SqlDecimal.Null</LI>
		///		 <LI>TotalEstimateBudget. May be SqlDecimal.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingProject_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sReferenceNo", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _referenceNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProjectName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProjectDescribution", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectDescribution));
				cmdToExecute.Parameters.Add(new SqlParameter("@sInitiateBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _initiateBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStaffInvolved", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _staffInvolved));
				cmdToExecute.Parameters.Add(new SqlParameter("@daProjectStartingDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectStartingDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daProjectPlannedEndingDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectPlannedEndingDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWorkingProcessComment", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _workingProcessComment));
				cmdToExecute.Parameters.Add(new SqlParameter("@daProjectActualEndDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectActualEndDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProjectIncome", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _projectIncome));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProjectExpense", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _projectExpense));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProjectGrossProfit", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _projectGrossProfit));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRating", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProjectReport", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectReport));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcTotalBudget", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _totalBudget));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcTotalEstimateBudget", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _totalEstimateBudget));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingProject_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingProject::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>ReferenceNo. May be SqlString.Null</LI>
		///		 <LI>ProjectName. May be SqlString.Null</LI>
		///		 <LI>ProjectDescribution. May be SqlString.Null</LI>
		///		 <LI>InitiateBy. May be SqlString.Null</LI>
		///		 <LI>StaffInvolved. May be SqlString.Null</LI>
		///		 <LI>ProjectStartingDate. May be SqlDateTime.Null</LI>
		///		 <LI>ProjectPlannedEndingDate. May be SqlDateTime.Null</LI>
		///		 <LI>WorkingProcessComment. May be SqlString.Null</LI>
		///		 <LI>ProjectActualEndDate. May be SqlDateTime.Null</LI>
		///		 <LI>ProjectIncome. May be SqlDecimal.Null</LI>
		///		 <LI>ProjectExpense. May be SqlDecimal.Null</LI>
		///		 <LI>ProjectGrossProfit. May be SqlDecimal.Null</LI>
		///		 <LI>Rating. May be SqlString.Null</LI>
		///		 <LI>ProjectReport. May be SqlString.Null</LI>
		///		 <LI>TotalBudget. May be SqlDecimal.Null</LI>
		///		 <LI>TotalEstimateBudget. May be SqlDecimal.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingProject_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sReferenceNo", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _referenceNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProjectName", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProjectDescribution", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectDescribution));
				cmdToExecute.Parameters.Add(new SqlParameter("@sInitiateBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _initiateBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStaffInvolved", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _staffInvolved));
				cmdToExecute.Parameters.Add(new SqlParameter("@daProjectStartingDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectStartingDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@daProjectPlannedEndingDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectPlannedEndingDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWorkingProcessComment", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _workingProcessComment));
				cmdToExecute.Parameters.Add(new SqlParameter("@daProjectActualEndDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectActualEndDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProjectIncome", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _projectIncome));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProjectExpense", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _projectExpense));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcProjectGrossProfit", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _projectGrossProfit));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRating", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sProjectReport", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _projectReport));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcTotalBudget", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _totalBudget));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcTotalEstimateBudget", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _totalEstimateBudget));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingProject_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingProject::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingProject_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingProject_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingProject::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>ReferenceNo</LI>
		///		 <LI>ProjectName</LI>
		///		 <LI>ProjectDescribution</LI>
		///		 <LI>InitiateBy</LI>
		///		 <LI>StaffInvolved</LI>
		///		 <LI>ProjectStartingDate</LI>
		///		 <LI>ProjectPlannedEndingDate</LI>
		///		 <LI>WorkingProcessComment</LI>
		///		 <LI>ProjectActualEndDate</LI>
		///		 <LI>ProjectIncome</LI>
		///		 <LI>ProjectExpense</LI>
		///		 <LI>ProjectGrossProfit</LI>
		///		 <LI>Rating</LI>
		///		 <LI>ProjectReport</LI>
		///		 <LI>TotalBudget</LI>
		///		 <LI>TotalEstimateBudget</LI>
		///		 <LI>ExtraField1</LI>
		///		 <LI>ExtraField2</LI>
		///		 <LI>ExtraField3</LI>
		///		 <LI>ExtraField4</LI>
		///		 <LI>ExtraField5</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingProject_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("MarketingProject");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingProject_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_companyID = toReturn.Rows[0]["CompanyID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["CompanyID"];
					_referenceNo = toReturn.Rows[0]["ReferenceNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ReferenceNo"];
					_projectName = toReturn.Rows[0]["ProjectName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ProjectName"];
					_projectDescribution = toReturn.Rows[0]["ProjectDescribution"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ProjectDescribution"];
					_initiateBy = toReturn.Rows[0]["InitiateBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["InitiateBy"];
					_staffInvolved = toReturn.Rows[0]["StaffInvolved"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["StaffInvolved"];
					_projectStartingDate = toReturn.Rows[0]["ProjectStartingDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ProjectStartingDate"];
					_projectPlannedEndingDate = toReturn.Rows[0]["ProjectPlannedEndingDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ProjectPlannedEndingDate"];
					_workingProcessComment = toReturn.Rows[0]["WorkingProcessComment"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["WorkingProcessComment"];
					_projectActualEndDate = toReturn.Rows[0]["ProjectActualEndDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ProjectActualEndDate"];
					_projectIncome = toReturn.Rows[0]["ProjectIncome"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["ProjectIncome"];
					_projectExpense = toReturn.Rows[0]["ProjectExpense"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["ProjectExpense"];
					_projectGrossProfit = toReturn.Rows[0]["ProjectGrossProfit"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["ProjectGrossProfit"];
					_rating = toReturn.Rows[0]["Rating"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Rating"];
					_projectReport = toReturn.Rows[0]["ProjectReport"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ProjectReport"];
					_totalBudget = toReturn.Rows[0]["TotalBudget"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["TotalBudget"];
					_totalEstimateBudget = toReturn.Rows[0]["TotalEstimateBudget"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["TotalEstimateBudget"];
					_extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
					_extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
					_extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
					_extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
					_extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingProject::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingProject_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("MarketingProject");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingProject_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingProject::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 CompanyID
		{
			get
			{
				return _companyID;
			}
			set
			{
				_companyID = value;
			}
		}


		public SqlString ReferenceNo
		{
			get
			{
				return _referenceNo;
			}
			set
			{
				_referenceNo = value;
			}
		}


		public SqlString ProjectName
		{
			get
			{
				return _projectName;
			}
			set
			{
				_projectName = value;
			}
		}


		public SqlString ProjectDescribution
		{
			get
			{
				return _projectDescribution;
			}
			set
			{
				_projectDescribution = value;
			}
		}


		public SqlString InitiateBy
		{
			get
			{
				return _initiateBy;
			}
			set
			{
				_initiateBy = value;
			}
		}


		public SqlString StaffInvolved
		{
			get
			{
				return _staffInvolved;
			}
			set
			{
				_staffInvolved = value;
			}
		}


		public SqlDateTime ProjectStartingDate
		{
			get
			{
				return _projectStartingDate;
			}
			set
			{
				_projectStartingDate = value;
			}
		}


		public SqlDateTime ProjectPlannedEndingDate
		{
			get
			{
				return _projectPlannedEndingDate;
			}
			set
			{
				_projectPlannedEndingDate = value;
			}
		}


		public SqlString WorkingProcessComment
		{
			get
			{
				return _workingProcessComment;
			}
			set
			{
				_workingProcessComment = value;
			}
		}


		public SqlDateTime ProjectActualEndDate
		{
			get
			{
				return _projectActualEndDate;
			}
			set
			{
				_projectActualEndDate = value;
			}
		}


		public SqlDecimal ProjectIncome
		{
			get
			{
				return _projectIncome;
			}
			set
			{
				_projectIncome = value;
			}
		}


		public SqlDecimal ProjectExpense
		{
			get
			{
				return _projectExpense;
			}
			set
			{
				_projectExpense = value;
			}
		}


		public SqlDecimal ProjectGrossProfit
		{
			get
			{
				return _projectGrossProfit;
			}
			set
			{
				_projectGrossProfit = value;
			}
		}


		public SqlString Rating
		{
			get
			{
				return _rating;
			}
			set
			{
				_rating = value;
			}
		}


		public SqlString ProjectReport
		{
			get
			{
				return _projectReport;
			}
			set
			{
				_projectReport = value;
			}
		}


		public SqlDecimal TotalBudget
		{
			get
			{
				return _totalBudget;
			}
			set
			{
				_totalBudget = value;
			}
		}


		public SqlDecimal TotalEstimateBudget
		{
			get
			{
				return _totalEstimateBudget;
			}
			set
			{
				_totalEstimateBudget = value;
			}
		}


		public SqlString ExtraField1
		{
			get
			{
				return _extraField1;
			}
			set
			{
				_extraField1 = value;
			}
		}


		public SqlString ExtraField2
		{
			get
			{
				return _extraField2;
			}
			set
			{
				_extraField2 = value;
			}
		}


		public SqlString ExtraField3
		{
			get
			{
				return _extraField3;
			}
			set
			{
				_extraField3 = value;
			}
		}


		public SqlString ExtraField4
		{
			get
			{
				return _extraField4;
			}
			set
			{
				_extraField4 = value;
			}
		}


		public SqlString ExtraField5
		{
			get
			{
				return _extraField5;
			}
			set
			{
				_extraField5 = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
