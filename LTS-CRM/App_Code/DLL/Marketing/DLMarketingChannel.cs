 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'MarketingChannel'.
	/// </summary>
	public class DLMarketingChannel : DLDBInteractionBase
	{
		#region Class Member Declarations
			private SqlDateTime		_contractExpireDate, _createdDate, _updatedDate;
			private SqlDecimal		_amountOfFee;
			private SqlInt32		_iD, _companyID;
			private SqlString		_faxNo, _mobileNo, _weChat, _whatsup, _skype, _position, _firstName, _department, _directPhone, _email, _facebook, _extraField5, _extraField4, _createdBy, _status, _updatedBy, _linkedIn, _twitter, _extraField1, _extraField3, _extraField2, _surName, _country, _city, _postCode, _companyWebsite, _advertisingCost, _companyAddress2, _password, _username, _companyTelephone, _companyAddress1, _companyEmail, _firstDayOfCollabration, _comments, _companyName, _title, _contractedBy, _blackList, _paymentTerms, _rating, _howToUseMarketingChannel, _mainBusiness;
			private SqlBinary		_signedAgreedContractUpload;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public DLMarketingChannel()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>ContractedBy. May be SqlString.Null</LI>
		///		 <LI>CompanyName. May be SqlString.Null</LI>
		///		 <LI>CompanyWebsite. May be SqlString.Null</LI>
		///		 <LI>Username. May be SqlString.Null</LI>
		///		 <LI>Password. May be SqlString.Null</LI>
		///		 <LI>CompanyTelephone. May be SqlString.Null</LI>
		///		 <LI>CompanyEmail. May be SqlString.Null</LI>
		///		 <LI>CompanyAddress1. May be SqlString.Null</LI>
		///		 <LI>CompanyAddress2. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>PostCode. May be SqlString.Null</LI>
		///		 <LI>AdvertisingCost. May be SqlString.Null</LI>
		///		 <LI>AmountOfFee. May be SqlDecimal.Null</LI>
		///		 <LI>PaymentTerms. May be SqlString.Null</LI>
		///		 <LI>BlackList. May be SqlString.Null</LI>
		///		 <LI>Rating. May be SqlString.Null</LI>
		///		 <LI>MainBusiness. May be SqlString.Null</LI>
		///		 <LI>HowToUseMarketingChannel. May be SqlString.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>FirstDayOfCollabration. May be SqlString.Null</LI>
		///		 <LI>ContractExpireDate. May be SqlDateTime.Null</LI>
		///		 <LI>Status. May be SqlString.Null</LI>
		///		 <LI>SignedAgreedContractUpload. May be SqlBinary.Null</LI>
		///		 <LI>Title. May be SqlString.Null</LI>
		///		 <LI>SurName</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>Position. May be SqlString.Null</LI>
		///		 <LI>Department. May be SqlString.Null</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>DirectPhone. May be SqlString.Null</LI>
		///		 <LI>MobileNo. May be SqlString.Null</LI>
		///		 <LI>FaxNo. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Whatsup. May be SqlString.Null</LI>
		///		 <LI>Skype. May be SqlString.Null</LI>
		///		 <LI>Facebook. May be SqlString.Null</LI>
		///		 <LI>Twitter. May be SqlString.Null</LI>
		///		 <LI>LinkedIn. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingChannel_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyName", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyWebsite", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyWebsite));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUsername", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _username));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPassword", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _password));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyTelephone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyTelephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyAddress1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyAddress1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyAddress2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyAddress2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPostCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAdvertisingCost", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _advertisingCost));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcAmountOfFee", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _amountOfFee));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPaymentTerms", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _paymentTerms));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRating", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMainBusiness", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mainBusiness));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToUseMarketingChannel", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToUseMarketingChannel));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstDayOfCollabration", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstDayOfCollabration));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractExpireDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractExpireDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStatus", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _status));
				cmdToExecute.Parameters.Add(new SqlParameter("@biSignedAgreedContractUpload", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _signedAgreedContractUpload));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _title));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPosition", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _position));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDepartment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _department));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDirectPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _directPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWhatsup", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _whatsup));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSkype", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _skype));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFacebook", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _facebook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTwitter", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _twitter));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLinkedIn", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _linkedIn));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingChannel_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingChannel::Insert::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID. May be SqlInt32.Null</LI>
		///		 <LI>ContractedBy. May be SqlString.Null</LI>
		///		 <LI>CompanyName. May be SqlString.Null</LI>
		///		 <LI>CompanyWebsite. May be SqlString.Null</LI>
		///		 <LI>Username. May be SqlString.Null</LI>
		///		 <LI>Password. May be SqlString.Null</LI>
		///		 <LI>CompanyTelephone. May be SqlString.Null</LI>
		///		 <LI>CompanyEmail. May be SqlString.Null</LI>
		///		 <LI>CompanyAddress1. May be SqlString.Null</LI>
		///		 <LI>CompanyAddress2. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlString.Null</LI>
		///		 <LI>Country. May be SqlString.Null</LI>
		///		 <LI>PostCode. May be SqlString.Null</LI>
		///		 <LI>AdvertisingCost. May be SqlString.Null</LI>
		///		 <LI>AmountOfFee. May be SqlDecimal.Null</LI>
		///		 <LI>PaymentTerms. May be SqlString.Null</LI>
		///		 <LI>BlackList. May be SqlString.Null</LI>
		///		 <LI>Rating. May be SqlString.Null</LI>
		///		 <LI>MainBusiness. May be SqlString.Null</LI>
		///		 <LI>HowToUseMarketingChannel. May be SqlString.Null</LI>
		///		 <LI>Comments. May be SqlString.Null</LI>
		///		 <LI>FirstDayOfCollabration. May be SqlString.Null</LI>
		///		 <LI>ContractExpireDate. May be SqlDateTime.Null</LI>
		///		 <LI>Status. May be SqlString.Null</LI>
		///		 <LI>SignedAgreedContractUpload. May be SqlBinary.Null</LI>
		///		 <LI>Title. May be SqlString.Null</LI>
		///		 <LI>SurName</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>Position. May be SqlString.Null</LI>
		///		 <LI>Department. May be SqlString.Null</LI>
		///		 <LI>Email. May be SqlString.Null</LI>
		///		 <LI>DirectPhone. May be SqlString.Null</LI>
		///		 <LI>MobileNo. May be SqlString.Null</LI>
		///		 <LI>FaxNo. May be SqlString.Null</LI>
		///		 <LI>WeChat. May be SqlString.Null</LI>
		///		 <LI>Whatsup. May be SqlString.Null</LI>
		///		 <LI>Skype. May be SqlString.Null</LI>
		///		 <LI>Facebook. May be SqlString.Null</LI>
		///		 <LI>Twitter. May be SqlString.Null</LI>
		///		 <LI>LinkedIn. May be SqlString.Null</LI>
		///		 <LI>ExtraField1. May be SqlString.Null</LI>
		///		 <LI>ExtraField2. May be SqlString.Null</LI>
		///		 <LI>ExtraField3. May be SqlString.Null</LI>
		///		 <LI>ExtraField4. May be SqlString.Null</LI>
		///		 <LI>ExtraField5. May be SqlString.Null</LI>
		///		 <LI>CreatedBy. May be SqlString.Null</LI>
		///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdatedBy. May be SqlString.Null</LI>
		///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingChannel_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, _companyID));
				cmdToExecute.Parameters.Add(new SqlParameter("@sContractedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyName", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyWebsite", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyWebsite));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUsername", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _username));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPassword", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _password));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyTelephone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyTelephone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyEmail));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyAddress1", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyAddress1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyAddress2", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyAddress2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPostCode", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postCode));
				cmdToExecute.Parameters.Add(new SqlParameter("@sAdvertisingCost", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _advertisingCost));
				cmdToExecute.Parameters.Add(new SqlParameter("@dcAmountOfFee", SqlDbType.Decimal, 9, ParameterDirection.Input, true, 18, 2, "", DataRowVersion.Proposed, _amountOfFee));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPaymentTerms", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _paymentTerms));
				cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
				cmdToExecute.Parameters.Add(new SqlParameter("@sRating", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _rating));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMainBusiness", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mainBusiness));
				cmdToExecute.Parameters.Add(new SqlParameter("@sHowToUseMarketingChannel", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _howToUseMarketingChannel));
				cmdToExecute.Parameters.Add(new SqlParameter("@sComments", SqlDbType.NVarChar, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comments));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstDayOfCollabration", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _firstDayOfCollabration));
				cmdToExecute.Parameters.Add(new SqlParameter("@daContractExpireDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _contractExpireDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sStatus", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _status));
				cmdToExecute.Parameters.Add(new SqlParameter("@biSignedAgreedContractUpload", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _signedAgreedContractUpload));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTitle", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _title));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSurName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _surName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFirstName", SqlDbType.NVarChar, 150, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _firstName));
				cmdToExecute.Parameters.Add(new SqlParameter("@sPosition", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _position));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDepartment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _department));
				cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
				cmdToExecute.Parameters.Add(new SqlParameter("@sDirectPhone", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _directPhone));
				cmdToExecute.Parameters.Add(new SqlParameter("@sMobileNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobileNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFaxNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _faxNo));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWeChat", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _weChat));
				cmdToExecute.Parameters.Add(new SqlParameter("@sWhatsup", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _whatsup));
				cmdToExecute.Parameters.Add(new SqlParameter("@sSkype", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _skype));
				cmdToExecute.Parameters.Add(new SqlParameter("@sFacebook", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _facebook));
				cmdToExecute.Parameters.Add(new SqlParameter("@sTwitter", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _twitter));
				cmdToExecute.Parameters.Add(new SqlParameter("@sLinkedIn", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _linkedIn));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField1", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField1));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField2", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField2));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField3", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField3));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField4", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField4));
				cmdToExecute.Parameters.Add(new SqlParameter("@sExtraField5", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _extraField5));
				cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingChannel_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingChannel::Update::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingChannel_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingChannel_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingChannel::Delete::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>ID</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>ID</LI>
		///		 <LI>CompanyID</LI>
		///		 <LI>ContractedBy</LI>
		///		 <LI>CompanyName</LI>
		///		 <LI>CompanyWebsite</LI>
		///		 <LI>Username</LI>
		///		 <LI>Password</LI>
		///		 <LI>CompanyTelephone</LI>
		///		 <LI>CompanyEmail</LI>
		///		 <LI>CompanyAddress1</LI>
		///		 <LI>CompanyAddress2</LI>
		///		 <LI>City</LI>
		///		 <LI>Country</LI>
		///		 <LI>PostCode</LI>
		///		 <LI>AdvertisingCost</LI>
		///		 <LI>AmountOfFee</LI>
		///		 <LI>PaymentTerms</LI>
		///		 <LI>BlackList</LI>
		///		 <LI>Rating</LI>
		///		 <LI>MainBusiness</LI>
		///		 <LI>HowToUseMarketingChannel</LI>
		///		 <LI>Comments</LI>
		///		 <LI>FirstDayOfCollabration</LI>
		///		 <LI>ContractExpireDate</LI>
		///		 <LI>Status</LI>
		///		 <LI>SignedAgreedContractUpload</LI>
		///		 <LI>Title</LI>
		///		 <LI>SurName</LI>
		///		 <LI>FirstName</LI>
		///		 <LI>Position</LI>
		///		 <LI>Department</LI>
		///		 <LI>Email</LI>
		///		 <LI>DirectPhone</LI>
		///		 <LI>MobileNo</LI>
		///		 <LI>FaxNo</LI>
		///		 <LI>WeChat</LI>
		///		 <LI>Whatsup</LI>
		///		 <LI>Skype</LI>
		///		 <LI>Facebook</LI>
		///		 <LI>Twitter</LI>
		///		 <LI>LinkedIn</LI>
		///		 <LI>ExtraField1</LI>
		///		 <LI>ExtraField2</LI>
		///		 <LI>ExtraField3</LI>
		///		 <LI>ExtraField4</LI>
		///		 <LI>ExtraField5</LI>
		///		 <LI>CreatedBy</LI>
		///		 <LI>CreatedDate</LI>
		///		 <LI>UpdatedBy</LI>
		///		 <LI>UpdatedDate</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingChannel_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("MarketingChannel");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingChannel_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_iD = (Int32)toReturn.Rows[0]["ID"];
					_companyID = toReturn.Rows[0]["CompanyID"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["CompanyID"];
					_contractedBy = toReturn.Rows[0]["ContractedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ContractedBy"];
					_companyName = toReturn.Rows[0]["CompanyName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CompanyName"];
					_companyWebsite = toReturn.Rows[0]["CompanyWebsite"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CompanyWebsite"];
					_username = toReturn.Rows[0]["Username"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Username"];
					_password = toReturn.Rows[0]["Password"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Password"];
					_companyTelephone = toReturn.Rows[0]["CompanyTelephone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CompanyTelephone"];
					_companyEmail = toReturn.Rows[0]["CompanyEmail"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CompanyEmail"];
					_companyAddress1 = toReturn.Rows[0]["CompanyAddress1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CompanyAddress1"];
					_companyAddress2 = toReturn.Rows[0]["CompanyAddress2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CompanyAddress2"];
					_city = toReturn.Rows[0]["City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["City"];
					_country = toReturn.Rows[0]["Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Country"];
					_postCode = toReturn.Rows[0]["PostCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PostCode"];
					_advertisingCost = toReturn.Rows[0]["AdvertisingCost"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["AdvertisingCost"];
					_amountOfFee = toReturn.Rows[0]["AmountOfFee"] == System.DBNull.Value ? SqlDecimal.Null : (Decimal)toReturn.Rows[0]["AmountOfFee"];
					_paymentTerms = toReturn.Rows[0]["PaymentTerms"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PaymentTerms"];
					_blackList = toReturn.Rows[0]["BlackList"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["BlackList"];
					_rating = toReturn.Rows[0]["Rating"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Rating"];
					_mainBusiness = toReturn.Rows[0]["MainBusiness"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["MainBusiness"];
					_howToUseMarketingChannel = toReturn.Rows[0]["HowToUseMarketingChannel"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["HowToUseMarketingChannel"];
					_comments = toReturn.Rows[0]["Comments"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Comments"];
					_firstDayOfCollabration = toReturn.Rows[0]["FirstDayOfCollabration"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FirstDayOfCollabration"];
					_contractExpireDate = toReturn.Rows[0]["ContractExpireDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ContractExpireDate"];
					_status = toReturn.Rows[0]["Status"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Status"];
					_signedAgreedContractUpload = toReturn.Rows[0]["SignedAgreedContractUpload"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["SignedAgreedContractUpload"];
					_title = toReturn.Rows[0]["Title"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Title"];
					_surName = (string)toReturn.Rows[0]["SurName"];
					_firstName = (string)toReturn.Rows[0]["FirstName"];
					_position = toReturn.Rows[0]["Position"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Position"];
					_department = toReturn.Rows[0]["Department"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Department"];
					_email = toReturn.Rows[0]["Email"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Email"];
					_directPhone = toReturn.Rows[0]["DirectPhone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["DirectPhone"];
					_mobileNo = toReturn.Rows[0]["MobileNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["MobileNo"];
					_faxNo = toReturn.Rows[0]["FaxNo"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["FaxNo"];
					_weChat = toReturn.Rows[0]["WeChat"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["WeChat"];
					_whatsup = toReturn.Rows[0]["Whatsup"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Whatsup"];
					_skype = toReturn.Rows[0]["Skype"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Skype"];
					_facebook = toReturn.Rows[0]["Facebook"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Facebook"];
					_twitter = toReturn.Rows[0]["Twitter"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Twitter"];
					_linkedIn = toReturn.Rows[0]["LinkedIn"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["LinkedIn"];
					_extraField1 = toReturn.Rows[0]["ExtraField1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField1"];
					_extraField2 = toReturn.Rows[0]["ExtraField2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField2"];
					_extraField3 = toReturn.Rows[0]["ExtraField3"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField3"];
					_extraField4 = toReturn.Rows[0]["ExtraField4"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField4"];
					_extraField5 = toReturn.Rows[0]["ExtraField5"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["ExtraField5"];
					_createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
					_createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
					_updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
					_updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingChannel::SelectOne::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[sp_MarketingChannel_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("MarketingChannel");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'sp_MarketingChannel_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("DLMarketingChannel::SelectAll::Error occured.", ex);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 ID
		{
			get
			{
				return _iD;
			}
			set
			{
				SqlInt32 iDTmp = (SqlInt32)value;
				if(iDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
				}
				_iD = value;
			}
		}


		public SqlInt32 CompanyID
		{
			get
			{
				return _companyID;
			}
			set
			{
				_companyID = value;
			}
		}


		public SqlString ContractedBy
		{
			get
			{
				return _contractedBy;
			}
			set
			{
				_contractedBy = value;
			}
		}


		public SqlString CompanyName
		{
			get
			{
				return _companyName;
			}
			set
			{
				_companyName = value;
			}
		}


		public SqlString CompanyWebsite
		{
			get
			{
				return _companyWebsite;
			}
			set
			{
				_companyWebsite = value;
			}
		}


		public SqlString Username
		{
			get
			{
				return _username;
			}
			set
			{
				_username = value;
			}
		}


		public SqlString Password
		{
			get
			{
				return _password;
			}
			set
			{
				_password = value;
			}
		}


		public SqlString CompanyTelephone
		{
			get
			{
				return _companyTelephone;
			}
			set
			{
				_companyTelephone = value;
			}
		}


		public SqlString CompanyEmail
		{
			get
			{
				return _companyEmail;
			}
			set
			{
				_companyEmail = value;
			}
		}


		public SqlString CompanyAddress1
		{
			get
			{
				return _companyAddress1;
			}
			set
			{
				_companyAddress1 = value;
			}
		}


		public SqlString CompanyAddress2
		{
			get
			{
				return _companyAddress2;
			}
			set
			{
				_companyAddress2 = value;
			}
		}


		public SqlString City
		{
			get
			{
				return _city;
			}
			set
			{
				_city = value;
			}
		}


		public SqlString Country
		{
			get
			{
				return _country;
			}
			set
			{
				_country = value;
			}
		}


		public SqlString PostCode
		{
			get
			{
				return _postCode;
			}
			set
			{
				_postCode = value;
			}
		}


		public SqlString AdvertisingCost
		{
			get
			{
				return _advertisingCost;
			}
			set
			{
				_advertisingCost = value;
			}
		}


		public SqlDecimal AmountOfFee
		{
			get
			{
				return _amountOfFee;
			}
			set
			{
				_amountOfFee = value;
			}
		}


		public SqlString PaymentTerms
		{
			get
			{
				return _paymentTerms;
			}
			set
			{
				_paymentTerms = value;
			}
		}


		public SqlString BlackList
		{
			get
			{
				return _blackList;
			}
			set
			{
				_blackList = value;
			}
		}


		public SqlString Rating
		{
			get
			{
				return _rating;
			}
			set
			{
				_rating = value;
			}
		}


		public SqlString MainBusiness
		{
			get
			{
				return _mainBusiness;
			}
			set
			{
				_mainBusiness = value;
			}
		}


		public SqlString HowToUseMarketingChannel
		{
			get
			{
				return _howToUseMarketingChannel;
			}
			set
			{
				_howToUseMarketingChannel = value;
			}
		}


		public SqlString Comments
		{
			get
			{
				return _comments;
			}
			set
			{
				_comments = value;
			}
		}


		public SqlString FirstDayOfCollabration
		{
			get
			{
				return _firstDayOfCollabration;
			}
			set
			{
				_firstDayOfCollabration = value;
			}
		}


		public SqlDateTime ContractExpireDate
		{
			get
			{
				return _contractExpireDate;
			}
			set
			{
				_contractExpireDate = value;
			}
		}


		public SqlString Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}


		public SqlBinary SignedAgreedContractUpload
		{
			get
			{
				return _signedAgreedContractUpload;
			}
			set
			{
				_signedAgreedContractUpload = value;
			}
		}


		public SqlString Title
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
			}
		}


		public SqlString SurName
		{
			get
			{
				return _surName;
			}
			set
			{
				SqlString surNameTmp = (SqlString)value;
				if(surNameTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("SurName", "SurName can't be NULL");
				}
				_surName = value;
			}
		}


		public SqlString FirstName
		{
			get
			{
				return _firstName;
			}
			set
			{
				SqlString firstNameTmp = (SqlString)value;
				if(firstNameTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("FirstName", "FirstName can't be NULL");
				}
				_firstName = value;
			}
		}


		public SqlString Position
		{
			get
			{
				return _position;
			}
			set
			{
				_position = value;
			}
		}


		public SqlString Department
		{
			get
			{
				return _department;
			}
			set
			{
				_department = value;
			}
		}


		public SqlString Email
		{
			get
			{
				return _email;
			}
			set
			{
				_email = value;
			}
		}


		public SqlString DirectPhone
		{
			get
			{
				return _directPhone;
			}
			set
			{
				_directPhone = value;
			}
		}


		public SqlString MobileNo
		{
			get
			{
				return _mobileNo;
			}
			set
			{
				_mobileNo = value;
			}
		}


		public SqlString FaxNo
		{
			get
			{
				return _faxNo;
			}
			set
			{
				_faxNo = value;
			}
		}


		public SqlString WeChat
		{
			get
			{
				return _weChat;
			}
			set
			{
				_weChat = value;
			}
		}


		public SqlString Whatsup
		{
			get
			{
				return _whatsup;
			}
			set
			{
				_whatsup = value;
			}
		}


		public SqlString Skype
		{
			get
			{
				return _skype;
			}
			set
			{
				_skype = value;
			}
		}


		public SqlString Facebook
		{
			get
			{
				return _facebook;
			}
			set
			{
				_facebook = value;
			}
		}


		public SqlString Twitter
		{
			get
			{
				return _twitter;
			}
			set
			{
				_twitter = value;
			}
		}


		public SqlString LinkedIn
		{
			get
			{
				return _linkedIn;
			}
			set
			{
				_linkedIn = value;
			}
		}


		public SqlString ExtraField1
		{
			get
			{
				return _extraField1;
			}
			set
			{
				_extraField1 = value;
			}
		}


		public SqlString ExtraField2
		{
			get
			{
				return _extraField2;
			}
			set
			{
				_extraField2 = value;
			}
		}


		public SqlString ExtraField3
		{
			get
			{
				return _extraField3;
			}
			set
			{
				_extraField3 = value;
			}
		}


		public SqlString ExtraField4
		{
			get
			{
				return _extraField4;
			}
			set
			{
				_extraField4 = value;
			}
		}


		public SqlString ExtraField5
		{
			get
			{
				return _extraField5;
			}
			set
			{
				_extraField5 = value;
			}
		}


		public SqlString CreatedBy
		{
			get
			{
				return _createdBy;
			}
			set
			{
				_createdBy = value;
			}
		}


		public SqlDateTime CreatedDate
		{
			get
			{
				return _createdDate;
			}
			set
			{
				_createdDate = value;
			}
		}


		public SqlString UpdatedBy
		{
			get
			{
				return _updatedBy;
			}
			set
			{
				_updatedBy = value;
			}
		}


		public SqlDateTime UpdatedDate
		{
			get
			{
				return _updatedDate;
			}
			set
			{
				_updatedDate = value;
			}
		}
		#endregion
	}
}
