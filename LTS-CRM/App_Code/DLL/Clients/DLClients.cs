 
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace LTSCRM_DL
{
    /// <summary>
    /// Purpose: Data Access class for the table 'Clients'.
    /// </summary>
    public class DLClients : DLDBInteractionBase
    {
        #region Class Member Declarations
			private SqlDateTime		_clientDate, _createdDate, _updatedDate;
        private SqlDouble _rating;
			private SqlInt32		_iD, _companyID, _ClientContactID;
			private SqlString		_paymentTerms, _blackList, _postalCode, _country, _salesStaff, _updatedBy, _address2, _comment, _createdBy, _telephone, _fax, _website, _directOrAgenet, _companyName, _companyOrInd, _city, _state, _email, _address1;
        private SqlBinary _fileData;
        #endregion


        /// <summary>
        /// Purpose: Class constructor.
        /// </summary>
        public DLClients()
        {
            // Nothing for now.
        }


        /// <summary>
        /// Purpose: Insert method. This method will insert one new row into the database.
        /// </summary>
        /// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
        /// <remarks>
        /// Properties needed for this method: 
        /// <UL>
        ///		 <LI>ClientDate. May be SqlDateTime.Null</LI>
        ///		 <LI>SalesStaff. May be SqlString.Null</LI>
        ///		 <LI>CompanyOrInd</LI>
        ///		 <LI>DirectOrAgenet. May be SqlString.Null</LI>
        ///		 <LI>CompanyName. May be SqlString.Null</LI>
        ///		 <LI>Website. May be SqlString.Null</LI>
        ///		 <LI>Telephone. May be SqlString.Null</LI>
        ///		 <LI>Fax</LI>
        ///		 <LI>Email. May be SqlString.Null</LI>
        ///		 <LI>Address1. May be SqlString.Null</LI>
        ///		 <LI>Address2. May be SqlString.Null</LI>
		///		 <LI>Postcode. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlInt32.Null</LI>
		///		 <LI>State. May be SqlInt32.Null</LI>
		///		 <LI>Country. May be SqlInt32.Null</LI>
        ///		 <LI>City. May be SqlString.Null</LI>
        ///		 <LI>State. May be SqlString.Null</LI>
        ///		 <LI>Country. May be SqlString.Null</LI>
        ///		 <LI>PostalCode. May be SqlString.Null</LI>
        ///		 <LI>PaymentTerms. May be SqlString.Null</LI>
        ///		 <LI>BlackList. May be SqlString.Null</LI>
        ///		 <LI>Rating. May be SqlDouble.Null</LI>
        ///		 <LI>Comment. May be SqlString.Null</LI>
        ///		 <LI>FileData. May be SqlBinary.Null</LI>
        ///		 <LI>CreatedBy. May be SqlString.Null</LI>
        ///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
        ///		 <LI>UpdatedBy. May be SqlString.Null</LI>
        ///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
        /// </UL>
        /// Properties set after a succesful call of this method: 
        /// <UL>
        ///		 <LI>ID</LI>
        ///		 <LI>ErrorCode</LI>
        /// </UL>
        /// </remarks>
        public override bool Insert()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_Clients_Insert]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _companyID));
                cmdToExecute.Parameters.Add(new SqlParameter("@daClientDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _clientDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@sSalesStaff", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _salesStaff));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyOrInd", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _companyOrInd));
                cmdToExecute.Parameters.Add(new SqlParameter("@sDirectOrAgenet", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _directOrAgenet));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyName", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyName));
                cmdToExecute.Parameters.Add(new SqlParameter("@sWebsite", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _website));
                cmdToExecute.Parameters.Add(new SqlParameter("@sTelephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _telephone));
                cmdToExecute.Parameters.Add(new SqlParameter("@sFax", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _fax));
                cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
                cmdToExecute.Parameters.Add(new SqlParameter("@sAddress1", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address1));
                cmdToExecute.Parameters.Add(new SqlParameter("@sAddress2", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address2));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
                cmdToExecute.Parameters.Add(new SqlParameter("@sState", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _state));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
                cmdToExecute.Parameters.Add(new SqlParameter("@sPostalCode", SqlDbType.NVarChar, 6, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postalCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@sPaymentTerms", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _paymentTerms));
                cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 200, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
                cmdToExecute.Parameters.Add(new SqlParameter("@fRating", SqlDbType.Float, 8, ParameterDirection.Input, true, 38, 0, "", DataRowVersion.Proposed, _rating));
                cmdToExecute.Parameters.Add(new SqlParameter("@sComment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comment));
                cmdToExecute.Parameters.Add(new SqlParameter("@biFileData", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _fileData));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _iD));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

			 
                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
					 
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _iD = (SqlInt32)cmdToExecute.Parameters["@iID"].Value;
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_Clients_Insert' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLClients::Insert::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }


        /// <summary>
        /// Purpose: Update method. This method will Update one existing row in the database.
        /// </summary>
        /// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
        /// <remarks>
        /// Properties needed for this method: 
        /// <UL>
        ///		 <LI>ID</LI>
        ///		 <LI>ClientDate. May be SqlDateTime.Null</LI>
        ///		 <LI>SalesStaff. May be SqlString.Null</LI>
        ///		 <LI>CompanyOrInd</LI>
        ///		 <LI>DirectOrAgenet. May be SqlString.Null</LI>
        ///		 <LI>CompanyName. May be SqlString.Null</LI>
        ///		 <LI>Website. May be SqlString.Null</LI>
        ///		 <LI>Telephone. May be SqlString.Null</LI>
        ///		 <LI>Fax</LI>
        ///		 <LI>Email. May be SqlString.Null</LI>
        ///		 <LI>Address1. May be SqlString.Null</LI>
        ///		 <LI>Address2. May be SqlString.Null</LI>
		///		 <LI>Postcode. May be SqlString.Null</LI>
		///		 <LI>City. May be SqlInt32.Null</LI>
		///		 <LI>State. May be SqlInt32.Null</LI>
		///		 <LI>Country. May be SqlInt32.Null</LI>
        ///		 <LI>City. May be SqlString.Null</LI>
        ///		 <LI>State. May be SqlString.Null</LI>
        ///		 <LI>Country. May be SqlString.Null</LI>
        ///		 <LI>PostalCode. May be SqlString.Null</LI>
        ///		 <LI>PaymentTerms. May be SqlString.Null</LI>
        ///		 <LI>BlackList. May be SqlString.Null</LI>
        ///		 <LI>Rating. May be SqlDouble.Null</LI>
        ///		 <LI>Comment. May be SqlString.Null</LI>
        ///		 <LI>FileData. May be SqlBinary.Null</LI>
        ///		 <LI>CreatedBy. May be SqlString.Null</LI>
        ///		 <LI>CreatedDate. May be SqlDateTime.Null</LI>
        ///		 <LI>UpdatedBy. May be SqlString.Null</LI>
        ///		 <LI>UpdatedDate. May be SqlDateTime.Null</LI>
        /// </UL>
        /// Properties set after a succesful call of this method: 
        /// <UL>
        ///		 <LI>ErrorCode</LI>
        /// </UL>
        /// </remarks>
        public override bool Update()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_Clients_Update]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
				cmdToExecute.Parameters.Add(new SqlParameter("@iCompanyID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _companyID));
                cmdToExecute.Parameters.Add(new SqlParameter("@daClientDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _clientDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@sSalesStaff", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _salesStaff));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyOrInd", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _companyOrInd));
                cmdToExecute.Parameters.Add(new SqlParameter("@sDirectOrAgenet", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _directOrAgenet));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCompanyName", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _companyName));
                cmdToExecute.Parameters.Add(new SqlParameter("@sWebsite", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _website));
                cmdToExecute.Parameters.Add(new SqlParameter("@sTelephone", SqlDbType.NVarChar, 15, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _telephone));
                cmdToExecute.Parameters.Add(new SqlParameter("@sFax", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _fax));
                cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
                cmdToExecute.Parameters.Add(new SqlParameter("@sAddress1", SqlDbType.NVarChar, 250, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address1));
                cmdToExecute.Parameters.Add(new SqlParameter("@sAddress2", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _address2));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCity", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _city));
                cmdToExecute.Parameters.Add(new SqlParameter("@sState", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _state));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCountry", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _country));
                cmdToExecute.Parameters.Add(new SqlParameter("@sPostalCode", SqlDbType.NVarChar, 6, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _postalCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@sPaymentTerms", SqlDbType.NVarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _paymentTerms));
                cmdToExecute.Parameters.Add(new SqlParameter("@sBlackList", SqlDbType.NVarChar, 200, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _blackList));
                cmdToExecute.Parameters.Add(new SqlParameter("@fRating", SqlDbType.Float, 8, ParameterDirection.Input, true, 38, 0, "", DataRowVersion.Proposed, _rating));
                cmdToExecute.Parameters.Add(new SqlParameter("@sComment", SqlDbType.NVarChar, 150, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _comment));
                cmdToExecute.Parameters.Add(new SqlParameter("@biFileData", SqlDbType.VarBinary, -1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _fileData));
                cmdToExecute.Parameters.Add(new SqlParameter("@sCreatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@daCreatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createdDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@sUpdatedBy", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@daUpdatedDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updatedDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_Clients_Update' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
			catch(Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLClients::Update::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }


        /// <summary>
        /// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
        /// </summary>
        /// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
        /// <remarks>
        /// Properties needed for this method: 
        /// <UL>
        ///		 <LI>ID</LI>
        /// </UL>
        /// Properties set after a succesful call of this method: 
        /// <UL>
        ///		 <LI>ErrorCode</LI>
        /// </UL>
        /// </remarks>
        public override bool Delete()   
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_Clients_Delete]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_Clients_Delete' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
			catch(Exception ex) 
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLClients::Delete::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }


        /// <summary>
        /// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
        /// </summary>
        /// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
        /// <remarks>
        /// Properties needed for this method: 
        /// <UL>
        ///		 <LI>ID</LI>
        /// </UL>
        /// Properties set after a succesful call of this method: 
        /// <UL>
        ///		 <LI>ErrorCode</LI>
        ///		 <LI>ID</LI>
        ///		 <LI>ClientDate</LI>
        ///		 <LI>SalesStaff</LI>
        ///		 <LI>CompanyOrInd</LI>
        ///		 <LI>DirectOrAgenet</LI>
        ///		 <LI>CompanyName</LI>
        ///		 <LI>Website</LI>
        ///		 <LI>Telephone</LI>
        ///		 <LI>Fax</LI>
        ///		 <LI>Email</LI>
        ///		 <LI>Address1</LI>
        ///		 <LI>Address2</LI>
		///		 <LI>Postcode</LI>
        ///		 <LI>City</LI>
        ///		 <LI>State</LI>
        ///		 <LI>Country</LI>
        ///		 <LI>PostalCode</LI>
        ///		 <LI>PaymentTerms</LI>
        ///		 <LI>BlackList</LI>
        ///		 <LI>Rating</LI>
        ///		 <LI>Comment</LI>
        ///		 <LI>FileData</LI>
        ///		 <LI>CreatedBy</LI>
        ///		 <LI>CreatedDate</LI>
        ///		 <LI>UpdatedBy</LI>
        ///		 <LI>UpdatedDate</LI>
        /// </UL>
        /// Will fill all properties corresponding with a field in the table with the value of the row selected.
        /// </remarks>
        public override DataTable SelectOne()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_Clients_SelectOne]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("Clients");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_Clients_SelectOne' reported the ErrorCode: " + _errorCode);
                }

                if (toReturn.Rows.Count > 0)
                {
                    _iD = (Int32)toReturn.Rows[0]["ID"];
					_companyID = (Int32)toReturn.Rows[0]["CompanyID"];
                  //  _ClientContactID = (Int32)toReturn.Rows[0]["ClientContactID"];
                    _clientDate = toReturn.Rows[0]["ClientDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["ClientDate"];
                    _salesStaff = toReturn.Rows[0]["SalesStaff"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["SalesStaff"];
                    _companyOrInd = (string)toReturn.Rows[0]["CompanyOrInd"];
                    _directOrAgenet = toReturn.Rows[0]["DirectOrAgenet"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["DirectOrAgenet"];
                    _companyName = toReturn.Rows[0]["CompanyName"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CompanyName"];
                    _website = toReturn.Rows[0]["Website"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Website"];
                    _telephone = toReturn.Rows[0]["Telephone"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Telephone"];
                    _fax = (string)toReturn.Rows[0]["Fax"];
                    _email = toReturn.Rows[0]["Email"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Email"];
                    _address1 = toReturn.Rows[0]["Address1"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address1"];
                    _address2 = toReturn.Rows[0]["Address2"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Address2"];
                    _city = toReturn.Rows[0]["City"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["City"];
                    _state = toReturn.Rows[0]["State"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["State"];
                    _country = toReturn.Rows[0]["Country"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Country"];
                    _postalCode = toReturn.Rows[0]["PostalCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PostalCode"];
                    _paymentTerms = toReturn.Rows[0]["PaymentTerms"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["PaymentTerms"];
                    _blackList = toReturn.Rows[0]["BlackList"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["BlackList"];
                    _rating = toReturn.Rows[0]["Rating"] == System.DBNull.Value ? SqlDouble.Null : (double)toReturn.Rows[0]["Rating"];
                    _comment = toReturn.Rows[0]["Comment"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["Comment"];
                    _fileData = toReturn.Rows[0]["FileData"] == System.DBNull.Value ? SqlBinary.Null : (byte[])toReturn.Rows[0]["FileData"];
                    _createdBy = toReturn.Rows[0]["CreatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["CreatedBy"];
                    _createdDate = toReturn.Rows[0]["CreatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["CreatedDate"];
                    _updatedBy = toReturn.Rows[0]["UpdatedBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["UpdatedBy"];
                    _updatedDate = toReturn.Rows[0]["UpdatedDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["UpdatedDate"];
                }
                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLClients::SelectOne::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }


        /// <summary>
        /// Purpose: SelectAll method. This method will Select all rows from the table.
        /// </summary>
        /// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
        /// <remarks>
        /// Properties set after a succesful call of this method: 
        /// <UL>
        ///		 <LI>ErrorCode</LI>
        /// </UL>
        /// </remarks>
        public override DataTable SelectAll()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_Clients_SelectAll]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("Clients");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_Clients_SelectAll' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLClients::SelectAll::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable SelectOneByEmail()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[sp_Clients_SelectOneByEmail]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("Clients");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@iID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _iD));
                cmdToExecute.Parameters.Add(new SqlParameter("@sEmail", SqlDbType.NVarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email));
                cmdToExecute.Parameters.Add(new SqlParameter("@iErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@iErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'sp_Clients_SelectAll' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("DLClients::SelectAll::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        #region Class Property Declarations
        public SqlInt32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                SqlInt32 iDTmp = (SqlInt32)value;
                if (iDTmp.IsNull)
                {
                    throw new ArgumentOutOfRangeException("ID", "ID can't be NULL");
                }
                _iD = value;
            }
        }

public SqlInt32 ClientContactID
        {
			get
			{
				return _ClientContactID;
			}
			set
			{

                _ClientContactID = value;
			}
		}
		public SqlInt32 CompanyID
		{
			get
			{
				return _companyID;
			}
			set
			{
				SqlInt32 companyIDTmp = (SqlInt32)value;
				if(companyIDTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("CompanyID", "CompanyID can't be NULL");
				}
				_companyID = value;
			}
		}
        public SqlDateTime ClientDate
        {
            get
            {
                return _clientDate;
            }
            set
            {
                _clientDate = value;
            }
        }


        public SqlString SalesStaff
        {
            get
            {
                return _salesStaff;
            }
            set
            {
                _salesStaff = value;
            }
        }


        public SqlString CompanyOrInd
        {
            get
            {
                return _companyOrInd;
            }
            set
            {
                SqlString companyOrIndTmp = (SqlString)value;
                if (companyOrIndTmp.IsNull)
                {
                    throw new ArgumentOutOfRangeException("CompanyOrInd", "CompanyOrInd can't be NULL");
                }
                _companyOrInd = value;
            }
        }


        public SqlString DirectOrAgenet
        {
            get
            {
                return _directOrAgenet;
            }
            set
            {
                _directOrAgenet = value;
            }
        }


        public SqlString CompanyName
        {
            get
            {
                return _companyName;
            }
            set
            {
                _companyName = value;
            }
        }


        public SqlString Website
        {
            get
            {
                return _website;
            }
            set
            {
                _website = value;
            }
        }


        public SqlString Telephone
        {
            get
            {
                return _telephone;
            }
            set
            {
                _telephone = value;
            }
        }


        public SqlString Fax
        {
            get
            {
                return _fax;
            }
            set
            {
                SqlString faxTmp = (SqlString)value;
                if (faxTmp.IsNull)
                {
                    throw new ArgumentOutOfRangeException("Fax", "Fax can't be NULL");
                }
                _fax = value;
            }
        }


        public SqlString Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }


        public SqlString Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }


        public SqlString Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }




        public SqlString City
		{
			get
			{
                return _city;
            }
            set
            {
                _city = value;
            }
        }


        public SqlString State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }


        public SqlString Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value;
            }
        }


        public SqlString PostalCode
        {
            get
            {
                return _postalCode;
            }
            set
            {
                _postalCode = value;
            }
        }


        public SqlString PaymentTerms
        {
            get
            {
                return _paymentTerms;
            }
            set
            {
                _paymentTerms = value;
            }
        }


        public SqlString BlackList
        {
            get
            {
                return _blackList;
            }
            set
            {
                _blackList = value;
            }
        }


        public SqlDouble Rating
        {
            get
            {
                return _rating;
            }
            set
            {
                _rating = value;
            }
        }


        public SqlString Comment
        {
            get
            {
                return _comment;
            }
            set
            {
                _comment = value;
            }
        }


        public SqlBinary FileData
        {
            get
            {
                return _fileData;
            }
            set
            {
                _fileData = value;
            }
        }


        public SqlString CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }


        public SqlDateTime CreatedDate
        {
            get
            {
                return _createdDate;
            }
            set
            {
                _createdDate = value;
            }
        }


        public SqlString UpdatedBy
        {
            get
            {
                return _updatedBy;
            }
            set
            {
                _updatedBy = value;
            }
        }


        public SqlDateTime UpdatedDate
        {
            get
            {
                return _updatedDate;
            }
            set
            {
                _updatedDate = value;
            }
        }
        #endregion
    }
}
