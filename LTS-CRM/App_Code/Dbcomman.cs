﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;

namespace LTS_CRM.App_Code
{
    static class Dbcomman
    {
        public static string CEncypt(string text)
        {
            EncryptionHelper h = new EncryptionHelper();
            return h.Encrypt(text);
        }
        public static string CDecypt(string text)
        {
            EncryptionHelper h = new EncryptionHelper();
            return h.Decrypt(text);
        }
    }
}