﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="HolidayAddEdit.aspx.cs" Inherits="LTS_CRM.HR.Leave.HolidayAddEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

           
            <ol class="breadcrumb">
                 <li>Leave</li>
                <li>Holiday</li>
            </ol>
            

          
        </div>
          
        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
           
            <header>
                <h2>Holiday Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
           
            </div>
          
            <div class="widget-body">


                <div class="well">
          
                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                        <tbody>
                            <tr>
                                <td style="width: 10%;">Office</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="cboCompanyID" runat="server" ErrorMessage="Company Name Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>  
                            <tr>
                                <td style="width: 10%;">Holiday Date</td>
                                <td style="width: 10%">

                                    <div class="col-lg-8">
                                      
										<label class="class="input""> 
											<input type="text" name="txtHolidayDate" placeholder="Holiday Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" id="txtHolidayDate" style="height:30px" Font-Bold="true" ForeColor="Red">
										<i class="icon-append fa fa-calendar"></i>

										</label>
									
										 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtHolidayDate" runat="server" ErrorMessage="Holiday date Required" Display="Dynamic" Font-Bold="true" ForeColor="Red" ></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>

                            </tr>     
                            <tr>
                                <td style="width: 10%;">Holiday Name</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtHolidayName" runat="server" class="form-control"  placeholder="Leave Name" MaxLength="50" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="req1" ControlToValidate="txtHolidayName" runat="server" ErrorMessage="Holiday Name Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                            
                          
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%"></td>
                </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%">
                        <div class="col-lg-4">
                            <asp:Button ID="btnSave" class="btn bg-color-blueDark txt-color-white" Style="width: 100px" runat="server" Text="Save" OnClick="btnSave_Click" />
                            <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                        </div>
                    </td>
                </tr>



                </tbody>
                        </table>
                 

            </div>










            <!-- end widget content -->

        </div>
    <!-- end widget div -->

    </div>

    </div>
     <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
   
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                      <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                  <p><asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
     
            </div>
          </div>
</asp:Content>
