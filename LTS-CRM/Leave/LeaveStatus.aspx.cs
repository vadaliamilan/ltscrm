﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace LTS_CRM.Leave
{
    public partial class LeaveStatus : System.Web.UI.Page
    {
        string StrSql;
        System.Data.DataSet ds = new System.Data.DataSet();
        protected void Page_Load(object sender, System.EventArgs e)
        {
            //Response.Cache.SetCacheability(HttpCacheability.NoCache)
            string EMPID = null;
            Session.Add("EmpUserID", "EMP001");
            EMPID = SessionValue("EmpUserID");
            if (EMPID == null == true)
            {
                Session.Remove("EmployeeID");
                Page.Response.Redirect("~/Default.aspx");
                return;
            }
            else
            {
                //DB.UserInfo(EMPID)
            }

            if (Page.IsPostBack == false)
            {

            }
            FilterData();
        }
        private void FilterData()
        {
            string strWhere = null;
            strWhere = "";

            if (Pending.Checked == true)
            {
                strWhere = " Status='" + "Pending Approval" + "' ";
            }
            //If Approved.Checked = True Then
            //    If strWhere <> "" Then strWhere = strWhere & " OR "
            //    strWhere = strWhere & " Status='" & "Application Approved" & "' "
            //End If
            //If Rejected.Checked = True Then
            //    If strWhere <> "" Then strWhere = strWhere & " OR "
            //    strWhere = strWhere & " Status='" & "Application Rejected" & "' "
            //End If
            //If Canceled.Checked = True Then
            //    If strWhere <> "" Then strWhere = strWhere & " OR "
            //    strWhere = strWhere & " Status='" & "Application Canceled" & "' "
            //End If

            if (Approved.Checked == true)
            {
                if (!string.IsNullOrEmpty(strWhere))
                    strWhere = strWhere + " OR ";
                strWhere = strWhere + " lt.Status='" + "Application Approved" + "' OR Status='" + "Cancellation Approved" + "' ";

            }
            if (Rejected.Checked == true)
            {
                if (!string.IsNullOrEmpty(strWhere))
                    strWhere = strWhere + " OR ";
                strWhere = strWhere + " lt.Status='" + "Application Rejected" + "' OR Status='" + "Cancellation Rejected" + "' ";
            }
            if (Canceled.Checked == true)
            {
                if (!string.IsNullOrEmpty(strWhere))
                    strWhere = strWhere + " OR ";
                strWhere = strWhere + " lt.Status='" + "Cancellation Approved" + "' ";
            }
            //LeaveDateFrom >='" & Now.Date.ToString("yyyy/MM/dd") & "'
            StrSql = "Select Convert(Char(12),lt.ApplyDate,13) as [Apply Date],lt.LeaveNo as [Ref. No],lt.LeaveType as [Leave Type], convert(Char(12),lt.LeaveDateFrom,13) as [Date From],convert(char(12),lt.LeaveDateTo,13) as [Date To],lt.DayType as [Day Type],lt.Status as [Leave Status],lt.AppStatus [Current Status],lt.Reason,emp.EmployeeID as EmpName from LeaveTransaction lt Inner Join Employee EMP On EMP.EmployeeID=lt.EmployeeID where  lt.EmployeeID= '" + Session["EmpUserID"] + "' AND (lt.LeaveDateFrom >='" + DateTime.Now.ToString("yyyy/MM/dd") + "' OR lt.Status='Pending Approval')";
            if (Pending.Checked == true | Approved.Checked == true | Rejected.Checked == true | Canceled.Checked == true)
            {
                StrSql = StrSql + " AND (" + strWhere + " )";
            }

            StrSql = StrSql + "  order by lt.LeaveNo desc";
            LoadData(StrSql);
            return;
        }
        private void LoadData(string StrSql)
        {
            ds = CreatDsPage(StrSql);


            if (ds.Tables[0].Rows.Count == 0)
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();

                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;

                GridView1.Rows[0].Cells[0].Text = "No Records Found";
                GridView1.HeaderRow.Cells[7].Attributes.Add("style", "display:none;");
                GridView1.HeaderRow.Cells[8].Attributes.Add("style", "display:none;");
                GridView1.HeaderRow.Cells[9].Attributes.Add("style", "display:none;");


            }
            else
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();

                GridView1.HeaderRow.Cells[7].Attributes.Add("style", "display:none;");
                GridView1.HeaderRow.Cells[8].Attributes.Add("style", "display:none;");
                GridView1.HeaderRow.Cells[9].Attributes.Add("style", "display:none;");


                foreach (GridViewRow gvr in GridView1.Rows)
                {
                    gvr.Cells[7].Attributes.Add("style", "display:none;");
                    gvr.Cells[8].Attributes.Add("style", "display:none;");
                    gvr.Cells[9].Attributes.Add("style", "display:none;");
                }
            }


        }

        protected void GridView1_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            LoadData(StrSql);
        }

        protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            GridViewRow rw = e.Row;
            //e.Row.Cells(0).Width = Unit.Percentage(10) 'Apply Date"
            //e.Row.Cells(1).Width = Unit.Percentage(10) 'Ref. No
            //e.Row.Cells(2).Width = Unit.Percentage(25) 'leavetype
            //e.Row.Cells(3).Width = Unit.Percentage(10) 'date from
            //e.Row.Cells(4).Width = Unit.Percentage(10) 'date to
            //e.Row.Cells(5).Width = Unit.Percentage(10) 'day type
            //e.Row.Cells(6).Width = Unit.Percentage(25) 'leavestatus      

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].ForeColor = System.Drawing.Color.Blue;
                e.Row.Cells[1].Font.Bold = true;
                e.Row.Cells[1].Font.Underline = true;
                e.Row.Cells[1].Font.Underline = true;
                e.Row.Cells[1].Attributes.Add("onmouseover", "this.style.backgroundColor='silver';this.style.cursor='pointer';");

                //e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(sender, "Select$" & e.Row.RowIndex.ToString))
                e.Row.Attributes.Add("onclick", string.Format("setCurrentIndexOfGrid('{0}')", e.Row.RowIndex));

            }


        }

        //Protected Sub msgbtnok_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles msgbtnok.Click
        //    MSGBox.Visible = False
        //End Sub

        private DataSet CreatDsPage(string StrSql)
        {
            DataSet ds = default(DataSet);
            SqlDataAdapter da = default(SqlDataAdapter);
            SqlConnection ConnPage = default(SqlConnection);
            ConnPage = new SqlConnection();
            ConnPage.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            ConnPage.Open();
            da = new SqlDataAdapter(StrSql, ConnPage);
            ds = new DataSet();
            try
            {
                da.Fill(ds);
                ConnPage.Close();
                ConnPage.Dispose();
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                ConnPage.Close();
                ConnPage.Dispose();
            }

            return ds;

        }
        private void ExecuteSQLPage(string StrSql)
        {
            SqlCommand myCommand = default(SqlCommand);
            SqlConnection ConnPage = default(SqlConnection);
            ConnPage = new SqlConnection();

            ConnPage.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            ConnPage.Open();

            try
            {
                myCommand = new SqlCommand(StrSql, ConnPage);
                myCommand.CommandType = System.Data.CommandType.Text;
                myCommand.CommandText = StrSql;
                myCommand.ExecuteNonQuery();
                myCommand.Dispose();
                ConnPage.Close();
                ConnPage.Dispose();
            }
            catch (Exception ex)
            {
               

            }
            finally
            {
                ConnPage.Close();
                ConnPage.Dispose();

            }
        }
        private bool RecordExistPage(string StrSql)
        {
            SqlConnection ConnPage = default(SqlConnection);
            SqlCommand SqlComm = default(SqlCommand);
            SqlDataReader DataReader = default(SqlDataReader);
            ConnPage = new SqlConnection();
            ConnPage.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            ConnPage.Open();

            //Open the Database
            SqlComm = new SqlCommand(StrSql, ConnPage);
            //Execute the reader, Read data from Resoursfoodmeal
            try
            {
                DataReader = SqlComm.ExecuteReader();
                if (DataReader.Read())
                {
                    DataReader.Close();
                    return true;
                }
                else
                {
                    DataReader.Close();
                    return false;
                }
            }
            catch (Exception ex)
            {
                SqlComm.Dispose();
               
            }
            finally
            {
                SqlComm.Dispose();
                ConnPage.Close();
                ConnPage.Dispose();
            }
            return false;

        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }
    }
}