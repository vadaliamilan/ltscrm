﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.HR.Leave
{
    public partial class HolidayAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                LoadcboCompanyID();
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();

                Session.Add("UserName", "Nilesh");
                if (SessionValue("AutoID").ToString().Length > 0)
                {
                    String sql = "select * from [dbo].EmpHoliday where [ID] = @ID";

                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.Read())
                    {
                        cboCompanyID.SelectedValue = Reader["CompanyID"].ToString();
                        txtHolidayName.Text = Reader["HolidayName"].ToString();
                        String s;
                        s = Reader["Date"].ToString();
                        DateTime dt = new DateTime();
                        dt = Convert.ToDateTime(s);
                        txtHolidayDate.Value = dt.ToString("dd/MM/yyyy");
                    }

                    Reader.Close();
                    Connection.Close();
                }

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();
            if (checkDuplicate() == true && ID == "")
            {
                lblheader.Text = "Record Exist";
                lblmsg.Text = "Holiday exist.";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            }
            else
            {

                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Connection.Open();

                SqlCommand Command;

                String InsertSQL = "Insert into [dbo].[EmpHoliday] (CompanyID,[HolidayName],[Date],CreatedBy,CreatedDate) VALUES ( @CompanyID,@HolidayName,@Date,@CreatedBy,@CreatedDate ) ";

                String UpdateSQL = "Update [dbo].[EmpHoliday] set CompanyID=@CompanyID,[HolidayName]=@HolidayName,[Date]=@Date,UpdatedBy=@UpdatedBy,UpdatedDate=@UpdatedDate where [ID] = @ID ";


                if (String.IsNullOrEmpty(ID))
                {
                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                }
                else
                {
                    Command = new SqlCommand(UpdateSQL, Connection);
                    Command.Parameters.AddWithValue("@ID", ID);
                    Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);
                }
                Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
                Command.Parameters.AddWithValue("@HolidayName", txtHolidayName.Text);
                string res = txtHolidayDate.Value;
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);   
                Command.Parameters.AddWithValue("@Date", d);

                Command.ExecuteNonQuery();
                Connection.Close();
                Response.Redirect("Holiday.aspx");
            }
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LeaveType.aspx");
        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        protected Boolean checkDuplicate()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            string strsql = "select * from [dbo].[EmpHoliday] where Date=@Date And CompanyID=@CompanyID ";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            string res = txtHolidayDate.Value;
            DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //Console.WriteLine(d.ToString("MM/dd/yyyy"));

            Command.Parameters.AddWithValue("@Date", d);            
            Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);

            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }
        protected void btnOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        }

        protected void btnCloseModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalHide", "$('#myModal').hide();", true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModalAgainShow", "$('#myModal').modal();", true);
        }
        protected void LoadcboCompanyID()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            String sql = "select ID, CompanyName from [Company]  ";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCompanyID.DataTextField = "CompanyName";
            cboCompanyID.DataValueField = "ID";
            cboCompanyID.DataSource = Command.ExecuteReader();
            cboCompanyID.DataBind();

            Connection.Close();

        }
    }
}