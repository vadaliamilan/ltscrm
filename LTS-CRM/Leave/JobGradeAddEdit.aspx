﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="JobGradeAddEdit.aspx.cs" Inherits="LTS_CRM.HR.Leave.JobGradeAddEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

           
            <ol class="breadcrumb">
                 <li>Leave</li>
                <li>Job Grade</li>
            </ol>
            

          
        </div>
          
        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
           
            <header>
                <h2>Job Grade Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
           
            </div>
          
            <div class="widget-body">


                <div class="well">
          
                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                        <tbody>
                            
                            <tr>
                                <td style="width: 10%;">Office</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="cboCompanyID" runat="server" ErrorMessage="Company Name Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>       
                            <tr>
                                <td style="width: 10%;">Job Grade</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtJobGrade" runat="server" class="form-control"  placeholder="Job Grade" MaxLength="10" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="req1" ControlToValidate="txtJobGrade" runat="server" ErrorMessage="Job Grade Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Days</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtDays" runat="server" class="form-control"  placeholder="Days" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="2" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDays" runat="server" ErrorMessage="Day Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                          
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%"></td>
                </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%">
                        <div class="col-lg-4">
                            <asp:Button ID="btnSave" class="btn bg-color-blueDark txt-color-white" Style="width: 100px" runat="server" Text="Save" OnClick="btnSave_Click" />
                            <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                        </div>
                    </td>
                </tr>



                </tbody>
                        </table>
                 

            </div>










            <!-- end widget content -->

        </div>
    <!-- end widget div -->

    </div>

    </div>
     <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
   
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                      <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                  <p><asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
     
            </div>
          </div>
</asp:Content>
