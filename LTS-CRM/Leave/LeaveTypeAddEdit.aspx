﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="LeaveTypeAddEdit.aspx.cs" Inherits="LTS_CRM.HR.Leave.LeaveTypeAddEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="col-sm-12">


        <div id="ribbon">

            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>

           
            <ol class="breadcrumb">
                 <li>Leave</li>
                <li>Leave Type</li>
            </ol>
            

          
        </div>
          
        <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">
           
            <header>
                <h2>Leave Type Add-Edit</h2>
            </header>

            <div class="jarviswidget-editbox">
           
            </div>
          
            <div class="widget-body">


                <div class="well">
          
                    <table id="user" class="table table-bordered table-striped" style="clear: both;">
                        <tbody>
                             <tr>
                                <td style="width: 10%;">Office</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="cboCompanyID" runat="server" ErrorMessage="Company Name Required" Display="Dynamic"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>       
                            <tr>
                                <td style="width: 10%;">Leave Name</td>
                                <td style="width: 10%">
                                    <div class="col-lg-4">
                                        <asp:TextBox ID="txtLeaveName" runat="server" class="form-control"  placeholder="Leave Name" MaxLength="50" Text=""></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="req1" ControlToValidate="txtLeaveName" runat="server" ErrorMessage="Leave Name Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Entitlement Day(s)</td>
                                <td style="width: 10%">
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtDays" runat="server" class="form-control"  placeholder="Entitlement Day(s)" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="3" Text="0"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtDays" runat="server" ErrorMessage="Entitlement Day(s) Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                          <tr>
                                <td style="width: 10%;">Leave Day(s) </td>
                                <td style="width: 10%">
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtLeaveDay1" runat="server" class="form-control"  placeholder="Leave Day(s)" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="3" Text="0"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtLeaveDay1" runat="server" ErrorMessage="Apply Before Day(s) Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                       
                                    </div>
                                    <div class="col-lg-2">
                                        Apply Befor Day(s) 
                                        </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtLeaveBefore1" runat="server" class="form-control"  placeholder="Apply Before Days" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="3" Text="0"></asp:TextBox>                                       
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtLeaveBefore1" runat="server" ErrorMessage="Apply Before Day(s) Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Leave Day(s) </td>
                                <td style="width: 10%">
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtLeaveDay2" runat="server" class="form-control"  placeholder="Apply Leave Day(s)" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="3" Text="0"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtLeaveDay2" runat="server" ErrorMessage="Apply Before Day(s) Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                       
                                    </div>
                                    <div class="col-lg-2">
                                        Apply Before Day(s) 
                                        </div>
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtLeaveBefore2" runat="server" class="form-control"  placeholder="Apply Before Days" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="3" Text="0"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtLeaveBefore2" runat="server" ErrorMessage="Apply Before Day(s) Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Max. Day(s) Approve by Approver 1</td>
                                <td style="width: 10%">
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtapproveHOD" runat="server" class="form-control"  placeholder="Max. Day(s) Approve by Approver 1" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="3" Text="0"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtapproveHOD" runat="server" ErrorMessage="Apply Before Day(s) Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%;">Max. Day(s) Approve by Approver 2</td>
                                <td style="width: 10%">
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtApproveHRAdmin" runat="server" class="form-control"  placeholder="Max. Day(s) Approve by Approver 2" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);" MaxLength="3" Text="0"></asp:TextBox>                                        
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtApproveHRAdmin" runat="server" ErrorMessage="Apply Before Day(s) Required"  Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
                                    </div>
                                </td>
                            </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%">

                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;"></td>
                    <td style="width: 40%">
                        <div class="col-lg-4">
                            <asp:Button ID="btnSave" class="btn bg-color-blueDark txt-color-white" Style="width: 100px" runat="server" Text="Save" OnClick="btnSave_Click" />
                            <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />
                        </div>
                    </td>
                </tr>



                </tbody>
                        </table>
                 

            </div>










            <!-- end widget content -->

        </div>
    <!-- end widget div -->

    </div>

    </div>
     <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
   
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">
                      <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                  <p><asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
     
            </div>
          </div>
</asp:Content>
