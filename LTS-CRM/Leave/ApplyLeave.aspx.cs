﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace LTS_CRM.Leave
{
    public partial class ApplyLeave : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Add("UserName", "EMP001");
            Session.Add("CompanyID", "1");
            Session.Add("EmpGender", "Male");
            Session.Add("EmpHOD", "1");
            if (IsPostBack == false)
            {
                LoadcboLeaveType();
                txtStartDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                lbldaycount.Text = "1";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string LeaveNo = "";
            string StrSql = "";
            System.DateTime dtfrom = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            System.DateTime dtto = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (ValidatingLeave(cmbLeaveType.SelectedValue, dtfrom, dtto) == true)
            {
                LeaveNo = comman.LeaveRunNo();




                StrSql = "INSERT INTO  [LeaveTransaction] ";
                StrSql = StrSql + "([LeaveNo],[ApplyDate],[EmployeeID],[LeaveType],[LeaveDateFrom] ";
                StrSql = StrSql + ",[LeaveDateTo],[Reason],[DayType],[Status] ";
                StrSql = StrSql + ",[HOD],[AppStatus],[DocumentRecieved],[Duration])";
                StrSql = StrSql + " VALUES (";
                StrSql = StrSql + "'" + LeaveNo + "','" + DateTime.Now.ToString("yyyy/MM/dd") + "','" + SessionValue("UserName") + "','" + cmbLeaveType.SelectedValue + "','" + dtfrom.ToString("yyyy/MM/dd") + "' ";
                StrSql = StrSql + ",'" + dtto.ToString("yyyy/MM/dd") + "','" + txtreason.Text + "','" + cbodaytype.SelectedValue + "','" + "Pending Approval" + "' ";
                StrSql = StrSql + ",'" + SessionValue("EmpHOD") + "','" + "Pending Approval" + "'," + 0 + ", " + Convert.ToDecimal(lbldaycount.Text) + ")";
                ExeCuteStat(StrSql);

                ShowMSG("Leave", "Leave Application Successfully Submited");
                //'update runing number
                //StrSql = "Update RuningNumber SET LeaveNo=" & CInt(lblRefNo.Text) & ""
                //ExecuteSQLPage(StrSql)
                //this.SendEmail(cmbLeaveType.SelectedValue, Convert.ToDateTime(this.txtFromDate.Value), Convert.ToDateTime(this.txtTodate.Value), this.txtReason.Text);
                //NewLeave();

                //msglbl.Text = "Leave Application Successfully Submited";
                // MSG.Visible = true;
            }
            else 
            {
            

            }
           
        }
        private bool ValidatingLeave(string StrLeave, DateTime txtFromDate, DateTime txtTodate)
        {
            //Dim ApplyLeave As Boolean
            //txtReason.Text = Strings.Replace(txtReason.Text, "'", "");
            switch (StrLeave)
            {
                case "Annual Leave":
                    //Annual Leave
                    ValidateAnnualLeave(Convert.ToDateTime(txtFromDate), Convert.ToDateTime(txtTodate));
                    break;
                //case "Emergency Leave":
                //    //Emergency Leave
                //    ApplyEmergencyLeave(ValidateEmergencyLeave(Convert.ToDateTime(this.txtFromDate.Value), Convert.ToDateTime(this.txtTodate.Value)));
                //    break;
                //case "Others Leave":
                //    //Others Leave
                //    ApplyOthersLeave(ValidateOthersLeave(Convert.ToDateTime(this.txtFromDate.Value), Convert.ToDateTime(this.txtTodate.Value)));
                //    break;
                //case "Special Leave":
                //    //Special Leave
                //    ApplySpecialLeave(ValidateSpecialLeave(Convert.ToDateTime(this.txtFromDate.Value), Convert.ToDateTime(this.txtTodate.Value)));
                //    break;
                //case "Medical Leave":
                //    //Medical Leave
                //    MedicalLeave();
                //    break;
                //case "Compassionate Leave":
                //    //Compassionate Leave
                //    CompassionateLeave();
                //    break;
                //case "Congratulatory Leave":
                //    //Congratulatory Leave
                //    CongratulatoryLeave();
                //    break;
                //case "Paternity Leave":
                //    //Paternity Leave
                //    PaternityLeave();
                //    break;
                //case "Maternity Leave":
                //    //Maternity Leave
                //    Maternity();
                //    break;
                //case "Study Leave":
                //    //Study Leave
                //    StudyLeave();
                //    break;
                //case "Examination Leave":
                //    //Examination Leave
                //    ExaminationLeave();
                //    break;
                //case "Unpaid Leave":
                //    //Unpaid Leave
                //    UnpaidLeave(ValidateUnpaidLeave(Convert.ToDateTime(this.txtFromDate.Value), Convert.ToDateTime(this.txtTodate.Value)));
                //    break;
                //case "Hospitalization":
                //    //Hospitalization
                //    HospitalizationLeave();
                //    break;
                   
            }
            return false;
        }
        private bool ValidateAnnualLeave(System.DateTime FromDt, System.DateTime ToDt)
        {

            decimal DayDiff = default(decimal);
            int intI = default(Int16);
            string StrSql = "";
            System.DateTime ApplyBeforDt = DateTime.Now.Date ;

            //Date
            if (Convert.ToDateTime(FromDt) < ApplyBeforDt)
            {
                //MSG.Visible = True
                msglbl.Text = "Invalid Application Date, Date has passed.";
                return false;
            }

            
            //Applicabale for Permenent and contract employee only
            if ((comman.uCase(Session["EmpStatus"].ToString())) == comman.uCase("CONTRACTUAL") || comman.uCase(Session["EmpStatus"].ToString()) == comman.uCase("CONFIRMED"))
            {
                DayDiff = Convert.ToDecimal(lbldaycount.Text);
                if (DayDiff <= 2)
                {
                    for (intI = 1; intI <= 4; intI++)
                    {
                        //Check for the weekend
                        if (ApplyBeforDt.DayOfWeek == DayOfWeek.Saturday | ApplyBeforDt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            intI = intI - 1;
                        }
                        else
                        {
                            //check for the holiday
                            StrSql = "Select * from Holiday where [Date]='" + ApplyBeforDt.ToString("yyyy/MM/dd") + "' and Division='" + Session["EmpDivision"] + "'";
                            if (RecordExist(StrSql) == true)
                            {
                                intI = intI - 1;
                            }
                        }
                        //Add Days for applay before date
                        ApplyBeforDt = ApplyBeforDt.AddDays(1); 
                    }
                    if (FromDt >= ApplyBeforDt)
                    {
                        return true;
                    }
                    else
                    {
                        msglbl.Text = "Annual Leave must be applied before four working days.";
                        return false;
                    }
                }
                else if (DayDiff > 2 & DayDiff < 7)
                {
                    for (intI = 1; intI <= 7; intI++)
                    {
                        //Check for the weekend
                        if (ApplyBeforDt.DayOfWeek == DayOfWeek.Saturday | ApplyBeforDt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            intI = intI - 1;
                        }
                        else
                        {
                            //check for the holiday
                            StrSql = "Select * from Holiday where [Date]='" + ApplyBeforDt.ToString("yyyy/MM/dd") + "' and EmpHoliday='" + Session["CompanyID"] + "'";
                            if (RecordExist(StrSql) == true)
                            {
                                intI = intI - 1;
                            }
                        }
                        //Add Days for applay before date
                        ApplyBeforDt = ApplyBeforDt.AddDays(1);
                    }
                    if (FromDt >= ApplyBeforDt)
                    {
                        return true;
                    }
                    else
                    {
                        msglbl.Text = "Application for leave of more than 2 days must be applied before seven working days.";
                        return false;
                    }
                }
                else if (DayDiff >= 7)
                {
                    //Add Days for applay before date
                    ApplyBeforDt = ApplyBeforDt.AddDays(1);

                    if (FromDt >= ApplyBeforDt)
                    {
                        return true;
                    }
                    else
                    {
                        msglbl.Text = "Application for leave of more than 7 days must be applied before 30 days.";
                        return false;
                    }
                }

            }
            else
            {
                msglbl.Text = "You are not eligible for Annual Leave";
                return false;
            }
            return false;
        }
        protected void CalendarChange(object sender, EventArgs e)
        {
            Duration();
        }
        private void Duration()
        {
            string StrSql = "";
            int DayCount = 0;
            int I = 0;
            System.DateTime dt = new System.DateTime();

            DateTime FromYear = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime ToYear = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);     
            TimeSpan objTimeSpan = ToYear - FromYear;
            int Years = ToYear.Year - FromYear.Year;
            int month = ToYear.Month - FromYear.Month;
            double DayDiff = Convert.ToDouble(objTimeSpan.TotalDays) + 1;
            dt = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            for (I = 1; I <= DayDiff; I++)
            {
                if (dt.DayOfWeek == DayOfWeek.Saturday | dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    //I = I - 1
                   
                }
                else
                {
                    //check for the holiday
                   
                    StrSql = "Select * from EmpHoliday where [Date]='" + dt.ToString("yyyy/MM/dd") + "' and CompanyID='" + SessionValue("CompanyID").ToString() + "'";
                    if (RecordExist(StrSql) == false)
                    {
                        DayCount = DayCount + 1;

                    }

                }
                dt = dt.AddDays(1);
            }

            StrSql = cmbLeaveType.SelectedValue;

            if (StrSql == "Maternity Leave")
            {

                DayCount = Convert.ToInt32(objTimeSpan.TotalDays) + 1; 
            }
            //if (DayCount == 0)
            //    DayCount = 1;
            lbldaycount.Text = DayCount.ToString ();
            if (DayCount == 0)
            {
                //txtStartDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                //txtEndDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                //lbldaycount.Text = "0 Day";
            }
            else if (DayCount > 9)
            {
                lbldaycount.Text = lbldaycount.Text ;
            }
            else
            {
                lbldaycount.Text = lbldaycount.Text ;
            }

            if (DayCount == 1 || DayCount == 0)
            {
                cbodaytype.Enabled = true;
            }
            else
            {
                cbodaytype.SelectedValue = "FullDay";
                cbodaytype.Enabled = false;
                //this.FirstHalf.Enabled = false;
                //this.SecondHalf.Enabled = false;
                //this.FullDay.Checked = true;
            }
        }
        protected void cboDaytype_Changed(object sender, EventArgs e)
        {
            if (cbodaytype.SelectedValue == "FullDay")
            {

            }
            else
            {
                lbldaycount.Text = "0.5";
            }
        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected Boolean RecordExist(string strsql)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
           
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }

        }

        protected void LoadcboLeaveType()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            String sql = "";
            Connection.Open();
            if (SessionValue("EmpGender").ToString() == "Male")
            {
                sql = "select LeaveName,LeaveName from [EmpLeaveType] where LeaveName Not Like '%Maternity%' And CompanyID=@ID";
            }
            else
            {
                sql = "select LeaveName,LeaveName from [EmpLeaveType] where LeaveName Not Like '%Paternity%' And CompanyID=@ID";
            }
            
            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@ID", SessionValue("CompanyID"));
            cmbLeaveType.DataTextField = "LeaveName";
            cmbLeaveType.DataValueField = "LeaveName";
            cmbLeaveType.DataSource = Command.ExecuteReader();
            cmbLeaveType.DataBind();
            Connection.Close();
        }

        private bool RecDateExist(System.DateTime Dt)
        {
            string StrSql = null;
            if (cbodaytype.SelectedValue == "FullDay")
            {
                StrSql = "Select * From LeaveTransaction where DayType='Full Day' And EmployeeID='" + SessionValue("UserName") + "' and ('" + Dt.ToString("yyyy/MM/dd") + "' between LeaveDateFrom And LeaveDateTo) And Status<>'Application Rejected'";
                if (RecordExist(StrSql) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (cbodaytype.SelectedValue == "FirstHalf")
            {
                StrSql = "Select * From LeaveTransaction where DayType='First Half' And EmployeeID='" + SessionValue("UserName") + "' and ('" + Dt.ToString("yyyy/MM/dd") + "' between LeaveDateFrom And LeaveDateTo) And Status<>'Application Rejected'";
                if (RecordExist(StrSql) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (cbodaytype.SelectedValue == "SecondHalf")
            {
                StrSql = "Select * From LeaveTransaction where DayType='Second Half' And EmployeeID='" + SessionValue("UserName") + "' and ('" + Dt.ToString("yyyy/MM/dd") + "' between LeaveDateFrom And LeaveDateTo) And Status<>'Application Rejected'";
                if (RecordExist(StrSql) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        protected void ExeCuteStat(string StrSql)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            try
            {
                SqlCommand Command = new SqlCommand();
                Command.Connection = Connection;
                Command.CommandText = StrSql;

                Command.ExecuteNonQuery();
                Connection.Close();

            }
            catch (Exception ex)
            {
                Connection.Close();

            }
            finally
            {
                Connection.Close();
            }

        }

        protected void ShowMSG(string Header, string MSG)
        {
            lblheader.Text = Header;
            lblmsg.Text = MSG;

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
        
        }
    }
}