﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Security.Cryptography;
using System.IO;
using System.Data.SqlTypes;

namespace LTS_CRM
{
    class comman
    {
        public static System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection();
        //Public Shared Conn1 As New System.Data.SqlClient.SqlConnection
        //All function for Any application
        public static System.Data.Odbc.OdbcConnection Conn1 = new System.Data.Odbc.OdbcConnection();
        //Open Connection
        public static void OpenConn()
        {
            //CloseConn()
            if (Conn.State == System.Data.ConnectionState.Closed)
            {
                Conn = new System.Data.SqlClient.SqlConnection();
                Conn.ConnectionString = (System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                Conn.Open();
            }
        }
        //Close Connection
        public static void CloseConn()
        {
            Conn.Close();
            Conn.Dispose();
            //SqlConnection.ClearAllPools()
            //conn.clearpools()
        }
        //Execute Statement Add,Edit,Delete
        public static string EmpRunNo()
        {
            int intI = 0;
            string strNum = null;
            string CurNo = null;
            string strsql = null;
            //Dim StrSql As String
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            strsql = "Select EmpNo From ARunningNo where ID=1";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            SqlDataReader dr = Command.ExecuteReader();
            if (dr.Read())
            {
                CurNo = dr["EmpNo"].ToString();
            }

            intI = Convert.ToInt32(CurNo) + 1;
            //update runing number
            strsql = "Update ARunningNo SET EmpNo=" + intI + "";
            ExeCuteStat(strsql);
            strNum = intI.ToString();
            for (intI = strNum.Length; intI <= 3; intI++)
            {
                strNum = "0" + strNum;
            }
            strNum = "EMP" + strNum;
            return strNum;
        }
        public static string LeaveRunNo()
        {
            int intI = 0;
            string strNum = null;
            string CurNo = null;
            string strsql = null;
            //Dim StrSql As String
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            strsql = "Select LeaveRequest From ARunningNo where ID=1";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            SqlDataReader dr = Command.ExecuteReader();
            if (dr.Read())
            {
                CurNo = dr["LeaveRequest"].ToString();
            }

            intI = Convert.ToInt32(CurNo) + 1;
            //update runing number
            strsql = "Update ARunningNo SET LeaveRequest=" + intI + "";
            ExeCuteStat(strsql);
            strNum = intI.ToString();
            for (intI = strNum.Length; intI <= 5; intI++)
            {
                strNum = "0" + strNum;
            }

            return strNum;
        }
        public static string JobReqRunNo()
        {
            int intI = 0;
            string strNum = null;
            string CurNo = null;
            string strsql = null;
            //Dim StrSql As String
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            strsql = "Select JobRequest From ARunningNo where ID=1";

            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = strsql;
            SqlDataReader dr = Command.ExecuteReader();
            if (dr.Read())
            {
                CurNo = dr["JobRequest"].ToString();
            }
            //DB.EmpVPHR = ds.Tables(0).Rows(0).Item("VPHR").ToString
            int numb = Convert.ToInt32(CurNo);
            numb = numb + 1;
            CurNo = Convert.ToString(numb); ;
            //update runing number
            strsql = "Update ARunningNo SET JobRequest=" + numb + "";
            ExeCuteStat(strsql);
            strNum = CurNo;
            for (intI = strNum.Length; intI <= 5; intI++)
            {
                strNum = "0" + strNum;
            }
            string pre = "JR-";
            strNum = pre + strNum;
            return strNum;
        }

        public static DataTable GetAFile(int id)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            DataTable file = new DataTable();

            Connection.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Connection;
            cmd.CommandTimeout = 0;

            cmd.CommandText = "SELECT ID, FileName, FileType, FileData FROM Document "
                + "WHERE ID=@ID";
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();

            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters["@ID"].Value = id;

            adapter.SelectCommand = cmd;
            adapter.Fill(file);

            Connection.Close();

            return file;
        }

        public static void ExeCuteStat(string StrSql)
        {
            try
            {
                OpenConn();
                System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand(StrSql, Conn);
                myCommand.CommandText = StrSql;
                myCommand.ExecuteNonQuery();
                CloseConn();

            }
            catch (Exception ex)
            {
                CloseConn();

            }
            finally
            {
                CloseConn();
            }

        }
        //------------------Record Exist or not------------------
        public static bool RecordExist(string StrSql)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();



            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = StrSql;
            SqlDataReader dr = Command.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Close();
                Connection.Close();
                return true;
            }
            else
            {
                dr.Close();
                Connection.Close();
                return false;
            }
        }
        //------------------Create Datareader------------------
        public static System.Data.SqlClient.SqlDataReader CreateDR(string StrSql)
        {
            OpenConn();
            System.Data.SqlClient.SqlDataReader DataReader = default(System.Data.SqlClient.SqlDataReader);
            //Open the Database
            System.Data.SqlClient.SqlCommand SqlComm = new System.Data.SqlClient.SqlCommand(StrSql, Conn);
            //Execute the reader, Read data from Resoursfoodmeal
            DataReader = SqlComm.ExecuteReader();
            return DataReader;
        }
        //------------------Create Datareader------------------
        public static System.Data.DataSet CreateDS(string StrSql)
        {
            OpenConn();
            System.Data.DataSet DS = new System.Data.DataSet();
            System.Data.SqlClient.SqlDataAdapter DA = new System.Data.SqlClient.SqlDataAdapter(StrSql, Conn);
            try
            {
                DA.Fill(DS);
                CloseConn();
                return DS;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                CloseConn();
            }


            return DS;
        }
        public static object lCase(string strIn)
        {
            CultureInfo c = CultureInfo.CurrentCulture;

            return strIn.ToLower(c);
        }
        public static object uCase(string strIn)
        {
            CultureInfo c = CultureInfo.CurrentCulture;

            return strIn.ToUpper(c);
        }


        private const string cryptoKey = "mitnilom009";

        // The Initialization Vector for the DES encryption routine
        private static readonly byte[] IV =
            new byte[8] { 240, 3, 45, 29, 0, 76, 173, 59 };

        /// <summary>
        /// Encrypts provided string parameter
        /// </summary>
        public static string Encrypt(string s)
        {
            if (s == null || s.Length == 0) return string.Empty;

            string result = string.Empty;

            try
            {
                byte[] buffer = Encoding.ASCII.GetBytes(s);

                TripleDESCryptoServiceProvider des =
                    new TripleDESCryptoServiceProvider();

                MD5CryptoServiceProvider MD5 =
                    new MD5CryptoServiceProvider();

                des.Key =
                    MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cryptoKey));

                des.IV = IV;
                result = Convert.ToBase64String(
                    des.CreateEncryptor().TransformFinalBlock(
                        buffer, 0, buffer.Length));
            }
            catch
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Decrypts provided string parameter
        /// </summary>
        public static string Decrypt(string s)
        {
            if (s == null || s.Length == 0) return string.Empty;

            string result = string.Empty;
            s = s.Replace(" ", "+");
            try
            {
                byte[] buffer = Convert.FromBase64String(s);

                TripleDESCryptoServiceProvider des =
                    new TripleDESCryptoServiceProvider();

                MD5CryptoServiceProvider MD5 =
                    new MD5CryptoServiceProvider();

                des.Key =
                    MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cryptoKey));

                des.IV = IV;

                result = Encoding.ASCII.GetString(
                    des.CreateDecryptor().TransformFinalBlock(
                    buffer, 0, buffer.Length));
            }
            catch
            {
                throw;
            }

            return result;
        }

        public static string GetSimpleExtension(string fileName)
        {
            return Path.GetExtension(fileName).Replace(".", "");
        }

        public const int TourPhotoSizeLimit = 3000000;
    }
}
