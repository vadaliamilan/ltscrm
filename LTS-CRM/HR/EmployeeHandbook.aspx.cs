﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.HR
{
    public partial class EmployeeHandBook : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            documentload(30);     
        }

        protected void documentload(int id)
        {

            string embed = "<object data=\"{0}{1}\" type=\"application/pdf\" width=\"100%\" height=\"100%\">";
            embed += "If you are unable to view file, you can download from <a href = \"{0}{1}&download=1\">here</a>";
            embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
            embed += "</object>";
           // ltEmbed.Text = string.Format(embed, ResolveUrl("~/FileCS.ashx?Id="), id);
            lblpdfviewer.InnerHtml= string.Format(embed, ResolveUrl("~/FileCS.ashx?Id="), id);
        
        }
    }
}