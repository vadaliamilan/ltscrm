﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="Employee.aspx.cs" Inherits="LTS_CRM.HR.Employee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Humana Resource</li>
                <li>Employee</li>
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Employee List </h2>
                        </header>
                        <div>
                            <div class="jarviswidget-editbox">                           
                            </div>                        
                            <div class="widget-body no-padding">
                                <div class="widget-body-toolbar">
                                    <div class="row">
                                        <div class="col-xs-9 col-sm-5 col-md-5 col-lg-5">
                                            <div class="input-group">
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-7 col-md-7 col-lg-7 text-right">
                                            <a id="A1" class="btn bg-color-blueDark txt-color-white" href="#" onserverclick="cmdAddNew_Click" runat="server">
                                                <i class="fa fa-plus"></i><span class="hidden-mobile">AddNew </span>
                                            </a>
                                            <a class="btn bg-color-blueDark txt-color-white" href="OrganizationAddEdit.aspx">
                                                <i class="fa fa-table"></i><span class="hidden-mobile">Export </span>
                                            </a>
                                        </div>

                                    </div>
                                    <asp:Repeater ID="MyRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                        <HeaderTemplate>
                                            <table id="datatable_fixed_column" class="table table-striped table-bordered" width="100%">

                                                <thead>
                                                    <tr>
                                                        <th class="hasinput" style="width: 5%">
                                                            <input type="text" class="form-control" placeholder="No" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%">
                                                            <input type="text" class="form-control" placeholder="Employee ID" />
                                                        </th>
                                                         <th class="hasinput" style="width: 15%">
                                                            <input type="text" class="form-control" placeholder="First Name" />
                                                        </th>
                                                        <th class="hasinput" style="width: 15%">
                                                            <input type="text" class="form-control" placeholder="Last Name" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%">
                                                            <input type="text" class="form-control" placeholder="Email Address" />
                                                        </th>
                                                        <th class="hasinput" style="width: 15%">
                                                            <input type="text" class="form-control" placeholder="Office Name" />
                                                        </th>
                                                        <th class="hasinput" style="width: 15%">
                                                            <input type="text" class="form-control" placeholder="Department Name" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%">
                                                            <input type="text" class="form-control" placeholder="User Status" />
                                                        </th>
                                                        <th class="hasinput" style="width: 10%"></th>
                                                    </tr>
                                                    <tr>
                                                        <th data-hide="phone">No</th>
                                                        <th data-class="expand">Employee ID</th>                                                       
                                                        <th data-hide="phone">First Name</th>
                                                        <th data-hide="phone">Last Name</th>
                                                        <th data-hide="phone">Email Address</th>
                                                        <th data-hide="phone">Office Name</th>
                                                        <th data-hide="phone">Department Name</th>
                                                        <th data-hide="phone">User Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# DataBinder.Eval(Container.DataItem, "No")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "EmployeeID")%></td>                                                
                                                <td><%# DataBinder.Eval(Container.DataItem, "FirstName")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "Surname")%></td>                                                
                                                <td><%# DataBinder.Eval(Container.DataItem, "EmailAddress")%></td>
                                                 <td><%# DataBinder.Eval(Container.DataItem, "CompanyName")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "DepartmentName")%></td>
                                                <td><%# DataBinder.Eval(Container.DataItem, "UserStatus")%></td>
                                                <td>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="getid" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnEdit_Click"><i class="fa fa-edit"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                    <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="btndel" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                </td>

                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>
</asp:Content>
