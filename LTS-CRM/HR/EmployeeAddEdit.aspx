﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="EmployeeAddEdit.aspx.cs" Inherits="LTS_CRM.HR.EmployeeAddEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">





    <div id="ribbon">

        <span class="ribbon-button-alignment">
            <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                <i class="fa fa-refresh"></i>
            </span>
        </span>


        <ol class="breadcrumb">
            <li>Human Resource</li>
            <li>Employee Add-Edit</li>
        </ol>



    </div>

    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-deletebutton="false" data-widget-sortable="false">

        <header>
            <h2>Employee Add-Edit</h2>
        </header>

        <div class="jarviswidget-editbox">
        </div>

        <div class="widget-body">





            <table id="user" class="table table-bordered table-striped" style="clear: both;">
                <tbody>
                    <tr>
                        <td style="width: 100%;">

                            <div class="jarviswidget" id="wid-id-5" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
                                <!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				
								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"
				
								-->
                                <header>
                                    <h2>Employee Information </h2>

                                </header>

                                <!-- widget div-->
                                <div>

                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->

                                    </div>
                                    <!-- end widget edit box -->

                                    <!-- widget content -->
                                    <div class="widget-body">

                                        <div class="tabs-left">
                                            <ul class="nav nav-tabs tabs-left " id="demo-pill-nav">
                                                <li class="bg-color-blueDark txt-color-white">
                                                    <a href="#tab1" data-toggle="tab">Official  </a>
                                                </li>
                                                <li class="bg-color-blueDark txt-color-white">
                                                    <a href="#tab2" data-toggle="tab">Personal </a>
                                                </li>
                                                <li class="bg-color-blueDark txt-color-white">
                                                    <a href="#tab3" data-toggle="tab">Next Keen </a>
                                                </li>
                                                <li class="bg-color-blueDark txt-color-white">
                                                    <a href="#tab4" data-toggle="tab">Passport </a>
                                                </li>
                                                <li class="bg-color-blueDark txt-color-white">
                                                    <a href="#tab5" data-toggle="tab">Document </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab1" runat="server" clientidmode="Static">

                                                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                                        <div id="checkout-form1" class="smart-form">

                                                            <div class="row">
                                                                <section class="col col-2">
                                                                    <label class="label">Prefix.</label>
                                                                    <label class="input">

                                                                        <asp:DropDownList class="form-control" ID="cboPrefix" runat="server">
                                                                            <asp:ListItem Value="Mr." Text="Mr."></asp:ListItem>
                                                                            <asp:ListItem Value="Miss." Text="Miss."></asp:ListItem>
                                                                            <asp:ListItem Value="Mrs." Text="Mrs."></asp:ListItem>
                                                                        </asp:DropDownList>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Employee ID</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>

                                                                        <asp:TextBox ID="txtEmployeeID" runat="server" class="form-control" placeholder="Auto ID" Text="" ReadOnly="True"></asp:TextBox>


                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Employee Role</label>
                                                                    <label class="input">

                                                                        <asp:DropDownList class="form-control" ID="cmbemprole" runat="server">
                                                                        </asp:DropDownList>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Employee Status</label>
                                                                    <label class="input">

                                                                        <asp:DropDownList class="form-control" ID="cboUserStatus" runat="server">
                                                                            <asp:ListItem Value="Active" Text="Active"></asp:ListItem>
                                                                            <asp:ListItem Value="Not Active" Text="Not Active"></asp:ListItem>

                                                                        </asp:DropDownList>

                                                                    </label>

                                                                </section>

                                                            </div>
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">First Name</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>

                                                                        <asp:TextBox ID="txtFirstName" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtfirstname" runat="server" ErrorMessage="First Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Surname</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>

                                                                        <asp:TextBox ID="txtSurName" runat="server" class="form-control" placeholder="Surname" Text=""></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtSurName" runat="server" ErrorMessage="SurName Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Email Address</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-envelope-o"></i>

                                                                        <asp:TextBox ID="txtEmailAddress" runat="server" class="form-control" placeholder="Email Address" Text="" TextMode="Email"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="txtEmailAddress" runat="server" ErrorMessage="Email Address Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="regEmail" ControlToValidate="txtEmailAddress" Text="Invalid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" runat="server" Font-Bold="true" ForeColor="Red" />
                                                                    </label>
                                                                </section>
                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">First Name (Chinese)</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>
                                                                        <asp:TextBox ID="txtFirstNameChinese" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Surname (Chinese)</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-user"></i>
                                                                        <asp:TextBox ID="txtSurnameChinese" runat="server" class="form-control" placeholder="Surname" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Password</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-key"></i>
                                                                        <asp:TextBox ID="txtSecurePassword" runat="server" class="form-control" placeholder="Password" Text="" TextMode="Password"></asp:TextBox>

                                                                    </label>
                                                                </section>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                                        <label class="input">
                                                            <label for="imageupload">
                                                                <asp:FileUpload class="input" ID="imageupload" Style="display: none;" runat="server" ClientIDMode="static" />

                                                                <img id="lmglogo" src="../img/Photo.png" class="img-responsive" alt="img" runat="server" />
                                                            </label>
                                                        </label>

                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="checkout-form2" class="smart-form">
                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">Office</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboCompanyID" runat="server">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="cboDepartmentID" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Department</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboDepartmentID" runat="server">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="cboDepartmentID" runat="server" ErrorMessage="Department Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                    </label>

                                                                </section>

                                                                <section class="col col-3">
                                                                    <label class="label">Job Title</label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtJobTitle" runat="server" class="form-control" placeholder="Job Title" Text=""></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtJobTitle" runat="server" ErrorMessage="Job Title Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">Employment Status</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboEmployementStatus" runat="server">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="cboEmployementStatus" runat="server" ErrorMessage="Company Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Job Grade</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboJobGrade" runat="server">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="cboDepartmentID" runat="server" ErrorMessage="Department Name Required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Date of Joining</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-calendar"></i>
                                                                        <input type="text" name="request" id="txtDateofJoining" placeholder="Joining Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ControlToValidate="txtDateofJoining" runat="server" ErrorMessage="Joining date required" Display="Dynamic" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Date of Leaving</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-calendar"></i>
                                                                        <input type="text" name="request" id="txtDateofLeaving" placeholder="Leaving Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />

                                                                    </label>
                                                                </section>
                                                            </div>


                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">National Insurance No</label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtNationalInsurance" runat="server" class="form-control" placeholder="National Insurance No." Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Work Telephone</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-phone"></i>
                                                                        <asp:TextBox ID="txtWorkTelephoneNo" runat="server" class="form-control" placeholder="Company Phone" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Extension</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-mobile"></i>
                                                                        <asp:TextBox ID="txtExtention" runat="server" class="form-control" placeholder="Extension" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Fax Number</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-fax"></i>
                                                                        <asp:TextBox ID="txtWorkMobileNo" runat="server" class="form-control" placeholder="Fax Number" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>

                                                            </div>


                                                            <div class="row">
                                                            </div>




                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane" id="tab2" runat="server" clientidmode="Static">

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="Div1" class="smart-form">

                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">Address</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtAddress" runat="server" class="form-control" placeholder="Address" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Postcode</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtPostcode" runat="server" class="form-control" placeholder="PostCode" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">City</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtCity" runat="server" class="form-control" placeholder="City" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">State</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtState" runat="server" class="form-control" placeholder="State" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Country</label>
                                                                    <label class="input">

                                                                        <select id="txtCountry" class="form-control" runat="server">

                                                                            <option value="Afghanistan">Afghanistan</option>
                                                                            <option value="Albania">Albania</option>
                                                                            <option value="Algeria">Algeria</option>
                                                                            <option value="American Samoa">American Samoa</option>
                                                                            <option value="Andorra">Andorra</option>
                                                                            <option value="Angola">Angola</option>
                                                                            <option value="Anguilla">Anguilla</option>
                                                                            <option value="Antartica">Antarctica</option>
                                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                            <option value="Argentina">Argentina</option>
                                                                            <option value="Armenia">Armenia</option>
                                                                            <option value="Aruba">Aruba</option>
                                                                            <option value="Australia">Australia</option>
                                                                            <option value="Austria">Austria</option>
                                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                                            <option value="Bahamas">Bahamas</option>
                                                                            <option value="Bahrain">Bahrain</option>
                                                                            <option value="Bangladesh">Bangladesh</option>
                                                                            <option value="Barbados">Barbados</option>
                                                                            <option value="Belarus">Belarus</option>
                                                                            <option value="Belgium">Belgium</option>
                                                                            <option value="Belize">Belize</option>
                                                                            <option value="Benin">Benin</option>
                                                                            <option value="Bermuda">Bermuda</option>
                                                                            <option value="Bhutan">Bhutan</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                            <option value="Botswana">Botswana</option>
                                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                                            <option value="Brazil">Brazil</option>
                                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                            <option value="Bulgaria">Bulgaria</option>
                                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                                            <option value="Burundi">Burundi</option>
                                                                            <option value="Cambodia">Cambodia</option>
                                                                            <option value="Cameroon">Cameroon</option>
                                                                            <option value="Canada">Canada</option>
                                                                            <option value="Cape Verde">Cape Verde</option>
                                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                                            <option value="Central African Republic">Central African Republic</option>
                                                                            <option value="Chad">Chad</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="China">China</option>
                                                                            <option value="Christmas Island">Christmas Island</option>
                                                                            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Comoros">Comoros</option>
                                                                            <option value="Congo">Congo</option>
                                                                            <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                            <option value="Cook Islands">Cook Islands</option>
                                                                            <option value="Costa Rica">Costa Rica</option>
                                                                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                            <option value="Croatia">Croatia (Hrvatska)</option>
                                                                            <option value="Cuba">Cuba</option>
                                                                            <option value="Cyprus">Cyprus</option>
                                                                            <option value="Czech Republic">Czech Republic</option>
                                                                            <option value="Denmark">Denmark</option>
                                                                            <option value="Djibouti">Djibouti</option>
                                                                            <option value="Dominica">Dominica</option>
                                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                                            <option value="East Timor">East Timor</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Egypt">Egypt</option>
                                                                            <option value="El Salvador">El Salvador</option>
                                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                            <option value="Eritrea">Eritrea</option>
                                                                            <option value="Estonia">Estonia</option>
                                                                            <option value="Ethiopia">Ethiopia</option>
                                                                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                                            <option value="Fiji">Fiji</option>
                                                                            <option value="Finland">Finland</option>
                                                                            <option value="France">France</option>
                                                                            <option value="France Metropolitan">France, Metropolitan</option>
                                                                            <option value="French Guiana">French Guiana</option>
                                                                            <option value="French Polynesia">French Polynesia</option>
                                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                                            <option value="Gabon">Gabon</option>
                                                                            <option value="Gambia">Gambia</option>
                                                                            <option value="Georgia">Georgia</option>
                                                                            <option value="Germany">Germany</option>
                                                                            <option value="Ghana">Ghana</option>
                                                                            <option value="Gibraltar">Gibraltar</option>
                                                                            <option value="Greece">Greece</option>
                                                                            <option value="Greenland">Greenland</option>
                                                                            <option value="Grenada">Grenada</option>
                                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                                            <option value="Guam">Guam</option>
                                                                            <option value="Guatemala">Guatemala</option>
                                                                            <option value="Guinea">Guinea</option>
                                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                            <option value="Guyana">Guyana</option>
                                                                            <option value="Haiti">Haiti</option>
                                                                            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                            <option value="Holy See">Holy See (Vatican City State)</option>
                                                                            <option value="Honduras">Honduras</option>
                                                                            <option value="Hong Kong">Hong Kong</option>
                                                                            <option value="Hungary">Hungary</option>
                                                                            <option value="Iceland">Iceland</option>
                                                                            <option value="India">India</option>
                                                                            <option value="Indonesia">Indonesia</option>
                                                                            <option value="Iran">Iran (Islamic Republic of)</option>
                                                                            <option value="Iraq">Iraq</option>
                                                                            <option value="Ireland">Ireland</option>
                                                                            <option value="Israel">Israel</option>
                                                                            <option value="Italy">Italy</option>
                                                                            <option value="Jamaica">Jamaica</option>
                                                                            <option value="Japan">Japan</option>
                                                                            <option value="Jordan">Jordan</option>
                                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                                            <option value="Kenya">Kenya</option>
                                                                            <option value="Kiribati">Kiribati</option>
                                                                            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                            <option value="Korea">Korea, Republic of</option>
                                                                            <option value="Kuwait">Kuwait</option>
                                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                            <option value="Lao">Lao People's Democratic Republic</option>
                                                                            <option value="Latvia">Latvia</option>
                                                                            <option value="Lebanon">Lebanon</option>
                                                                            <option value="Lesotho">Lesotho</option>
                                                                            <option value="Liberia">Liberia</option>
                                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                                            <option value="Lithuania">Lithuania</option>
                                                                            <option value="Luxembourg">Luxembourg</option>
                                                                            <option value="Macau">Macau</option>
                                                                            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                            <option value="Madagascar">Madagascar</option>
                                                                            <option value="Malawi">Malawi</option>
                                                                            <option value="Malaysia">Malaysia</option>
                                                                            <option value="Maldives">Maldives</option>
                                                                            <option value="Mali">Mali</option>
                                                                            <option value="Malta">Malta</option>
                                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                                            <option value="Martinique">Martinique</option>
                                                                            <option value="Mauritania">Mauritania</option>
                                                                            <option value="Mauritius">Mauritius</option>
                                                                            <option value="Mayotte">Mayotte</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Micronesia">Micronesia, Federated States of</option>
                                                                            <option value="Moldova">Moldova, Republic of</option>
                                                                            <option value="Monaco">Monaco</option>
                                                                            <option value="Mongolia">Mongolia</option>
                                                                            <option value="Montserrat">Montserrat</option>
                                                                            <option value="Morocco">Morocco</option>
                                                                            <option value="Mozambique">Mozambique</option>
                                                                            <option value="Myanmar">Myanmar</option>
                                                                            <option value="Namibia">Namibia</option>
                                                                            <option value="Nauru">Nauru</option>
                                                                            <option value="Nepal">Nepal</option>
                                                                            <option value="Netherlands">Netherlands</option>
                                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                            <option value="New Caledonia">New Caledonia</option>
                                                                            <option value="New Zealand">New Zealand</option>
                                                                            <option value="Nicaragua">Nicaragua</option>
                                                                            <option value="Niger">Niger</option>
                                                                            <option value="Nigeria">Nigeria</option>
                                                                            <option value="Niue">Niue</option>
                                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                            <option value="Norway">Norway</option>
                                                                            <option value="Oman">Oman</option>
                                                                            <option value="Pakistan">Pakistan</option>
                                                                            <option value="Palau">Palau</option>
                                                                            <option value="Panama">Panama</option>
                                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Philippines">Philippines</option>
                                                                            <option value="Pitcairn">Pitcairn</option>
                                                                            <option value="Poland">Poland</option>
                                                                            <option value="Portugal">Portugal</option>
                                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                                            <option value="Qatar">Qatar</option>
                                                                            <option value="Reunion">Reunion</option>
                                                                            <option value="Romania">Romania</option>
                                                                            <option value="Russia">Russian Federation</option>
                                                                            <option value="Rwanda">Rwanda</option>
                                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                            <option value="Saint LUCIA">Saint LUCIA</option>
                                                                            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                            <option value="Samoa">Samoa</option>
                                                                            <option value="San Marino">San Marino</option>
                                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                                            <option value="Senegal">Senegal</option>
                                                                            <option value="Seychelles">Seychelles</option>
                                                                            <option value="Sierra">Sierra Leone</option>
                                                                            <option value="Singapore">Singapore</option>
                                                                            <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                            <option value="Slovenia">Slovenia</option>
                                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                                            <option value="Somalia">Somalia</option>
                                                                            <option value="South Africa">South Africa</option>
                                                                            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                            <option value="Span">Spain</option>
                                                                            <option value="SriLanka">Sri Lanka</option>
                                                                            <option value="St. Helena">St. Helena</option>
                                                                            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                            <option value="Sudan">Sudan</option>
                                                                            <option value="Suriname">Suriname</option>
                                                                            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="Swaziland">Swaziland</option>
                                                                            <option value="Sweden">Sweden</option>
                                                                            <option value="Switzerland">Switzerland</option>
                                                                            <option value="Syria">Syrian Arab Republic</option>
                                                                            <option value="Taiwan">Taiwan, Province of China</option>
                                                                            <option value="Tajikistan">Tajikistan</option>
                                                                            <option value="Tanzania">Tanzania, United Republic of</option>
                                                                            <option value="Thailand">Thailand</option>
                                                                            <option value="Togo">Togo</option>
                                                                            <option value="Tokelau">Tokelau</option>
                                                                            <option value="Tonga">Tonga</option>
                                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                            <option value="Tunisia">Tunisia</option>
                                                                            <option value="Turkey">Turkey</option>
                                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                                            <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                            <option value="Tuvalu">Tuvalu</option>
                                                                            <option value="Uganda">Uganda</option>
                                                                            <option value="Ukraine">Ukraine</option>
                                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                                            <option value="United Kingdom" selected>United Kingdom</option>
                                                                            <option value="United States">United States</option>
                                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                                            <option value="Vanuatu">Vanuatu</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Vietnam">Viet Nam</option>
                                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                            <option value="Western Sahara">Western Sahara</option>
                                                                            <option value="Yemen">Yemen</option>
                                                                            <option value="Yugoslavia">Yugoslavia</option>
                                                                            <option value="Zambia">Zambia</option>
                                                                            <option value="Zimbabwe">Zimbabwe</option>



                                                                        </select>
                                                                    </label>
                                                                </section>

                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">Date of Birth</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-calendar"></i>
                                                                        <input type="text" name="request" id="txtDateofBirth" placeholder="Date of Birth" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />

                                                                    </label>
                                                                </section>

                                                                <section class="col col-3">
                                                                    <label class="label">Place of Birth</label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtPlaceofBirth" runat="server" class="form-control" placeholder="Place Of Birth" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Gender</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboGender" runat="server">
                                                                            <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                                                            <asp:ListItem Value="Female" Text="Female" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Value="Others" Text="Others"></asp:ListItem>
                                                                        </asp:DropDownList>


                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Maritial Status</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboMaritialStatus" runat="server">
                                                                            <asp:ListItem Value="Married" Text="Married"></asp:ListItem>
                                                                            <asp:ListItem Value="Single" Text="Single" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Value="Single Mother" Text="Single Mother"></asp:ListItem>
                                                                            <asp:ListItem Value="Widow" Text="Widow"></asp:ListItem>
                                                                            <asp:ListItem Value="Seprated" Text="Seprated"></asp:ListItem>
                                                                        </asp:DropDownList>


                                                                    </label>
                                                                </section>

                                                            </div>




                                                            <div class="row">
                                                                <section class="col col-3">
                                                                    <label class="label">Personal Email</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-envelope o"></i>
                                                                        <asp:TextBox ID="txtPersonalEmail" runat="server" class="form-control" placeholder="Personal Email Address" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Home Landline</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-phone"></i>
                                                                        <asp:TextBox ID="txtLandLineNumberr" runat="server" class="form-control" placeholder="Home Landline No" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Mobile</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-mobile"></i>
                                                                        <asp:TextBox ID="txtMobileNo" runat="server" class="form-control" placeholder="Mobile" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Personal WeChat</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-wechat"></i>
                                                                        <asp:TextBox ID="txtPersonalWeChat" runat="server" class="form-control" placeholder="Personal WeChat" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>

                                                            </div>

                                                            <div class="row">

                                                                <section class="col col-3">
                                                                    <label class="label">Personal Whatsapp</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-whatsapp"></i>
                                                                        <asp:TextBox ID="txtPersonWhatsup" runat="server" class="form-control" placeholder="Perrsonal Whatsapp" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-3">
                                                                    <label class="label">Personal QQ</label>
                                                                    <label class="input">
                                                                        <i class="icon-prepend fa fa-qq"></i>
                                                                        <asp:TextBox ID="txtPersonQQ" runat="server" class="form-control" placeholder="Personal QQ" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>


                                                            </div>





                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="tab-pane" id="tab3" runat="server" clientidmode="Static">

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="Div2" class="smart-form">
                                                            <h3><u>Next Keen A</u></h3>
                                                            <br />
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">First Name</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAFirstName" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Middle Name</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAMiddleName" runat="server" class="form-control" placeholder="Middle Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Surname</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKASurName" runat="server" class="form-control" placeholder="Surname" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>

                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">First Name (Chinese)</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAFirstNameChinese" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Middle Name (Chinese)</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAMiddleNameChiness" runat="server" class="form-control" placeholder="Middle Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Surname (Chinese)</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKASurNameChiness" runat="server" class="form-control" placeholder="Surname" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>

                                                            </div>

                                                            <div class="row">

                                                                <section class="col col-2">
                                                                    <label class="label">Gender</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboNKASex" runat="server">
                                                                            <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                                                            <asp:ListItem Value="Female" Text="Female" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Value="Others" Text="Others"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>
                                                                </section>

                                                                <section class="col col-2">
                                                                    <label class="label">Relationship</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboNKARelationship" runat="server">
                                                                            <asp:ListItem Value="Wife" Text="Wife" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Value="Son" Text="Son"></asp:ListItem>
                                                                            <asp:ListItem Value="Mother" Text="Mother"></asp:ListItem>
                                                                            <asp:ListItem Value="Sister" Text="Sister"></asp:ListItem>
                                                                            <asp:ListItem Value="Brother" Text="Brother"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Email Address</label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtNKAEmailAddress" runat="server" class="form-control" placeholder="Middle Name" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>

                                                                <section class="col col-2">
                                                                    <label class="label">Home Landline</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKALandLineNumber" runat="server" class="form-control" placeholder="Home Landline No" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Mobile</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAMobileNo" runat="server" class="form-control" placeholder="Mobile" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>


                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">Address</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAHomeAddress" runat="server" class="form-control" placeholder="Address" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Postcode</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAPostcode" runat="server" class="form-control" placeholder="PostCode" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">City</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKACity" runat="server" class="form-control" placeholder="City" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">State</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKAState" runat="server" class="form-control" placeholder="State" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Country</label>
                                                                    <label class="input">



                                                                        <select id="txtNKACountry" class="form-control" runat="server">

                                                                            <option value="Afghanistan">Afghanistan</option>
                                                                            <option value="Albania">Albania</option>
                                                                            <option value="Algeria">Algeria</option>
                                                                            <option value="American Samoa">American Samoa</option>
                                                                            <option value="Andorra">Andorra</option>
                                                                            <option value="Angola">Angola</option>
                                                                            <option value="Anguilla">Anguilla</option>
                                                                            <option value="Antartica">Antarctica</option>
                                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                            <option value="Argentina">Argentina</option>
                                                                            <option value="Armenia">Armenia</option>
                                                                            <option value="Aruba">Aruba</option>
                                                                            <option value="Australia">Australia</option>
                                                                            <option value="Austria">Austria</option>
                                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                                            <option value="Bahamas">Bahamas</option>
                                                                            <option value="Bahrain">Bahrain</option>
                                                                            <option value="Bangladesh">Bangladesh</option>
                                                                            <option value="Barbados">Barbados</option>
                                                                            <option value="Belarus">Belarus</option>
                                                                            <option value="Belgium">Belgium</option>
                                                                            <option value="Belize">Belize</option>
                                                                            <option value="Benin">Benin</option>
                                                                            <option value="Bermuda">Bermuda</option>
                                                                            <option value="Bhutan">Bhutan</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                            <option value="Botswana">Botswana</option>
                                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                                            <option value="Brazil">Brazil</option>
                                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                            <option value="Bulgaria">Bulgaria</option>
                                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                                            <option value="Burundi">Burundi</option>
                                                                            <option value="Cambodia">Cambodia</option>
                                                                            <option value="Cameroon">Cameroon</option>
                                                                            <option value="Canada">Canada</option>
                                                                            <option value="Cape Verde">Cape Verde</option>
                                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                                            <option value="Central African Republic">Central African Republic</option>
                                                                            <option value="Chad">Chad</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="China">China</option>
                                                                            <option value="Christmas Island">Christmas Island</option>
                                                                            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Comoros">Comoros</option>
                                                                            <option value="Congo">Congo</option>
                                                                            <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                            <option value="Cook Islands">Cook Islands</option>
                                                                            <option value="Costa Rica">Costa Rica</option>
                                                                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                            <option value="Croatia">Croatia (Hrvatska)</option>
                                                                            <option value="Cuba">Cuba</option>
                                                                            <option value="Cyprus">Cyprus</option>
                                                                            <option value="Czech Republic">Czech Republic</option>
                                                                            <option value="Denmark">Denmark</option>
                                                                            <option value="Djibouti">Djibouti</option>
                                                                            <option value="Dominica">Dominica</option>
                                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                                            <option value="East Timor">East Timor</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Egypt">Egypt</option>
                                                                            <option value="El Salvador">El Salvador</option>
                                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                            <option value="Eritrea">Eritrea</option>
                                                                            <option value="Estonia">Estonia</option>
                                                                            <option value="Ethiopia">Ethiopia</option>
                                                                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                                            <option value="Fiji">Fiji</option>
                                                                            <option value="Finland">Finland</option>
                                                                            <option value="France">France</option>
                                                                            <option value="France Metropolitan">France, Metropolitan</option>
                                                                            <option value="French Guiana">French Guiana</option>
                                                                            <option value="French Polynesia">French Polynesia</option>
                                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                                            <option value="Gabon">Gabon</option>
                                                                            <option value="Gambia">Gambia</option>
                                                                            <option value="Georgia">Georgia</option>
                                                                            <option value="Germany">Germany</option>
                                                                            <option value="Ghana">Ghana</option>
                                                                            <option value="Gibraltar">Gibraltar</option>
                                                                            <option value="Greece">Greece</option>
                                                                            <option value="Greenland">Greenland</option>
                                                                            <option value="Grenada">Grenada</option>
                                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                                            <option value="Guam">Guam</option>
                                                                            <option value="Guatemala">Guatemala</option>
                                                                            <option value="Guinea">Guinea</option>
                                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                            <option value="Guyana">Guyana</option>
                                                                            <option value="Haiti">Haiti</option>
                                                                            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                            <option value="Holy See">Holy See (Vatican City State)</option>
                                                                            <option value="Honduras">Honduras</option>
                                                                            <option value="Hong Kong">Hong Kong</option>
                                                                            <option value="Hungary">Hungary</option>
                                                                            <option value="Iceland">Iceland</option>
                                                                            <option value="India">India</option>
                                                                            <option value="Indonesia">Indonesia</option>
                                                                            <option value="Iran">Iran (Islamic Republic of)</option>
                                                                            <option value="Iraq">Iraq</option>
                                                                            <option value="Ireland">Ireland</option>
                                                                            <option value="Israel">Israel</option>
                                                                            <option value="Italy">Italy</option>
                                                                            <option value="Jamaica">Jamaica</option>
                                                                            <option value="Japan">Japan</option>
                                                                            <option value="Jordan">Jordan</option>
                                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                                            <option value="Kenya">Kenya</option>
                                                                            <option value="Kiribati">Kiribati</option>
                                                                            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                            <option value="Korea">Korea, Republic of</option>
                                                                            <option value="Kuwait">Kuwait</option>
                                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                            <option value="Lao">Lao People's Democratic Republic</option>
                                                                            <option value="Latvia">Latvia</option>
                                                                            <option value="Lebanon">Lebanon</option>
                                                                            <option value="Lesotho">Lesotho</option>
                                                                            <option value="Liberia">Liberia</option>
                                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                                            <option value="Lithuania">Lithuania</option>
                                                                            <option value="Luxembourg">Luxembourg</option>
                                                                            <option value="Macau">Macau</option>
                                                                            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                            <option value="Madagascar">Madagascar</option>
                                                                            <option value="Malawi">Malawi</option>
                                                                            <option value="Malaysia">Malaysia</option>
                                                                            <option value="Maldives">Maldives</option>
                                                                            <option value="Mali">Mali</option>
                                                                            <option value="Malta">Malta</option>
                                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                                            <option value="Martinique">Martinique</option>
                                                                            <option value="Mauritania">Mauritania</option>
                                                                            <option value="Mauritius">Mauritius</option>
                                                                            <option value="Mayotte">Mayotte</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Micronesia">Micronesia, Federated States of</option>
                                                                            <option value="Moldova">Moldova, Republic of</option>
                                                                            <option value="Monaco">Monaco</option>
                                                                            <option value="Mongolia">Mongolia</option>
                                                                            <option value="Montserrat">Montserrat</option>
                                                                            <option value="Morocco">Morocco</option>
                                                                            <option value="Mozambique">Mozambique</option>
                                                                            <option value="Myanmar">Myanmar</option>
                                                                            <option value="Namibia">Namibia</option>
                                                                            <option value="Nauru">Nauru</option>
                                                                            <option value="Nepal">Nepal</option>
                                                                            <option value="Netherlands">Netherlands</option>
                                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                            <option value="New Caledonia">New Caledonia</option>
                                                                            <option value="New Zealand">New Zealand</option>
                                                                            <option value="Nicaragua">Nicaragua</option>
                                                                            <option value="Niger">Niger</option>
                                                                            <option value="Nigeria">Nigeria</option>
                                                                            <option value="Niue">Niue</option>
                                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                            <option value="Norway">Norway</option>
                                                                            <option value="Oman">Oman</option>
                                                                            <option value="Pakistan">Pakistan</option>
                                                                            <option value="Palau">Palau</option>
                                                                            <option value="Panama">Panama</option>
                                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Philippines">Philippines</option>
                                                                            <option value="Pitcairn">Pitcairn</option>
                                                                            <option value="Poland">Poland</option>
                                                                            <option value="Portugal">Portugal</option>
                                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                                            <option value="Qatar">Qatar</option>
                                                                            <option value="Reunion">Reunion</option>
                                                                            <option value="Romania">Romania</option>
                                                                            <option value="Russia">Russian Federation</option>
                                                                            <option value="Rwanda">Rwanda</option>
                                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                            <option value="Saint LUCIA">Saint LUCIA</option>
                                                                            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                            <option value="Samoa">Samoa</option>
                                                                            <option value="San Marino">San Marino</option>
                                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                                            <option value="Senegal">Senegal</option>
                                                                            <option value="Seychelles">Seychelles</option>
                                                                            <option value="Sierra">Sierra Leone</option>
                                                                            <option value="Singapore">Singapore</option>
                                                                            <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                            <option value="Slovenia">Slovenia</option>
                                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                                            <option value="Somalia">Somalia</option>
                                                                            <option value="South Africa">South Africa</option>
                                                                            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                            <option value="Span">Spain</option>
                                                                            <option value="SriLanka">Sri Lanka</option>
                                                                            <option value="St. Helena">St. Helena</option>
                                                                            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                            <option value="Sudan">Sudan</option>
                                                                            <option value="Suriname">Suriname</option>
                                                                            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="Swaziland">Swaziland</option>
                                                                            <option value="Sweden">Sweden</option>
                                                                            <option value="Switzerland">Switzerland</option>
                                                                            <option value="Syria">Syrian Arab Republic</option>
                                                                            <option value="Taiwan">Taiwan, Province of China</option>
                                                                            <option value="Tajikistan">Tajikistan</option>
                                                                            <option value="Tanzania">Tanzania, United Republic of</option>
                                                                            <option value="Thailand">Thailand</option>
                                                                            <option value="Togo">Togo</option>
                                                                            <option value="Tokelau">Tokelau</option>
                                                                            <option value="Tonga">Tonga</option>
                                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                            <option value="Tunisia">Tunisia</option>
                                                                            <option value="Turkey">Turkey</option>
                                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                                            <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                            <option value="Tuvalu">Tuvalu</option>
                                                                            <option value="Uganda">Uganda</option>
                                                                            <option value="Ukraine">Ukraine</option>
                                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                                            <option value="United Kingdom" selected>United Kingdom</option>
                                                                            <option value="United States">United States</option>
                                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                                            <option value="Vanuatu">Vanuatu</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Vietnam">Viet Nam</option>
                                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                            <option value="Western Sahara">Western Sahara</option>
                                                                            <option value="Yemen">Yemen</option>
                                                                            <option value="Yugoslavia">Yugoslavia</option>
                                                                            <option value="Zambia">Zambia</option>
                                                                            <option value="Zimbabwe">Zimbabwe</option>



                                                                        </select>
                                                                    </label>
                                                                </section>

                                                            </div>


                                                            <h3><u>Next Keen B</u></h3>
                                                            <br />
                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">First Name</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBFirstName" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Middle Name</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBMiddleName" runat="server" class="form-control" placeholder="Middle Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Surname</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBSurName" runat="server" class="form-control" placeholder="Surname" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>

                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">First Name (Chinese)</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBFirstNameChines" runat="server" class="form-control" placeholder="First Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Middle Name (Chinese)</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBMiddleChiness" runat="server" class="form-control" placeholder="Middle Name" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Surname (Chinese)</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBSurNameChines" runat="server" class="form-control" placeholder="Surname" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>

                                                            </div>

                                                            <div class="row">

                                                                <section class="col col-2">
                                                                    <label class="label">Gender</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="cboNKBSex" runat="server">
                                                                            <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                                                            <asp:ListItem Value="Female" Text="Female" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Value="Others" Text="Others"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>
                                                                </section>

                                                                <section class="col col-2">
                                                                    <label class="label">Relationship</label>
                                                                    <label class="input">
                                                                        <asp:DropDownList class="form-control" ID="txtNKBRelationship" runat="server">
                                                                            <asp:ListItem Value="Wife" Text="Wife" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Value="Son" Text="Son"></asp:ListItem>
                                                                            <asp:ListItem Value="Mother" Text="Mother"></asp:ListItem>
                                                                            <asp:ListItem Value="Sister" Text="Sister"></asp:ListItem>
                                                                            <asp:ListItem Value="Brother" Text="Brother"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </label>

                                                                </section>
                                                                <section class="col col-4">
                                                                    <label class="label">Email Address</label>
                                                                    <label class="input">
                                                                        <asp:TextBox ID="txtNKBEmailAddress" runat="server" class="form-control" placeholder="Middle Name" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>

                                                                <section class="col col-2">
                                                                    <label class="label">Home Landline</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBLandLineNumber" runat="server" class="form-control" placeholder="Home Landline No" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Mobile</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBMobileNo" runat="server" class="form-control" placeholder="Mobile" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                            </div>


                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">Address</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBHomeAddress" runat="server" class="form-control" placeholder="Address" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Postcode</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBPostcode" runat="server" class="form-control" placeholder="PostCode" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">City</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBCity" runat="server" class="form-control" placeholder="City" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">State</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtNKBState" runat="server" class="form-control" placeholder="State" Text=""></asp:TextBox>
                                                                    </label>
                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Country</label>
                                                                    <label class="input">


                                                                        <select id="txtNKBCountry" class="form-control" runat="server">

                                                                            <option value="Afghanistan">Afghanistan</option>
                                                                            <option value="Albania">Albania</option>
                                                                            <option value="Algeria">Algeria</option>
                                                                            <option value="American Samoa">American Samoa</option>
                                                                            <option value="Andorra">Andorra</option>
                                                                            <option value="Angola">Angola</option>
                                                                            <option value="Anguilla">Anguilla</option>
                                                                            <option value="Antartica">Antarctica</option>
                                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                            <option value="Argentina">Argentina</option>
                                                                            <option value="Armenia">Armenia</option>
                                                                            <option value="Aruba">Aruba</option>
                                                                            <option value="Australia">Australia</option>
                                                                            <option value="Austria">Austria</option>
                                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                                            <option value="Bahamas">Bahamas</option>
                                                                            <option value="Bahrain">Bahrain</option>
                                                                            <option value="Bangladesh">Bangladesh</option>
                                                                            <option value="Barbados">Barbados</option>
                                                                            <option value="Belarus">Belarus</option>
                                                                            <option value="Belgium">Belgium</option>
                                                                            <option value="Belize">Belize</option>
                                                                            <option value="Benin">Benin</option>
                                                                            <option value="Bermuda">Bermuda</option>
                                                                            <option value="Bhutan">Bhutan</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                            <option value="Botswana">Botswana</option>
                                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                                            <option value="Brazil">Brazil</option>
                                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                            <option value="Bulgaria">Bulgaria</option>
                                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                                            <option value="Burundi">Burundi</option>
                                                                            <option value="Cambodia">Cambodia</option>
                                                                            <option value="Cameroon">Cameroon</option>
                                                                            <option value="Canada">Canada</option>
                                                                            <option value="Cape Verde">Cape Verde</option>
                                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                                            <option value="Central African Republic">Central African Republic</option>
                                                                            <option value="Chad">Chad</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="China">China</option>
                                                                            <option value="Christmas Island">Christmas Island</option>
                                                                            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Comoros">Comoros</option>
                                                                            <option value="Congo">Congo</option>
                                                                            <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                            <option value="Cook Islands">Cook Islands</option>
                                                                            <option value="Costa Rica">Costa Rica</option>
                                                                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                            <option value="Croatia">Croatia (Hrvatska)</option>
                                                                            <option value="Cuba">Cuba</option>
                                                                            <option value="Cyprus">Cyprus</option>
                                                                            <option value="Czech Republic">Czech Republic</option>
                                                                            <option value="Denmark">Denmark</option>
                                                                            <option value="Djibouti">Djibouti</option>
                                                                            <option value="Dominica">Dominica</option>
                                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                                            <option value="East Timor">East Timor</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Egypt">Egypt</option>
                                                                            <option value="El Salvador">El Salvador</option>
                                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                            <option value="Eritrea">Eritrea</option>
                                                                            <option value="Estonia">Estonia</option>
                                                                            <option value="Ethiopia">Ethiopia</option>
                                                                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                                            <option value="Fiji">Fiji</option>
                                                                            <option value="Finland">Finland</option>
                                                                            <option value="France">France</option>
                                                                            <option value="France Metropolitan">France, Metropolitan</option>
                                                                            <option value="French Guiana">French Guiana</option>
                                                                            <option value="French Polynesia">French Polynesia</option>
                                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                                            <option value="Gabon">Gabon</option>
                                                                            <option value="Gambia">Gambia</option>
                                                                            <option value="Georgia">Georgia</option>
                                                                            <option value="Germany">Germany</option>
                                                                            <option value="Ghana">Ghana</option>
                                                                            <option value="Gibraltar">Gibraltar</option>
                                                                            <option value="Greece">Greece</option>
                                                                            <option value="Greenland">Greenland</option>
                                                                            <option value="Grenada">Grenada</option>
                                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                                            <option value="Guam">Guam</option>
                                                                            <option value="Guatemala">Guatemala</option>
                                                                            <option value="Guinea">Guinea</option>
                                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                            <option value="Guyana">Guyana</option>
                                                                            <option value="Haiti">Haiti</option>
                                                                            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                            <option value="Holy See">Holy See (Vatican City State)</option>
                                                                            <option value="Honduras">Honduras</option>
                                                                            <option value="Hong Kong">Hong Kong</option>
                                                                            <option value="Hungary">Hungary</option>
                                                                            <option value="Iceland">Iceland</option>
                                                                            <option value="India">India</option>
                                                                            <option value="Indonesia">Indonesia</option>
                                                                            <option value="Iran">Iran (Islamic Republic of)</option>
                                                                            <option value="Iraq">Iraq</option>
                                                                            <option value="Ireland">Ireland</option>
                                                                            <option value="Israel">Israel</option>
                                                                            <option value="Italy">Italy</option>
                                                                            <option value="Jamaica">Jamaica</option>
                                                                            <option value="Japan">Japan</option>
                                                                            <option value="Jordan">Jordan</option>
                                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                                            <option value="Kenya">Kenya</option>
                                                                            <option value="Kiribati">Kiribati</option>
                                                                            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                            <option value="Korea">Korea, Republic of</option>
                                                                            <option value="Kuwait">Kuwait</option>
                                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                            <option value="Lao">Lao People's Democratic Republic</option>
                                                                            <option value="Latvia">Latvia</option>
                                                                            <option value="Lebanon">Lebanon</option>
                                                                            <option value="Lesotho">Lesotho</option>
                                                                            <option value="Liberia">Liberia</option>
                                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                                            <option value="Lithuania">Lithuania</option>
                                                                            <option value="Luxembourg">Luxembourg</option>
                                                                            <option value="Macau">Macau</option>
                                                                            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                            <option value="Madagascar">Madagascar</option>
                                                                            <option value="Malawi">Malawi</option>
                                                                            <option value="Malaysia">Malaysia</option>
                                                                            <option value="Maldives">Maldives</option>
                                                                            <option value="Mali">Mali</option>
                                                                            <option value="Malta">Malta</option>
                                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                                            <option value="Martinique">Martinique</option>
                                                                            <option value="Mauritania">Mauritania</option>
                                                                            <option value="Mauritius">Mauritius</option>
                                                                            <option value="Mayotte">Mayotte</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Micronesia">Micronesia, Federated States of</option>
                                                                            <option value="Moldova">Moldova, Republic of</option>
                                                                            <option value="Monaco">Monaco</option>
                                                                            <option value="Mongolia">Mongolia</option>
                                                                            <option value="Montserrat">Montserrat</option>
                                                                            <option value="Morocco">Morocco</option>
                                                                            <option value="Mozambique">Mozambique</option>
                                                                            <option value="Myanmar">Myanmar</option>
                                                                            <option value="Namibia">Namibia</option>
                                                                            <option value="Nauru">Nauru</option>
                                                                            <option value="Nepal">Nepal</option>
                                                                            <option value="Netherlands">Netherlands</option>
                                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                            <option value="New Caledonia">New Caledonia</option>
                                                                            <option value="New Zealand">New Zealand</option>
                                                                            <option value="Nicaragua">Nicaragua</option>
                                                                            <option value="Niger">Niger</option>
                                                                            <option value="Nigeria">Nigeria</option>
                                                                            <option value="Niue">Niue</option>
                                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                            <option value="Norway">Norway</option>
                                                                            <option value="Oman">Oman</option>
                                                                            <option value="Pakistan">Pakistan</option>
                                                                            <option value="Palau">Palau</option>
                                                                            <option value="Panama">Panama</option>
                                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Philippines">Philippines</option>
                                                                            <option value="Pitcairn">Pitcairn</option>
                                                                            <option value="Poland">Poland</option>
                                                                            <option value="Portugal">Portugal</option>
                                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                                            <option value="Qatar">Qatar</option>
                                                                            <option value="Reunion">Reunion</option>
                                                                            <option value="Romania">Romania</option>
                                                                            <option value="Russia">Russian Federation</option>
                                                                            <option value="Rwanda">Rwanda</option>
                                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                            <option value="Saint LUCIA">Saint LUCIA</option>
                                                                            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                            <option value="Samoa">Samoa</option>
                                                                            <option value="San Marino">San Marino</option>
                                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                                            <option value="Senegal">Senegal</option>
                                                                            <option value="Seychelles">Seychelles</option>
                                                                            <option value="Sierra">Sierra Leone</option>
                                                                            <option value="Singapore">Singapore</option>
                                                                            <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                            <option value="Slovenia">Slovenia</option>
                                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                                            <option value="Somalia">Somalia</option>
                                                                            <option value="South Africa">South Africa</option>
                                                                            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                            <option value="Span">Spain</option>
                                                                            <option value="SriLanka">Sri Lanka</option>
                                                                            <option value="St. Helena">St. Helena</option>
                                                                            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                            <option value="Sudan">Sudan</option>
                                                                            <option value="Suriname">Suriname</option>
                                                                            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="Swaziland">Swaziland</option>
                                                                            <option value="Sweden">Sweden</option>
                                                                            <option value="Switzerland">Switzerland</option>
                                                                            <option value="Syria">Syrian Arab Republic</option>
                                                                            <option value="Taiwan">Taiwan, Province of China</option>
                                                                            <option value="Tajikistan">Tajikistan</option>
                                                                            <option value="Tanzania">Tanzania, United Republic of</option>
                                                                            <option value="Thailand">Thailand</option>
                                                                            <option value="Togo">Togo</option>
                                                                            <option value="Tokelau">Tokelau</option>
                                                                            <option value="Tonga">Tonga</option>
                                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                            <option value="Tunisia">Tunisia</option>
                                                                            <option value="Turkey">Turkey</option>
                                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                                            <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                            <option value="Tuvalu">Tuvalu</option>
                                                                            <option value="Uganda">Uganda</option>
                                                                            <option value="Ukraine">Ukraine</option>
                                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                                            <option value="United Kingdom" selected>United Kingdom</option>
                                                                            <option value="United States">United States</option>
                                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                                            <option value="Vanuatu">Vanuatu</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Vietnam">Viet Nam</option>
                                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                            <option value="Western Sahara">Western Sahara</option>
                                                                            <option value="Yemen">Yemen</option>
                                                                            <option value="Yugoslavia">Yugoslavia</option>
                                                                            <option value="Zambia">Zambia</option>
                                                                            <option value="Zimbabwe">Zimbabwe</option>



                                                                        </select>
                                                                    </label>
                                                                </section>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane" id="tab4" runat="server" clientidmode="Static">

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div id="Div3" class="smart-form">

                                                            <div class="row">
                                                                <section class="col col-2">
                                                                    <label class="label">Passport No.</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtPassportNo" runat="server" class="form-control" placeholder="Passport No." Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Passport Issue Country</label>
                                                                    <label class="input">

                                                                        <select id="cboPassportIssueCountry" class="form-control" runat="server">

                                                                            <option value="Afghanistan">Afghanistan</option>
                                                                            <option value="Albania">Albania</option>
                                                                            <option value="Algeria">Algeria</option>
                                                                            <option value="American Samoa">American Samoa</option>
                                                                            <option value="Andorra">Andorra</option>
                                                                            <option value="Angola">Angola</option>
                                                                            <option value="Anguilla">Anguilla</option>
                                                                            <option value="Antartica">Antarctica</option>
                                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                                            <option value="Argentina">Argentina</option>
                                                                            <option value="Armenia">Armenia</option>
                                                                            <option value="Aruba">Aruba</option>
                                                                            <option value="Australia">Australia</option>
                                                                            <option value="Austria">Austria</option>
                                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                                            <option value="Bahamas">Bahamas</option>
                                                                            <option value="Bahrain">Bahrain</option>
                                                                            <option value="Bangladesh">Bangladesh</option>
                                                                            <option value="Barbados">Barbados</option>
                                                                            <option value="Belarus">Belarus</option>
                                                                            <option value="Belgium">Belgium</option>
                                                                            <option value="Belize">Belize</option>
                                                                            <option value="Benin">Benin</option>
                                                                            <option value="Bermuda">Bermuda</option>
                                                                            <option value="Bhutan">Bhutan</option>
                                                                            <option value="Bolivia">Bolivia</option>
                                                                            <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                                                                            <option value="Botswana">Botswana</option>
                                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                                            <option value="Brazil">Brazil</option>
                                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                                            <option value="Bulgaria">Bulgaria</option>
                                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                                            <option value="Burundi">Burundi</option>
                                                                            <option value="Cambodia">Cambodia</option>
                                                                            <option value="Cameroon">Cameroon</option>
                                                                            <option value="Canada">Canada</option>
                                                                            <option value="Cape Verde">Cape Verde</option>
                                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                                            <option value="Central African Republic">Central African Republic</option>
                                                                            <option value="Chad">Chad</option>
                                                                            <option value="Chile">Chile</option>
                                                                            <option value="China">China</option>
                                                                            <option value="Christmas Island">Christmas Island</option>
                                                                            <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                                            <option value="Colombia">Colombia</option>
                                                                            <option value="Comoros">Comoros</option>
                                                                            <option value="Congo">Congo</option>
                                                                            <option value="Congo">Congo, the Democratic Republic of the</option>
                                                                            <option value="Cook Islands">Cook Islands</option>
                                                                            <option value="Costa Rica">Costa Rica</option>
                                                                            <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                                            <option value="Croatia">Croatia (Hrvatska)</option>
                                                                            <option value="Cuba">Cuba</option>
                                                                            <option value="Cyprus">Cyprus</option>
                                                                            <option value="Czech Republic">Czech Republic</option>
                                                                            <option value="Denmark">Denmark</option>
                                                                            <option value="Djibouti">Djibouti</option>
                                                                            <option value="Dominica">Dominica</option>
                                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                                            <option value="East Timor">East Timor</option>
                                                                            <option value="Ecuador">Ecuador</option>
                                                                            <option value="Egypt">Egypt</option>
                                                                            <option value="El Salvador">El Salvador</option>
                                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                                            <option value="Eritrea">Eritrea</option>
                                                                            <option value="Estonia">Estonia</option>
                                                                            <option value="Ethiopia">Ethiopia</option>
                                                                            <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                                            <option value="Fiji">Fiji</option>
                                                                            <option value="Finland">Finland</option>
                                                                            <option value="France">France</option>
                                                                            <option value="France Metropolitan">France, Metropolitan</option>
                                                                            <option value="French Guiana">French Guiana</option>
                                                                            <option value="French Polynesia">French Polynesia</option>
                                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                                            <option value="Gabon">Gabon</option>
                                                                            <option value="Gambia">Gambia</option>
                                                                            <option value="Georgia">Georgia</option>
                                                                            <option value="Germany">Germany</option>
                                                                            <option value="Ghana">Ghana</option>
                                                                            <option value="Gibraltar">Gibraltar</option>
                                                                            <option value="Greece">Greece</option>
                                                                            <option value="Greenland">Greenland</option>
                                                                            <option value="Grenada">Grenada</option>
                                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                                            <option value="Guam">Guam</option>
                                                                            <option value="Guatemala">Guatemala</option>
                                                                            <option value="Guinea">Guinea</option>
                                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                                            <option value="Guyana">Guyana</option>
                                                                            <option value="Haiti">Haiti</option>
                                                                            <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                                                                            <option value="Holy See">Holy See (Vatican City State)</option>
                                                                            <option value="Honduras">Honduras</option>
                                                                            <option value="Hong Kong">Hong Kong</option>
                                                                            <option value="Hungary">Hungary</option>
                                                                            <option value="Iceland">Iceland</option>
                                                                            <option value="India">India</option>
                                                                            <option value="Indonesia">Indonesia</option>
                                                                            <option value="Iran">Iran (Islamic Republic of)</option>
                                                                            <option value="Iraq">Iraq</option>
                                                                            <option value="Ireland">Ireland</option>
                                                                            <option value="Israel">Israel</option>
                                                                            <option value="Italy">Italy</option>
                                                                            <option value="Jamaica">Jamaica</option>
                                                                            <option value="Japan">Japan</option>
                                                                            <option value="Jordan">Jordan</option>
                                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                                            <option value="Kenya">Kenya</option>
                                                                            <option value="Kiribati">Kiribati</option>
                                                                            <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                                                            <option value="Korea">Korea, Republic of</option>
                                                                            <option value="Kuwait">Kuwait</option>
                                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                                            <option value="Lao">Lao People's Democratic Republic</option>
                                                                            <option value="Latvia">Latvia</option>
                                                                            <option value="Lebanon">Lebanon</option>
                                                                            <option value="Lesotho">Lesotho</option>
                                                                            <option value="Liberia">Liberia</option>
                                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                                            <option value="Lithuania">Lithuania</option>
                                                                            <option value="Luxembourg">Luxembourg</option>
                                                                            <option value="Macau">Macau</option>
                                                                            <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                                                                            <option value="Madagascar">Madagascar</option>
                                                                            <option value="Malawi">Malawi</option>
                                                                            <option value="Malaysia">Malaysia</option>
                                                                            <option value="Maldives">Maldives</option>
                                                                            <option value="Mali">Mali</option>
                                                                            <option value="Malta">Malta</option>
                                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                                            <option value="Martinique">Martinique</option>
                                                                            <option value="Mauritania">Mauritania</option>
                                                                            <option value="Mauritius">Mauritius</option>
                                                                            <option value="Mayotte">Mayotte</option>
                                                                            <option value="Mexico">Mexico</option>
                                                                            <option value="Micronesia">Micronesia, Federated States of</option>
                                                                            <option value="Moldova">Moldova, Republic of</option>
                                                                            <option value="Monaco">Monaco</option>
                                                                            <option value="Mongolia">Mongolia</option>
                                                                            <option value="Montserrat">Montserrat</option>
                                                                            <option value="Morocco">Morocco</option>
                                                                            <option value="Mozambique">Mozambique</option>
                                                                            <option value="Myanmar">Myanmar</option>
                                                                            <option value="Namibia">Namibia</option>
                                                                            <option value="Nauru">Nauru</option>
                                                                            <option value="Nepal">Nepal</option>
                                                                            <option value="Netherlands">Netherlands</option>
                                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                                            <option value="New Caledonia">New Caledonia</option>
                                                                            <option value="New Zealand">New Zealand</option>
                                                                            <option value="Nicaragua">Nicaragua</option>
                                                                            <option value="Niger">Niger</option>
                                                                            <option value="Nigeria">Nigeria</option>
                                                                            <option value="Niue">Niue</option>
                                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                                            <option value="Norway">Norway</option>
                                                                            <option value="Oman">Oman</option>
                                                                            <option value="Pakistan">Pakistan</option>
                                                                            <option value="Palau">Palau</option>
                                                                            <option value="Panama">Panama</option>
                                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                                            <option value="Paraguay">Paraguay</option>
                                                                            <option value="Peru">Peru</option>
                                                                            <option value="Philippines">Philippines</option>
                                                                            <option value="Pitcairn">Pitcairn</option>
                                                                            <option value="Poland">Poland</option>
                                                                            <option value="Portugal">Portugal</option>
                                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                                            <option value="Qatar">Qatar</option>
                                                                            <option value="Reunion">Reunion</option>
                                                                            <option value="Romania">Romania</option>
                                                                            <option value="Russia">Russian Federation</option>
                                                                            <option value="Rwanda">Rwanda</option>
                                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                                            <option value="Saint LUCIA">Saint LUCIA</option>
                                                                            <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                                                            <option value="Samoa">Samoa</option>
                                                                            <option value="San Marino">San Marino</option>
                                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                                            <option value="Senegal">Senegal</option>
                                                                            <option value="Seychelles">Seychelles</option>
                                                                            <option value="Sierra">Sierra Leone</option>
                                                                            <option value="Singapore">Singapore</option>
                                                                            <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                                            <option value="Slovenia">Slovenia</option>
                                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                                            <option value="Somalia">Somalia</option>
                                                                            <option value="South Africa">South Africa</option>
                                                                            <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                                                            <option value="Span">Spain</option>
                                                                            <option value="SriLanka">Sri Lanka</option>
                                                                            <option value="St. Helena">St. Helena</option>
                                                                            <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                                                            <option value="Sudan">Sudan</option>
                                                                            <option value="Suriname">Suriname</option>
                                                                            <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                                            <option value="Swaziland">Swaziland</option>
                                                                            <option value="Sweden">Sweden</option>
                                                                            <option value="Switzerland">Switzerland</option>
                                                                            <option value="Syria">Syrian Arab Republic</option>
                                                                            <option value="Taiwan">Taiwan, Province of China</option>
                                                                            <option value="Tajikistan">Tajikistan</option>
                                                                            <option value="Tanzania">Tanzania, United Republic of</option>
                                                                            <option value="Thailand">Thailand</option>
                                                                            <option value="Togo">Togo</option>
                                                                            <option value="Tokelau">Tokelau</option>
                                                                            <option value="Tonga">Tonga</option>
                                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                                            <option value="Tunisia">Tunisia</option>
                                                                            <option value="Turkey">Turkey</option>
                                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                                            <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                                            <option value="Tuvalu">Tuvalu</option>
                                                                            <option value="Uganda">Uganda</option>
                                                                            <option value="Ukraine">Ukraine</option>
                                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                                            <option value="United Kingdom" selected>United Kingdom</option>
                                                                            <option value="United States">United States</option>
                                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                                            <option value="Uruguay">Uruguay</option>
                                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                                            <option value="Vanuatu">Vanuatu</option>
                                                                            <option value="Venezuela">Venezuela</option>
                                                                            <option value="Vietnam">Viet Nam</option>
                                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                                            <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                                            <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                                                                            <option value="Western Sahara">Western Sahara</option>
                                                                            <option value="Yemen">Yemen</option>
                                                                            <option value="Yugoslavia">Yugoslavia</option>
                                                                            <option value="Zambia">Zambia</option>
                                                                            <option value="Zimbabwe">Zimbabwe</option>



                                                                        </select>

                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Passport Issue Date</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-calendar"></i>
                                                                        <input type="text" name="request" id="txtPassportIssueDate" placeholder="Passport Issue Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Passport Expiry Date</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-calendar"></i>
                                                                        <input type="text" name="request" id="txtPassportExpiryDate" placeholder="Passport Expiry Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />

                                                                    </label>

                                                                </section>
                                                            </div>

                                                            <div class="row">
                                                                <section class="col col-4">
                                                                    <label class="label">Visa Type</label>
                                                                    <label class="input">

                                                                        <asp:TextBox ID="txtVisaType" runat="server" class="form-control" placeholder="Visa Type" Text=""></asp:TextBox>

                                                                    </label>

                                                                </section>

                                                                <section class="col col-2">
                                                                    <label class="label">Visa Issue Date</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-calendar"></i>
                                                                        <input type="text" name="request" id="txtVisaStartDate" placeholder="Visa Issue Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                                    </label>

                                                                </section>
                                                                <section class="col col-2">
                                                                    <label class="label">Visa Expiry Date</label>
                                                                    <label class="input">
                                                                        <i class="icon-append fa fa-calendar"></i>
                                                                        <input type="text" name="request" id="txtVisaEndDate" placeholder="Visa Expiry Date" class="datepicker" data-dateformat='dd/mm/yy' runat="server" />
                                                                    </label>

                                                                </section>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="tab-pane" id="tab5" runat="server" clientidmode="Static">

                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                                        <br />
                                                        <br />

                                                        <div class="row">
                                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                                <div class="input-group">
                                                                    <asp:FileUpload class="btn btn-default" ID="FileUpload1" runat="server" />

                                                                </div>
                                                            </div>
                                                            <div class="col-xs-3 col-sm-9 col-md-9 col-lg-9 text-left">

                                                                <a id="A1" class="btn bg-color-blueDark txt-color-white" href="#" onserverclick="cmdSave_Click" runat="server">
                                                                    <i class="fa fa-plus"></i><span class="hidden-mobile">Upload </span>
                                                                </a>
                                                                
                                                            </div>

                                                        </div>
                                                        <br />
                                                        <br />
                                                        <asp:Repeater ID="MyRepeater" runat="server" ClientIDMode="AutoID" EnableTheming="False">
                                                            <HeaderTemplate>
                                                                <table id="datatable_fixed_column" class="table " width="100%">

                                                                    <thead>

                                                                        <tr>
                                                                            <th data-hide="phone">No</th>
                                                                            <th data-hide="phone">Document Name</th>


                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>

                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "No")%></td>
                                                                    <td><%# DataBinder.Eval(Container.DataItem, "DocumentName")%></td>



                                                                    <td>
                                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="getid" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnEdit_Click"><i class="fa fa-eye"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                        <asp:LinkButton class="btn bg-color-blueDark txt-color-white" ID="btndel" runat="server" Text="Click" CommandArgument='<%#Eval("ID")%>' Visible="true" OnClick="btnDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this record?')) return false;"><i class="fa fa-times" aria-hidden="true"></i><span class="hidden-mobile"> </span></asp:LinkButton>
                                                                    </td>

                                                                </tr>

                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                            </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <br />
                                                    </div>



                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end widget content -->

                                </div>
                                <!-- end widget div -->

                            </div>
                            <!-- end widget -->

                        </td>

                    </tr>

                    <tr>

                        <td style="width: 100%">
                            <div class="col-lg-12" style="text-align: right">

                                <asp:Button ID="btnSave" class="btn bg-color-blueDark txt-color-white" Style="width: 100px" runat="server" Text="Save" OnClick="btnSave_Click" />
                                <input action="action" type="button" class="btn bg-color-blueDark txt-color-white" value="Cancel" style="width: 100px" onclick="history.go(-1);" />

                            </div>
                        </td>
                    </tr>



                </tbody>
            </table>













            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>



    <div class="modal fade" id="myModal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <asp:Label ID="lblheader" runat="server" Text="Message"></asp:Label></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label ID="lblmsg" runat="server" Text="Message"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
    </div>


</asp:Content>
