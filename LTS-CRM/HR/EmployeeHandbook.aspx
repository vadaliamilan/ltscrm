﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MasterAdmin.Master" AutoEventWireup="true" CodeBehind="EmployeeHandbook.aspx.cs" Inherits="LTS_CRM.HR.EmployeeHandBook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <div class="well">
        <div id="ribbon">
            <span class="ribbon-button-alignment">
                <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                    <i class="fa fa-refresh"></i>
                </span>
            </span>
            <ol class="breadcrumb">
                <li>Employee Handbook</li>
               
            </ol>
        </div>
        <section id="Section1" class="">
            <div class="row">
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="empdir" data-widget-editbutton="false">
                        <header>
                            <span class="widget-icon"><i class="fa fa-list"></i></span>
                            <h2>Employee Handbook </h2>
                        </header>
     <div runat="server" id="lblpdfviewer" name="lblpdfviewer" style="width: 100%; height: 1600px"></div>
                        <hr />
<asp:Literal ID="ltEmbed" runat="server" />
                        </div>
                    </article>
                </div>
            </section>
             </div>
</asp:Content>
