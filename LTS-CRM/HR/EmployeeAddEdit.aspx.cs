﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LTS_CRM.HR
{
    public partial class EmployeeAddEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                LoadcboCompanyID();
                LoadcboDepartmentID();
                LoadcboEmploymentStatus();
                LoadcboJobGrade();
                LoadcboEmpRole();
                BindGrid();
                SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                Connection.Open();
                imageupload.AllowMultiple = false;
                FileUpload1.AllowMultiple = true;
                Session.Add("UserName", "Nilesh");
                Session.Add("EmpID", "EMP0001");
                if (SessionValue("AutoID").ToString().Length > 0)
                {


                    String sql = "select * from [dbo].[Employee] where [ID] = @ID";

                    SqlCommand Command = new SqlCommand(sql, Connection);
                    Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
                    SqlDataReader Reader = Command.ExecuteReader();

                    if (Reader.Read())
                    {

                        cboCompanyID.SelectedValue = Reader["CompanyID"].ToString();

                        if (Reader["Image"] != null && Reader["Image"] != DBNull.Value)
                        {
                            Session.Add("imagesrc", Reader["Image"]);
                            byte[] img = (byte[])(Reader["Image"]);

                            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                            Bitmap MyBitmap = (Bitmap)tc.ConvertFrom(img);
                            string imgString = Convert.ToBase64String(img);
                            lmglogo.Src = String.Format("data:image/Bmp;base64,{0}\"", imgString);
                        }

                        Session.Add("userpassword", Reader["SecurePassword"]);
                        cboUserStatus.SelectedValue = Reader["UserStatus"].ToString();
                        txtEmployeeID.Text = Reader["EmployeeID"].ToString();
                        cboCompanyID.SelectedValue = Reader["CompanyID"].ToString();
                        cboDepartmentID.SelectedValue = Reader["DepartmentID"].ToString();
                        cboPrefix.SelectedValue = Reader["Prefix"].ToString();
                        txtEmailAddress.Text = Reader["EmailAddress"].ToString();
                        txtSecurePassword.Text = Reader["SecurePassword"].ToString();
                        txtFirstName.Text = Reader["FirstName"].ToString();
                        txtSurName.Text = Reader["SurName"].ToString();
                        txtFirstNameChinese.Text = Reader["FirstNameChinese"].ToString();
                        txtSurnameChinese.Text = Reader["SurnameChinese"].ToString();
                        cboMaritialStatus.SelectedValue = Reader["MaritialStatus"].ToString();
                        cmbemprole.SelectedValue = Reader["EmpRoleID"].ToString();

                        String s1;
                        s1 = Reader["DateofBirth"].ToString();
                        if (IsDate(s1))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s1);
                            txtDateofBirth.Value = dt.ToString("dd/MM/yyyy");
                        }
                        
                        txtPlaceofBirth.Text = Reader["PlaceofBirth"].ToString();
                        cboJobGrade.SelectedValue = Reader["JobGrade"].ToString();
                        cboGender.SelectedValue = Reader["Gender"].ToString();
                        txtPassportNo.Text = Reader["PassportNo"].ToString();
                       cboPassportIssueCountry.Value = Reader["PassportIssueCountry"].ToString();
                       String s2;
                       s2 = Reader["PassportIssueDate"].ToString();
                       if (IsDate(s2))
                       {
                           DateTime dt = new DateTime();
                           dt = Convert.ToDateTime(s2);
                           txtPassportIssueDate.Value = dt.ToString("dd/MM/yyyy");
                       }
                       String s3;
                       s3 = Reader["PassportExpiryDate"].ToString();
                       if (IsDate(s3))
                       {
                           DateTime dt = new DateTime();
                           dt = Convert.ToDateTime(s3);
                           txtPassportExpiryDate.Value = dt.ToString("dd/MM/yyyy");
                       }
                        
                        txtVisaType.Text = Reader["VisaType"].ToString();
                        String s4;
                        s4 = Reader["VisaStartDate"].ToString();
                        if (IsDate(s4))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s4);
                            txtVisaStartDate.Value = dt.ToString("dd/MM/yyyy");
                        }
                        String s5;
                        s5 = Reader["VisaEndDate"].ToString();
                        if (IsDate(s5))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s5);
                            txtVisaEndDate.Value = dt.ToString("dd/MM/yyyy");
                        }
                       
                        txtLandLineNumberr.Text = Reader["LandLineNumberr"].ToString();
                        txtMobileNo.Text = Reader["MobileNo"].ToString();
                        txtPersonalEmail.Text = Reader["PersonalEmail"].ToString();
                        txtPersonalWeChat.Text = Reader["PersonalWeChat"].ToString();
                        txtPersonWhatsup.Text = Reader["PersonWhatsup"].ToString();
                        txtPersonQQ.Text = Reader["PersonQQ"].ToString();
                        txtAddress.Text = Reader["Address1"].ToString();
                        //txtAddress2.Text = Reader["Address2"].ToString();
                        txtPostcode.Text = Reader["Postcode"].ToString();
                        txtCity.Text = Reader["City"].ToString();
                        txtState.Text = Reader["State"].ToString();
                        txtCountry.Value = Reader["Country"].ToString();
                        txtNationalInsurance.Text = Reader["NationalInsurance"].ToString();
                        //txtRole.Text = Reader["Role"].ToString();
                        
                        //txtPosition.Text = Reader["Position"].ToString();
                        txtJobTitle.Text = Reader["JobTitle"].ToString();
                        //txtJobDetail.Text = Reader["JobDetail"].ToString();
                        cboEmployementStatus.SelectedValue = Reader["EmploymentStatus"].ToString();
                        String s6;
                        s6 = Reader["DateofJoining"].ToString();
                        if (IsDate(s6))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s6);
                            txtDateofJoining.Value = dt.ToString("dd/MM/yyyy");
                        }
                        String s7;
                        s7 = Reader["DateofLeaving"].ToString();
                        if (IsDate(s7))
                        {
                            DateTime dt = new DateTime();
                            dt = Convert.ToDateTime(s7);
                            txtDateofLeaving.Value = dt.ToString("dd/MM/yyyy");
                        }
                       
                        //txtYearofExperience.Text = Reader["YearofExperience"].ToString();
                        txtWorkTelephoneNo.Text = Reader["WorkTelephoneNo"].ToString();
                        txtExtention.Text = Reader["Extention"].ToString();
                        txtWorkMobileNo.Text = Reader["WorkMobileNo"].ToString();
                        //txtComment.Text = Reader["Comment"].ToString();
                        txtNKAFirstName.Text = Reader["NKAFirstName"].ToString();
                        txtNKAMiddleName.Text = Reader["NKAMiddleName"].ToString();
                        txtNKASurName.Text = Reader["NKASurName"].ToString();
                        txtNKAFirstNameChinese.Text = Reader["NKAFirstNameChines"].ToString();
                        txtNKASurNameChiness.Text = Reader["NKASurNameChiness"].ToString();
                        txtNKAMiddleNameChiness.Text = Reader["NKAMiddleChiness"].ToString();
                        cboNKASex.SelectedValue = Reader["NKASex"].ToString();
                        cboNKARelationship.SelectedValue = Reader["NKARelationship"].ToString();
                        txtNKALandLineNumber.Text = Reader["NKALandLineNumber"].ToString();
                        txtNKAMobileNo.Text = Reader["NKAMobileNo"].ToString();
                        txtNKAEmailAddress.Text = Reader["NKAEmailAddress"].ToString();
                        txtNKAHomeAddress.Text = Reader["NKAHomeAddress"].ToString();
                        txtNKACity.Text = Reader["NKACity"].ToString();
                        txtNKAState.Text = Reader["NKAState"].ToString();
                        txtNKACountry.Value = Reader["NKACountry"].ToString();
                        txtNKBFirstName.Text = Reader["NKBFirstName"].ToString();
                        txtNKBMiddleName.Text = Reader["NKBMiddleName"].ToString();
                        txtNKBSurName.Text = Reader["NKBSurName"].ToString();
                        txtNKBFirstName.Text = Reader["NKBFirstName"].ToString();
                        txtNKBFirstNameChines.Text = Reader["NKBFirstNameChinese"].ToString();
                        
                        txtNKBSurNameChines.Text = Reader["NKBSurNameChines"].ToString();
                        txtNKBMiddleChiness.Text = Reader["NKBMiddleChiness"].ToString();
                        cboNKBSex.SelectedValue = Reader["NKBSex"].ToString();
                        txtNKBRelationship.SelectedValue = Reader["NKBRelationship"].ToString();
                        txtNKBLandLineNumber.Text = Reader["NKBLandLineNumber"].ToString();
                        txtNKBMobileNo.Text = Reader["NKBMobileNo"].ToString();
                        txtNKBEmailAddress.Text = Reader["NKBEmailAddress"].ToString();
                        txtNKBHomeAddress.Text = Reader["NKBHomeAddress"].ToString();
                        txtNKBCity.Text = Reader["NKBCity"].ToString();
                        txtNKBState.Text = Reader["NKBState"].ToString();
                        txtNKBCountry.Value = Reader["NKBCountry"].ToString();
                      


                    }

                    Reader.Close();
                    Connection.Close();
                }
                else
                {
                    imageupload.AllowMultiple = false;
                   
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            SqlCommand Command;

            String InsertSQL = "Insert into [dbo].[Employee] ([UserStatus], [EmployeeID], [CompanyID], [DepartmentID], [Prefix], [EmailAddress], [SecurePassword], [FirstName], [SurName], [FirstNameChinese], [SurnameChinese], [MaritialStatus], [DateofBirth], [PlaceofBirth], [Gender], [PassportNo], [PassportIssueCountry], [PassportIssueDate], [PassportExpiryDate], [VisaType], [VisaStartDate], [VisaEndDate], [LandLineNumberr], [MobileNo], [PersonalEmail], [PersonalWeChat], [PersonWhatsup], [PersonQQ], [Address1],  [Postcode], [City], [State], [Country], [NationalInsurance], [JobGrade], [JobTitle], [EmploymentStatus], [DateofJoining], [DateofLeaving], [WorkTelephoneNo], [Extention], [WorkMobileNo], [NKAFirstName], [NKAMiddleName], [NKASurName], [NKAFirstNameChines], [NKASurNameChiness], [NKAMiddleChiness], [NKASex], [NKARelationship], [NKALandLineNumber], [NKAMobileNo], [NKAEmailAddress], [NKAHomeAddress], [NKACity], [NKAState], [NKACountry], [NKBFirstName], [NKBMiddleName], [NKBSurName], [NKBFirstNameChinese], [NKBSurNameChines], [NKBMiddleChiness], [NKBSex], [NKBRelationship], [NKBLandLineNumber], [NKBMobileNo], [NKBEmailAddress], [NKBHomeAddress], [NKBCity], [NKBState], [NKBCountry], [CreatedBy], [CreatedDate],  [Image],EmpRoleID ) VALUES ( @UserStatus, @EmployeeID, @CompanyID, @DepartmentID, @Prefix, @EmailAddress, @SecurePassword, @FirstName, @SurName, @FirstNameChinese, @SurnameChinese, @MaritialStatus, @DateofBirth, @PlaceofBirth, @Gender, @PassportNo, @PassportIssueCountry, @PassportIssueDate, @PassportExpiryDate, @VisaType, @VisaStartDate, @VisaEndDate, @LandLineNumberr, @MobileNo, @PersonalEmail, @PersonalWeChat, @PersonWhatsup, @PersonQQ, @Address1,  @Postcode, @City, @State, @Country, @NationalInsurance, @JobGrade, @JobTitle, @EmploymentStatus, @DateofJoining, @DateofLeaving, @WorkTelephoneNo, @Extention, @WorkMobileNo, @NKAFirstName, @NKAMiddleName, @NKASurName, @NKAFirstNameChines, @NKASurNameChiness, @NKAMiddleChiness, @NKASex, @NKARelationship, @NKALandLineNumber, @NKAMobileNo, @NKAEmailAddress, @NKAHomeAddress, @NKACity, @NKAState, @NKACountry, @NKBFirstName, @NKBMiddleName, @NKBSurName, @NKBFirstNameChinese, @NKBSurNameChines, @NKBMiddleChiness, @NKBSex, @NKBRelationship, @NKBLandLineNumber, @NKBMobileNo, @NKBEmailAddress, @NKBHomeAddress, @NKBCity, @NKBState, @NKBCountry, @CreatedBy, @CreatedDate, @Image ,@EmpRoleID) ";

            String UpdateSQL = "Update [dbo].[Employee] set [UserStatus] = @UserStatus, [EmployeeID] = @EmployeeID, [CompanyID] = @CompanyID, [DepartmentID] = @DepartmentID, [Prefix] = @Prefix, [EmailAddress] = @EmailAddress, [SecurePassword] = @SecurePassword, [FirstName] = @FirstName, [SurName] = @SurName, [FirstNameChinese] = @FirstNameChinese, [SurnameChinese] = @SurnameChinese, [MaritialStatus] = @MaritialStatus, [DateofBirth] = @DateofBirth, [PlaceofBirth] = @PlaceofBirth, [Gender] = @Gender, [PassportNo] = @PassportNo, [PassportIssueCountry] = @PassportIssueCountry, [PassportIssueDate] = @PassportIssueDate, [PassportExpiryDate] = @PassportExpiryDate, [VisaType] = @VisaType, [VisaStartDate] = @VisaStartDate, [VisaEndDate] = @VisaEndDate, [LandLineNumberr] = @LandLineNumberr, [MobileNo] = @MobileNo, [PersonalEmail] = @PersonalEmail, [PersonalWeChat] = @PersonalWeChat, [PersonWhatsup] = @PersonWhatsup, [PersonQQ] = @PersonQQ, [Address1] = @Address1,  [Postcode] = @Postcode, [City] = @City, [State] = @State, [Country] = @Country, [NationalInsurance] = @NationalInsurance, [JobGrade] = @JobGrade, [JobTitle] = @JobTitle, [EmploymentStatus] = @EmploymentStatus, [DateofJoining] = @DateofJoining, [DateofLeaving] = @DateofLeaving, [WorkTelephoneNo] = @WorkTelephoneNo, [Extention] = @Extention, [WorkMobileNo] = @WorkMobileNo, [NKAFirstName] = @NKAFirstName, [NKAMiddleName] = @NKAMiddleName, [NKASurName] = @NKASurName, [NKAFirstNameChines] = @NKAFirstNameChines, [NKASurNameChiness] = @NKASurNameChiness, [NKAMiddleChiness] = @NKAMiddleChiness, [NKASex] = @NKASex, [NKARelationship] = @NKARelationship, [NKALandLineNumber] = @NKALandLineNumber, [NKAMobileNo] = @NKAMobileNo, [NKAEmailAddress] = @NKAEmailAddress, [NKAHomeAddress] = @NKAHomeAddress, [NKACity] = @NKACity, [NKAState] = @NKAState, [NKACountry] = @NKACountry, [NKBFirstName] = @NKBFirstName, [NKBMiddleName] = @NKBMiddleName, [NKBSurName] = @NKBSurName, [NKBFirstNameChinese] = @NKBFirstNameChinese, [NKBSurNameChines] = @NKBSurNameChines, [NKBMiddleChiness] = @NKBMiddleChiness, [NKBSex] = @NKBSex, [NKBRelationship] = @NKBRelationship, [NKBLandLineNumber] = @NKBLandLineNumber, [NKBMobileNo] = @NKBMobileNo, [NKBEmailAddress] = @NKBEmailAddress, [NKBHomeAddress] = @NKBHomeAddress, [NKBCity] = @NKBCity, [NKBState] = @NKBState, [NKBCountry] = @NKBCountry,  [UpdatedBy] = @UpdatedBy, [UpdatedDate] = @UpdatedDate, [Image] = @Image,EmpRoleID=@EmpRoleID where [ID] = @ID ";


            if (String.IsNullOrEmpty(ID))
            {
                Command = new SqlCommand(InsertSQL, Connection);
                Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);
                txtEmployeeID.Text = comman.EmpRunNo();
            }
            else
            {
                Command = new SqlCommand(UpdateSQL, Connection);
                Command.Parameters.AddWithValue("@ID", ID);
                Command.Parameters.AddWithValue("@UpdatedBy", SessionValue("UserName"));
                Command.Parameters.AddWithValue("@UpdatedDate", DateTime.Today);
                if (txtSecurePassword.Text == "")
                {
                    txtSecurePassword.Text  = SessionValue("userpassword");
                }
            }
            
            FileUpload img = (FileUpload)imageupload;
            Byte[] imgByte = null;
            if (img.HasFile && img.PostedFile != null)
            {
                //To create a PostedFile
                HttpPostedFile File = imageupload.PostedFile;
                //Create byte Array with file len
                imgByte = new Byte[File.ContentLength];
                //force the control to load data in array
                File.InputStream.Read(imgByte, 0, File.ContentLength);
                Command.Parameters.AddWithValue("@Image", imgByte);
            }
            else
            {
                Command.CommandText = Command.CommandText.Replace(", [Image] = @Image", " ");
                Command.CommandText = Command.CommandText.Replace(",  [Image]", " ");
                Command.CommandText = Command.CommandText.Replace(", @Image", " ");
            }

            Command.Parameters.AddWithValue("@UserStatus", cboUserStatus.SelectedValue);
            Command.Parameters.AddWithValue("@EmployeeID", txtEmployeeID.Text);
            if (cboCompanyID.SelectedValue.Length > 0)
            {
                Command.Parameters.AddWithValue("@CompanyID", cboCompanyID.SelectedValue);
            }
            else
            {
                Command.Parameters.AddWithValue("@CompanyID", DBNull.Value);
            }

            if (cboDepartmentID.SelectedValue.Length > 0)
            {
                Command.Parameters.AddWithValue("@DepartmentID", cboDepartmentID.SelectedValue);
            }
            else
            {
                Command.Parameters.AddWithValue("@DepartmentID", DBNull.Value);
            }

            Command.Parameters.AddWithValue("@Prefix", cboPrefix.SelectedValue);
            Command.Parameters.AddWithValue("@EmailAddress", txtEmailAddress.Text);
            Command.Parameters.AddWithValue("@SecurePassword", txtSecurePassword.Text);
            Command.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            Command.Parameters.AddWithValue("@SurName", txtSurName.Text);
            Command.Parameters.AddWithValue("@FirstNameChinese", txtFirstNameChinese.Text);
            Command.Parameters.AddWithValue("@SurnameChinese", txtSurnameChinese.Text);
            Command.Parameters.AddWithValue("@MaritialStatus", cboMaritialStatus.SelectedValue);
            string res = txtDateofBirth.Value;
            if (res != "")
            {
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Command.Parameters.AddWithValue("@DateofBirth", d);
            }
            else
            {
                Command.Parameters.AddWithValue("@DateofBirth", DBNull.Value);
            }           

            Command.Parameters.AddWithValue("@PlaceofBirth", txtPlaceofBirth.Text);
            Command.Parameters.AddWithValue("@Gender", cboGender.SelectedValue);
            Command.Parameters.AddWithValue("@PassportNo", txtPassportNo.Text);
            Command.Parameters.AddWithValue("@PassportIssueCountry", cboPassportIssueCountry.Value);
            res = txtPassportIssueDate.Value;
            if (res != "")
            {
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Command.Parameters.AddWithValue("@PassportIssueDate", d);
            }
            else
            {
                Command.Parameters.AddWithValue("@PassportIssueDate", DBNull.Value);
            }
            res = txtPassportExpiryDate.Value;
            if (res != "")
            {
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Command.Parameters.AddWithValue("@PassportExpiryDate", d);
            }
            else
            {
                Command.Parameters.AddWithValue("@PassportExpiryDate", DBNull.Value);
            }  

            

            Command.Parameters.AddWithValue("@VisaType", txtVisaType.Text);
            res = txtVisaStartDate.Value;
            if (res != "")
            {
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Command.Parameters.AddWithValue("@VisaStartDate", d);
            }
            else
            {
                Command.Parameters.AddWithValue("@VisaStartDate", DBNull.Value);
            }

            res = txtVisaEndDate.Value;
            if (res != "")
            {
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Command.Parameters.AddWithValue("@VisaEndDate", d);
            }
            else
            {
                Command.Parameters.AddWithValue("@VisaEndDate", DBNull.Value);
            }  
            Command.Parameters.AddWithValue("@LandLineNumberr", txtLandLineNumberr.Text);
            Command.Parameters.AddWithValue("@MobileNo", txtMobileNo.Text);
            Command.Parameters.AddWithValue("@PersonalEmail", txtPersonalEmail.Text);
            Command.Parameters.AddWithValue("@PersonalWeChat", txtPersonalWeChat.Text);
            Command.Parameters.AddWithValue("@PersonWhatsup", txtPersonWhatsup.Text);
            Command.Parameters.AddWithValue("@PersonQQ", txtPersonQQ.Text);
            Command.Parameters.AddWithValue("@Address1", txtAddress.Text);
            Command.Parameters.AddWithValue("@Postcode", txtPostcode.Text);
            Command.Parameters.AddWithValue("@City", txtCity.Text);
            Command.Parameters.AddWithValue("@State", txtState.Text);
            Command.Parameters.AddWithValue("@Country", txtCountry.Value);
            Command.Parameters.AddWithValue("@NationalInsurance", txtNationalInsurance.Text);
            if (cboJobGrade.SelectedValue.Length > 0)
            {
                Command.Parameters.AddWithValue("@JobGrade", cboJobGrade.SelectedValue);
            }
            else
            {
                Command.Parameters.AddWithValue("@JobGrade", DBNull.Value);
            }

            Command.Parameters.AddWithValue("@JobTitle", txtJobTitle.Text);

            if (cboEmployementStatus.SelectedValue.Length > 0)
            {
                Command.Parameters.AddWithValue("@EmploymentStatus", cboEmployementStatus.SelectedValue);
            }
            else
            {
                Command.Parameters.AddWithValue("@EmploymentStatus", DBNull.Value);
            }
            if (cmbemprole.SelectedValue.Length > 0)
            {
                Command.Parameters.AddWithValue("@EmpRoleID", cmbemprole.SelectedValue);
            }
            else
            {
                Command.Parameters.AddWithValue("@EmpRoleID", 1);
            }
            res = txtDateofJoining.Value;
            if (res != "")
            {
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Command.Parameters.AddWithValue("@DateofJoining", d);
            }
            else
            {
                Command.Parameters.AddWithValue("@DateofJoining", DBNull.Value);
            }
            res = txtDateofLeaving.Value;
            if (res != "")
            {
                DateTime d = DateTime.ParseExact(res, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                Command.Parameters.AddWithValue("@DateofLeaving", d);
            }
            else
            {
                Command.Parameters.AddWithValue("@DateofLeaving", DBNull.Value);
            }              

            Command.Parameters.AddWithValue("@WorkTelephoneNo", txtWorkTelephoneNo.Text);
            Command.Parameters.AddWithValue("@Extention", txtExtention.Text);
            Command.Parameters.AddWithValue("@WorkMobileNo", txtWorkMobileNo.Text);

            Command.Parameters.AddWithValue("@NKAFirstName", txtNKAFirstName.Text);
            Command.Parameters.AddWithValue("@NKAMiddleName", txtNKAMiddleName.Text);
            Command.Parameters.AddWithValue("@NKASurName", txtNKASurName.Text);
            Command.Parameters.AddWithValue("@NKAFirstNameChines", txtNKAFirstNameChinese.Text);
            Command.Parameters.AddWithValue("@NKASurNameChiness", txtNKASurNameChiness.Text);
            Command.Parameters.AddWithValue("@NKAMiddleChiness", txtNKAMiddleNameChiness.Text);
            Command.Parameters.AddWithValue("@NKASex", cboNKASex.SelectedValue);
            Command.Parameters.AddWithValue("@NKARelationship", txtNKBRelationship.SelectedValue);
            Command.Parameters.AddWithValue("@NKALandLineNumber", txtNKALandLineNumber.Text);
            Command.Parameters.AddWithValue("@NKAMobileNo", txtNKAMobileNo.Text);
            Command.Parameters.AddWithValue("@NKAEmailAddress", txtNKAEmailAddress.Text);
            Command.Parameters.AddWithValue("@NKAHomeAddress", txtNKAHomeAddress.Text);
            Command.Parameters.AddWithValue("@NKACity", txtNKACity.Text);
            Command.Parameters.AddWithValue("@NKAState", txtNKAState.Text);
            Command.Parameters.AddWithValue("@NKACountry", txtNKACountry.Value);

            Command.Parameters.AddWithValue("@NKBFirstName", txtNKBFirstName.Text);
            Command.Parameters.AddWithValue("@NKBMiddleName", txtNKBMiddleName.Text);
            Command.Parameters.AddWithValue("@NKBSurName", txtNKBSurName.Text);
            Command.Parameters.AddWithValue("@NKBFirstNameChinese", txtNKBSurNameChines.Text);
            Command.Parameters.AddWithValue("@NKBSurNameChines", txtNKBSurNameChines.Text);
            Command.Parameters.AddWithValue("@NKBMiddleChiness", txtNKBMiddleChiness.Text);
            Command.Parameters.AddWithValue("@NKBSex", cboNKBSex.SelectedValue);
            Command.Parameters.AddWithValue("@NKBRelationship", txtNKBRelationship.Text);
            Command.Parameters.AddWithValue("@NKBLandLineNumber", txtNKBLandLineNumber.Text);
            Command.Parameters.AddWithValue("@NKBMobileNo", txtNKBMobileNo.Text);
            Command.Parameters.AddWithValue("@NKBEmailAddress", txtNKBEmailAddress.Text);
            Command.Parameters.AddWithValue("@NKBHomeAddress", txtNKBHomeAddress.Text);
            Command.Parameters.AddWithValue("@NKBCity", txtNKBCity.Text);
            Command.Parameters.AddWithValue("@NKBState", txtNKBState.Text);
            Command.Parameters.AddWithValue("@NKBCountry", txtNKBCountry.Value);
            Command.ExecuteNonQuery();
            Connection.Close();
            Response.Redirect("Employee.aspx");
        }
        protected void cmdAddNew_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = "";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal1", "$('#myModal1').modal();", true);
        }
        protected void btninsert_Click(object sender, EventArgs e)
        {
        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            string ID = SessionValue("AutoID").ToString();

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            SqlCommand Command;

            //Declare string InsertSQL 
            String InsertSQL = "Insert into [dbo].[EmpDocument] ([EmployeeID], [DocumentName],  [FileType], [FileName], [FileData], [CreatedBy], [CreatedDate] ) VALUES ( @EmployeeID, @DocumentName, @FileType, @FileName, @FileData, @CreatedBy, @CreatedDate) ";

            

           


                foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                {
                    //To create a PostedFile

                    Command = new SqlCommand(InsertSQL, Connection);
                    Command.Parameters.AddWithValue("@CreatedBy", SessionValue("UserName"));
                    Command.Parameters.AddWithValue("@CreatedDate", DateTime.Today);

                   

                    HttpPostedFile File = postedFile;

                    int size = File.ContentLength;
                    string name = File.FileName;
                    int position = name.LastIndexOf("\\");
                    name = name.Substring(position + 1);
                    string contentType = File.ContentType;
                    byte[] fileData = new byte[size];
                    File.InputStream.Read(fileData, 0, size);

                    Command.Parameters.AddWithValue("@EmployeeID", SessionValue("EmpID"));
                    Command.Parameters.AddWithValue("@DocumentName", name);
                    
                    Command.Parameters.AddWithValue("@FileType", contentType);
                    Command.Parameters.AddWithValue("@FileName", name);
                    Command.Parameters.AddWithValue("@FileData", fileData);
                    Command.ExecuteNonQuery();


                }

                Connection.Close();
           


            BindGrid();
            tabactive();
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            string ID = SessionValue("AutoID").ToString();


            DataTable file = GetAFile(Convert.ToInt16(ID));
            DataRow row = file.Rows[0];

            string name = (string)row["FileName"];
            string contentType = (string)row["FileType"];
            Byte[] data = (Byte[])row["FileData"];

            // Send the file to the browser
            Response.AddHeader("Content-type", contentType);
            Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
            Response.BinaryWrite(data);
            Response.Flush();
            Response.End();
        }
        // Get a file from the database by ID
        public static DataTable GetAFile(int id)
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            DataTable file = new DataTable();

            Connection.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Connection;
            cmd.CommandTimeout = 0;

            cmd.CommandText = "SELECT ID, FileName, FileType, FileData FROM EmpDocument "
                + "WHERE ID=@ID";
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();

            cmd.Parameters.Add("@ID", SqlDbType.Int);
            cmd.Parameters["@ID"].Value = id;

            adapter.SelectCommand = cmd;
            adapter.Fill(file);

            Connection.Close();

            return file;
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            Connection.Open();
            Session["AutoID"] = (sender as LinkButton).CommandArgument;
            String sql = "delete from [dbo].[EmpDocument] where [ID] = @ID";

            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.Parameters.AddWithValue("@ID", SessionValue("AutoID"));
            Command.ExecuteNonQuery();
            BindGrid();
            tabactive();
        }
        protected void tabactive()
        {
            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");
            tab3.Attributes.Remove("class");
            tab4.Attributes.Remove("class");
            tab1.Attributes.Add("class", "tab-pane");
            tab2.Attributes.Add("class", "tab-pane");
            tab3.Attributes.Add("class", "tab-pane");
            tab4.Attributes.Add("class", "tab-pane");
            tab5.Attributes.Add("class", "tab-pane active");
       }
        protected void BindGrid()
        {

            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();

            String sql = " SELECT ROW_NUMBER() OVER (ORDER BY  ID ) AS No,  DocumentName,ID FROM [dbo].[EmpDocument]  ";


            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = sql;

            MyRepeater.DataSource = Command.ExecuteReader();
            MyRepeater.DataBind();

            Connection.Close();

        }
        private string SessionValue(string Key)
        {
            Object SessionObject = Session[Key];
            if (SessionObject == null)
            {
                return "";
            }
            else
            {
                return Session[Key].ToString();
            }
        }
        private string RequestValue(string Key)
        {
            Object RequestObject = Request[Key];
            if (RequestObject == null)
            {
                return "";
            }
            else
            {
                return Request[Key].ToString();
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Employee.aspx");
        }


        private bool IsNumeric(string TextNumber)
        {
            Regex RegexIsNumeric = new Regex(@"^(?:(?:[+\-]?\$?)|(?:\$?[+\-]?))?(?:(?:\d{1,3}(?:(?:,\d{3})|(?:\d))*(?:\.(?:\d*|\d+[eE][+\-]\d+))?)|(?:\.\d+(?:[eE][+\-]\d+)?))$");
            Match MatchIsNumeric = RegexIsNumeric.Match(TextNumber);
            return MatchIsNumeric.Success;
        }

        private bool IsDate(String s)
        {
            DateTime Result = new DateTime();
            return DateTime.TryParse(s, out Result);
        }
        protected void LoadcboCompanyID()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, CompanyName from [Company] order by CompanyName";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboCompanyID.DataTextField = "CompanyName";
            cboCompanyID.DataValueField = "ID";
            cboCompanyID.DataSource = Command.ExecuteReader();
            cboCompanyID.DataBind();
            Connection.Close();
        }

        protected void LoadcboDepartmentID()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, DepartmentName from [Department] order by DepartmentName";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboDepartmentID.DataTextField = "DepartmentName";
            cboDepartmentID.DataValueField = "ID";
            cboDepartmentID.DataSource = Command.ExecuteReader();
            cboDepartmentID.DataBind();
            Connection.Close();
        }

        protected void LoadcboEmploymentStatus()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, EmploymentStatus from [EmploymentStatus] order by EmploymentStatus";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboEmployementStatus.DataTextField = "EmploymentStatus";
            cboEmployementStatus.DataValueField = "ID";
            cboEmployementStatus.DataSource = Command.ExecuteReader();
            cboEmployementStatus.DataBind();
            Connection.Close();
        }
        protected void LoadcboJobGrade()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, Grade from [EmpJobGrade] order by Grade";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cboJobGrade.DataTextField = "Grade";
            cboJobGrade.DataValueField = "ID";
            cboJobGrade.DataSource = Command.ExecuteReader();
            cboJobGrade.DataBind();
            Connection.Close();
        }

        protected void LoadcboEmpRole()
        {
            SqlConnection Connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            Connection.Open();
            String sql = "select ID, RoleName from [EmpRole] order by RoleName";
            SqlCommand Command = new SqlCommand(sql, Connection);
            cmbemprole.DataTextField = "RoleName";
            cmbemprole.DataValueField = "ID";
            cmbemprole.DataSource = Command.ExecuteReader();
            cmbemprole.DataBind();
            Connection.Close();
        }
    }
}